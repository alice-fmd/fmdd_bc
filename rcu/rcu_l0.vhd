------------------------------------------------------------------------------
-- Title      : Architecture that makes triggers
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu_trig.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-19
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/10/02  1.0      cholm	Created
------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

-------------------------------------------------------------------------------
architecture l0_stim of rcu is
  constant PERIOD          : time               := 1 sec / 40000000;
  signal   rclk_i          : std_logic;
  signal   sclk_i          : std_logic;
  signal   writ_i          : std_logic          := 'H';
  signal   grst_i          : std_logic;
  signal   addr_i          : std_logic_vector(39 downto 20);  -- Debug
  signal   data_i          : std_logic_vector(19 downto 0);   -- Debug
  signal   what_i          : string(7 downto 1) := (others => ' ');
  signal   current_delay_i : integer;
  signal   current_width_i : integer;

  type delay_list is array (natural range <>) of time;
  constant delays_i : delay_list := (2   ns, 
                                     5   ns,
                                     7   ns,
                                     10  ns,
                                     15  ns,
                                     20  ns,
                                     25  ns);

  type width_list is array (natural range <>) of time;
  constant widths_i : width_list := (10 ns,
                                     15 ns,
                                     20 ns,
                                     25 ns,
                                     27 ns,
                                     30 ns,
                                     50 ns,
                                     100 ns,
                                     110 ns);    
begin  -- architecture stimulus
  connectivity : block is
  begin  -- block  connectivity
    rclk      <= rclk_i;
    sclk      <= sclk_i;
    writ      <= writ_i;
    addr_i    <= bd(39 downto 20);
    data_i    <= bd(19 downto 0);
    grst      <= grst_i;
    scl_a     <= 'H';
    scl_b     <= 'H';
    sda_out_a <= 'H';
    sda_out_b <= 'H';
  end block connectivity;

  -- Reset
  reset_it : entity rcu_model.reseter port map (rstb => grst_i);

  -- 40 Mhz
  rclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 40000000)
    port map (clk       => rclk_i);

  -- 10 MHz  
  sclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 10000000)
    port map (clk       => sclk_i);

  stim : process is
  begin  -- process stim
    bd     <= (others => '0');
    writ_i <= 'H';
    cstb   <= 'H';
    lvl0   <= '0';
    lvl1   <= '1';
    lvl2   <= '1';
    wait until grst_i = '1';

    wait for 100 ns;
    for i in widths_i'range loop
      current_width_i <= widths_i(i) / 1 ns; 
      for j in delays_i'range loop
        wait for  PERIOD;
        current_delay_i <= delays_i(j) / 1 ns;
        wait until rising_edge(rclk_i);
        wait for PERIOD;
        if delays_i(j) /= 0 ns then
          wait for delays_i(j);
        end if;
        lvl0 <= '1';
        wait for widths_i(i);
        lvl0 <= '0';
      end loop;  -- j
      wait for 100 ns;
    end loop;  -- i

    wait;
  end process stim;
end architecture l0_stim;
------------------------------------------------------------------------------
-- 
-- EOF
--
