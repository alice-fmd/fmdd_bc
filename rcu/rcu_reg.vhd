------------------------------------------------------------------------------
-- Title      : Architecture that reads/writes registers
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu_reg.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-19
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/10/02  1.0      cholm	Created
------------------------------------------------------------------------------

library rcu_model;
use rcu_model.rcu_pack.all;

-------------------------------------------------------------------------------
architecture reg_stim of rcu is
  signal rclk_i : std_logic;
  signal sclk_i : std_logic;
  signal writ_i : std_logic          := 'H';
  signal grst_i : std_logic;
  signal addr_i : std_logic_vector(39 downto 20);  -- Debug
  signal data_i : std_logic_vector(19 downto 0);   -- Debug
  signal what_i : string(7 downto 1) := (others => ' ');

begin  -- architecture stimulus
  connectivity : block is
  begin  -- block  connectivity
    rclk      <= rclk_i;
    sclk      <= sclk_i;
    writ      <= writ_i;
    addr_i    <= bd(39 downto 20);
    data_i    <= bd(19 downto 0);
    grst      <= grst_i;
    scl_a     <= 'H';
    scl_b     <= 'H';
    sda_out_a <= 'H';
    sda_out_b <= 'H';
  end block connectivity;

  -- Reset
  reset_it : entity rcu_model.reseter port map (rstb => grst_i);

  -- 40 Mhz
  rclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 40000000)
    port map (clk       => rclk_i);

  -- 10 MHz  
  sclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 10000000)
    port map (clk       => sclk_i);

  stim : process is
  begin  -- process stim
    bd     <= (others => '0');
    writ_i <= 'H';
    cstb   <= 'H';
    lvl0   <= '0';
    lvl1   <= '1';
    lvl2   <= '1';
    wait until grst_i = '1';

    wait for 1.5 us;

    exec_commands(hadd => HADD,
                  bd   => bd,
                  cstb => cstb,
                  writ => writ_i,
                  ackn => ackn,
                  trsf => trsf,
                  rclk => rclk_i,
                  name => what_i);
    wait for 1 us;
    read_registers(hadd => HADD,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);
    wait for 1 us;
    write_registers(hadd => HADD,
                    bd   => bd,
                    cstb => cstb,
                    writ => writ_i,
                    ackn => ackn,
                    trsf => trsf,
                    rclk => rclk_i,
                    name => what_i);
    wait for 1 us;
    exec_command(hadd  => HADD,
                 bcast => '0',
                 what  => "CSR1CLR",
                 bd    => bd,
                 cstb  => cstb,
                 writ  => writ_i,
                 ackn  => ackn,
                 trsf  => trsf,
                 rclk  => rclk_i,
                 name  => what_i);
    wait for 1 us;
    read_registers(hadd => HADD,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);
    wait;
  end process stim;
end architecture reg_stim;

------------------------------------------------------------------------------
-- 
-- EOF
--
