------------------------------------------------------------------------------
-- Title      : Architecure that runs conversion
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu_cnv.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-25
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/10/02  1.0      cholm   Created
------------------------------------------------------------------------------

library rcu_model;
use rcu_model.rcu_pack.all;
library msmodule2_lib;
use msmodule2_lib.msm2_interface_i2c_pack.all;

-------------------------------------------------------------------------------
architecture i2c2_stim of rcu is
  signal rclk_i : std_logic          := '0';
  signal sclk_i : std_logic          := '0';
  signal writ_i : std_logic          := 'H';
  signal grst_i : std_logic;
  signal addr_i : std_logic_vector(39 downto 20);  -- Debug
  signal data_i : std_logic_vector(19 downto 0);   -- Debug
  signal what_i : string(7 downto 1) := (others => ' ');

  -- SDA_IN  has 14.6ns delay from input to debug output (VITAL)
  -- SCL     has 10.1ns delay from input to debug output (VITAL)
  -- SDA_OUT has 3ns    delay from input to debug output (VITAL)
  -- RCLK    has 10.9ns delay from input to debug output (VITAL)
  constant SCL_DELAY     : time := 26 ns;  -- 36 ns;  -- Delay in cables
  constant SDA_IN_DELAY  : time := 23 ns;  -- 38 ns;  -- Delay in cables
  constant SDA_OUT_DELAY : time := 25 ns;  -- 28 ns;  -- Delay in cables
  constant RCLK_DELAY    : time := 1.5625 ns;  -- Delay in cables
  -- RCLK_DELAY = 1      is OK
  -- RCLK_DELAY = 1.5625 fails with VITAL and Verilog MSM
  -- RCLK_DELAY = 1.5625 fails (similar to HW) with VITAL and VHDL MSM 
  -- RCLK_DELAY = 1.6    fails (similar to HW) with VITAL and VHDL MSM 
  -- RCLK_DELAY = 1.5    succeeds with VITAL and VHDL MSM
  -- RCLK_DELAY = 1.5 -> 12.4ns delay from source to probe-point
  
  signal err_reg_i : std_logic_vector(3 downto 0);

  signal A_in_i   : std_logic_vector(15 downto 0) := X"dead";
  signal B_in_i   : std_logic_vector(15 downto 0) := X"beef";
  signal result_i : std_logic_vector(15 downto 0) := (others => 'Z');

  signal exec_i     : std_logic                    := '0';
  signal rnw_i      : std_logic                    := '0';
  signal bcast_i    : std_logic                    := '0';
  signal branch_i   : std_logic                    := '0';
  signal fec_add_i  : std_logic_vector(3 downto 0) := X"0";
  signal reg_add_i  : std_logic_vector(7 downto 0) := X"00";
  signal i2c_busy_i : std_logic;
  signal rst_i      : std_logic                    := '1';

  signal scl_a_i     : std_logic;
  signal sda_out_a_i : std_logic;
  signal sda_in_a_i  : std_logic;
  
  signal i : integer;
  signal j : integer;

  -- purpose: Read one register via I2C from BC
  procedure read_one (
    signal   clk     : in  std_logic;   -- Clock
    signal   busy    : in  std_logic;
    constant fec     : in  integer range 0 to 15;            -- FEC number
    constant reg     : in  integer range 0 to 255;           -- Register
    signal   exec    : out std_logic;   -- Execute it
    signal   rnw     : out std_logic;   -- Read-not-write
    signal   bcast   : out std_logic;   -- Broadcast
    signal   branch  : out std_logic;   -- '1' for branch B, else branch 'A'
    signal   fec_add : out std_logic_vector(3 downto 0);     -- FEC #
    signal   reg_add : out std_logic_vector(7 downto 0)) is  -- Register #
  begin  -- read_one
    if busy = '1' then
      wait until busy = '0';
    end if;

    bcast   <= '0';
    rnw     <= '1';
    branch  <= '0';
    fec_add <= int2slv(fec, 4);
    reg_add <= int2slv(reg, 8);


    spacer(rclk => clk); report "After spacer" severity note;
    wait until rising_edge(clk);
    exec <= '1';
    spacer(rclk => clk); report "After spacer" severity note;
    -- wait for 4 * PERIOD;
    exec <= '0';

    if busy = '1' then
      wait until busy = '0';
    end if;
  end read_one;
  
begin  -- architecture stimulus
  connectivity : block is
  begin  -- block  connectivity
    rclk       <= rclk_i      after RCLK_DELAY;
    sclk       <= sclk_i;
    writ       <= writ_i;
    addr_i     <= bd(39 downto 20);
    data_i     <= bd(19 downto 0);
    bd         <= (others => '0');
    cstb       <= 'H';
    lvl0       <= '0';
    lvl1       <= '1';
    lvl2       <= '1';
    grst       <= grst_i;
    rst_i      <= not grst_i;
    scl_b      <= 'H';
    sda_out_b  <= 'H';
    scl_a      <= scl_a_i     after SCL_DELAY;
    sda_out_a  <= sda_out_a_i after SDA_OUT_DELAY;
    sda_in_a_i <= sda_in_a    after SDA_IN_DELAY;
                 
  end block connectivity;

  -- I2C interface
  msm2_interface_I2C_1 : msm2_interface_I2C
    port map (
      rst          => rst_i,                  -- [in]
      clk          => rclk_i,                 -- [in]
      exec         => exec_i,                 -- [in]
      rd_nwr       => rnw_i,                  -- [in]
      ld_regs      => '0',                    -- [in]
      fec_add      => fec_add_i,              -- [in]
      bc_reg_add   => reg_add_i,              -- [in]
      B1_din       => A_in_i(7 downto 0),     -- [in]
      B2_din       => A_in_i(15 downto 8),    -- [in]
      rst_rslt     => '0',                    -- [in]
      sv_rslt      => open,                   -- [out]
      sda_in_B1    => result_i(7 downto 0),   -- [out]
      sda_in_B2    => result_i(15 downto 8),  -- [out]
      end_rd       => open,                   -- [out]
      I2C_BSY      => i2c_busy_i,             -- [out]
      err_fec_ack  => err_reg_i(0),           -- [out]
      err_bc_add   => err_reg_i(1),           -- [out]
      err_din1_ack => err_reg_i(2),           -- [out]
      err_din2_ack => err_reg_i(3),           -- [out]
      fsm_state    => open,                   -- [out]
      bcast        => bcast_i,                -- [in]
      sda_in       => sda_in_a_i,             -- [in]
      sda_out      => sda_out_a_i,            -- [out]
      s_clk        => scl_a_i);               -- [out]

  -- Reset
  reset_it : entity rcu_model.reseter port map (rstb => grst_i);

  -- 40 Mhz
  rclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 40000000)
    port map (clk       => rclk_i);

  -- 10 MHz  
  sclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 10000000)
    port map (clk       => sclk_i);


  stim : process is
  begin  -- process stim

    report "Doing a reset" severity note;
    wait until grst_i = '1';
    report "After reset" severity note;

    spacer(rclk => rclk_i); report "After spacer" severity note;

    for i in 0 to 1 loop
      for j in 0 to 2 loop
        report "Reading from FEC " & integer'image(i) &
          " register " & integer'image(j) severity note;
        read_one(clk     => rclk_i,
                 busy    => i2c_busy_i,
                 fec     => i,
                 reg     => j,
                 exec    => exec_i,
                 rnw     => rnw_i,
                 bcast   => bcast_i,
                 branch  => branch_i,
                 fec_add => fec_add_i,
                 reg_add => reg_add_i);

        wait for 1 us;
        result_i <= (others => 'Z');
      end loop;  -- j
    end loop;  -- i
  end process stim;
end architecture i2c2_stim;

------------------------------------------------------------------------------
-- 
-- EOF
--
