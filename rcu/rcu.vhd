------------------------------------------------------------------------------
-- Title      : Model of the Read-out Controller Unit
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-19
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This model provides stimuli, as if send from the RCU. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2005-04-18  1.0      dk      Created
-- 2006/09/25  1.1      cholm   Tweek to new setup
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

library rcu_model;
use rcu_model.rcu_misc_pack.all;

entity rcu is
  generic (
    -- Hardware address
    HADD : std_logic_vector(4 downto 0) := "00000");  
  port (
    -- Altro Bus: all active low exept BD
    bd        : inout std_logic_vector(39 downto 0) := (others => 'L');  -- Bus
    writ      : out   std_logic;        -- Write strobe
    cstb      : out   std_logic;        -- Control strobe
    ackn      : in    std_logic;        -- Acknowledge
    eror      : in    std_logic;        -- Error
    trsf      : in    std_logic;        -- Transfer pulse from ALTRO's
    dstb      : in    std_logic;        -- Data strobe from ALTROs
    busy      : in    std_logic;        -- Busy from FECs
    lvl0      : out   std_logic;        -- Strictly not from the rcu
    lvl1      : out   std_logic;        -- Level 1 trigger (active low)
    lvl2      : out   std_logic;        -- Level 2 trigger (active low)
    grst      : out   std_logic;        -- Global reset
    sclk      : out   std_logic                     := '0';  -- Sample clock 
    rclk      : out   std_logic                     := '0';  -- Normal clock 
    sda_in_a  : in    std_logic;        -- Serial data from BC (branch A)
    sda_in_b  : in    std_logic;        -- Serial data from BC (branch b)
    scl_a     : out   std_logic                     := 'H';  -- I2C clock to BC
    scl_b     : out   std_logic                     := 'H';  -- I2C clock to BC
    sda_out_a : out   std_logic                     := 'H';  -- I2C data to BC
    sda_out_b : out   std_logic                     := 'H');  -- I2C data to BC
end entity rcu;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package rcu_pack is
  component rcu
    generic (
      HADD : std_logic_vector(4 downto 0));  -- Hardware address
    port (
      bd        : inout std_logic_vector(39 downto 0) := (others => 'L');  -- Bus
      writ      : out   std_logic;      -- Writ strobe
      cstb      : out   std_logic;      -- Control strobe
      ackn      : in    std_logic;      -- Acknowledge
      eror      : in    std_logic;      -- Error
      trsf      : in    std_logic;      -- Transfer pulse from ALTRO's
      dstb      : in    std_logic;      -- Data strobe from ALTROs
      busy      : in    std_logic;      -- Busy from FECs
      lvl0      : out   std_logic;      -- Strictly not from the rcu
      lvl1      : out   std_logic;      -- Level 1 trigger (active low)
      lvl2      : out   std_logic;      -- Level 2 trigger (active low)
      grst      : out   std_logic;      -- Global reset
      sclk      : out   std_logic;      -- Sample clock (10 MHz)
      rclk      : out   std_logic;      -- Normal clock (40 MHz)
      sda_in_a  : in    std_logic;      -- Serial data from BC (branch A)
      sda_in_b  : in    std_logic;      -- Serial data from BC (branch b)
      scl_a     : out   std_logic;      -- I2C clock to BC
      scl_b     : out   std_logic;      -- I2C clock to BC
      sda_out_a : out   std_logic;      -- I2C data to BC
      sda_out_b : out   std_logic);     -- I2C data to BC
  end component rcu;
end package rcu_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
