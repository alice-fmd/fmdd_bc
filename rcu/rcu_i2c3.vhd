------------------------------------------------------------------------------
-- Title      : Architecure that runs conversion
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu_cnv.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-03-03
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/10/02  1.0      cholm   Created
------------------------------------------------------------------------------

library rcu_model;
use rcu_model.rcu_pack.all;
library msmodule2_lib;
use msmodule2_lib.msm2_msmodule_pack.all;

-------------------------------------------------------------------------------
architecture i2c3_stim of rcu is
  signal rclk_i : std_logic          := '0';
  signal sclk_i : std_logic          := '0';
  signal writ_i : std_logic          := 'H';
  signal grst_i : std_logic;
  signal addr_i : std_logic_vector(39 downto 20);  -- Debug
  signal data_i : std_logic_vector(19 downto 0);   -- Debug
  signal what_i : string(7 downto 1) := (others => ' ');

  -- SDA_IN  has 14.6ns delay from input to debug output (VITAL)
  -- SCL     has 10.1ns delay from input to debug output (VITAL)
  -- SDA_OUT has 3ns    delay from input to debug output (VITAL)
  -- RCLK    has 10.9ns delay from input to debug output (VITAL)
  constant SCL_DELAY     : time := 26 ns;  -- 36 ns;  -- Delay in cables
  constant SDA_IN_DELAY  : time := 23 ns;  -- 38 ns;  -- Delay in cables
  constant SDA_OUT_DELAY : time := 25 ns;  -- 28 ns;  -- Delay in cables
  --constant RCLK_DELAY    : time := 1.5625 ns;  -- Delay in cables
  constant RCLK_DELAY    : time := 10.9 ns;  -- Delay in cables
  -- RCLK_DELAY = 1      is OK
  -- RCLK_DELAY = 1.5625 fails with VITAL and Verilog MSM
  -- RCLK_DELAY = 1.5625 fails (similar to HW) with VITAL and VHDL MSM 
  -- RCLK_DELAY = 1.6    fails (similar to HW) with VITAL and VHDL MSM 
  -- RCLK_DELAY = 1.5    succeeds with VITAL and VHDL MSM
  -- RCLK_DELAY = 1.5 -> 12.4ns delay from source to probe-point
  
  
  signal rst_i    : std_logic                     := '1';
  signal add_i    : std_logic_vector(15 downto 0) := (others => '0');
  signal dat_i    : std_logic_vector(31 downto 0) := (others => '0');
  signal wen_i    : std_logic                     := '0';
  signal result_i : std_logic_vector(31 downto 0) := (others => 'Z');
  
  signal interrupt_a_i : std_logic := '0';
  signal scl_a_i       : std_logic;
  signal sda_out_a_i   : std_logic;
  signal sda_in_a_i    : std_logic;
  signal interrupt_b_i : std_logic := '0';
  signal scl_b_i       : std_logic;
  signal sda_out_b_i   : std_logic;
  signal sda_in_b_i    : std_logic;
  
  signal i : integer;
  signal j : integer;

  -- purpose: Read one register via I2C from BC
  procedure read_one (
    signal   clk  : in  std_logic;                      -- Clock
    constant fec  : in  integer range 0 to 15;          -- FEC number
    constant reg  : in  integer range 0 to 255;         -- Register
    signal   add  : out std_logic_vector(15 downto 0);  -- RCU Register
    signal   dat  : out std_logic_vector(31 downto 0);  -- Data for RCU Reg.
    signal   wen  : out std_logic) is                   -- Register #
  begin  -- read_one
    if busy = '1' then
      wait until busy = '0';
    end if;

    add(15 downto 4)  <= X"800";           -- Prefix
    add(3 downto 0)   <= X"5";             -- SCADD address
    dat(31 downto 16) <= X"0000";
    dat(15)           <= '0';
    dat(14)           <= '1';              -- Read not write
    dat(13)           <= '0';              -- Broadcast
    dat(12)           <= '0';              -- Branch
    dat(11 downto 8)  <= int2slv(fec, 4);  -- FEC
    dat(7 downto 0)   <= int2slv(reg, 8);
    wen               <= '1';
    spacer(rclk => clk); report "Wrote to SCADD" severity note;
    wait until rising_edge(clk);
    wen               <= '0';

    spacer(rclk                 => clk);
    add(15 downto 4) <= X"801";         -- prefix
    add(3 downto 0)  <= X"0";           -- SCEXEC address
    dat              <= (others => '0');
    wen              <= '1';
    spacer(rclk                 => clk); report "Wrote to SCEXEC" severity note;
    wait until rising_edge(clk);
    wen              <= '0';

    wait for 10 us;
  end read_one;
  
begin  -- architecture stimulus
  connectivity : block is
  begin  -- block  connectivity
    rclk       <= rclk_i      after RCLK_DELAY;
    sclk       <= sclk_i;
    writ       <= writ_i;
    addr_i     <= bd(39 downto 20);
    data_i     <= bd(19 downto 0);
    bd         <= (others => '0');
    cstb       <= 'H';
    lvl0       <= '0';
    lvl1       <= '1';
    lvl2       <= '1';
    grst       <= grst_i;
    rst_i      <= not grst_i;
    scl_a      <= scl_a_i     after SCL_DELAY;
    sda_out_a  <= sda_out_a_i after SDA_OUT_DELAY;
    sda_in_a_i <= sda_in_a    after SDA_IN_DELAY;
    scl_b      <= scl_b_i     after SCL_DELAY;
    sda_out_b  <= sda_out_b_i after SDA_OUT_DELAY;
    sda_in_b_i <= sda_in_b    after SDA_IN_DELAY;
                 
  end block connectivity;

  msm2_msmodule_1 : msm2_msmodule
    port map (
      rst           => rst_i,           -- [in]
      clk           => rclk_i,          -- [in]
      wen_dcs       => wen_i,           -- [in]
      addr_dcs      => add_i,           -- [in]
      data_dcs      => dat_i,           -- [in]
      dat_out_dcs   => result_i,        -- [out]
      fec_act_list  => X"00030003",     -- [in]
      rcu_version   => X"ae123110",     -- [in]
      err_reg_a     => X"00000000",     -- [in]
      err_reg_b     => X"00000000",     -- [in]
      sda_in_A      => sda_in_a_i,      -- [in]
      sda_out_A     => sda_out_A_i,     -- [out]
      s_clk_A       => scl_a_i,         -- [out]
      interruptA_in => interrupt_a_i,   -- [in]
      warningTOdcs  => open,            -- [out]
      interruptB_in => interrupt_b_i,   -- [in]
      sda_in_B      => sda_in_b_i,      -- [in]
      sda_out_B     => sda_out_b_i,     -- [out]
      s_clk_B       => scl_b_i);        -- [out]

  -- Reset
  reset_it : entity rcu_model.reseter port map (rstb => grst_i);

  -- 40 Mhz
  rclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 40000000)
    port map (clk       => rclk_i);

  -- 10 MHz  
  sclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 10000000)
    port map (clk       => sclk_i);


  stim : process is
  begin  -- process stim

    report "Doing a reset" severity note;
    wait until grst_i = '1';
    report "After reset" severity note;

    spacer(rclk => rclk_i); report "After spacer" severity note;

    for i in 0 to 1 loop
      for j in 0 to 2 loop
        report "Reading from FEC " & integer'image(i) &
          " register " & integer'image(j) severity note;
        read_one(clk => rclk_i,
                 fec => i,
                 reg => j,
                 wen => wen_i,
                 add => add_i,
                 dat => dat_i);

        wait for 1 us;
        result_i <= (others => 'Z');
      end loop;  -- j
    end loop;  -- i
  end process stim;
end architecture i2c3_stim;

------------------------------------------------------------------------------
-- 
-- EOF
--
