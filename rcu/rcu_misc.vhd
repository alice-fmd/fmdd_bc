------------------------------------------------------------------------------
-- Title      : Miscellanous stuff for the RCU model
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu_misc.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-03-09
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/25  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity clocker is
  generic (
    PERIOD :     time := 1 sec / 40000000;
    DELAY  :     time := 0 ns);  -- Clock cycle
  port (
    clk    : out std_logic);                 -- Clock
end entity clocker;

architecture behave of clocker is
  signal clk_i : std_logic := '0';
begin  -- architecture behave
  clk     <= clk_i;
  makeit       : process is
    variable init : boolean := true;
  begin  -- process makeit
    if init then
      wait for DELAY;
      init := false;
    end if;
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process makeit;
end architecture behave;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity reseter is
  generic (
    DUR   :     time := 100 ns;         -- Lenght of reset
    DELAY :     time := 0 ns);          -- Delay
  port (
    rstb  : out std_logic);
end entity reseter;

architecture behave of reseter is
begin  -- architecture behave
  process is
  begin  -- process
    rstb <= '1';
    wait for DELAY;
    rstb <= '0';
    wait for DUR;
    rstb <= 'H';                        -- Drive weak high to allow overwrite
    wait;                               -- forever
  end process;
end architecture behave;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
package rcu_misc_pack is
  constant L0_DELAY : time := 1.2 us;

  ----------------------------------------------------------------------------
  -- Simple clock generator 
  component clocker
    generic (
      PERIOD :     time;
    DELAY  :     time := 0 ns);               -- Clock cycle
    port (
      clk    : out std_logic);          -- Clock
  end component clocker;

  ----------------------------------------------------------------------------
  -- Simple reseter 
  component reseter
    generic (
      DUR   :     time := 100 ns;       -- Lenght of reset
      DELAY :     time := 0 ns);        -- Delay
    port (
      rstb  : out std_logic);
  end component reseter;

  ----------------------------------------------------------------------------
  type bus_const_t
    is record
         name  : string(7 downto 1);    -- Name of command
         writ  : std_logic;             -- Allow Write
         read  : std_logic;             -- Allow Read
         bcast : std_logic;             -- Allow Broadcast
         bcal  : std_logic;             -- BC (1) or ALTRO (0)
         code  : integer;               -- Instruction
       end record bus_const_t;
  type bus_const_array_t is array (natural range <>) of bus_const_t;

  ----------------------------------------------------------------------------
  constant bus_default : bus_const_t := (
    "_______", '0', '0', '0', '0', 16#00#);
  constant bc_instructions : bus_const_array_t(127 downto 0) := (
    0      => ("ZERO___", '0', '0', '0', '0', 16#00#),
    1      => ("T1_TH__", '1', '1', '1', '1', 16#01#),
    2      => ("FLASITH", '1', '1', '1', '1', 16#02#),
    3      => ("ALDIITH", '1', '1', '1', '1', 16#03#),
    4      => ("ALANITH", '1', '1', '1', '1', 16#04#),
    5      => ("VARIPTH", '1', '1', '1', '1', 16#05#),
    6      => ("T1_____", '0', '1', '0', '1', 16#06#),
    7      => ("FLASI__", '0', '1', '0', '1', 16#07#),
    8      => ("ALDII__", '0', '1', '0', '1', 16#08#),
    9      => ("ALANI__", '0', '1', '0', '1', 16#09#),
    10     => ("VARIP__", '0', '1', '0', '1', 16#0A#),
    11     => ("L1CNT__", '0', '1', '0', '1', 16#0B#),
    12     => ("L2CNT__", '0', '1', '0', '1', 16#0C#),
    13     => ("SCLKCNT", '0', '1', '0', '1', 16#0D#),
    14     => ("DSTBCNT", '0', '1', '0', '1', 16#0E#),
    15     => ("TSMWORD", '1', '1', '1', '1', 16#0F#),
    16     => ("USRATIO", '1', '1', '1', '1', 16#10#),
    17     => ("CSR0___", '1', '1', '1', '1', 16#11#),
    18     => ("CSR1___", '1', '1', '1', '1', 16#12#),
    19     => ("CSR2___", '1', '1', '1', '1', 16#13#),
    20     => ("CSR3___", '1', '1', '1', '1', 16#14#),
    -- Free address
    21     => ("FREE___", '0', '0', '0', '0', 16#15#),
    -- Commands 
    22     => ("CNTLAT_", '1', '0', '1', '1', 16#16#),
    23     => ("CNTCLR_", '1', '0', '1', '1', 16#17#),
    24     => ("CSR1CLR", '1', '0', '1', '1', 16#18#),
    25     => ("ALRST__", '1', '0', '1', '1', 16#19#),
    26     => ("BCRST__", '1', '0', '1', '1', 16#1A#),
    27     => ("STCNV__", '1', '0', '1', '1', 16#1B#),
    28     => ("SCEVL__", '1', '0', '1', '1', 16#1C#),
    29     => ("EVLRDO_", '1', '0', '1', '1', 16#1D#),
    30     => ("STTSM__", '1', '0', '1', '1', 16#1E#),
    31     => ("ACQRDO_", '1', '0', '1', '1', 16#1F#),
    -- FMDD
    32     => ("FMDSTAT", '0', '1', '0', '1', 16#20#),
    33     => ("L0CNT__", '0', '1', '0', '1', 16#21#),
    34     => ("HOLD_WA", '1', '1', '1', '1', 16#22#),
    35     => ("L1_TIMO", '1', '1', '1', '1', 16#23#),
    36     => ("L2_TIMO", '1', '1', '1', '1', 16#24#),
    37     => ("SHIFTCL", '1', '1', '1', '1', 16#25#),
    38     => ("STRIPS_", '1', '1', '1', '1', 16#26#),
    39     => ("CAL_LVL", '1', '1', '1', '1', 16#27#),
    40     => ("SHAPE_B", '1', '1', '1', '1', 16#28#),
    41     => ("VFS____", '1', '1', '1', '1', 16#29#),
    42     => ("VFP____", '1', '1', '1', '1', 16#2A#),
    43     => ("SAMPLEC", '1', '1', '1', '1', 16#2B#),
    44     => ("FMDDCMD", '1', '0', '1', '1', 16#2C#),
    -- Extra monitors 
    45     => ("T2_TH__", '1', '1', '1', '1', 16#2D#),
    46     => ("VASIPTH", '1', '1', '1', '1', 16#2E#),
    47     => ("VARIMTH", '1', '1', '1', '1', 16#2F#),
    48     => ("VASIPTH", '1', '1', '1', '1', 16#30#),
    49     => ("GTLU_TH", '1', '1', '1', '1', 16#31#),
    50     => ("T2_____", '0', '1', '0', '1', 16#32#),
    51     => ("VASIP__", '0', '1', '0', '1', 16#33#),
    52     => ("VARIM__", '0', '1', '0', '1', 16#34#),
    53     => ("VASIP__", '0', '1', '0', '1', 16#35#),
    54     => ("GTLU___", '0', '1', '0', '1', 16#36#),
    55     => ("T3_TH__", '1', '1', '1', '1', 16#37#),
    56     => ("TEMP1TH", '1', '1', '1', '1', 16#38#),
    57     => ("TEMP2TH", '1', '1', '1', '1', 16#39#),
    58     => ("ALDIUTH", '1', '1', '1', '1', 16#3A#),
    59     => ("ALANUTH", '1', '1', '1', '1', 16#3B#),
    60     => ("T3_____", '0', '1', '0', '1', 16#3C#),
    61     => ("TEMP1__", '0', '1', '0', '1', 16#3D#),
    62     => ("TEMP2__", '0', '1', '0', '1', 16#3E#),
    63     => ("ALDIU__", '0', '1', '0', '1', 16#3F#),
    64     => ("ALANU__", '0', '1', '0', '1', 16#40#),
    75     => ("T4_TH__", '1', '1', '1', '1', 16#41#),
    76     => ("VARUPTH", '1', '1', '1', '1', 16#42#),
    77     => ("VASUPTH", '1', '1', '1', '1', 16#43#),
    78     => ("VASUMTH", '1', '1', '1', '1', 16#44#),
    79     => ("VARUMTH", '1', '1', '1', '1', 16#45#),
    80     => ("T4_____", '0', '1', '0', '1', 16#46#),
    81     => ("VARUP__", '0', '1', '0', '1', 16#47#),
    82     => ("VASUP__", '0', '1', '0', '1', 16#48#),
    83     => ("VASUM__", '0', '1', '0', '1', 16#49#),
    84     => ("VARUM__", '0', '1', '0', '1', 16#4A#),
    85     => ("CALITER", '1', '1', '1', '1', 16#4B#),
    86     => ("MEBS___", '1', '1', '1', '1', 16#4C#),
    -- ALTRO RPINC command
    87     => ("RPINC__", '1', '0', '1', '0', 16#19#),
    others => bus_default
    );

  ----------------------------------------------------------------------------
  constant cmd_change_dac : integer := 0;  -- change dacs
  constant cmd_trigger    : integer := 1;  -- Make trigger sequence
  constant cmd_l0         : integer := 2;  -- Make L0 trigger
  constant cmd_reset      : integer := 3;  -- Reset
  constant cmd_calib_on   : integer := 4;  -- Turn on calibration mode;
  constant cmd_calib_off  : integer := 5;  -- Turn off calibration mode;
  constant cmd_cal_run    : integer := 8;  -- Calibration run
  
  ----------------------------------------------------------------------------
  type cmd_t is
    record                                            -- Record of commands
      code          : std_logic_vector(15 downto 0);  -- Instruction code
      name          : string(5 downto 1);             -- Name
    end record cmd_t;
  type cmd_list_t is array (0 to 15) of cmd_t;
  constant cmd_list : cmd_list_t := (
    cmd_change_dac => (X"0001", "CHDAC"),
    cmd_trigger    => (X"0002", "TRIGS"),
    cmd_l0         => (X"0004", "TRIG0"),
    cmd_reset      => (X"0008", "RESET"),
    cmd_calib_on   => (X"0010", "CALON"),
    cmd_calib_off  => (X"0020", "CALOF"),
    others         => (X"0000", "-----"));            -- commands

  -----------------------------------------------------------------------------
  function padstr (constant what : string) return string;
  
  ----------------------------------------------------------------------------
  function parity (constant x : std_logic_vector) return std_logic;

  ----------------------------------------------------------------------------
  procedure bus_raw_rw(
    constant data : in  std_logic_vector(19 downto 0);
    constant addr : in  std_logic_vector(39 downto 20);
    constant rw   : in  std_logic;
    signal   bd   : out std_logic_vector(39 downto 0);
    signal   writ : out std_logic;
    signal   cstb : out std_logic;
    signal   rclk : in  std_logic;
    signal   ackn : in  std_logic);

  ----------------------------------------------------------------------------
  procedure bus_rw (
    constant rw      : in  character;   -- Read (R) or Write (W)
    constant bcast   : in  std_logic;   -- Broadcast
    constant branch  : in  std_logic;   -- A/B
    constant board   : in  std_logic_vector(3 downto 0);  -- Board no.
    constant chip    : in  std_logic_vector(2 downto 0);
    constant channel : in  std_logic_vector(3 downto 0);
    constant what    : in  bus_const_t;  -- Instruction
    constant data    : in  std_logic_vector(19 downto 0);  -- Data
    signal   bd      : out std_logic_vector(39 downto 0);  -- Bus
    signal   writ    : out std_logic;   -- Write pulse
    signal   cstb    : out std_logic;   -- Control stobe
    signal   clk     : in  std_logic;   -- Clock
    signal   ackn    : in  std_logic);  -- Acknowledge

  ----------------------------------------------------------------------------
  procedure bus_send (
    constant name    : in  string(7 downto 1);             -- Instruction name
    constant rw      : in  character;                      -- Read/Write
    constant bcast   : in  std_logic;                      -- Broadcast? 
    constant branch  : in  std_logic;                      -- Branch
    constant board   : in  std_logic_vector(3 downto 0);   -- Board #
    constant chip    : in  std_logic_vector(2 downto 0);   -- ALTRO #
    constant channel : in  std_logic_vector(3 downto 0);   -- Channel #
    constant data    : in  std_logic_vector(19 downto 0);  -- Data
    signal   bd      : out std_logic_vector(39 downto 0);  -- Bus
    signal   writ    : out std_logic;                      -- Write pulse
    signal   cstb    : out std_logic;                      -- Control strobe
    signal   clk     : in  std_logic;                      -- Clock
    signal   ackn    : in  std_logic);

  ----------------------------------------------------------------------------
  procedure wait_for_trsf (
    signal rclk : in    std_logic;
    signal trsf : in    std_logic;
    signal bd   : inout std_logic_vector(39 downto 0));

  ----------------------------------------------------------------------------
  procedure wait_for_free (
    signal rclk : in std_logic;
    signal ackn : in std_logic);

  ----------------------------------------------------------------------------
  procedure spacer (
    signal rclk : in std_logic);

  -----------------------------------------------------------------------------
  -- purpose: Execute a BC command
  procedure exec_command(
    constant hadd  : in    std_logic_vector(4 downto 0) := "00000";
    constant what  : in    bus_const_t;  -- Instruction
    constant bcast : in    std_logic;    -- Broad-cast
    signal   bd    : inout std_logic_vector(39 downto 0);
    signal   cstb  : out   std_logic;
    signal   writ  : out   std_logic;
    signal   ackn  : in    std_logic;
    signal   trsf  : in    std_logic;
    signal   rclk  : in    std_logic;
    signal   name  : out   string(7 downto 1));

  -----------------------------------------------------------------------------
  -- purpose: Execute a BC command
  procedure exec_command(
    constant hadd  : in    std_logic_vector(4 downto 0) := "00000";
    constant what  : in    string;      -- Instruction
    constant bcast : in    std_logic;   -- Broad-cast
    signal   bd    : inout std_logic_vector(39 downto 0);
    signal   cstb  : out   std_logic;
    signal   writ  : out   std_logic;
    signal   ackn  : in    std_logic;
    signal   trsf  : in    std_logic;
    signal   rclk  : in    std_logic;
    signal   name  : out   string(7 downto 1));

  -----------------------------------------------------------------------------
  -- purpose: Execute all TPC BC commands
  procedure exec_commands(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1));

  ---------------------------------------------------------------------------
  -- purpose: Read all TPC BC registers
  procedure read_register(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    constant what : in    bus_const_t;  -- Instruction
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1));

  ---------------------------------------------------------------------------
  -- purpose: Read all TPC BC registers
  procedure read_register(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    constant what : in    string;       -- Instruction
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1));

  ---------------------------------------------------------------------------
  -- purpose: Read all TPC BC registers
  procedure read_registers(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1));

  -----------------------------------------------------------------------------
  -- purpose: Write to all TPC registers
  procedure write_register(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    constant what : in    bus_const_t;  -- Instruction
    constant data : in    integer;
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1));

  -----------------------------------------------------------------------------
  -- purpose: Write to all TPC registers
  procedure write_register(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    constant what : in    string;       -- Instruction
    constant data : in    integer;
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1));

  -----------------------------------------------------------------------------
  -- purpose: Write to all TPC registers
  procedure write_registers(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1));

  -----------------------------------------------------------------------------
  -- purpose: Make a trigger sequence
  procedure make_trig (
    constant l0_delay : in  time;       -- L0 delay
    constant l1_delay : in  time;       -- L1 delay
    constant l2_delay : in  time;       -- L2 delay
    constant l0_len   : in  time;       -- Trigger length
    constant l1_len   : in  time;       -- Trigger length
    constant l2_len   : in  time;       -- Trigger length
    signal   l0       : out std_logic;  -- L0 trigger
    signal   l1       : out std_logic;  -- L1 trigger
    signal   l2       : out std_logic;  -- L2 trigger
    signal   name     : out string(7 downto 1));

  -----------------------------------------------------------------------------
  -- purpose: Make a trigger sequence
  procedure make_trigger (
    signal l0   : out std_logic;            -- Positive logic 
    signal l1   : out std_logic;            -- Negative logic
    signal l2   : out std_logic;
    signal name : out string(7 downto 1));  -- Negative logic 

  -----------------------------------------------------------------------------
  -- Make altro RPINC command
  procedure rpinc (
    signal bd   : inout std_logic_vector(39 downto 0);
    signal cstb : out   std_logic;
    signal writ : out   std_logic;
    signal ackn : in    std_logic;
    signal trsf : in    std_logic;
    signal rclk : in    std_logic;
    signal name : out   string(7 downto 1));
  
  -----------------------------------------------------------------------------
  -- purpose: convert integer to std_logic_vector
  function int2slv (val : integer; width : positive) return std_logic_vector;

  -----------------------------------------------------------------------------
  -- purpose: convert std_logic_vector to string
  function slv2str (input : std_logic_vector) return string;

end package rcu_misc_pack;

-------------------------------------------------------------------------------
package body rcu_misc_pack is
  constant ACK_TIMEOUT : integer := 20;

  -----------------------------------------------------------------------------
  -- Purpose: Convert integer to std_logic_vector
  function int2slv (
    val             : integer;
    width           : positive) return std_logic_vector
  is
    variable retval : std_logic_vector(width - 1 downto 0);
  begin
    retval := std_logic_vector(to_unsigned(val, width));
    return retval;
  end int2slv;

  -----------------------------------------------------------------------------
  -- purpose: Convert std_logic_vector bus to string
  function slv2str (input : std_logic_vector) return string
  is
    variable retval       : string(input'length downto 1);
  begin  -- slv2str
    for i in input'range loop
      case input(i) is
        when '1' | 'H' => retval(i+1) := '1';
        when '0' | 'L' => retval(i+1) := '0';
        when 'U'       => retval(i+1) := 'U';
        when 'X'       => retval(i+1) := 'X';
        when 'Z' | 'W' => retval(i+1) := 'Z';
        when '-'       => retval(i+1) := '-';
        when others    => retval(i+1) := '?';
      end case;
    end loop;  -- i
    return retval;
  end slv2str;


  ---------------------------------------------------------------------------
  function padstr (constant what : string) return string
  is
    variable                tmp  : string(7 downto 1) := (others => '_');
    variable                i    : integer;
    variable                c    : character;
  begin
    for i in tmp'range loop
      if i > what'length then
        c := '_';
      else
        c := what(i);
      end if;
      tmp(tmp'length - i + 1) := c;
    end loop;  -- i
    return tmp;
  end padstr;
  
  ---------------------------------------------------------------------------
  -- purpose: calculate parity
  function parity (constant x   : std_logic_vector) return std_logic is
    variable                tmp : std_logic := '0';
  begin  -- function parity
    for j in x'range loop
      tmp                                   := tmp xor x(j);
    end loop;  -- j
    return tmp;
  end function parity;


  ---------------------------------------------------------------------------
  -- wait for ACK_N to go low, or time out
  procedure wait_for_ack(
    signal   rclk : in std_logic;
    signal   ackn : in std_logic) is
    variable i    :    integer;
  begin
    if ackn = '0' then
      wait until not (ackn = '0');
    end if;
    i   := ACK_TIMEOUT;
    while i > 0 and not (ackn = '0') loop
      wait until rising_edge(rclk);
      i := i - 1;
    end loop;
    assert i > 0 report "Acknowledge timeout" severity warning;
    wait until rising_edge(rclk);
  end wait_for_ack;

  ---------------------------------------------------------------------------
  -- wait for ACK_N to go low, or time out
  procedure wait_for_free (
    signal rclk : in std_logic;
    signal ackn : in std_logic) is
  begin
    if ackn = '0' then
      wait until not (ackn = '0');
    end if;
    wait until rising_edge(rclk);
  end wait_for_free;

  -----------------------------------------------------------------------------
  -- purpose: Wait for transfer signal, or time out.
  procedure wait_for_trsf (
    signal   rclk : in    std_logic;    -- Clock
    signal   trsf : in    std_logic;    -- Trsf strobe
    signal   bd   : inout std_logic_vector(39 downto 0))
  is
    variable i    :       integer;
  begin
    bd <= (others => 'Z');
    if trsf = '0' then
      wait until not(trsf = '0');
    end if;
    i   := ACK_TIMEOUT;
    while i > 0 and not (trsf = '0') loop
      wait until rising_edge(rclk);
      i := i - 1;
    end loop;
    assert i > 0 report "Transfer timeout" severity warning;
    wait until rising_edge(rclk);
  end wait_for_trsf;

  -----------------------------------------------------------------------------
  -- purpose: Wait for 3 clock cycles
  procedure spacer (
    signal rclk : in std_logic) is      -- Clock
  begin
    wait until rising_edge(rclk);
    wait until rising_edge(rclk);
    wait until rising_edge(rclk);
  end spacer;

  -----------------------------------------------------------------------------
  -- purpose: read/write raw to the altro data bus.
  procedure bus_raw_rw(
    constant data            : in  std_logic_vector(19 downto 0);
    constant addr            : in  std_logic_vector(39 downto 20);
    constant rw              : in  std_logic;
    signal   bd              : out std_logic_vector(39 downto 0);
    signal   writ            : out std_logic;
    signal   cstb            : out std_logic;
    signal   rclk            : in  std_logic;
    signal   ackn            : in  std_logic) is
    constant pre_wait_time   :     time := 20 ns;
    constant cycle_wait_time :     time := 50 ns;
    constant stabilize_time  :     time := 50 ns;  -- Time to stabilize WRITE
  begin  -- ALTRO_BUS_SIM
    cstb             <= 'H';
    writ             <= 'H';
    bd(39 downto 20) <= addr;

    if rw = '0' then                    -- Write 
      bd(19 downto 0) <= data;
    else
      bd(19 downto 0) <= (others => 'Z');
    end if;

    wait until rising_edge(rclk);
    writ <= rw;

    wait for stabilize_time;
    wait until rising_edge(rclk);
    cstb <= '0';

    wait until rising_edge(rclk);
    wait_for_ack(rclk          => rclk,
                 ackn          => ackn);
    bd(19 downto 0) <= (others => 'Z');
    cstb            <= 'H';
    writ            <= 'H';
    wait until rising_edge(rclk);
  end bus_raw_rw;

  ----------------------------------------------------------------------------
  -- purpose: Send an instruction on the bus
  procedure bus_rw (
    constant rw      : in  character;   -- Read (R) or Write (W)
    constant bcast   : in  std_logic;
    constant branch  : in  std_logic;
    constant board   : in  std_logic_vector(3 downto 0);  -- Board no.
    constant chip    : in  std_logic_vector(2 downto 0);
    constant channel : in  std_logic_vector(3 downto 0);
    constant what    : in  bus_const_t;  -- Instruction
    constant data    : in  std_logic_vector(19 downto 0);  -- Data
    signal   bd      : out std_logic_vector(39 downto 0);  -- Addr on bus
    signal   writ    : out std_logic;   -- Write pulse
    signal   cstb    : out std_logic;   -- Control stobe
    signal   clk     : in  std_logic;   -- Clock
    signal   ackn    : in  std_logic)   -- Acknowledge
  is
    variable addr_i  :     std_logic_vector(39 downto 20) := (others => '0');
    variable cont_i  :     boolean                        := true;
    variable writ_i  :     std_logic                      := '1';
  begin  -- procedure abus_rw

    if what.code = 0 or what.name = "_______" then
      report "No such instruction " & integer'image(what.code) & "/" &
        what.name severity warning;
      cont_i := false;
    end if;

    if (bcast = '1' and what.bcast = '0') then
      report "Instruction " & what.name & " cannot be broadcasted"
        severity warning;
      cont_i := false;
    end if;

    if (rw = 'W' or rw = 'w') then
      if what.writ = '0' then
        report "Instruction " & what.name & " is read-only"
          severity warning;
        cont_i := false;
      end if;
      writ_i   := '0';
    elsif (rw = 'R' or rw = 'r') then
      if what.read = '0' then
        report "Instruction " & what.name & " is write-only"
          severity warning;
        cont_i := false;
      end if;
      writ_i   := '1';
    else
      report "Read/write was " & character'image(rw)
        & " must be 'R' or 'W'" severity warning;
      cont_i   := false;
    end if;

    if cont_i then
      addr_i(38)             := bcast;
      addr_i(37)             := what.bcal;
      addr_i(36)             := branch;
      addr_i(35 downto 32)   := board;
      if what.bcal = '1' then           -- BC instruction
        addr_i(31 downto 27) := (others => '0');
        addr_i(26 downto 20) := std_logic_vector(to_unsigned(what.code, 7));
      else                              -- ALTRO Instruction
        addr_i(31 downto 29) := chip;
        addr_i(28 downto 25) := channel;
        addr_i(24 downto 20) := std_logic_vector(to_unsigned(what.code, 5));
      end if;
      addr_i(39)             := parity(addr_i(38 downto 20));

      bus_raw_rw(data => data,
                 addr => addr_i,
                 rw   => writ_i,
                 bd   => bd,
                 writ => writ,
                 cstb => cstb,
                 ackn => ackn,
                 rclk => clk);
    end if;
  end procedure bus_rw;


  -- purpose: Send a named instruction
  procedure bus_send (
    constant name    : in  string(7 downto 1);             -- Instruction name
    constant rw      : in  character;                      -- Read/Writ
    constant bcast   : in  std_logic;                      -- Broadcast? 
    constant branch  : in  std_logic;                      -- Branch
    constant board   : in  std_logic_vector(3 downto 0);   -- Board #
    constant chip    : in  std_logic_vector(2 downto 0);   -- ALTRO #
    constant channel : in  std_logic_vector(3 downto 0);   -- Channel #
    constant data    : in  std_logic_vector(19 downto 0);  -- Data
    signal   bd      : out std_logic_vector(39 downto 0);  -- Bus
    signal   writ    : out std_logic;                      -- Write pulse
    signal   cstb    : out std_logic;                      -- Control strobe
    signal   clk     : in  std_logic;                      -- Clock
    signal   ackn    : in  std_logic) is                   -- Acknowledge
    variable i       :     integer     := 0;               -- Loop variable
    variable what_i  :     bus_const_t := bus_default;
  begin  -- procedure bus_send
    while bc_instructions(i) /= bus_default loop
      if bc_instructions(i).name = name then
        bus_rw(rw      => rw,
               bcast   => bcast,
               branch  => branch,
               board   => board,
               chip    => chip,
               channel => channel,
               what    => bc_instructions(i),
               data    => data,
               bd      => bd,
               writ    => writ,
               cstb    => cstb,
               clk     => clk,
               ackn    => ackn);
        exit;
      end if;
      i                                := i + 1;
    end loop;
    if bc_instructions(i) = bus_default then
      report "Unknown instruction " & name severity warning;
    end if;
  end procedure bus_send;


  -----------------------------------------------------------------------------
  -- purpose: Execute a BC commands
  procedure exec_command(
    constant hadd  : in    std_logic_vector(4 downto 0) := "00000";
    constant what  : in    bus_const_t;  -- Instruction
    constant bcast : in    std_logic;    -- Broad-cast
    signal   bd    : inout std_logic_vector(39 downto 0);
    signal   cstb  : out   std_logic;
    signal   writ  : out   std_logic;
    signal   ackn  : in    std_logic;
    signal   trsf  : in    std_logic;
    signal   rclk  : in    std_logic;
    signal   name  : out   string(7 downto 1))
  is
    variable data_i : std_logic_vector(19 downto 0);
  begin  -- exec_commands

    data_i := (others      => '0');
    if what.writ = '0' then
      report "Instruction " & what.name & " read-only"
        severity warning;
    else
      report "Executing " & what.name severity note;
      name   <= what.name;
      bus_rw(what          => what,
             bcast         => bcast,
             rw            => 'W',
             branch        => hadd(4),
             board         => hadd(3 downto 0),
             chip          => (others => 'Z'), -- (others => '-'),
             channel       => (others => 'Z'), -- (others => '-'),
             data          => data_i,
             bd            => bd,
             writ          => writ,
             cstb          => cstb,
             clk           => rclk,
             ackn          => ackn);
      name   <= (others    => ' ');
      wait_for_free(rclk   => rclk, ackn => ackn);
      if ((what.name = "EVLRDO_") or
          (what.name = "ACQRDO_")) then
        name <= "WAITTRF";
        wait_for_trsf(rclk => rclk, trsf => trsf, bd => bd);
        wait until rising_edge(rclk);
        if trsf = '0' then
          wait until not (trsf = '0');
        end if;
        name <= (others    => ' ');
      end if;
    end if;
  end exec_command;

  -----------------------------------------------------------------------------
  -- purpose: Write to all TPC registers
  procedure exec_command(
    constant hadd  : in    std_logic_vector(4 downto 0) := "00000";
    constant what  : in    string;      -- Instruction
    constant bcast : in    std_logic;   -- Broad-cast
    signal   bd    : inout std_logic_vector(39 downto 0);
    signal   cstb  : out   std_logic;
    signal   writ  : out   std_logic;
    signal   ackn  : in    std_logic;
    signal   trsf  : in    std_logic;
    signal   rclk  : in    std_logic;
    signal   name  : out   string(7 downto 1))
  is
    variable i : integer;
    variable s : string(7 downto 1);
  begin

    s := padstr(what);
    report "Looking for command '" & s & "' .." severity note;
    for i in 0 to 127 loop
      if bc_instructions(i).name = s then
        exec_command(hadd  => hadd,
                     bcast => bcast,
                     what  => bc_instructions(i),
                     bd    => bd,
                     cstb  => cstb,
                     writ  => writ,
                     ackn  => ackn,
                     trsf  => trsf,
                     rclk  => rclk,
                     name  => name);
        exit;
      end if;
    end loop;  -- i
    assert i < 127 report "Command '" & s & "' not found" severity warning;
  end exec_command;

  -----------------------------------------------------------------------------
  -- purpose: Execute all TPC BC commands
  procedure exec_commands(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1))
  is
    variable what_i  : bus_const_t;
    variable i       : integer   := 0;
  begin  -- exec_commands

    report "Executing TPC commands" severity note;
    for i in 22 to 31 loop
      what_i := bc_instructions(i);
      if ((what_i.name = "EVLRDO_") or
          (what_i.name = "ACQRDO_") or
          (what_i.name = "STTSM__") or
          (what_i.name = "SCEVL__") or
          (what_i.name = "STCNV__")) then
        report "Not executing " & what_i.name severity warning;
      else
        exec_command(hadd  => hadd,
                     bcast => '0',
                     what  => what_i,
                     bd    => bd,
                     cstb  => cstb,
                     writ  => writ,
                     ackn  => ackn,
                     trsf  => trsf,
                     rclk  => rclk,
                     name  => name);
        spacer(rclk => rclk);
      end if;
    end loop;
    report "Executing FMD commands" severity note;
    for i in 0 to 5 loop
      write_register(hadd => hadd,
                     what => bc_instructions(44),
                     data => 2**i,
                     bd   => bd,
                     cstb => cstb,
                     writ => writ,
                     ackn => ackn,
                     trsf => trsf,
                     rclk => rclk,
                     name => name);
    end loop;  -- i 
  end exec_commands;

  ---------------------------------------------------------------------------
  -- purpose: Read a BC registers
  procedure read_register(
    constant hadd   : in    std_logic_vector(4 downto 0) := "00000";
    constant what   : in    bus_const_t;  -- Instruction
    signal   bd     : inout std_logic_vector(39 downto 0);
    signal   cstb   : out   std_logic;
    signal   writ   : out   std_logic;
    signal   ackn   : in    std_logic;
    signal   trsf   : in    std_logic;
    signal   rclk   : in    std_logic;
    signal   name   : out   string(7 downto 1))
  is
    variable data_i :       std_logic_vector(19 downto 0);
    variable i      :       integer                      := 0;
  begin  -- read_register

    data_i := (others    => '-');
    if what.read = '0' then
      report "Instruction " & what.name & " write-only"
        severity warning;
    else
      report "Reading " & what.name severity note;
      name <= what.name;
      bus_rw(what        => what,
             bcast       => '0',
             rw          => 'R',
             branch      => hadd(4),
             board       => hadd(3 downto 0),
             chip        => (others => '-'),
             channel     => (others => '-'),
             data        => data_i,
             bd          => bd,
             cstb        => cstb,
             writ        => writ,
             ackn        => ackn,
             clk         => rclk);
      name <= (others    => ' ');
      wait_for_free(rclk => rclk, ackn => ackn);
    end if;
  end read_register;

  -----------------------------------------------------------------------------
  -- purpose: Read to all TPC registers
  procedure read_register(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    constant what : in    string;       -- Instruction
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1))
  is
    variable i    :       integer;      -- Counter
    variable s    :       string(7 downto 1);
  begin

    s := padstr(what);
    for i in 0 to 127 loop
      if bc_instructions(i).name = s then
        read_register(hadd => hadd,
                      what => bc_instructions(i),
                      bd   => bd,
                      cstb => cstb,
                      writ => writ,
                      ackn => ackn,
                      trsf => trsf,
                      rclk => rclk,
                      name => name);
        exit;
      end if;
    end loop;  -- i
  end read_register;

  ---------------------------------------------------------------------------
  -- purpose: Read all TPC BC registers
  procedure read_registers(
    constant hadd   : in    std_logic_vector(4 downto 0) := "00000";
    signal   bd     : inout std_logic_vector(39 downto 0);
    signal   cstb   : out   std_logic;
    signal   writ   : out   std_logic;
    signal   ackn   : in    std_logic;
    signal   trsf   : in    std_logic;
    signal   rclk   : in    std_logic;
    signal   name   : out   string(7 downto 1))
  is
    variable what_i :       bus_const_t;
    variable i      :       integer                      := 0;
  begin  -- read_registers

    -- read all cern bc registers
    report "Reading from TPC registers" severity note;
    for i in 0 to 19 loop
      what_i := bc_instructions(i);
      read_register(hadd => hadd,
                    what => what_i,
                    bd   => bd,
                    cstb => cstb,
                    writ => writ,
                    ackn => ackn,
                    trsf => trsf,
                    rclk => rclk,
                    name => name);
      spacer(rclk        => rclk);
    end loop;
    report "Reading from FMD registers" severity note;
    for i in 32 to 84 loop
      what_i := bc_instructions(i);
      read_register(hadd => hadd,
                    what => what_i,
                    bd   => bd,
                    cstb => cstb,
                    writ => writ,
                    ackn => ackn,
                    trsf => trsf,
                    rclk => rclk,
                    name => name);
      spacer(rclk        => rclk);
    end loop;
  end read_registers;

  -----------------------------------------------------------------------------
  -- purpose: Write to all TPC registers
  procedure write_register(
    constant hadd   : in    std_logic_vector(4 downto 0) := "00000";
    constant what   : in    bus_const_t;  -- Instruction
    constant data   : in    integer;      -- Data
    signal   bd     : inout std_logic_vector(39 downto 0);
    signal   cstb   : out   std_logic;
    signal   writ   : out   std_logic;
    signal   ackn   : in    std_logic;
    signal   trsf   : in    std_logic;
    signal   rclk   : in    std_logic;
    signal   name   : out   string(7 downto 1))
  is
    variable data_i :       std_logic_vector(19 downto 0);
  begin  -- tpc_write_registers

    -- write all cern bc registers
    if what.writ = '0' then
      report "Instruction " & what.name & " read-only"
        severity warning;
    else
      report "Writing " & what.name severity note;
      data_i := std_logic_vector(to_unsigned(data, 20));
      name <= what.name;
      bus_rw(what        => what,
             bcast       => '0',
             rw          => 'W',
             branch      => hadd(4),
             board       => hadd(3 downto 0),
             chip        => (others => '-'),
             channel     => (others => '-'),
             data        => data_i,
             bd          => bd,
             cstb        => cstb,
             writ        => writ,
             ackn        => ackn,
             clk         => rclk);
      name <= (others    => ' ');
      wait_for_free(rclk => rclk, ackn => ackn);
    end if;
  end write_register;

  -----------------------------------------------------------------------------
  -- purpose: Write to all TPC registers
  procedure write_register(
    constant hadd : in    std_logic_vector(4 downto 0) := "00000";
    constant what : in    string;       -- Instruction
    constant data : in    integer;      -- Data
    signal   bd   : inout std_logic_vector(39 downto 0);
    signal   cstb : out   std_logic;
    signal   writ : out   std_logic;
    signal   ackn : in    std_logic;
    signal   trsf : in    std_logic;
    signal   rclk : in    std_logic;
    signal   name : out   string(7 downto 1))
  is
    variable i    :       integer;
    variable s    :       string(7 downto 1);
    variable done :       boolean                      := false;
  begin


    s := padstr(what);
    for i in 0 to 127 loop
      if bc_instructions(i).name = s then
        write_register(hadd => hadd,
                       what => bc_instructions(i),
                       data => data,
                       bd   => bd,
                       cstb => cstb,
                       writ => writ,
                       ackn => ackn,
                       trsf => trsf,
                       rclk => rclk,
                       name => name);
        done := true;
        exit;
      end if;
    end loop;  -- i
    assert done report "Register " & what & " not found" severity warning;
  end write_register;

  -----------------------------------------------------------------------------
  -- purpose: Write to all TPC registers
  procedure write_registers(
    constant hadd   : in    std_logic_vector(4 downto 0) := "00000";
    signal   bd     : inout std_logic_vector(39 downto 0);
    signal   cstb   : out   std_logic;
    signal   writ   : out   std_logic;
    signal   ackn   : in    std_logic;
    signal   trsf   : in    std_logic;
    signal   rclk   : in    std_logic;
    signal   name   : out   string(7 downto 1))
  is
    variable what_i :       bus_const_t;
    variable i      :       integer                      := 0;
    variable data_i :       integer;
  begin  -- tpc_write_registers

    -- write all cern bc registers
    report "Writing to TPC registers" severity note;
    for i in 1 to 20 loop
      what_i     := bc_instructions(i);
      if what_i.writ = '0' then
        report "Instruction " & what_i.name & " read-only"
          severity warning;
      else
        if what_i.name = "CSR2___" then
          data_i := 16#F#;
        else
          data_i := i + 1;
        end if;
        report "Writing " & integer'image(data_i) &
          " to " & what_i.name severity note;
        write_register(hadd => hadd,
                       what => what_i,
                       data => data_i,
                       bd   => bd,
                       cstb => cstb,
                       writ => writ,
                       ackn => ackn,
                       trsf => trsf,
                       rclk => rclk,
                       name => name);
        spacer(rclk         => rclk);
      end if;
    end loop;
    -- write all cern bc registers
    report "Writing to FMDD registers" severity note;
    for i in 33 to 84 loop
      what_i     := bc_instructions(i);
      if what_i.writ = '0' then
        report "Instruction " & what_i.name & " read-only"
          severity warning;
      else
        if what_i.name = "SHIFTCL" then
          data_i := 16#0804#;
        elsif what_i.name = "SAMPLEC" then
          data_i := 16#0401#;
        else
          data_i := i + 1;
        end if;
        report "Writing " & integer'image(data_i) &
          " to " & what_i.name severity note;
        write_register(hadd => hadd,
                       what => what_i,
                       data => data_i,
                       bd   => bd,
                       cstb => cstb,
                       writ => writ,
                       ackn => ackn,
                       trsf => trsf,
                       rclk => rclk,
                       name => name);
        spacer(rclk         => rclk);
      end if;
    end loop;
  end write_registers;

  -----------------------------------------------------------------------------
  -- purpose: Make a trigger sequence
  procedure make_trig (
    constant l0_delay : in  time;       -- Delay of L0 after Bunch X'ing
    constant l1_delay : in  time;       -- Delay of L1 after Bunch X'ing
    constant l2_delay : in  time;       -- Delay of L2 after Bunch X'ing
    constant l0_len   : in  time;       -- Length of trigger signal
    constant l1_len   : in  time;       -- Length of trigger signal
    constant l2_len   : in  time;       -- Length of trigger signal
    signal   l0       : out std_logic;  -- Positive logic 
    signal   l1       : out std_logic;  -- Negative logic
    signal   l2       : out std_logic;  -- Negative logic 
    signal   name     : out string(7 downto 1))
  is
    variable l1_down : time := l1_delay-l0_delay;
    variable l1_up   : time := l1_delay-l0_delay+l1_len;
    variable l2_down : time := l2_delay-l0_delay;
    variable l2_up   : time := l2_delay-l0_delay+l2_len;
  begin  -- make_trigger
    assert l0_len > 0 ns report "Invalid L0 trigger length" severity error;
    assert l1_len > 0 ns report "Invalid L0 trigger length" severity error;
    assert l2_len > 0 ns report "Invalid L0 trigger length" severity error;
    assert l1_delay > 0 ns report "L1 is in the past" severity error;
    assert l0_len < l1_delay report "L1 time is before L0 is done"
      severity warning;
    assert l1_down < l2_down report "L2 time is before L1 time"
      severity warning;
    assert l1_up < l2_down report "L2 time is before L1 is done"
      severity warning;
    l0   <= '1', '0' after l0_len;      -- trig_len;
    l1   <= '1', '0' after l1_down, '1' after l1_up;
    l2   <= '1', '0' after l2_down, '1' after l2_up;
    name <= "L0_____",
            (others => ' ') after l0_len,
            "L1_____"       after l1_down,
            (others => ' ') after l1_up,
            "L2_____"       after l2_down,
            (others => ' ') after l2_up;
  end make_trig;

  -----------------------------------------------------------------------------
  -- purpose: Make a trigger sequence
  procedure make_trigger (
    signal l0   : out std_logic;           -- Positive logic 
    signal l1   : out std_logic;           -- Negative logic
    signal l2   : out std_logic;
    signal name : out string(7 downto 1))  -- Negative logic 
  is
  begin  -- make_trigger
    make_trig(l0_delay => L0_DELAY,        -- Delay of L0 after Bunch X'ing
              l1_delay => 6.5 us,          -- Delay of L1 after Bunch X'ing
              l2_delay => 88 us,           -- Delay of L2 after Bunch X'ing
              l0_len   => 25 ns,           -- Length of trigger signal
              l1_len   => 200 ns,          -- Length of trigger signal
              l2_len   => 50 ns,           -- Length of trigger signal
              l0       => l0,
              l1       => l1,
              l2       => l2,
              name     => name);
    wait for 1 us;
  end make_trigger;

  -----------------------------------------------------------------------------
  -- Send ALTRO command RPINC in broad-cast
  procedure rpinc (
    signal bd   : inout std_logic_vector(39 downto 0);
    signal cstb : out   std_logic;
    signal writ : out   std_logic;
    signal ackn : in    std_logic;
    signal trsf : in    std_logic;
    signal rclk : in    std_logic;
    signal name : out   string(7 downto 1))
  is
  begin
    exec_command(what  => "RPINC__",
                 bcast => '1',
                 bd    => bd,
                 cstb  => cstb,
                 writ  => writ,
                 ackn  => ackn,
                 trsf  => trsf,
                 rclk  => rclk,
                 name  => name);
  end rpinc;
    
end package body rcu_misc_pack;

------------------------------------------------------------------------------
--
-- EOF
--
