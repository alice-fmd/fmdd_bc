------------------------------------------------------------------------------
-- Title      : Architecture that makes triggers
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu_trig.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-19
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/10/02  1.0      cholm   Created
------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

-------------------------------------------------------------------------------
architecture busy_stim of rcu is
  signal rclk_i : std_logic;
  signal sclk_i : std_logic;
  signal writ_i : std_logic          := 'H';
  signal grst_i : std_logic;
  signal addr_i : std_logic_vector(39 downto 20);  -- Debug
  signal data_i : std_logic_vector(19 downto 0);   -- Debug
  signal what_i : string(7 downto 1) := (others => ' ');

  constant TRIG_TIME  : time := 8 us;
  constant TRIG2_TIME : time := 11 us;
  constant RPINC_TIME : time := 3 us;

begin  -- architecture stimulus
  connectivity : block is
  begin  -- block  connectivity
    rclk      <= rclk_i;
    sclk      <= sclk_i;
    writ      <= writ_i;
    addr_i    <= bd(39 downto 20);
    data_i    <= bd(19 downto 0);
    grst      <= grst_i;
    scl_a     <= 'H';
    scl_b     <= 'H';
    sda_out_a <= 'H';
    sda_out_b <= 'H';
  end block connectivity;

  -- Reset
  reset_it : entity rcu_model.reseter port map (rstb => grst_i);

  -- 40 Mhz
  rclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 40000000)
    port map (clk       => rclk_i);

  -- 10 MHz  
  sclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 10000000)
    port map (clk       => sclk_i);

  stim : process is
    variable last : time := NOW;
  begin  -- process stim
    bd     <= (others => '0');
    writ_i <= 'H';
    cstb   <= 'H';
    lvl0   <= '0';
    lvl1   <= '1';
    lvl2   <= '1';
    wait until grst_i = '1';
    wait until rising_edge(rclk_i);
    wait for 100 ns;
    write_register(hadd => HADD,
                   what => "STRIPS_",
                   data => 256,         -- 16#0200#,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);

    write_register(hadd => HADD,
                   what => "HOLD_WA",
                   data => 16,            -- 16#0002#,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);
    write_register(hadd => HADD,
                   what => "L2_TIMO",
                   data => 8 us / 25 ns,  -- 16#0002#,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);
    write_register(hadd => HADD,
                   what => "MEBS___",
                   data => 16#0140#,      -- 16#0002#,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);

    
    report "Making 4 events" severity note;
    for i in 1 to 4 loop
      wait for i * TRIG_TIME - NOW;
      make_trig(l0_delay => L0_DELAY,
                l1_delay => 2 us,
                l2_delay => 6 us,
                l0_len   => 25 ns,
                l1_len   => 200 ns,
                l2_len   => 50 ns,
                l0       => lvl0,
                l1       => lvl1,
                l2       => lvl2,
                name     => what_i);
    end loop;  -- i
    last := NOW;

    report "Poping the last 4 events" severity note;
    for i in 1 to 4 loop
      wait for last + i * RPINC_TIME - NOW;
      rpinc(bd   => bd,
            cstb => cstb,
            writ => writ_i,
            ackn => ackn,
            trsf => trsf,
            rclk => rclk_i,
            name => what_i);
    end loop;
    last := NOW;


    for i in 1 to 2 loop
      report "Making 2 events" severity note;
      wait for last + i * TRIG_TIME - NOW;
      make_trig(l0_delay => L0_DELAY,
                l1_delay => 2 us,
                l2_delay => 6 us,
                l0_len   => 25 ns,
                l1_len   => 200 ns,
                l2_len   => 50 ns,
                l0       => lvl0,
                l1       => lvl1,
                l2       => lvl2,
                name     => what_i);
    end loop;  -- i
    last := NOW;

    report "Poping the last 2 events" severity note;
    for i in 1 to 2 loop
      wait for last + i * RPINC_TIME - NOW;
      rpinc(bd   => bd,
            cstb => cstb,
            writ => writ_i,
            ackn => ackn,
            trsf => trsf,
            rclk => rclk_i,
            name => what_i);
    end loop;
    last := NOW;

    report "Making 3 triggers to almost fill the buffers" severity note;
    for i in 1 to 3 loop
      wait for last + i * TRIG_TIME - NOW;
      make_trig(l0_delay => L0_DELAY,
                l1_delay => 2 us,
                l2_delay => 6 us,
                l0_len   => 25 ns,
                l1_len   => 200 ns,
                l2_len   => 50 ns,
                l0       => lvl0,
                l1       => lvl1,
                l2       => lvl2,
                name     => what_i);
    end loop;  -- i
    last := NOW;

    report "Make one event with l2 timeout" severity note;
    wait for last + 1 * TRIG2_TIME - NOW;
    make_trig(l0_delay => L0_DELAY,
              l1_delay => 2 us,
              l2_delay => 9 us,
              l0_len   => 25 ns,
              l1_len   => 200 ns,
              l2_len   => 50 ns,
              l0       => lvl0,
              l1       => lvl1,
              l2       => lvl2,
              name     => what_i);
    last := NOW;

    report "Poping the last 3 events" severity note;
    for i in 1 to 3 loop
      wait for last + i * RPINC_TIME - NOW;
      rpinc(bd   => bd,
            cstb => cstb,
            writ => writ_i,
            ackn => ackn,
            trsf => trsf,
            rclk => rclk_i,
            name => what_i);
    end loop;
    last := NOW;

    report "Poping the last 2 events" severity note;
    for i in 1 to 2 loop
      wait for last + i * RPINC_TIME - NOW;
      rpinc(bd   => bd,
            cstb => cstb,
            writ => writ_i,
            ackn => ackn,
            trsf => trsf,
            rclk => rclk_i,
            name => what_i);
    end loop;
    last := NOW;

    wait;
  end process stim;
end architecture busy_stim;
------------------------------------------------------------------------------
-- 
-- EOF
--
