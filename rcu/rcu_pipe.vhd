-----------------------------------------------------------------------------
-- Title      : Pipe-line architecture of RCU.  Allows com with control GUI
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu_pipe.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-19
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/10/02  1.0      cholm	Created
------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

------------------------------------------------------------------------------
architecture pipe_stim of rcu is
  constant PERIOD : time      := 25 ns;
  constant SIZE   : integer   := 8;
  signal   clk_i  : std_logic := '1';
  signal   sclk_i : std_logic := '1';
  signal   scl_i  : std_logic := '1';
  signal   grst_i : std_logic := '0';
  signal   l0_i   : std_logic;
  signal   l1_i   : std_logic;
  signal   l2_i   : std_logic;
  signal   l1_ii  : std_logic;
  signal   l2_ii  : std_logic;
  signal   done_i : boolean   := false;

  ----------------------------------------------------------------------------
  type wr_state_t is (idle, got_address, got_data);
  type rd_state_t is (idle, got_address, start, output, endit);
  type ctrl_t is (idle, start, wait_for_write, wait_for_read);
  type exec_st_t is (idle, start, next_instr, rcu_instr, wait_clk,
                     altro_instr, altro_instr2, set_bus, strobe,
                     wait_strobe, wait_ack, take_data);
  type tr_st_t is (idle, gen_l0, wait_to_l1, gen_l1, wait_to_l2, gen_l2);
  signal trg_st_i     : tr_st_t;
  signal exec_st_i    : exec_st_t;
  signal wr_st_i      : wr_state_t;
  signal rd_st_i      : rd_state_t;
  signal ctrl_st_i    : ctrl_t;
  signal enable_rd_i  : std_logic := '0';
  signal enable_wr_i  : std_logic := '0';
  signal finish_rd_i  : std_logic := '0';
  signal finish_wr_i  : std_logic := '0';
  signal enable_out_i : std_logic := '0';
  signal timeout_i    : std_logic := '0';

  ----------------------------------------------------------------------------
  signal ioaddr_i : std_logic_vector(19 downto 0) := (others => '-');
  signal idata_i  : std_logic_vector(31 downto 0) := (others => '-');

  ----------------------------------------------------------------------------
  constant add_errst    : integer                       := 16#7800#;
  constant add_trcfg    : integer                       := 16#7801#;
  constant add_trcnt    : integer                       := 16#7802#;
  constant add_lwadd    : integer                       := 16#7803#;
  constant add_iradd    : integer                       := 16#7804#;
  constant add_irdat    : integer                       := 16#7805#;
  constant add_pmcfg    : integer                       := 16#7806#;
  constant add_chadd    : integer                       := 16#7807#;
  constant add_afl      : integer                       := 16#8000#;
  signal   errst_i      : std_logic_vector(31 downto 0) := (others => '0');
  signal   trcfg_i      : std_logic_vector(31 downto 0) := (others => '0');
  signal   trcnt_i      : std_logic_vector(31 downto 0) := (others => '0');
  signal   lwadd_i      : std_logic_vector(17 downto 0) := (others => '0');
  signal   iradd_i      : std_logic_vector(19 downto 0) := (others => '0');
  signal   irdat_i      : std_logic_vector(19 downto 0) := (others => '0');
  signal   pmcfg_i      : std_logic_vector(19 downto 0) := (others => '0');
  signal   chadd_i      : std_logic_vector(23 downto 0) := (others => '0');
  signal   afl_i        : std_logic_vector(31 downto 0) := (others => '0');
  signal   load_trcfg_i : std_logic                     := '0';
  signal   load_pmcfg_i : std_logic                     := '0';
  signal   load_afl_i   : std_logic                     := '0';

  ----------------------------------------------------------------------------
  constant add_rs_status   : integer   := 16#6C01#;
  constant add_rs_trcfg    : integer   := 16#6C02#;
  constant add_rs_trcnt    : integer   := 16#6C03#;
  constant add_rs_buf1     : integer   := 16#6C04#;
  constant add_rs_buf2     : integer   := 16#6C05#;
  constant add_exec        : integer   := 16#0000#;
  constant add_abort       : integer   := 16#0800#;
  constant add_dcs_on      : integer   := 16#E000#;
  constant add_dll_on      : integer   := 16#F000#;
  constant add_l1_ttc      : integer   := 16#E800#;
  constant add_l1_i2c      : integer   := 16#F800#;
  constant add_l1_cmd      : integer   := 16#D800#;
  constant add_l1          : integer   := 16#D000#;
  constant add_glb_reset   : integer   := 16#2000#;
  constant add_fec_reset   : integer   := 16#2001#;
  constant add_rcu_reset   : integer   := 16#2002#;
  signal   exec_addr_i     : integer   := 0;
  signal   cmd_rs_status_i : std_logic := '0';
  signal   cmd_rs_trcfg_i  : std_logic := '0';
  signal   cmd_rs_trcnt_i  : std_logic := '0';
  signal   cmd_rs_buf1_i   : std_logic := '0';
  signal   cmd_rs_buf2_i   : std_logic := '0';
  signal   cmd_exec_i      : std_logic := '0';
  signal   cmd_abort_i     : std_logic := '0';
  signal   exec_busy_i     : std_logic := '0';
  signal   cmd_dcs_on_i    : std_logic := '0';
  signal   cmd_dll_on_i    : std_logic := '0';
  signal   cmd_l1_ttc_i    : std_logic := '0';
  signal   cmd_l1_i2c_i    : std_logic := '0';
  signal   cmd_l1_cmd_i    : std_logic := '0';
  signal   cmd_l1_i        : std_logic := '0';
  signal   cmd_glb_reset_i : std_logic := '0';
  signal   cmd_fec_reset_i : std_logic := '0';
  signal   cmd_rcu_reset_i : std_logic := '0';

  ----------------------------------------------------------------------------
  signal   icmd_rs_trcfg_i  : std_logic := '0';
  signal   icmd_rs_status_i : std_logic := '0';
  signal   icmd_rs_trcnt_i  : std_logic := '0';
  signal   icmd_trigger_i   : std_logic := '0';

  ----------------------------------------------------------------------------
  constant add_imem : integer := 16#7000#;
  constant add_pmem : integer := 16#6800#;
  constant add_rmem : integer := 16#6000#;
  constant add_acl  : integer := 16#6400#;
  constant add_dm1l : integer := 16#7400#;
  constant add_dm1h : integer := 16#7500#;
  constant add_dm2l : integer := 16#7C00#;
  constant add_dm2h : integer := 16#7D00#;
  constant add_head : integer := 16#4001#;
  constant sz_imem  : integer := 256;
  constant sz_pmem  : integer := 1024;
  constant sz_rmem  : integer := 128;
  constant sz_acl   : integer := 256;
  constant sz_dmem  : integer := 256;
  constant sz_head  : integer := 8;
  type imem_t is array (0 to sz_imem-1) of std_logic_vector(23 downto 0);
  type pmem_t is array (0 to sz_pmem-1) of std_logic_vector(9 downto 0);
  type rmem_t is array (0 to sz_rmem-1) of std_logic_vector(19 downto 0);
  type acl_t is array (0 to sz_acl-1) of std_logic_vector(15 downto 0);
  type dmem_t is array (0 to sz_dmem-1) of std_logic_vector(19 downto 0);
  type head_t is array (0 to sz_head-1) of std_logic_vector(31 downto 0);
  signal   imem_i   : imem_t  := (others => (others => '0'));
  signal   pmem_i   : pmem_t  := (others => (others => '0'));
  signal   rmem_i   : rmem_t  := (others => (others => '0'));
  signal   acl_i    : acl_t   := (others => (others => '0'));
  signal   dm1l_i   : dmem_t  := (others => (others => '0'));
  signal   dm1h_i   : dmem_t  := (others => (others => '0'));
  signal   dm2l_i   : dmem_t  := (others => (others => '0'));
  signal   dm2h_i   : dmem_t  := (others => (others => '0'));
  signal   head_i   : head_t  := (others => (others => '0'));

  ----------------------------------------------------------------------------
  -- purpose: Read leading character
  procedure read_what (
    file input      :     text;
    variable what   : out character)
  is
    variable line_i :     line;
    variable good_i :     boolean;
  begin  -- function read_what
    what   := ' ';
    readline(input, line_i);
    if not endfile(input) then
      read(line_i, what, good_i);
    end if;
    if not good_i then
      what := ' ';
    end if;
  end procedure read_what;

  ----------------------------------------------------------------------------
  -- purpose: Read a real line
  procedure read_addr_data (
    file input      :     text;            -- File
    variable what   : out character;       -- What to do
    variable addr   : out string(1 to 7);  -- Address
    variable data   : out string(1 to 10))
  is
    variable line_i :     line;
    variable good_i :     boolean;
    variable blnk_i :     character       := ' ';
    variable what_i :     character       := ' ';
    variable addr_i :     string(1 to 7)  := (others => ' ');
    variable data_i :     string(1 to 10) := (others => ' ');
  begin  -- procedure read_addr_data
    what                            := ' ';
    addr                            := (others => ' ');
    data                            := (others => ' ');
    readline(input, line_i);
    if not endfile(input) then
      read(line_i, what_i, good_i);
      what := what_i;
      if good_i and what_i /= 'e' then
        read(line_i, blnk_i, good_i);
        read(line_i, addr_i, good_i);
        addr := addr_i;
        if good_i then
          read(line_i, blnk_i, good_i);
          read(line_i, data_i, good_i);
          data := data_i;
        end if;
      end if;
    end if;
    if not good_i then
      what                          := ' ';
      addr                          := (others => ' ');
      data                          := (others => ' ');
    end if;
  end procedure read_addr_data;

  ----------------------------------------------------------------------------
  -- purpose: Convert std_logic_vector to a string
  function slv2str (
    constant v   : std_logic_vector)
    return string
  is
    variable ret : string(v'length downto 1);
    variable i   : integer;
  begin  -- function slv2str
    ret := (others => 'X');

    for i in v'range loop
      case v(i) is
        when 'U'    => ret(i+1) := 'U';
        when 'X'    => ret(i+1) := 'X';
        when '0'    => ret(i+1) := '0';
        when '1'    => ret(i+1) := '1';
        when 'Z'    => ret(i+1) := 'Z';
        when 'W'    => ret(i+1) := 'W';
        when 'L'    => ret(i+1) := 'L';
        when 'H'    => ret(i+1) := 'H';
        when '-'    => ret(i+1) := '-';
        when others => ret(i+1) := 'X';
      end case;
    end loop;  -- i
    return ret;
  end function slv2str;
  
  ----------------------------------------------------------------------------
  -- purpose: Convert hex character to std_logic_vector (quad)
  function hex2quad (
    constant x   : character)
    return std_logic_vector
  is
    variable ret : std_logic_vector(0 to 3);
  begin  -- function hex2quad
    ret                                     := (others => '0');
    case x is
      when '0'                       => ret := X"0";
      when '1'                       => ret := X"1";
      when '2'                       => ret := X"2";
      when '3'                       => ret := X"3";
      when '4'                       => ret := X"4";
      when '5'                       => ret := X"5";
      when '6'                       => ret := X"6";
      when '7'                       => ret := X"7";
      when '8'                       => ret := X"8";
      when '9'                       => ret := X"9";
      when 'A'|'a'                   => ret := X"A";
      when 'B'|'b'                   => ret := X"B";
      when 'C'|'c'                   => ret := X"C";
      when 'D'|'d'                   => ret := X"D";
      when 'E'|'e'                   => ret := X"E";
      when 'F'|'f'                   => ret := X"F";
      when others                    =>
        report "Bad hex character: '" & x & "'" severity warning;
    end case;
    return ret;
  end function hex2quad;

  -- purpose Convert a string (with hex numbers) to a std_logic_vector
  function hstr2slv (
    constant x   : string)
    return std_logic_vector
  is
    variable j   : integer;
    variable ns  : integer    := x'length - 3;
    variable ret : std_logic_vector(0 to 4 * ns + 4 - 1);
  begin
    ret                       := (others => '0');
    assert x(1) = '0' and (x(2) = 'x' or x(2) = 'X')
      report "Bad hex prefix on: '" & x & "'" severity warning;
    for i in 0 to ns loop
      j := 4 * i;
      ret(j to j + 3) := hex2quad(x(i+3));
    end loop;  -- i
    return ret;
  end function hstr2slv;

  ----------------------------------------------------------------------------
  -- purpose: Convert a quad of std_logic to a character
  function quad2hex (
    constant q   : std_logic_vector(0 to 3))
    return character
  is
    variable ret : character  := '0';
    variable x   : integer    := 0;
  begin  -- function quad2str
    for i in q'range loop
      case q(i) is
        when 'H' | '1' => x   := x + 2**(3-i);
        when 'L' | '0' => null;
        when others    =>
          report "Unrepresentable bit value " & std_logic'image(q(i)) &
            " in " & slv2str(q) severity warning;
      end case;
    end loop;  -- i
    case x is
      when 16#0#       => ret := '0';
      when 16#1#       => ret := '1';
      when 16#2#       => ret := '2';
      when 16#3#       => ret := '3';
      when 16#4#       => ret := '4';
      when 16#5#       => ret := '5';
      when 16#6#       => ret := '6';
      when 16#7#       => ret := '7';
      when 16#8#       => ret := '8';
      when 16#9#       => ret := '9';
      when 16#A#       => ret := 'A';
      when 16#B#       => ret := 'B';
      when 16#C#       => ret := 'C';
      when 16#D#       => ret := 'D';
      when 16#E#       => ret := 'E';
      when 16#F#       => ret := 'F';
      when others      => null;
    end case;
    return ret;
  end function quad2hex;

  -- purpose: Turn a std_logic_vector into a string
  function slv2hstr (
    constant v   : std_logic_vector)
    return string
  is
    variable ns  : natural := v'length / 4;
    variable ret : string(1 to 2 + ns);
    variable j   : integer;
    variable q   : std_logic_vector(0 to 3);
  begin  -- function slv2hstr
    assert v'length mod 4 = 0 report
      "Vector length is not a multiplum of 4" severity warning;
    ret                    := (others => '0');
    ret(2)                 := 'x';
    for i in 0 to ns - 1 loop
      j                    := 4 * i;
      q                    := v(j+3 downto j);
      ret(ret'length - i)  := quad2hex(q);
    end loop;  -- i
    return ret;
  end function slv2hstr;

  ----------------------------------------------------------------------------
  -- purpose: Turn a std_logic_vector into an integer
  function slv2int (
    constant x : std_logic_vector)
    return integer is
  begin  -- function slv2int
    return to_integer(unsigned(x));
  end function slv2int;

  -- purpose: Turn an integer into a std_logic_vector
  function int2slv (
    constant x : integer;
    constant l : natural)
    return std_logic_vector
  is
    variable ret : std_logic_vector(l - 1 downto 0) := (others => '0');
  begin
    ret := std_logic_vector(to_unsigned(x, l));
    return ret;
  end function int2slv;

  ---------------------------------------------------------------------------
  -- purpose: calculate parity
  function parity (constant x   : std_logic_vector) return std_logic is
    variable                tmp : std_logic := '0';
  begin  -- function parity
    for j in x'range loop
      tmp                                   := tmp xor x(j);
    end loop;  -- j
    return tmp;
  end function parity;
begin  -- architecture pipe_stim

  -- purpose: Combinatorics
  combi : block is
  begin  -- block combi
    lvl0      <= l0_i;
    lvl1      <= l1_i;
    lvl2      <= l2_i;
    rclk      <= clk_i;
    sclk      <= sclk_i;
    -- grst <= grst_i;
    scl_a     <= 'H';
    scl_b     <= 'H';
    sda_out_a <= 'H';
    sda_out_b <= 'H';
  end block combi;

  -- purpose: Provide stimuli
  stimuli : process is
  begin  -- process stimuli
    -- wait for 100 ns;
    -- grst_i <= '1';
    wait;
  end process stimuli;

  -- purpose: Read one line
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  control           : process(clk_i, grst_i)
    file input_i    : text open read_mode is "/dev/stdin";  -- Input pipe
    variable what_i : character;
    variable addr_i : string(1 to 7);
    variable data_i : string(1 to 10);
  begin  -- process read_in

    if grst_i = '0' then
      ctrl_st_i <= idle;
      done_i    <= false;

    elsif clk_i'event and clk_i = '1' then
      enable_wr_i <= '0';
      enable_rd_i <= '0';
      -- idata_i     <= (others => '0');
      -- ioaddr_i    <= (others => '0');
      ctrl_st_i   <= ctrl_st_i;

      case ctrl_st_i is
        when idle         =>
          if not done_i then
            read_what(input_i, what_i);
            case what_i is
              when ' '    =>
                report "At end of file in state " &
                  ctrl_t'image(ctrl_st_i) severity error;
                done_i    <= true;
              when 's'    =>            -- Start condition
                report "Got start" severity note;
                ctrl_st_i <= start;
              when others =>
                report "Unexpected control character '" & what_i &
                  "' while in state " & ctrl_t'image(ctrl_st_i)
                  severity warning;
            end case;
          end if;

        when start   =>
          read_addr_data(input_i, what_i, addr_i, data_i);
          case what_i is
            when 'w' =>
              -- report "Writing" severity note;
              ioaddr_i    <= hstr2slv(addr_i);
              idata_i     <= hstr2slv(data_i);
              enable_wr_i <= '1';
              enable_rd_i <= '0';
              ctrl_st_i   <= wait_for_write;

            when 'r' =>
              -- report "Reading" severity note;
              ioaddr_i    <= hstr2slv(addr_i);
              idata_i     <= hstr2slv(data_i);
              enable_wr_i <= '0';
              enable_rd_i <= '1';
              ctrl_st_i   <= wait_for_read;

            when 'e'                 =>
              report "Got end" severity note;
              enable_wr_i <= '0';
              enable_rd_i <= '0';
              ctrl_st_i   <= idle;

            when ' ' =>
              report "at end of file in state " & ctrl_t'image(ctrl_st_i)
                severity error;
              done_i <= true;
              ctrl_st_i <= idle;
              
            when others              =>
              report "Unexpected control character '" & what_i &
                "' while in state " & ctrl_t'image(ctrl_st_i)
                severity warning;
              ctrl_st_i <= idle;
          end case;

        when wait_for_write =>
          if finish_wr_i = '1' then
            ctrl_st_i <= start;
          end if;

        when wait_for_read =>
          if finish_rd_i = '1' then
            ctrl_st_i <= start;
          end if;
          
        when others =>
          ctrl_st_i <= idle;
          
      end case;
    end if;
  end process control;

  -- purpose: Write to registers
  -- type   : sequential
  -- inputs : clk_i, grst_i
  -- outputs: 
  writer            : process (clk_i, grst_i)
    variable addr_i : integer := 0;
  begin  -- process writer
    if grst_i = '0' then                    -- asynchronous reset (active low)
      wr_st_i         <= idle;
      finish_wr_i     <= '0';
      cmd_l1_ttc_i    <= '0';
      cmd_l1_i2c_i    <= '0';
      cmd_l1_cmd_i    <= '1';
      cmd_rs_status_i <= '0';
      cmd_rs_trcfg_i  <= '0';
      cmd_rs_trcnt_i  <= '0';
      cmd_rs_buf1_i   <= '0';
      cmd_rs_buf2_i   <= '0';
      cmd_exec_i      <= '0';
      cmd_abort_i     <= '0';
      cmd_dcs_on_i    <= '0';
      cmd_dll_on_i    <= '0';
      cmd_l1_i        <= '0';
      cmd_glb_reset_i <= '0';
      cmd_fec_reset_i <= '0';
      cmd_rcu_reset_i <= '0';
      load_trcfg_i    <= '0';
      load_pmcfg_i    <= '0';
      load_afl_i      <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      wr_st_i         <= wr_st_i;
      finish_wr_i     <= '0';

      case wr_st_i is
        when idle =>
          if enable_wr_i = '1' then
            wr_st_i <= got_address;
          end if;

        when got_address =>
          addr_i := slv2int(ioaddr_i);
          wr_st_i <= got_data;

          -- report "Writing to " & slv2hstr(ioaddr_i) &
          --  ": " & slv2hstr(idata_i) severity note;
          case addr_i is
            when add_errst =>           -- errst
              report "Register errst is read-only" severity warning;
            when add_trcfg =>           -- trcfg
              load_trcfg_i <= '1';
            when add_trcnt =>           -- trcnt
              report "Register trcnt is read-only" severity warning;
            when add_lwadd =>           -- lwadd
              report "Register lwadd is read-only" severity warning;
            when add_iradd =>           -- iradd
              report "Register iradd is read-only" severity warning;
            when add_irdat =>           -- irdat
              report "Register irdat is read-only" severity warning;
            when add_pmcfg =>           -- pmcfg
              load_pmcfg_i <= '1';
            when add_chadd =>           -- chadd
              report "Register chadd is read-only" severity warning;
            when add_afl =>
              load_afl_i <= '1';

            when add_rs_status              =>  -- rs_status
              cmd_rs_status_i <= '1';
            when add_rs_trcfg               =>  -- rs_trcfg
              cmd_rs_trcfg_i  <= '1';
            when add_rs_trcnt               =>  -- rs_trcnt
              cmd_rs_trcnt_i  <= '1';
            when add_rs_buf1                =>  -- rs_buf1
              cmd_rs_buf1_i   <= '1';
            when add_rs_buf2                =>  -- rs_buf2
              cmd_rs_buf2_i   <= '1';
            when add_exec to add_exec + 127 =>  -- exec
              cmd_exec_i      <= '1';
              exec_addr_i     <= slv2int(ioaddr_i(7 downto 0));
            when add_abort                  =>  -- abort
              cmd_abort_i     <= '1';
            when add_dcs_on                 =>  -- dcs_on
              cmd_dcs_on_i    <= '1';
            when add_dll_on                 =>  -- dll_on
              cmd_dll_on_i    <= '1';
            when add_l1_ttc                 =>  -- l1_ttc
              cmd_l1_ttc_i    <= '1';
              cmd_l1_i2c_i    <= '0';
              cmd_l1_cmd_i    <= '0';
            when add_l1_i2c                 =>  -- l1_i2c
              cmd_l1_ttc_i    <= '0';
              cmd_l1_i2c_i    <= '1';
              cmd_l1_cmd_i    <= '0';
            when add_l1_cmd                 =>  -- l1_cmd
              cmd_l1_ttc_i    <= '0';
              cmd_l1_i2c_i    <= '0';
              cmd_l1_cmd_i    <= '1';
            when add_l1                     =>  -- l1
              cmd_l1_i        <= '1';
            when add_glb_reset              =>  -- glb_reset
              cmd_glb_reset_i <= '1';
            when add_fec_reset              =>  -- fec_reset
              cmd_fec_reset_i <= '1';
            when add_rcu_reset              =>  -- rcu_reset
              cmd_rcu_reset_i <= '1';

            when add_imem to add_imem+sz_imem-1 =>  -- IMEM
              imem_i(addr_i - add_imem) <= idata_i(23 downto 0);
            when add_pmem to add_pmem+sz_pmem-1 =>  -- PMEM
              pmem_i(addr_i - add_pmem) <= idata_i(9 downto 0);
            when add_rmem to add_rmem+sz_rmem-1 =>  -- RMEM
              null;
              -- rmem_i(addr_i - add_rmem) <= idata_i(19 downto 0);
            when add_acl to add_acl+sz_acl =>  -- ACL
              acl_i(addr_i - add_acl) <= idata_i(15 downto 0);
            when add_dm1l to add_dm1l+sz_dmem-1 =>  -- DM1L
              dm1l_i(addr_i - add_dm1l) <= idata_i(19 downto 0);
            when add_dm1h to add_dm1h+sz_dmem-1 =>  -- DM1H
              dm1h_i(addr_i - add_dm1h) <= idata_i(19 downto 0);
            when add_dm2l to add_dm2l+sz_dmem-1 =>  -- DM2L
              dm2l_i(addr_i - add_dm2l) <= idata_i(19 downto 0);
            when add_dm2h to add_dm2h+sz_dmem-1 =>  -- DM2H
              dm2h_i(addr_i - add_dm2h) <= idata_i(19 downto 0);
            when add_head to add_head+sz_head-1 =>  -- HEAD
              head_i(addr_i - add_head) <= idata_i(31 downto 0);

            when others =>
              report "Invalid address " & integer'image(addr_i)
                severity warning;
          end case;

        when got_data =>
          cmd_rs_status_i <= '0';
          cmd_rs_trcfg_i  <= '0';
          cmd_rs_trcnt_i  <= '0';
          cmd_rs_buf1_i   <= '0';
          cmd_rs_buf2_i   <= '0';
          cmd_exec_i      <= '0';
          cmd_abort_i     <= '0';
          cmd_dcs_on_i    <= '0';
          cmd_dll_on_i    <= '0';
          cmd_l1_i        <= '0';
          cmd_glb_reset_i <= '0';
          cmd_fec_reset_i <= '0';
          cmd_rcu_reset_i <= '0';
          load_trcfg_i    <= '0';
          load_pmcfg_i    <= '0';
          load_afl_i      <= '0';
          if cmd_exec_i = '1' or exec_busy_i = '1' then
            wr_st_i       <= got_data;
          else
            finish_wr_i   <= '1';
            exec_addr_i   <= 0;
            wr_st_i       <= idle;
          end if;

        when others => 
          wr_st_i <= idle;

      end case;
    end if;
  end process writer;

  -- purpose: Read from registers
  -- type   : sequential
  -- inputs : clk_i, grst_i
  -- outputs: 
  reader              : process (clk_i, grst_i)
    file output_i     : text open write_mode is "/dev/stdout";  -- Input pipe
    variable str_i    : string(1 to 10) := (others => ' ');
    variable line_i   : line;
    variable data_i   : std_logic_vector(31 downto 0);
    variable addr_i   : integer         := 0;
    variable cnt_i    : integer         := 0;  -- Count
    variable cur_i    : integer         := 0;
    variable en_out_i : boolean         := false;
  begin  -- process reader

    if grst_i = '0' then                    -- asynchronous reset (active low)
      rd_st_i      <= idle;
      finish_rd_i  <= '0';
      cnt_i  := 0;
      cur_i  := 0;
      data_i := (others => '0');
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      rd_st_i      <= rd_st_i;
      finish_rd_i  <= '0';
      enable_out_i <= '0';

      case rd_st_i is
        when idle             =>
          cnt_i    := 0;
          cur_i    := 0;
          data_i   := (others => '0');
          en_out_i := true;
          if enable_rd_i = '1' then
            rd_st_i <= got_address;
          end if;

        when got_address =>
          addr_i   := slv2int(ioaddr_i);
          cur_i    := addr_i;
          cnt_i    := slv2int(idata_i);

          case addr_i is
            when add_rs_status => -- rs_status
              report "rs_status is a command" severity warning;
            when add_rs_trcfg => -- rs_trcfg
              report "rs_trcfg is a command" severity warning;
            when add_rs_trcnt => -- rs_trcnt
              report "rs_trcnt is a command" severity warning;
            when add_rs_buf1 => -- rs_buf1
              report "rs_buf1 is a command" severity warning;
            when add_rs_buf2 => -- rs_buf2
              report "rs_buf2 is a command" severity warning;
            when add_exec to add_exec + 127 => -- exec
              report "exec is a command" severity warning;
            when add_abort => -- abort
              report "abort is a command" severity warning;
            when add_dcs_on                 =>  -- dcs_on
              report "dcs_on is a command" severity warning;
            when add_dll_on                 =>  -- dll_on
              report "dll_on is a command" severity warning;
            when add_l1_ttc                 =>  -- l1_ttc
              report "l1_ttc is a command" severity warning;
            when add_l1_i2c                 =>  -- l1_i2c
              report "l1_i2c is a command" severity warning;
            when add_l1_cmd                 =>  -- l1_cmd
              report "l1_cmd is a command" severity warning;
            when add_l1                     =>  -- l1
              report "l1 is a command" severity warning;
            when add_glb_reset              =>  -- glb_reset
              report "glb_reset is a command" severity warning;
            when add_fec_reset              =>  -- fec_reset
              report "fec_reset is a command" severity warning;
            when add_rcu_reset              =>  -- rcu_reset
              report "rcu_reset is a command" severity warning;
            when
              add_errst | add_trcfg | add_trcnt | add_lwadd |
              add_iradd | add_irdat | add_pmcfg | add_chadd | add_afl |
              add_imem to add_imem+sz_imem-1 |
              add_pmem to add_pmem+sz_pmem-1 |
              add_rmem to add_rmem+sz_rmem-1 |
              add_acl to add_acl+sz_acl |
              add_dm1l to add_dm1l+sz_dmem-1 |
              add_dm1h to add_dm1h+sz_dmem-1 |
              add_dm2l to add_dm2l+sz_dmem-1 |
              add_dm2h to add_dm2h+sz_dmem-1 |
              add_head to add_head+sz_head-1  =>
              en_out_i := true;
            when others =>
              report "Invalid address " & integer'image(addr_i)
                severity warning;
          end case;
          if en_out_i then
            rd_st_i <= start;
          else
            rd_st_i <= idle;
          end if;

        when start => 
          -- report "Writing an s" severity note;
          write(line_i, 's');
          writeline(output_i, line_i);
          rd_st_i <= output;

        when output =>
          if cur_i < addr_i or cur_i >= addr_i + cnt_i then
            rd_st_i <= endit;
          else
            data_i   := (others => '0');

            case cur_i is
              when add_errst => -- errst
                data_i(31 downto 0) :=  errst_i;
              when add_trcfg => -- trcfg
                data_i(31 downto 0) := trcfg_i;
              when add_trcnt => -- trcnt
                data_i(31 downto 0) := trcnt_i;
              when add_lwadd => -- lwadd
                data_i(17 downto 0) := lwadd_i;
              when add_iradd => -- iradd
                data_i(19 downto 0) := iradd_i;
              when add_irdat => -- irdat
                data_i(19 downto 0) := irdat_i;
              when add_pmcfg => -- pmcfg
                data_i(19 downto 0) :=  pmcfg_i;
              when add_chadd => -- chadd
                data_i(23 downto 0) := chadd_i;
              when add_afl => -- afl
                data_i(31 downto 0) := afl_i;

              when add_imem to add_imem+sz_imem-1 =>  -- IMEM
                data_i(23 downto 0) := imem_i(cur_i - add_imem);
              when add_pmem to add_pmem+sz_pmem-1 =>  -- PMEM
                data_i(9 downto 0) := pmem_i(cur_i - add_pmem);
              when add_rmem to add_rmem+sz_rmem-1 =>  -- RMEM
                data_i(19 downto 0) := rmem_i(cur_i - add_rmem);
              when add_acl to add_acl+sz_acl =>  -- ACL
                data_i(15 downto 0) := acl_i(cur_i - add_acl);
              when add_dm1l to add_dm1l+sz_dmem-1 =>  -- DM1L
                data_i(19 downto 0) := dm1l_i(cur_i - add_dm1l);
              when add_dm1h to add_dm1h+sz_dmem-1 =>  -- DM1H
                data_i(19 downto 0) := dm1h_i(cur_i - add_dm1h);
              when add_dm2l to add_dm2l+sz_dmem-1 =>  -- DM2L
                data_i(19 downto 0) := dm2l_i(cur_i - add_dm2l);
              when add_dm2h to add_dm2h+sz_dmem-1 =>  -- DM2H
                data_i(19 downto 0) := dm2h_i(cur_i - add_dm2h);
              when add_head to add_head+sz_head-1 =>  -- HEAD
                data_i(31 downto 0) := head_i(cur_i - add_head);
              when others => null;
            end case;
            str_i                           := slv2hstr(data_i);
            -- report "Reading from " & slv2hstr(int2slv(cur_i, 20)) &
            --   ": " & str_i severity note;
            write(line_i, str_i);
            writeline(output_i, line_i);
            cur_i := cur_i + 1;
          end if;

        when endit          =>
          -- report "Writing an e" severity note;
          write(line_i, 'e');
          writeline(output_i, line_i);
          data_i := (others => '0');
          cur_i  := 0;
          addr_i := 0;
          cnt_i  := 0;
          finish_rd_i <= '1';
          rd_st_i     <= idle;

        when others => 
          rd_st_i <= idle;
      end case;
    end if;
  end process reader;

  -- purpose: Execute instruction memory
  -- type   : sequential
  -- inputs : clk_i, grst_i, exec_cmd_i, cmd_abort_i, exec_addr_i
  -- outputs: bd, cstb, write
  imem_exec             : process (clk_i, grst_i)
    variable cur_i      : integer := 0;  -- Current instruction offset
    variable jump_i     : integer := 0;  -- Current instruction offset
    variable loop_i     : integer := 0;  -- Number of loops
    variable iter_i     : integer := 0;  -- Current iteration
    variable nwait_i    : integer := 0;  -- Clock cycles to wait
    variable bcast_i    : std_logic;
    variable ack_cnt_i  : integer;
    variable cstb_cnt_i : integer;
    variable writ_i     : boolean := false;
    constant dont_care_i : std_logic_vector(19 downto 0) := (others => 'Z');
  begin  -- process imem_exec

    if grst_i = '0' then                    -- asynchronous reset (active low)
      cstb     <= 'H';
      writ    <= 'H';
      bd        <= (others => 'L');
      exec_st_i <= idle;
      iradd_i   <= (others => '0');
      irdat_i   <= (others => '0');
      cur_i := 0;
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if cmd_glb_reset_i = '1' or cmd_rcu_reset_i = '1' then
        exec_st_i <= idle;
      else
        
        case exec_st_i is
          when idle =>
            loop_i := 0;
            jump_i := 0;
            iter_i := 0;
            bd     <= (others => 'L');
            cstb  <= 'H';
            writ <= 'H';

            exec_busy_i   <= '0';
            if cmd_exec_i = '1' then
              exec_st_i   <= start;
              exec_busy_i <= '1';
            end if;

          when start =>
            if exec_addr_i < 0 or exec_addr_i >= sz_imem then
              report "Address point out of scope" severity warning;
              exec_st_i <= idle;
            else
              cur_i := exec_addr_i;
              exec_st_i <= next_instr;
            end if;

          when next_instr =>
            -- report "Looking at instruction @ " & integer'image(cur_i) &
            --   ": " & slv2hstr(imem_i(cur_i)) severity note;
            if cmd_abort_i = '1' then
              exec_st_i <= idle;
            elsif imem_i(cur_i)(22) = '0' then
              exec_st_i <= rcu_instr;
            else
              exec_st_i <= altro_instr;
            end if;

          when rcu_instr =>
            exec_st_i <= next_instr;
            if cmd_abort_i = '1' then
              exec_st_i <= idle;
            else
              case slv2int(imem_i(cur_i)(19 downto 16)) is
                when 0     =>                    -- Loop or jump
                  jump_i   := slv2int(imem_i(cur_i)(7 downto 0)) - 1;
                  if imem_i(cur_i)(15) = '0' then  -- Jump
                    cur_i  := jump_i;            -- Jump to address
                  elsif loop_i = 0 then
                    loop_i := slv2int(imem_i(cur_i)(14 downto 8));
                    iter_i := 1;
                    cur_i  := jump_i;
                  elsif iter_i < loop_i then
                    iter_i := iter_i + 1;
                    cur_i  := jump_i;
                  else
                    loop_i := 0;
                    iter_i := 0;
                  end if;
                when 1 =>                   -- Reset errst
                  icmd_rs_status_i <= '1';
                when 2 =>                   -- Reset trcfg
                  icmd_rs_trcfg_i <= '1';
                when 3 =>                   -- Reset trcnt
                  icmd_rs_trcnt_i <= '1'; 
                when 6 =>                   -- ChRdo (not handled)
                  null;
                when 7 =>                   -- PMREAD (not handled)
                  null;
                when 8 =>                   -- PMWRITE (not handled)
                  null;
                when 9 =>                   -- End of block;
                  exec_st_i <= idle;
                when 10 =>                  -- Wait for X clock cycles
                  nwait_i := slv2int(imem_i(cur_i)(15 downto 0));
                  exec_st_i <= wait_clk;
                when 11 =>
                  icmd_trigger_i <= '1';
                when others => null;
              end case;
              cur_i := cur_i + 1;
            end if;

          when wait_clk =>
            if cmd_abort_i = '1' then
              exec_st_i <= idle;
            elsif nwait_i = 0 then
              exec_st_i <= next_instr;
            else
              nwait_i := nwait_i - 1;
            end if;


          when altro_instr =>
            if cmd_abort_i = '1' then
              exec_st_i <= idle;
            else
              -- Set address on bus
              iradd_i   <= (parity(imem_i(cur_i)(18 downto 0)) &
                            imem_i(cur_i)(18 downto 0));
              bcast_i := imem_i(cur_i)(18);
              writ_i  := imem_i(cur_i)(21) = '1';
              if writ_i then
                writ  <= '0';
              else
                writ  <= 'H';
              end if;
              cur_i   := cur_i + 1;
              exec_st_i <= altro_instr2;
            end if;

          when altro_instr2 =>
            if cmd_abort_i = '1' then
              exec_st_i <= idle;
            else
              -- Set data on bus
              irdat_i   <= imem_i(cur_i)(19 downto 0);
              cur_i := cur_i + 1;
              exec_st_i <= set_bus;
            end if;

          when set_bus                       =>
            if cmd_abort_i = '1' then
              exec_st_i <= idle;
            else
              if writ_i then
                bd      <= iradd_i & irdat_i;
              else
                bd      <= iradd_i & dont_care_i;
              end if;
              exec_st_i <= strobe;
            end if;

          when strobe =>
            if cmd_abort_i = '1' then
              exec_st_i   <= idle;
            else
              -- Assert the control strobe
              cstb       <= '0';
              timeout_i   <= '0';
              if bcast_i = '1' then
                cstb_cnt_i := 4;
                exec_st_i <= wait_strobe;
              else
                ack_cnt_i  := 32;
                exec_st_i <= wait_ack;
              end if;
            end if;

          when wait_strobe =>
            if cmd_abort_i = '1' then
              exec_st_i <= idle;
            elsif cstb_cnt_i = 0 then     
              cstb     <= 'H';
              writ    <= 'H';
              exec_st_i <= next_instr;
            else
              cstb_cnt_i := cstb_cnt_i - 1;
            end if;

          when wait_ack                   =>
            if cmd_abort_i = '1' then
              exec_st_i        <= idle;
            elsif eror = '0' then
              exec_st_i        <= idle;
            elsif ackn = '0' then
              cstb             <= 'H';
              writ             <= 'H';
              bd(39 downto 20) <= (others => 'L');
              exec_st_i        <= take_data;
            -- elsif ack_cnt_i = 16 then  -- Temporary
              -- cstb      <= 'H';
              -- writ      <= 'H';
              -- bd        <= (others => 'L');
              -- exec_st_i <= take_data;
            elsif ack_cnt_i = 0 then
              report "Acknowledge timeout " severity WARNING;
              cstb             <= 'H';
              writ             <= 'H';
              timeout_i        <= '1';
              exec_st_i        <= next_instr;
            else
              ack_cnt_i := ack_cnt_i - 1;
            end if;

          when take_data =>
            -- report "data on bus: " & slv2hstr(bd(19 downto 0)) &
            --   " rmem_0=" & slv2hstr(rmem_i(0)) severity note;
            rmem_i(0) <= bd(19 downto 0);
            if ackn /= '0' then
              exec_st_i <= next_instr;
            end if;

          when others => null;
        end case;
      end if;
    end if;
  end process imem_exec;


  -- purpose: Handle register trcfg_i
  -- type   : sequential
  -- inputs : clk_i, grst_i
  -- outputs: 
  registers           : process (clk_i, grst_i)
    variable l1_cnt_i  : integer range 0 to 2**16 - 1;
    variable l2_cnt_i  : integer range 0 to 2**16 - 1;
    variable rst_cnt_i : integer := 0;
  begin  -- process trcfg_proc
    if grst_i = '0' then                    -- asynchronous reset (active low)
      trcfg_i <= (others => '0');
      pmcfg_i <= (others => '0');
      l1_cnt_i                   := 0;
      l2_cnt_i                   := 0;
      grst  <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge

      if (icmd_rs_trcfg_i = '1' or
          cmd_rs_trcfg_i = '1' or
          cmd_rcu_reset_i = '1' or
          cmd_glb_reset_i = '1') then
        trcfg_i <= (others => '0');
      elsif load_trcfg_i = '1' then
        trcfg_i <= idata_i;
      end if;

      if (cmd_rcu_reset_i = '1' or
          cmd_glb_reset_i = '1') then
        pmcfg_i <= (others => '0');
      elsif load_pmcfg_i = '1'  then
        pmcfg_i <= idata_i(19 downto 0);
      end if;

      if (cmd_rcu_reset_i = '1' or
          cmd_glb_reset_i = '1') then
        afl_i <= (others => '0');
      elsif load_afl_i = '1'  then
        afl_i <= idata_i(31 downto 0);
      end if;

      if (cmd_rs_status_i = '1' or
          icmd_rs_status_i = '1' or
          cmd_rcu_reset_i = '1' or
          cmd_glb_reset_i = '1') then
        errst_i              <= (others => '0');
      else
        errst_i(31)          <= exec_busy_i;
        errst_i(30 downto 4) <= (others => '0');
        if eror = '0' then
          errst_i(3)         <= '1';
        else
          errst_i(3)         <= '0';
        end if;
        if cmd_abort_i = '1' then
          errst_i(1)         <= '1';
        else
          errst_i(1)         <= '0';
        end if;
        if timeout_i = '1' then
          errst_i(2)         <= '1';
        else
          errst_i(2)         <= '0';
        end if;
        errst_i(0)           <= '0';
      end if;

      l1_ii <= l1_i;
      l2_ii <= l2_i;
      if (cmd_rs_trcnt_i = '1' or
          icmd_rs_trcnt_i = '1' or
          cmd_rcu_reset_i = '1' or
          cmd_glb_reset_i = '1') then
        l1_cnt_i := 0;
        l2_cnt_i := 0;
      elsif l1_i = '0' and l1_ii = '1' then
        l1_cnt_i := l1_cnt_i + 1;
      elsif l2_i = '0' and l2_ii = '1' then
        l2_cnt_i := l2_cnt_i + 1;
      end if;
      trcnt_i <= int2slv(l2_cnt_i, 16) & int2slv(l1_cnt_i, 16);

      -- Keep the fec reset down for at least 200 ns;
      if (cmd_glb_reset_i = '1' or cmd_fec_reset_i = '1') then
        rst_cnt_i := 8;
        grst <= '0';
      elsif rst_cnt_i > 0 then
        rst_cnt_i := rst_cnt_i - 1;
        grst <= '0';
      else
        grst <= '1';
      end if;

    end if;
  end process registers;

  -- purpose: Make triggers
  -- type   : sequential
  -- inputs : clk_i, grst_i
  -- outputs: 
  triggers             : process (clk_i, grst_i)
    variable l0_dur_i  : integer := 0;
    variable l1_dur_i  : integer := 0;
    variable l2_dur_i  : integer := 0;
    variable l1_wait_i : integer := 0;
    variable l2_wait_i : integer := 0;
  begin  -- process triggers 
    if grst_i = '0' then                    -- asynchronous reset (active low)
      trg_st_i <= idle;
      l0_i     <= '0';
      l1_i     <= '1';
      l2_i     <= '1';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      trg_st_i <= trg_st_i;

      case trg_st_i is
        when idle =>
          l0_i <= '0';
          l1_i <= '1';
          l2_i <= '1';
          if ((cmd_l1_i = '1' or icmd_trigger_i = '1') and
              cmd_l1_cmd_i = '1' and trcfg_i(16 downto 15) = "10") then
            trg_st_i <= gen_l0;
            l0_dur_i := 8;
            l0_i <= '1';
          end if;

        when gen_l0 =>
          if l0_dur_i = 0 then
            l1_wait_i := 5.5 us / 25 ns - 8;
            l0_i     <= '0';
            trg_st_i <= wait_to_l2;
          else
            l0_i     <= '1';
            l0_dur_i  := l0_dur_i - 1;
          end if;

        when wait_to_l1 =>
          if l2_wait_i = 0 then
            trg_st_i <= gen_l1;
          else
            l1_wait_i := l1_wait_i - 1;
          end if;

        when gen_l1 =>
          if l1_dur_i = 0 then
            l2_wait_i := slv2int(trcfg_i(13 downto 0));
            l1_i     <= '1';
            trg_st_i <= wait_to_l2;
          else
            l1_i     <= '0';
            l1_dur_i  := l1_dur_i - 1;
          end if;

        when wait_to_l2 =>
          if l2_wait_i = 0 then
            trg_st_i <= gen_l2;
          else
            l2_wait_i := l2_wait_i - 1;
          end if;
          
        when gen_l2 =>
          if l2_dur_i = 0 then
            l2_i <= '1';
            trg_st_i <= idle;
          else
            l2_i <= '0';
            l2_dur_i := l2_dur_i - 1;
          end if;

        when others => null;
      end case;
    end if;
  end process triggers ;

  -- Reset
  reset_it : entity rcu_model.reseter port map (rstb => grst_i);

  -- 40 Mhz
  rclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 40000000)
    port map (clk       => clk_i);

  -- 10 MHz  
  sclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 10000000)
    port map (clk       => sclk_i);

end architecture pipe_stim;

------------------------------------------------------------------------------
-- 
-- EOF
--
