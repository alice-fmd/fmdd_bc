------------------------------------------------------------------------------
-- Title      : Architecture that makes triggers
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rcu_trig.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-19
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/10/02  1.0      cholm   Created
------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;
library ctp_model;
use ctp_model.ctp_pack.all;

-------------------------------------------------------------------------------
architecture ctp_stim of rcu is
  signal rclk_i  : std_logic;
  signal sclk_i  : std_logic;
  signal writ_i  : std_logic          := 'H';
  signal grst_i  : std_logic;
  signal addr_i  : std_logic_vector(39 downto 20);  -- Debug
  signal data_i  : std_logic_vector(19 downto 0);   -- Debug
  signal what_i  : string(7 downto 1) := (others => ' ');
  signal what_ii : string(7 downto 1) := (others => ' ');

  constant PERIOD     : time    := 25 ns;
  constant L1_DELAY   : time    := 1 us;
  constant L2_DELAY   : time    := 10 us;
  constant SCALE      : integer := 1;
  constant RPINC_TIME : time    := 2 us;
  
  signal l1_delay_i : std_logic_vector(31 downto 0) := (others => '1');
  signal l2_delay_i : std_logic_vector(31 downto 0) := (others => '1');
  signal scale_i    : std_logic_vector(31 downto 0) := (others => '0');
  signal busy_i     : std_logic;
  signal busy_ii    : std_logic                     := '1';
  signal lvl2_i     : std_logic;
  signal rpinc_i    : integer                       := RPINC_TIME / PERIOD;
  signal l2_cnt_i   : integer                       := 0;
  signal do_rpinc_i : boolean                       := false;
begin  -- architecture stimulus
  connectivity : block is
  begin  -- block  connectivity
    rclk      <= rclk_i;
    sclk      <= sclk_i;
    writ      <= writ_i;
    addr_i    <= bd(39 downto 20);
    data_i    <= bd(19 downto 0);
    grst      <= grst_i;
    busy_i    <= busy or busy_ii;
    lvl2      <= lvl2_i;
    scl_a     <= 'H';
    scl_b     <= 'H';
    sda_out_a <= 'H';
    sda_out_b <= 'H';
  end block connectivity;

  -- purpose: Convert 
  conversion : block
  begin  -- block conversion
    l1_delay_i <= std_logic_vector(to_unsigned(L1_DELAY / PERIOD, 32));
    l2_delay_i <= std_logic_vector(to_unsigned(L2_DELAY / PERIOD, 32));
    scale_i    <= std_logic_vector(to_unsigned(SCALE, 32));
  end block conversion;

  -- Reset
  reset_it : entity rcu_model.reseter port map (rstb => grst_i);

  -- 40 Mhz
  rclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 40000000)
    port map (clk       => rclk_i);

  -- 10 MHz  
  sclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 10000000)
    port map (clk       => sclk_i);

  -- Trigger
  trigger: entity ctp_model.ctp
    port map (clk      => rclk_i,          -- [in]  Master clock (must be fast)
              rstb     => grst_i,         -- [in]  Async. reset (-)
              l0       => lvl0,           -- [out] L0 output (+)
              l1       => lvl1,           -- [out] L1 output (-)
              l2       => lvl2_i,           -- [out] L2 output (-)
              busy     => busy_i,         -- [in]  Busy input
              scale    => scale_i,        -- [in]  Down scaler
              l1_delay => l1_delay_i,     -- [in]  L1 delay from L0
              l2_delay => l2_delay_i);    -- [in] L2 delay from L1
  
  stim : process is
    variable last : time := NOW;
  begin  -- process stim
    bd     <= (others => '0');
    writ_i <= 'H';
    cstb   <= 'H';
    -- lvl0   <= '0';
    -- lvl1   <= '1';
    -- lvl2   <= '1';
    wait until grst_i = '1';
    wait until rising_edge(rclk_i);
    wait for 100 ns;
    write_register(hadd => HADD,
                   what => "MEBS___",
                   data => 16#0140#,    -- 16#0002#,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);
    write_register(hadd => HADD,
                   what => "L1_TIMO",
                   data => L1_DELAY / PERIOD + 1,    -- 16#0002#,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);
    write_register(hadd => HADD,
                   what => "L2_TIMO",
                   data => L2_DELAY / PERIOD + 1,    -- 16#0002#,
                   bd   => bd,
                   cstb => cstb,
                   writ => writ_i,
                   ackn => ackn,
                   trsf => trsf,
                   rclk => rclk_i,
                   name => what_i);
    wait for 1 us;
    busy_ii <= '0';

    wait;
  end process stim;


  counters: process (rclk_i, grst_i)
  begin  -- process counters
    if grst_i = '0' then                -- asynchronous reset (active low)
      rpinc_i  <= RPINC_TIME / PERIOD;
    elsif rclk_i'event and rclk_i = '1' then  -- rising clock edge
      do_rpinc_i <= false;
      if lvl2_i = '0' then
        rpinc_i  <= RPINC_TIME / PERIOD;
        l2_cnt_i <= l2_cnt_i + 1;
      elsif rpinc_i <= 0 then
        rpinc_i <= 2 * RPINC_TIME / PERIOD / 3;
        l2_cnt_i <= l2_cnt_i - 1;
        do_rpinc_i <= true;
      else
        rpinc_i <= rpinc_i - 1;
      end if;
    end if;
  end process counters;
  -- purpose: Do ALRPINC sometime after an L2 was seen
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  read_out : process
  begin  -- process read_out
    if do_rpinc_i  then
        rpinc(bd => bd,
              cstb => cstb,
              writ => writ_i,
              ackn => ackn,
              trsf => trsf,
              rclk => rclk_i,
              name => what_ii);
    end if;
  end process read_out;


--      rpinc(bd => bd,
--            cstb => cstb,
--            writ => writ_i,
--            ackn => ackn,
--            trsf => trsf,
--            rclk => rclk_i,
--            name => what_i);
end architecture ctp_stim;
------------------------------------------------------------------------------
-- 
-- EOF
--
