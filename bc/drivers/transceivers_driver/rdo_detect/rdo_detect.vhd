------------------------------------------------------------------------------
-- Title      : Detect read-out
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rdo_detect.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Detect a channel read-out instruction on the bus. Note, that
--              the instruction is for the ALTRO's and is therefor only 5 bits
--              wide - not 7, as for the BC (FMD version).  The component
--              actually detects the acknowledge on the bus, and then checks
--              if the command acknowledges as a channel read-out command.  If
--              that's the case, then an enable signal is sent to the GTL
--              drivers to allow data to flow _out_ of the card.  This is
--              handled in the transcievers_driver component, one level up. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rdo_detect is
  port (
    clk        : in  std_logic;         -- clock
    rstb       : in  std_logic;         -- Async reset
    ackn_chrdo : in  std_logic;         -- Acknowledge during read-out
    al_cs      : in  std_logic;         -- ALTRO card select
    icode      : in  std_logic_vector(4 downto 0);  -- Instruction code
    rto        : out std_logic);
end entity rdo_detect;

architecture rtl of rdo_detect is
  type state_t is (idle, ackn, rto0, rto1, rto2, rto3);
  signal   st_i      : state_t;         -- State
  constant add_chrdo : unsigned(4 downto 0) := "1" & X"A";

begin  -- architecture rtl

  -- purpose: Detect read-out mode
  -- type   : sequential
  -- inputs : clk, rstb, icode, al_cs, ackn_chrdo
  -- outputs: rto
  detect: process (clk, rstb) is
  begin  -- process detect
    if rstb = '0' then                  -- asynchronous reset (active low)
      rto <= '0';
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge

      case st_i is
        when idle =>
          rto <= '0';
          if  ((unsigned(icode) = add_chrdo)
               and al_cs = '1' and ackn_chrdo = '1') then
            st_i <= ackn;
          else
            st_i <= idle;
          end if;

        when ackn   =>
          if ackn_chrdo = '0' then
            rto  <= '1';
            st_i <= rto0;
          else
            rto  <= '0';
            st_i <= ackn;
          end if;

        when rto0 =>
          rto  <= '1';
          st_i <= rto1;

        when rto1 =>
          rto  <= '1';
          st_i <= rto2;
 
        when rto2 =>
          rto  <= '1';
          st_i <= rto3;
 
        when rto3 =>
          rto  <= '1';
          st_i <= idle;
 
        when others =>
          rto  <= '0';
          st_i <= idle;
      end case;
    end if;
  end process detect;

end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package rdo_detect_pack is
  component rdo_detect
    port (
      clk        : in  std_logic;       -- clock
      rstb       : in  std_logic;       -- Async reset
      ackn_chrdo : in  std_logic;       -- Acknowledge during read-out
      al_cs      : in  std_logic;       -- ALTRO card select
      icode      : in  std_logic_vector(4 downto 0);  -- Instruction code
      rto        : out std_logic);
  end component rdo_detect;
end package rdo_detect_pack;

------------------------------------------------------------------------------
-- 
-- EOF
--
