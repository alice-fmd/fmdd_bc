------------------------------------------------------------------------------
-- Title      : I/O Drivers of signals (pins)
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : transceivers_driver.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-03
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Drive the GTL's on the card.
--
--                 ab:  LVTTL -> GTL   (data out)
--                 ba:  GTL   -> LVTTL (data in)
--
--              Note, inverted logic enable inputs
--              This component takes care of controlling the GTL drivers on
--              the board.  That means, that this component either allows or
--              disallows data to flow in or out of the card. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mstable_unit_pack.all;
use work.rdo_detect_pack.all;
use work.tr_driver_pack.all;

entity transceivers_driver is
  port (
    clk            : in  std_logic;     -- Clock
    rstb           : in  std_logic;     -- Async reset
    tsm_on         : in  std_logic;     -- TSM: on
    al_cs          : in  std_logic;     -- ALTRO card select
    bc_cs          : in  std_logic;     -- BC card select
    cstb           : in  std_logic;     -- Control strobe
    wr_al          : in  std_logic;     -- Bus: Write
    al_dolo_en     : in  std_logic;     -- Bus: ALTRO data enable
    bc_dolo_en     : in  std_logic;     -- Bus: BC data enable
    al_trsf_en     : in  std_logic;     -- Bus: ALTRO transfer enable
    card_isolation : in  std_logic;     -- Bus: Card is isolated
    bc_master      : in  std_logic;     -- Bus: BC is local master
    rmtrsf_en      : in  std_logic;     -- TSM/EVL: Transfer enable
    ackn_chrdo     : in  std_logic;     -- TSM/EVL: Acknowledge during read-out
    altro_sw       : in  std_logic;     -- HW: ALTRO switch
    sda_out        : in  std_logic;     -- I2C: Out data
    icode          : in  std_logic_vector(4 downto 0);  -- Instruction
    oeba_l         : out std_logic;     -- GTL: Enable out low bits
    oeab_l         : out std_logic;     -- GTL: Enable in low bits
    oeba_h         : out std_logic;     -- GTL: Enable out high bits
    oeab_h         : out std_logic;     -- GTL: Enable in high bits
    read_data      : out std_logic;     -- Read data
    ric            : out std_logic;     -- ???
    ctr_in         : out std_logic;     -- Slow control in
    ctr_out        : out std_logic);    -- Slow control out
end entity transceivers_driver;

architecture rtl of transceivers_driver is
  signal or_cs_i         : std_logic;   -- ALTRO or BC card select
  signal not_write_i     : std_logic;   -- Not write
  signal cs_cstb_i       : std_logic;   -- Card selected, and data strobe
  signal bc_dolo_en_i    : std_logic;   -- Registered bc_dolo_en
  signal read_cs_i       : std_logic;   -- Read instruction for Card
  signal read_cs_in_i    : std_logic;   -- Read instr. chose in
  signal read_cs_out_i   : std_logic;   -- Read instr. chose out
  signal trsf_data_i     : std_logic;   -- Transfer data on bus
  signal trsf_data_in_i  : std_logic;   -- Transfer data chose in
  signal trsf_data_out_i : std_logic;   -- Transfer data chose out
  signal rto_i           : std_logic;   -- Read-out, transciever out dir.
  signal local_i         : std_logic;   -- Master, isolated, or no ALTRO
  signal reenable_i      : std_logic;   -- Re-enable GTL's
  signal or_cstb_i       : std_logic;
begin  -- architecture rtl
  -- purpose: collect combinatorics
  combi                  : block is
  begin  -- block combi
    or_cs_i     <= al_cs or bc_cs;
    not_write_i <= not wr_al;
    cs_cstb_i   <= or_cs_i and cstb and not_write_i;
    trsf_data_i <= al_trsf_en or rmtrsf_en or rto_i;
    read_cs_i   <= (cs_cstb_i or
                    al_dolo_en or
                    bc_dolo_en or
                    bc_dolo_en_i or
                    trsf_data_i);
    local_i     <= not altro_sw or bc_master or card_isolation;
  end block combi;

  -- purpose: Register bc_dolo_en (1 more clock cycle delay) to avoid
  --          internal/GTL transciever data conflict
  -- type   : sequential
  -- inputs : clk, rstb, bc_dolo_en
  -- outputs: bc_dolo_en_i
  reg_bc_dolo : process (clk, rstb) is
  begin  -- process reg_bc_dolo
    if rstb = '0' then                  -- asynchronous reset (active low)
      bc_dolo_en_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      bc_dolo_en_i <= bc_dolo_en;
    end if;
  end process reg_bc_dolo;

  read_driver : entity work.tr_driver
    port map (
      dir_out => read_cs_i,             -- in  Output enable
      enb_in  => read_cs_in_i,          -- out Enabled input
      enb_out => read_cs_out_i);        -- out Enabled output

  -- Transcievers are isolated when the card is isolated, or the ALTRO's are
  -- off. 
  trsf_driver : entity work.tr_driver
    port map (
      dir_out => trsf_data_i,           -- in  Output enable
      enb_in  => trsf_data_in_i,        -- out Enabled input
      enb_out => trsf_data_out_i);      -- out Enabled output

  -- Detect read-out
  ro_detect   : entity work.rdo_detect
    port map (
        clk        => clk,              -- in  clock
        rstb       => rstb,             -- in  Async reset
        ackn_chrdo => ackn_chrdo,       -- in  Acknowledge during read-out
        al_cs      => al_cs,            -- in  ALTRO card select
        icode      => icode,            -- in  Instruction code
        rto        => rto_i);           -- out

  -- Re-enable GTL's after local transfers. 
  re_en_gtl : entity work.mstable_unit
    port map (
        clk  => clk,                    -- in  Clock
        rstb => rstb,                   -- in  Async reset
        i    => local_i,                -- in  Input data
        o    => reenable_i);            -- out Output data

  -- purpose: Output the signals
  output : block is
  begin  -- block output
    oeba_l    <= reenable_i or read_cs_in_i or tsm_on;
    oeba_h    <= reenable_i or trsf_data_in_i or tsm_on;
    oeab_l    <= reenable_i or read_cs_out_i;
    oeab_h    <= reenable_i or trsf_data_out_i;
    read_data <= or_cs_i;
    ric       <= not_write_i;
    -- Transcievers for control signals permanently enabled in normal mode.
    -- ctr_out should be enabled when:
    --    ALTRO's are on,
    --    ALTRO's are off AND sda_out asserted by local slow control
    -- (in the later case, error, ackn, dstb, and trsf will be asserted).
    ctr_in    <= '0'; -- reenable_i;
    ctr_out   <= '0'; -- reenable_i and not sda_out;
  end block output;

  
end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package transceivers_driver_pack is

end package transceivers_driver_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
