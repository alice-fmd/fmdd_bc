------------------------------------------------------------------------------
-- Title      : Test of meta-stable unit
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : mstable_unit_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/21
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mstable_unit_pack.all;

------------------------------------------------------------------------------
entity mstable_unit_tb is
end entity mstable_unit_tb;

------------------------------------------------------------------------------

architecture test of mstable_unit_tb is
  constant PERIOD : time := 25 ns;
  signal   clk_i  : std_logic := '0';          -- Clock
  signal   rstb_i : std_logic := '0';          -- Async reset
  signal   i_i    : std_logic := '0';          -- Input data
  signal   o_i    : std_logic;          -- Output data
begin  -- architecture test

  dut: entity work.mstable_unit
    port map (
        clk  => clk_i,                  -- in  Clock
        rstb => rstb_i,                 -- in  Async reset
        i    => i_i,                    -- in  Input data
        o    => o_i);                   -- out Output data

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';
    wait for 100 ns;
    wait until rising_edge(clk_i);
    i_i <= '1';
    wait until rising_edge(clk_i);
    wait until rising_edge(clk_i);
    i_i <= '0';
    wait until o_i = '0';
    wait until rising_edge(clk_i);
    i_i <= '1';
    wait until rising_edge(clk_i);
    i_i <= '0';

    wait;                               -- forever
  end process stimuli;
  
  -- purpose: make clock
  -- type   : combinational
  -- inputs : clk_i
  -- outputs: clk_i
  clk_gen: process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;

end architecture test;

------------------------------------------------------------------------------
