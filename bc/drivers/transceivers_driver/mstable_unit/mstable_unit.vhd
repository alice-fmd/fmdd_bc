------------------------------------------------------------------------------
-- Title      : Meta stable unit
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : mstable_unit.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Minimum required 1 (ALTRO trsf_e/dolo_en  - tau) = 180 ns ->
--              ~ 7 clk cycles @ 40 MHz. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity mstable_unit is
  port (
    clk  : in  std_logic;               -- Clock
    rstb : in  std_logic;               -- Async reset
    i    : in  std_logic;               -- Input data
    o    : out std_logic);              -- Output data
end entity mstable_unit;

architecture rtl of mstable_unit is
  signal reg : std_logic_vector(7 downto 0);
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi: block is
  begin  -- block combi
    o <= (i or
          reg(0) or reg(1) or reg(2) or reg(3) or
          reg(4) or reg(5) or reg(6) or reg(7));
  end block combi;

  -- purpose: Meta stable unit
  -- type   : sequential
  -- inputs : clk, rstb, i
  -- outputs: o
  msu : process (clk, rstb) is
  begin  -- process msu
    if rstb = '0' then                  -- asynchronous reset (active low)
      reg    <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      reg(7) <= reg(6);
      reg(6) <= reg(5);
      reg(5) <= reg(4);
      reg(4) <= reg(3);
      reg(3) <= reg(2);
      reg(2) <= reg(1);
      reg(1) <= reg(0);
      reg(0) <= i;
    end if;
  end process msu;
end architecture rtl;

------------------------------------------------------------------------------
architecture rtl2 of mstable_unit is
  signal reg_0 : std_logic;
  signal reg_1 : std_logic;
  signal reg_2 : std_logic;
  signal reg_3 : std_logic;
  signal reg_4 : std_logic;
  signal reg_5 : std_logic;
  signal reg_6 : std_logic;
  signal reg_7 : std_logic;
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi: block is
  begin  -- block combi
    o <= (i or
          reg_0 or reg_1 or reg_2 or reg_3 or
          reg_4 or reg_5 or reg_6 or reg_7);
  end block combi;

  -- purpose: Meta stable unit
  -- type   : sequential
  -- inputs : clk, rstb, i
  -- outputs: o
  msu : process (clk, rstb) is
  begin  -- process msu
    if rstb = '0' then                  -- asynchronous reset (active low)
      reg_0 <= '0'; 
      reg_1 <= '0'; 
      reg_2 <= '0'; 
      reg_3 <= '0'; 
      reg_4 <= '0'; 
      reg_5 <= '0'; 
      reg_6 <= '0'; 
      reg_7 <= '0'; 
    elsif clk'event and clk = '1' then  -- rising clock edge
      reg_7 <= reg_6;
      reg_6 <= reg_5;
      reg_5 <= reg_4;
      reg_4 <= reg_3;
      reg_3 <= reg_2;
      reg_2 <= reg_1;
      reg_1 <= reg_0;
      reg_0 <= i;
    end if;
  end process msu;
end architecture rtl2;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package mstable_unit_pack is
  component mstable_unit
    port (
      clk  : in  std_logic;             -- Clock
      rstb : in  std_logic;             -- Async reset
      i    : in  std_logic;             -- Input data
      o    : out std_logic);            -- Output data
  end component mstable_unit;
end package mstable_unit_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
