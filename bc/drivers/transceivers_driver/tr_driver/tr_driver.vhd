------------------------------------------------------------------------------
-- Title      : Transciever driver
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : tr_driver.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Enable outputs in inverted logic
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity tr_driver is
  port (
    dir_out    : in  std_logic;         -- Output enable
    enb_in     : out std_logic;         -- Enabled input
    enb_out    : out std_logic);        -- Enabled output
end entity tr_driver;

architecture rtl of tr_driver is
  signal out_i :     std_logic_vector(1 downto 0);
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi        :     block is
  begin  -- block combi
    out_i   <= "01" when dir_out = '1' else "10";
    enb_in  <= out_i(0);
    enb_out <= out_i(1);
  end block combi;
end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package tr_driver_pack is
  component tr_driver
    port (
      dir_out : in  std_logic;          -- Output enable
      enb_in  : out std_logic;          -- Enabled input
      enb_out : out std_logic);         -- Enabled output
  end component tr_driver;
end package tr_driver_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
