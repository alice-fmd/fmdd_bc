------------------------------------------------------------------------------
-- Title      : Test of signals drivers - in particular the error filter
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : signals_driver_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/22
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/22  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.signals_driver_pack.all;

------------------------------------------------------------------------------
entity signals_driver_tb is
end entity signals_driver_tb;

------------------------------------------------------------------------------

architecture test of signals_driver_tb is
  constant PERIOD : time := 25 ns;      -- Clock cycle

  signal clk_i         : std_logic := '0';  -- Clock
  signal rstb_i        : std_logic := '0';  -- Async reset
  signal dout_i        : std_logic_vector(15 downto 0);  -- Bus: Output
  signal bc_dolo_en_i  : std_logic := '0';  -- Bus: Data enable
  signal bc_ackn_i     : std_logic := '0';  -- Bus: Acknowledge from BC
  signal bc_ackn_en_i  : std_logic := '0';  -- Bus: Acknowledge enable
  signal bc_error_i    : std_logic := '0';  -- Bus: Error
  signal evl_addr_i    : std_logic_vector(39 downto 20);  -- EVL: Address
  signal alevl_rdtrx_i : std_logic := '0';  -- EVL: read transmit
  signal evl_cstb_i    : std_logic := '0';  -- EVL: Control strobe
  signal rmdata_out_i  : std_logic_vector(39 downto 0);  -- EVL/TSM output data
  signal rmtrsf_i      : std_logic := '0';  -- EVL/TSM: Transfer
  signal rmtrsf_en_i   : std_logic := '0';  -- EVL/TSM: Enable transfer
  signal rmdstb_i      : std_logic := '0';  -- EVL/TSM: Data strobe
  signal din_i         : std_logic_vector(39 downto 0);  -- Bus: data in
  signal al_errorb_i   : std_logic;     -- Bus: Altro error
  signal bd_i          : std_logic_vector(39 downto 0);  -- Bus
  signal errorb_i      : std_logic := 'Z';  -- Bus: Acknowledge
  signal acknb_i       : std_logic := 'Z';  -- Bus: Acknowledge
  signal trsfb_i       : std_logic := 'Z';  -- Bus: Transfer
  signal dstbb_i       : std_logic := 'Z';  -- Bus: Data strobe
  signal writeb_i      : std_logic := 'Z';  -- Bus: Write
  signal cstbb_i       : std_logic := 'Z';  -- Bus : Control strobe

begin  -- architecture test

  dut : entity work.signals_driver
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        dout        => dout_i,          -- in  Bus: Output
        bc_dolo_en  => bc_dolo_en_i,    -- in  Bus: Data enable
        bc_ackn     => bc_ackn_i,       -- in  Bus: Acknowledge from BC
        bc_ackn_en  => bc_ackn_en_i,    -- in  Bus: Acknowledge enable
        bc_error    => bc_error_i,      -- in  Bus: Error
        evl_addr    => evl_addr_i,      -- in  EVL: Address
        alevl_rdtrx => alevl_rdtrx_i,   -- in  EVL: read transmit
        evl_cstb    => evl_cstb_i,      -- in  EVL: Control strobe
        rmdata_out  => rmdata_out_i,    -- in  EVL/TSM output data
        rmtrsf      => rmtrsf_i,        -- in  EVL/TSM: Transfer
        rmtrsf_en   => rmtrsf_en_i,     -- in  EVL/TSM: Enable transfer
        rmdstb      => rmdstb_i,        -- in  EVL/TSM: Data strobe
        din         => din_i,           -- out Bus: data in
        al_errorb   => al_errorb_i,     -- out Bus: Altro error
        bd          => bd_i,            -- inout Bus
        errorb      => errorb_i,        -- inout Bus: error
        acknb       => acknb_i,         -- inout Bus: Acknowledge
        trsfb       => trsfb_i,         -- inout Bus: Transfer
        dstbb       => dstbb_i,         -- inout Bus: Data strobe
        writeb      => writeb_i,        -- inout Bus: Write
        cstbb       => cstbb_i);        -- inout Bus : Control strobe

  -- purpose: Provide stimuli 
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli : process is
  begin  -- process stimuli
    dout_i       <= (others => '0');
    bd_i         <= (others => 'Z');
    rmdata_out_i <= (others => '0');
    evl_addr_i   <= (others => '0');
    errorb_i     <= 'H';
    wait for 100 ns;
    rstb_i       <= '1';
    wait for 100 ns;

    wait until rising_edge(clk_i);
    errorb_i <= '1';
    wait until rising_edge(clk_i);
    wait for 0.375 * PERIOD;
    errorb_i <= '0';
    wait for 100 ns;
    wait until rising_edge(clk_i);    
    errorb_i <= '1';
    wait until rising_edge(clk_i);    
    errorb_i <= 'H';

    wait;                               -- forever
  end process stimuli;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_gen: process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;
end architecture test;

------------------------------------------------------------------------------
-- 
-- EOF
--
