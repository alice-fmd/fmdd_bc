------------------------------------------------------------------------------
-- Title      : I/O Drivers of signals (pins)
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : signals_driver.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mux_bus_pack.all;

entity signals_driver is
  port (
    clk         : in    std_logic;      -- Clock
    rstb        : in    std_logic;      -- Async reset
    dout        : in    std_logic_vector(15 downto 0);  -- Bus: Output
    bc_dolo_en  : in    std_logic;      -- Bus: Data enable
    bc_ackn     : in    std_logic;      -- Bus: Acknowledge from BC
    bc_ackn_en  : in    std_logic;      -- Bus: Acknowledge enable
    bc_error    : in    std_logic;      -- Bus: Error
    evl_addr    : in    std_logic_vector(39 downto 20);  -- EVL: Address
    alevl_rdtrx : in    std_logic;      -- EVL: read transmit
    evl_cstb    : in    std_logic;      -- EVL: Control strobe
    rmdata_out  : in    std_logic_vector(39 downto 0);  -- EVL/TSM output data
    rmtrsf      : in    std_logic;      -- EVL/TSM: Transfer
    rmtrsf_en   : in    std_logic;      -- EVL/TSM: Enable transfer
    rmdstb      : in    std_logic;      -- EVL/TSM: Data strobe
    din         : out   std_logic_vector(39 downto 0);  -- Bus: data in
    al_errorb   : out   std_logic;      -- Bus: Altro error
    bd          : inout std_logic_vector(39 downto 0);  -- Bus
    errorb      : inout std_logic;      -- Bus: Error 
    acknb       : inout std_logic;      -- Bus: Acknowledge
    trsfb       : inout std_logic;      -- Bus: Transfer
    dstbb       : inout std_logic;      -- Bus: Data strobe
    writeb      : inout std_logic;      -- Bus: Write
    cstbb       : inout std_logic);     -- Bus: Control strobe
end entity signals_driver;

architecture rtl of signals_driver is
  signal dout_mux_i    : std_logic_vector(39 downto 0);  -- chosen output
  signal dout_i        : std_logic_vector(39 downto 0);  -- normal data output
  signal en_addr_i     : std_logic;     -- Enable output of upper 20 bits
  signal en_data_i     : std_logic;     -- Enable output of lower 20 bits
  signal error_fedge_i : std_logic;     -- Filtered error edge
  signal error_i       : std_logic;     -- Filtering
  signal error_ii      : std_logic;     -- Filtering
  signal error_iii     : std_logic;     -- Filtering

begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    dout_i        <= evl_addr & "0000" & dout;
    en_addr_i     <= rmtrsf or alevl_rdtrx;
    en_data_i     <= rmtrsf or bc_dolo_en;
    al_errorb     <= not (not bc_error and error_fedge_i);
    error_fedge_i <= not error_ii and error_iii;  -- Edge detect. 
  end block combi;

  -- purpose: ALTRO error glitch and metastability filter
  -- type   : sequential
  -- inputs : clk, rstb, errorb
  -- outputs: error_fedge
  glitch_meta_filter : process (clk, rstb) is
  begin  -- process glitch_meta_filter
    if rstb = '0' then                  -- asynchronous reset (active low)
      error_i   <= '0';
      error_ii  <= '0';
      error_iii <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      error_i   <= errorb;              -- Register
      error_ii  <= errorb or error_i;   -- Glitch filter
      error_iii <= error_ii;            -- Meta stability
    end if;
  end process glitch_meta_filter;

  -- purpose: Bus tri-states
  bus_tri : block is
  begin  -- block bus_tri
    bd(39 downto 20)  <= dout_mux_i(39 downto 20) when
                         en_addr_i = '1' else (others => 'Z');
    bd(19 downto 0)   <= dout_mux_i(19 downto 0)  when
                         en_data_i = '1' else (others => 'Z');
    din(39 downto 20) <= bd(39 downto 20)         when
                         en_addr_i = '0' else (others => 'Z');
    din(19 downto 0)  <= bd(19 downto 0)          when
                         en_data_i = '0' else (others => 'Z');

    acknb  <= not bc_ackn when bc_ackn_en = '1' else 'Z';
    errorb <= '0'         when bc_error = '1'   else 'Z';

    -- Open collectors for TSM and EVL
    trsfb  <= not rmtrsf   when rmtrsf_en = '1'   else 'Z';
    dstbb  <= not rmdstb   when rmtrsf_en = '1'   else 'Z';
    writeb <= '1'          when alevl_rdtrx = '1' else 'Z';
    cstbb  <= not evl_cstb when alevl_rdtrx = '1' else 'Z';
  end block bus_tri;

  --  Select output 
  mux : entity work.mux_bus
    port map (
      data_in_1 => rmdata_out,          -- in  Input 1
      data_in_0 => dout_i,              -- in  Input 0
      sel       => rmtrsf_en,           -- in  Select 1
      result    => dout_mux_i);         -- out output

end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package signals_driver_pack is
  component signals_driver
    port (
      clk         : in    std_logic;    -- Clock
      rstb        : in    std_logic;    -- Async reset
      dout        : in    std_logic_vector(15 downto 0);  -- Bus: Output
      bc_dolo_en  : in    std_logic;    -- Bus: Data enable
      bc_ackn     : in    std_logic;    -- Bus: Acknowledge from BC
      bc_ackn_en  : in    std_logic;    -- Bus: Acknowledge enable
      bc_error    : in    std_logic;    -- Bus: Error
      evl_addr    : in    std_logic_vector(39 downto 20);  -- EVL: Address
      alevl_rdtrx : in    std_logic;    -- EVL: read transmit
      evl_cstb    : in    std_logic;    -- EVL: Control strobe
      rmdata_out  : in    std_logic_vector(39 downto 0);  -- EVL/TSM output 
      rmtrsf      : in    std_logic;    -- EVL/TSM: Transfer
      rmtrsf_en   : in    std_logic;    -- EVL/TSM: Enable transfer
      rmdstb      : in    std_logic;    -- EVL/TSM: Data strobe
      din         : out   std_logic_vector(39 downto 0);  -- Bus: data in
      al_errorb   : out   std_logic;    -- Bus: Altro error
      bd          : inout std_logic_vector(39 downto 0);  -- Bus
      errorb      : inout std_logic;    -- Bus: Error 
      acknb       : inout std_logic;    -- Bus: Acknowledge
      trsfb       : inout std_logic;    -- Bus: Transfer
      dstbb       : inout std_logic;    -- Bus: Data strobe
      writeb      : inout std_logic;    -- Bus: Write
      cstbb       : inout std_logic);   -- Bus : Control strobe
  end component signals_driver;
end package signals_driver_pack;
------------------------------------------------------------------------------
--
-- EOF
--
