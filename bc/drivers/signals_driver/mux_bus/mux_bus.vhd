------------------------------------------------------------------------------
-- Title      : Multiplexer of bus signals
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : mux_bus.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity mux_bus is
  port (
    data_in_1 : in  std_logic_vector(39 downto 0);   -- Input 1
    data_in_0 : in  std_logic_vector(39 downto 0);   -- Input 0
    sel       : in  std_logic;                       -- Select 1
    result    : out std_logic_vector(39 downto 0));  -- output
end entity mux_bus;

architecture rtl of mux_bus is
begin  -- architecture rtl
  -- purpose: Select outout
  combi: block is
  begin  -- block combi
    result <= data_in_1 when sel = '1' else data_in_0;
  end block combi;
end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package mux_bus_pack is
  component mux_bus
    port (
      data_in_1 : in  std_logic_vector(39 downto 0);   -- Input 1
      data_in_0 : in  std_logic_vector(39 downto 0);   -- Input 0
      sel       : in  std_logic;                       -- Select 1
      result    : out std_logic_vector(39 downto 0));  -- output
  end component mux_bus;
end package mux_bus_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
