------------------------------------------------------------------------------
-- Title      : I/O Drivers of signals (pins)
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : drivers.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.signals_driver_pack.all;
use work.transceivers_driver_pack.all;

entity drivers is
  port (
    clk            : in    std_logic;   -- Clock
    rstb           : in    std_logic;   -- Async reset
    tsm_on         : in    std_logic;   -- TSM: on
    al_cs          : in    std_logic;   -- ALTRO card select
    bc_cs          : in    std_logic;   -- BC card select
    cstb           : in    std_logic;   -- Bus: Control strobe
    wr_al          : in    std_logic;   -- Bus: Write
    al_dolo_en     : in    std_logic;   -- Bus: ALTRO data enable
    bc_dolo_en     : in    std_logic;   -- Bus: BC data enable
    al_trsf_en     : in    std_logic;   -- Bus: ALTRO transfer enable
    bc_master      : in    std_logic;   -- Bus: BC is local master
    card_isolation : in    std_logic;   -- Bus: Card is isolated
    bc_ackn        : in    std_logic;   -- Bus: Acknowledge from BC
    bc_ack_en      : in    std_logic;   -- Bus: Acknowledge from BC enable
    bc_error       : in    std_logic;   -- Bus: Error
    dout           : in    std_logic_vector(15 downto 0);  -- Bus: data out
    alevl_rdtrx    : in    std_logic;   -- EVL: Transmit
    evl_cstb       : in    std_logic;   -- EVL: Control strobe
    evl_addr       : in    std_logic_vector(39 downto 20);  -- EVL: address out
    rmtrsf_en      : in    std_logic;   -- TSM/EVL: Transfer enable
    ackn_chrdo     : in    std_logic;   -- TSM/EVL: Acknowledge
    rmtrsf         : in    std_logic;   -- TSM/EVL: transfer
    rmdstb         : in    std_logic;   -- TSM/EVL: Data strob
    rmdata_out     : in    std_logic_vector(39 downto 0);  -- TSM/EVL: data out
    altro_sw       : in    std_logic;   -- HW: ALTRO switch
    sda_out        : in    std_logic;   -- I2C: serial data out
    oeba_l         : out   std_logic;   -- GTL: in (low bits)
    oeab_l         : out   std_logic;   -- GTL: out (low bits)
    oeba_h         : out   std_logic;   -- GTL: in (high bits)
    oeab_h         : out   std_logic;   -- GTL: out (high bits)
    read_data      : out   std_logic;   -- Read data 
    ric            : out   std_logic;   -- ???
    ctr_in         : out   std_logic;   -- Control in
    ctr_out        : out   std_logic;   -- Control out
    al_errorb      : out   std_logic;   -- ALTRO error
    din            : out   std_logic_vector(39 downto 0);  -- Bus: Data input
    bd             : inout std_logic_vector(39 downto 0);  -- Bus: tri-state 
    acknb          : inout std_logic;   -- Ctrl: Acknowledge tri-state
    errorb         : inout std_logic;   -- Ctrl: Control strobe tri-state
    trsfb          : inout std_logic;   -- Ctrl: Transfer tri-state
    dstbb          : inout std_logic;   -- Ctrl: Data strobe tri-state
    writeb         : inout std_logic;   -- Ctrl: Write tri-state
    cstbb          : inout std_logic);  -- Ctrl: Control strobe tri-state
end entity drivers;

architecture rtl of drivers is

begin  -- architecture rtl

  transcievers : entity work.transceivers_driver
    port map (
        clk            => clk,               -- in  Clock
        rstb           => rstb,              -- in  Async reset
        tsm_on         => tsm_on,            -- in  TSM: on
        al_cs          => al_cs,             -- in  ALTRO card select
        bc_cs          => bc_cs,             -- in  BC card select
        cstb           => cstb,              -- in  Control strobe
        wr_al          => wr_al,             -- in  Bus: Write
        al_dolo_en     => al_dolo_en,        -- in  Bus: ALTRO data enable
        bc_dolo_en     => bc_dolo_en,        -- in  Bus: BC data enable
        al_trsf_en     => al_trsf_en,        -- in  Bus: ALTRO transfer enable
        card_isolation => card_isolation,    -- in  Bus: Card is isolated
        bc_master      => bc_master,         -- in  Bus: BC is local master
        rmtrsf_en      => rmtrsf_en,         -- in  TSM/EVL: Transfer enable
        ackn_chrdo     => ackn_chrdo,        -- in  TSM/EVL: Acknowledge
        altro_sw       => altro_sw,          -- in  HW: ALTRO switch
        sda_out        => sda_out,           -- in  I2C: Out data
        icode          => bd(24 downto 20),  -- in  Instruction
        oeba_l         => oeba_l,            -- out GTL: Enable out low bits
        oeab_l         => oeab_l,            -- out GTL: Enable in low bits
        oeba_h         => oeba_h,            -- out GTL: Enable out high bits
        oeab_h         => oeab_h,            -- out GTL: Enable in high bits
        read_data      => read_data,         -- out Read data
        ric            => ric,               -- out ???
        ctr_in         => ctr_in,            -- out Slow control in
        ctr_out        => ctr_out);          -- out Slow control out

  signals : entity work.signals_driver
    port map (
        clk         => clk,             -- in  Clock
        rstb        => rstb,            -- in  Async reset
        dout        => dout,            -- in  Bus: Output
        bc_dolo_en  => bc_dolo_en,      -- in  Bus: Data enable
        bc_ackn     => bc_ackn,         -- in  Bus: Acknowledge from BC
        bc_ackn_en  => bc_ack_en,       -- in  Bus: Acknowledge enable
        bc_error    => bc_error,        -- in  Bus: Error
        evl_addr    => evl_addr,        -- in  EVL: Address
        alevl_rdtrx => alevl_rdtrx,     -- in  EVL: read transmit
        evl_cstb    => evl_cstb,        -- in  EVL: Control strobe
        rmdata_out  => rmdata_out,      -- in  EVL/TSM output data
        rmtrsf      => rmtrsf,          -- in  EVL/TSM: Transfer
        rmtrsf_en   => rmtrsf_en,       -- in  EVL/TSM: Enable transfer
        rmdstb      => rmdstb,          -- in  EVL/TSM: Data strobe
        din         => din,             -- out Bus: data in
        al_errorb   => al_errorb,       -- out Bus: Altro error
        bd          => bd,              -- inout Bus
        errorb      => errorb,          -- inout Bus: Error 
        acknb       => acknb,           -- inout Bus: Acknowledge
        trsfb       => trsfb,           -- inout Bus: Transfer
        dstbb       => dstbb,           -- inout Bus: Data strobe
        writeb      => writeb,          -- inout Bus: Write
        cstbb       => cstbb);          -- inout Bus : Control strobe

end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package drivers_pack is
  component drivers
    port (
      clk            : in    std_logic;  -- Clock
      rstb           : in    std_logic;  -- Async reset
      tsm_on         : in    std_logic;  -- TSM: on
      al_cs          : in    std_logic;  -- ALTRO card select
      bc_cs          : in    std_logic;  -- BC card select
      cstb           : in    std_logic;  -- Bus: Control strobe
      wr_al          : in    std_logic;  -- Bus: Write
      al_dolo_en     : in    std_logic;  -- Bus: ALTRO data enable
      bc_dolo_en     : in    std_logic;  -- Bus: BC data enable
      al_trsf_en     : in    std_logic;  -- Bus: ALTRO transfer enable
      bc_master      : in    std_logic;  -- Bus: BC is local master
      card_isolation : in    std_logic;  -- Bus: Card is isolated
      bc_ackn        : in    std_logic;  -- Bus: Acknowledge from BC
      bc_ack_en      : in    std_logic;  -- Bus: Acknowledge from BC enable
      bc_error       : in    std_logic;  -- Bus: Error
      dout           : in    std_logic_vector(15 downto 0);  -- Bus: data out
      alevl_rdtrx    : in    std_logic;  -- EVL: Transmit
      evl_cstb       : in    std_logic;  -- EVL: Control strobe
      evl_addr       : in    std_logic_vector(39 downto 20);  -- EVL: address
      rmtrsf_en      : in    std_logic;  -- TSM/EVL: Transfer enable
      ackn_chrdo     : in    std_logic;  -- TSM/EVL: Acknowledge
      rmtrsf         : in    std_logic;  -- TSM/EVL: transfer
      rmdstb         : in    std_logic;  -- TSM/EVL: Data strob
      rmdata_out     : in    std_logic_vector(39 downto 0);  -- TSM/EVL: data
      altro_sw       : in    std_logic;  -- HW: ALTRO switch
      sda_out        : in    std_logic;  -- I2C: serial data out
      oeba_l         : out   std_logic;  -- GTL: in (low bits)
      oeab_l         : out   std_logic;  -- GTL: out (low bits)
      oeba_h         : out   std_logic;  -- GTL: in (high bits)
      oeab_h         : out   std_logic;  -- GTL: out (high bits)
      read_data      : out   std_logic;  -- Read data 
      ric            : out   std_logic;  -- ???
      ctr_in         : out   std_logic;  -- Control in
      ctr_out        : out   std_logic;  -- Control out
      al_errorb      : out   std_logic;  -- ALTRO error
      din            : out   std_logic_vector(39 downto 0);  -- Bus: Data input
      bd             : inout std_logic_vector(39 downto 0);  -- Bus: tri-state 
      acknb          : inout std_logic;  -- Ctrl: Acknowledge tri-state
      errorb         : inout std_logic;  -- Ctrl: Control strobe tri-state
      trsfb          : inout std_logic;  -- Ctrl: Transfer tri-state
      dstbb          : inout std_logic;  -- Ctrl: Data strobe tri-state
      writeb         : inout std_logic;  -- Ctrl: Write tri-state
      cstbb          : inout std_logic);  -- Ctrl : Control strobe tri-state
  end component drivers;
end package drivers_pack;
------------------------------------------------------------------------------
--
-- EOF
--
