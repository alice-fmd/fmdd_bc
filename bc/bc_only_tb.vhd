------------------------------------------------------------------------------
-- Title      : Test bench of Board Controller code
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : bc_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/29
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/25  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library rcu_model;
use rcu_model.rcu_pack.all;

library ad7417_model;
use ad7417_model.ad7417_pack.all;

use work.bc_only_pack.all;

------------------------------------------------------------------------------
entity bc_only_tb is
end entity bc_only_tb;

------------------------------------------------------------------------------
architecture test of bc_only_tb is
  constant HADD : std_logic_vector(4 downto 0) := "00000";

  signal clk_i         : std_logic;     -- Clock
  signal rstb_i        : std_logic;     -- Async reset
  signal debug_sw_i    : std_logic := '1';  -- Debug switch (should be high)
  signal sclk_i        : std_logic;     -- Sample clock (10 MHz)
  signal scl_i         : std_logic := 'H';  -- I2C: Serial clock (<= 5MHz)
  signal sda_in_i      : std_logic := 'H';  -- I2C: data input
  signal otib_i        : std_logic := '0';  -- Not used
  signal alps_errorb_i : std_logic := '1';  -- ALTRO power supply error (-)
  signal l0_i          : std_logic;     -- Level 0 trigger
  signal l1b_i         : std_logic;     -- Level 1 trigger
  signal l2b_i         : std_logic;     -- Level 2 trigger
  signal al_dolo_enb_i : std_logic := '1';  -- ALTRO data out enable (-)
  signal al_trsf_enb_i : std_logic := '1';  -- ALTRO transfer enable (-)
  signal paps_errorb_i : std_logic := '1';  -- PASA power supply error (-)
  signal mscl_i        : std_logic := 'H';  -- ADC: clock
  signal msda_i        : std_logic := 'H';  -- ADC: data in/out
  signal bc_int_i      : std_logic;     -- CTL: Interrupt
  signal altro_sw_i    : std_logic;     -- ALTRO switch
  signal pasa_sw_i     : std_logic;     -- PASA switch
  signal rdoclk_en_i   : std_logic;     -- TSM: Readout clock enable
  signal adcclk_en_i   : std_logic;     -- TSM: ADC clock enable
  signal adc_add0_i    : std_logic;     -- ADC address bit 0
  signal adc_add1_i    : std_logic;     -- ADC address bit 1
  signal test_a_i      : std_logic;     -- TSM: mask a
  signal test_b_i      : std_logic;     -- TSM: mask b
  signal test_c_i      : std_logic;     -- TSM: mask c
  signal test_d_i      : std_logic;     -- TSM: mask d
  signal test_e_i      : std_logic;     -- TSM: mask e
  signal test_f_i      : std_logic;     -- TSM: mask f
  signal test_g_i      : std_logic;     -- TSM: mask g
  signal test_h_i      : std_logic;     -- TSM: mask h
  signal sda_out_i     : std_logic;     -- I2C: data out
  signal rst_fbc_i     : std_logic;     -- Reset front-end card
  signal bcout_ad_i    : std_logic_vector(4 downto 0) := (others => '0');
  signal oeba_l_i      : std_logic;     -- GTL: enable in low bits
  signal oeab_l_i      : std_logic;     -- GTL: enable out low bits
  signal oeba_h_i      : std_logic;     -- GTL: enable in high bits
  signal oeab_h_i      : std_logic;     -- GTL: enable out high bits
  signal ctr_in_i      : std_logic;     -- GTL: enable ctrl bus in
  signal ctr_out_i     : std_logic;     -- GTL: enable ctrl bus out
  signal bd_i          : std_logic_vector(39 downto 0) := (others => '0');
  signal acknb_i       : std_logic := 'H';  -- CTL: Acknowledge
  signal errorb_i      : std_logic := 'H';  -- CTL: Error
  signal trsfb_i       : std_logic := 'H';  -- CTL: Transfer
  signal dstbb_i       : std_logic := 'H';  -- CTL: Data strobe
  signal writeb_i      : std_logic := 'H';  -- CTL: Write 
  signal cstbb_i       : std_logic := 'H';  -- Control strobe
  signal mcst_i        : std_logic;     -- ADC : Montor start disabled='0';
  signal tp_spare0_i   : std_logic;     -- Test pin 0
  signal tp_spare2_i   : std_logic;     -- Test pin 1
  signal tp_spare3_i   : std_logic;     -- Test pin 3
  signal tp_spare4_i   : std_logic;     -- Test pin 4
  signal tp_spare5_i   : std_logic;     -- Test pin 5

begin  -- architecture test
  -- purpose: Pull up serial lines
  pull_up : block
  begin  -- block pull_up
    mscl_i   <= 'H';
    msda_i   <= 'H';
    errorb_i <= 'H';
  end block pull_up;

  dut : entity work.bc_only
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        debug_sw    => debug_sw_i,      -- in  Debug switch (should be high)
        hadd        => HADD,            -- in  Card address
        sclk        => sclk_i,          -- in  Sample clock (10 MHz)
        scl         => scl_i,           -- in  I2C: Serial clock (<= 5MHz)
        sda_in      => sda_in_i,        -- in  I2C: data input
        otib        => otib_i,          -- in  Not used
        alps_errorb => alps_errorb_i,   -- in  ALTRO power supply error
        l1b         => l1b_i,           -- in  Level 1 trigger
        l2b         => l2b_i,           -- in  Level 2 trigger
        al_dolo_enb => al_dolo_enb_i,   -- in  ALTRO data out enable
        al_trsf_enb => al_trsf_enb_i,   -- in  ALTRO transfer enable
        paps_errorb => paps_errorb_i,   -- in  PASA power supply error
        mscl        => mscl_i,          -- out ADC: clock
        msda        => msda_i,          -- inout ADC: data in/out
        bc_int      => bc_int_i,        -- out CTL: Interrupt
        altro_sw    => altro_sw_i,      -- out ALTRO switch
        pasa_sw     => pasa_sw_i,       -- out PASA switch
        rdoclk_en   => rdoclk_en_i,     -- out TSM: Readout clock enable
        adcclk_en   => adcclk_en_i,     -- out TSM: ADC clock enable
        adc_add0    => adc_add0_i,      -- out ADC address bit 0
        adc_add1    => adc_add1_i,      -- out ADC address bit 1
        test_a      => test_a_i,        -- out TSM: mask a
        test_b      => test_b_i,        -- out TSM: mask b
        test_c      => test_c_i,        -- out TSM: mask c
        test_d      => test_d_i,        -- out TSM: mask d
        test_e      => test_e_i,        -- out TSM: mask e
        test_f      => test_f_i,        -- out TSM: mask f
        test_g      => test_g_i,        -- out TSM: mask g
        test_h      => test_h_i,        -- out TSM: mask h
        sda_out     => sda_out_i,       -- out I2C: data out
        rst_fbc     => rst_fbc_i,       -- out Reset front-end card
        bcout_ad    => bcout_ad_i,      -- out Out board address
        oeba_l      => oeba_l_i,        -- out GTL: enable in low bits
        oeab_l      => oeab_l_i,        -- out GTL: enable out low bits
        oeba_h      => oeba_h_i,        -- out GTL: enable in high bits
        oeab_h      => oeab_h_i,        -- out GTL: enable out high bits
        ctr_in      => ctr_in_i,        -- out GTL: enable ctrl bus in
        ctr_out     => ctr_out_i,       -- out GTL: enable ctrl bus out
        bd          => bd_i,            -- inout Bus: 40 bits
        acknb       => acknb_i,         -- inout CTL: Acknowledge
        errorb      => errorb_i,        -- inout CTL: Error
        trsfb       => trsfb_i,         -- inout CTL: Transfer
        dstbb       => dstbb_i,         -- inout CTL: Data strobe
        writeb      => writeb_i,        -- inout CTL: Write 
        cstbb       => cstbb_i,         -- inout Control strobe
        mcst        => mcst_i,          -- out ADC : Montor start disabled='0';
        tp_spare0   => tp_spare0_i,     -- out Test pin 0
        tp_spare2   => tp_spare2_i,     -- out Test pin 1
        tp_spare3   => tp_spare3_i,     -- out Test pin 3
        tp_spare4   => tp_spare4_i,     -- out Test pin 4
        tp_spare5   => tp_spare5_i);    -- out Test pin 5


  rcu_i : entity rcu_model.rcu(reg_stim)
    generic map (
        HADD  => HADD)                  -- Hardware address
    port map (
        bd    => bd_i,                  -- inout Bus
        write => writeb_i,              -- out Write strobe
        cstb  => cstbb_i,               -- out Control strobe
        ackn  => acknb_i,               -- in  Acknowledge
        eror  => errorb_i,              -- in  Error
        trsf  => trsfb_i,               -- in  Transfer pulse from ALTRO's
        dstb  => dstbb_i,               -- in  Data strobe from ALTROs
        lvl0  => l0_i,                  -- out Strictly not from the rcu
        lvl1  => l1b_i,                 -- out Level 1 trigger (active low)
        lvl2  => l2b_i,                 -- out Level 2 trigger (active low)
        grst  => rstb_i,                -- out Global reset
        sclk  => sclk_i,                -- out Sample clock (10 MHz)
        rclk  => clk_i,                 -- out Normal clock (40 MHz)
        scl   => scl_i);                -- out Serial clock (5 MHz)

  monitor : entity ad7417_model.ad7417
    generic map (
        VENDOR => "0101",               -- Vendor ID
        ADD    => "000",                -- Address
        PERIOD => 6.5 us,               -- Period of clk
        T      => X"dead",              -- T value
        ADC1   => X"beaf",              -- ADC1 value
        ADC2   => X"1111",              -- ADC2 value
        ADC3   => X"FFFF",              -- ADC3 value
        ADC4   => X"5555")              -- ADC4 value
    port map (
        scl    => mscl_i,               -- inout I2C Clock
        sda    => msda_i);              -- inout I2C data

  -- purpose: Report progress
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  progress: process is
  begin  -- process progress
    wait for 10 us;
    report "Time is now " & time'image(NOW) severity note;
  end process progress;
end architecture test;

------------------------------------------------------------------------------
