------------------------------------------------------------------------------
-- Title      : Test of top-level entity
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : bc_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-03-09
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/28  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library rcu_model;
use rcu_model.rcu_pack.all;

library ad7417_model;
use ad7417_model.ad7417_pack.all;

use work.bc_pack.all;

------------------------------------------------------------------------------
entity bc_tb is
end entity bc_tb;

------------------------------------------------------------------------------
architecture test of bc_tb is
  constant HADD : std_logic_vector(4 downto 0) := "00000";

  signal clk_i           : std_logic;   -- Clock
  signal rstb_i          : std_logic;   -- Async reset
  signal sclk_i          : std_logic;   -- Sample clock (10 MHz)
  signal scl_i           : std_logic;   -- I2C: Serial clock (<= 5MHz)
  signal sda_in_i        : std_logic;   -- I2C: data input
  signal sda_out_i       : std_logic;   -- I2C: data out
  signal l0_i            : std_logic;   -- Level 0 trigger 
  signal l1b_i           : std_logic;   -- Level 1 trigger
  signal l2b_i           : std_logic;   -- Level 2 trigger
  signal mscl_i          : std_logic;   -- ADC: clock
  signal msda_i          : std_logic;   -- ADC: data in/out
  signal pasa_sw_i       : std_logic;   -- PASA switch
  signal paps_errorb_i   : std_logic := '1';  -- PASA power supply error
  signal rdoclk_en_i     : std_logic;   -- TSM: Readout clock enable
  signal adcclk_en_i     : std_logic;   -- TSM: ADC clock enable
  signal adc_add0_i      : std_logic;   -- ADC address bit 0
  signal adc_add1_i      : std_logic;   -- ADC address bit 1
  signal test_a_i        : std_logic;   -- TSM: mask a
  signal test_b_i        : std_logic;   -- TSM: mask b
  signal test_c_i        : std_logic;   -- TSM: mask c
  signal rst_fbc_i       : std_logic;   -- Reset front-end card
  signal hadd_i          : std_logic_vector(4 downto 0);  -- Card address
  signal bcout_ad_i      : std_logic_vector(4 downto 0);  -- Out board address
  signal oeba_l_i        : std_logic;   -- GTL: enable in low bits
  signal oeab_l_i        : std_logic;   -- GTL: enable out low bits
  signal oeba_h_i        : std_logic;   -- GTL: enable in high bits
  signal oeab_h_i        : std_logic;   -- GTL: enable out high bits
  signal ctr_in_i        : std_logic;   -- GTL: enable ctrl bus in
  signal ctr_out_i       : std_logic;   -- GTL: enable ctrl bus out
  signal bd_i            : std_logic_vector(39 downto 0);  -- Bus: 40 bits
  signal acknb_i         : std_logic;   -- CTL: Acknowledge
  signal errorb_i        : std_logic;   -- CTL: Error
  signal trsfb_i         : std_logic;   -- CTL: Transfer
  signal dstbb_i         : std_logic;   -- CTL: Data strobe
  signal writeb_i        : std_logic;   -- CTL: Write 
  signal cstbb_i         : std_logic;   -- Control strobe
  signal bc_int_i        : std_logic;   -- CTL: Interrupt
  signal mcst_i          : std_logic;   -- ADC : Montor start disabled='0';
  signal altro_sw_i      : std_logic;   -- ALTRO switch
  signal sample_clk_i    : std_logic_vector(2 downto 0);  -- Sample clock 
  signal altro_l1_i      : std_logic;   -- L1 for ALTROs (active low)
  signal altro_l2_i      : std_logic;   -- L2 for ALTROs (active low)
  signal alps_errorb_i   : std_logic := '1';  -- ALTRO power supply error
  signal al_dolo_enb_i   : std_logic := 'H';  -- ALTRO data out enable
  signal al_trsf_enb_i   : std_logic := 'H';  -- ALTRO transfer enable
  signal digital_reset_i : std_logic;   -- Reset VA1's
  signal test_on_i       : std_logic;   -- Calibration mode on.
  signal pulser_enable_i : std_logic;   -- Enable step on pulser
  signal shift_in_i      : std_logic_vector(2 downto 0);  -- shift reset
  signal shift_clk_i     : std_logic_vector(2 downto 0);  -- shift register
  signal hold_i          : std_logic_vector(2 downto 0);  -- Hold charge
  signal dac_addr_i      : std_logic_vector (11 downto 0);  -- DAC address
  signal dac_data_i      : std_logic_vector (7 downto 0);  -- DAC data
  signal busy_i          : std_logic;   -- busy (LED on test)
  signal debug_i         : std_logic_vector(14 downto 0);  -- Debug pins

  -- purpose: convert integer to std_logic_vector
  function int2slv (
    constant x   : integer;             -- Value
    constant l   : natural)             -- Size
    return std_logic_vector is
    variable ret : std_logic_vector(l-1 downto 0);
  begin  -- function int2slv
    ret := std_logic_vector(to_unsigned(x, l));
    return ret;
  end function int2slv;

  -- purpose: Convert std_logic_vector to a string
  function slv2str (
    constant v   : std_logic_vector)
    return string
  is
    variable ret : string(v'length downto 1);
    variable i   : integer;
  begin  -- function slv2str
    ret := (others => 'X');

    for i in v'range loop
      case v(i) is
        when 'U'    => ret(i+1) := 'U';
        when 'X'    => ret(i+1) := 'X';
        when '0'    => ret(i+1) := '0';
        when '1'    => ret(i+1) := '1';
        when 'Z'    => ret(i+1) := 'Z';
        when 'W'    => ret(i+1) := 'W';
        when 'L'    => ret(i+1) := 'L';
        when 'H'    => ret(i+1) := 'H';
        when '-'    => ret(i+1) := '-';
        when others => ret(i+1) := 'X';
      end case;
    end loop;  -- i
    return ret;
  end function slv2str;

  -- purpose: Convert std_logic_vector to integer
  function slv2int (constant v : std_logic_vector) return integer is
  begin
    return to_integer(unsigned(v));
  end function slv2int;
begin  -- architecture test
  -- purpose: Pull up serial lines
  pull_up : block
  begin  -- block pull_up
    mscl_i        <= 'H';
    msda_i        <= 'H';
    errorb_i      <= 'H';
    al_dolo_enb_i <= 'H';               -- ALTRO data out enable
    al_trsf_enb_i <= 'H';
  end block pull_up;

  dut : entity work.bc
    port map (
      clk           => clk_i,            -- in  Clock                        
      rstb          => rstb_i,           -- in  Async reset
      sclk          => sclk_i,           -- in  Sample clock (10 MHz)
      -- Serial interface
      scl           => scl_i,            -- in  I2C: Serial clock (<= 5MHz)
      sda_in        => sda_in_i,         -- in  I2C: data input
      sda_out       => sda_out_i,        -- out I2C: data out
      -- Triggers
      l0            => l0_i,             -- in  Level 0 trigger 
      l1b           => l1b_i,            -- in  Level 1 trigger
      l2b           => l2b_i,            -- in  Level 2 trigger
      -- Monitor interface
      mscl          => mscl_i,           -- out ADC: clock
      msda          => msda_i,           -- inout ADC: data in/out
      -- Pasa
      pasa_sw       => pasa_sw_i,        -- out PASA switch
      paps_errorb   => paps_errorb_i,    -- in  PASA power supply error
      -- Test mode
      rdoclk_en     => rdoclk_en_i,      -- out TSM: Readout clock enable
      adcclk_en     => adcclk_en_i,      -- out TSM: ADC clock enable
      adc_add0      => adc_add0_i,       -- out ADC address bit 0
      adc_add1      => adc_add1_i,       -- out ADC address bit 1
      test_a        => test_a_i,         -- out TSM: mask a
      test_b        => test_b_i,         -- out TSM: mask b
      test_c        => test_c_i,         -- out TSM: mask c
      -- Hardware stuff
      rst_fbc       => rst_fbc_i,        -- out Reset front-end card
      hadd          => HADD,             -- in  Card address
      bcout_ad      => bcout_ad_i,       -- out Out board address
      -- GTL enable on/off
      oeba_l        => oeba_l_i,         -- out GTL: enable in low bits
      oeab_l        => oeab_l_i,         -- out GTL: enable out low bits
      oeba_h        => oeba_h_i,         -- out GTL: enable in high bits
      oeab_h        => oeab_h_i,         -- out GTL: enable out high bits
      ctr_in        => ctr_in_i,         -- out GTL: enable ctrl bus in
      ctr_out       => ctr_out_i,        -- out GTL: enable ctrl bus out
      -- Bus
      bd            => bd_i,             -- inout Bus: 40 bits
      -- Control bus
      acknb         => acknb_i,          -- inout CTL: Acknowledge
      errorb        => errorb_i,         -- inout CTL: Error
      trsfb         => trsfb_i,          -- inout CTL: Transfer
      dstbb         => dstbb_i,          -- inout CTL: Data strobe
      writeb        => writeb_i,         -- inout CTL: Write 
      cstbb         => cstbb_i,          -- inout Control strobe
      bc_int        => bc_int_i,         -- out CTL: Interrupt
      -- Misc
      otib          => '1',              -- Not used 
      mcst          => mcst_i,           -- out ADC : start disabled='0';
      clk10         => '1',              -- Not used
      debug_on      => '1',              -- Not used
      -- ALTRO
      altro_sw      => altro_sw_i,       -- out ALTRO switch
      sample_clk    => sample_clk_i,     -- out Sample clock 
      altro_l1      => altro_l1_i,       -- out L1 for ALTROs (active low)
      altro_l2      => altro_l2_i,       -- out L2 for ALTROs (active low)
      alps_errorb   => alps_errorb_i,    -- in  ALTRO power supply error
      al_dolo_enb   => al_dolo_enb_i,    -- in  ALTRO data out enable
      al_trsf_enb   => al_trsf_enb_i,    -- in  ALTRO transfer enable
      al_ackn_enb   => '1',              -- Not used 
      -- VA1 
      digital_reset => digital_reset_i,  -- out Reset VA1's
      test_on       => test_on_i,        -- out Calibration mode on.
      pulser_enable => pulser_enable_i,  -- out Enable step on pulser
      shift_in      => shift_in_i,       -- out shift reset
      shift_clk     => shift_clk_i,      -- out shift register
      hold          => hold_i,           -- out Hold charge
      -- DAC interface
      dac_addr      => dac_addr_i,       -- out DAC address
      dac_data      => dac_data_i,       -- out DAC data
      -- L0 interface
      busy          => busy_i,           -- out busy (LED on test)
      -- Debug
      debug         => debug_i);         -- out Debug pins

  rcu_i : rcu
    generic map (
      HADD => HADD)                     -- Hardware address
    port map (
      bd        => bd_i,                -- inout Bus
      writ      => writeb_i,            -- out Write strobe
      cstb      => cstbb_i,             -- out Control strobe
      ackn      => acknb_i,             -- in  Acknowledge
      eror      => errorb_i,            -- in  Error
      trsf      => trsfb_i,             -- in  Transfer pulse from ALTRO's
      dstb      => dstbb_i,             -- in  Data strobe from ALTROs
      busy      => busy_i,              -- in  Busy from FECs
      lvl0      => l0_i,                -- out Strictly not from the rcu
      lvl1      => l1b_i,               -- out Level 1 trigger (active low)
      lvl2      => l2b_i,               -- out Level 2 trigger (active low)
      grst      => rstb_i,              -- out Global reset
      sclk      => sclk_i,              -- out Sample clock (10 MHz)
      rclk      => clk_i,               -- out Normal clock (40 MHz)
      scl_a     => scl_i,               -- out Serial clock (5 MHz)
      scl_b     => open,
      sda_in_a  => sda_out_i,
      sda_in_b  => 'H',
      sda_out_a => sda_in_i,
      sda_out_b => open);

  mons      : for i in 0 to 3 generate
  begin  -- generate mons
    adc : entity ad7417_model.ad7417
    generic map (
      VENDOR => "0101",                  -- Vendor ID
      ADD    => int2slv(i, 3),           -- Address
      PERIOD => 6.5 us,                  -- Period of clk
      T      => to_unsigned(5*i+1, 16),  -- T value
      ADC1   => to_unsigned(5*i+2, 16),  -- ADC1 value
      ADC2   => to_unsigned(5*i+3, 16),  -- ADC2 value
      ADC3   => to_unsigned(5*i+4, 16),  -- ADC3 value
      ADC4   => to_unsigned(5*i+5, 16))  -- ADC4 value
    port map (
      scl    => mscl_i,                  -- inout I2C Clock
      sda    => msda_i);                 -- inout I2C data
  end generate mons;

  -- purpose: Report progress
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  progress: process is
  begin  -- process progress
    wait for 10 us;
    report "Time is now " & time'image(NOW) severity note;
  end process progress;  
end architecture test;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_pipe of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu 
      use entity rcu_model.rcu(pipe_stim);
    end for;
  end for;
end configuration bc_pipe;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_reg of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(reg_stim);
    end for;
  end for;
end configuration bc_reg;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_cnv of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(cnv_stim);
    end for;
  end for;
end configuration bc_cnv;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_trig of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(trig_stim);
    end for;
  end for;
end configuration bc_trig;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_busy of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(busy_stim);
    end for;
  end for;
end configuration bc_busy;

------------------------------------------------------------------------------
--library rcu_model;
--use rcu_model.rcu_pack.all;

--configuration bc_ctp of bc_tb is
--  use work.all;
--  for test
--    for rcu_i : rcu
--      use entity rcu_model.rcu(ctp_stim);
--    end for;
--  end for;
--end configuration bc_ctp;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_l0 of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(l0_stim);
    end for;
  end for;
end configuration bc_l0;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_i2c of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(i2c_stim);
    end for;
  end for;
end configuration bc_i2c;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_i2c2 of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(i2c2_stim);
    end for;
  end for;
end configuration bc_i2c2;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_i2c3 of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(i2c3_stim);
    end for;
  end for;
end configuration bc_i2c3;

------------------------------------------------------------------------------
library rcu_model;
use rcu_model.rcu_pack.all;

configuration bc_cal of bc_tb is
  use work.all;
  for test
    for rcu_i : rcu
      use entity rcu_model.rcu(cal_stim);
    end for;
  end for;
end configuration bc_cal;

------------------------------------------------------------------------------
-- 
-- EOF
--
