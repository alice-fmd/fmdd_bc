-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.protect_veto_pack.all;

entity protect_veto_tb is
end protect_veto_tb;

-------------------------------------------------------------------------------
architecture test of protect_veto_tb is
  -- component generics
  constant WIDTH : natural := 16;

  -- component ports
  signal clk_i      : std_logic                          := '0';  -- [in]
  signal rstb_i     : std_logic                          := '0';  -- [in]
  signal delay_i    : std_logic_vector(15 downto 0) := X"0020";
  signal busy_i     : std_logic                          := '0';  -- [in]
  signal busy_out_i : std_logic;          -- [out]

  -- clock period
  constant PERIOD : time := 25 ns;

begin  -- test

  -- component instantiation
  DUT: protect_veto
    port map (
      clk      => clk_i,                  -- [in]
      rstb     => rstb_i,                 -- [in]
      delay    => delay_i,                -- [in]
      busy     => busy_i,                 -- [in]
      busy_out => busy_out_i);            -- [out]

  -- clock generation
  clk_i <= not clk_i after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    wait for 4 * PERIOD;
    rstb_i <= '1';

    
    wait for 4 * PERIOD;
    report "Raising busy" severity note;
    busy_i <= '1';
    wait for 10 * PERIOD;
    report "Removing busy" severity note;
    busy_i <= '0';
    wait for 10 * PERIOD;
    assert busy_out_i = '1'
      report "Should still be busy, but isn't"
      severity error;
    report "Waiting for busy to disappear" severity note;

    wait until busy_out_i = '0';
    report "Output busy disappeared" severity note;
    
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
