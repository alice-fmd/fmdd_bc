library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--
-- Entity to set-up a protective veto so that we are guarantied a
-- minimum time between triggers.
--
-- By default, the extra delay register is 16bit wide, which
-- allows us a maximum delay of
--
--    2^16 * 25ns = 65536 * 25ns = 1638400ns ~ 1.6ms
--
entity protect_veto is

  port (
    clk      : in  std_logic;                           --  Clock
    rstb     : in  std_logic;                           -- Async reset
    delay    : in  std_logic_vector(15 downto 0);  -- Veto period
    busy     : in  std_logic;                           -- External busy
    busy_out : out std_logic);                          -- Output busy

end protect_veto;

architecture rtl of protect_veto is
  type   state_t is (idle, counting);   -- State
  signal st_i   : state_t;
  signal cnt_i  : integer range 0 to 2**16-1;
  signal busy_i : std_logic;            -- Internal busy
begin  -- rtl
  -- purpose: Collect combinatorics
  combinatorics: block
  begin  -- block combinatoris
    busy_out <= '1' when (busy = '1' or busy_i = '1') else '0';
  end block combinatorics;
  
  -- purpose: State machine - on reception of external busy, count down to 0
  -- type   : sequential
  -- inputs : clk, rstb, delay, busy
  -- outputs: busy_i
  fsm : process (clk, rstb)
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i  <= idle;
      cnt_i <= to_integer(unsigned(delay));
    elsif clk'event and clk = '1' then  -- rising clock edge
      case st_i is
        when idle =>
          cnt_i  <= to_integer(unsigned(delay));
          busy_i <= '0';
          if busy = '1' then
            st_i <= counting;
          else
            st_i <= idle;
          end if;

        when counting =>
          report "Counter is now " &
            integer'image(cnt_i) & "/" &
            integer'image(to_integer(unsigned(delay))) severity note;
          cnt_i  <= cnt_i - 1;
          busy_i <= '1';
          if cnt_i <= 1 then
            st_i <= idle;
          else
            st_i <= counting;
          end if;
        when others =>
          busy_i <= '0';
          st_i   <= idle;
      end case;
    end if;
  end process fsm;  
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package protect_veto_pack is
  component protect_veto
    port (
      clk      : in  std_logic;
      rstb     : in  std_logic;
      delay    : in  std_logic_vector(15 downto 0);
      busy     : in  std_logic;
      busy_out : out std_logic);
  end component;
end protect_veto_pack;

-------------------------------------------------------------------------------
--
-- EOF
-- 
