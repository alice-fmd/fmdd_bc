------------------------------------------------------------------------------
-- Title      : Simulation package for the fmdd
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : simul_pack.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-06-15
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package fmdd_simul_pack is
  constant PERIOD   : time := 25 ns;
  constant L0_DELAY : time := 1.2 us;

  -- Commands
  constant cmd_change_dac : integer    := 0;  -- change dacs
  constant cmd_trigger    : integer    := 1;  -- Make trigger sequence
  constant cmd_l0         : integer    := 2;  -- Make L0 trigger
  constant cmd_reset      : integer    := 3;  -- Reset
  constant cmd_calib_on   : integer    := 4;  -- Turn on calibration mode;
  constant cmd_calib_off  : integer    := 5;  -- Turn off calibration mode;
  type cmd_t is
    record                              -- Record of commands
      code                : std_logic_vector(15 downto 0);  -- Instruction code
      name                : string(5 downto 1);  -- Name
    end record cmd_t;
  type cmd_list_t is array (0 to 15) of cmd_t;
  constant cmd_list       : cmd_list_t := (
    cmd_change_dac => (X"0001", "CHDAC"),
    cmd_trigger    => (X"0002", "TRIGS"),
    cmd_l0         => (X"0004", "TRIG0"),
    cmd_reset      => (X"0008", "RESET"),
    cmd_calib_on   => (X"0010", "CALON"),
    cmd_calib_off  => (X"0020", "CALOF"),
    others         => (X"0000", "-----"));  -- commands

  -- purpose: convert integer to std_logic_vector
  function int2slv (
    constant x : integer;               -- Value
    constant l : natural)               -- Size
    return std_logic_vector;

  -- purpose: Convert std_logic_vector to a string
  function slv2str (constant v : std_logic_vector) return string;

  -- purpose: Convert std_logic_vector to integer
  function slv2int (constant v : std_logic_vector) return integer;

  -- purpose: Change clock, and wait
  procedure change_clock (
    constant count     : in  integer;
    constant phase     : in  integer;
    signal   div_count : out std_logic_vector;
    signal   div_phase : out std_logic_vector);

  -- purpose: Change strip range
  procedure change_range (
    constant s1    : in  integer;
    constant s2    : in  integer;
    signal   clk   : in  std_logic;
    signal   first : out std_logic_vector;
    signal   last  : out std_logic_vector);

  -- purpose: Change timeouts
  procedure change_timeouts (
    constant hold_delay : in  time;
    constant l1_wait    : in  time;
    constant l2_wait    : in  time;
    signal   clk        : in  std_logic;
    signal   hold_wait  : out std_logic_vector;
    signal   l1_timeout : out std_logic_vector;
    signal   l2_timeout : out std_logic_vector);

  -- purpose: Change DAC registers 
  procedure change_biases (
    constant shape0  : in  integer;            -- Shaping bias 0
    constant shape1  : in  integer;            -- Shaping bias 1
    constant vfs0    : in  integer;            -- Shaping ref. voltage 0
    constant vfs1    : in  integer;            -- Shaping ref. voltage 1
    constant vfp0    : in  integer;            -- Pre-amp. ref. voltage 
    constant vfp1    : in  integer;            -- Pre-amp. ref. voltage 1
    constant cal     : in  integer;            -- Calibration pulse
    signal   shape_0 : out std_logic_vector;   -- Shaping bias reg. 0
    signal   shape_1 : out std_logic_vector;   -- Shaping bias reg. 1
    signal   vfs_0   : out std_logic_vector;   -- Shaping ref. voltage reg. 0
    signal   vfs_1   : out std_logic_vector;   -- Shaping ref. voltage reg. 1
    signal   vfp_0   : out std_logic_vector;   -- Pre-amp. ref. voltage reg. 0
    signal   vfp_1   : out std_logic_vector;   -- Pre-amp. ref. voltage reg. 1
    signal   cal_p   : out std_logic_vector);  -- Calibration pulse

  -- purpose: Start readout
  procedure start_it (
    signal start : out std_logic;
    signal clk   : in  std_logic);

  -- Wait for read-out to finish
  procedure wait_for_busy (
    signal busy : in std_logic;
    signal clk  : in std_logic);

  -- purpose: Make a L1 trigger
  procedure make_l0 (
    constant len : in  time := 25 ns;   -- Length of pulse
    signal   l0  : out std_logic);      -- trigger signal

  -- purpose: Make a L1 trigger
  procedure make_l1 (
    constant len : in  time := 200 ns;  -- Length of pulse
    signal   clk : in  std_logic;       -- clock
    signal   l1  : out std_logic);      -- trigger signal

  -- purpose: Make a L1 trigger
  procedure make_l2 (
    constant len : in  time := 200 ns;  -- Length of pulse
    signal   clk : in  std_logic;       -- clock
    signal   l2  : out std_logic);      -- trigger signal

  -- purpose: make a trigger sequence
  procedure make_triggers (
    signal   clk     : in  std_logic;            -- Clock
    signal   l0      : out std_logic;            -- L0 trigger
    signal   l1      : out std_logic;            -- L1 trigger
    signal   l2      : out std_logic;            -- L2 trigger 
    constant l0_time : in  time    := L0_DELAY;  -- Time of L0 rel. to BC
    constant l1_time : in  time    := 6.5 us;    -- Time of L1 rel. to BC
    constant l2_time : in  time    := 88 us;     -- Time of L2 rel. to BC
    constant l0_dur  : in  time    := 25  ns;    -- Duration of L0
    constant l1_dur  : in  time    := 200 ns;    -- Duration of L1
    constant l2_dur  : in  time    := 200 ns;    -- Duration of L2
    constant verb    : in  boolean := false);    -- Whether to be verbose

  procedure wait_for_busy_or_timeout (
    signal busy  : in std_logic;        -- Busy from trigger handler
    signal l1_to : in std_logic;        -- Timeout
    signal l2_to : in std_logic);       -- Time out

  procedure exec_command (
    constant c       : in  cmd_t;       -- Command 
    signal   clk     : in  std_logic;
    signal   command : out std_logic_vector(15 downto 0));

end package fmdd_simul_pack;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package body fmdd_simul_pack is
  -- purpose: convert integer to std_logic_vector
  function int2slv (
    constant x   : integer;             -- Value
    constant l   : natural)             -- Size
    return std_logic_vector is
    variable ret : std_logic_vector(l-1 downto 0);
  begin  -- function int2slv
    ret := std_logic_vector(to_unsigned(x, l));
    return ret;
  end function int2slv;

  -- purpose: Convert std_logic_vector to a string
  function slv2str (
    constant v   : std_logic_vector)
    return string
  is
    variable ret : string(v'length downto 1);
    variable i   : integer;
  begin  -- function slv2str
    ret := (others => 'X');

    for i in v'range loop
      case v(i) is
        when 'U'    => ret(i+1) := 'U';
        when 'X'    => ret(i+1) := 'X';
        when '0'    => ret(i+1) := '0';
        when '1'    => ret(i+1) := '1';
        when 'Z'    => ret(i+1) := 'Z';
        when 'W'    => ret(i+1) := 'W';
        when 'L'    => ret(i+1) := 'L';
        when 'H'    => ret(i+1) := 'H';
        when '-'    => ret(i+1) := '-';
        when others => ret(i+1) := 'X';
      end case;
    end loop;  -- i
    return ret;
  end function slv2str;

  -- purpose: Convert std_logic_vector to integer
  function slv2int (constant v : std_logic_vector) return integer is
  begin
    return to_integer(unsigned(v));
  end function slv2int;

  -- purpose: Change clock, and wait
  procedure change_clock (
    constant count     : in  integer;
    constant phase     : in  integer;
    signal   div_count : out std_logic_vector;
    signal   div_phase : out std_logic_vector) is
  begin  -- change_params
    report "Changing clock to " & integer'image(count) & ", " &
      integer'image(phase) severity note;
    div_count <= int2slv(count, div_count'length);
    div_phase <= int2slv(phase, div_phase'length);
  end change_clock;

  -- purpose: Change range
  procedure change_range (
    constant s1    : in  integer;
    constant s2    : in  integer;
    signal   clk   : in  std_logic;
    signal   first : out std_logic_vector;
    signal   last  : out std_logic_vector) is
  begin  -- change_range
    report "Changing range to " & integer'image(s1) & " - > " &
      integer'image(s2) severity note;
    wait until rising_edge(clk);
    first <= int2slv(s1, first'length);
    last  <= int2slv(s2, last'length);
    wait until falling_edge(clk);
    wait until rising_edge(clk);
  end change_range;

  -- purpose: Change timeouts
  procedure change_timeouts (
    constant hold_delay : in  time;
    constant l1_wait    : in  time;
    constant l2_wait    : in  time;
    signal   clk        : in  std_logic;
    signal   hold_wait  : out std_logic_vector;
    signal   l1_timeout : out std_logic_vector;
    signal   l2_timeout : out std_logic_vector) is
    variable hold_cnt_i :     integer := (hold_delay - L0_DELAY) / PERIOD;
    variable l1_cnt_i   :     integer := (l1_wait - L0_DELAY) / PERIOD;
    variable l2_cnt_i   :     integer := (l2_wait - L0_DELAY) / PERIOD;
  begin  -- change_timeouts
    wait until rising_edge(clk);
    report "Changing time-outs to " & time'image(hold_delay) & ", " &
      time'image(l1_wait) & ", " & time'image(l2_wait) severity note;
    hold_wait  <= int2slv(hold_cnt_i, hold_wait'length);
    l1_timeout <= int2slv(l1_cnt_i, l1_timeout'length);
    l2_timeout <= int2slv(l2_cnt_i, l2_timeout'length);
    wait until falling_edge(clk);
    wait until rising_edge(clk);
  end change_timeouts;

  -- purpose: Change DAC register values
  procedure change_biases (
    constant shape0  : in  integer;     -- Shaping bias 0
    constant shape1  : in  integer;     -- Shaping bias 1
    constant vfs0    : in  integer;     -- Shaping ref. voltage 0
    constant vfs1    : in  integer;     -- Shaping ref. voltage 1
    constant vfp0    : in  integer;     -- Pre-amp. ref. voltage 
    constant vfp1    : in  integer;     -- Pre-amp. ref. voltage 1
    constant cal     : in  integer;     -- Calibration pulse
    signal   shape_0 : out std_logic_vector;  -- Shaping bias reg. 0
    signal   shape_1 : out std_logic_vector;  -- Shaping bias reg. 1
    signal   vfs_0   : out std_logic_vector;  -- Shaping ref. voltage reg. 0
    signal   vfs_1   : out std_logic_vector;  -- Shaping ref. voltage reg. 1
    signal   vfp_0   : out std_logic_vector;  -- Pre-amp. ref. voltage reg. 0
    signal   vfp_1   : out std_logic_vector;  -- Pre-amp. ref. voltage reg. 1
    signal   cal_p   : out std_logic_vector) is  -- Calibration pulse reg.
  begin  -- procedure change_biases
    shape_0 <= int2slv(shape0, shape_0'length);
    shape_1 <= int2slv(shape1, shape_1'length);
    vfs_0   <= int2slv(vfs0, vfs_0'length);
    vfs_1   <= int2slv(vfs1, vfs_1'length);
    vfp_0   <= int2slv(vfp0, vfp_0'length);
    vfp_1   <= int2slv(vfp1, vfp_1'length);
    cal_p   <= int2slv(cal, cal_p'length);
  end procedure change_biases;

  -- purpose: Start readout
  procedure start_it (
    signal start : out std_logic;
    signal clk   : in  std_logic) is
  begin  -- start_ro
    wait until rising_edge(clk);
    start <= '1';
    wait until rising_edge(clk);
    start <= '0';
  end start_it;

  -- Wait for read-out to finish
  procedure wait_for_busy (
    signal busy : in std_logic;
    signal clk  : in std_logic) is
  begin  -- wait_for_busy
    wait until rising_edge(clk);
    wait until busy = '0';
    wait until rising_edge(clk);
  end wait_for_busy;

  -- purpose: Make a L1 trigger
  procedure make_l0 (
    constant len : in  time := 25 ns;  -- Length of pulse
    signal   l0  : out std_logic) is    -- trigger signal
  begin  -- make_l0
    l0 <= '1', '0' after len;
  end make_l0;

  -- purpose: Make a L1 trigger
  procedure make_l1 (
    constant len : in  time := 200 ns;  -- Length of pulse
    signal   clk : in  std_logic;       -- clock
    signal   l1  : out std_logic) is    -- trigger signal
  begin  -- make_l0
    -- wait until rising_edge(clk);
    l1 <= '0', '1' after len;
  end make_l1;

  -- purpose: Make a L2 trigger
  procedure make_l2 (
    constant len : in  time := 200 ns;  -- Length of pulse
    signal   clk : in  std_logic;       -- clock
    signal   l2  : out std_logic) is    -- trigger signal
  begin  -- make_l0
    -- wait until rising_edge(clk);
    l2 <= '0', '1' after len;
  end make_l2;

  -- purpose: make a trigger sequence
  procedure make_triggers (
    signal   clk        : in  std_logic;            -- Clock
    signal   l0         : out std_logic;            -- L0 trigger
    signal   l1         : out std_logic;            -- L1 trigger
    signal   l2         : out std_logic;            -- L2 trigger 
    constant l0_time    : in  time    := L0_DELAY;  -- Time of L0 rel. to BC
    constant l1_time    : in  time    := 6.5 us;    -- Time of L1 rel. to BC
    constant l2_time    : in  time    := 88 us;     -- Time of L2 rel. to BC
    constant l0_dur     : in  time    := 25  ns;    -- Duration of L0
    constant l1_dur     : in  time    := 200 ns;    -- Duration of L1
    constant l2_dur     : in  time    := 200 ns;    -- Duration of L2
    constant verb       : in  boolean := false)     -- Whether to be verbose
  is
    variable l1_delta_i :     time    := (l1_time - l0_time);
    variable l2_delta_i :     time    := (l2_time - l1_time);
  begin
    assert not verb report "Making L0" severity note;
    make_l0(len => l0_dur,
            l0  => l0);
    wait for l1_delta_i;
    assert not verb report "Making L1" severity note;
    make_l1(len => l1_dur,
            clk => clk,
            l1  => l1);
    wait for l2_delta_i;
    assert not verb report "Making L2" severity note;
    make_l2(len => l2_dur,
            clk => clk,
            l2  => l2);
  end make_triggers;

  -- purpose: wait for busy to go down, or timeout
  procedure wait_for_busy_or_timeout (
    signal busy  : in std_logic;        -- Busy from trigger handler
    signal l1_to : in std_logic;        -- Timeout
    signal l2_to : in std_logic) is     -- Timeout
  begin
    if busy /= '0' and l1_to /= '1' and l2_to /= '1' then
      wait until busy = '0' or l1_to = '1' or l2_to = '1';
      report "Ready or timeout" severity note;
    end if;
    -- assert busy = '0' report "Timed-out" severity warning;
  end wait_for_busy_or_timeout;

  -- purpose: Execute a command 
  procedure exec_command (
    constant c       : in  cmd_t;                          -- Command bit
    signal   clk     : in  std_logic;
    signal   command : out std_logic_vector(15 downto 0))  -- Command output
  is
  begin
    report "Executing Command # " & slv2str(c.code)
      & "            :     " & c.name severity note;
    wait until rising_edge(clk);
    command <= c.code;
    wait until rising_edge(clk);
    command <= (others => '0');
  end exec_command;

end package body fmdd_simul_pack;
------------------------------------------------------------------------------
--
-- EOF
--
