------------------------------------------------------------------------------
-- Title      : Glitch filter
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : glitch_filter.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2007/01/29
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: The input signal must be asserted (1) over at least
--              two rising clock edges. The output signal will be one
--              clock cycle long.  The output is one clock cycle
--              delayed.
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
entity glitch_filter is
  port (
    clk    : in  std_logic;             -- Clock
    rstb   : in  std_logic;             -- Async reset
    input  : in  std_logic;             -- Input signal
    output : out std_logic);            -- Output signal
end glitch_filter;

-------------------------------------------------------------------------------
architecture rtl of glitch_filter is
  signal w0_i  : std_logic;
  signal r1_i  : std_logic;
  signal r2_i  : std_logic;
  signal r3_i  : std_logic;
  signal out_i : std_logic;
begin  -- rtl
  output <= out_i;
  w0_i   <= r1_i and r2_i;
  out_i  <= not r3_i and w0_i;

  p_filter : process (clk, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      r1_i <= '0';
      r2_i <= '0';
      r3_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      r1_i <= input;
      r2_i <= r1_i;
      r3_i <= r2_i;
    end if;
  end process;
end rtl;

------------------------------------------------------------------------------
--
-- This architecture provides a glitch filter for signals that are
-- at least 25 ns long.  It triggers on both the falling and rising edge of
-- the clock to do this.  It may not be possible to implement this in
-- hardware, but we will see.
-- 
architecture rtl3 of glitch_filter is
  signal w0_i  : std_logic;             -- Wire 0 (and of wire 1 and r1)
  signal w1_i  : std_logic;             -- Wire 1 (or of falling edges)
  signal r1_i  : std_logic;             -- Reg. 1 (registered input)
  signal r2_i  : std_logic;             -- Reg. 2 (delayed reg. 1)
  signal r3_i  : std_logic;             -- Reg. 3 (registered input falling)
  signal r4_i  : std_logic;             -- Reg. 4 (delayed reg. 4)
  signal out_i : std_logic;             -- Output wire.

begin  -- architecture rtl3
  output <= out_i;
  w1_i   <= r3_i or r4_i;
  w0_i   <= (r1_i and w1_i); -- and not r2_i;

  -- purpose: get rising edge
  -- type   : sequential
  -- inputs : clk, rstb, input
  -- outputs: r1_i, r2_i
  p_rising : process (clk, rstb) is
  begin  -- process p_rising
    if rstb = '0' then                  -- asynchronous reset (active low)
      r1_i <= '0';
      r2_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      r1_i <= input;
      r2_i <= r1_i;
    end if;
  end process p_rising;

  -- purpose: Get falling edge
  -- type   : sequential
  -- inputs : clk, rstb, input
  -- outputs: r3_i, r4_i
  p_falling : process (clk, rstb) is
  begin  -- process p_falling
    if rstb = '0' then                  -- asynchronous reset (active low)
      r3_i <= '0';
      r4_i <= '0';
    elsif clk'event and clk = '0' then  -- rising clock edge
      r3_i <= input;
      r4_i <= r3_i;
    end if;
  end process p_falling;

  -- purpose: Output
  -- type   : sequential
  -- inputs : clk, rstb, w0_i, r2_i
  -- outputs: out_i
  p_out : process (clk, rstb) is
  begin  -- process p_out
    if rstb = '0' then                  -- asynchronous reset (active low)
      out_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      out_i <= w0_i and not (r2_i);
    end if;
  end process p_out;
end architecture rtl3;

-------------------------------------------------------------------------------
architecture rtl2 of glitch_filter is
  signal w0_i  : std_logic;
  signal r1_i  : std_logic;
  signal r2_i  : std_logic;
  signal r3_i  : std_logic;
  signal out_i : std_logic;
begin  -- rtl2
  output <= out_i;
  w0_i   <= not (r1_i and r2_i);
  out_i  <= not (w0_i or r3_i);

  p_filter : process (clk, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      r1_i <= '0';
      r2_i <= '0';
      r3_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      r1_i <= input;
      r2_i <= r1_i;
      r3_i <= r2_i;
    end if;
  end process;
end rtl2;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package glitch_filter_pack is
  component glitch_filter
    port (
      input   : in  std_logic;
      clk  : in  std_logic;
      rstb : in  std_logic;
      output   : out std_logic);
  end component;
end glitch_filter_pack;
-------------------------------------------------------------------------------
--
-- EOF
--


