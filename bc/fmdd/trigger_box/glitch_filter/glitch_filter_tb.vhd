------------------------------------------------------------------------------
-- Title      : Test of glitch filter
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : glitch_filter_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/27
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.glitch_filter_pack.all;

------------------------------------------------------------------------------
entity glitch_filter_tb is
end entity glitch_filter_tb;

------------------------------------------------------------------------------
architecture test of glitch_filter_tb is
  signal PERIOD   : time      := 25 ns;
  signal clk_i    : std_logic := '0';   -- Clock
  signal rstb_i   : std_logic := '0';   -- Async reset
  signal input_i  : std_logic := '0';   -- Input signal
  signal output_i : std_logic;          -- Output signal

  type delay_list is array (0 to 6) of time;
  constant delays_i : delay_list := (5 ns,
                                     10 ns,
                                     20 ns,
                                     25 ns,
                                     35 ns,
                                     50 ns,
                                     75 ns);
  procedure try (
    signal   x     : out std_logic;
    signal   clk   : in  std_logic;
    constant delay : in  time) is
  begin  -- procedure try
    for i in delay_list'range loop
      wait for 100 ns;
      wait until rising_edge(clk);
      if delay /= 0 ns then
        wait for delay;
      end if;
      x <= '1';
      wait for delays_i(i);
      x <= '0';
    end loop;  -- i

  end procedure try;

begin  -- architecture test
  dut : entity work.glitch_filter(rtl2)
    port map (
        clk    => clk_i,                -- in  Clock
        rstb   => rstb_i,               -- in  Async reset
        input  => input_i,              -- in  Input signal
        output => output_i);            -- out Output signal

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';

    try(x => input_i, clk => clk_i, delay => 0 ns);
    wait for 100 ns;
    try(x => input_i, clk => clk_i, delay => 5 ns);
    wait for 100 ns;
    try(x => input_i, clk => clk_i, delay => 10 ns);
    wait for 100 ns;
    try(x => input_i, clk => clk_i, delay => 20 ns);

    wait; -- forever
  end process stimuli;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  clock_gen: process is
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;
end architecture test;
------------------------------------------------------------------------------
-- 
-- EOF
--
