------------------------------------------------------------------------------
-- Title      : Module to make fake triggers
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : trigger_box.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-09-02
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Make fake triggers
--              The listens for the start command, and when asserted,
--              makes an internal trigger sequence.  If the option
--              l0_only is asserted, then only an internal Level 0
--              trigger is made.  Otherwise, when this state machine
--              is idle, the external triggers are passed through as
--              is.
--
--              FIXME: Perhaps we should put in a glicth filter on the
--              triggers, at least the level 0 trigger should probably
--              have one.
--
--              Note: we used to sync the L1 here, but since the VA1
--              readout module takes care of that now, I don't think
--              we really need it here.  If we do, I'll put in a
--              glicth filter on that too.
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.glitch_filter_pack.all;

entity trigger_box is
  generic (
    GLITCH : boolean               := false;  -- Enable glitch filter on L0
    L0_DUR : integer range 1 to 16 := 1;      -- Length of L0
    L1_DUR : integer range 1 to 16 := 8;      -- Length of L1
    L2_DUR : integer range 1 to 16 := 8);     -- Length of L2
  port (
    clk        : in  std_logic;         -- Clock
    rstb       : in  std_logic;         -- Reset (active low)
    start      : in  std_logic;         -- Make a fake trigger sequence
    l0_only    : in  std_logic;         -- Only make a fake L0
    inhibit    : in  std_logic;         -- Inhibit triggers
    l0         : in  std_logic;         -- Real L0 trigger (active high)
    l1b        : in  std_logic;         -- Real L1 trigger (active low)
    l2b        : in  std_logic;         -- Real L2 trigger (active low)
    l1_timeout : in  std_logic_vector(15 downto 0);  -- Timeout for L1
    l2_timeout : in  std_logic_vector(15 downto 0);  -- Timeout for L2
    l0_out     : out std_logic;         -- The L0 trigger  (fake or real)
    l1b_out    : out std_logic;         -- The L1 trigger (fake or real)
    l2b_out    : out std_logic;         -- The L2 trigger (fake or real)
    l1r        : out std_logic;         -- L1 Reject (timeout)
    l2r        : out std_logic;         -- L2 Reject (timeout)
    busy       : out std_logic;         -- Status: trigger busy
    timeout    : out std_logic;         -- Status: trigger timeout
    overlap    : out std_logic);        -- Status: overlapping
end trigger_box;


-------------------------------------------------------------------------------
architecture rtl of trigger_box is
  type state_t is (idle,
                   generate_l0,
                   wait_to_l1,
                   generate_l1,
                   wait_to_l2,
                   generate_l2,
                   finish);
  type veto_t is (wait_l0,
                  got_l0,
                  seen_l0,
                  got_l1,
                  seen_l1,
                  got_l2);

  signal veto_i        : veto_t;
  signal state_i       : state_t;
  signal filter_l0_i   : std_logic;     -- Filtered L0
  signal l0_i          : std_logic;
  signal l1b_i         : std_logic;
  signal l2b_i         : std_logic;
  signal l0_out_i      : std_logic;
  signal l1b_out_i     : std_logic;
  signal l2b_out_i     : std_logic;
  signal busy_i        : std_logic;     -- Busy signal
  signal cnt_l0_dur_i  : integer range 0 to 2**4;
  signal cnt_l1_wait_i : integer range 0 to 2**16;
  signal cnt_l1_dur_i  : integer range 0 to 2**4;
  signal cnt_l2_wait_i : integer range 0 to 2**16;
  signal cnt_l2_dur_i  : integer range 0 to 2**4;
  signal l1_timeout_i  : integer range 0 to 2**16;
  signal l2_timeout_i  : integer range 0 to 2**16;
  signal here_i        : boolean;
  signal l0_only_i     : boolean;       -- Register option
  signal l1r_i         : std_logic;
  signal l1r_ii        : std_logic;
  signal l2r_i         : std_logic;
  signal l2r_ii        : std_logic;
  
begin  -- rtl
  -- purpose: Set output signals
  combinatoric : block
  begin  -- block combinatoric
    l0_out_i <= l0_i when here_i or inhibit = '1' else
                filter_l0_i when GLITCH else l0;
    l1b_out_i <= l1b_i when here_i or inhibit = '1' else l1b;
    l2b_out_i <= l2b_i when here_i or inhibit = '1' else l2b;
    l0_out    <= l0_out_i;
    l1b_out   <= l1b_out_i;
    l2b_out   <= l2b_out_i;
    busy      <= busy_i;
  end block combinatoric;

  -- Glicth and meta stability filter for the L0 signal 
  filter : entity work.glitch_filter(rtl3)
    port map (
      clk    => clk,                    -- in  Clock
      rstb   => rstb,                   -- in  Async reset
      input  => l0,                     -- in  Input signal
      output => filter_l0_i);           -- out Output signal

  -- purpose: Statemachine to produce a fake trigger sequence
  -- type   : sequential
  -- inputs : clk, rstb, start 
  -- outputs: l0_out, l1b_out, l2b_out
  fsm : process (clk, rstb)
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_l0_dur_i <= 0;
      cnt_l1_dur_i <= 0;
      cnt_l2_dur_i <= 0;
      here_i       <= false;
      l0_i         <= '0';
      l1b_i        <= '1';
      l2b_i        <= '1';
      state_i      <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      state_i <= state_i;
      l0_i    <= '0';
      l1b_i   <= '1';
      l2b_i   <= '1';
      case state_i is
        when idle =>
          here_i    <= false;
          l0_only_i <= false;
          if start = '1' and inhibit = '0' and busy_i = '0' then
            if (l0_only = '1') then
              l0_only_i <= true;
            end if;
            cnt_l0_dur_i <= 1;
            l0_i         <= '1';
            here_i       <= true;
            state_i      <= generate_l0;
          end if;

        when generate_l0 =>
          here_i       <= true;
          l0_i         <= '1';
          l0_only_i    <= false;
          cnt_l0_dur_i <= cnt_l0_dur_i + 1;
          if cnt_l0_dur_i >= L0_DUR then
            l0_i <= '0';
            -- If we're only asked to do a L0 trigger, then get out here_i.
            if l0_only_i then
              state_i <= finish;
            else
              state_i <= wait_to_l1;
            end if;
          end if;

        when wait_to_l1 =>
          -- Add one to get the right time 
          if cnt_l1_wait_i = 2 then
            cnt_l1_dur_i <= 0;
            l1b_i        <= '0';
            state_i      <= generate_l1;
          elsif busy_i = '0' then       -- we were interrupted
            l1b_i   <= '1';
            state_i <= idle;
          end if;

        when generate_l1 =>
          l1b_i        <= '0';
          cnt_l1_dur_i <= cnt_l1_dur_i + 1;
          if cnt_l1_dur_i >= L1_DUR then
            l1b_i   <= '1';
            state_i <= wait_to_l2;
          elsif busy_i = '0' then       -- we were interrupted
            state_i <= idle;
          end if;

        when wait_to_l2 =>
          -- Add one to get the right time 
          if cnt_l2_wait_i = 2 then
            cnt_l2_dur_i <= 0;
            l2b_i        <= '0';
            state_i      <= generate_l2;
          elsif busy_i = '0' then       -- we were interrupted
            l2b_i   <= '1';
            state_i <= idle;
          end if;

        when generate_l2 =>
          l2b_i        <= '0';
          cnt_l2_dur_i <= cnt_l2_dur_i + 1;
          if cnt_l2_dur_i >= L2_DUR then
            l2b_i   <= '1';
            state_i <= finish;
          elsif busy_i = '0' then       -- we were interrupted
            state_i <= idle;
          end if;

        when finish =>
          cnt_l0_dur_i <= 0;
          cnt_l1_dur_i <= 0;
          cnt_l2_dur_i <= 0;
          state_i      <= idle;

        when others =>
          state_i <= idle;
      end case;
    end if;
  end process fsm;

  -- purpose: Count clock cycles.  We can use this to veto making
  --          triggers while another trigger sequence is running
  -- type   : sequential
  -- inputs : clk, rstb, state_i
  -- outputs: cnt_l1_wait, cnt_l2_wait
  veto_it : process (clk, rstb) is
  begin  -- process counting
    if rstb = '0' then                  -- asynchronous reset (active low)
      veto_i        <= wait_l0;
      cnt_l1_wait_i <= 0;
      cnt_l2_wait_i <= 0;
      l1_timeout_i  <= 0;               -- to_integer(unsigned(l1_timeout));
      l2_timeout_i  <= 0;               -- to_integer(unsigned(l2_timeout));
      timeout       <= '0';
      overlap       <= '0';
      busy_i        <= '0';
      l1r_i         <= '0';
      l2r_i         <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- veto_i <= veto_i;
      -- busy_i <= busy_i;

      case veto_i is
        when wait_l0 =>
          busy_i        <= '0';
          cnt_l1_wait_i <= to_integer(unsigned(l1_timeout));
          cnt_l2_wait_i <= to_integer(unsigned(l2_timeout));
          l1r_i         <= '0';
          l2r_i         <= '0';
          timeout       <= '0';
          overlap       <= '0';
          if l0_out_i = '1' then
            busy_i <= '1';
            veto_i <= got_l0;
          else
            busy_i <= '0';
            veto_i <= wait_l0;
          end if;

        when got_l0 =>
          cnt_l1_wait_i <= cnt_l1_wait_i - 1;
          cnt_l2_wait_i <= cnt_l2_wait_i - 1;
          busy_i        <= '1';
          if l0_out_i = '0' then        -- Wait until trigger goes away
            veto_i <= seen_l0;
          else
            veto_i <= got_l0;
          end if;

        when seen_l0 =>
          cnt_l1_wait_i <= cnt_l1_wait_i - 1;
          cnt_l2_wait_i <= cnt_l2_wait_i - 1;
          busy_i        <= '1';
          if l1b_out_i = '0' then
            veto_i <= got_l1;
          elsif l0_out_i = '1' or l2b_out_i = '0' then
            report "Overlapping trigger L0/L2 " &
              std_logic'image(l0_out_i) & "/" &
              std_logic'image(l2b_out_i) &
              " while waiting for L1" severity warning;
            overlap <= '1';
            l1r_i   <= '1';
            veto_i  <= wait_l0;         -- Is this correct?
          elsif cnt_l1_wait_i = 1 then
            timeout <= '1';
            l1r_i   <= '1';
            veto_i  <= wait_l0;
          else
            veto_i <= seen_l0;
          end if;

        when got_l1 =>
          cnt_l2_wait_i <= cnt_l2_wait_i - 1;
          busy_i        <= '1';
          if l1b_out_i = '1' then       -- Wait until trigger goes away
            veto_i <= seen_l1;
          else
            veto_i <= got_l1;
          end if;

        when seen_l1 =>
          cnt_l2_wait_i <= cnt_l2_wait_i - 1;
          busy_i        <= '1';
          if l2b_out_i = '0' then
            veto_i <= got_l2;
          elsif l0_out_i = '1' or l1b_out_i = '0' then
            report "Overlapping trigger L0/L1 " &
              std_logic'image(l0_out_i) & "/" &
              std_logic'image(l1b_out_i) &
              " while waiting for L2" severity warning;
            overlap <= '1';
            l2r_i   <= '1';
            veto_i  <= seen_l1;         -- idle;
          elsif cnt_l2_wait_i = 1 then
            l2r_i   <= '1';
            timeout <= '1';
            veto_i  <= wait_l0;
          else
            veto_i <= seen_l1;
          end if;

        when got_l2 =>
          cnt_l1_wait_i <= 0;
          cnt_l2_wait_i <= 0;
          veto_i        <= wait_l0;
          busy_i        <= '1';

        when others =>
          veto_i <= wait_l0;
      end case;
    end if;
  end process veto_it;

  -- purpose: Keep up L1R and L2R for two clock cycles
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  keep_rs : process (clk, rstb)
  begin  -- process keep_rs
    if rstb = '0' then                  -- asynchronous reset (active low)
      l1r    <= '0';
      l2r    <= '0';
      l1r_ii <= '0';
      l2r_ii <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      l1r_ii <= l1r_i;
      l1r    <= l1r_ii or l1r_i;
      l2r_ii <= l2r_i;
      l2r    <= l2r_ii or l2r_i;
    end if;
  end process keep_rs;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package trigger_box_pack is
  component trigger_box
    generic (
      GLITCH : boolean;                 -- Enable glitch filter
      L0_DUR : integer range 1 to 16;   -- Length of L0
      L1_DUR : integer range 1 to 16;   -- Length of L1
      L2_DUR : integer range 1 to 16);  -- Length of L2
    port (
      clk        : in  std_logic;       -- Clock
      rstb       : in  std_logic;       -- Reset (active low)
      start      : in  std_logic;       -- Make a fake trigger sequence
      l0_only    : in  std_logic;       -- Only make a fake L0
      inhibit    : in  std_logic;       -- Inhibit triggers
      l0         : in  std_logic;       -- Real L0 trigger (active high)
      l1b        : in  std_logic;       -- Real L1 trigger (active low)
      l2b        : in  std_logic;       -- Real L2 trigger (active low)
      l1_timeout : in  std_logic_vector(15 downto 0);  -- Timeout for L1
      l2_timeout : in  std_logic_vector(15 downto 0);  -- Timeout for L2
      l0_out     : out std_logic;       -- The L0 trigger  (fake or real)
      l1b_out    : out std_logic;       -- The L1 trigger (fake or real)
      l2b_out    : out std_logic;       -- The L2 trigger (fake or real)
      l1r        : out std_logic;       -- L1 Reject (timeout)
      l2r        : out std_logic;       -- L2 Reject (timeout)
      busy       : out std_logic;       -- Status: trigger busy
      timeout    : out std_logic;       -- Status: trigger timeout
      overlap    : out std_logic);      -- Status : overlapping
  end component trigger_box;
end trigger_box_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
