------------------------------------------------------------------------------
-- Title      : Test bench of trigger box
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : trigger_box_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/27
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This tests the trigger box
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.trigger_box_pack.all;
use work.fmdd_simul_pack.all;

------------------------------------------------------------------------------
entity trigger_box_tb is
end entity trigger_box_tb;

------------------------------------------------------------------------------

architecture test of trigger_box_tb is

  constant L0_DUR : integer := 1;       -- Length of L0
  constant L1_DUR : integer := 8;       -- Length of L1
  constant L2_DUR : integer := 8;       -- Length of L2

  signal clk_i        : std_logic := '0';  -- Clock
  signal rstb_i       : std_logic := '0';  -- Reset (active low)
  signal start_i      : std_logic := '0';  -- Make a fake trigger sequence
  signal l0_only_i    : std_logic := '0';  -- Only make a fake L0
  signal inhibit_i    : std_logic := '0';  -- Inhibit triggers
  signal l0_i         : std_logic := '0';  -- Real L0 trigger (active high)
  signal l1b_i        : std_logic := '1';  -- Real L1 trigger (active low)
  signal l2b_i        : std_logic := '1';  -- Real L2 trigger (active low)
  signal hold_wait_i  : std_logic_vector(15 downto 0);  -- Not used
  signal l1_timeout_i : std_logic_vector(15 downto 0);  -- Timeout for L1
  signal l2_timeout_i : std_logic_vector(15 downto 0);  -- Timeout for L2
  signal l0_out_i     : std_logic;      -- The L0 trigger  (fake or real)
  signal l1b_out_i    : std_logic;      -- The L1 trigger (fake or real)
  signal l2b_out_i    : std_logic;      -- The L2 trigger (fake or real)
  signal busy_i       : std_logic;      -- Status: trigger busy
  signal timeout_i    : std_logic;      -- Status: trigger timeout
  signal overlap_i    : std_logic;      -- Status : overlapping
begin  -- architecture test
  dut: entity work.trigger_box
    generic map (
        L0_DUR => L0_DUR,               -- Length of L0
        L1_DUR => L1_DUR,               -- Length of L1
        L2_DUR => L2_DUR)               -- Length of L2
    port map (
        clk        => clk_i,            -- in  Clock
        rstb       => rstb_i,           -- in  Reset (active low)
        start      => start_i,          -- in  Make a fake trigger sequence
        l0_only    => l0_only_i,        -- in  Only make a fake L0
        inhibit    => inhibit_i,        -- in  Inhibit triggers
        l0         => l0_i,             -- in  Real L0 trigger (active high)
        l1b        => l1b_i,            -- in  Real L1 trigger (active low)
        l2b        => l2b_i,            -- in  Real L2 trigger (active low)
        l1_timeout => l1_timeout_i,     -- in  Timeout for L1
        l2_timeout => l2_timeout_i,     -- in  Timeout for L2
        l0_out     => l0_out_i,         -- out The L0 trigger  (fake or real)
        l1b_out    => l1b_out_i,        -- out The L1 trigger (fake or real)
        l2b_out    => l2b_out_i,        -- out The L2 trigger (fake or real)
        busy       => busy_i,           -- out Status: trigger busy
        timeout    => timeout_i,        -- out Status: trigger timeout
        overlap    => overlap_i);       -- out Status : overlapping

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    change_timeouts(hold_delay => 1.5 us,
                    l1_wait    => 6.5 us,
                    l2_wait    => 88 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);
    wait for 100 ns;
    rstb_i <= '1';

    wait for 1 us - NOW;
    -- Start internal trigger box
    start_it(start => start_i,
             clk => clk_i);

    -- Overlap internal with external 
    wait for 100 us - NOW;
    start_it(start => start_i,
             clk => clk_i);
    wait for 100 ns;
    make_l0(l0 => l0_i);
    wait for 5 us;
    make_l1(l1 => l1b_i,
            clk => clk_i);
    wait for 80 us;
    make_l2(l2 => l2b_i,
            clk => clk_i);
    
    -- Overlap external with internal
    wait for 200 us - NOW;
    make_l0(l0 => l0_i);
    wait for 100 ns;
    start_it(start => start_i,
             clk => clk_i);
    wait for 5 us;
    make_l1(l1 => l1b_i,
            clk => clk_i);
    wait for 80 us;
    make_l2(l2 => l2b_i,
            clk => clk_i);

    -- External
    wait for 301.2 us - NOW;
    make_triggers(clk       => clk_i,
                  l0        => l0_i,
                  l1        => l1b_i,
                  l2        => l2b_i);

    -- External, that times out L1 
    wait for 401.2 us - NOW;
    make_l0(l0  => l0_i);
    wait for 10 us;
    make_l1(l1  => l1b_i,
            clk => clk_i);
    wait for 100 ns;
    rstb_i <= '0';
    wait for 100 ns;
    rstb_i <= '1';

    -- External, that overlaps out L0 
    wait for 501.2 us - NOW;
    make_l0(l0 => l0_i);
    wait for 1 us;
    make_l0(l0 => l0_i);
    wait for 100 ns;
    rstb_i <= '0';
    wait for 100 ns;
    rstb_i <= '1';
    

    wait;                               -- forever 
  end process stimuli;

  -- purpose: make a clock
  clock_gen: process is
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;
end architecture test;

------------------------------------------------------------------------------
