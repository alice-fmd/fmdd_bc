------------------------------------------------------------------------------
-- Title      : Calibration manager test-bench
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : cal_manager_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008/08/14
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test of calibration manager 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2007/01/03  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.cal_manager_pack.all;
use work.fmdd_simul_pack.all;

------------------------------------------------------------------------------
entity cal_manager_tb is
end entity cal_manager_tb;

------------------------------------------------------------------------------

architecture test of cal_manager_tb is
  constant PERIOD : time := 25 ns;

  signal clk_i         : std_logic := '1';              -- Clock
  signal rstb_i        : std_logic := '0';              -- Reset
  signal enable_i      : std_logic := '0';              -- Start pulser mode
  signal busy_i        : std_logic;                     -- We're busy
  signal clear_seq_i   : std_logic;                     -- Reset VA shift reg.
  signal l2_i          : std_logic := '1';              -- L2 trigger
  signal iterations_i  : std_logic_vector(15 downto 0);
                                                        -- Number of iterations
  signal step_i        : std_logic_vector(7 downto 0);  -- Number of divisions
  signal strip_min_i   : std_logic_vector(7 downto 0);  -- Min
  signal strip_max_i   : std_logic_vector(7 downto 0);  -- Max
  signal strip_i       : std_logic_vector(7 downto 0);  -- output strip no
  signal pulse_i       : std_logic_vector(7 downto 0);  -- Pulse size
  signal change_i      : std_logic;
  signal change_busy_i : std_logic;
  signal dac_cnt_i     : integer;

begin  -- architecture test

  dut : entity work.cal_manager
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Reset
        enable      => enable_i,        -- in  Start pulser mode
        busy        => busy_i,          -- out We're active
        clear_seq   => clear_seq_i,     -- out Reset VA shift reg. 
        l2          => l2_i,            -- in  L2 trigger
        iterations  => iterations_i,    -- in  Number of iterations
        step        => step_i,          -- in  Number of divisions
        strip_min   => strip_min_i,     -- in  Min strip
        strip_max   => strip_max_i,     -- in  Max strip
        strip       => strip_i,         -- out output strip no
        pulse       => pulse_i,         -- out Pulse size
        change      => change_i,        -- out 
        change_busy => change_busy_i);  -- in

  -- Connect change done to change
  -- change_busy_i <= not change_i;

  -- purpose: Fake DAC response
  -- type   : sequential
  -- inputs : clk, rstb_i, change_i
  -- outputs: change_busy_i
  fake_dac: process (clk_i, rstb_i) is
  begin  -- process fake_dac
    if rstb_i = '0' then                -- asynchronous reset (active low)
      change_busy_i <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if change_i = '1' then
        change_busy_i <= '1';
        dac_cnt_i <= 1;
      elsif dac_cnt_i = 3 then
        dac_cnt_i <= 0;
        change_busy_i <= '0';
      elsif dac_cnt_i > 0 then
        change_busy_i <= '1';
        dac_cnt_i <= dac_cnt_i + 1;
      end if;
    end if;
  end process fake_dac;

  stim : process
  begin  -- process stim
    strip_min_i  <= X"00";
    strip_max_i  <= X"03";
    iterations_i <= X"0004";
    step_i       <= X"7F";
    wait for 100 ns; 
    rstb_i       <= '1';

    wait for 100 ns;
    start_it(start     => enable_i, clk => clk_i);
    -- wait_for_busy(busy => busy_i, clk => clk_i);

    for i in 0 to 100 loop
      wait for 200 ns;
      wait until rising_edge(clk_i);
      l2_i <= '0';
      wait until rising_edge(clk_i);
      l2_i <= '1';
    end loop;  -- i

    wait;                               -- forever
  end process stim;
  -- purpose: Make clock
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  clocker : process is
  begin  -- process clocker
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clocker;

end architecture test;

------------------------------------------------------------------------------
--
-- EOF
--
