------------------------------------------------------------------------------
-- Title      : Calibration sequence manager
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : cal_manager.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008/08/14
-- Platform   : ACEX10K
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This state machine manages a pulser calibration run.  The
--              state machine is started by the start command.  For each strip
--              in the range strip_min, strip_max, it will increment the
--              pulser value from 0 upto and including 255 in nsteps, and for
--              each pulser value it will pass iterations number of events.
--              Once this is done, the state machine returns the chip to
--              normal operations. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2007/01/03  1.0      cholm   Created
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cal_manager is
  port (
    clk         : in  std_logic;                      --! Clock
    rstb        : in  std_logic;                      --! Reset
    enable      : in  std_logic;                      --! Start pulser mode
    busy        : out std_logic;                      --! We're active
    clear_seq   : out std_logic;                      --! Reset VA shift reg. 
    l2          : in  std_logic;                      --! L2 trigger
    iterations  : in  std_logic_vector(15 downto 0);  --! Number of iterations
    step        : in  std_logic_vector(7 downto 0);   --! Step size 
    strip_min   : in  std_logic_vector(7 downto 0);   --! Min strip
    strip_max   : in  std_logic_vector(7 downto 0);   --! Max strip
    strip       : out std_logic_vector(7 downto 0);   --! output strip no
    pulse       : out std_logic_vector(7 downto 0);   --! Pulse size
    change      : out std_logic;
    change_busy : in  std_logic);
end entity cal_manager;

architecture rtl of cal_manager is
  type state_t is (idle,
                   setup_strip,
                   setup_pulse,
                   change_pulse,
                   wait_pulse,
                   wait_dac,
                   wait_l2,
                   done);                         --! State type
  signal state_i     : state_t;                   --! State
  signal l2_cnt_i    : integer range 0 to 2**16;  --! L2 counter
  signal strip_cnt_i : integer range 0 to 2**16;  --! Strip counter
  -- signal min_i       : integer range 0 to 2**8;   --! Min strip (as integer)
  signal max_i       : integer range 0 to 2**8;   --! Max strip (as integer);
  signal iter_i      : integer range 0 to 2**16;  --! Iterations (as integer)
  signal dpulse_i    : integer range 0 to 2**8;   --! Step size (as integer)
  signal pulse_i     : integer range 0 to 2**8;   --! Pulse (as integer)
  signal l2_old_i    : std_logic;
begin  -- architecture rtl

  -- purpose: State machine to control swithing strip and pulser
  -- type   : sequential
  -- inputs : clk, rstb, enable, iterations, nsteps, strip_min, strip_max, l2
  -- outputs: pulse, strip, change
  fsm : process (clk, rstb) is
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      strip       <= strip_min;
      state_i     <= idle;
      change      <= '0';
      pulse       <= (others => '0');
      l2_cnt_i    <= 0;
      strip_cnt_i <= 0;
      iter_i      <= 0;
      pulse_i     <= 0;
      dpulse_i    <= 0;
      -- min_i       <= 0;
      max_i       <= 0;
      busy        <= '0';
      clear_seq   <= '0';

    elsif clk'event and clk = '1' then  -- rising clock edge
      state_i  <= state_i;
      l2_old_i <= l2;

      case state_i is
        when idle                  =>
          strip         <= (others => '0');
          pulse         <= (others => '0');
          l2_cnt_i      <= 0;
          strip_cnt_i   <= 0;
          busy          <= '0';
          clear_seq     <= '0';
          if enable = '1' and step /= X"00" and step <= X"FF" then
            iter_i      <= to_integer(unsigned(iterations));  -- Cache values
            pulse_i     <= 0;
            strip_cnt_i <= to_integer(unsigned(strip_min));
            max_i       <= to_integer(unsigned(strip_max));
            dpulse_i    <= to_integer(unsigned(step));
            state_i     <= setup_strip;
            busy        <= '1';
            clear_seq   <= '1';             -- Force sequencer to reset
          end if;

        when setup_strip =>
          -- Choose only one strip. 
          strip     <= std_logic_vector(to_unsigned(strip_cnt_i, 8));
          busy      <= '1';
          clear_seq <= '0'; 
          state_i   <= setup_pulse;

        when setup_pulse =>
          -- Set the pulser value 
          pulse     <= std_logic_vector(to_unsigned(pulse_i, 8));
          l2_cnt_i  <= 0;
          change    <= '0';
          busy      <= '1';
          state_i   <= change_pulse;

        when change_pulse =>
          -- Enable change of pulse
          change  <= '1';               -- Let the DAC interface do
          busy    <= '1';               -- it's job
          state_i <= wait_pulse;
          
        when wait_pulse =>
          -- Wait for the DAC interface to be busy
          change    <= '0';
          busy      <= '1';
          if change_busy = '1' then
            state_i <= wait_dac;
          end if;

        when wait_dac =>
          -- Wait for the DAC interface to be done
          busy <= '1';
          if change_busy = '0'  then
            state_i <= wait_l2;
          end if;
          
        when wait_l2 =>
          -- Wait for a falling edge on the l2 signal. 
          busy            <= '1';
          if l2 = '0' and l2_old_i = '1' then
            l2_cnt_i      <= l2_cnt_i + 1;
          end if;
          -- Check if we had all the iterations we needed. 
          if l2_cnt_i >= iter_i then
            if pulse_i + dpulse_i > 255 then
              strip_cnt_i <= strip_cnt_i + 1;  -- If the pulser it at max,
              pulse_i     <= 0;                -- go to the next strip
              if strip_cnt_i = max_i then      -- If we're at the last strip,
                state_i   <= done;             -- end this machine.
              else
                state_i   <= setup_strip;      --         
              end if;
            elsif strip_cnt_i > max_i then     -- If we're at the last strip,
              state_i     <= done;             -- end this machine. 
            else
              pulse_i     <= pulse_i + dpulse_i;
              state_i     <= setup_pulse;      -- Increment the pulse 
            end if;
          end if;

        when done            =>
          strip   <= (others => '0');   -- We get here when we're done. 
          pulse   <= (others => '0');
          busy    <= '0';
          state_i <= idle;
        when others          => null;
      end case;
    end if;
  end process fsm;


end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package cal_manager_pack is
  component cal_manager
    port (
      clk         : in  std_logic;      --! Clock
      rstb        : in  std_logic;      --! Reset
      enable      : in  std_logic;      --! Start pulser mode
      busy        : out std_logic;      --! We're active 
      clear_seq   : out std_logic;      --! Reset VA shift reg. 
      l2          : in  std_logic;      --! L2 trigger
      iterations  : in  std_logic_vector(15 downto 0);  --! # iterations
      step        : in  std_logic_vector(7 downto 0);  --! Step size
      strip_min   : in  std_logic_vector(7 downto 0);  --! Min strip
      strip_max   : in  std_logic_vector(7 downto 0);  --! Max strip
      strip       : out std_logic_vector(7 downto 0);  --! output strip no
      pulse       : out std_logic_vector(7 downto 0);  --! Pulse size
      change      : out std_logic;
      change_busy : in  std_logic);
  end component cal_manager;
end package cal_manager_pack;
------------------------------------------------------------------------------
--
-- EOF
--
