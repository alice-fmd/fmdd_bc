------------------------------------------------------------------------------
-- Title      : Trigger handler for the FMDD
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : trigger_handler.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-09-02
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This component handles the triggers as they come from the CTP+
--              RCU or the internal trigger box.
--
--              This component can operate in two modes: normal mode, and
--              pulser mode.  The later is selected when cal_on is asserted.
--
--              Here's the normal sequence of things:
--
--              1.  Upon recieving a level 0 trigger (l0), a busy signal is
--                  sent to the Central Trigger Processor (CTP). 
--              2.  The module then waits for a specified amount of
--                  clock cycles (hold_wait).  Note, that the level 1
--                  trigger timeout counter (internal signal timeout_counter)
--                  is also incremented at this stage. Hence, the timeout
--                  counter counts from l0 trigger upto the timeout.
--              3.  When the module has waited for the specified clock
--                  cycles, it issues the hold signal, thereby
--                  freezing the charge in the VA1 circuit.  Hence, it
--                  is important that the number of cycles to wait is
--                  tuned so that the hold is issued at the peaking
--                  time of the pre-amplifier (nominal 1.5 us).
--              4.  Then the module waits until recieving a level 1 trigger.
--                  If no level 1 trigger was recieved before the time
--                  out (number of clock cycles specified in
--                  l1_timeout), the module aborts the event, and
--                  deasserts the busy line to CTP.
--              5.  Upon receiving the level 1 trigger (l1), the
--                  module checks if the sequencer (entity
--                  VA1_readout) isn't busy.  If it is busy, then we
--                  wait until it isn't.  The VA1 readout sequence is
--                  then started by asserting the output
--                  sequencer_start.
--              6.  The module then waits for the VA1 readout sequence to
--                  finish, signalled by the VA1_readout module
--                  de-asserting sequencer_busy.  When this happens,
--                  the trigger is processed and accepted, and pushed
--                  onto the ALTRO ADC chip.
--
--              In calibration mode (signaled by cal_on), the module
--              behaves more or less like the above, except that for
--              in 2.  Here, the module asserts the calibration pulser
--              enable (pulser_enable) so that a calibration pulse is
--              pushed onto the inputs of the VA1 chips.  Also, the
--              module waits an additional amount of clock cycles
--              (specified in cal_delay), to allow the VA1 shaper to
--              shape the signal.  The test_on signal is asserted
--              through out the hold sequence (as long as busy is
--              enabled).
--
--              The time constrains involved for this module are:
--
--              - L0 arrive at chip after bunch crossing:    1.2 us
--              - L1 arrive at chip after bunch crossing:    6.5 us
--              - VA1 shapping time:                         1.5 us
--
--              Hence, we need to wait 300ns from the level 1 trigger
--              until we assert the hold signal.  This corresponds to
--              12 clock cycles of a 40MHz clock (1 clock cycle is 25
--              ns), so the input regiser hold_wait should have the
--              value 12 (or 0xc in hex).
--
--              The time between we recieve the l0 and the l1 is 5.3
--              us, or 212 40Mhz clock cycles.  Hence the register
--              l1_timeout should contain the value 212 (or 0xd4 in
--              hex).
--
--              In calibration mode, we need to postpone the hold
--              signal the time it normally takes for the l0 trigger
--              to reach the chip: 1.2 us.  That corresponds to 48
--              40Mhz clock cycles, so the constant cal_delay should
--              be 48 (or 0x30 in hex).
--
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2005/03/31  1.0      tiago   Created
-- 2005/04/04  1.1      tiago   clk to 40 MHz added sequencer_start,
--                              sequencer_busy ports.
-- 2005/06/30  1.2      cholm   Added some documentation. 
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- library work; use work.all;

----------------------------------------------------------------------
entity trigger_handler is
  port (
    clk             : in  std_logic;    --! Clock
    rstb            : in  std_logic;    --! Async reset
    l0              : in  std_logic;    --! Level 0 Trigger (active high)
    l1              : in  std_logic;    --! Level 1 trigger (active low)
    l2              : in  std_logic;    --! Level 2 trigger (active low)
    l2r             : in  std_logic;    --! L2 reject (timeout)
    l1r             : in  std_logic;    --! L1 reject (timeout)
    hold            : out std_logic;    --! Hold to VA1s
    busy            : out std_logic;    --! Busy to CTP
    -- prog values for delay
    hold_wait       : in  std_logic_vector(15 downto 0);  --! Wait to hold
    cal_delay       : in  std_logic_vector(15 downto 0);  --! Extra delay (cal)
    -- Start and stop of sequencer
    sequencer_start : out std_logic;    --! Start sequencer
    sequencer_busy  : in  std_logic;    --! Sequencer is busy
    sequencer_clear : out std_logic;    --! Clear sequencer
    -- Calibration mode
    cal_on          : in  std_logic;    --! Calibration mode
    pulser_enable   : out std_logic;    --! Step pulser single - active low
    -- Status
    incomplete      : out std_logic);   --! Status word
end trigger_handler;


----------------------------------------------------------------------
architecture rtl2 of trigger_handler is
  type state_t is (idle,
                   cal_extra,
                   level_0,
                   holding,
                   check_seq,
                   seq_start,
                   sequencer);
  signal state_i       : state_t;
  signal cnt_extra_i   : integer range 0 to 2**16-1;
  signal cnt_hold_i    : integer range 0 to 2**16-1;
  
  
  -- 1.2 us / 50 ns
  -- constant cal_delay      : integer range 0 to 2**16-1 := 48;
begin  -- rtl
  
  -- purpose: the state machine
  handler_proc : process (clk,
                          rstb,
                          hold_wait)
  begin  -- process handler_proc
    -- asynchronous reset (active low)
    if rstb = '0' then
      state_i         <= idle;
      --  signal reset values
      busy            <= '0';
      hold            <= '0';
      sequencer_start <= '0';
      pulser_enable   <= '1';
      incomplete      <= '0';
      sequencer_clear <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- default values
      -- busy            <= '0';
      -- hold            <= '0';
      -- sequencer_start <= '0';

      -- state switch logic
      case state_i is
        when idle =>
          -- Update values
          busy            <= '0';
          hold            <= '0';
          cnt_hold_i      <= to_integer(unsigned(hold_wait)) - 2;
          cnt_extra_i     <= to_integer(unsigned(cal_delay));
          pulser_enable   <= '1';
          sequencer_start <= '0';
          sequencer_clear <= '0';
          incomplete      <= '0';
          
          -- When we get a L0 trigger (active high), change state.
          if l0 = '1' then              -- 
            -- If calibration mode is turned on, add an additional delay to the
            -- hold wait, and turn on the calibration pulser, to make the
            -- voltage step. 
            if cal_on = '1' then
              pulser_enable <= '0';
              state_i <= cal_extra;
            else
              state_i <= level_0;
            end if;
          end if;

        when cal_extra =>
          busy        <= '1';
          cnt_extra_i <= cnt_extra_i - 1;
          hold        <= '0';
          if cnt_extra_i = 1 then
            state_i <= level_0;
          else
            state_i <= cal_extra;
          end if;
          
        when level_0 =>
          -- When we got a L0 trigger, flag this detector as busy, decrement
          -- the counters by 1. 
          cnt_hold_i      <= cnt_hold_i    - 1;
          busy            <= '1';
          hold            <= '0';
          sequencer_start <= '0';

          -- If we reach the timeout, abort the event.   Otherwise, if we have
          -- waited long enough for the hold signal, assert that signal. 
          if l1r = '1' then
            report "L0 timed out" severity warning;
            state_i <= idle;
          elsif l2r = '1' then
            state_i <= idle;
-- This may be bad, since the L0 could be longer than 25 ns.
--          elsif l1 = '0' then
--            report "L1 unexpected at this time - too early" severity warning;
--            incomplete <= '1';
--            state_i    <= idle;
          elsif cnt_hold_i <= 1 then
            hold    <= '1';
            state_i <= holding;
          end if;

        when holding =>
          -- Wait until we get a L1, or we reach the timeout.
          busy            <= '1';
          hold            <= '1';
          sequencer_start <= '0';
          if l1 = '0' then
            state_i <= check_seq;
          elsif l1r = '1' then
            report "L0 timed out" severity warning;
            state_i <= idle;
          elsif l2r = '1' then
            report "Holding too long" severity warning;
            state_i <= idle;
          else
            state_i <= holding;
          end if;

        when check_seq =>
          -- Check that the sequencer isn't busy.  Probably not really needed,
          -- but it's good form to make a real hand-shake here. 
          busy              <= '1';
          hold              <= '1';
          sequencer_start   <= '0';
          -- state_i           <= state_i;
          if l2r = '1' then
            report "Sequencer busy too long" severity warning;
            sequencer_clear <= '1';
            state_i         <= idle;
          elsif sequencer_busy = '0' then
            -- Hold is used as an enable for passing Level 1 trigger onto the
            -- ALTRO's.  We need to take it down really quickly. 
            -- hold          <= '0';
            state_i         <= seq_start;
          else
            state_i <= check_seq;
          end if;

        when seq_start =>
          -- Start the VA1 readout, and wait until it's started.
          busy            <= '1';
          hold            <= '1';
          sequencer_start <= '1';
          state_i         <= state_i;
          if l2r = '1' then
            report "Sequencer timed out" severity warning;
            sequencer_clear <= '1';
            state_i         <= idle;
          elsif sequencer_busy = '1' then
            sequencer_start <= '0';
            state_i         <= sequencer;
          else
            state_i <= seq_start;
          end if;

        when sequencer =>
          -- Wait for the VA1 read out to finish. 
          busy              <= '1';
          hold              <= '1';
          sequencer_start   <= '0';
          if l2r = '1' or l2 = '0' then
            report "Incomplete read-out (Bad L2 timeout)" severity warning;
            incomplete      <= '1';
            sequencer_clear <= '1';
            state_i         <= idle;
          elsif sequencer_busy = '0' then
            state_i         <= idle;
          else
            state_i <= sequencer;
          end if;

        when others =>
          busy            <= '0';
          hold            <= '0';
          sequencer_start <= '0';
          sequencer_clear <= '0';
          state_i         <= idle;

      end case;
    end if;
  end process handler_proc;
end rtl2;


----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package trigger_handler_pack is
  component trigger_handler
    port (clk             : in  std_logic;  -- ! Clock
          rstb            : in  std_logic;  -- ! Async reset
          l0              : in  std_logic;  -- ! Level 0 Trigger (active high)
          l1              : in  std_logic;  -- ! Level 1 trigger (active low)
          l2              : in  std_logic;  -- ! Level 1 trigger (active low)
          l2r             : in  std_logic;  -- ! L2 reject (timeout)
          l1r             : in  std_logic;  -- ! L1 reject (timeout)
          hold            : out std_logic;  -- ! Hold to VA1s
          busy            : out std_logic;  -- ! Busy to CTP
          hold_wait       : in  std_logic_vector(15 downto 0);  -- ! wait to ho
          cal_delay       : in  std_logic_vector(15 downto 0);  --! Extra delay
          sequencer_start : out std_logic;  -- ! Start sequencer
          sequencer_busy  : in  std_logic;  -- ! Sequencer is busy
          sequencer_clear : out std_logic;  -- ! Clear sequencer
          cal_on          : in  std_logic;  -- ! Calibration mode
          pulser_enable   : out std_logic;  -- ! Step pulser single - low
          incomplete      : out std_logic);  -- ! Status word
  end component;
end trigger_handler_pack;

----------------------------------------------------------------------
--
-- EOF
--
