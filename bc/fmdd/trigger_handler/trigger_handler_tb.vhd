------------------------------------------------------------------------------
-- Title      : Test bench of trigger handler
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : trigger_handler_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-08-15
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/26  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.trigger_handler_pack.all;
use work.fmdd_simul_pack.all;
use work.glitch_filter_pack.all;

------------------------------------------------------------------------------
entity trigger_handler_tb is
end entity trigger_handler_tb;

------------------------------------------------------------------------------
architecture test of trigger_handler_tb is
  signal clk_i             : std_logic := '0';  -- Clock
  signal rstb_i            : std_logic := '0';  -- Async reset
  signal l0_i              : std_logic := '0';  -- Level 0 Trigger (+)
  signal l1_i              : std_logic := '1';  -- Level 1 trigger (-)
  signal l2_i              : std_logic := '1';  -- Level 2 trigger (-)
  signal l0_ii             : std_logic;  -- Glitch filtered L0
  signal hold_i            : std_logic;  -- Hold to VA1s
  signal busy_i            : std_logic;  -- Busy to CTP
  signal hold_wait_i       : std_logic_vector(15 downto 0);  -- wait to hold
  signal l1_timeout_i      : std_logic_vector(15 downto 0);  -- L1 time out
  signal l2_timeout_i      : std_logic_vector(15 downto 0);  -- L2 time out 
  signal sequencer_start_i : std_logic;  -- Start sequencer
  signal sequencer_busy_i  : std_logic;  -- Sequencer is busy
  signal sequencer_clear_i : std_logic;  -- Sequencer reset
  signal cal_on_i          : std_logic := '0';  -- Calibration mode
  signal pulser_enable_i   : std_logic;  -- Step pulser single - active low
  signal incomplete_i      : std_logic;  -- Status word
  signal l2r_i             : std_logic;  -- L2 reject (timeout)
  signal l21_i             : std_logic;  -- L2 reject (timeout)
  signal l2_to_i           : std_logic;
  signal l1_to_i           : std_logic;

  type st_resp_t is (seq_wait, seq_delay, seq_setup, seq_busy);
  signal resp_state_i      : st_resp_t;
  signal resp_count_i      : integer;

  type st_mon_t is (wait_l0, wait_l1, wait_l2);
  signal mon_state_i       : st_mon_t;
  signal mon_count_i       : integer;

begin  -- architecture test

  filter : entity work.glitch_filter(rtl3)
    port map (
      clk    => clk_i,
      rstb   => rstb_i,
      input  => l0_i,
      output => l0_ii);

  dut : entity work.trigger_handler(rtl2)
    port map (
        clk             => clk_i,              -- in  Clock
        rstb            => rstb_i,             -- in  Async reset
        l0              => l0_ii,              -- in  Level 0 Trigger (+)
        l1              => l1_i,               -- in  Level 1 trigger (-)
        l2              => l2_i,               -- in  Level 2 trigger (-)
        hold            => hold_i,             -- out Hold to VA1s
        busy            => busy_i,             -- out Busy to CTP
        hold_wait       => hold_wait_i,        -- in  wait to hold
        l1_timeout      => l1_timeout_i,       -- in  L1 time out
        l2_timeout      => l2_timeout_i,       -- in  L2 time out 
        sequencer_start => sequencer_start_i,  -- out Start sequencer
        sequencer_busy  => sequencer_busy_i,   -- in  Sequencer is busy
        sequencer_clear => sequencer_clear_i,  -- out Clear sequencer
        cal_on          => cal_on_i,           -- in  Calibration mode
        pulser_enable   => pulser_enable_i,    -- out Step pulser single (-)
        incomplete      => incomplete_i,       -- out Status word
        l2r             => l2r_i,
        l1r             => l1r_i);

  -- stimuli
  -- purpose: set stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: ?
  stimuli : process
    variable l2_timeout_t : time := 0 ns;
  begin  -- process
    change_timeouts(hold_delay => 1.575 us,
                    l1_wait    => 6.725 us,
                    l2_wait    => 83.2 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);
    wait for 100 ns;
    rstb_i <= '1';

    --
    report "making a normal trigger sequence" severity note;
    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l0(l0 => l0_i);
    wait for 1 us;
    make_l1(l1  => l1_i,
            clk => clk_i,
            len => 25 ns);
    change_timeouts(hold_delay => 2.0 us,
                    l1_wait    => 7.45 us,
                    l2_wait    => 4.5 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);
    wait_for_busy_or_timeout(busy  => busy_i,
                             l1_to => l1_to_i,
                             l2_to => l2_to_i);
    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l2(l2  => l2_i,
            clk => clk_i,
            len => 25 ns);
    wait for 0.5 us;

    wait for 1 us;
    report "Making a trigger sequence without l2a" severity note;
    l2_timeout_t := to_integer(unsigned(l2_timeout_i)) * PERIOD;
    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l0(l0 => l0_i);
    wait for 2.0 us - L0_DELAY + PERIOD;
    make_l1(l1  => l1_i,
            clk => clk_i,
            len => 25 ns);
    report "waiting for " & time'image(l2_timeout_t) severity note;
    wait for l2_timeout_t - L0_DELAY + 16 * PERIOD;
    wait for 3 * PERIOD - 0.5 ns; 
    make_l2(l2  => l2_i,
            clk => clk_i,
            len => 25 ns);
    

    
    wait for 1 us;
    report "Making another trigger sequence" severity note;
    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l0(l0 => l0_i);
    wait for 2.0 us - L0_DELAY + PERIOD;
    make_l1(l1  => l1_i,
            clk => clk_i,
            len => 25 ns);
    wait_for_busy_or_timeout(busy  => busy_i,
                             l1_to => l1_to_i,
                             l2_to => l2_to_i);
    wait for 1 us;
    rstb_i <= '0';
    report "Reseting" severity note;
    wait for 100 ns;
    rstb_i <= '1';

    report "Making a trigger sequence with early L1" severity note;
    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l0(l0                     => l0_i);
    wait for 100 ns;
    make_l1(l1                     => l1_i,
            clk                    => clk_i);
    wait_for_busy_or_timeout(busy  => busy_i,
                             l1_to => l1_to_i,
                             l2_to => l2_to_i);
    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l2(l2  => l2_i,
            clk => clk_i,
            len => 25 ns);
    wait for 1 us;

    change_timeouts(hold_delay => 1.5 us,
                    l1_wait    => 6.5 us,
                    l2_wait    => 88 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);

    --

    report "making a normal trigger sequence in cal mode" severity note;
    wait until rising_edge(clk_i);
    cal_on_i <= '1';
    wait for PERIOD - 0.5 ns;
    make_l0(l0 => l0_i);
    wait for 5 us;
    make_l1(l1  => l1_i,
            clk => clk_i,
            len => 25 ns);
    change_timeouts(hold_delay => 2.0 us,
                    l1_wait    => 7.45 us,
                    l2_wait    => 4.5 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);
    wait_for_busy_or_timeout(busy  => busy_i,
                             l1_to => l1_to_i,
                             l2_to => l2_to_i);
    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l2(l2  => l2_i,
            clk => clk_i,
            len => 25 ns);
    cal_on_i <= '0';
    wait for 0.5 us;

    report "Making an overlapping  L0 trigger sequence" severity note;
    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l0(l0                     => l0_i);
    wait for 100 ns;
    wait until rising_edge(clk_i);
    make_l0(l0                     => l0_i);
    wait for 100 ns;
    make_l1(l1                     => l1_i,
            clk                    => clk_i);
    wait_for_busy_or_timeout(busy  => busy_i,
                             l1_to => l1_to_i,
                             l2_to => l2_to_i);

    wait until rising_edge(clk_i);
    wait for PERIOD - 0.5 ns;
    make_l2(l2  => l2_i,
            clk => clk_i,
            len => 25 ns);

    wait;
  end process;

  -- Simulate sequencer state 
  -- sequencer_busy_i <= '0';               -- sequencer_start_i;
  seq_resp : process (clk_i, rstb_i, sequencer_clear_i)
  begin  -- process seq_resp
    if rstb_i = '0' then                    -- asynchronous reset (active low)
      resp_state_i         <= seq_wait;
      resp_count_i         <= 10;
      sequencer_busy_i     <= '0';
    elsif sequencer_clear_i = '1' then
      resp_state_i         <= seq_wait;
      resp_count_i         <= 10;
      sequencer_busy_i     <= '0';      
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      case resp_state_i is
        when seq_wait =>
          sequencer_busy_i <= '0';
          resp_count_i     <= 10;
          if sequencer_start_i = '1' then
            resp_state_i   <= seq_delay;
          end if;

        when seq_delay =>
          sequencer_busy_i <= '0';
          resp_count_i     <= resp_count_i - 1;
          if resp_count_i = 0 then
            resp_state_i   <= seq_setup;
          end if;

        when seq_setup =>
          resp_count_i     <= 10;
          resp_state_i     <= seq_busy;
          sequencer_busy_i <= '1';

        when seq_busy =>
          sequencer_busy_i <= '1';
          resp_count_i     <= resp_count_i - 1;
          if resp_count_i = 0 then
            resp_state_i   <= seq_wait;
          end if;

        when others =>
          resp_state_i <= seq_wait;
      end case;
    end if;
  end process seq_resp;

  -- purpose: Monitor time
  -- type   : sequential
  -- inputs : clk_i, rstb_i, l0_i, l1_i
  timeout : process (clk_i, rstb_i)
  begin  -- process timeout
    if rstb_i = '0' then                    -- asynchronous reset (active low)
      mon_state_i <= wait_l0;
      l1_to_i     <= '0';
      l2_to_i     <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      mon_state_i <= mon_state_i;
      l1_to_i     <= '0';
      l2_to_i     <= '0';

      case mon_state_i is
        when wait_l0 =>
          mon_count_i   <= 0;
          if l0_i = '1' then
            mon_state_i <= wait_l1;
          end if;

        when wait_l1 =>
          mon_count_i <= mon_count_i + 1;
          if l1_i = '0' then
            mon_state_i <= wait_l2;
          elsif mon_count_i >= to_integer(unsigned(l1_timeout_i)) then
            l1_to_i <= '1';
          end if;

        when wait_l2 =>
          mon_count_i <= mon_count_i + 1;
          if busy_i = '0' then
            mon_state_i <= wait_l0;            
          elsif mon_count_i >= to_integer(unsigned(l2_timeout_i)) then
            l2_to_i <= '1';
          end if;
          
        when others =>
          mon_state_i <= wait_l0;
          
      end case;
    end if;
  end process timeout;
  -- clock
  process
  begin  -- process
    clk_i <= '0';
    wait for PERIOD/2;
    clk_i <= '1';
    wait for PERIOD/2;
  end process;
end architecture test;
------------------------------------------------------------------------------
-- 
-- EOF
--
