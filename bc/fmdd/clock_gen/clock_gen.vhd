-------------------------------------------------------------------------------
-- Title      : clock generator
-- Project    : ALICE FMD Digitizer Board Controller
-------------------------------------------------------------------------------
-- File       : clock_gen.vhd
-- Author     : Daniel G. Kirschner  <Daniel.G.Kirschner@physik.uni-giessen.de>
-- Company    : NBI
-- Created    : 2005-04-15
-- Last update: 2008-08-22
-- Platform   : ACEX1K - EP1K100QC208-1
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description:
--
-- This is module to make derived divided clocks from the input clock.
--
-- It can be configured through external registers: The number of divisions
-- (div_count) and phase shift (div_phase).  Note, that care is taken to update
-- the internal registers (div_count_reg, div_phase_reg) only on a rising (or
-- falling, depending on the parameter OUT_POLARITY) edge of the derived clock.
--
-- Suppose you have a 40MHz input clock (clk), and set div_count to 4, then you
-- get a 10MHz clock on the output (clk_o).  Setting div_phase to 2 shifts this
-- clock by 50ns.
--
-- Note, that it only makes sense to do even divisions.
--
-- The output clk_o_en is asserted on the rising (or falling, depending on
-- OUT_POLARITY) edge of the output clock clk_o.
-- 
-------------------------------------------------------------------------------
-- Copyright (c) 2005 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2005-04-15  1.0      dk      Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity clock_gen is
  generic (
    WIDTH  : integer   := 8;            -- Width of divider
    IN_POL : std_logic := '1';          -- rising edge 
    -- safety
    CATCH  : string    := "Y");         -- lots of logic
  port (
    -- Clocks in
    clk          : in  std_logic;       -- Master input clock
    rstb         : in  std_logic;       -- Master Rstb
    sync         : in  std_logic;       -- Syncronisation
    -- Clocks out
    clk_en_o     : out std_logic;       -- Rising edge
    clk_o        : out std_logic;       -- Gen. clock
    -- Register set
    out_polarity : in  std_logic := '1';                     -- Polarity
    div_phase    : in  std_logic_vector(WIDTH-1 downto 0);   -- div
    div_count    : in  std_logic_vector(WIDTH-1 downto 0));  -- off

end clock_gen;


architecture rtl5 of clock_gen is
  signal   phase_store_i : unsigned(WIDTH-1 downto 0);
  signal   div_store_i   : unsigned(WIDTH-1 downto 0);
  signal   new_values_i  : boolean;
  signal   cnt_i         : integer range 0 to 2**WIDTH := 0;
  signal   max_i         : integer range 0 to 2**WIDTH := 16;
  signal   idx_i         : integer range 0 to 2**WIDTH := 0;
  constant NPHASES       : integer                     := 32;
  signal   phases_i      : std_logic_vector(NPHASES-1 downto 0);
  signal   clk_i         : std_logic;
  signal   sync_i        : std_logic;
  signal   sync_ii       : std_logic;
  signal   ack_i         : std_logic;
begin  -- rtl5
  -- purpose: Store new values of division and phase
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  store_values : process (clk, rstb)
  begin  -- process store_values
    if rstb = '0' then                  -- asynchronous reset (active low)
      phase_store_i <= unsigned(div_phase);
      div_store_i   <= unsigned(div_count);
      new_values_i  <= true;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if (phase_store_i /= unsigned(div_phase)) or
        (div_store_i /= unsigned(div_count)) then
        phase_store_i <= unsigned(div_phase);
        div_store_i   <= unsigned(div_count);
        new_values_i  <= true;
      elsif ack_i = '1' then
        new_values_i <= false;
      end if;
    end if;
  end process store_values;
  
  clk_gen : process (clk,rstb)
  begin  -- process clk_gen
    if rstb = '0' then
      sync_i  <= sync;
      sync_ii <= sync_i;
      ack_i <= '0';
      cnt_i <= to_integer(div_store_i);
      max_i <= to_integer(div_store_i);
      clk_i <= '0';
      idx_i <= to_integer(phase_store_i);
    elsif clk'event and clk = '1' then     -- rising clock edge
      sync_i  <= sync;
      sync_ii <= sync_i;
      
      -- Count clock cycles
      if (new_values_i) and (sync_i = '0') and (sync_ii = '1') then
        -- Register delays -> -2
        -- cnt_i <= to_integer(div_store_i) - 2;
        max_i <= to_integer(div_store_i);
        idx_i <= to_integer(phase_store_i);
        ack_i <= '1';
      elsif cnt_i <= 1 then
        -- cnt_i <= max_i;
        ack_i <= '0';
      else
        -- cnt_i <= cnt_i - 1;
        ack_i <= '0';
      end if;

      if cnt_i <= 1 then
        cnt_i <= max_i;
      else
        cnt_i <= cnt_i - 1;
      end if;
      
      -- Output clock
      if cnt_i > max_i / 2 then
        clk_i    <= '1';
      else
        clk_i    <= '0';
      end if;

    end if;
  end process clk_gen;

  -- Generate phase shifted clock
  phase_gen : process (clk)
    variable i : integer range 0 to NPHASES - 1;
  begin  -- process phase_gen
    if rstb = '0' then
      phases_i <= (others => '0');
      clk_en_o <= '0';
      clk_o    <= '0';
    elsif clk'event and clk = '1' then     -- rising clock edge
      phases_i(0) <= clk_i;
      for i in 1 to NPHASES-1 loop
        phases_i(i) <= phases_i(i-1);
      end loop;  -- i
      clk_o <= phases_i(idx_i);
      assert idx_i+1 < NPHASES-1 report "Invalid phase index: "
        & integer'image(idx_i+1) & " - valid range is [0,"
        & integer'image(NPHASES-1) & "]" severity warning;

      clk_en_o <= phases_i(idx_i) and not phases_i(idx_i+1);
    end if;
  end process phase_gen;
  -- clk_o <= clk_i;
  -- clk_en_o <= '0';
  
end rtl5;
----------------------------------------------------------------------
--! This architecture (implementation) defines a clock generator where the
--! phase of the generated clock can be tuned relative to the @a sync input
--! signal. @a sync is assumed to be a clock-like thing e.g., the sample clock
--! of the ALTRO's.  
architecture rtl4 of clock_gen is
  type   state_t is (idle, new_values, emit, wait_ack);
  signal st_i          : state_t;
  signal ack_new_i     : boolean;
  signal old_sync_i    : std_logic;     -- Previous value of sync
  signal cnt_i         : integer range 0 to WIDTH**2-1;
  signal phase_i       : integer range 0 to WIDTH**2;
  signal div_i         : integer range 0 to WIDTH**2;
  signal phase_store_i : unsigned(WIDTH-1 downto 0);
  signal div_store_i   : unsigned(WIDTH-1 downto 0);
  signal clk_i         : std_logic;
  signal clk_en_i      : std_logic;
  signal new_value     : boolean;
  signal next_cnt_i    : integer range 0 to WIDTH**2-1;
  signal next_phase_i  : integer range 0 to WIDTH**2;
  signal next_div_i    : integer range 0 to WIDTH**2;

begin  -- rtl4
  -- purpose: Check if we're using a combinatorial clock 
  -- catch divide by one and zero (otional)
  -- type   : combinational
  -- inputs : Clock
  -- outputs: clk_o, clk_en_o
  combinatorics : process (clk_i, clk_en_i)
  begin  -- process combinatorical_clocker
    clk_o    <= clk_i;
    clk_en_o <= clk_en_i;
  end process combinatorics;

  -- purpose: Update settings.  This process exists only to make the code meet
  --          the timing requirements, by doing the calculations piece-meal at
  --          different clock cycles in different states. 
  -- type   : sequential
  -- inputs : clk, rstb, div_phase, div_count
  -- outputs: new_value, next_div_i, next_cnt_i
  updater : process (clk, rstb)
  begin  -- process updater
    if rstb = '0' then                  -- asynchronous reset (active low)
      phase_store_i <= unsigned(div_phase);
      div_store_i   <= unsigned(div_count);
      next_div_i    <= to_integer(unsigned(div_count));
      next_phase_i  <= to_integer(unsigned(div_phase));
      next_cnt_i    <= 0;
      new_value     <= false;
      st_i          <= new_values;
    elsif clk'event and clk = '1' then  -- rising clock edge
      case st_i is
        when idle =>
          new_value  <= false;
          next_cnt_i <= 0;
          if (unsigned(div_phase) /= phase_store_i or
              unsigned(div_count) /= div_store_i) then
            phase_store_i <= unsigned(div_phase);
            div_store_i   <= unsigned(div_count);
            next_div_i    <= to_integer(unsigned(div_count));
            next_phase_i  <= to_integer(unsigned(div_phase));
            st_i          <= new_values;
          else
            next_div_i   <= 0;
            next_phase_i <= 0;
            st_i         <= idle;
          end if;

        when new_values =>
          next_div_i <= next_div_i;
          -- next_phase <= next_phase mod (next_div / 2)l
          if next_phase_i >= next_div_i / 2 then
            next_phase_i <= next_phase_i - next_div_i / 2;
          end if;
          next_cnt_i <= 0;
          new_value  <= false;
          st_i       <= emit;

        when emit =>
          next_div_i   <= next_div_i;
          next_phase_i <= next_phase_i;
          -- +1 for reg. delay
          next_cnt_i   <= next_div_i - next_phase_i + 1;
          new_value    <= true;
          st_i         <= wait_ack;

        when wait_ack =>
          next_div_i   <= next_div_i;
          next_phase_i <= next_phase_i;
          next_cnt_i   <= next_cnt_i;
          new_value    <= true;
          if ack_new_i then
            new_value <= false;
            st_i      <= idle;
          else
            new_value <= true;
            st_i      <= wait_ack;
          end if;
          
        when others =>
          next_div_i   <= 0;
          next_phase_i <= 0;
          next_cnt_i   <= 0;
          new_value    <= false;
          st_i         <= idle;
      end case;
    end if;
  end process updater;

  -- purpose: Produce the clock
  -- type   : sequential
  -- inputs : clk, rstb, sync
  -- outputs: clk_i, clk_en_i
  clocker : process (clk, rstb)
    -- variable next_cnt_i   : integer range 0 to WIDTH**2-1;
    -- variable next_phase_i : integer range 0 to WIDTH**2;
    -- variable next_div_i   : integer range 0 to WIDTH**2;
  begin  -- process clocker
    if rstb = '0' then                  -- asynchronous reset (active low)
      clk_i         <= '0';
      clk_en_i      <= '0';
      old_sync_i    <= '0';
      cnt_i         <= 0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      old_sync_i <= sync;

      if new_value and old_sync_i = '0' and sync = '1'then
        div_i     <= next_div_i;
        if next_cnt_i >= next_div_i  then
          cnt_i <= next_cnt_i - next_div_i;
        else
          cnt_i <= next_cnt_i;
        end if;
        -- cnt_i     <= next_cnt_i mod next_div_i;
        ack_new_i <= true;
      elsif cnt_i >= div_i - 1 then
        cnt_i     <= 0;
        ack_new_i <= false;
      else
        cnt_i     <= cnt_i + 1;
        ack_new_i <= false;
      end if;  -- new_value

      if cnt_i = 0 then
        clk_en_i <= '1';
        clk_i    <= '1';
      elsif cnt_i <= (div_i / 2) - 1 then
        clk_en_i <= '0';
        clk_i    <= '1';
      else
        clk_en_i <= '0';
        clk_i    <= '0';
      end if;  -- cnt_i < (div_i / 2) - 1
    end if;  -- clk'event
  end process clocker;

end rtl4;

-------------------------------------------------------------------------------
architecture rtl of clock_gen is
  signal div_phase_store_i : unsigned(WIDTH-1 downto 0);
  signal div_count_store_i : unsigned(WIDTH-1 downto 0);
  signal div_phase_i       : integer range 0 to WIDTH**2;
  signal div_count_i       : integer range 0 to WIDTH**2;
  signal out_polarity_i    : std_logic := '1';
  signal new_phase         : boolean;
  signal clk_fast_i        : std_logic;
  signal clk_en_i          : std_logic;
  signal clk_cnt_i         : integer range 0 to WIDTH**2-1;
  signal div_en_i          : std_logic;
  signal div_fast_i        : std_logic;
  signal div_cnt_i         : integer range 0 to WIDTH**2-1;
begin  -- rtl
  -- purpose: Check if we're using a combinatorial clock 
  -- catch divide by one and zero (otional)
  -- type   : combinational
  -- inputs : Clock
  -- outputs: clk_o, clk_en_o
  combinatorics : process (clk_fast_i, clk_en_i)
  begin  -- process combinatorical_clocker
    clk_o    <= clk_fast_i;
    clk_en_o <= clk_en_i;
  end process combinatorics;

  -- purpose: Divide the input clock 
  -- type   : sequential
  -- inputs : Clk, RSTB
  -- outputs: 
  clk_divide : process (clk, rstb,
                        out_polarity_i,
                        out_polarity,
                        div_count, div_phase)

    variable new_values : boolean := false;
  begin  -- process clk_divide
    -- asynchronous rstb (active low)
    if rstb = '0' then
      clk_cnt_i         <= 0;           -- to_integer(unsigned(div_phase));
      clk_en_i          <= not out_polarity_i;
      clk_fast_i        <= not out_polarity_i;
      div_cnt_i         <= 0;
      div_en_i          <= not out_polarity_i;
      div_fast_i        <= not out_polarity_i;
      out_polarity_i    <= out_polarity;
      div_phase_store_i <= (others => '0');  -- unsigned(div_phase);
      div_count_store_i <= (others => '0');  -- unsigned(div_count);
      div_phase_i       <= 0;           -- to_integer(unsigned(div_phase));
      div_count_i       <= 0;           -- to_integer(unsigned(div_count));
      report
        " div_count = "&integer'image(div_count_i) &
        " div_phase = "&integer'image(div_phase_i)
        severity note;
    elsif clk'event and clk = IN_POL then    -- rising clock edge
      if (unsigned(div_phase) /= div_phase_store_i or
          unsigned(div_count) /= div_count_store_i) then
        div_phase_store_i <= unsigned(div_phase);
        div_count_store_i <= unsigned(div_count);
        new_phase         <= true;
      end if;

      div_fast_i <= div_fast_i;
      div_cnt_i  <= div_cnt_i+1;

      -- Divide the clock 
      if div_cnt_i >= div_count_i-1 then
        div_fast_i     <= out_polarity_i;
        div_cnt_i      <= 0;
        -- get settings for next cycle
        out_polarity_i <= out_polarity;
        div_phase_i    <= to_integer(div_phase_store_i);
        div_count_i    <= to_integer(div_count_store_i);
        new_phase      <= false;
        new_values     := new_phase;
      elsif div_cnt_i = (div_count_i-1)/2 then
        div_cnt_i  <= div_cnt_i+1;
        div_fast_i <= not out_polarity_i;
      else
        div_cnt_i <= div_cnt_i+1;
      end if;

      -- Phase shift clock 
      if new_values then
        if div_phase_store_i = 0 then
          clk_cnt_i <= 0;
        else
          clk_cnt_i <= to_integer(div_count_store_i - div_phase_store_i);
        end if;
        new_values := false;
      else
        if clk_cnt_i >= div_count_i-1 then
          clk_cnt_i <= 0;
        else
          clk_cnt_i <= (clk_cnt_i + 1);
        end if;
        -- clk_cnt_i <= (clk_cnt_i + 1) mod div_count_i;
      end if;

      -- make output clock
      if clk_cnt_i >= div_count_i-1 then
        -- report "clk_cnt_i is at max" severity note;
        clk_en_i   <= out_polarity_i;
        clk_fast_i <= out_polarity_i;
      elsif clk_cnt_i = (div_count_i-1)/2 then
        clk_fast_i <= not out_polarity_i;
      else
        clk_en_i <= not out_polarity_i;
      end if;

      if CATCH = "Y" then
        if (unsigned(div_count) = 1 or
            unsigned(div_phase) > unsigned(div_count) or
            unsigned(div_count) = 0) then
          report "Bad setting" severity warning;
          clk_fast_i <= not out_polarity_i;
          clk_en_i   <= not out_polarity_i;
        end if;
      end if;
    end if;
  end process clk_divide;
end rtl;

----------------------------------------------------------------------

architecture rtl2 of clock_gen is
  signal div_phase_store_i : unsigned(WIDTH-1 downto 0);
  signal div_count_store_i : unsigned(WIDTH-1 downto 0);
  signal div_phase_i       : integer range 0 to WIDTH**2;
  signal div_count_i       : integer range 0 to WIDTH**2;
  signal out_polarity_i    : std_logic := '1';
  signal new_phase         : boolean;
  signal clk_fast_i        : std_logic;
  signal clk_en_i          : std_logic;
  signal clk_cnt_i         : integer range 0 to WIDTH**2-1;
  signal div_en_i          : std_logic;
  signal div_fast_i        : std_logic;
  signal div_cnt_i         : integer range 0 to WIDTH**2-1;

begin  -- rtl2
  -- purpose: Check if we're using a combinatorial clock 
  -- catch divide by one and zero (otional)
  -- type   : combinational
  -- inputs : Clock
  -- outputs: clk_o, clk_en_o
  combinatorics : process (clk_fast_i, clk_en_i)
  begin  -- process combinatorical_clocker
    clk_o    <= clk_fast_i;
    clk_en_o <= clk_en_i;
  end process combinatorics;

  -- purpose: Divide the input clock 
  -- type   : sequential
  -- inputs : Clk, RSTB
  -- outputs: 
  clk_divide : process (clk, rstb,
                        out_polarity_i,
                        out_polarity,
                        div_count, div_phase)

    -- variable new_values : boolean := false;
  begin  -- process clk_divide
    -- asynchronous rstb (active low)
    if rstb = '0' then
      clk_cnt_i         <= to_integer(unsigned(div_phase));
      clk_en_i          <= not out_polarity_i;
      clk_fast_i        <= not out_polarity_i;
      div_cnt_i         <= 0;
      div_en_i          <= not out_polarity_i;
      div_fast_i        <= not out_polarity_i;
      out_polarity_i    <= out_polarity;
      div_phase_store_i <= unsigned(div_phase);
      div_count_store_i <= unsigned(div_count);
      div_phase_i       <= to_integer(unsigned(div_phase));
      div_count_i       <= to_integer(unsigned(div_count));
    elsif clk'event and clk = IN_POL then  -- rising clock edge
      if (unsigned(div_phase) /= div_phase_store_i or
          unsigned(div_count) /= div_count_store_i) then
        div_phase_store_i <= unsigned(div_phase);
        div_count_store_i <= unsigned(div_count);
        new_phase         <= true;
      end if;

      div_fast_i <= div_fast_i;
      div_cnt_i  <= div_cnt_i+1;

      -- Divide the clock 
      if div_cnt_i >= div_count_i-1 then
        div_fast_i     <= out_polarity_i;
        div_cnt_i      <= 0;
        -- get settings for next cycle
        out_polarity_i <= out_polarity;
        div_phase_i    <= to_integer(div_phase_store_i);
        div_count_i    <= to_integer(div_count_store_i);
      elsif div_cnt_i = (div_count_i-1)/2 then
        div_cnt_i  <= div_cnt_i+1;
        div_fast_i <= not out_polarity_i;
      else
        div_cnt_i <= div_cnt_i+1;
      end if;

      -- Phase shift clock
      if new_phase then
        clk_cnt_i <= to_integer(div_count_store_i - div_phase_store_i);
        new_phase <= false;
      else
        clk_cnt_i <= (clk_cnt_i + 1) mod div_count_i;
      end if;

      -- make output clock
      if clk_cnt_i >= div_count_i-1 then
        -- report "clk_cnt_i is at max" severity note;
        clk_en_i   <= out_polarity_i;
        clk_fast_i <= out_polarity_i;
      elsif clk_cnt_i = (div_count_i-1)/2 then
        clk_fast_i <= not out_polarity_i;
      else
        clk_en_i <= not out_polarity_i;
      end if;

      if CATCH = "Y" then
        if (unsigned(div_count) = 1 or
            unsigned(div_phase) > unsigned(div_count) or
            unsigned(div_count) = 0) then
          -- report "Bad setting" severity warning;
          clk_fast_i <= not out_polarity_i;
          clk_en_i   <= not out_polarity_i;
        end if;
      end if;
    end if;
  end process clk_divide;
end rtl2;

----------------------------------------------------------------------
architecture rtl3 of clock_gen is
  type   state_t is (setup, up, down);  -- States
  signal st_i          : state_t;       -- State
  signal old_sync_i    : std_logic;     -- Previous value of sync
  signal cnt_i         : integer range 0 to WIDTH**2-1;
  signal phase_i       : integer range 0 to WIDTH**2;
  signal div_i         : integer range 0 to WIDTH**2;
  signal phase_store_i : unsigned(WIDTH-1 downto 0);
  signal div_store_i   : unsigned(WIDTH-1 downto 0);
  signal clk_i         : std_logic;
  signal clk_en_i      : std_logic;
  signal new_value     : boolean;

begin  -- rtl3
  -- purpose: Check if we're using a combinatorial clock 
  -- catch divide by one and zero (otional)
  -- type   : combinational
  -- inputs : Clock
  -- outputs: clk_o, clk_en_o
  combinatorics : process (clk_i, clk_en_i)
  begin  -- process combinatorical_clocker
    clk_o    <= clk_i;
    clk_en_o <= clk_en_i;
  end process combinatorics;

  -- purpose: Produce the clock
  -- type   : sequential
  -- inputs : clk, rstb, sync
  -- outputs: clk_i, clk_en_i
  clocker : process (clk, rstb)
    variable next_cnt_i   : integer range 0 to WIDTH**2-1;
    variable next_phase_i : integer range 0 to WIDTH**2;
    variable next_div_i   : integer range 0 to WIDTH**2;
  begin  -- process clocker
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i          <= setup;
      phase_store_i <= unsigned(div_phase);
      div_store_i   <= unsigned(div_count);
      -- next_div_i    := to_integer(unsigned(div_count));
      -- next_phase_i  := to_integer(unsigned(div_phase)) mod next_div_i/2;
      -- next_cnt_i    := next_div_i - next_phase_i;
      -- phase_i       <= next_phase_i;
      -- div_i         <= next_div_i;
      -- cnt_i         <= next_cnt_i;
      clk_i         <= '0';
      clk_en_i      <= '0';
      old_sync_i    <= '0';
      new_value     <= false;
    elsif clk'event and clk = '1' then  -- rising clock edge
      old_sync_i <= sync;
      if (unsigned(div_phase) /= phase_store_i or
          unsigned(div_count) /= div_store_i) then
        phase_store_i <= unsigned(div_phase);
        div_store_i   <= unsigned(div_count);
        new_value     <= true;
      end if;

      case st_i is
        when setup =>
          clk_en_i     <= '0';
          new_value    <= false;
          next_div_i   := to_integer(div_store_i);
          next_phase_i := to_integer(phase_store_i) mod (next_div_i/2);
          next_cnt_i   := next_phase_i;
          if old_sync_i = '0' and sync = '1' then
            phase_i <= next_phase_i;
            div_i   <= next_div_i;
            if next_phase_i = 0 then
              clk_i <= '1';
              st_i  <= up;
              cnt_i <= 0;
            else
              clk_i <= '0';
              st_i  <= down;
              cnt_i <= next_div_i - next_phase_i;
            end if;
          else
            clk_i <= '0';
            st_i  <= setup;
          end if;

        when up =>
          clk_i    <= '1';
          clk_en_i <= '0';
          cnt_i    <= cnt_i + 1;
          st_i     <= up;
          if cnt_i >= (div_i / 2) - 1 then
            st_i  <= down;
            clk_i <= '0';
          end if;

        when down =>
          if cnt_i >= div_i-1 then
            cnt_i <= 0;
            if new_value then
              st_i     <= setup;
              clk_i    <= '0';
              clk_en_i <= '0';
            else
              st_i     <= up;
              clk_i    <= '1';
              clk_en_i <= '1';
            end if;
          else
            cnt_i <= cnt_i + 1;
            clk_i <= '0';
            st_i  <= down;
          end if;

        when others =>
          clk_i <= '0';
          st_i  <= setup;

      end case;
    end if;
  end process clocker;
end rtl3;


----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package clock_gen_pack is
  component clock_gen
    generic (
      WIDTH  : integer;                 -- Width of divider
      IN_POL : std_logic;               -- rising edge 
      CATCH  : string);                 -- lots of logic
    port (
      clk          : in  std_logic;     -- Master input clock
      rstb         : in  std_logic;     -- Master Rstb
      clk_en_o     : out std_logic;     -- Rising edge
      clk_o        : out std_logic;     -- Gen. clock
      out_polarity : in  std_logic := '1';                     -- Polarity
      div_phase    : in  std_logic_vector(WIDTH-1 downto 0);   -- div
      div_count    : in  std_logic_vector(WIDTH-1 downto 0));  -- off
  end component clock_gen;
end clock_gen_pack;

----------------------------------------------------------------------
--
-- EOF
--


