------------------------------------------------------------------------------
-- Title      : Test of clock generator
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : clock_gen_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-08-22
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/26  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.clock_gen_pack.all;
use work.fmdd_simul_pack.all;

------------------------------------------------------------------------------
entity clock_gen_tb is
end entity clock_gen_tb;

------------------------------------------------------------------------------

architecture test of clock_gen_tb is

  constant WIDTH   : integer   := 8;    -- Width of divider
  constant IN_POL  : std_logic := '1';  -- rising edge 
  constant CATCH   : string    := "Y";  -- lots of logic
  constant OUT_POL : std_logic := '1';  -- Output polarity

  signal clk_i       : std_logic := '0';  -- Master input clock
  signal sclk_i      : std_logic := '1';  -- Master input clock
  signal rstb_i      : std_logic := '0';  -- Master Rstb
  signal clk_en_o_i  : std_logic;       -- Rising edge
  signal clk_o_i     : std_logic;       -- Gen. clock
  signal div_phase_i : std_logic_vector(WIDTH-1 downto 0);  -- div
  signal div_count_i : std_logic_vector(WIDTH-1 downto 0);  -- off
  signal old_sclk_i  : std_logic;
  signal do_sync_i   : std_logic;

  type     div_vals is array (3 downto 0) of integer range 0 to 2**WIDTH-1;
  constant div_vals_i : div_vals := (2, 4, 8, 16);
  constant N_CLKS     : integer  := 4;
  
begin  -- architecture test

  dut : entity work.clock_gen(rtl5)
    generic map (
        WIDTH  => WIDTH,                -- Width of divider
        IN_POL => IN_POL,               -- rising edge 
        CATCH  => CATCH)                -- lots of logic
    port map (
        clk          => clk_i,          -- in  Master input clock
        rstb         => rstb_i,         -- in  Master Rstb
        sync         => sclk_i,
        clk_en_o     => clk_en_o_i,     -- out Rising edge
        clk_o        => clk_o_i,        -- out Gen. clock
        out_polarity => OUT_POL,        -- in  Polarity
        div_phase    => div_phase_i,    -- in  div
        div_count    => div_count_i);   -- in off

  -- Simulation stimuli
  stim : process
  begin  -- process stim
    div_count_i <= std_logic_vector(to_unsigned(div_vals_i(0), WIDTH));
    div_phase_i <= std_logic_vector(to_unsigned(0, WIDTH));
    wait until rising_edge(clk_i);
    wait until rising_edge(clk_i);
    rstb_i      <= '1';
    wait for 75 ns;
    
    -- for i in 0 to 6 loop
    --   wait until rising_edge(clk_o_i);
    -- end loop;  -- i
    -- change_clock(phase     => 1,
    --              count     => 3,
    --              div_count => div_count_i,
    --              div_phase => div_phase_i);

    for i in div_vals_i'range loop
      for j in 0 to (div_vals_i(i)/2)-1 loop
        change_clock(phase     => j,
                     count     => div_vals_i(i),
                     div_count => div_count_i,
                     div_phase => div_phase_i);
        -- for k in 0 to N_CLKS-1 loop
        -- wait until rising_edge(clk_o_i);
        -- end loop;  -- i
        wait for 1 us;
        wait until rising_edge(clk_i);
      end loop;  -- j
    end loop;  -- i

    wait;
  end process stim;

  foo: process (clk_i, rstb_i)
  begin  -- process foo
    if rstb_i = '0' then                  -- asynchronous reset (active low)
      do_sync_i <= '0';
      old_sclk_i <= sclk_i;
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      old_sclk_i <= sclk_i;
      if old_sclk_i = '0' and sclk_i = '1' then
        do_sync_i <= '1';
      else
        do_sync_i <= '0';
      end if;
    end if;
  end process foo;
  
  -- Clock 
  clocker : process
  begin  -- process clocker
    clk_i <= not clk_i; -- after 2 ns;
    wait for PERIOD / 2;
  end process clocker;

  -- Clock 
  sclocker : process
  begin  -- process clocker
    sclk_i <= not sclk_i;
    wait for 4 * PERIOD / 2;
  end process sclocker;
end architecture test;
------------------------------------------------------------------------------
-- 
-- EOF
--
