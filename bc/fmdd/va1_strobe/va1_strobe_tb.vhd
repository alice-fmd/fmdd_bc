-------------------------------------------------------------------------------
-- Title      : Testbench for design "va1_strobe"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : va1_strobe_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Created    : 2008-09-04
-- Last update: 2008-09-04
-- Platform   : 
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2008 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2008-09-04  1.0      cholm   Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.va1_strobe_pack.all;

-------------------------------------------------------------------------------

entity va1_strobe_tb is

end va1_strobe_tb;

-------------------------------------------------------------------------------

architecture test of va1_strobe_tb is
  constant SCLK_DIV : integer := 4;
  constant PERIOD   : time    := 25 ns;
  
  -- component ports
  signal clk_i       : std_logic := '1';              -- [in]  Fast clock
  signal rstb_i      : std_logic := '0';              -- [in]  Reset
  signal start_i     : std_logic := '1';              -- [in]  Start signal
  signal div_i       : std_logic_vector(7 downto 0);  -- [in]  Shift clock division
  signal phase_i     : std_logic_vector(7 downto 0);  -- [in]  Shift clock division
  signal first_i     : std_logic_vector(7 downto 0);  -- [in]
  signal last_i      : std_logic_vector(7 downto 0);  -- [in]
  signal busy_i      : std_logic;
  signal shift_clk_i : std_logic;       -- [out] Shift clock
  signal shift_in_i  : std_logic;       -- [out]
  signal dreset_i    : std_logic;       -- [out]

  signal sclk_i  : std_logic := '1';
  signal gsclk_i : std_logic;
  
  -----------------------------------------------------------------------------
  -- purpose: Change settings
  procedure change_clk (constant idiv   : in  integer range 0 to 2**8-1;
                        constant iphase : in  integer range 0 to 2**8-1;
                        signal   clk    : in  std_logic;
                        signal   odiv   : out std_logic_vector(7 downto 0);
                        signal   ophase : out std_logic_vector(7 downto 0)) is
  begin  -- change_clk
    wait until rising_edge(clk);
    odiv   <= std_logic_vector(to_unsigned(idiv, 8));
    ophase <= std_logic_vector(to_unsigned(iphase, 8));
  end change_clk;

  -----------------------------------------------------------------------------
  -- purpose: Change settings
  procedure change_range (constant ifirst : in  integer range 0 to 2**8-1;
                          constant ilast  : in  integer range 0 to 2**8-1;
                          signal   clk    : in  std_logic;
                          signal   ofirst : out std_logic_vector(7 downto 0);
                          signal   olast  : out std_logic_vector(7 downto 0)) is
  begin  -- change_clk
    wait until rising_edge(clk);
    ofirst <= std_logic_vector(to_unsigned(ifirst, 8));
    olast  <= std_logic_vector(to_unsigned(ilast, 8));
  end change_range;

  -----------------------------------------------------------------------------
  -- purpose: Make trigger
  procedure start_it (constant l1_len : in  integer;
                      signal sclk     : in  std_logic;
                      signal l1       : out std_logic) is
  begin  -- start_it
    wait until rising_edge(sclk);
    l1 <= '0', '1' after l1_len * PERIOD;
  end start_it;

  -----------------------------------------------------------------------------
  procedure wait_for_busy (signal clk : in std_logic;
                           signal busy : in std_logic) is
  begin
    wait until rising_edge(clk);
    wait for PERIOD;
    wait until busy = '0';
  end wait_for_busy;
  
begin  -- test

  -- component instantiation
  DUT : va1_strobe
    port map (clk   => clk_i,           -- [in]  Fast clock
              rstb  => rstb_i,          -- [in]  Reset
              start => start_i,         -- [in]  Start signal
              div   => div_i,           -- [in]  Shift clock division
              phase => phase_i,         -- [in]  Shift clock division
              first => first_i,         -- [in]
              last  => last_i,          -- [in]
              busy  => busy_i,          -- [in]
              shift_clk => shift_clk_i,  -- [out] Shift clock
              shift_in  => shift_in_i,  -- [out]
              dreset    => dreset_i);    -- [out]

  -- clock generation
  clk_i  <= not clk_i  after PERIOD / 2;
  sclk_i <= not sclk_i after PERIOD / 2 * SCLK_DIV;
  -- waveform generation
  stim : process
    variable i : integer;
  begin
    -- insert signal assignments here
    change_clk(idiv   => 16,
               iphase => 0,
               clk    => clk_i,
               odiv   => div_i,
               ophase => phase_i);
    change_range(ifirst => 5,
               ilast    => 5,
               clk      => clk_i,
               ofirst   => first_i,
               olast    => last_i);
    wait for 100 ns;
    rstb_i <= '1';
    wait for 900 ns;

    for i in 0 to 3 loop
      wait for 4 * PERIOD;
      start_it(l1_len => 2 * SCLK_DIV,
               sclk   => sclk_i,
               l1     => start_i);
      wait_for_busy(clk  => clk_i,
                    busy => busy_i);
      if i = 2 then
        change_clk(idiv => 16,
                   iphase => 2,
                   clk => clk_i,
                   odiv => div_i,
                   ophase => phase_i);
      end if;
      wait for 1 us;
    end loop;  -- i
    
    change_range(ifirst => 1,
               ilast    => 5,
               clk      => clk_i,
               ofirst   => first_i,
               olast    => last_i);
    wait for 2 us;
    
    for j in 0 to 10 loop
      
      change_clk(idiv => 16,
                 iphase => j,
                 clk => clk_i,
                 odiv => div_i,
                 ophase => phase_i);
      wait for 1 us;
      for i in 0 to 1 loop
        wait for 4 * PERIOD;
        start_it(l1_len => 2 * SCLK_DIV,
                 sclk   => sclk_i,
                 l1     => start_i);
        wait_for_busy(clk  => clk_i,
                    busy => busy_i);
        wait for 1 us;
      end loop;  -- i
    end loop;  -- j

    wait;                               -- for ever
  end process stim;

  gsclk_i <= sclk_i when (start_i = '0' or busy_i = '1') else '1';

end test;
-------------------------------------------------------------------------------
--
-- EOF
--
