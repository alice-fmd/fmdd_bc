------------------------------------------------------------------------------
-- Title      : Send read-out sequence to VA1s
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : va1_readout_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-09-04
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This module takes care of giving the right read-out seqeunce to
--              the VA1 interface.
-- The strobe generator is initialised by the @a start signal.  It will then
-- start strobing the VA1s after @a phase clock cycles.  Note that there's an
-- implicit delay of 3 clock cycles (= 75ns) from the reception of the @a
-- start signal.
--
-- Note, that the rest of the design may implicitly introduce an additinal
-- delay on the start signal.  This must be factored in when tunning the
-- setting of @a phase.
--
-- The period of the strobe is set by the input @a div, so tha
-- the period is equal to @f$ div \times 25ns@f$.
--
-- The range strips (and consequently the number of strobes) is set by the
-- inputs @a first and @a last.   If the value of @a first is changed, then
-- the architecture will set-up the VA1 pointer to that value.   That means
-- sending a single @a shift_clk pulse together with a @a shift_in pulse
-- (which resets the cursor to the start of the VA1) , and then send @a first
-- @a shift_clk pulses. 
-- 
-- When start signal arrives, the architecture will send (@a last - @a first)
-- strobes, then send a digital reset (@a dreset) strobe, and then do a setup
-- as outlined above.
-- 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/26  1.0      cholm   Created
------------------------------------------------------------------------------

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;


entity va1_strobe is
  port (clk       : in  std_logic;                     -- Fast clock
        rstb      : in  std_logic;                     -- Reset
        start     : in  std_logic;                     -- Start signal
        div       : in  std_logic_vector(7 downto 0);  -- Shift clock division
        phase     : in  std_logic_vector(7 downto 0);  -- Shift clock division
        first     : in  std_logic_vector(7 downto 0);
        last      : in  std_logic_vector(7 downto 0);
        busy      : out std_logic;
        shift_clk : out std_logic;                     -- Shift clock
        shift_in  : out std_logic;
        dreset    : out std_logic);
end va1_strobe;

-------------------------------------------------------------------------------
--! Architecture of VA1 strobe generator.
--! 
--! The strobe generator is initialised by the @a start signal.  It will then
--! start strobing the VA1s after @a phase clock cycles.  Note that there's an
--! implicit delay of 3 clock cycles (= 75ns) from the reception of the @a
--! start signal.
--!
--! Note, that the rest of the design may implicitly introduce an additinal
--! delay on the start signal.  This must be factored in when tunning the
--! setting of @a phase.
--!
--! The period of the strobe is set by the input @a div, so tha
--! the period is equal to @f$ div \times 25ns@f$.
--!
--! The range strips (and consequently the number of strobes) is set by the
--! inputs @a first and @a last.   If the value of @a first is changed, then
--! the architecture will set-up the VA1 pointer to that value.   That means
--! sending a single @a shift_clk pulse together with a @a shift_in pulse
--! (which resets the cursor to the start of the VA1) , and then send @a first
--! @a shift_clk pulses. 
--! 
--! When start signal arrives, the architecture will send (@a last - @a first)
--! strobes, then send a digital reset (@a dreset) strobe, and then do a setup
--! as outlined above.
--! 
architecture behaviour of va1_strobe is
  type state_t is (idle,
                   delay,
                   stay_on_strip,
                   wait_shift_down,
                   clocking,
                   wait_reset,
                   reset,
                   wait_shift_reset,
                   shift_reset,
                   setup);
  constant RESET_LEN : integer := 4;

  signal state_i : state_t;             -- State

  signal div_i       : integer range 0 to 2**8;
  signal div_ii      : integer range 0 to 2**8;
  signal div_cnt_i   : integer range 0 to 2**8;  -- Division counter
  signal phase_ii    : integer range 0 to 2**8;
  signal cnt_i       : integer range 0 to 2**8;  -- Division counter
  signal first_i     : integer range 0 to 2**8;
  signal first_ii    : integer range 0 to 2**8;
  signal last_i      : integer range 0 to 2**8;
  signal last_ii     : integer range 0 to 2**8;
  signal rst_cnt_i   : integer range 0 to 2**8;
  signal phase_cnt_i : integer range 0 to 2**8;
  
  signal shift_en_i   : std_logic;      -- Out clock enable
  signal shift_up_i   : std_logic;      -- Out clock enable
  signal shift_down_i : std_logic;      -- Out clock enable
  signal shift_out_i  : std_logic;
  signal shift_clk_i  : std_logic;
  signal take_i       : std_logic;
begin  -- behaviour


  -- purpose: State machine
  -- type   : sequential
  -- inputs : clk, rstb, start
  -- outputs: va_clk_en_i
  fsm : process (clk, rstb)
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      first_ii    <= to_integer(unsigned(first));
      last_ii     <= to_integer(unsigned(last));
      phase_ii    <= to_integer(unsigned(phase));
      shift_en_i  <= '0';
      shift_out_i <= '0';
      take_i      <= '0';
      state_i     <= idle;
      dreset      <= '0';
      shift_in    <= '1';
      busy        <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      first_ii <= to_integer(unsigned(first));
      last_ii  <= to_integer(unsigned(last));
      phase_ii <= to_integer(unsigned(phase));

      case state_i is
        when idle =>
          dreset      <= '0';
          shift_in    <= '1';
          first_i     <= first_ii;
          last_i      <= last_ii;
          cnt_i       <= first_ii;
          phase_cnt_i <= phase_ii;
          if start = '0' then
            take_i      <= '1';
            busy        <= '1';
            if phase_ii <= 0 then
              shift_en_i  <= '1';
              shift_out_i <= '1';
              if first_i = last_i then
                state_i <= stay_on_strip;
              else
                state_i <= wait_shift_down;
              end if;
            else
              shift_en_i  <= '0';
              shift_out_i <= '0';
              state_i     <= delay;
            end if;
          elsif first_ii /= first_i then  -- setup for new strip range
            cnt_i   <= 0;
            state_i <= wait_shift_reset; -- setup;
          else
            take_i      <= '0';
            shift_en_i  <= '0';
            shift_out_i <= '0';
            state_i     <= idle;
            busy        <= '0';
          end if;

        when delay =>
          busy           <= '1';
          dreset         <= '0';
          shift_in       <= '1';
          take_i         <= '1';
          shift_out_i    <= '0';
          shift_en_i     <= '1';
          phase_cnt_i    <= phase_cnt_i - 1;
          if phase_cnt_i <= 1 then
            if first_i = last_i then
              state_i <= stay_on_strip;
            else
              state_i <= wait_shift_down;
            end if;
          else
            state_i <= delay;
          end if;

        when stay_on_strip =>
          busy        <= '1';
          dreset      <= '0';
          shift_in    <= '1';
          shift_out_i <= '0';
          shift_en_i  <= '1';
          take_i      <= '0';
          if shift_up_i = '1' then
            state_i <= idle;
          end if;
          
        when wait_shift_down =>
          dreset      <= '0';
          shift_in    <= '1';
          shift_en_i  <= '1';
          shift_out_i <= '1';
          take_i      <= '0';
          busy        <= '1';
          if shift_down_i = '1' then
            state_i <= clocking;
            cnt_i   <= cnt_i + 1;
          else
            state_i <= wait_shift_down;
          end if;

        when clocking =>
          dreset      <= '0';
          shift_in    <= '1';
          shift_en_i  <= '1';
          shift_out_i <= '1';
          take_i      <= '0';
          busy        <= '1';
          if cnt_i >= last_i and shift_up_i = '1' then  -- Last one
            state_i <= wait_reset;
          elsif shift_down_i = '1' then                -- Got a clock pulse
            cnt_i   <= cnt_i + 1;
            state_i <= clocking;
          end if;
          
        when wait_reset =>
          dreset      <= '0';
          shift_in    <= '1';
          shift_en_i  <= '1';
          shift_out_i <= '0';
          take_i      <= '0';
          busy        <= '1';
          if shift_down_i = '1' then
            state_i   <= reset;
            rst_cnt_i <= RESET_LEN;
          else
            state_i <= wait_reset;
          end if;

        when reset =>
          shift_en_i   <= '1';
          shift_out_i  <= '0';
          shift_in     <= '1';
          dreset       <= '1';
          busy         <= '1';
          if rst_cnt_i <= 1 then
            state_i <= wait_shift_reset;
          else
            rst_cnt_i <= rst_cnt_i - 1;
            state_i   <= reset;
          end if;

        when wait_shift_reset =>
          dreset      <= '0';
          shift_en_i  <= '1';
          shift_out_i <= '0';
          shift_in    <= '1';
          take_i      <= '0';
          busy        <= '1';
          if shift_up_i = '1' then
            state_i  <= shift_reset;
            shift_in <= '0';
          else
            state_i <= wait_shift_reset;
          end if;

        when shift_reset =>
          dreset      <= '0';
          shift_in    <= '0';
          shift_en_i  <= '1';
          shift_out_i <= '1';
          take_i      <= '0';
          busy        <= '1';
          if shift_up_i = '1' then
            state_i <= setup;
            cnt_i   <= 0;
          else
            state_i <= shift_reset;
          end if;

        when setup =>
          dreset      <= '0';
          shift_in    <= '1';
          shift_en_i  <= '1';
          shift_out_i <= '1';
          take_i      <= '0';
          busy        <= '1';
          if cnt_i = first_i and shift_up_i = '1' then
            state_i <= idle;
          else
            if shift_down_i = '1' then
              cnt_i <= cnt_i + 1;
            end if;
            state_i <= setup;
          end if;
          
        when others =>
          dreset      <= '0';
          shift_in    <= '1';
          shift_en_i  <= '0';
          shift_out_i <= '0';
          take_i      <= '0';
          state_i     <= idle;
          busy        <= '0';
      end case;
    end if;
  end process fsm;

  -- purpose: Generate shift clock
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  shift_gen : process (clk, rstb)
  begin  -- process shift_gen
    if rstb = '0' then
      div_ii       <= to_integer(unsigned(div));
      div_i        <= to_integer(unsigned(div));
      div_cnt_i    <= to_integer(unsigned(phase));
      shift_clk_i  <= '1';
      shift_down_i <= '0';
      shift_up_i   <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      div_ii   <= to_integer(unsigned(div));

      if take_i = '1' then
        div_cnt_i <= div_ii; -- phase_ii;
        div_i     <= div_ii;
      elsif shift_en_i = '1' then
        if div_cnt_i = div_i then
          report "Down edge" severity note;
          div_cnt_i    <= div_cnt_i - 1;
          shift_clk_i  <= '0';
          shift_down_i <= '1';
          shift_up_i   <= '0';
        elsif div_cnt_i > div_i / 2 then
          report "Down" severity note;
          div_cnt_i    <= div_cnt_i - 1;
          shift_clk_i  <= '0';
          shift_down_i <= '0';
          shift_up_i   <= '0';
        elsif div_cnt_i = div_i / 2 then
          report "Up edge" severity note;
          div_cnt_i    <= div_cnt_i - 1;
          shift_clk_i  <= '1';
          shift_down_i <= '0';
          shift_up_i   <= '1';
        elsif div_cnt_i <= 1 then
          report "Reset" severity note;
          div_cnt_i    <= div_i;
          shift_clk_i  <= '1';
          shift_down_i <= '0';
          shift_up_i   <= '0';
        else
          report "Up" severity note;
          div_cnt_i    <= div_cnt_i - 1;
          shift_clk_i  <= '1';
          shift_down_i <= '0';
          shift_up_i   <= '0';
        end if;
      else
        shift_clk_i  <= '1';
        shift_down_i <= '0';
        shift_up_i   <= '0';
      end if;
      
    end if;
  end process shift_gen;

  shift_clk <= shift_clk_i when shift_out_i = '1' else '1';

end behaviour;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package va1_strobe_pack is
  component va1_strobe
    port (clk       : in  std_logic;    -- Fast clock
          rstb      : in  std_logic;    -- Reset
          start     : in  std_logic;    -- Start signal
          div       : in  std_logic_vector(7 downto 0);  -- Shift clock division
          phase     : in  std_logic_vector(7 downto 0);  -- Shift clock division
          first     : in  std_logic_vector(7 downto 0);
          last      : in  std_logic_vector(7 downto 0);
          busy      : out std_logic;
          shift_clk : out std_logic;    -- Shift clock
          shift_in  : out std_logic;
          dreset    : out std_logic);
  end component;
end va1_strobe_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
