------------------------------------------------------------------------------
-- Title      : Test bench of trigger handler, readout sequencer, and clock
--              generator 
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : handle_box_readout_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/28
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/26  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.trigger_handler_pack.all;
use work.trigger_box_pack.all;
use work.va1_readout_pack.all;
use work.clock_gen_pack.all;
use work.fmdd_simul_pack.all;

-------------------------------------------------------------------------------
entity handle_box_readout_tb is
end handle_box_readout_tb;

------------------------------------------------------------------------------
architecture test of handle_box_readout_tb is

  -- Timing parameters
  signal hold_wait_i  : std_logic_vector(15 downto 0) := (others => '0');
  signal l1_timeout_i : std_logic_vector(15 downto 0) := (others => '0');
  signal l2_timeout_i : std_logic_vector(15 downto 0) := (others => '0');

  -- VA1 Clock parameters 
  signal   va_phase_i : std_logic_vector(7 downto 0) := int2slv(3, 8);
  signal   va_div_i   : std_logic_vector(7 downto 0) := int2slv(16, 8);

  -- ALTRO Clock parameters 
  signal sclk_phase_i : std_logic_vector(7 downto 0) := int2slv(0, 8);
  signal sclk_div_i   : std_logic_vector(7 downto 0) := int2slv(4, 8);

  -- Read-out parameters
  signal first_i : std_logic_vector(7 downto 0) := int2slv(0, 8);
  signal last_i  : std_logic_vector(7 downto 0) := int2slv(127, 8);

  -- Control signals 
  signal clk_i             : std_logic := '0';  -- Clock
  signal rstb_i            : std_logic := '0';  -- Async reset
  signal l0_i              : std_logic := '0';  -- External L0
  signal l1b_i             : std_logic := '1';  -- External L1
  signal l2b_i             : std_logic := '1';  -- External L2
  signal cal_on_i          : std_logic := '0';  -- Turn on calibration
  signal start_i           : std_logic := '0';  -- Start trigger box
  signal inhibit_i         : std_logic := '0';  -- Inhibit triggers
  signal l0_only_i         : std_logic := '0';  -- Only gen L0
  -- Output from trigger box
  signal l0_out_i          : std_logic;         -- Validated L0
  signal l1b_out_i         : std_logic;         -- Validated L1
  signal l2b_out_i         : std_logic;         -- Validated L2
  signal box_busy_i        : std_logic;         -- Status: trigger busy
  signal timeout_i         : std_logic;         -- Status: trigger timeout
  signal overlap_i         : std_logic;         -- Status : overlapping
  -- Output from trigger handler
  signal hold_i            : std_logic;         -- Hold VA1 value
  signal busy_i            : std_logic;         -- Busy processing
  signal pulser_enable_i   : std_logic;         -- Enable calib. pulser
  signal incomplete_i      : std_logic;         -- Incomplete readout
  -- VA1 and shift clock
  signal va_clk_i          : std_logic;         -- Shift clock mother
  signal shift_clk_i       : std_logic;         -- Shift clock burs
  -- ALTRO clock
  signal sclk_i            : std_logic := '0';  -- Sample clock
  -- Sequencer signals 
  signal sequencer_start_i : std_logic;         -- Start the sequencer
  signal sequencer_busy_i  : std_logic;         -- Sequencer is busy 
  signal sequencer_clear_i : std_logic;         -- Clear the sequencer
  signal shift_in_i        : std_logic;         -- Reset shift register in VA1
  signal digital_reset_i   : std_logic;         -- Reset VA1
  signal altro_l1_i        : std_logic;         -- L1 trigger to ALTROs

begin  -- test

  box : entity work.trigger_box
    generic map (
        L0_DUR     => 1,                -- Length of L0
        L1_DUR     => 4,                -- Length of L1
        L2_DUR     => 4)                -- Length of L2
    port map (
        clk        => clk_i,            -- in  Clock
        rstb       => rstb_i,           -- in  Reset (active low)
        start      => start_i,          -- in  Make a fake trigger sequence
        l0_only    => l0_only_i,        -- in  Only make a fake L0
        inhibit    => inhibit_i,        -- in  Inhibit triggers
        l0         => l0_i,             -- in  Real L0 trigger (active high)
        l1b        => l1b_i,            -- in  Real L1 trigger (active low)
        l2b        => l2b_i,            -- in  Real L2 trigger (active low)
        l1_timeout => l1_timeout_i,     -- in  Timeout for L1
        l2_timeout => l2_timeout_i,     -- in  Timeout for L2
        l0_out     => l0_out_i,         -- out The L0 trigger  (fake or real)
        l1b_out    => l1b_out_i,        -- out The L1 trigger (fake or real)
        l2b_out    => l2b_out_i,        -- out The L2 trigger (fake or real)
        busy       => box_busy_i,       -- out Status: trigger busy
        timeout    => timeout_i,        -- out Status: trigger timeout
        overlap    => overlap_i);       -- out Status : overlapping

  handle : entity work.trigger_handler
    port map (
        clk             => clk_i,              -- in  Clock
        rstb            => rstb_i,             -- in  Async reset
        l0              => l0_out_i,           -- in  Level 0 (-)
        l1              => l1b_out_i,          -- in  Level 1 (-)
        hold            => hold_i,             -- out Hold to VA1s
        busy            => busy_i,             -- out Busy to CTP
        hold_wait       => hold_wait_i,        -- in  wait hold
        l1_timeout      => l1_timeout_i,       -- in  L1 timeout
        l2_timeout      => l2_timeout_i,       -- in  L2 timeout 
        sequencer_start => sequencer_start_i,  -- out Start seq.
        sequencer_busy  => sequencer_busy_i,   -- in  Seq. busy
        sequencer_clear => sequencer_clear_i,  -- out Clear seq. 
        cal_on          => cal_on_i,           -- in  Calib. mode
        pulser_enable   => pulser_enable_i,    -- out Step cal.
        incomplete      => incomplete_i);      -- out Status

  readout : entity work.va1_readout
    generic map (
        DRESET_LENGTH => 4,                  -- dreset length
        L1_LENGTH     => 8,                  -- L1 output length
        SIZE          => 127)
    port map (
        clk           => clk_i,              -- in  Clock
        rstb          => rstb_i,             -- in  Async reset
        clear         => sequencer_clear_i,  -- in  Clear
        va_clk        => va_clk_i,           -- in  VA input clock
        first         => first_i,            -- in  First strip
        last          => last_i,             -- in  Lasst strip
        start         => sequencer_start_i,  -- in  Start
        busy          => sequencer_busy_i,   -- out Busy
        l1            => altro_l1_i,         -- out L1 to ALTROs
        shift_clk     => shift_clk_i,        -- out Burst 
        shift_in      => shift_in_i,         -- out Reset shift-reg
        digital_reset => digital_reset_i);   -- out Reset VA1s

  va_clk_gen : entity work.clock_gen
    generic map (
        WIDTH        => 8,              -- Width of divider
        IN_POL       => '1',            -- rising edge 
        CATCH        => "Y")            -- lots of logic
    port map (
        clk          => clk_i,          -- in  Master input clock
        rstb         => rstb_i,         -- in  Master Rstb
        clk_en_o     => open,           -- out Rising edge
        clk_o        => va_clk_i,       -- out Gen. clock
        out_polarity => '1',            -- in  Polarity
        div_phase    => va_phase_i,     -- in  div
        div_count    => va_div_i);      -- in off

  sclk_gen : entity work.clock_gen
    generic map (
        WIDTH        => 8,              -- Width of divider
        IN_POL       => '1',            -- rising edge 
        CATCH        => "Y")            -- lots of logic
    port map (
        clk          => clk_i,          -- in  Master input clock
        rstb         => rstb_i,         -- in  Master Rstb
        clk_en_o     => open,           -- out Rising edge
        clk_o        => sclk_i,         -- out Gen. clock
        out_polarity => '1',            -- in  Polarity
        div_phase    => sclk_phase_i,   -- in  div
        div_count    => sclk_div_i);    -- in off

  -- purpose: Stimuli
  stimuli : process
  begin  -- process stimuli
    change_timeouts(hold_delay => 1.5 us,
                    l1_wait    => 6.5 us,
                    l2_wait    => 88 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);
    wait for 4 * PERIOD;
    rstb_i <= '1';


    -- External triggers
    wait for (L0_DELAY - NOW);
    make_triggers(clk       => clk_i,
                  l0        => l0_i,
                  l1        => l1b_i,
                  l2        => l2b_i);

    -- Internal triggers
    wait for 100 us + L0_DELAY - NOW;
    start_it(start => start_i, clk => clk_i);

    -- Turn on cal_pulse 
    wait for 195 us - NOW;
    cal_on_i <= '1';
    wait for 200 us - NOW;
    make_triggers(clk       => clk_i,
                  l0        => l0_i,
                  l1        => l1b_i,
                  l2        => l2b_i);
    wait;
  end process stimuli;

  -- Clock 
  clocker : process
  begin  -- process clocker
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clocker;
end test;
------------------------------------------------------------------------------
-- 
-- EOF
--
