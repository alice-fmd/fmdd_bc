------------------------------------------------------------------------------
-- Title      : Test bench of trigger handler, readout sequencer, and clock
--              generator 
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : fmdd_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2009-04-15
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/26  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.interpreter_pack.all;
use work.trigger_handler_pack.all;
use work.trigger_box_pack.all;
use work.dac_interface_pack.all;
use work.va1_readout_pack.all;
use work.clock_gen_pack.all;
use work.cal_manager_pack.all;
-- use work.meb_watchdog.all;

-------------------------------------------------------------------------------
entity fmdd is
  port (
    clk           : in  std_logic;      --! Clock
    sclk          : in  std_logic;      --! Sample clock
    rstb          : in  std_logic;      --! Async reset
    -- Trigger interface 
    l0            : in  std_logic;      --! L0 trigger
    l1b           : in  std_logic;      --! L1 trigger
    l2b           : in  std_logic;      --! L2 trigger
    hold_wait     : in  std_logic_vector(15 downto 0);   --! Wait to hold
    l1_timeout    : in  std_logic_vector(15 downto 0);   --! L1 timeout
    l2_timeout    : in  std_logic_vector(15 downto 0);   --! L2 timeout
    busy          : out std_logic;      --! Busy signal
    l2r           : out std_logic;      --! L2 reject (timeout)
    -- VA1 interface 
    shift_div     : in  std_logic_vector(15 downto 0);   --! Shift clock params
    shift_in      : out std_logic;      --! Shift reg. reset to VA1s (-)
    shift_clk     : out std_logic;      --! Shift clock to VA1s (-)
    strips        : in  std_logic_vector(15 downto 0);   --! Strip range
    digital_reset : out std_logic;      --! Reset VA1s (+)
    hold          : out std_logic;      --! Hold values in VA1 (-)
    -- Cal pulse interface 
    cal_level     : in  std_logic_vector(15 downto 0);   --! Cal. level
    cal_iter      : in  std_logic_vector(15 downto 0);   --! Cal. events
    cal_on        : out std_logic;      --! Calib mode on (-)
    cal_enable    : out std_logic;      --! Enable cal step (-)
    cal_delay     : in  std_logic_vector(15 downto 0);  --! Extra delay
    -- DAC interface
    shape_bias    : in  std_logic_vector(15 downto 0);   --! Shape bias
    vfs           : in  std_logic_vector(15 downto 0);   --! Shape ref. 
    vfp           : in  std_logic_vector(15 downto 0);   --! Pre-amp. ref.
    dac_addr      : out std_logic_vector(11 downto 0);   --! DAC address
    dac_data      : out std_logic_vector(7 downto 0);    --! DAC value
    -- ALTRO interface 
    sample_div    : in  std_logic_vector(15 downto 0);   --! Sample clock param
    sample_clk    : out std_logic;      --! Sample clock to ALTROs
    altro_l1      : out std_logic;      --! L1 to ALTROs
    altro_l2      : out std_logic;      --! L2 to ALTROs
    -- Commands
    command       : in  std_logic_vector(15 downto 0);   --! Commands
    -- Status output 
    status        : out std_logic_vector(15 downto 0);
    debug         : out std_logic_vector(14 downto 0));  --! Status
end fmdd;

------------------------------------------------------------------------------
architecture rtl of fmdd is
  -- Commands 
  signal l0_only_i         : std_logic;  -- Only gen L0
  signal start_box_i       : std_logic;  -- Start trigger box
  signal cmd_cal_on_i      : std_logic;  -- Calibration mode on
  signal cmd_test_on_i     : std_logic;  -- Test mode on
  signal cmd_change_dac_i  : std_logic;  -- Change DACs
  signal soft_reset_i      : std_logic;  -- Soft reset
  -- Triggers
  signal l0_i              : std_logic;  -- Validated L0
  signal l1b_i             : std_logic;  -- Validated L1
  signal l2b_i             : std_logic;  -- Validated L2
  signal trg_clear_seq_i   : std_logic;  -- Reset VA shift reg.
  -- Busy signals
  signal box_busy_i        : std_logic;  -- Trigger box is busy
  signal handler_busy_i    : std_logic;  -- Busy processing
  signal sequencer_busy_i  : std_logic;  -- Sequencer is busy
  signal dac_busy_i        : std_logic;  -- DAC interface busy  
  -- Status bits
  signal l2r_i             : std_logic;  -- L2 reject (timeout)
  signal l1r_i             : std_logic;  -- L1 reject (timeout)
  signal incomplete_i      : std_logic;  -- Incomplete readout
  signal overlap_i         : std_logic;  -- Trigger overlap
  signal timeout_i         : std_logic;  -- Trigger timeout
  -- VA1 and shift clock
  signal va_clk_i          : std_logic;  -- Shift clock mother
  -- Sequencer signals 
  signal sequencer_start_i : std_logic;  -- Start the sequencer
  signal sequencer_clear_i : std_logic;  -- Clear the sequencer
  -- Pulser calibration manager
  signal cal_man_start_i   : std_logic;  -- Start calibration manager
  signal cal_man_busy_i    : std_logic;  -- Calibration manager is running
  signal cal_man_change_i  : std_logic;  -- Cal. man change DACs
  signal cal_man_strip_i   : std_logic_vector(7 downto 0);  -- strip
  signal cal_man_pulse_i   : std_logic_vector(7 downto 0);  -- pulser
  signal cal_clear_seq_i   : std_logic;  -- Reset VA shift reg.
  -- constant iterations_i      : std_logic_vector(15 downto 0) := X"0064";
  -- Mux values
  signal strip_first_i     : std_logic_vector(7 downto 0);  -- First strip
  signal strip_last_i      : std_logic_vector(7 downto 0);  -- Last strip
  signal pulse_lvl_i       : std_logic_vector(7 downto 0);  -- pulser level
  signal dac_start_i       : std_logic;  -- Change DACs
  signal test_on_i         : std_logic;  -- VA1 Test mode on
  signal cal_on_i          : std_logic;  -- Calib pulser on.
  signal dreset_i          : std_logic;  -- Digitial reset
  -- Fan-outs 
  signal altro_l1_i        : std_logic;  -- ALTRO L1 trigger
  signal hold_i            : std_logic;
  signal shift_clk_i       : std_logic;
  signal shift_in_i        : std_logic;
  signal sample_clk_i      : std_logic;
  signal digital_reset_i   : std_logic;
  signal cal_enable_i      : std_logic;

begin  -- test
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    cal_on               <= not test_on_i;     -- Calibration on
    altro_l1             <= altro_l1_i;           -- ALTRO level 1 trigger 
    altro_l2             <= l2b_i;             -- ALTRO level 2 trigger
    -- altro_l1_i           <= l1b_i;
    busy                 <= handler_busy_i;
    shift_clk            <= shift_clk_i;
    sample_clk           <= sclk;              -- sample_clk_i;
    hold                 <= not hold_i;        -- Hold values in VA1s
    shift_in             <= shift_in_i;
    digital_reset        <= not digital_reset_i;
    cal_enable           <= cal_enable_i;
    l2r                  <= l2r_i;
    status(0)            <= cal_on_i;          -- Cal. mode
    status(1)            <= handler_busy_i;    -- Trigger handler busy
    status(2)            <= dac_busy_i;        -- DAC is being changed
    status(3)            <= sequencer_busy_i;  -- VA1 readout in progress
    status(4)            <= box_busy_i;        -- Trigggers seen
    status(5)            <= incomplete_i;      -- Incomplete read-out
    status(6)            <= overlap_i;         -- Overlapping triggers
    status(7)            <= timeout_i;         -- Trigger timed out
    status(8)            <= test_on_i;         -- VA1 test mode
    status(9)            <= cal_man_busy_i;    -- Cal. manager busy  
    status(15 downto 10) <= (others => '0');   -- Zero reset
    debug(0)             <= l0_i;              -- Processed L0
    debug(1)             <= altro_l1_i;        -- L1 to ALTRO's
    debug(2)             <= l2b_i;             -- L2 to ALTRO's
    debug(3)             <= sclk;  -- sample_clk_i;  -- Sample clock
    debug(4)             <= shift_clk_i;       -- Shift burst 
    debug(5)             <= shift_in_i;        -- Shift reset
    debug(6)             <= l1r_i;             -- L1 reject
    debug(7)             <= l2r_i;             -- Reset
    debug(8)             <= l1b_i;             -- Enable pulse
    debug(9)             <= l2b_i;             -- Pulse enabled on VA1
    -- debug(6)             <= not hold_i;           -- Hold
    -- debug(7)             <= not digital_reset_i;  -- Reset
    -- debug(8)             <= cal_enable_i;         -- Enable pulse
    -- debug(9)             <= not cal_on_i;         -- Pulse enabled on VA1
    -- debug(9)             <= sequencer_clear_i;
    debug(10)            <= l1b;               -- Input L1
    debug(11)            <= l2b;               -- Input L2
    -- debug(12)            <= sequencer_start_i;  -- 
    -- debug(13)            <= sequencer_busy_i;   --
    debug(14)            <= va_clk_i;          --
    debug(12)            <= start_box_i;
    -- debug(12)            <= incomplete_i;         -- Incomplete read-out 
    debug(13)            <= overlap_i;         -- Overlapping triggers
    -- debug(14)            <= handler_busy_i;    -- handler_busy_i;
  end block combi;

  -- purpose: Mux to select some inputs
  cal_normal_mux : block is
  begin  -- block cal_normal_mux
    strip_first_i     <= cal_man_strip_i  when cal_man_busy_i = '1'
                       else strips(7 downto 0);
    strip_last_i      <= cal_man_strip_i  when cal_man_busy_i = '1'
                       else strips(15 downto 8);
    pulse_lvl_i       <= cal_man_pulse_i  when cal_man_busy_i = '1'
                       else cal_level(7 downto 0);
    dac_start_i       <= cal_man_change_i when cal_man_busy_i = '1'
                       else cmd_change_dac_i;
    test_on_i         <= '1'              when cal_man_busy_i = '1'
                       else cmd_test_on_i;
    cal_on_i          <= '1'              when cal_man_busy_i = '1'
                       else cmd_cal_on_i;
    digital_reset_i   <= dreset_i;
    sequencer_clear_i <= cal_clear_seq_i or trg_clear_seq_i;
  end block cal_normal_mux;

  -- purpose: Interpret commands send from the RCU
  exec : entity work.interpreter
    port map (
      clk               => clk,               -- in  clock
      rstb              => rstb,              -- in  Reset (-)
      command           => command,           -- in  
      cal_on            => cmd_cal_on_i,      -- out VA1 test mode
      test_on           => cmd_test_on_i,     -- out VA1 test mode
      start_cal         => cal_man_start_i,   -- out Start calibration man
      start_trigger_box => start_box_i,       -- out Make trigger sequence
      l0_only           => l0_only_i,         -- out Make L0 trigger
      change_dac        => cmd_change_dac_i,  -- out Signal new DAC values
      soft_reset        => soft_reset_i);     -- out Reset state machines

  -- purpose: manage pulse calibration mode.  When this is active, we override
  --          certain of the registers by the values set by this module. 
  cal_man : entity work.cal_manager
    port map (
      clk         => clk,                     -- in  Clock
      rstb        => rstb,                    -- in  Reset
      enable      => cal_man_start_i,         -- in  Start pulser mode
      busy        => cal_man_busy_i,          -- out We're active
      clear_seq   => cal_clear_seq_i,         -- Reset VA shift reg.
      l2          => l2b_i,                   -- in  L2 trigger
      iterations  => cal_iter,                -- in  Number of iterations
      step        => cal_level(15 downto 8),  -- in  Pulser step size
      strip_min   => strips(7 downto 0),      -- in  Min strip
      strip_max   => strips(15 downto 8),     -- in  Max strip
      strip       => cal_man_strip_i,         -- out output strip no
      pulse       => cal_man_pulse_i,         -- out Pulse size
      change      => cal_man_change_i,        -- out 
      change_busy => dac_busy_i);             -- in

  -- purpose: Validate triggers, and make fake triggers.  Note, that
  --          we explicity inhibit triggers if we are changing DAC
  --          values.
  box : entity work.trigger_box
    generic map (
      GLITCH => true,                   -- Disable glitch filter on L0
      L0_DUR => 1,                      -- Length of L0
      L1_DUR => 16,                     -- Length of L1
      L2_DUR => 4)                      -- Length of L2
    port map (
      clk        => clk,                -- in  Clock
      rstb       => rstb,               -- in  Reset (active low)
      start      => start_box_i,        -- in  Make a fake trigger sequence
      l0_only    => l0_only_i,          -- in  Only make a fake L0
      inhibit    => dac_busy_i,         -- in  Inhibit triggers
      l0         => l0,                 -- in  Real L0 trigger (active high)
      l1b        => l1b,                -- in  Real L1 trigger (active low)
      l2b        => l2b,                -- in  Real L2 trigger (active low)
      l1_timeout => l1_timeout,         -- in  Timeout for L1
      l2_timeout => l2_timeout,         -- in  Timeout for L2
      l0_out     => l0_i,               -- out The L0 trigger  (fake or real)
      l1b_out    => l1b_i,              -- out The L1 trigger (fake or real)
      l2b_out    => l2b_i,              -- out The L2 trigger (fake or real)
      l1r        => l1r_i,              -- out L1 reject (timeout)
      l2r        => l2r_i,              -- out L1 reject (timeout)
      busy       => box_busy_i,         -- out Status: trigger busy
      timeout    => timeout_i,          -- out Status: trigger timeout
      overlap    => overlap_i);         -- out Status : overlapping

  -- purpose: Pulling down hold, and start the read-out
  handle : entity work.trigger_handler(rtl2)
    port map (
      clk             => clk,                -- in  Clock
      rstb            => rstb,               -- in  Async reset
      l0              => l0_i,               -- in  Level 0 (+)
      l1              => l1b_i,              -- in  Level 1 (-)
      l2              => l2b_i,              -- in  Level 2 (-)
      l2r             => l2r_i,              -- in L2 reject
      l1r             => l1r_i,              -- in L1 reject
      hold            => hold_i,             -- out Hold to VA1s
      busy            => handler_busy_i,     -- out Busy to CTP
      hold_wait       => hold_wait,          -- in  wait hold
      cal_delay       => cal_delay,          -- in Extra delay
      sequencer_start => sequencer_start_i,  -- out Start seq.
      sequencer_busy  => sequencer_busy_i,   -- in  Seq. busy
      sequencer_clear => trg_clear_seq_i,    -- out Clear seq. 
      cal_on          => cal_on_i,           -- in  Calib. mode
      pulser_enable   => cal_enable_i,       -- out Step cal.
      incomplete      => incomplete_i);      -- out Status

  -- purpose: Clock out the VA1's
  readout : entity work.va1_readout
    generic map (
      DRESET_LENGTH => 4,                  -- dreset length
      L1_LENGTH     => 32,                 -- L1 output length
      SIZE          => 127)
    port map (
      clk           => clk,                -- in  Clock
      rstb          => rstb,               -- in  Async reset
      clear         => sequencer_clear_i,  -- in  Clear
      va_clk        => va_clk_i,           -- in  VA input clock
      first         => strip_first_i,      -- in  First strip
      last          => strip_last_i,       -- in  Last strip
      start         => sequencer_start_i,  -- in  Start
      busy          => sequencer_busy_i,   -- out Busy
      l1            => altro_l1_i,         -- out L1 to ALTROs
      shift_clk     => shift_clk_i,        -- out Burst 
      shift_in      => shift_in_i,         -- out Reset shift-reg
      digital_reset => dreset_i);          -- out Reset VA1s

  -- purpose: Adjust BIAS an pulser settings.  Note, that we cannot do
  --          this when the trigger handler is busy.
  adjust : entity work.dac_interface
    generic map (
      DELAYED       => true)                     -- Allow delayed execution
    port map (
      shape_bias_0  => shape_bias(7 downto 0),   -- in  Bias (lower)
      shape_bias_1  => shape_bias(15 downto 8),  -- in  Bias (upper)
      vfp_0         => vfp(7 downto 0),          -- in  VFP low
      vfp_1         => vfp(15 downto 8),         -- in  VFP upper
      vfs_0         => vfs(7 downto 0),          -- in  VFS low  
      vfs_1         => vfs(15 downto 8),         -- in  VFS upper
      cal_pulse_lvl => pulse_lvl_i,              -- in  Calibration
      test_analog   => cal_level(15 downto 8),   -- in  Not used
      clk           => clk,                      -- in  Clock
      rstb          => rstb,                     -- in  Reset (active low)
      start         => dac_start_i,              -- in  Command signal
      inhibit       => handler_busy_i,           -- in  Inhibit DAC
      busy          => dac_busy_i,               -- out Busy 
      dac_addr      => dac_addr,                 -- out DAC address
      dac_data      => dac_data);                -- out DAC value

  -- purpose: Clock generator for shift clock 
  va_clk_gen : entity work.clock_gen(rtl5)
    generic map (
      WIDTH  => 8,                              -- Width of divider
      IN_POL => '1',                            -- rising edge 
      CATCH  => "Y")                            -- lots of logic
    port map (
      clk          => clk,                      -- in  Master input clock
      rstb         => rstb,                     -- in  Master Rstb
      sync         => sclk,                     -- sample clock to sync to 
      clk_en_o     => open,                     -- out Rising edge
      clk_o        => va_clk_i,                 -- out Gen. clock
      out_polarity => '1',                      -- in  Polarity
      div_phase    => shift_div(7 downto 0),    -- in  offset
      div_count    => shift_div(15 downto 8));  -- in Factor

--  -- purpose: Clock generator for sample clock
--  sclk_gen : entity work.clock_gen
--    generic map (
--      WIDTH        => 8,                         -- Width of divider
--      IN_POL       => '1',                       -- rising edge 
--      CATCH        => "Y")                       -- lots of logic
--    port map (
--      clk          => clk,                       -- in  Master input clock
--      rstb         => rstb,                      -- in  Master Rstb
--      clk_en_o     => open,                      -- out Rising edge
--      clk_o        => sample_clk_i,              -- out Gen. clock
--      out_polarity => '1',                       -- in  Polarity
--      div_phase    => sample_div(7 downto 0),    -- in  offset
--      div_count    => sample_div(15 downto 8));  -- in off

end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package fmdd_pack is
  component fmdd
    port (clk           : in  std_logic;  -- Clock
          sclk          : in  std_logic;  -- Sample clock
          rstb          : in  std_logic;  -- Async reset
          l0            : in  std_logic;  -- L0 trigger
          l1b           : in  std_logic;  -- L1 trigger
          l2b           : in  std_logic;  -- L2 trigger
          hold_wait     : in  std_logic_vector(15 downto 0);  -- Wait to hold
          l1_timeout    : in  std_logic_vector(15 downto 0);  -- L1 timeout
          l2_timeout    : in  std_logic_vector(15 downto 0);  -- L2 timeout
          busy          : out std_logic;  -- Busy signal
          l2r           : out std_logic;  -- L2 reject (timeout)
          shift_div     : in  std_logic_vector(15 downto 0);  -- Shift clock params
          shift_in      : out std_logic;  -- Shift reg. reset to VA1s (-)
          shift_clk     : out std_logic;  -- Shift clock to VA1s (-)
          strips        : in  std_logic_vector(15 downto 0);  -- Strip range
          digital_reset : out std_logic;  -- Reset VA1s (+)
          hold          : out std_logic;  -- Hold values in VA1 (-)
          cal_level     : in  std_logic_vector(15 downto 0);  -- Cal. level
          cal_iter      : in  std_logic_vector(15 downto 0);  -- Cal. events
          cal_on        : out std_logic;  -- Calib mode on (-)
          cal_enable    : out std_logic;  -- Enable cal step (-)
          cal_delay     : in  std_logic_vector(15 downto 0);  -- Extra delay
          shape_bias    : in  std_logic_vector(15 downto 0);  -- Shape bias
          vfs           : in  std_logic_vector(15 downto 0);  -- Shape ref.
          vfp           : in  std_logic_vector(15 downto 0);  -- Pre-amp. ref.
          dac_addr      : out std_logic_vector(11 downto 0);  -- DAC address
          dac_data      : out std_logic_vector(7 downto 0);   -- DAC value
          sample_div    : in  std_logic_vector(15 downto 0);  -- Sample clock params
          sample_clk    : out std_logic;  -- Sample clock to ALTROs
          altro_l1      : out std_logic;  -- L1 to ALTROs
          altro_l2      : out std_logic;  -- L2 to ALTROs
          command       : in  std_logic_vector(15 downto 0);  -- Commands
          status        : out std_logic_vector(15 downto 0);
          debug         : out std_logic_vector(14 downto 0));  -- Status
  end component;
end package fmdd_pack;
------------------------------------------------------------------------------
--
-- EOF
--
