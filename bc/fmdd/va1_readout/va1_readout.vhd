------------------------------------------------------------------------------
-- Title      : Send read-out sequence to VA1s
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : va1_readout_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-09-03
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This module takes care of giving the right read-out seqeunce to
--              the VA1 interface.
--
--              The sequence is as follows:
--
--              1.  Upon reciving a start signal (from the trigger
--                  handler), this module waits until the next rising
--                  edge of the va1 shift clock (va_clk), and then
--                  asserts the external l1 (active low).
--              2.  Then we wait for the falling edge of the shift
--                  clock, and assert the shift clock enable.  We wait
--                  this long to allow the ALTROs to sample the first
--                  strip (first).
--              3.  Starting from the current count (internal signal
--                  i), it counts up to the current maximum (internal
--                  register last_i), still asserting the
--                  va_clk_en. This will give (last_i - first_i)
--                  strobes of the shift clock, corresponding to
--                  readin out (last_i - first_i + 1) strips.
--              4.  After reaching last_i, the module prepares for the
--                  next start signal.  It does so, by first asserting
--                  the digital reset (digital_reset) for a specified
--                  number of clock cycles (DRESET_LENGTH).
--              5.  Then, the module updates the minimum and maximum
--                  (internal registers first_i and last_i) from the
--                  external memory (first, last).
--              6.  The the module asserts the shift register reset
--                  (shift_in_b), and enables the VA1 shift clock for
--                  exactly one VA1 clock cycle.  This resets the
--                  VA1's shift register to the first strip (0).
--              7.  Counting from 1 (internal signal i), the module
--                  enables the va1 clock for the current minimum
--                  (internal register first_i).  This will shift the
--                  data output of the VA1's to the first_i'th strip.
--              8.  Then the module goes to the idle state.
--
--              In the case that first=last, that is, we only want one
--              strip read out (for example when pulsing), steps 3 to
--              7 are skipped.
--
--              The internal range registers (first_i and last_i) are
--              updated from the external input (first and last) when
--              ever these change value.  When a change is detected,
--              the module performs steps 6 to 8.
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/26  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

----------------------------------------------------------------------
entity va1_readout is
  generic (
    DRESET_LENGTH : integer := 4;       -- dreset length in clk cycles
    L1_LENGTH     : integer := 8;       -- L1 output length in clk cycles
    SIZE          : integer := 128);
  port (
    clk           : in  std_logic;      -- Board clock
    rstb          : in  std_logic;      -- Rstb the state-machine
    clear         : in  std_logic;      -- Clear state machine 
    va_clk        : in  std_logic;      -- VA input clock
    first         : in  std_logic_vector (7 downto 0);  -- Minimum strip
    last          : in  std_logic_vector (7 downto 0);  -- Maximum strip
    start         : in  std_logic;      -- Start signal
    busy          : out std_logic;      -- Stop signal
    l1            : out std_logic;      -- L1 to ALTRO's (min 200ns long)
    shift_clk     : out std_logic;      -- VA Read-out clock enable
    shift_in      : out std_logic;      -- Enable read-out of VA chips
    digital_reset : out std_logic);     -- Reset digital part of VA's
end va1_readout;

-------------------------------------------------------------------------------
architecture rtl of va1_readout is
  type state_t is (idle,
                   clocking_va,
                   clocking_wait_va_clk,
                   clocking_wait_falling_edge,
                   clocking_wait_rissing_edge,
                   reset,
                   setup,
                   setup_check_va_clk,
                   setup_wait_va_clk,
                   setup_wait_next_va_clk,
                   setup_2_min,
                   keep_l1);            -- States

  signal va_clk_i    : std_logic;                        -- Old VA clok value
  signal va_clk_en_i : std_logic;                        -- Enable out clock
  signal state_i     : state_t;                          -- State variable
  -- Counters
  signal last_i      : integer range 0 to SIZE := SIZE;  -- Last strip
  signal first_i     : integer range 0 to SIZE := 0;     -- First strip
  signal shift_cnt_i : integer range 0 to SIZE;          -- Count clocks
  signal reset_cnt_i : integer range 0 to 15;            -- Width of Reset
  signal l1_cnt_i    : integer range 0 to L1_LENGTH+10;  -- Width of L1

begin  -- rtl
  -- purpose: Delay shift clock by one clock cycle, to align with L1
  -- type   : sequential inputs : clk, rstb, va_clk_en_i outputs:
  --          shift_clk
  delay_clk : process (clk, rstb) is
  begin  -- process delay_clk
    if rstb = '0' then                  -- asynchronous reset (active low)
      shift_clk <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if va_clk_en_i = '1' then
        shift_clk <= not va_clk;
      else
        shift_clk <= '1';        
      end if;
    end if;
  end process delay_clk;
  -- shift_clk <= not va_clk when va_clk_en_i = '1' else '1';
  

  -- purpose: State machine controlling the sequencing of the VA chips
  -- type   : sequential
  -- inputs : clk
  -- outputs: stop, va_clk_en_i, digital_reset
  process (clk, rstb, clear)
  begin  -- process
    if rstb = '0' then
      va_clk_en_i   <= '0';
      shift_in      <= '1';             -- negative logic
      busy          <= '0';
      digital_reset <= '1';
      state_i       <= reset;
      shift_cnt_i   <= 0;
      reset_cnt_i   <= DRESET_LENGTH;
      va_clk_i      <= '0';
      first_i       <= 0;               -- to_integer(unsigned(first));
      last_i        <= 0;               -- to_integer(unsigned(last));
      l1            <= '1';             -- negative logic
    elsif clear = '1' then
      va_clk_en_i   <= '0';
      shift_in      <= '1';             -- negative logic
      busy          <= '0';
      digital_reset <= '1';
      state_i       <= reset;
      shift_cnt_i   <= 0;
      reset_cnt_i   <= DRESET_LENGTH;
      va_clk_i      <= '0';
      first_i       <= to_integer(unsigned(first));
      last_i        <= to_integer(unsigned(last));
      l1            <= '1';             -- negative logic      
    elsif clk'event and clk = '1' then  -- rising clock edge
      --  and clk_en = '1'
      state_i       <= state_i;
      busy          <= '1';
      va_clk_i      <= va_clk;
      l1            <= '1';
      digital_reset <= '0';
      shift_cnt_i   <= shift_cnt_i;

      case state_i is

        when idle =>
          state_i       <= state_i;
          busy          <= '0';
          if start = '1' then
            state_i     <= clocking_wait_va_clk;
          end if;
          -- If we get new values of the first and last, reset immediately if
          -- we are in the idle state. 
          if (first_i /= to_integer(unsigned(first)) or
              last_i /= to_integer(unsigned(last))) then
            state_i     <= reset;
            reset_cnt_i <= DRESET_LENGTH;
          end if;

        when reset =>
          -- Send reset to VA1's and take new value of range
          digital_reset <= '1';
          first_i       <= to_integer(unsigned(first));
          last_i        <= to_integer(unsigned(last));
          if reset_cnt_i = 1 then
            state_i     <= setup;
            reset_cnt_i <= 0;
          else
            reset_cnt_i <= reset_cnt_i - 1;  -- Hold down reset
          end if;

        when setup =>
          -- Wait until a VA1 clk falling edge 
          digital_reset <= '0';
          if va_clk = '0' then
            state_i     <= setup_check_va_clk;
          end if;

        when setup_check_va_clk =>
          -- Check that we have a falling clock edge 
          if va_clk = '0' then
            va_clk_en_i   <= '1';
            -- If the curser is before min, then we simply clock, other wise,
            -- we need to go back to the start, and clock up to min.  This is
            -- needed to make the pulser calibration sequence faster.

            --  Temporarily disabled since it seems that we have problems with
            --  this. 
            -- if shift_cnt_i < first_i then
            -- state_i     <= setup_2_min;
            -- else
              -- Take down (active low) the shift_in 
              shift_in    <= '0';
              shift_cnt_i <= 0;
              state_i     <= setup_wait_va_clk;
            -- end if;
          end if;

        when setup_wait_va_clk =>
          -- Waiting for a rising clock edge 
          va_clk_en_i <= '1';
          if va_clk = '1' then
            state_i   <= setup_wait_next_va_clk;
          end if;

        when setup_wait_next_va_clk =>
          -- Waiting for next falling clock edge 
          if va_clk = '0' then
            -- First clock for priming
            shift_cnt_i <= 0;
            shift_in    <= '1';
            state_i     <= setup_2_min;
            -- if 1 >= first_i then
            --   va_clk_en_i <= '0';
            --   state_i     <= idle;
            -- else
            --   state_i     <= setup_2_min;
            -- end if;
          end if;

        when setup_2_min =>
          -- Enable clock as long as we need to setup to first strip
          va_clk_en_i   <= '1';
          if va_clk = '0' and shift_cnt_i >= first_i then
            state_i     <= idle;
            va_clk_en_i <= '0';
          end if;
          -- Only count on the edges of the clock
          if va_clk = '1' and va_clk_i = '0' then
            shift_cnt_i <= shift_cnt_i + 1;
          end if;

        when clocking_wait_va_clk =>
          -- Wait at least one full shift clock
          if va_clk = '1' and va_clk_i = '0' then
            state_i  <= clocking_wait_falling_edge;
            l1       <= '0';
            l1_cnt_i <= L1_LENGTH;
          end if;

        when clocking_wait_falling_edge =>
          -- Keep L1 down as long as needed
          if l1_cnt_i > 1 then
            l1       <= '0';
            l1_cnt_i <= l1_cnt_i-1;
          end if;
          -- wait for rissing edge
          if va_clk = '0' and va_clk_i = '1' then
            state_i  <= clocking_wait_rissing_edge;
          end if;

        when clocking_wait_rissing_edge =>
          -- Keep L1 down as long as needed
          if l1_cnt_i > 1 then
            l1            <= '0';
            l1_cnt_i      <= l1_cnt_i-1;
          end if;
          -- Waiting for first clock edge
          if va_clk = '0' then
            -- If the counter is already there (e.g., first=last), go
            -- to reset immediately.  Alternatively, we could go to
            -- idle, leaving the channel open.
            if shift_cnt_i >= last_i then
              -- report "Counter " & integer'image(shift_cnt_i) &
              --   " is already >= " & integer'image(last_i) severity note;
              -- state_i <= reset;
              state_i     <= keep_l1;
              -- shift_cnt_i <= 0;      -- We shouldn't reset the counter!
              reset_cnt_i <= DRESET_LENGTH;
            else
              state_i     <= clocking_va;
              va_clk_en_i <= '1';
            end if;
          end if;

        when clocking_va =>
          -- Keep L1 down as long as needed
          if l1_cnt_i > 1 then
            l1          <= '0';
            l1_cnt_i    <= l1_cnt_i-1;
          else
            l1_cnt_i    <= 0;
          end if;
          -- Now clocking through shift register 
          va_clk_en_i   <= '1';
          if va_clk = '0' and shift_cnt_i >= last_i then
            va_clk_en_i <= '0';         -- Remove enable before switching state
          elsif va_clk = '1' and va_clk_i = '0' and shift_cnt_i >= last_i then
            state_i     <= reset;
            va_clk_en_i <= '0';
            -- shift_cnt_i <= 0;        -- Probably shouldn't be here
            reset_cnt_i <= DRESET_LENGTH;
          elsif va_clk = '1' and va_clk_i = '0' then
            shift_cnt_i <= shift_cnt_i + 1;
          end if;

        when keep_l1 =>
          if l1_cnt_i > 1 then
            l1       <= '0';
            l1_cnt_i <= l1_cnt_i - 1;
          else
            l1_cnt_i <= 0;
            state_i  <= idle;
          end if;
        when others  =>
          state_i    <= idle;
      end case;
    end if;
  end process;

  
end rtl;

----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package va1_readout_pack is
  component va1_readout
    generic (
      DRESET_LENGTH : integer;          -- dreset length in clk cycles
      L1_LENGTH     : integer;          -- L1 output length in clk cycles
      SIZE          : integer); 
    port (
      clk           : in  std_logic;    -- Board clock
      rstb          : in  std_logic;    -- Rstb the state-machine
      clear         : in  std_logic;    -- Clear state machine 
      va_clk        : in  std_logic;    -- VA input clock
      first         : in  std_logic_vector (7 downto 0);  -- Minimum strip
      last          : in  std_logic_vector (7 downto 0);  -- Maximum strip
      start         : in  std_logic;    -- Start signal
      busy          : out std_logic;    -- Stop signal
      l1            : out std_logic;    -- L1 to ALTRO's (min 200ns long)
      shift_clk     : out std_logic;    -- VA Read-out clock enable
      shift_in      : out std_logic;    -- Enable read-out of VA chips
      digital_reset : out std_logic);   -- Reset digital part of VA's
  end component va1_readout;
end va1_readout_pack;
----------------------------------------------------------------------
--
-- EOF
--

