------------------------------------------------------------------------------
-- Title      : Send read-out sequence to VA1s
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : va1_readout_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-09-03
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This component sends a read-out sequence to the VA1s.
--              1: When the `start' signal is asserted, the state 
--                 machine starts.
--              2: First, it waits for one shift clock cycle to allow the
--                 first strip to be sampled
--              3: Then it gives max-min strobes of the shift clock to read
--                 out the remaing strips.
--              4: When that's done, it gives a digitial reset to the
--                 connected VA1s.
--              5: Then, it resets the shift registers on the VA1 to the first
--                 strip.
--              6: Finally, it gives min strobes of shift clock to shift the 
--                shift register into the first requested strip.
--              In the case that min=max, that is, we only want one strip read
--              out (for example when pulsing), steps 3 to 5 are skipped
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/26  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.va1_readout_pack.all;
use work.fmdd_simul_pack.all;

-------------------------------------------------------------------------------
entity va1_readout_tb is
end va1_readout_tb;

-------------------------------------------------------------------------------
architecture test of va1_readout_tb is
  constant SCLK_DIV        : integer   := 16;
  constant VACLK_DIV       : integer   := 16;
  constant DRESET_LENGTH   : integer   := 4;
  constant L1_LEN          : integer   := 2;
  signal   vaclk_phase     : integer   := 0;
  signal   clk_i           : std_logic := '0';
  signal   sclk_i          : std_logic := '0';
  signal   rstb_i          : std_logic := '0';
  signal   va_clk_i        : std_logic := '0';
  signal   va_clk_ii       : std_logic := '0';
  signal   first_i         : std_logic_vector (7 downto 0);
  signal   last_i          : std_logic_vector (7 downto 0);
  signal   start_i         : std_logic := '0';
  signal   busy_i          : std_logic;
  signal   shift_clk_i     : std_logic;
  signal   shift_in_i      : std_logic;
  signal   digital_reset_i : std_logic;
  signal   l1_i            : std_logic;       -- Output clock for va1
  signal   cnt_i           : integer   := 0;  -- counter
  signal   phases_i        : std_logic_vector(31 downto 0);
  signal   gsclk_i         : std_logic;
  
    -- purpose: Generate a trigger
  procedure make_sclk_l1 (constant len : in  integer;    -- Length of trigger
                          signal sclk  : in  std_logic;  -- Clock
                          signal l1    : out std_logic) is
  begin  -- make_l1
    wait until rising_edge(sclk);
    l1 <= '1', '0' after len * SCLK_DIV * PERIOD;
  end make_sclk_l1;

begin  -- test

  dut : entity work.va1_readout(rtl)
    generic map (
      DRESET_LENGTH => DRESET_LENGTH,     -- dreset length in clk cycles
      L1_LENGTH     => 8,                 -- L1 output length in clk cycles
      SIZE          => 127)
    port map (
      clk           => clk_i,             -- in  Board clock
      sclk          => sclk_i,
      rstb          => rstb_i,            -- in  Rstb the state-machine
      clear         => '0',               -- in  Clear state machine
      va_clk        => va_clk_i,          -- in  VA input clock
      first         => first_i,           -- in  Minimum strip
      last          => last_i,            -- in  Maximum strip
      start         => start_i,           -- in  Start signal
      busy          => busy_i,            -- out Stop signal
      l1            => l1_i,              -- out L1 to ALTRO's (min 200ns long)
      shift_clk     => shift_clk_i,       -- out VA Read-out clock enable
      shift_in      => shift_in_i,        -- out Enable read-out of VA chips
      digital_reset => digital_reset_i);  -- out Reset digital part of VA's


  -- Simulation stimuli
  stim : process
  begin  -- process stim
    rstb_i  <= '0';
    change_range(s1    => 0, s2 => 4, clk => clk_i,
                 first => first_i, last => last_i);
    wait for 2 * PERIOD;
    rstb_i  <= '1';

    wait_for_busy(busy => busy_i, clk => clk_i);

    for i in 0 to 39 loop
      wait for 1 us;
      if i mod 4 = 0 then
        vaclk_phase <= i / 4;
        report "Changing phase to " & integer'image(vaclk_phase) severity note;
        wait for 1 us;
      end if;
      make_sclk_l1(len => L1_LEN, sclk => sclk_i, l1 => start_i);
      wait_for_busy(busy => busy_i, clk => clk_i);
    end loop;  -- i

    wait for 10 us;
    make_sclk_l1(len => L1_LEN, sclk => sclk_i, l1 => start_i);
    wait_for_busy(busy => busy_i, clk => clk_i);

    wait for 1 us;
    make_sclk_l1(len => L1_LEN, sclk => sclk_i, l1 => start_i);
    wait for 8 * PERIOD;
    change_range(s1    => 3, s2 => 5, clk => clk_i,
                 first => first_i, last => last_i);
    wait_for_busy(busy => busy_i, clk => clk_i);

    wait for 1 us;
    make_sclk_l1(len => L1_LEN, sclk => sclk_i, l1 => start_i);
    wait_for_busy(busy => busy_i, clk => clk_i);

    wait for 1 us;
    change_range(s1    => 2, s2 => 2, clk => clk_i,
                 first => first_i, last => last_i);
    wait_for_busy(busy => busy_i, clk => clk_i);

    for i in 0 to 4 loop
      wait for 1 us;
      make_sclk_l1(len => L1_LEN, sclk => sclk_i, l1 => start_i);
      wait_for_busy(busy => busy_i, clk => clk_i);
    end loop;  -- i

    wait for 1 us;
    change_range(s1    => 3, s2 => 3, clk => clk_i,
                 first => first_i, last => last_i);
    wait_for_busy(busy => busy_i, clk => clk_i);

    for i in 0 to 2 loop
      wait for 1 us;
      make_sclk_l1(len => L1_LEN, sclk => sclk_i, l1 => start_i);
      wait_for_busy(busy => busy_i, clk => clk_i);
    end loop;  -- i
    
    wait for 1 us;
    change_range(s1    => 0, s2 => 127, clk => clk_i,
                 first => first_i, last => last_i);

    wait for 1 us;
    make_sclk_l1(len => L1_LEN, sclk => sclk_i, l1 => start_i);
    wait_for_busy(busy => busy_i, clk => clk_i);

    wait;
  end process stim;

  -- clock
  clock_gen : process
  begin  -- process
    clk_i <= '0';
    wait for PERIOD/2;
    clk_i <= '1';
    wait for PERIOD/2;
  end process;

  -- clock
  sclock_gen : process
  begin  -- process
    sclk_i <= not sclk_i;
    wait for SCLK_DIV * PERIOD / 2;
  end process;

  -- clock
  va_clock_gen : process
  begin  -- process
    va_clk_ii <= not va_clk_ii;
    wait for VACLK_DIV * PERIOD / 2;
  end process;

  process (clk_i, rstb_i)
    variable i : integer;
  begin  -- process
    if rstb_i = '0' then                  -- asynchronous reset (active low)
      phases_i <= (others => '0');
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      phases_i(0) <= va_clk_ii;
      for i in 1 to phases_i'length-1 loop
        phases_i(i) <= phases_i(i-1);
      end loop;  -- i
    end if;
  end process;

  va_clk_i <= phases_i(vaclk_phase);
  gsclk_i <= sclk_i and (start_i or busy_i);
  
  -- purpose: Count number of pulses in burst
  -- type   : combinational
  -- inputs : start_i, va_clk_o, busy_i
  -- outputs: 
  count_burst : process (start_i, shift_clk_i, busy_i)
  begin  -- process count_burst
    if start_i = '1' then
      cnt_i   <= 0;
    elsif busy_i = '1' and falling_edge(shift_clk_i) then
      if shift_in_i = '0' then
        cnt_i <= 0;
      else
        cnt_i <= cnt_i + 1;
      end if;
    end if;
  end process count_burst;


end test;
------------------------------------------------------------------------------
--
-- EOF
--
