------------------------------------------------------------------------------
-- Title      : Test bench of FMDD top-level entity
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : fmdd_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2007/01/25
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/28  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.fmdd_simul_pack.all;
use work.fmdd_pack.all;

------------------------------------------------------------------------------
entity fmdd_tb is
end entity fmdd_tb;

------------------------------------------------------------------------------
architecture test of fmdd_tb is
  signal clk_i           : std_logic := '0';               -- Clock
  signal rstb_i          : std_logic := '0';               -- Async reset
  signal l0_i            : std_logic := '0';               -- L0 trigger
  signal l1b_i           : std_logic := '1';               -- L1 trigger
  signal l2b_i           : std_logic := '1';               -- L2 trigger
  signal hold_wait_i     : std_logic_vector(15 downto 0);  -- Wait to hold
  signal l1_timeout_i    : std_logic_vector(15 downto 0);  -- L1 timeout
  signal l2_timeout_i    : std_logic_vector(15 downto 0);  -- L2 timeout
  signal busy_i          : std_logic;                      -- Busy signal
  signal shift_div_i     : std_logic_vector(15 downto 0);  -- Shift clock
  signal shift_in_i      : std_logic;                      -- Reg. reset to
  signal shift_clk_i     : std_logic;                      -- Shift clock 
  signal strips_i        : std_logic_vector(15 downto 0);  -- Strip range
  signal digital_reset_i : std_logic;                      -- Reset VA1s
  signal hold_i          : std_logic;                      -- Hold in VA1
  signal cal_level_i     : std_logic_vector(15 downto 0);  -- Cal. level
  signal cal_on_i        : std_logic;                      -- Calib mode on
  signal cal_enable_i    : std_logic;                      -- Enable cal step
  signal cal_iter_i      : std_logic_vector(15 downto 0);  -- Cal iterations
  signal shape_bias_i    : std_logic_vector(15 downto 0);  -- Shape bias
  signal vfs_i           : std_logic_vector(15 downto 0);  -- Shape ref. 
  signal vfp_i           : std_logic_vector(15 downto 0);  -- Pre-amp. ref.
  signal dac_addr_i      : std_logic_vector(11 downto 0);  -- DAC address
  signal dac_data_i      : std_logic_vector(7 downto 0);   -- DAC value
  signal sample_div_i    : std_logic_vector(15 downto 0);  -- Sample clock
  signal sample_clk_i    : std_logic;                      -- Sample clock
  signal altro_l1_i      : std_logic;                      -- L1 to ALTROs
  signal altro_l2_i      : std_logic;                      -- L2 to ALTROs
  signal command_i       : std_logic_vector(15 downto 0);  -- Commands
  signal status_i        : std_logic_vector(15 downto 0);  -- Status
  signal debug_i         : std_logic_vector(14 downto 0);  -- Debug

begin  -- architecture test
  -- FMDD top-level 
  dut : entity work.fmdd
    port map (
        clk           => clk_i,            -- in  Clock
        rstb          => rstb_i,           -- in  Async reset
        l0            => l0_i,             -- in  L0 trigger
        l1b           => l1b_i,            -- in  L1 trigger
        l2b           => l2b_i,            -- in  L2 trigger
        hold_wait     => hold_wait_i,      -- in  Wait to hold
        l1_timeout    => l1_timeout_i,     -- in  L1 timeout
        l2_timeout    => l2_timeout_i,     -- in  L2 timeout
        busy          => busy_i,           -- out Busy signal
        shift_div     => shift_div_i,      -- in  Shift clock params
        shift_in      => shift_in_i,       -- out Shift reg. reset to VA1s
        shift_clk     => shift_clk_i,      -- out Shift clock to VA1s
        strips        => strips_i,         -- in  Strip range
        digital_reset => digital_reset_i,  -- out Reset VA1s
        hold          => hold_i,           -- out Hold values in VA1
        cal_level     => cal_level_i,      -- in  Cal. level
        cal_on        => cal_on_i,         -- out Calib mode on
        cal_enable    => cal_enable_i,     -- out Enable cal step
        shape_bias    => shape_bias_i,     -- in  Shape bias
        vfs           => vfs_i,            -- in  Shape ref. 
        vfp           => vfp_i,            -- in  Pre-amp. ref.
        cal_iter      => cal_iter_i,       -- Calibration iterations 
        dac_addr      => dac_addr_i,       -- out DAC address
        dac_data      => dac_data_i,       -- out DAC value
        sample_div    => sample_div_i,     -- in  Sample clock params
        sample_clk    => sample_clk_i,     -- out Sample clock to ALTROs
        altro_l1      => altro_l1_i,       -- out L1 to ALTROs
        altro_l2      => altro_l2_i,       -- out L2 to ALTROs
        command       => command_i,        -- in  Commands
        status        => status_i,         -- out Status
        debug         => debug_i);         -- out Debug

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli : process is
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';
    -- Wait until initial setup from DAC is done 
    wait_for_busy(busy => status_i(2),
                  clk  => clk_i);
    report "Setup is done" severity note;

    wait for 2 us - NOW;
    make_triggers(clk => clk_i,
                  l0 => l0_i,
                  l1 => l1b_i,
                  l2 => l2b_i);

    wait;                               -- forever 
  end process stimuli;

  -- purpose: Initial set up of the registers
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  setup : process is
  begin  -- process setup
    command_i                <= (others => '0');
    change_clock(phase                  => 0,
                 count                  => 4,
                 div_count              => sample_div_i(15 downto 8),
                 div_phase              => sample_div_i(7 downto 0));
    change_clock(phase                  => 3,
                 count                  => 8,
                 div_count              => shift_div_i(15 downto 8),
                 div_phase              => shift_div_i(7 downto 0));
    change_biases (shape0               => 11,
                   shape1               => 12,
                   vfs0                 => 13,
                   vfs1                 => 14,
                   vfp0                 => 15,
                   vfp1                 => 16,
                   cal                  => 17,
                   shape_0              => shape_bias_i(7 downto 0),
                   shape_1              => shape_bias_i(15 downto 8),
                   vfs_0                => vfs_i(7 downto 0),
                   vfs_1                => vfs_i(15 downto 8),
                   vfp_0                => vfp_i(7 downto 0),
                   vfp_1                => vfp_i(15 downto 0),
                   cal_p                => cal_level_i(7 downto 0));
    cal_level_i(15 downto 0) <= (others => '0');
    change_timeouts(hold_delay          => 1.5 us,
                    l1_wait             => 6.5 us,
                    l2_wait             => 88 us,
                    clk                 => clk_i,
                    hold_wait           => hold_wait_i,
                    l1_timeout          => l1_timeout_i,
                    l2_timeout          => l2_timeout_i);
    change_range(s1                     => 0,
                 s2                     => 127,
                 clk                    => clk_i,
                 first                  => strips_i(7 downto 0),
                 last                   => strips_i(15 downto 8));
    cal_iter_i <= int2slv(100, 16);
    wait;                               -- forever 
  end process setup;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  clock_gen: process is
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;

end architecture test;

------------------------------------------------------------------------------
