------------------------------------------------------------------------------
-- Title      : Test bench of DAC interface
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : dac_interface_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/27
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.dac_interface_pack.all;
use work.fmdd_simul_pack.all;

------------------------------------------------------------------------------
entity dac_interface_tb is
end entity dac_interface_tb;

------------------------------------------------------------------------------

architecture test of dac_interface_tb is
  signal shape_bias_0_i  : std_logic_vector(7 downto 0);  -- Bias (lower)
  signal shape_bias_1_i  : std_logic_vector(7 downto 0);  -- Bias (upper)
  signal vfp_0_i         : std_logic_vector(7 downto 0);  -- VFP low
  signal vfp_1_i         : std_logic_vector(7 downto 0);  -- VFP upper
  signal vfs_0_i         : std_logic_vector(7 downto 0);  -- VFS low  
  signal vfs_1_i         : std_logic_vector(7 downto 0);  -- VFS upper
  signal cal_pulse_lvl_i : std_logic_vector(7 downto 0);  -- Calibration
  signal test_analog_i   : std_logic_vector(7 downto 0);  -- Not used
  signal clk_i           : std_logic := '0';  -- Clock
  signal rstb_i          : std_logic := '0';  -- Reset (active low)
  signal start_i         : std_logic := '0';  -- Command signal
  signal inhibit_i       : std_logic := '0';  -- Inhibit changing DAC's 
  signal busy_i          : std_logic;   -- Busy 
  signal dac_addr_i      : std_logic_vector(11 downto 0);  -- DAC address
  signal dac_data_i      : std_logic_vector(7 downto 0);  -- DAC value

begin  -- architecture test

  dut: entity work.dac_interface
    generic map (
      DELAYED => true)
    port map (
        shape_bias_0  => shape_bias_0_i,   -- in  Bias (lower)
        shape_bias_1  => shape_bias_1_i,   -- in  Bias (upper)
        vfp_0         => vfp_0_i,          -- in  VFP low
        vfp_1         => vfp_1_i,          -- in  VFP upper
        vfs_0         => vfs_0_i,          -- in  VFS low  
        vfs_1         => vfs_1_i,          -- in  VFS upper
        cal_pulse_lvl => cal_pulse_lvl_i,  -- in  Calibration
        test_analog   => test_analog_i,    -- in  Not used
        clk           => clk_i,            -- in  Clock
        rstb          => rstb_i,           -- in  Reset (active low)
        start         => start_i,          -- in  Command signal
        inhibit       => inhibit_i,        -- in  Inhibit changing DAC's 
        busy          => busy_i,           -- out Busy 
        dac_addr      => dac_addr_i,       -- out DAC address
        dac_data      => dac_data_i);      -- out DAC value

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli : process is
  begin  -- process stimuli
    test_analog_i <= X"ff";
    change_biases (
      shape0  => 1,
      shape1  => 2,
      vfs0    => 3,
      vfs1    => 4,
      vfp0    => 5,
      vfp1    => 6,
      cal     => 7,
      shape_0 => shape_bias_0_i,
      shape_1 => shape_bias_1_i,
      vfs_0   => vfs_0_i,
      vfs_1   => vfs_1_i,
      vfp_0   => vfp_0_i,
      vfp_1   => vfp_1_i,
      cal_p   => cal_pulse_lvl_i);
    wait for 100 ns;
    rstb_i        <= '1';
    wait_for_busy(busy => busy_i,
                  clk  => clk_i);

    wait for 100 ns;
    change_biases (
      shape0  => 11,
      shape1  => 12,
      vfs0    => 13,
      vfs1    => 14,
      vfp0    => 15,
      vfp1    => 16,
      cal     => 17,
      shape_0 => shape_bias_0_i,
      shape_1 => shape_bias_1_i,
      vfs_0   => vfs_0_i,
      vfs_1   => vfs_1_i,
      vfp_0   => vfp_0_i,
      vfp_1   => vfp_1_i,
      cal_p   => cal_pulse_lvl_i);

    wait for 3 us - NOW;
    start_it(start     => start_i,
             clk       => clk_i);
    wait_for_busy(busy => busy_i,
                  clk  => clk_i);

    wait for 6 us - NOW;
    inhibit_i <= '1', '0' after 200 ns;
    wait for 100 ns;
    start_it(start     => start_i,
             clk       => clk_i);
    wait_for_busy(busy => busy_i,
                  clk  => clk_i);
    
    wait;
  end process stimuli;

  -- purpose: make clock 
  clock_gen : process
  begin  -- process clocker
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;
end architecture test;
------------------------------------------------------------------------------
-- 
-- EOF
--
