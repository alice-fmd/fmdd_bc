------------------------------------------------------------------------------
-- Title      : Interface to DACs
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : dac_interface.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Interface to the DAC's on the FMD digitizier board.
--
--              In total there are 4 DAC's, which comes in two blocks
--              - one for each VA1 conntector.  Hence, they share the
--              values propagated to them.  Currently, the DAC values
--              are written out sequetially.  However, with minor
--              modifications, they could be written out in parallel
--              over the VA1 conntectors.
--
--              Note, that this entity and the trigger handler entity
--              are mutually exclusive.  That is, when this entity is
--              busy writting values to the DAC's, the `busy' line of
--              this entity is asserted, and the trigger box entity
--              blocks triggers as long as that line is asserted.
--              Also, when the trigger handler entity is busy, it
--              asserts the inhibit line of this entity.
--
--              The DAC's are distributed as
--
--                          | shape_bias |   vfp     |   vfs       |  cal
--                          | 0     1    | 0     1   | 0    1      |
--              ------------+------------+-----------+-------------+  -------
--              DAC ID      | 0     0    | 0     0   | 1    1      |   1
--              DAC ch      | 0     1    | 2     3   | 0    1      |   2
--              Range (V)   | 0 - -1.5   | -0.2 - -1 | -0.55 - 0.43| 0 - 1
--              Default (V) | -1         | -0.38     | -0.03       |   0
--              Step (V)    | -0.0059    | -0.031    | 0.0038      | 0.0004
--              Default     | 170 (0xaa) | 58 (0x3a) | 136 (0x88)  | 0 (0x00)
--
--              In the table above, the DAC Id is the offset in the
--              dac_addr from bit 2, so that if bit 3 of dac_addr is
--              '1' then DAC 1 is addressed.
--
--              On the output side of the DAC's we get values in the
--              range of 0 - Vref volts, where Vref ~ 3.2V.  Below is
--              a table voltages, the corresponding component, and pin
--              numbers, as well as their nominal output.
--
--              Name          Component  Pin  Default
--              -------------------------------------
--              shape_bias_0    IC103      2    2.30
--              shape_bias_1    IC103      1    2.30 
--              vfp_0           IC103     20    0.72 
--              vfp_1           IC103     19    0.72 
--              vfs_0           IC104      2    1.70
--              vfs_1           IC104      1    1.70
--              cal_pulse_lvl   IC104     20    0.0 
--              analog_test     IC104     19    1.60
--
--              Here's a schematic of the two adjecent DAC components.
--
--                vfp_0                  cal_pulse_lvl
--                  |                    |
--                  | vfp_1              | analog_test
--                  |   |                |   |
--                +--------------+     +--------------+    
--                | 20  19       |     | 20  19       |    
--                )    IC103     |     )    IC104     |    
--                | 1   2        |     | 1   2        |    
--                +--------------+     +--------------+    
--                  |   |                |   |
--                  | shape_bias_0       | vfs_0
--                  |                    |
--                 shape_bias_1        vfs_1
--
--              The output of these DAC's are put through some other
--              components to make the final supply voltages for the
--              VA1's.  These components, and the position of the
--              supply voltages are shown below.
--
--                vfp_0 (-0.38V)                 
--                  |                       |
--                  |   shape_bias_1 (-1V)  |     vfs_1 (-0.03V)
--                  |        |              |        | 
--                +------------+          +------------+    
--                | 14       8 |          | 14       8 |    
--                )    IC106   |          )    IC107   |    
--                | 1        7 |          | 1        7 |    
--                +------------+          +------------+    
--                  |        |              |        |
--                  |  cal_pulse_lvl (0V)   |     vfs_0 (-0.03V)
--                  |                       |
--                 shape_bias_0 (-1V)    vfp_1 (-0.38V)
--
--              FIXME: Perhaps we want to latch the start command, and in the
--              case that inhibit is low, wait until it's released and then go
--              through the cycle.  The cycle takes about 1.5 us so there
--              should be time after a trigger to go through it and do the
--              thing.   The Generic DELAYED enables this mode if set to true
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dac_interface is
  generic (
    DELAYED : boolean := true);         -- Allow delayed execution
  port (
    shape_bias_0  : in  std_logic_vector(7 downto 0);  -- Bias (lower)
    shape_bias_1  : in  std_logic_vector(7 downto 0);  -- Bias (upper)
    vfp_0         : in  std_logic_vector(7 downto 0);  -- VFP low
    vfp_1         : in  std_logic_vector(7 downto 0);  -- VFP upper
    vfs_0         : in  std_logic_vector(7 downto 0);  -- VFS low  
    vfs_1         : in  std_logic_vector(7 downto 0);  -- VFS upper
    cal_pulse_lvl : in  std_logic_vector(7 downto 0);  -- Calibration
    test_analog   : in  std_logic_vector(7 downto 0);  -- Not used
    clk           : in  std_logic;      -- Clock
    rstb          : in  std_logic;      -- Reset (active low)
    start         : in  std_logic;      -- Command signal
    inhibit       : in  std_logic;      -- Inhibit changing DAC's 
    busy          : out std_logic;      -- Busy 
    dac_addr      : out std_logic_vector(11 downto 0);  -- DAC address
    dac_data      : out std_logic_vector(7 downto 0));  -- DAC value
end dac_interface;

architecture rtl of dac_interface is
  constant NDAC         : integer  := 8;
  constant WRITE_LENGTH : integer  := 4;
  constant NBRANCH      : integer  := 5;  -- Number of `branches'
  type state_t is (idle, setup, new_dac, write_dac, finish);
  type dac_entry is
    record                                -- Record of DAC information
      dac_id            : integer;
      dac_ch            : integer;
    end record;
  type dac_list is array (NDAC - 1 downto 0) of dac_entry;
  type dac_values is array (NDAC - 1 downto 0) of std_logic_vector(7 downto 0);
  -- Assignment goes in order of declaration, that is from (NDAC - 1)
  -- downto 0. 
  constant addresses    : dac_list := (7 => (0, 0),
                                       6 => (0, 1),
                                       5 => (0, 2),
                                       4 => (0, 3),
                                       3 => (1, 0),
                                       2 => (1, 1),
                                       1 => (1, 2),
                                       0 => (1, 3));
  signal   values_i     : dac_values;
  signal   state_i      : state_t;
  signal   i            : integer range 0 to NDAC+1;
  signal   j            : integer range 0 to WRITE_LENGTH+1;
  signal   start_i      : std_logic;
begin  -- rtl

  -- purpose: Clock out values to the DACs
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  fsm             : process (clk, rstb)
    variable ch_i : std_logic_vector(1 downto 0);
    variable id_i : integer;
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      -- Rather than going to the idle state, we could go to the `setup' state
      -- to automatically initialize the DAC's.  However, that may not be
      -- feasible in the production environment.  
      -- state_i    <= idle;
      state_i     <= setup;
      values_i(7) <= shape_bias_0;
      values_i(6) <= shape_bias_1;
      values_i(5) <= vfp_0;
      values_i(4) <= vfp_1;
      values_i(3) <= vfs_0;
      values_i(2) <= vfs_1;
      values_i(1) <= cal_pulse_lvl;
      values_i(0) <= test_analog;
      dac_addr    <= (others => '1');
      dac_data    <= (others => '1');
      i           <= 0;
      busy        <= '1';
      start_i     <= '0';
      ch_i := "00";
      id_i := 0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      state_i     <= state_i;
      dac_addr    <= (others => '1');   -- write is active low
      dac_data    <= (others => '0');
      start_i     <= start_i;

      case state_i is
        when idle =>
          busy      <= '0';
          if start = '1' and inhibit = '1' and DELAYED then
            start_i <= '1';             -- Latch start 
          elsif (start = '1' or start_i = '1') and inhibit = '0' then
            state_i <= setup;
            start_i <= '0';
          end if;

        when setup =>
          busy        <= '1';
          -- Register values
          values_i(7) <= shape_bias_0;
          values_i(6) <= shape_bias_1;
          values_i(5) <= vfp_0;
          values_i(4) <= vfp_1;
          values_i(3) <= vfs_0;
          values_i(2) <= vfs_1;
          values_i(1) <= cal_pulse_lvl;
          values_i(0) <= test_analog;
          i           <= 0;
          ch_i := "00";
          id_i := 0;
          state_i     <= new_dac;

        when new_dac =>
          if i >= NDAC then
            state_i              <= finish;
          else
            -- ich   := addresses(i).dac_ch;
            ch_i := std_logic_vector(to_unsigned(addresses(i).dac_ch, 2));
            id_i := addresses(i).dac_id;
            dac_addr(1 downto 0) <= ch_i;
            dac_data             <= values_i(i);
            j                    <= 0;
            state_i              <= write_dac;
          end if;

        when write_dac =>
          if j >= WRITE_LENGTH then
            state_i              <= new_dac;
            i                    <= i + 1;
          else
            -- Channel address 
            dac_addr(1 downto 0) <= ch_i;
            -- Write the same values to all 5 branches.
            for k in 1 to NBRANCH loop
              dac_addr(id_i + 2 * k) <= '0';
            end loop;  -- k
            dac_data             <= values_i(i);
            j                    <= j+1;
          end if;

        when finish =>
          ch_i := "00";
          id_i := 0;
          start_i <= '0';
          i       <= 0;
          state_i <= idle;
          busy    <= '0';

        when others =>
          state_i <= idle;
      end case;
    end if;
  end process fsm;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package dac_interface_pack is
  component dac_interface
    generic (
      DELAYED : boolean := true);         -- Allow delayed execution
    port (
      shape_bias_0  : in  std_logic_vector(7 downto 0);  -- Bias (lower)
      shape_bias_1  : in  std_logic_vector(7 downto 0);  -- Bias (upper)
      vfp_0         : in  std_logic_vector(7 downto 0);  -- VFP low
      vfp_1         : in  std_logic_vector(7 downto 0);  -- VFP upper
      vfs_0         : in  std_logic_vector(7 downto 0);  -- VFS low  
      vfs_1         : in  std_logic_vector(7 downto 0);  -- VFS upper
      cal_pulse_lvl : in  std_logic_vector(7 downto 0);  -- Calibration
      test_analog   : in  std_logic_vector(7 downto 0);  -- Not used
      clk           : in  std_logic;    -- Clock
      rstb          : in  std_logic;    -- Reset (active low)
      start         : in  std_logic;    -- Command signal
      inhibit       : in  std_logic;    -- Inhibit changing DAC's 
      busy          : out std_logic;    -- Busy 
      dac_addr      : out std_logic_vector(11 downto 0);  -- DAC address
      dac_data      : out std_logic_vector(7 downto 0));  -- DAC value
  end component dac_interface;
end dac_interface_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
