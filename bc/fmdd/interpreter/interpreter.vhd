------------------------------------------------------------------------------
-- Title      : Command intepreter
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : interpreter.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2007/01/04
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: this module takes care of translating the bits of the command
--              register to actual signals.  The commands and their
--              corresonding bit are 
--
--              Commmand                       Bit
--              ---------------------------------------------------
--                Change DAC values             0
--                Start internal trigger box    1
--                Make a fake level 0 trigger   2
--                Do a soft reset               3
--                Start calibtation mode        4
--                End calibration mode          5

------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
entity interpreter is
  port (
    clk               : in  std_logic;   -- clock
    rstb              : in  std_logic;   -- Reset (-)
    command           : in  std_logic_vector(15 downto 0);
    cal_on            : out std_logic;   -- VA1 pulser mode
    test_on           : out std_logic;   -- VA1 test mode
    start_cal         : out std_logic;   -- Turn on calibration manager
    start_trigger_box : out std_logic;   -- Make a fake trigger sequence
    l0_only           : out std_logic;   -- Make only a fake L0 trigger
    change_dac        : out std_logic;   -- Signal new DAC values
    soft_reset        : out std_logic);  -- Reset state machines
end interpreter;

-------------------------------------------------------------------------------
architecture rtl of interpreter is
  type state_t is ( idle, exec, finish );
  signal state_i : state_t;
  signal cmd_i   : std_logic_vector(15 downto 0);
begin  -- rtl

  -- purpose: Decode the commands send
  -- type : sequential
  -- inputs : clk, rstb
  -- outputs: clear, new_shift_clock, new_altro_clock,
  -- new_strip_range, cal_on, new_dac_values, new_timeouts 
  p_command_dec : process (clk, rstb)
  begin  -- process p_command_dec
    if rstb = '0' then                  -- asynchronous reset (active low)
      cal_on            <= '0';
      test_on           <= '0';
      start_trigger_box <= '0';
      l0_only           <= '0';
      change_dac        <= '0';
      soft_reset        <= '0';
      start_cal         <= '0';
      state_i           <= idle;
      cmd_i             <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      start_trigger_box <= '0';
      l0_only           <= '0';
      change_dac        <= '0';
      soft_reset        <= '0';
      start_cal         <= '0';
      state_i           <= state_i;

      case state_i is
        when idle               =>
          if command /= X"0000" then
            state_i <= exec;
            cmd_i   <= command;         -- Register the command 
          end if;

        when exec           =>
          for i in 8 downto 0 loop
            if cmd_i(i) = '1' then
              case i is
                when 8      =>          -- `Start calibration'
                  start_cal         <= '1';
                when 7      =>          -- `VA1 test mode off' 
                  test_on           <= '0';
                when 6      =>          -- `VA1 test mode on, but no pulse'
                  test_on           <= '1';
                when 5      =>          -- `VA1 test mode off' 
                  cal_on            <= '0';
                  test_on           <= '0';
                when 4      =>          -- `VA1 test mode on'
                  cal_on            <= '1';
                  test_on           <= '1';
                when 3      =>          -- `Soft reset'
                  soft_reset        <= '1';
                  null;
                when 2      =>          -- `Fake L0 trigger only'
                  start_trigger_box <= '1';
                  l0_only           <= '1';
                when 1      =>          -- `Fake trigger sequence'
                  start_trigger_box <= '1';
                when 0      =>          -- `Do DACs'
                  change_dac        <= '1';
                when others => null;
              end case;
            end if;
          end loop;  -- i
          state_i                   <= finish;

        when finish          =>
          cmd_i   <= (others => '0');
          state_i <= idle;

        when others =>
          state_i <= idle;
      end case;
    end if;
  end process p_command_dec;
end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package interpreter_pack is
  component interpreter
    port (
      clk               : in  std_logic;   -- clock
      rstb              : in  std_logic;   -- Reset (-)
      command           : in  std_logic_vector(15 downto 0);
      cal_on            : out std_logic;   -- VA1 pulser mode
      test_on           : out std_logic;   -- VA1 test mode
      start_cal         : out std_logic;   -- Turn on calibration manager
      start_trigger_box : out std_logic;   -- Make a fake trigger sequence
      l0_only           : out std_logic;   -- Make only a fake L0 trigger
      change_dac        : out std_logic;   -- Signal new DAC values
      soft_reset        : out std_logic);  -- Reset state machines
  end component interpreter;
end interpreter_pack;

-------------------------------------------------------------------------------
--
-- EOF
--

