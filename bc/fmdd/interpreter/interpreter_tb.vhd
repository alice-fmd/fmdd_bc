------------------------------------------------------------------------------
-- Title      : Test bench of command interpreter
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : interpreter_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/18
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.interpreter_pack.all;
use work.fmdd_simul_pack.all;

------------------------------------------------------------------------------
entity interpreter_tb is
end entity interpreter_tb;

------------------------------------------------------------------------------
architecture test of interpreter_tb is

  signal clk_i               : std_logic := '0';  -- clock
  signal rstb_i              : std_logic := '0';  -- Reset (-)
  signal command_i           : std_logic_vector(15 downto 0);
  signal cal_on_i            : std_logic;  -- VA1 test mode
  signal test_on_i           : std_logic;  -- VA1 test mode
  signal start_trigger_box_i : std_logic;  -- Make a fake trigger sequence
  signal l0_only_i           : std_logic;  -- Make only a fake L0 trigger
  signal change_dac_i        : std_logic;  -- Signal new DAC values
  signal soft_reset_i        : std_logic;  -- Reset state machines

begin  -- architecture test
  dut : entity work.interpreter
    port map (
        clk               => clk_i,                -- in  clock
        rstb              => rstb_i,               -- in  Reset (-)
        command           => command_i,            -- in  Command register
        cal_on            => cal_on_i,             -- out VA1 pulser mode
        test_on           => test_on_i,            -- out VA1 test mode
        start_trigger_box => start_trigger_box_i,  -- out Make trigger seq.
        l0_only           => l0_only_i,            -- out Make L0 trigger
        change_dac        => change_dac_i,         -- out Change DACs
        soft_reset        => soft_reset_i);        -- out Reset


  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    command_i <= (others => '0');
    wait for 100 ns;
    rstb_i <= '1';

    for i in cmd_list'range loop
      wait for 100 ns;
      if cmd_list(i).code = X"0000" then
        exit;
      end if;
      exec_command(c       => cmd_list(i),
                   clk     => clk_i,
                   command => command_i);
    end loop;  -- i

    wait for 100 ns;
    command_i <= (others => '0');
    for i in 0 to 5 loop
      wait for 100 ns;
      wait until rising_edge(clk_i);
      command_i(i) <= '1';
      wait until rising_edge(clk_i);
      command_i <= (others => '0');
    end loop;  -- i
    wait;
  end process stimuli;

  -- purpose: Make a clock
  clk_gen: process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;
end architecture test;

------------------------------------------------------------------------------
