library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.cal_manager_pack.all;
use work.dac_interface_pack.all;
use work.va1_readout_pack.all;
use work.clock_gen_pack.all;
use work.trigger_handler_pack.all;
use work.fmdd_simul_pack.all;


entity cal_dac_readout_tb is
end entity cal_dac_readout_tb;

------------------------------------------------------------------------------
architecture test of cal_dac_readout_tb is
  -- Global signals 
  signal clk_i   : std_logic := '0';
  signal rstb_i  : std_logic := '0';

  -- Timing parameters
  signal hold_wait_i  : std_logic_vector(15 downto 0) := (others => '0');
  signal l1_timeout_i : std_logic_vector(15 downto 0) := (others => '0');
  signal l2_timeout_i : std_logic_vector(15 downto 0) := (others => '0');

  -- VA1 Clock parameters 
  signal va_phase_i : std_logic_vector(7 downto 0) := int2slv(3, 8);
  signal va_div_i   : std_logic_vector(7 downto 0) := int2slv(16, 8);

  -- Read-out parameters
  signal range_i : std_logic_vector(15 downto 0) := X"7F00";
  signal first_i : std_logic_vector(7 downto 0);
  signal last_i  : std_logic_vector(7 downto 0);

  -- Control signals 
  signal start_i           : std_logic := '0';
  signal l0_i              : std_logic := '0';
  signal l1b_i             : std_logic := '1';
  signal l2b_i             : std_logic := '1';
  signal cal_on_i          : std_logic := '0';

  -- Output from trigger handler
  signal hold_i            : std_logic;
  signal busy_i            : std_logic;
  signal pulser_enable_i   : std_logic;
  signal incomplete_i      : std_logic;

  -- VA1 and shift clock
  signal va_clk_i          : std_logic;
  signal shift_clk_i       : std_logic;

  -- ALTRO clock
  signal sclk_i            : std_logic := '0';

  -- Sequencer signals 
  signal sequencer_start_i : std_logic;
  signal sequencer_busy_i  : std_logic;
  signal sequencer_clear_i : std_logic;
  signal shift_in_i        : std_logic;
  signal digital_reset_i   : std_logic;
  signal altro_l1_i        : std_logic;

  -- Calibration manager signals 
  signal iterations_i     : std_logic_vector(15 downto 0) := int2slv(2, 16);
  signal step_i           : std_logic_vector(7 downto 0)  := int2slv(127, 8);
  signal cal_man_start_i  : std_logic                     := '0';
  signal cal_man_busy_i   : std_logic;
  signal cal_man_strip_i  : std_logic_vector(7 downto 0);  -- output strip no
  signal cal_man_pulse_i  : std_logic_vector(7 downto 0);  -- Pulse size
  signal cal_man_change_i : std_logic;

  -- DAC interface signals 
  signal shape_bias_0_i : std_logic_vector(7 downto 0) := X"AA";
  signal shape_bias_1_i : std_logic_vector(7 downto 0) := X"BB";
  signal vfp_0_i        : std_logic_vector(7 downto 0) := X"CC";
  signal vfp_1_i        : std_logic_vector(7 downto 0) := X"DD";
  signal vfs_0_i        : std_logic_vector(7 downto 0) := X"EE";
  signal vfs_1_i        : std_logic_vector(7 downto 0) := X"FF";
  signal cal_lvl_i      : std_logic_vector(7 downto 0) := X"99";
  signal test_analog_i  : std_logic_vector(7 downto 0) := X"88";
  signal dac_en_i       : std_logic := '0';  -- Command signal
  signal dac_inh_i      : std_logic := '0';  -- Inhibit changing DAC's 
  signal dac_busy_i     : std_logic;    -- Busy 
  signal dac_addr_i     : std_logic_vector(11 downto 0);  -- DAC address
  signal dac_data_i     : std_logic_vector(7 downto 0);  -- DAC value

  signal change_busy_i : std_logic;
  signal not_busy_i    : std_logic;
  
begin  -- architecture test
  not_busy_i <= not cal_man_busy_i;
  
  mux                  : block is
  begin  -- block mux
    first_i   <= cal_man_strip_i  when cal_man_busy_i = '1'
                 else range_i(7 downto 0);
    last_i    <= cal_man_strip_i  when cal_man_busy_i = '1'
                 else range_i(15 downto 8);
    cal_on_i  <= '1'              when cal_man_busy_i = '1'
                 else '0';
    dac_en_i  <= cal_man_change_i when cal_man_busy_i = '1'
                 else '0';
    cal_lvl_i <= cal_man_pulse_i  when cal_man_busy_i = '1'
                 else (others => '0');
  end block mux;

  -- Device under test
  dut : entity work.cal_manager
    port map (
      clk         => clk_i,                 -- in  Clock
      rstb        => rstb_i,                -- in  Reset
      enable      => cal_man_start_i,       -- in  Start pulser mode
      busy        => cal_man_busy_i,        -- out We're active 
      l2          => l2b_i,                 -- in  L2 trigger
      iterations  => iterations_i,          -- in  Number of iterations
      step        => step_i,                -- in  Number of divisions
      strip_min   => range_i(7 downto 0),   -- in  Min strip
      strip_max   => range_i(15 downto 8),  -- in  Max strip
      strip       => cal_man_strip_i,       -- out output strip no
      pulse       => cal_man_pulse_i,       -- out Pulse size
      change      => cal_man_change_i,      -- out 
      change_busy => dac_busy_i);           -- in

  -- Handle triggers 
  handler : entity work.trigger_handler
    port map (
        clk             => clk_i,              -- in  Clock
        rstb            => rstb_i,             -- in  Async reset
        l0              => l0_i,               -- in  Level 0 (-)
        l1              => l1b_i,              -- in  Level 1 (-)
        hold            => hold_i,             -- out Hold to VA1s
        busy            => busy_i,             -- out Busy to CTP
        hold_wait       => hold_wait_i,        -- in  wait hold
        l1_timeout      => l1_timeout_i,       -- in  L1 timeout
        l2_timeout      => l2_timeout_i,       -- in  L2 timeout 
        sequencer_start => sequencer_start_i,  -- out Start seq.
        sequencer_busy  => sequencer_busy_i,   -- in  Seq. busy
        sequencer_clear => sequencer_clear_i,  -- out Clear seq. 
        cal_on          => cal_on_i,           -- in  Calib. mode
        pulser_enable   => pulser_enable_i,    -- out Step cal.
        incomplete      => incomplete_i);      -- out Status

  -- Readout of VA1's 
  readout : entity work.va1_readout
    generic map (
        DRESET_LENGTH => 4,                  -- dreset length
        L1_LENGTH     => 8,                  -- L1 output length
        SIZE          => 127)
    port map (
        clk           => clk_i,              -- in  Clock
        rstb          => rstb_i,             -- in  Async reset
        clear         => sequencer_clear_i,  -- in  Clear
        va_clk        => va_clk_i,           -- in  VA input clock
        first         => first_i,            -- in  First strip
        last          => last_i,             -- in  Lasst strip
        start         => sequencer_start_i,  -- in  Start
        busy          => sequencer_busy_i,   -- out Busy
        l1            => altro_l1_i,         -- out L1 to ALTROs
        shift_clk     => shift_clk_i,        -- out Burst 
        shift_in      => shift_in_i,         -- out Reset shift-reg
        digital_reset => digital_reset_i);   -- out Reset VA1s

  -- DAC interface
  dac : entity work.dac_interface
    generic map (
      DELAYED         => true)
    port map (
        shape_bias_0  => shape_bias_0_i,  -- in  Bias (lower)
        shape_bias_1  => shape_bias_1_i,  -- in  Bias (upper)
        vfp_0         => vfp_0_i,         -- in  VFP low
        vfp_1         => vfp_1_i,         -- in  VFP upper
        vfs_0         => vfs_0_i,         -- in  VFS low  
        vfs_1         => vfs_1_i,         -- in  VFS upper
        cal_pulse_lvl => cal_lvl_i,       -- in  Calibration
        test_analog   => test_analog_i,   -- in  Not used
        clk           => clk_i,           -- in  Clock
        rstb          => rstb_i,          -- in  Reset (active low)
        start         => dac_en_i,        -- in  Command signal
        inhibit       => dac_inh_i,       -- in  Inhibit changing DAC's 
        busy          => dac_busy_i,      -- out Busy 
        dac_addr      => dac_addr_i,      -- out DAC address
        dac_data      => dac_data_i);     -- out DAC value

  -- Clock generator for the  VA1's 
  va_clk_gen : entity work.clock_gen
    generic map (
        WIDTH        => 8,              -- Width of divider
        IN_POL       => '1',            -- rising edge 
        CATCH        => "Y")            -- lots of logic
    port map (
        clk          => clk_i,          -- in  Master input clock
        rstb         => rstb_i,         -- in  Master Rstb
        clk_en_o     => open,           -- out Rising edge
        clk_o        => va_clk_i,       -- out Gen. clock
        out_polarity => '1',            -- in  Polarity
        div_phase    => va_phase_i,     -- in  div
        div_count    => va_div_i);      -- in off
  
  -- purpose: Stimuli
  stimuli : process
    variable counter_i : integer := 0;
  begin  -- process stimuli
    change_timeouts(hold_delay => 1.5 us,
                    l1_wait    => 6.5 us,
                    l2_wait    => 88 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);
    wait for 4 * PERIOD;
    rstb_i <= '1';

    -- Start cal-mode
    start_it(start => cal_man_start_i, clk => clk_i);
    wait until rising_edge(clk_i);

    counter_i := 1;
    while cal_man_busy_i = '1' loop
      wait for (counter_i * 100 us + L0_DELAY - NOW);
      report "Event # " & integer'image(counter_i) &
        " strip " & integer'image(slv2int(cal_man_strip_i)) &
        " pulser " & integer'image(slv2int(cal_man_pulse_i))
        severity note;
      make_triggers(clk       => clk_i,
                    l0        => l0_i,
                    l1        => l1b_i,
                    l2        => l2b_i);
      counter_i := counter_i + 1;
    end loop;

    wait;
  end process stimuli;

  -- Clock 
  clocker : process
  begin  -- process clocker
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clocker;

end architecture test;
