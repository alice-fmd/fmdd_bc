------------------------------------------------------------------------------
-- Title      : Test bench of trigger handler, readout sequencer, and clock
--              generator 
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : handle_readout_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/28
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/26  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.trigger_handler_pack.all;
use work.va1_readout_pack.all;
use work.clock_gen_pack.all;
use work.fmdd_simul_pack.all;

-------------------------------------------------------------------------------
entity handle_readout_tb is
end handle_readout_tb;

------------------------------------------------------------------------------
architecture test of handle_readout_tb is
  -- Timing parameters
  signal hold_wait_i  : std_logic_vector(15 downto 0) := (others => '0');
  signal l1_timeout_i : std_logic_vector(15 downto 0) := (others => '0');
  signal l2_timeout_i : std_logic_vector(15 downto 0) := (others => '0');

  -- VA1 Clock parameters 
  signal   va_phase_i : std_logic_vector(7 downto 0) := int2slv(3, 8);
  signal   va_div_i   : std_logic_vector(7 downto 0) := int2slv(16, 8);

  -- ALTRO Clock parameters 
  signal sclk_phase_i : std_logic_vector(7 downto 0) := int2slv(0, 8);
  signal sclk_div_i   : std_logic_vector(7 downto 0) := int2slv(4, 8);

  -- Read-out parameters
  signal first_i : std_logic_vector(7 downto 0) := int2slv(0, 8);
  signal last_i  : std_logic_vector(7 downto 0) := int2slv(127, 8);

  -- Control signals 
  signal clk_i             : std_logic := '0';
  signal rstb_i            : std_logic := '0';
  signal start_i           : std_logic := '0';
  signal l0_i              : std_logic := '0';
  signal l1b_i             : std_logic := '1';
  signal l2b_i             : std_logic := '1';
  signal cal_on_i          : std_logic := '0';
  -- Output from trigger handler
  signal hold_i            : std_logic;
  signal busy_i            : std_logic;
  signal pulser_enable_i   : std_logic;
  signal incomplete_i      : std_logic;
  -- VA1 and shift clock
  signal va_clk_i          : std_logic;
  signal shift_clk_i       : std_logic;
  -- ALTRO clock
  signal sclk_i            : std_logic := '0';
  -- Sequencer signals 
  signal sequencer_start_i : std_logic;
  signal sequencer_busy_i  : std_logic;
  signal sequencer_clear_i : std_logic;
  signal shift_in_i        : std_logic;
  signal digital_reset_i   : std_logic;
  signal altro_l1_i        : std_logic;

begin  -- test

  handler : entity work.trigger_handler
    port map (
        clk             => clk_i,              -- in  Clock
        rstb            => rstb_i,             -- in  Async reset
        l0              => l0_i,               -- in  Level 0 (-)
        l1              => l1b_i,              -- in  Level 1 (-)
        hold            => hold_i,             -- out Hold to VA1s
        busy            => busy_i,             -- out Busy to CTP
        hold_wait       => hold_wait_i,        -- in  wait hold
        l1_timeout      => l1_timeout_i,       -- in  L1 timeout
        l2_timeout      => l2_timeout_i,       -- in  L2 timeout 
        sequencer_start => sequencer_start_i,  -- out Start seq.
        sequencer_busy  => sequencer_busy_i,   -- in  Seq. busy
        sequencer_clear => sequencer_clear_i,  -- out Clear seq. 
        cal_on          => cal_on_i,           -- in  Calib. mode
        pulser_enable   => pulser_enable_i,    -- out Step cal.
        incomplete      => incomplete_i);      -- out Status

  readout : entity work.va1_readout
    generic map (
        DRESET_LENGTH => 4,                  -- dreset length
        L1_LENGTH     => 8,                  -- L1 output length
        SIZE          => 127)
    port map (
        clk           => clk_i,              -- in  Clock
        rstb          => rstb_i,             -- in  Async reset
        clear         => sequencer_clear_i,  -- in  Clear
        va_clk        => va_clk_i,           -- in  VA input clock
        first         => first_i,            -- in  First strip
        last          => last_i,             -- in  Lasst strip
        start         => sequencer_start_i,  -- in  Start
        busy          => sequencer_busy_i,   -- out Busy
        l1            => altro_l1_i,         -- out L1 to ALTROs
        shift_clk     => shift_clk_i,        -- out Burst 
        shift_in      => shift_in_i,         -- out Reset shift-reg
        digital_reset => digital_reset_i);   -- out Reset VA1s

  va_clk_gen : entity work.clock_gen
    generic map (
        WIDTH        => 8,              -- Width of divider
        IN_POL       => '1',            -- rising edge 
        CATCH        => "Y")            -- lots of logic
    port map (
        clk          => clk_i,          -- in  Master input clock
        rstb         => rstb_i,         -- in  Master Rstb
        clk_en_o     => open,           -- out Rising edge
        clk_o        => va_clk_i,       -- out Gen. clock
        out_polarity => '1',            -- in  Polarity
        div_phase    => va_phase_i,     -- in  div
        div_count    => va_div_i);      -- in off

  sclk_gen : entity work.clock_gen
    generic map (
        WIDTH        => 8,              -- Width of divider
        IN_POL       => '1',            -- rising edge 
        CATCH        => "Y")            -- lots of logic
    port map (
        clk          => clk_i,          -- in  Master input clock
        rstb         => rstb_i,         -- in  Master Rstb
        clk_en_o     => open,           -- out Rising edge
        clk_o        => sclk_i,         -- out Gen. clock
        out_polarity => '1',            -- in  Polarity
        div_phase    => sclk_phase_i,   -- in  div
        div_count    => sclk_div_i);    -- in off

  -- purpose: Stimuli
  stimuli : process
  begin  -- process stimuli
    change_timeouts(hold_delay => 1.5 us,
                    l1_wait    => 6.5 us,
                    l2_wait    => 88 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);
    wait for 4 * PERIOD;
    rstb_i <= '1';


    -- Internal triggers
    wait for (L0_DELAY - NOW);
    wait until rising_edge(va_clk_i);
    wait for 10.0 ns;
    make_triggers(clk       => clk_i,
                  l0        => l0_i,
                  l1        => l1b_i,
                  l2        => l2b_i);

    -- Internal triggers
    wait for (100 us + L0_DELAY - NOW);
    wait until rising_edge(va_clk_i);
    wait for 67.0 ns;
    make_triggers(clk       => clk_i,
                  l0        => l0_i,
                  l1        => l1b_i,
                  l2        => l2b_i);

    wait for 200 us - NOW;
    wait until rising_edge(va_clk_i);
    wait for PERIOD;
    make_triggers(clk       => clk_i,
                  l0        => l0_i,
                  l1        => l1b_i,
                  l2        => l2b_i);

    -- Check cal pulse problem
    wait for 395 us - NOW;
    cal_on_i <= '1';
    change_range(s1            => 0,
                 s2            => 10,
                 clk           => clk_i,
                 first         => first_i,
                 last          => last_i);
    change_timeouts(hold_delay => 2 us,
                    l1_wait    => 6.5 us,
                    l2_wait    => 88 us,
                    clk        => clk_i,
                    hold_wait  => hold_wait_i,
                    l1_timeout => l1_timeout_i,
                    l2_timeout => l2_timeout_i);
    report "Cal mode, hold = " & slv2str(hold_wait_i) severity note;
    wait for 400 us + L0_DELAY - NOW + 12 ns;
    make_l0(len                => 6 us,
            l0                 => l0_i);
    wait for 6.5 us - L0_DELAY;
    make_l1(len                => 200 ns,
            clk                => clk_i,
            l1                 => l1b_i);
    wait for 88 us - L0_DELAY;
    make_l2(len                => 200 ns,
            clk                => clk_i,
            l2                 => l2b_i);

    wait;
  end process stimuli;

  -- Clock 
  clocker : process
  begin  -- process clocker
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clocker;
end test;
------------------------------------------------------------------------------
-- 
-- EOF
--
