------------------------------------------------------------------------------
-- Title      : Decode address from I2C bus
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : fec_address.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-03-02
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/20  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fec_address is
  port (
    clk         : in  std_logic;                     -- Clock
    rstb        : in  std_logic;                     -- Async reset
    scl         : in  std_logic;                     -- Serial clock
    sda_in      : in  std_logic;                     -- Serial data in
    cnt_8       : in  std_logic;                     -- Got 1 byte
    data_par    : in  std_logic_vector(7 downto 0);  -- 1 byte input
    hadd        : in  std_logic_vector(4 downto 0);  -- Board address
    finish_tx   : in  std_logic;                     -- Transfer done
    finish_rx   : in  std_logic;                     -- Recieve done
    enable      : out std_logic;                     -- Shift one bit
    clear       : out std_logic;                     -- Clear shift register
    en_fec_tx   : out std_logic;                     -- Enable transfer
    en_fec_rx   : out std_logic;                     -- Enable recieve
    sda_out     : out std_logic;                     -- Serial data out
    sel_fec_add : out std_logic;                     -- Component activ
    bcast       : out std_logic;                     -- Broadcast flag
    slctr       : out std_logic;                     -- I2C control
    for_us      : out std_logic;                     -- We are addressed 
    state       : out std_logic_vector(3 downto 0)); 
end entity fec_address;

------------------------------------------------------------------------------
architecture rtl_minimal of fec_address is
  type state_t is (idle,
                   wait_start,
                   start,
                   wait_add,
                   latch_add,
                   fec_add,
                   ack_1,
                   s_rx,
                   s_tx);               -- States
  constant ST_idle       : std_logic_vector(3 downto 0) := "0000";
  constant ST_wait_start : std_logic_vector(3 downto 0) := "0001";
  constant ST_start      : std_logic_vector(3 downto 0) := "0010";
  constant ST_wait_add   : std_logic_vector(3 downto 0) := "0011";
  constant ST_latch_add  : std_logic_vector(3 downto 0) := "0100";
  constant ST_fec_add    : std_logic_vector(3 downto 0) := "0101";
  constant ST_ack_1      : std_logic_vector(3 downto 0) := "0110";
  constant ST_s_rx       : std_logic_vector(3 downto 0) := "0111";
  constant ST_s_tx       : std_logic_vector(3 downto 0) := "1000";

  signal st_i          : state_t;       -- State
  signal addr_i        : std_logic_vector(4 downto 0);
  -- signal en_fec_i : std_logic := '0';
  signal rw_i          : std_logic;
  signal bcast_i       : std_logic;
  signal sda_out_i     : std_logic;
  signal sel_fec_add_i : std_logic;
  -- signal for_us_i : std_logic;
  
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    bcast <= bcast_i;
    -- for_us <= for_us_i;
    -- state <= std_logic_vector(to_unsigned(state_t'pos(st_i),4));
    -- en_fec_i <= '1' when (addr_i = hadd) or bcast_i = '1' else '0';
  end block combi;

  -- purpose: Decode  the address sed over I2C
  -- type   : sequential
  -- inputs : clk, rstb, scl, sda_in, hadd, finish_tx, finish_rx,
  --          cnt_8, en_fec_i
  -- outputs: en_fec_rx, en_fec_tx, sel_fec_add, sda_out, clear, enable
  fsm_add : process (clk, rstb) is
  begin  -- process fsm_add
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i        <= idle;
      bcast_i     <= '0';
      addr_i      <= (others => '0');
      rw_i        <= '0';
      clear       <= '0';
      enable      <= '0';
      en_fec_rx   <= '0';
      en_fec_tx   <= '0';
      sda_out     <= '1';               -- sda_out_i;
      sel_fec_add <= '0';
      slctr       <= '0';
      for_us      <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- Default values
      -- bcast_i     <= bcast_i;           -- Not broadcast
      -- addr_i      <= addr_i;            -- Address is null right now
      -- rw_i        <= rw_i;              -- Not read
      clear       <= '0';               -- No clear 
      enable      <= '0';               -- No shift reg. enable
      en_fec_rx   <= '0';               -- Not enable Reciever
      en_fec_tx   <= '0';               -- Not enable Transciever
      sda_out     <= '1';               -- Drive serial line high
      sel_fec_add <= '0';               -- This module selected 
      slctr       <= '0';               -- Do not ask for control
      for_us      <= '0';

      case st_i is
        when idle =>
          state   <= ST_idle;
          bcast_i <= '0';               -- Not broadcast
          addr_i  <= (others => '0');   -- Address is null right now
          rw_i    <= '0';               -- Not read
          if scl = '1' and sda_in = '1' then
            st_i <= wait_start;         -- Saw clock high and data high
          else
            st_i <= idle;
          end if;

        when wait_start =>
          state <= ST_wait_start;
          if sda_in = '0' and scl = '1' then  -- wait for start condition
            st_i <= start;              -- Saw start of start-condition
          elsif scl = '0' then          -- If clock goes low, then start
            st_i <= idle;               -- condition timed out. 
          elsif sda_in = '1' then       -- If data is still high, then wait
            st_i <= wait_start;         -- for it to go low
          end if;

        when start =>
          state       <= ST_start;
          sel_fec_add <= '1';                 -- Module selected
          if sda_in = '0' and scl = '0' then  -- Data is still low, and clock
            st_i <= wait_add;                 -- went down, got start cond.
          else
            st_i <= start;
          end if;

        when wait_add =>
          state       <= ST_wait_add;
          sel_fec_add <= '1';           -- Module selected
          if scl = '1' then
            enable <= '1';              -- 2.32 - take up one early
            st_i   <= latch_add;        -- Got rising edge, store bit
          else
            -- enable <= '0';
            st_i <= wait_add;
          end if;

        when latch_add =>
          state       <= ST_latch_add;
          enable      <= '1';
          -- Take down acknowledge 1 clock-cycle before
          -- sda_out     <= '1';
          -- slctr       <= '0';
          sel_fec_add <= '1';
          if scl = '0' and cnt_8 = '1' then     -- Falling edge, 1 byte
            clear   <= '1';                     -- Clear shift register
            bcast_i <= data_par(6);             -- Check for broadcast flag
            addr_i  <= data_par(5 downto 1);    -- Get address 
            rw_i    <= data_par(0);             -- Check read/write flat
            st_i    <= fec_add;                 -- Next, check the address
            sda_out <= '0';                     -- Drive low - ACK
            -- Compare address as soon as possible
            if (data_par(5 downto 1) /= HADD) and (bcast_i = '0') then
              for_us <= '0';
            else
              for_us <= '1';
              slctr  <= '1';                    -- Selected 
            end if;
          elsif scl = '0' and cnt_8 = '0' then  -- Falling edge, missing bits
            st_i <= wait_add;
          else                                  -- wait for falling edge
            st_i <= latch_add;
          end if;

        when fec_add =>
          state       <= ST_fec_add;
          clear       <= '1';                           -- Clear shift register
          sel_fec_add <= '1';                           -- Select this module
          -- slctr <= '0';                 -- original line
          -- We used to go to idle if this instruction was not for us.
          -- However, looking in the PHOS BC code, I see that Johan Alme
          -- decodes the instruction no matter what, to protect against false
          -- start conditions.  Sense we seem to be stuck in a start condition
          -- from time to time, that stragegy seems like a good idea.  We only
          -- need to gate the slave_rx:we (write enable) to do this. 
          if (addr_i /= hadd) and (bcast_i = '0') then  -- Do not acknowledge
            sda_out <= '1';                             -- Drive high - No ACK
            st_i    <= idle;
            for_us  <= '0';
          else                                          -- Wait for rising edge
            slctr   <= '1';                             -- Take control
            for_us  <= '1';
            sda_out <= '0';                             -- Drive low - ACK
            -- if scl = '0' then                           -- Wait for rising edge
            st_i    <= ack_1;                           -- Acknowledge
            -- else
            -- st_i <= fec_add;
            -- end if;
          end if;

        when ack_1 =>
          state       <= ST_ack_1;
          sel_fec_add <= '1';                  -- We are selected
          -- slctr       <= '0'; -- original line
          slctr       <= '1';                  -- Take control
          for_us      <= '1';
          sda_out     <= '0';                  -- Drive low - ACK
          -- if scl = '0' and rw_i = '0' then     -- Write data
          if scl = '1' and rw_i = '0' then     -- Write data - rising edge
            en_fec_rx <= '1';                  -- Enable reciever
            st_i      <= s_rx;                 -- Wait for reciever
            -- elsif scl = '0' and rw_i = '1' then  -- Read data 
          elsif scl = '1' and rw_i = '1' then  -- Read data - rising edge
            en_fec_tx <= '1';                  -- Enable transmitter
            st_i      <= s_tx;                 -- Wait for transmitter
          end if;

        when s_rx =>
          state   <= ST_s_rx;
          slctr   <= '1';               -- Take control
          sda_out <= '0';               -- Drive low - ACK
          for_us  <= '1';
          if finish_rx = '1' then       -- Reciever done
            st_i <= idle;               -- Go back
          else
            st_i <= s_rx;
          end if;

        when s_tx =>
          state   <= ST_s_tx;
          slctr   <= '1';               -- Take control
          sda_out <= '0';               -- Drive low - ACK
          for_us  <= '1';
          if finish_tx = '1' then       -- Transmitter done
            st_i <= idle;               -- Go back
          else
            st_i <= s_tx;
          end if;

        when others =>
          state <= ST_idle;
          st_i  <= idle;
          
      end case;
    end if;
  end process fsm_add;
end architecture rtl_minimal;

------------------------------------------------------------------------------
-- This architecture will analyse the full I2C communication, but will only
-- enable output when the card is actually addressed. 
architecture rtl_full of fec_address is
  type state_t is (idle,
                   wait_start,
                   start,
                   wait_add,
                   latch_add,
                   fec_add,
                   ack_1,
                   s_rx,
                   s_tx);               -- States
  constant ST_idle       : std_logic_vector(3 downto 0) := "0000";
  constant ST_wait_start : std_logic_vector(3 downto 0) := "0001";
  constant ST_start      : std_logic_vector(3 downto 0) := "0010";
  constant ST_wait_add   : std_logic_vector(3 downto 0) := "0011";
  constant ST_latch_add  : std_logic_vector(3 downto 0) := "0100";
  constant ST_fec_add    : std_logic_vector(3 downto 0) := "0101";
  constant ST_ack_1      : std_logic_vector(3 downto 0) := "0110";
  constant ST_s_rx       : std_logic_vector(3 downto 0) := "0111";
  constant ST_s_tx       : std_logic_vector(3 downto 0) := "1000";

  signal st_i     : state_t;            -- State
  signal addr_i   : std_logic_vector(4 downto 0);
  -- signal en_fec_i : std_logic := '0';
  signal rw_i     : std_logic;
  signal bcast_i  : std_logic;
  signal for_us_i : std_logic;
  
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    bcast  <= bcast_i;
    for_us <= for_us_i;
    -- state <= std_logic_vector(to_unsigned(state_t'pos(st_i),4));
    -- en_fec_i <= '1' when (addr_i = hadd) or bcast_i = '1' else '0';
  end block combi;

  -- purpose: Decode  the address sed over I2C
  -- type   : sequential
  -- inputs : clk, rstb, scl, sda_in, hadd, finish_tx, finish_rx,
  --          cnt_8, en_fec_i
  -- outputs: en_fec_rx, en_fec_tx, sel_fec_add, sda_out, clear, enable
  fsm_add : process (clk, rstb) is
  begin  -- process fsm_add
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i        <= idle;
      bcast_i     <= '0';
      addr_i      <= (others => '0');
      rw_i        <= '0';
      clear       <= '0';
      enable      <= '0';
      en_fec_rx   <= '0';
      en_fec_tx   <= '0';
      sda_out     <= '1';
      sel_fec_add <= '0';
      slctr       <= '0';
      for_us_i    <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge

      case st_i is
        when idle =>
          state       <= ST_idle;
          bcast_i     <= '0';              -- Not broadcast
          addr_i      <= (others => '0');  -- Address is null right now
          rw_i        <= '0';              -- Not read
          clear       <= '0';              -- No clear 
          enable      <= '0';              -- No shift reg. enable
          en_fec_rx   <= '0';              -- Not enable Reciever
          en_fec_tx   <= '0';              -- Not enable Transciever
          sda_out     <= '1';              -- Drive serial line high
          sel_fec_add <= '0';              -- This module not selected 
          slctr       <= '0';              -- Do not ask for control
          for_us_i    <= '1';
          if scl = '1' and sda_in = '1' then
            st_i <= wait_start;            -- Saw clock high and data high
          else
            st_i <= idle;                  -- Stay in state
          end if;

        when wait_start =>
          state       <= ST_wait_start;
          bcast_i     <= '0';
          addr_i      <= (others => '0');
          rw_i        <= '0';
          clear       <= '0';
          enable      <= '0';
          en_fec_rx   <= '0';
          en_fec_tx   <= '0';
          sda_out     <= '1';
          sel_fec_add <= '0';
          slctr       <= '0';
          for_us_i    <= '1';
          if sda_in = '0' and scl = '1' then  -- wait for start condition
            st_i <= start;              -- Saw start of start-condition
          elsif scl = '0' then          -- If clock goes low, then start
            st_i <= idle;               -- condition timed out. 
          elsif sda_in = '1' then       -- If data is still high, then wait
            st_i <= wait_start;         -- for it to go low
          else
            st_i <= wait_start;         -- Keep on waiting
          end if;

        when start =>
          state       <= ST_start;
          bcast_i     <= '0';
          addr_i      <= (others => '0');
          rw_i        <= '0';
          clear       <= '0';
          enable      <= '0';
          en_fec_rx   <= '0';
          en_fec_tx   <= '0';
          sda_out     <= '1';
          sel_fec_add <= '1';           -- Module selected
          slctr       <= '0';
          for_us_i    <= '1';
          if sda_in = '0' and scl = '0' then  -- Data is still low, and clock
            st_i <= wait_add;           -- went down, got start cond.
          else
            st_i <= start;              -- Keep on waiting for start condition
          end if;

        when wait_add =>
          state       <= ST_wait_add;
          bcast_i     <= '0';
          addr_i      <= (others => '0');
          rw_i        <= '0';
          clear       <= '0';
          enable      <= '0';
          en_fec_rx   <= '0';
          en_fec_tx   <= '0';
          sda_out     <= '1';
          sel_fec_add <= '1';           -- Module selected
          slctr       <= '0';
          for_us_i    <= '1';
          if scl = '1' then
            st_i <= latch_add;          -- Got rising edge, store bit
          else
            st_i <= wait_add;           -- Wait for rising edge
          end if;

        when latch_add =>
          state       <= ST_latch_add;
          enable      <= '1';
          en_fec_rx   <= '0';
          en_fec_tx   <= '0';
          -- Take down acknowledge 1 clock-cycle before
          -- sda_out     <= '1';
          -- slctr       <= '0';
          sel_fec_add <= '1';
          for_us_i    <= '1';
          if scl = '0' and cnt_8 = '1' then     -- Falling edge, 1 byte
            clear   <= '1';                     -- Clear shift register
            bcast_i <= data_par(6);             -- Check for broadcast flag
            addr_i  <= data_par(5 downto 1);    -- Get address 
            rw_i    <= data_par(0);             -- Check read/write flat
            st_i    <= fec_add;                 -- Next, check the address
            sda_out <= '0';                     -- Drive low
            -- Compare address as soon as possible
            if (data_par(5 downto 1) /= HADD) and (bcast_i = '0') then
              slctr    <= '0';                  -- Not selected
              for_us_i <= '0';
            else
              slctr    <= '1';                  -- Selected 
              for_us_i <= '1';
            end if;
          elsif scl = '0' and cnt_8 = '0' then  -- Falling edge, missing bits
            clear    <= '0';
            bcast_i  <= '0';
            addr_i   <= (others => '0');
            rw_i     <= '0';
            st_i     <= wait_add;
            slctr    <= '0';
            sda_out  <= '1';
            for_us_i <= '1';
          else                                  -- wait for falling edge
            clear    <= '0';
            bcast_i  <= '0';
            addr_i   <= (others => '0');
            rw_i     <= '0';
            st_i     <= latch_add;
            slctr    <= '0';
            sda_out  <= '1';
            for_us_i <= '0';
          end if;

        when fec_add =>
          state       <= ST_fec_add;
          bcast_i     <= bcast_i;
          addr_i      <= addr_i;
          rw_i        <= rw_i;
          clear       <= '1';           -- Clear shift register
          enable      <= '0';           -- 
          en_fec_rx   <= '0';
          en_fec_tx   <= '0';
          sel_fec_add <= '1';           -- Select this module
          sda_out     <= '0';           -- Acknowledge
          -- slctr <= '0';                 -- original line
          -- We used to go to idle if this instruction was not for us.
          -- However, looking in the PHOS BC code, I see that Johan Alme
          -- decodes the instruction no matter what, to protect against false
          -- start conditions.  Sense we seem to be stuck in a start condition
          -- from time to time, that stragegy seems like a good idea.  We only
          -- need to gate the slave_rx:we (write enable) to do this. 
          if (addr_i /= hadd) and (bcast_i = '0') then  -- Do not acknowledge
            slctr    <= '0';            -- Not for us, give up control
            for_us_i <= '0';
          else                          -- Wait for rising edge
            slctr    <= '1';            -- Take control
            for_us_i <= '1';
          end if;
          if scl = '0' then             -- Wait for rising edge
            st_i <= ack_1;              -- Acknowledge
          else
            st_i <= fec_add;            -- Stay in this state
          end if;

        when ack_1 =>
          state       <= ST_ack_1;
          bcast_i     <= bcast_i;
          addr_i      <= addr_i;
          rw_i        <= rw_i;
          clear       <= '0';
          enable      <= '0';
          sel_fec_add <= '1';                  -- We are selected
          sda_out     <= '0';
          -- slctr       <= '0'; -- original line
          slctr       <= for_us_i;             -- Take control
          if scl = '0' and rw_i = '0' then     -- Write data
            en_fec_rx <= '1';                  -- Enable reciever
            en_fec_tx <= '0';                  -- Disable transmitter
            st_i      <= s_rx;                 -- Wait for reciever
          elsif scl = '0' and rw_i = '1' then  -- Read data 
            en_fec_rx <= '0';                  -- Disable reciever
            en_fec_tx <= '1';                  -- Enable transmitter
            st_i      <= s_tx;                 -- Wait for transmitter
          else                                 -- Wait for falling edge 
            en_fec_rx <= '0';
            en_fec_tx <= '0';
            st_i      <= ack_1;
          end if;

        when s_rx =>
          state       <= ST_s_rx;
          bcast_i     <= bcast_i;
          addr_i      <= addr_i;
          rw_i        <= rw_i;
          clear       <= '0';
          enable      <= '0';
          en_fec_rx   <= '0';
          en_fec_tx   <= '0';
          sda_out     <= '1';           -- Drive high
          sel_fec_add <= '0';           -- This module not selected
          slctr       <= for_us_i;      -- Take control
          if finish_rx = '1' then       -- Reciever done
            st_i <= idle;               -- Go back
          else
            st_i <= s_rx;               -- Wait until reciever is done
          end if;

        when s_tx =>
          state       <= ST_s_tx;
          bcast_i     <= bcast_i;
          addr_i      <= addr_i;
          rw_i        <= rw_i;
          clear       <= '0';
          enable      <= '0';
          en_fec_rx   <= '0';
          en_fec_tx   <= '0';
          sda_out     <= '1';           -- Drive high
          sel_fec_add <= '0';           -- This module not selected
          slctr       <= for_us_i;      -- Take control
          if finish_tx = '1' then       -- Transmitter done
            st_i <= idle;               -- Go back
          else
            st_i <= s_tx;               -- Wait until transmiter is done
          end if;

        when others =>
          state       <= ST_idle;
          bcast_i     <= '0';
          addr_i      <= (others => '0');
          rw_i        <= '0';
          clear       <= '0';
          enable      <= '0';
          en_fec_rx   <= '0';
          en_fec_tx   <= '0';
          sda_out     <= '1';
          sel_fec_add <= '1';
          slctr       <= '0';
          for_us_i    <= '0';

      end case;
    end if;
  end process fsm_add;
end architecture rtl_full;

------------------------------------------------------------------------------
--
-- This architecture is a direct translation of the old Verilog code by Carmen
-- to VHDL.  It uses the deprecated Moore-style FSM code
--
architecture rtl_direct of fec_address is
  type state_t is (idle,
                   wait_start,
                   start,
                   wait_add,
                   latch_add,
                   fec_add,
                   ack_1,
                   s_rx,
                   s_tx);               -- States
  constant ST_idle       : std_logic_vector(3 downto 0) := "0000";
  constant ST_wait_start : std_logic_vector(3 downto 0) := "0001";
  constant ST_start      : std_logic_vector(3 downto 0) := "0010";
  constant ST_wait_add   : std_logic_vector(3 downto 0) := "0011";
  constant ST_latch_add  : std_logic_vector(3 downto 0) := "0100";
  constant ST_fec_add    : std_logic_vector(3 downto 0) := "0101";
  constant ST_ack_1      : std_logic_vector(3 downto 0) := "0110";
  constant ST_s_rx       : std_logic_vector(3 downto 0) := "0111";
  constant ST_s_tx       : std_logic_vector(3 downto 0) := "1000";

  signal st_i     : state_t   := idle;             -- State
  signal nx_st_i  : state_t;                       -- Next state
  signal addr_i   : std_logic_vector(4 downto 0);  -- Address
  signal rw_i     : std_logic := '0';
  signal ready_i  : std_logic := '0';
  signal en_fec_i : std_logic := '0';
  signal bcast_i  : std_logic := '0';
begin  -- architecture rtl2
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    bcast <= bcast_i;
    -- state <= std_logic_vector(to_unsigned(state_t'pos(st_i), 4));
    -- en_fec_i <= '1' when  else '0';
  end block combi;

  -- purpose: Update the state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st_i
  -- outputs: st_i
  update_state : process (clk, rstb) is
  begin  -- process update_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process update_state;

  -- purpose: Decode the address send via I2C
  -- type   : combinational
  -- inputs : st_i, sda_in, scl, cnt_8, en_fec_i, rw_i, finish_tx, finish_rx
  -- outputs: clear, enable, en_fec_rx, en_fec_tx, sel_fec_add,
  --          sda_out, slctr, nx_st_i
  addr_fsm : process (st_i, sda_in, scl, cnt_8, en_fec_i, rw_i,
                      finish_tx, finish_rx) is
  begin  -- process addr_fsm

    case st_i is
      when idle =>
        state       <= ST_idle;
        clear       <= '0';
        enable      <= '0';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sda_out     <= '1';
        sel_fec_add <= '0';
        slctr       <= '0';
        ready_i     <= '0';
        if scl = '1' and sda_in = '1' then
          nx_st_i <= wait_start;
        else
          nx_st_i <= idle;
        end if;

      when wait_start =>
        state       <= ST_wait_start;
        clear       <= '0';
        enable      <= '0';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sda_out     <= '1';
        sel_fec_add <= '0';
        slctr       <= '0';
        ready_i     <= '0';
        if scl = '0' then
          nx_st_i <= idle;              -- Fluke
        elsif sda_in = '1' then
          nx_st_i <= wait_start;        -- Wait for falling edge on data
        elsif sda_in = '0' and scl = '1' then
          nx_st_i <= start;             -- Start condition
        end if;

      when start =>
        state       <= ST_start;
        clear       <= '0';
        enable      <= '0';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sda_out     <= '1';
        sel_fec_add <= '1';
        slctr       <= '0';
        ready_i     <= '0';
        if sda_in = '0' and scl = '0' then
          nx_st_i <= wait_add;          -- Falling edge
        else
          nx_st_i <= start;             -- Wait for falling edge
        end if;

      when wait_add =>
        state       <= ST_wait_add;
        clear       <= '0';
        enable      <= '0';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sda_out     <= '1';
        sel_fec_add <= '1';
        slctr       <= '0';
        ready_i     <= '0';
        if scl = '1' then
          nx_st_i <= latch_add;         -- Shift in one bit of address
        else
          nx_st_i <= wait_add;          -- Wait rising edge
        end if;

      when latch_add =>
        state       <= ST_latch_add;
        enable      <= '1';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sda_out     <= '1';
        sel_fec_add <= '1';
        slctr       <= '0';
        if scl = '0' and cnt_8 = '1' then     -- Falling edge, 1 byte
          clear   <= '1';
          ready_i <= '1';
          nx_st_i <= fec_add;
        elsif scl = '0' and cnt_8 = '0' then  -- Falling edge, missing bits
          clear   <= '0';
          ready_i <= '0';
          nx_st_i <= wait_add;
        else
          clear   <= '0';
          ready_i <= '0';
          nx_st_i <= latch_add;               -- Wait falling edge
        end if;

      when fec_add =>
        state       <= ST_fec_add;
        clear       <= '1';
        enable      <= '0';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sel_fec_add <= '1';
        -- slctr       <= '0';
        ready_i     <= '0';
        if (addr_i /= hadd) and bcast_i = '0' then
          slctr   <= '0';
          sda_out <= '1';               -- No acknowledge
          nx_st_i <= idle;              -- Not us
        elsif scl = '0' then
          slctr   <= '1';
          sda_out <= '0';               -- Acknowledge
          nx_st_i <= fec_add;           -- Wait for rising edge
        else
          slctr   <= '1';
          sda_out <= '0';               -- Acknowledge
          nx_st_i <= ack_1;             -- Acknowledge it
        end if;

      when ack_1 =>
        state       <= ST_ack_1;
        clear       <= '0';
        enable      <= '0';
        sda_out     <= '0';             -- Acknowledge
        sel_fec_add <= '1';
        slctr       <= '1';
        ready_i     <= '0';
        if scl = '0' and rw_i = '0' then
          en_fec_rx <= '1';
          en_fec_tx <= '0';
          nx_st_i   <= s_rx;            -- Start reciever
        elsif scl = '0' and rw_i = '1' then
          en_fec_rx <= '0';
          en_fec_tx <= '1';
          nx_st_i   <= s_tx;            -- Start transmitter
        else
          en_fec_rx <= '0';
          en_fec_tx <= '0';
          nx_st_i   <= ack_1;           -- Wait for falling edge
        end if;

      when s_rx =>
        state       <= ST_s_rx;
        clear       <= '0';
        enable      <= '0';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sda_out     <= '1';
        sel_fec_add <= '0';
        slctr       <= '1';
        ready_i     <= '0';
        if finish_rx = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= s_rx;
        end if;

      when s_tx =>
        state       <= ST_s_tx;
        clear       <= '0';
        enable      <= '0';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sda_out     <= '1';
        sel_fec_add <= '0';
        slctr       <= '1';
        ready_i     <= '0';
        if finish_tx = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= s_tx;
        end if;

      when others =>
        state       <= ST_idle;
        clear       <= '0';
        enable      <= '0';
        en_fec_rx   <= '0';
        en_fec_tx   <= '0';
        sda_out     <= '1';
        sel_fec_add <= '1';
        slctr       <= '0';
        ready_i     <= '0';
        nx_st_i     <= idle;

    end case;
  end process addr_fsm;

  -- purpose: Get the address from the I2C
  -- type   : sequential
  -- inputs : clk, rstb, ready_i
  -- outputs: bcast, rw_i, addr_i
  get_addr : process (clk, rstb) is
  begin  -- process get_addr
    if rstb = '0' then                  -- asynchronous reset (active low)
      bcast_i <= '0';
      addr_i  <= (others => '0');
      rw_i    <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if ready_i = '1' then
        bcast_i <= data_par(6);
        addr_i  <= data_par(5 downto 1);
        rw_i    <= data_par(0);
      end if;
    end if;
  end process get_addr;
end architecture rtl_direct;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package fec_address_pack is
  component fec_address
    port (
      clk         : in  std_logic;                     -- Clock
      rstb        : in  std_logic;                     -- Async reset
      scl         : in  std_logic;                     -- Serial clock
      sda_in      : in  std_logic;                     -- Serial data in
      cnt_8       : in  std_logic;                     -- Got 1 byte
      data_par    : in  std_logic_vector(7 downto 0);  -- 1 byte input
      hadd        : in  std_logic_vector(4 downto 0);  -- Hardware address
      finish_tx   : in  std_logic;                     -- Transfer done
      finish_rx   : in  std_logic;                     -- Recieve done
      enable      : out std_logic;                     -- Shift one bit
      clear       : out std_logic;                     -- Clear shift register
      en_fec_tx   : out std_logic;                     -- Enable transfer
      en_fec_rx   : out std_logic;                     -- Enable recieve
      sda_out     : out std_logic;                     -- Serial data out
      sel_fec_add : out std_logic;                     -- Component activ
      bcast       : out std_logic;                     -- Broadcast flag
      slctr       : out std_logic;                     -- I2C control
      for_us      : out std_logic;                     -- We are addressed
      state       : out std_logic_vector(3 downto 0));
  end component fec_address;
end package fec_address_pack;
------------------------------------------------------------------------------
--
-- EOF
--
