------------------------------------------------------------------------------
-- Title      : Tst of fec_address
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : fec_address_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/29
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/20  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.fec_address_pack.all;
use work.serializer_bc_pack.all;

------------------------------------------------------------------------------
entity fec_address_tb is
end entity fec_address_tb;

------------------------------------------------------------------------------

architecture test of fec_address_tb is
  constant PERIOD  : time                         := 25 ns;
   -- Must be at least 200 ns = 5MHz
  constant SPERIOD : time                         := 8 * PERIOD; 
  constant HADD    : std_logic_vector(4 downto 0) := "00000";

  signal clk_i         : std_logic := '0';  -- Clock
  signal rstb_i        : std_logic := '0';  -- Async reset
  signal scl_i         : std_logic := '0';  -- Serial clock
  signal sda_in_i      : std_logic := '0';  -- Serial data in
  signal cnt_8_i       : std_logic;     -- Got 1 byte
  signal data_par_i    : std_logic_vector(7 downto 0);  -- 1 byte input
  signal finish_tx_i   : std_logic := '0';  -- Transfer done
  signal finish_rx_i   : std_logic := '0';  -- Recieve done
  signal enable_i      : std_logic;     -- Shift one bit
  signal enable_ii     : std_logic;     -- Shift one bit
  signal clear_i       : std_logic;     -- Clear shift register
  signal clear_ii      : std_logic;     -- Clear shift register
  signal en_fec_tx_i   : std_logic;     -- Enable transfer
  signal en_fec_rx_i   : std_logic;     -- Enable recieve
  signal sda_out_i     : std_logic;     -- Serial data out
  signal sel_fec_add_i : std_logic;     -- Component activ
  signal bcast_i       : std_logic;     -- Broadcast flag
  signal slctr_i       : std_logic;     -- I2C control
  signal i             : integer;
  signal scl_en_i      : std_logic := '0';
  signal scl_ii        : std_logic := '0';

  -- purpose: Send a command
  procedure send (
    constant addr   : in  std_logic_vector(4 downto 0);  -- Address
    constant bcast  : in  std_logic;                     -- Broad cast
    constant rw     : in  std_logic;                     -- Read not write
    signal   clk    : in  std_logic;                     -- Clock
    signal   scl    : in  std_logic;                     -- Clock
    signal   scl_en : out std_logic;                     -- Enable scl
    signal   sda    : out std_logic;                     -- Data
    signal   finish : out std_logic)                     -- Finished
  is
    variable i      :     integer;                       -- counter
    variable data_i :     unsigned(7 downto 0);
  begin  -- procedure send
    data_i := '1' & bcast & unsigned(addr) & rw;
    wait until rising_edge(clk);
    wait until rising_edge(scl);
    sda    <= '1';
    scl_en <= '1';
    wait for SPERIOD / 4;
    sda    <= '0';
    for i in 7 downto 0 loop
      wait until falling_edge(scl);                      -- don't care
      wait for SPERIOD / 4;
      sda  <= data_i(i);
      wait until rising_edge(scl);                       -- don't care
    end loop;  -- i
    wait until rising_edge(scl);                         -- don't care
    wait until rising_edge(scl);                         -- don't care
    wait until rising_edge(clk);
    finish <= '1';
    wait until rising_edge(clk);
    finish <= '0';

  end procedure send;

begin  -- architecture test
  scl_ii <= scl_i and scl_en_i;


  serial : entity work.serializer_bc(rtl)
    port map (
        clk      => clk_i,              -- in  clock
        rstb     => rstb_i,             -- in  Async reset
        data     => X"00",              -- in  Data from BC
        shiftin  => sda_in_i,           -- in  Bit to shift in (from right)
        enable   => enable_ii,          -- in  Enable shift-in and load
        load     => '0',                -- in  Load a byte into shift register
        clear    => clear_ii,           -- in  Sync reset.
        cnt_8    => cnt_8_i,            -- out Got 8 shifts
        shiftout => open,               -- out Bit shifted out (from left)
        q        => data_par_i);        -- out Data from RCU

  dut : entity work.fec_address(rtl)
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        scl         => scl_ii,          -- in  Serial clock
        sda_in      => sda_in_i,        -- in  Serial data in
        cnt_8       => cnt_8_i,         -- in  Got 1 byte
        data_par    => data_par_i,      -- in  1 byte input
        hadd        => HADD,            -- in  Hardware address
        finish_tx   => finish_tx_i,     -- in  Transfer done
        finish_rx   => finish_rx_i,     -- in  Recieve done
        enable      => enable_i,        -- out Shift one bit
        clear       => clear_i,         -- out Clear shift register
        en_fec_tx   => en_fec_tx_i,     -- out Enable transfer
        en_fec_rx   => en_fec_rx_i,     -- out Enable recieve
        sda_out     => sda_out_i,       -- out Serial data out
        sel_fec_add => sel_fec_add_i,   -- out Component activ
        bcast       => bcast_i,         -- out Broadcast flag
        slctr       => slctr_i);        -- out I2C control

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';

    wait for 100 ns;
    send(addr   => HADD,
         bcast  => '0',
         rw     => '0',
         clk    => clk_i,
         scl    => scl_i,
         scl_en => scl_en_i,
         sda    => sda_in_i,
         finish => finish_rx_i);

    wait for 100 ns;
    send(addr   => HADD,
         bcast  => '0',
         rw     => '1',
         clk    => clk_i,
         scl    => scl_i,
         scl_en => scl_en_i,
         sda    => sda_in_i,
         finish => finish_tx_i);

    wait for 100 ns;
    send(addr   => HADD,
         bcast  => '1',
         rw     => '0',
         clk    => clk_i,
         scl    => scl_i,
         scl_en => scl_en_i,
         sda    => sda_in_i,
         finish => finish_rx_i);

    wait for 100 ns;
    send(addr   => HADD,
         bcast  => '1',
         rw     => '1',
         clk    => clk_i,
         scl    => scl_i,
         scl_en => scl_en_i,
         sda    => sda_in_i,
         finish => finish_tx_i);
    
  end process stimuli;
  
  -- purpose: Make the serial clock
  -- type   : combinational
  -- inputs : 
  -- outputs: scl_i
  sclk_gen: process is
  begin  -- process sclk_gen
    scl_i <= not scl_i;
    wait for SPERIOD / 2;
  end process sclk_gen;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_gen: process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;

  -- purpose: Syncronize some signals 
  sync_en : process (clk_i, rstb_i) is
  begin  -- process sync_en
    if rstb_i = '0' then                  -- asynchronous reset (active low)
      enable_ii   <= '0';
      clear_ii    <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if sel_fec_add_i = '1' then
        enable_ii <= enable_i;
        clear_ii  <= clear_i;
      else
        enable_ii <= '0';
        clear_ii  <= '0';
      end if;
    end if;
  end process sync_en;

end architecture test;

------------------------------------------------------------------------------
