------------------------------------------------------------------------------
-- Title      : I2C slave
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : slave.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-03-03
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This component is compremised of 5 separate components.  These
--              are:
--              serializer:   A shift register of 1 byte.  Serial input is the
--                            I2C bus serial data.  Parallel input is register
--                            values from the BC.  The parallel output is used
--                            by all state machines to get the needed data.
--                            The serial output is used by the transmitter to
--                            serialize the parallel input to the master of
--                            the I2C bus.
--              decoder:      A state machine that listens for the I2C start
--                            condition, and then decode the first byte
--                            transfered.  If this byte contains the address
--                            of this card (bits 5 to 1), or is broadcast
--                            (bit 6), the state machine acknowledges the
--                            request.  If the request is a write request
--                            (bit 0 is low), then the reciever is started.
--                            If the request is a read request (bit 0 high),
--                            the transmitter is started.
--              reciever:     Recieves one 1 byte, and compares the lower 7
--                            bit to valid instructions.  If the the
--                            instruction is correct, then the state machine
--                            acknowledges the request and reads 2 more bytes.
--                            The instruction code and 16bit data is output
--                            for handling by the BC.
--              transmitter:  Recieves one 1 byte, and compares the lower 7
--                            bit to valid instructions.  If the the
--                            instruction is correct, then the state machine
--                            acknowledges the request.  The instruction code
--                            is sent off to the BC, and valid data should be
--                            returned.   The transmitter then transfers the
--                            data in 2 1-byte blocks to the master of the I2C
--                            bus.
--              selector:     A multiplexer.  Depending on the state of the
--                            various state machines, this mux chooses the
--                            control inputs to the serializer, and the
--                            address returned to the BC.
--
--              Notes: The serial clock should not run faster than 5MHz, as
--              some signals must have proper time to settle before others are
--              propagated.  The serial clock should also run slow enough for
--              the BC to give back the right value in response to the
--              transmitter instruction code, before the transmitter latches
--              the value in the shift register.  In practise, it's probably
--              ok with a 5 Mhz or slower clock. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/20  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.fec_address_pack.all;
use work.serializer_bc_pack.all;
use work.slave_rx_pack.all;
use work.slave_tx_pack.all;
use work.sel_signals_pack.all;

entity slave is
  port (
    clk      : in  std_logic;           -- Clock
    rstb     : in  std_logic;           -- Async reset
    scl      : in  std_logic;           -- Serial clock (max 5 MHz)
    sda_in   : in  std_logic;           -- Input serial data
    hadd     : in  std_logic_vector(4 downto 0);    -- Hardware address
    data_tx  : in  std_logic_vector(15 downto 0);   -- Data to output
    bc_rst   : in  std_logic;           -- Command: Reset BC
    csr1_clr : in  std_logic;           -- Command: Clear CSR1
    sda_out  : out std_logic;           -- Output serial data
    reg_add  : out std_logic_vector(6 downto 0);    -- Register address
    data_rx  : out std_logic_vector(15 downto 0);   -- Data read from bus
    slctr    : out std_logic;           -- I2C-slave take control
    we       : out std_logic;           -- Write enable
    ierr_sc  : out std_logic;           -- Error : Instruction error
    state    : out std_logic_vector(10 downto 0));  -- States
end entity slave;

------------------------------------------------------------------------------
architecture rtl of slave is
  signal sel_add_i    : std_logic;      -- ADD: Select 
  signal clear_add_i  : std_logic;      -- ADD: Clear
  signal enable_add_i : std_logic;      -- ADD: Enable
  signal sda_add_i    : std_logic;      -- ADD: Serial data
  signal for_us_i     : std_logic;      -- ADD: Take control
  
  signal sel_rx_i       : std_logic;    -- RX: Select reciever
  signal clear_rx_i     : std_logic;    -- RX: Clear 
  signal enable_rx_i    : std_logic;    -- RX: Enable
  signal reg_add_rx_i   : std_logic_vector(6 downto 0);
                                        -- RX: Register address
  signal we_rx_i        : std_logic;    -- RX: Write enable
  signal sda_rx_i       : std_logic;    -- RX: Serial data
  signal ierr_rx_i      : std_logic;    -- RX: Instruction error
  signal sel_tx_i       : std_logic;    -- TX: Select transmitter
  signal clear_tx_i     : std_logic;    -- TX: Clear
  signal enable_tx_i    : std_logic;    -- TX: Enable
  signal load_tx_i      : std_logic;    -- TX: Load
  signal reg_add_tx_i   : std_logic_vector(6 downto 0);
                                        -- TX: register address.
  signal sda_tx_i       : std_logic;    -- TX: Serial data
  signal ierr_tx_i      : std_logic;    -- TX: instruction error
  signal clear_i        : std_logic;    -- Clear shift register
  signal enable_i       : std_logic;    -- Enable shift
  signal load_i         : std_logic;    -- Load shift register
  -- signal reg_add_i   : std_logic_vector(6 downto 0);  -- Register address
  -- signal sda_i       : std_logic;    -- Serial data
  -- signal we_i        : std_logic;    -- Write enable
  -- signal ierr_sc_i   : std_logic;    -- Instruction error
  signal cnt_8_i        : std_logic;    -- Got 1 byte
  signal data_ser_in_i  : std_logic;    -- Serial out data
  signal data_par_in_i  : std_logic_vector(7 downto 0);  -- 1 byte input
  signal data_par_out_i : std_logic_vector(7 downto 0);  -- 1 byte input
  signal finish_tx_i    : std_logic;    -- Transfer done
  signal finish_rx_i    : std_logic;    -- Recieve done
  signal en_fec_tx_i    : std_logic;    -- Enable transfer
  signal en_fec_rx_i    : std_logic;    -- Enable recieve
  signal bcast_i        : std_logic;    -- Broadcast flag
  signal state_dec_i    : std_logic_vector(3 downto 0);  -- State of fec_address
  signal state_rx_i     : std_logic_vector(2 downto 0);  -- State of fec_address
  signal state_tx_i     : std_logic_vector(3 downto 0);  -- State of fec_address
  -- signal state_i        : std_logic_vector(10 downto 0);
begin  -- architecture rtl
  -- state(2 downto 0)  <= state_rx_i;
  -- state(6 downto 3)  <= state_tx_i;
  state(2 downto 0)  <= "000";
  state(3)           <= sda_in;
  state(4)           <= clear_i;
  state(5)           <= enable_i;
  state(6)           <= cnt_8_i;
  state(10 downto 7) <= state_dec_i;

  serial : entity work.serializer_bc(rtl)
    port map (
      clk      => clk,                  -- in  clock
      rstb     => rstb,                 -- in  Async reset
      data     => data_par_out_i,       -- in  Data from BC
      shiftin  => sda_in,               -- in  Bit to shift in (from right)
      enable   => enable_i,             -- in  Enable shift-in and load
      load     => load_i,               -- in  Load a byte into shift register
      clear    => clear_i,              -- in  Sync reset.
      cnt_8    => cnt_8_i,              -- out Got 8 shifts
      shiftout => data_ser_in_i,        -- out Bit shifted out (from left)
      q        => data_par_in_i);       -- out Data from RCU

  -- Safe bet is to use rtl2 of this, but rtl seems to work too.
  decoder : entity work.fec_address(rtl_direct)
    port map (
      clk         => clk,               -- in  Clock
      rstb        => rstb,              -- in  Async reset
      scl         => scl,               -- in  Serial clock
      sda_in      => sda_in,            -- in  Serial data in
      cnt_8       => cnt_8_i,           -- in  Got 1 byte
      data_par    => data_par_in_i,     -- in  1 byte input
      hadd        => hadd,              -- in  Hardware address
      finish_tx   => finish_tx_i,       -- in  Transfer done
      finish_rx   => finish_rx_i,       -- in  Recieve done
      enable      => enable_add_i,      -- out Shift one bit
      clear       => clear_add_i,       -- out Clear shift register
      en_fec_tx   => en_fec_tx_i,       -- out Enable transfer
      en_fec_rx   => en_fec_rx_i,       -- out Enable recieve
      sda_out     => sda_add_i,         -- out Serial data out
      sel_fec_add => sel_add_i,         -- out Component activ
      bcast       => bcast_i,           -- out Broadcast flag
      slctr       => slctr,             -- out I2C control
      for_us      => for_us_i,          -- out We are addressed
      state       => state_dec_i);      -- out State

  -- rtl2 is mealy style
  -- rtl is moore style
  transmitter : entity work.slave_tx(rtl)
    port map (
      clk          => clk,              -- in  Clock
      rstb         => rstb,             -- in  Async reset
      bc_rst       => bc_rst,           -- in  Sync reset
      csr1_clr     => csr1_clr,         -- in  Command: Clear CSR1
      cnt_8        => cnt_8_i,          -- in  Serializer got 1 byte
      data_par_in  => data_par_in_i,    -- in  Byte in
      data_ser     => data_ser_in_i,    -- in  1 bit serial in
      data_tx      => data_tx,          -- in  Data to transmit
      en_fec_tx    => en_fec_tx_i,      -- in  Enable transmit
      sda_in       => sda_in,           -- in  Direct serial data
      scl          => scl,              -- in  Serial clock
      bcast        => bcast_i,          -- in  Broadcast flag
      clear        => clear_tx_i,       -- out Clear shift register
      data_par_out => data_par_out_i,   -- out Byte out
      enable       => enable_tx_i,      -- out Enable shift register
      load         => load_tx_i,        -- out Load shift register
      finish_tx    => finish_tx_i,      -- out End of transmit
      sda_out      => sda_tx_i,         -- out Direct serial data out
      reg_add      => reg_add_tx_i,     -- out Register address
      sel_tx       => sel_tx_i,         -- out Select transmit
      ierr_tx      => ierr_tx_i,        -- out Error : instruction error
      state        => state_tx_i);      -- out State

  reciever : entity work.slave_rx
    port map (
      clk         => clk,               -- in  Clock
      rstb        => rstb,              -- in  Async reset
      bc_rst      => bc_rst,            -- in  Sync reset
      csr1_clr    => csr1_clr,          -- in  Command: Clear CSR1
      cnt_8       => cnt_8_i,           -- in  Got 1 byte
      en_fec_rx   => en_fec_rx_i,       -- in  Start
      data_par_in => data_par_in_i,     -- in  In data
      sda_in      => sda_in,            -- in  Serial input data
      scl         => scl,               -- in  Serial clock
      clear       => clear_rx_i,        -- out Clear shift register
      enable      => enable_rx_i,       -- out Enable shift
      data        => data_rx,           -- out output data
      reg_add     => reg_add_rx_i,      -- out Register address
      sda_out     => sda_rx_i,          -- out Serial data out
      sel_rx      => sel_rx_i,          -- out Select reciever
      finish_rx   => finish_rx_i,       -- out Reciever done
      we          => we_rx_i,           -- out Write enable
      ierr_rx     => ierr_rx_i,         -- out Error : instruction error
      state       => state_rx_i);       -- out State

  selector : entity work.sel_signals(rtl_async)
    port map (
      clk        => clk,                -- in  Clock
      rstb       => rstb,               -- in  Async reset
      sel_add    => sel_add_i,          -- in  ADD: Select 
      clear_add  => clear_add_i,        -- in  ADD: Clear
      enable_add => enable_add_i,       -- in  ADD: Enable
      sda_add    => sda_add_i,          -- in  ADD: Serial data
      for_us     => for_us_i,           -- in  ADD: Take control
      sel_rx     => sel_rx_i,           -- in  RX: Select reciever
      clear_rx   => clear_rx_i,         -- in  RX: Clear 
      enable_rx  => enable_rx_i,        -- in  RX: Enable
      reg_add_rx => reg_add_rx_i,       -- in  RX: Register address
      we_rx      => we_rx_i,            -- in  RX: Write enable
      sda_rx     => sda_rx_i,           -- in  RX: Serial data
      ierr_rx    => ierr_rx_i,          -- in  RX: Instruction error
      sel_tx     => sel_tx_i,           -- in  TX: Select transmitter
      clear_tx   => clear_tx_i,         -- in  TX: Clear
      enable_tx  => enable_tx_i,        -- in  TX: Enable
      load_tx    => load_tx_i,          -- in  TX: Load
      reg_add_tx => reg_add_tx_i,       -- in  TX: register address.
      sda_tx     => sda_tx_i,           -- in  TX: Serial data
      ierr_tx    => ierr_tx_i,          -- in  TX: instruction error
      clear      => clear_i,            -- out Clear shift register
      enable     => enable_i,           -- out Enable shift
      load       => load_i,             -- out Load shift register
      reg_add    => reg_add,            -- out Register address
      sda        => sda_out,            -- out Serial data
      we         => we,                 -- out Write enable
      ierr_sc    => ierr_sc);           -- out Instruction error

end architecture rtl;
-------------------------------------------------------------------------------
architecture rtl_all of slave is
  signal start_i     : std_logic;       -- Detected start condition
  signal stop_i      : std_logic;       -- Detected stop condition
  signal scl_i       : std_logic;       -- Last value of SCL
  signal scl_ii      : std_logic;       -- Last value of SCL
  signal sda_in_i    : std_logic;       -- Last value of SDA_IN
  signal sda_in_ii   : std_logic;       -- Last value of SDA_IN
  signal up_edge_i   : std_logic;       -- Up edge on SCL
  signal down_edge_i : std_logic;       -- Down edge on SCL
  signal enable_i    : std_logic;
  signal clear_i     : std_logic;
  signal load_i      : std_logic;
  signal full_i      : std_logic;
  signal rnw_i       : std_logic;
  signal fec_i       : std_logic_vector(4 downto 0);
  signal bcast_i     : std_logic;
  
  signal sda_fsm_i    : std_logic;
  signal sda_shift_i  : std_logic;
  signal sda_select_i : std_logic;

  signal for_us_i : std_logic;
  
  signal word_i   : std_logic;
  signal input_i  : std_logic_vector(7 downto 0);
  signal output_i : std_logic_vector(7 downto 0);

  constant st_idle    : std_logic_vector(10 downto 0) := "00000000001";
  constant st_fec_add : std_logic_vector(10 downto 0) := "00000000010";
  constant st_fec_ack : std_logic_vector(10 downto 0) := "00000000100";
  constant st_reg_num : std_logic_vector(10 downto 0) := "00000001000";
  constant st_reg_ack : std_logic_vector(10 downto 0) := "00000010000";
  constant st_tx      : std_logic_vector(10 downto 0) := "00000100000";
  constant st_tx_ack  : std_logic_vector(10 downto 0) := "00001000000";
  constant st_rx      : std_logic_vector(10 downto 0) := "00010000000";
  constant st_rx_ack  : std_logic_vector(10 downto 0) := "00100000000";
  constant st_stop    : std_logic_vector(10 downto 0) := "01000000000";

  signal state_i : std_logic_vector(10 downto 0);
  
  
  type st_t is (idle,
                fec_add, fec_ack,
                reg_num, reg_ack,
                tx, tx_ack,
                rx, rx_ack,
                stop);                  -- State type
  signal st_i : st_t;                   -- State
begin  -- rtl_all
  combinatorics : block
  begin  -- block combinatorics
    for_us_i <= '1'         when (bcast_i = '1') or (fec_i = hadd) else '0';
    slctr    <= for_us_i;
    sda_out  <= sda_shift_i when sda_select_i = '1'            else sda_fsm_i;
    ierr_sc  <= '1'         when bcast_i = '1' and rnw_i = '1' else '0';

    state(1 downto 0) <= state_i(1 downto 0);
    state(2)          <= for_us_i;
    state(3)          <= start_i; 
    state(4)          <= stop_i; 
    state(5)          <= up_edge_i; 
    state(6)          <= down_edge_i; 
    state(7)          <= enable_i;
    state(8)          <= clear_i;
    state(9)          <= load_i;
    state(10)         <= full_i;

    start_i     <= scl_i and scl_ii and not sda_in_i and sda_in_ii;
    stop_i      <= scl_i and scl_ii and sda_in_i and not sda_in_ii;
    up_edge_i   <= scl_i and not scl_ii;
    down_edge_i <= not scl_i and scl_ii;
  end block combinatorics;

  -- purpose: Store last value of bus signals
  -- type   : sequential
  -- inputs : clk, rstb, scl, sda_in
  -- outputs: scl_i, sda_in_i
  --last_bus : process (clk, rstb)
  --begin  -- process last_bus
  --  if rstb = '0' then                  -- asynchronous reset (active low)
  --    sda_in_i <= '1';
  --    scl_i    <= '1';
  --  elsif clk'event and clk = '1' then  -- rising clock edge
  --    sda_in_i <= sda_in;
  --    scl_i    <= scl;
  --  end if;
  --end process last_bus;

  -- purpose: Detect start and stop conditions
  -- type   : combinational
  -- inputs : scl, sda_in
  -- outputs: start_i, stop_i
  --edge_detect : process (scl, sda_in, scl_i)
  --begin  -- process start_stop_detect
  --  if scl = '1' and scl_i = '1' then   -- Clock not moving
  --    if sda_in = '0' and sda_in_i = '1' then  -- Falling edge on SDA_IN
  --      start_i <= '1';
  --    else
  --      start_i <= '0';
  --    end if;
  --    if sda_in = '1' and sda_in_i = '0' then  -- Rising edge on SDA_IN
  --      stop_i <= '1';
  --    else
  --      stop_i <= '0';
  --    end if;
  --  else
  --    start_i <= '0';
  --    stop_i  <= '0';
  --  end if;
  --  if scl = '1' and scl_i = '0' then
  --    up_edge_i <= '1';
  --  else
  --    up_edge_i <= '0';
  --  end if;
  --  if scl = '0' and scl_i = '1' then
  --    down_edge_i <= '1';
  --  else
  --    down_edge_i <= '0';
  --  end if;
  --end process edge_detect;

  -- purpose: State machine
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  fsm : process (clk, rstb)
  begin  -- process clk
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i         <= idle;
      sda_fsm_i    <= '1';
      sda_select_i <= '0';
      enable_i     <= '0';
      load_i       <= '0';
      clear_i      <= '0';
      fec_i        <= (others => '0');
      bcast_i      <= '0';
      rnw_i        <= '0';
      word_i       <= '0';
      scl_i        <= '1';
      scl_ii       <= '1';
      sda_in_i     <= '1';
      sda_in_ii    <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge
      sda_fsm_i <= '1';
      enable_i  <= '0';
      load_i    <= '0';
      clear_i   <= '0';
      scl_i     <= scl;
      scl_ii    <= scl_i;
      sda_in_i  <= sda_in;
      sda_in_ii <= sda_in_i;
      
      case st_i is
        when idle =>
          state_i        <= st_idle;
          we           <= '0';
          fec_i        <= (others => '1');
          bcast_i      <= '0';
          rnw_i        <= '0';
          sda_select_i <= '0';
          if start_i = '1' then -- start(scl,scl_i,sda_in,sda_in_i) then
            clear_i <= '1';
            st_i    <= fec_add;
          end if;

        when fec_add =>
          state_i <= st_fec_add;
          if up_edge_i = '1' then 
            -- Take bit
            enable_i <= '1';            -- Take bit
          end if;
          if down_edge_i = '1' and full_i = '1' then 
            rnw_i   <= output_i(0);
            fec_i   <= output_i(5 downto 1);
            bcast_i <= output_i(6);
            st_i    <= fec_ack;         -- Set-up for acknowledge
          end if;
          if stop_i = '1' then -- stop(scl,scl_i,sda_in,sda_in_i) then
            st_i <= idle;
          end if;

        when fec_ack =>
          state_i     <= st_fec_ack;
          sda_fsm_i <= bcast_i;  -- '0';             -- Acknowledge
          if down_edge_i = '1' then 
            clear_i <= '1';             -- Clear shift register
            if for_us_i = '1' then
              st_i <= reg_num;
            else
              st_i <= stop;
            end if;
          end if;
          if stop_i = '1' then 
            st_i <= idle;
          end if;

        when reg_num =>
          state_i <= st_reg_num;
          if up_edge_i = '1' then 
            enable_i <= '1';            -- Take bit
          end if;
          if down_edge_i = '1' and full_i = '1' then 
            reg_add <= output_i(6 downto 0);
            st_i    <= reg_ack;         -- Set-up for acknowledge
          end if;
          if stop_i = '1' then 
            st_i <= idle;
          end if;

        when reg_ack =>
          state_i     <= st_reg_ack;
          sda_fsm_i <= bcast_i;  -- '0';             -- Acknowledge
          clear_i   <= '1';             -- Clear shift register
          if down_edge_i = '1' then
            clear_i <= '0';
            if rnw_i = '0' then
              word_i <= '0';
              st_i   <= rx;
            else
              sda_select_i <= '1';
              enable_i     <= '1';      -- Load byte
              load_i       <= '1';      -- Load byte
              input_i      <= data_tx(15 downto 8);
              st_i         <= tx;
            end if;
          end if;
          if stop_i = '1' then 
            st_i <= idle;
          end if;

        when tx =>
          state_i <= st_tx;
          if down_edge_i = '1' then 
            enable_i <= '1';            -- Next bit
            if full_i = '1' then
              sda_select_i <= '0';
              st_i         <= tx_ack;
            end if;
          end if;
          if stop_i = '1' then 
            st_i <= idle;
          end if;

        when tx_ack =>
          state_i <= st_tx_ack;
          if up_edge_i = '1' then 
            if sda_in /= '0' then       -- Master no-acknowledge
              sda_select_i <= '0';
              st_i         <= stop;
            end if;
          end if;
          clear_i <= '1';               -- Clear shift register
          if down_edge_i = '1' then
            clear_i      <= '0';        -- Clear shift register
            load_i       <= '1';        -- Next word
            enable_i     <= '1';        -- Load next byte
            sda_select_i <= '1';
            input_i      <= data_tx(7 downto 0);
            st_i         <= tx;
          end if;

        when rx =>
          state_i <= st_rx;
          if up_edge_i = '1' then 
            enable_i <= '1';            -- Shift in a bit
          end if;
          if down_edge_i = '1' and full_i = '1' then 
            st_i   <= rx_ack;
            word_i <= not word_i;       -- Next byte
            if word_i = '0' then
              data_rx(15 downto 8) <= output_i;
            else
              data_rx(7 downto 0) <= output_i;
            end if;
          end if;
          if stop_i = '1' then 
            st_i <= idle;
          end if;

        when rx_ack =>
          state_i     <= st_rx_ack;
          sda_fsm_i <= '0';             -- acknowledge
          if word_i = '1' then
            we <= '1';
          end if;
          if down_edge_i = '1' then 
            clear_i <= '1';             -- Clear shift register
            st_i    <= rx;
          end if;
          if stop_i = '1' then 
            st_i <= idle;
          end if;

        when stop =>
          state_i <= st_stop;
          if stop_i = '1' then 
            st_i <= idle;
          end if;

        when others => null;
      end case;
    end if;
  end process fsm;

  serializer_bc_1 : entity work.serializer_bc(rtl)
    port map (
      clk      => clk,                  -- [in]
      rstb     => rstb,                 -- [in]
      data     => input_i,              -- [in]
      shiftin  => sda_in,               -- [in]
      enable   => enable_i,             -- [in]
      load     => load_i,               -- [in]
      clear    => clear_i,              -- [in]
      cnt_8    => full_i,               -- [out]
      shiftout => sda_shift_i,          -- [out]
      q        => output_i);            -- [out]
  
end rtl_all;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package slave_pack is
  component slave
    port (
      clk      : in  std_logic;         -- Clock
      rstb     : in  std_logic;         -- Async reset
      scl      : in  std_logic;         -- Serial clock (max 5 MHz)
      sda_in   : in  std_logic;         -- Input serial data
      hadd     : in  std_logic_vector(4 downto 0);   -- Hardware address
      data_tx  : in  std_logic_vector(15 downto 0);  -- Data to output
      bc_rst   : in  std_logic;         -- Command: Reset BC
      csr1_clr : in  std_logic;         -- Command: Clear CSR1
      sda_out  : out std_logic;         -- Output serial data
      reg_add  : out std_logic_vector(6 downto 0);   -- Register address
      data_rx  : out std_logic_vector(15 downto 0);  -- Data read from bus
      slctr    : out std_logic;         -- I2C-slave take control
      we       : out std_logic;         -- Write enable
      ierr_sc  : out std_logic;         -- Error : Instruction error
      state    : out std_logic_vector(10 downto 0)); 
  end component slave;
end package slave_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
