------------------------------------------------------------------------------
-- Title      : I2C slave for transmission
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : slave_tx.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-25
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/19  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.register_config.all;

entity slave_tx is
  port (
    clk          : in  std_logic;       -- Clock
    rstb         : in  std_logic;       -- Async reset
    bc_rst       : in  std_logic;       -- Sync reset
    csr1_clr     : in  std_logic;       -- Command: Clear CSR1
    cnt_8        : in  std_logic;       -- Serializer got 1 byte
    data_par_in  : in  std_logic_vector(7 downto 0);   -- Byte in
    data_ser     : in  std_logic;       -- 1 bit serial in
    data_tx      : in  std_logic_vector(15 downto 0);  -- Data to transmit
    en_fec_tx    : in  std_logic;       -- Enable transmit
    sda_in       : in  std_logic;       -- Direct serial data
    scl          : in  std_logic;       -- Serial clock
    bcast        : in  std_logic;       -- Broadcast flag
    clear        : out std_logic;       -- Clear shift register
    data_par_out : out std_logic_vector(7 downto 0);   -- Byte out
    enable       : out std_logic;       -- Enable shift register
    load         : out std_logic;       -- Load shift register
    finish_tx    : out std_logic;       -- End of transmit
    sda_out      : out std_logic;       -- Direct serial data out
    reg_add      : out std_logic_vector(6 downto 0);   -- Register address
    sel_tx       : out std_logic;       -- Select transmit
    ierr_tx      : out std_logic;       -- Error : instruction error
    state        : out std_logic_vector(3 downto 0));  -- State 
end entity slave_tx;


architecture rtl2 of slave_tx is
  type state_t is (idle,
                   start,
                   wait_add,
                   store_add,
                   ack_1,
                   ack_2,
                   load_1,
                   load_2,
                   wait_dt,
                   store_dt,
                   ack_master_1,
                   ack_master_2,
                   wait_stop_1,
                   wait_stop_2,
                   wait_stop_3,
                   finish,
                   preload);            -- States

  constant ST_idle         : std_logic_vector(3 downto 0) := "0000";
  constant ST_start        : std_logic_vector(3 downto 0) := "0001";
  constant ST_wait_add     : std_logic_vector(3 downto 0) := "0010";
  constant ST_store_add    : std_logic_vector(3 downto 0) := "0011";
  constant ST_ack_1        : std_logic_vector(3 downto 0) := "0100";
  constant ST_ack_2        : std_logic_vector(3 downto 0) := "0101";
  constant ST_load_1       : std_logic_vector(3 downto 0) := "0110";
  constant ST_load_2       : std_logic_vector(3 downto 0) := "0111";
  constant ST_wait_dt      : std_logic_vector(3 downto 0) := "1000";
  constant ST_store_dt     : std_logic_vector(3 downto 0) := "1001";
  constant ST_ack_master_1 : std_logic_vector(3 downto 0) := "1010";
  constant ST_ack_master_2 : std_logic_vector(3 downto 0) := "1011";
  constant ST_wait_stop_1  : std_logic_vector(3 downto 0) := "1100";
  constant ST_wait_stop_2  : std_logic_vector(3 downto 0) := "1101";
  constant ST_wait_stop_3  : std_logic_vector(3 downto 0) := "1110";
  constant ST_preload      : std_logic_vector(3 downto 0) := "1111";

  signal st_i        : state_t;         -- State
  signal clr_cnt_i   : std_logic;       -- Clear counter
  signal en_cnt_i    : std_logic;       -- Enable counter
  signal en_reg_i    : std_logic;       -- Enable register output
  signal cnt_i       : unsigned(0 downto 0);          -- Counter
  signal ierr_tx_i   : std_logic;       -- Error: instruction error
  signal reg_add_i   : integer range 0 to NUM_REG-1;  -- Address
  signal not_sc_i    : std_logic;
  signal not_bcast_i : std_logic;

begin  -- rtl2
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    not_sc_i <= '1' when not regs(reg_add_i).sc else '0';
    not_bcast_i <= '0' when (reg_add_i = add_zero
                             or reg_add_i = add_free) else '1';
    ierr_tx   <= ierr_tx_i;
    ierr_tx_i <= not_bcast_i when bcast = '1' else not_sc_i;
    reg_add   <= std_logic_vector(to_unsigned(reg_add_i, ADD_BITS));
    -- state     <= std_logic_vector(to_unsigned(state_t'pos(st_i), 4));
  end block combi;

  -- purpose: Decode incoming signal, and transmit data
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  slave_tx_fsm : process (clk, rstb)
  begin  -- process slave_tx_fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      clear     <= '0';
      enable    <= '0';
      load      <= '0';
      finish_tx <= '0';
      sda_out   <= '1';
      clr_cnt_i <= '0';
      en_cnt_i  <= '0';
      en_reg_i  <= '0';
      sel_tx    <= '0';
      st_i      <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- default values
      clear     <= '0';
      enable    <= '0';
      load      <= '0';
      finish_tx <= '0';
      sda_out   <= '0';
      clr_cnt_i <= '0';
      en_cnt_i  <= '0';
      en_reg_i  <= '0';
      sel_tx    <= '1';
      st_i      <= st_i;                -- stay in state

      case st_i is
        when idle =>
          state <= ST_idle;
          if en_fec_tx = '1' then
            sel_tx  <= '1';
            st_i    <= start;
          else
            sel_tx <= '0';              -- Not selected
            st_i   <= idle;
          end if;

        when start =>                   -- Possible start condition
          state   <= ST_start;
          if scl = '0' then
            st_i <= wait_add;
          end if;

        when wait_add =>                -- Get the instruction
          state <= ST_wait_add;
          if scl = '1' then
            -- Take address one clock cycle earlier
            if scl = '0' and cnt_8 = '1' then
              en_reg_i <= '1';
            else
              en_reg_i <= '0';
            end if;
            st_i <= store_add;
          end if;

        when store_add =>                       -- Store 1 bit 
          state  <= ST_store_add;
          enable <= '1';
          if scl = '0' and cnt_8 = '1' then     -- Got 1 byte, falling edge
            -- acknowledge 1 clock cycle earlier
            if ierr_tx_i = '1' then
              sda_out <= '1';
            else
              sda_out <= '0';
            end if;
            clear    <= '1';                    -- Clear shift register
            en_reg_i <= '1';                    -- Keep addr
            st_i     <= ack_1;                  -- Acknowledge
          elsif scl = '0' and cnt_8 = '0' then  -- Not a word yet
            clear    <= '0';
            en_reg_i <= '0';
            sda_out  <= '1';
            st_i     <= wait_add;               -- Get more bits
          else
            clear    <= '0';
            en_reg_i <= '0';
            sda_out  <= '1';
            st_i     <= store_add;              -- Wait for falling edge
          end if;

        when ack_1 =>                   -- Acknowledge or fail
          state    <= ST_ack_1;
          clear    <= '1';              -- Clear shift register
          en_reg_i <= '1';              -- Store address
          if ierr_tx_i = '1' then       -- Bad instruction
            finish_tx <= '1';           -- Finish this
            sda_out   <= '1';           -- No acknowledge
            st_i      <= idle;
          else
            finish_tx <= '0';           -- More to come
            sda_out   <= '0';           -- Acknowledge
            if scl = '1' then
              st_i <= ack_2;
            else
              st_i <= ack_1;            -- Wait for rising edge
            end if;
          end if;

        when ack_2 =>                   -- Acknowledge
          state   <= ST_ack_2;
          sda_out <= '0';               -- Acknowledge bit
          if scl = '0' then             -- Falling edge
            st_i <= load_1;
          else
            st_i <= ack_2;              -- Wait for falling edge
          end if;

        when load_1 =>                  -- Get one data byte
          state   <= ST_load_1;
          enable  <= '1';
          load    <= '1';               -- Load 1 byte in shift regiser
          sda_out <= data_ser;
          if scl = '1' then             -- Rising edge
            st_i <= load_2;
          else
            st_i <= load_1;             -- Wait for rising edge
          end if;

        when load_2 =>
          state   <= ST_load_2;
          sda_out <= data_ser;
          if scl = '0' then             -- Falling edge
            st_i <= wait_dt;
          else
            st_i <= load_2;             -- Wait for falling edge
          end if;

        when wait_dt =>                 -- Shift out one bit
          state   <= ST_wait_dt;
          enable  <= '1';               -- Shift out one bit
          sda_out <= data_ser;
          if scl = '1' then             -- Rising edge
            st_i <= store_dt;
          else
            st_i <= wait_dt;            -- Wait for rising edge
          end if;
          
        when store_dt =>                     -- Store one data word
          state   <= ST_store_dt;
          sda_out <= data_ser;
          if scl = '0' and cnt_8 = '1' then  -- Got 1 byte on falling edge
            st_i <= ack_master_1;
          elsif scl = '0' and cnt_8 = '0' then
            st_i <= wait_dt;                 -- More data 
          else
            st_i <= store_dt;                -- Wait for falling edge
          end if;

        when ack_master_1 =>            -- Wait for acknowledge from master
          state   <= ST_ack_master_1;
          clear   <= '1';               -- Clear shift register
          sda_out <= data_ser;
          if scl = '1' and sda_in = '1' then     -- Rising edge and no-ack
            st_i <= ack_master_2;
          elsif scl = '1' and sda_in = '0' then  -- Acknowledge
            st_i <= preload;
          else
            st_i <= ack_master_1;       -- Wait for rising edge
          end if;

        when ack_master_2 =>            -- Wait for acknowledge from master
          state   <= ST_ack_master_2;
          sda_out <= data_ser;
          if scl = '0' and sda_in = '1' then  -- Falling edge and no-ack 
            clr_cnt_i <= '1';           -- Clear counter
            st_i      <= wait_stop_1;
          else                          -- Acknowledge
            clr_cnt_i <= '0';
            st_i      <= ack_master_2;  -- Wait for falling edge
          end if;

        when wait_stop_1 =>
          state   <= ST_wait_stop_1;
          sda_out <= '1';
          if scl = '0' and sda_in = '0' then
            st_i <= wait_stop_2;
          else
            st_i <= wait_stop_1;        -- Wait for falling edge
          end if;

        when wait_stop_2 =>
          state   <= ST_wait_stop_2;
          sda_out <= '1';
          if scl = '1' and sda_in = '0' then
            st_i <= wait_stop_3;
          else
            st_i <= wait_stop_2;        -- Wait for rising edge
          end if;

        when wait_stop_3 =>
          state   <= ST_wait_stop_3;
          sda_out <= '1';
          if scl = '1' and sda_in = '1' then  -- Stop condition
            finish_tx <= '1';
            st_i      <= finish;              -- we done
          else
            finish_tx <= '0';
            st_i      <= wait_stop_3;         -- Wait for rising edge on data
          end if;

        when finish =>
          state     <= ST_wait_stop_3;
          finish_tx <= '1';
          st_i      <= idle;
          
        when preload =>
          state   <= ST_preload;
          sda_out <= data_ser;
          if scl = '0' and sda_in = '0' then  -- Ack by mastere
            en_cnt_i <= '1';                  -- Got one byte
            st_i     <= load_1;
          else
            en_cnt_i <= '0';
            st_i     <= preload;
          end if;

        when others =>
          state <= ST_idle;
          st_i  <= idle;
      end case;
    end if;
  end process slave_tx_fsm;

  -- purpose: Count to 2
  -- type   : sequential
  -- inputs : clk, rstb, clr_cnt_i, en_cnt_i
  -- outputs: cnt_i
  counter : process (clk, rstb) is
  begin  -- process counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clr_cnt_i = '1' then
        cnt_i <= (others => '0');
      elsif en_cnt_i = '1' then
        cnt_i <= cnt_i + 1;
      end if;
    end if;
  end process counter;

  -- purpose: Output register address
  -- type   : sequential
  -- inputs : clk, rstb, bc_rst, csr1_clr, en_reg_i, cnt_i, data_par_in
  -- outputs: data_par_out, reg_add_i
  output : process (clk, rstb) is
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      reg_add_i    <= 0;
      data_par_out <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' or csr1_clr = '1' then
        reg_add_i <= 0;
      elsif en_reg_i = '1' then
        reg_add_i <= to_integer(unsigned(data_par_in(ADD_BITS-1 downto 0)));
      elsif cnt_i = "0" then
        data_par_out <= data_tx(15 downto 8);
      elsif cnt_i = "1" then
        data_par_out <= data_tx(7 downto 0);
      end if;
    end if;
  end process output;
  
end rtl2;
-------------------------------------------------------------------------------
architecture rtl of slave_tx is
  type state_t is (idle,
                   start,
                   wait_add,
                   store_add,
                   ack_1,
                   ack_2,
                   load_1,
                   load_2,
                   wait_dt,
                   store_dt,
                   ack_master_1,
                   ack_master_2,
                   wait_stop_1,
                   wait_stop_2,
                   wait_stop_3,
                   preload);            -- States
  constant ST_idle         : std_logic_vector(3 downto 0) := "0000";
  constant ST_start        : std_logic_vector(3 downto 0) := "0001";
  constant ST_wait_add     : std_logic_vector(3 downto 0) := "0010";
  constant ST_store_add    : std_logic_vector(3 downto 0) := "0011";
  constant ST_ack_1        : std_logic_vector(3 downto 0) := "0100";
  constant ST_ack_2        : std_logic_vector(3 downto 0) := "0101";
  constant ST_load_1       : std_logic_vector(3 downto 0) := "0110";
  constant ST_load_2       : std_logic_vector(3 downto 0) := "0111";
  constant ST_wait_dt      : std_logic_vector(3 downto 0) := "1000";
  constant ST_store_dt     : std_logic_vector(3 downto 0) := "1001";
  constant ST_ack_master_1 : std_logic_vector(3 downto 0) := "1010";
  constant ST_ack_master_2 : std_logic_vector(3 downto 0) := "1011";
  constant ST_wait_stop_1  : std_logic_vector(3 downto 0) := "1100";
  constant ST_wait_stop_2  : std_logic_vector(3 downto 0) := "1101";
  constant ST_wait_stop_3  : std_logic_vector(3 downto 0) := "1110";
  constant ST_preload      : std_logic_vector(3 downto 0) := "1111";

  signal st_i        : state_t;         -- State
  signal nx_st_i     : state_t;         -- Next state
  signal clr_cnt_i   : std_logic;       -- Clear counter
  signal en_cnt_i    : std_logic;       -- Enable counter
  signal en_reg_i    : std_logic;       -- Enable register output
  signal cnt_i       : unsigned(0 downto 0);          -- Counter
  signal ierr_tx_i   : std_logic;       -- Error: instruction error
  signal reg_add_i   : integer range 0 to NUM_REG-1;  -- Address
  signal not_sc_i    : std_logic;
  signal not_bcast_i : std_logic;
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    not_sc_i <= '1' when not regs(reg_add_i).sc else '0';
    not_bcast_i <= '0' when (reg_add_i = add_zero
                             or reg_add_i = add_free) else '1';
    ierr_tx   <= ierr_tx_i;
    ierr_tx_i <= not_bcast_i when bcast = '1' else not_sc_i;
    reg_add   <= std_logic_vector(to_unsigned(reg_add_i, ADD_BITS));
    sel_tx    <= '0'         when st_i = idle else '1';
    -- state     <= std_logic_vector(to_unsigned(state_t'pos(st_i), 4));
  end block combi;

  -- purpose: Update the next state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st_i
  -- outputs: st_i
  next_state : process (clk, rstb) is
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;

  -- purpose: Final state machine to handle the transmission
  -- type   : combinational
  -- inputs : st_i, en_fec_tx, scl, sda_in, cnt_8, data_ser, ierr_tx
  -- outputs: clear, enable, load, sda_out, en_reg_i, en_cnt_i, clr_cnt_i
  slave_tx_fsm : process (st_i, en_fec_tx, scl, sda_in, cnt_8,
                         data_ser, ierr_tx_i) is
  begin  -- process slave_tx_fsm

    case st_i is
      when idle =>
        state     <= ST_idle;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if en_fec_tx = '1' then
          nx_st_i <= start;
        else
          nx_st_i <= idle;
        end if;

      when start =>
        state     <= ST_start;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '0' then
          nx_st_i <= wait_add;
        else
          nx_st_i <= start;             -- Wait for falling edge
        end if;

      when wait_add =>                  -- Get the instruction
        state     <= ST_wait_add;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '1' then
          nx_st_i <= store_add;
        else
          nx_st_i <= wait_add;          -- Wait for rising edge
        end if;

      when store_add =>                       -- Store 1 bit 
        state     <= ST_store_add;
        enable    <= '1';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        if scl = '0' and cnt_8 = '1' then     -- Got 1 byte, falling edge
          clear    <= '1';                    -- Clear shift register
          en_reg_i <= '1';                    -- Keep addr
          nx_st_i  <= ack_1;                  -- Acknowledge
        elsif scl = '0' and cnt_8 = '0' then  -- Not a word yet
          clear    <= '0';
          en_reg_i <= '0';
          nx_st_i  <= wait_add;               -- Get more bits
        else
          clear    <= '0';
          en_reg_i <= '0';
          nx_st_i  <= store_add;              -- Wait for falling edge
        end if;

      when ack_1 =>                     -- Acknowledge or fail
        state     <= ST_ack_1;
        clear     <= '1';               -- Clear shift register
        enable    <= '0';
        load      <= '0';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '1';               -- Store address
        if ierr_tx_i = '1' then         -- Bad instruction
          finish_tx <= '1';             -- Finish this
          sda_out   <= '1';             -- No acknowledge
          nx_st_i   <= idle;
        else
          finish_tx <= '0';             -- More to come
          sda_out   <= '0';             -- Acknowledge
          if scl = '1' then
            nx_st_i <= ack_2;
          else
            nx_st_i <= ack_1;           -- Wait for rising edge
          end if;
        end if;

      when ack_2 =>                     -- Acknowledge
        state     <= ST_ack_2;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= '0';               -- Acknowledge bit
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '0' then               -- Falling edge
          nx_st_i <= load_1;
        else
          nx_st_i <= ack_2;             -- Wait for falling edge
        end if;

      when load_1 =>                    -- Get one data byte
        state     <= ST_load_1;
        clear     <= '0';
        enable    <= '1';
        load      <= '1';               -- Load 1 byte in shift regiser
        finish_tx <= '0';
        sda_out   <= data_ser;
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '1' then               -- Rising edge
          nx_st_i <= load_2;
        else
          nx_st_i <= load_1;            -- Wait for rising edge
        end if;

      when load_2 =>
        state     <= ST_load_2;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= data_ser;
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '0' then               -- Falling edge
          nx_st_i <= wait_dt;
        else
          nx_st_i <= load_2;            -- Wait for falling edge
        end if;

      when wait_dt =>                   -- Shift out one bit
        state     <= ST_wait_dt;
        clear     <= '0';
        enable    <= '1';               -- Shift out one bit
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= data_ser;
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '1' then               -- Rising edge
          nx_st_i <= store_dt;
        else
          nx_st_i <= wait_dt;           -- Wait for rising edge
        end if;
        
      when store_dt =>                     -- Store one data word
        state     <= ST_store_dt;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= data_ser;
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '0' and cnt_8 = '1' then  -- Got 1 byte on falling edge
          nx_st_i <= ack_master_1;
        elsif scl = '0' and cnt_8 = '0' then
          nx_st_i <= wait_dt;              -- More data 
        else
          nx_st_i <= store_dt;             -- Wait for falling edge
        end if;

      when ack_master_1 =>              -- Wait for acknowledge from master
        state     <= ST_ack_master_1;
        clear     <= '1';               -- Clear shift register
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= data_ser;
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '1' and sda_in = '1' then     -- Rising edge and no-ack
          nx_st_i <= ack_master_2;
        elsif scl = '1' and sda_in = '0' then  -- Acknowledge
          nx_st_i <= preload;
        else
          nx_st_i <= ack_master_1;      -- Wait for rising edge
        end if;

      when ack_master_2 =>                  -- Wait for acknowledge from master
        state     <= ST_ack_master_2;
        clear     <= '0';                   -- Clear shift register
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= data_ser;
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '0' and sda_in = '1' then  -- Falling edge and no-ack 
          clr_cnt_i <= '1';                 -- Clear counter
          nx_st_i   <= wait_stop_1;
        else                                -- Acknowledge
          clr_cnt_i <= '0';
          nx_st_i   <= ack_master_2;        -- Wait for falling edge
        end if;

      when wait_stop_1 =>
        state     <= ST_wait_stop_1;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '0' and sda_in = '0' then
          nx_st_i <= wait_stop_2;
        else
          nx_st_i <= wait_stop_1;       -- Wait for falling edge
        end if;

      when wait_stop_2 =>
        state     <= ST_wait_stop_2;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '1' and sda_in = '0' then
          nx_st_i <= wait_stop_3;
        else
          nx_st_i <= wait_stop_2;       -- Wait for rising edge
        end if;

      when wait_stop_3 =>
        state     <= ST_wait_stop_3;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        sda_out   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        if scl = '1' and sda_in = '1' then  -- Stop condition
          finish_tx <= '1';
          nx_st_i   <= idle;                -- we done
        else
          finish_tx <= '0';
          nx_st_i   <= wait_stop_3;         -- Wait for rising edge on data
        end if;

      when preload =>
        state     <= ST_preload;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= data_ser;
        clr_cnt_i <= '0';
        en_reg_i  <= '0';
        if scl = '0' and sda_in = '0' then  -- Ack by mastere
          en_cnt_i <= '1';                  -- Got one byte
          nx_st_i  <= load_1;
        else
          en_cnt_i <= '0';
          nx_st_i  <= preload;
        end if;

      when others =>
        state     <= ST_idle;
        clear     <= '0';
        enable    <= '0';
        load      <= '0';
        finish_tx <= '0';
        sda_out   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        nx_st_i   <= idle;
    end case;

  end process slave_tx_fsm;

  -- purpose: Count to 2
  -- type   : sequential
  -- inputs : clk, rstb, clr_cnt_i, en_cnt_i
  -- outputs: cnt_i
  counter : process (clk, rstb) is
  begin  -- process counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clr_cnt_i = '1' then
        cnt_i <= (others => '0');
      elsif en_cnt_i = '1' then
        cnt_i <= cnt_i + 1;
      end if;
    end if;
  end process counter;

  -- purpose: Output register address
  -- type   : sequential
  -- inputs : clk, rstb, bc_rst, csr1_clr, en_reg_i, cnt_i, data_par_in
  -- outputs: data_par_out, reg_add_i
  output : process (clk, rstb) is
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      reg_add_i    <= 0;
      data_par_out <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' or csr1_clr = '1' then
        reg_add_i <= 0;
      elsif en_reg_i = '1' then
        reg_add_i <= to_integer(unsigned(data_par_in(ADD_BITS-1 downto 0)));
      elsif cnt_i = "0" then
        data_par_out <= data_tx(15 downto 8);
      elsif cnt_i = "1" then
        data_par_out <= data_tx(7 downto 0);
      end if;
    end if;
  end process output;
end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package slave_tx_pack is
  component slave_tx
    port (
      clk          : in  std_logic;     -- Clock
      rstb         : in  std_logic;     -- Async reset
      bc_rst       : in  std_logic;     -- Sync reset
      csr1_clr     : in  std_logic;     -- Command: Clear CSR1
      cnt_8        : in  std_logic;     -- Serializer got 1 byte
      data_par_in  : in  std_logic_vector(7 downto 0);   -- Byte in
      data_ser     : in  std_logic;     -- 1 bit serial in
      data_tx      : in  std_logic_vector(15 downto 0);  -- Data to transmit
      en_fec_tx    : in  std_logic;     -- Enable transmit
      sda_in       : in  std_logic;     -- Direct serial data
      scl          : in  std_logic;     -- Serial clock
      bcast        : in  std_logic;     -- Broadcast flag
      clear        : out std_logic;     -- Clear shift register
      data_par_out : out std_logic_vector(7 downto 0);   -- Byte out
      enable       : out std_logic;     -- Enable shift register
      load         : out std_logic;     -- Load shift register
      finish_tx    : out std_logic;     -- End of transmit
      sda_out      : out std_logic;     -- Direct serial data out
      reg_add      : out std_logic_vector(6 downto 0);   -- Register address
      sel_tx       : out std_logic;     -- Select transmit
      ierr_tx      : out std_logic;     -- Error : instruction error
      state        : out std_logic_vector(3 downto 0));  -- State 
  end component slave_tx;
end package slave_tx_pack;

------------------------------------------------------------------------------
-- 
-- EOF
--
