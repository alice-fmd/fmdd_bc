------------------------------------------------------------------------------
-- Title      : Select which I2C signals to use
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : sel_signals.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-25
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/20  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity sel_signals is
  port (
    clk        : in  std_logic;                     -- Clock
    rstb       : in  std_logic;                     -- Async reset
    sel_add    : in  std_logic;                     -- ADD: Select 
    clear_add  : in  std_logic;                     -- ADD: Clear
    enable_add : in  std_logic;                     -- ADD: Enable
    sda_add    : in  std_logic;                     -- ADD: Serial data
    for_us     : in  std_logic;                     -- ADD: Take control
    sel_rx     : in  std_logic;                     -- RX: Select reciever
    clear_rx   : in  std_logic;                     -- RX: Clear 
    enable_rx  : in  std_logic;                     -- RX: Enable
    reg_add_rx : in  std_logic_vector(6 downto 0);  -- RX: Register address
    we_rx      : in  std_logic;                     -- RX: Write enable
    sda_rx     : in  std_logic;                     -- RX: Serial data
    ierr_rx    : in  std_logic;                     -- RX: Instruction error
    sel_tx     : in  std_logic;                     -- TX: Select transmitter
    clear_tx   : in  std_logic;                     -- TX: Clear
    enable_tx  : in  std_logic;                     -- TX: Enable
    load_tx    : in  std_logic;                     -- TX: Load
    reg_add_tx : in  std_logic_vector(6 downto 0);  -- TX: register address.
    sda_tx     : in  std_logic;                     -- TX: Serial data
    ierr_tx    : in  std_logic;                     -- TX: instruction error
    clear      : out std_logic;                     -- Clear shift register
    enable     : out std_logic;                     -- Enable shift
    load       : out std_logic;                     -- Load shift register
    reg_add    : out std_logic_vector(6 downto 0);  -- Register address
    sda        : out std_logic;                     -- Serial data
    we         : out std_logic;                     -- Write enable
    ierr_sc    : out std_logic);                    -- Instruction error
end entity sel_signals;

-------------------------------------------------------------------------------
architecture rtl_async of sel_signals is

begin  -- architecture rtl
  combinatorics : block
  begin  -- block combinatorics
    sda <= sda_add when sel_add = '1' else
           sda_rx  when sel_rx  = '1' else
           sda_tx  when sel_tx  = '1' else '1';
  end block combinatorics;
  
  -- purpose: Select which output to use
  -- type   : sequential
  -- inputs : clk, rstb, sel_rx, sel_tx, sel_add
  -- outputs: sda, enable, clear, load, reg_add, ierr_sc, we
  mux : process (clk, rstb) is
  begin  -- process mux
    if rstb = '0' then                  -- asynchronous reset (active low)
      enable  <= '0';
      clear   <= '0';
      load    <= '0';
      reg_add <= (others => '0');
      we      <= '0';
      ierr_sc <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if sel_add = '1' then             -- Address handler
        enable <= enable_add;
        clear  <= clear_add;
        load   <= '0';
        -- reg_add <= (others => '0');  -- Leave as is. 
        we     <= '0';
        -- ierr_sc <= '0';              -- Leave as is 
      elsif sel_rx = '1' then           -- Reciever
        enable <= enable_rx;
        clear  <= clear_rx;
        load   <= '0';
        if for_us = '1' then
          we <= we_rx;
        else
          we <= '0';
        end if;
        ierr_sc <= ierr_rx;
        if we_rx = '1' and for_us = '1' then
          reg_add <= reg_add_rx;
        end if;
      elsif sel_tx = '1' then           -- Transmitter
        enable  <= enable_tx;
        clear   <= clear_tx;
        load    <= load_tx;
        we      <= '0';
        ierr_sc <= ierr_tx;
        reg_add <= reg_add_tx;
      else                              -- No one 
        enable  <= '0';
        clear   <= '0';
        load    <= '0';
        reg_add <= (others => '0');
        we      <= '0';
        ierr_sc <= '0';
      end if;
    end if;
  end process mux;

end architecture rtl_async;

-------------------------------------------------------------------------------
architecture rtl_clocked of sel_signals is

begin  -- architecture rtl
  -- purpose: Select which output to use
  -- type   : sequential
  -- inputs : clk, rstb, sel_rx, sel_tx, sel_add
  -- outputs: sda, enable, clear, load, reg_add, ierr_sc, we
  mux : process (clk, rstb) is
  begin  -- process mux
    if rstb = '0' then                  -- asynchronous reset (active low)
      sda     <= '0';
      enable  <= '0';
      clear   <= '0';
      load    <= '0';
      reg_add <= (others => '0');
      we      <= '0';
      ierr_sc <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if sel_add = '1' then             -- Address handler
        sda    <= sda_add;
        enable <= enable_add;
        clear  <= clear_add;
        load   <= '0';
        -- reg_add <= (others => '0');  -- Leave as is. 
        we     <= '0';
        -- ierr_sc <= '0';              -- Leave as is 
      elsif sel_rx = '1' then           -- Reciever
        sda    <= sda_rx;
        enable <= enable_rx;
        clear  <= clear_rx;
        load   <= '0';
        if for_us = '1' then
          we <= we_rx;
        else
          we <= '0';
        end if;
        ierr_sc <= ierr_rx;
        if we_rx = '1' and for_us = '1' then
          reg_add <= reg_add_rx;
        end if;
      elsif sel_tx = '1' then           -- Transmitter
        sda     <= sda_tx;
        enable  <= enable_tx;
        clear   <= clear_tx;
        load    <= load_tx;
        we      <= '0';
        ierr_sc <= ierr_tx;
        reg_add <= reg_add_tx;
      else                              -- No one 
        sda     <= '0';
        enable  <= '0';
        clear   <= '0';
        load    <= '0';
        reg_add <= (others => '0');
        we      <= '0';
        ierr_sc <= '0';
      end if;
    end if;
  end process mux;

end architecture rtl_clocked;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package sel_signals_pack is
  component sel_signals
    port (
      clk        : in  std_logic;                     -- Clock
      rstb       : in  std_logic;                     -- Async reset
      sel_add    : in  std_logic;                     -- ADD: Select 
      clear_add  : in  std_logic;                     -- ADD: Clear
      enable_add : in  std_logic;                     -- ADD: Enable
      sda_add    : in  std_logic;                     -- ADD: Serial data
      for_us     : in  std_logic;                     -- ADD: We are addressed
      sel_rx     : in  std_logic;                     -- RX: Select reciever
      clear_rx   : in  std_logic;                     -- RX: Clear 
      enable_rx  : in  std_logic;                     -- RX: Enable
      reg_add_rx : in  std_logic_vector(6 downto 0);  -- RX: Register address
      we_rx      : in  std_logic;                     -- RX: Write enable
      sda_rx     : in  std_logic;                     -- RX: Serial data
      ierr_rx    : in  std_logic;                     -- RX: Instruction error
      sel_tx     : in  std_logic;                     -- TX: Select transmitter
      clear_tx   : in  std_logic;                     -- TX: Clear
      enable_tx  : in  std_logic;                     -- TX: Enable
      load_tx    : in  std_logic;                     -- TX: Load 
      reg_add_tx : in  std_logic_vector(6 downto 0);  -- TX: register address.
      sda_tx     : in  std_logic;                     -- TX: Serial data
      ierr_tx    : in  std_logic;                     -- TX: instruction error
      clear      : out std_logic;                     -- Clear shift register
      enable     : out std_logic;                     -- Enable shift
      load       : out std_logic;                     -- Load shift register
      reg_add    : out std_logic_vector(6 downto 0);  -- Register address
      sda        : out std_logic;                     -- Serial data
      we         : out std_logic;                     -- Write enable
      ierr_sc    : out std_logic);                    -- Instruction error
  end component sel_signals;
end package sel_signals_pack;

------------------------------------------------------------------------------
-- 
-- EOF
--
