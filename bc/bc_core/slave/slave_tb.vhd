------------------------------------------------------------------------------
-- Title      : Test of I2C slave
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : slave_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-03-03
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/20  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.slave_pack.all;
use work.i2c_simul_pack.all;

------------------------------------------------------------------------------
entity slave_tb is
end entity slave_tb;

------------------------------------------------------------------------------
architecture test of slave_tb is
  constant HADD   : std_logic_vector(4 downto 0) := "00000";
  constant NOTUS  : std_logic_vector(4 downto 0) := "00001";
  constant PERIOD : time                         := 25 ns;

  signal clk_i      : std_logic := '0';  -- Clock
  signal rstb_i     : std_logic := '0';  -- Async reset
  signal scl_i      : std_logic := '0';  -- Serial clock (max 5 MHz)
  signal sda_in_i   : std_logic := '1';  -- Input serial data
  signal data_tx_i  : std_logic_vector(15 downto 0);  -- Data to output
  signal bc_rst_i   : std_logic := '0';  -- Command: Reset BC
  signal csr1_clr_i : std_logic := '0';  -- Command: Clear CSR1
  signal sda_out_i  : std_logic;        -- Output serial data
  signal reg_add_i  : std_logic_vector(6 downto 0);   -- Register address
  signal data_rx_i  : std_logic_vector(15 downto 0);  -- Data read from bus
  signal slctr_i    : std_logic;        -- I2C-slave take control
  signal we_i       : std_logic;        -- Write enable
  signal ierr_sc_i  : std_logic;        -- Error : Instruction error
  signal scl_en_i   : std_logic := '0';
  signal scl_inh_i  : std_logic := '0';
  signal scl_ii     : std_logic;
  signal i          : integer;          -- counter
  signal buf_i      : unsigned(15 downto 0);
  signal res_i      : unsigned(15 downto 0);
  signal state_i    : i2c_state_t;
  
  -- Outout of dut1
  signal sda_out_1i : std_logic;        -- out Output serial data
  signal reg_add_1i : std_logic_vector(6 downto 0);   -- out Register address
  signal data_rx_1i : std_logic_vector(15 downto 0);  -- out Data read from bus
  signal slctr_1i   : std_logic;        -- out I2C-slave take control
  signal we_1i      : std_logic;        -- out Write enable
  signal ierr_sc_1i : std_logic;

  -- Outout of dut2
  signal sda_out_2i : std_logic;        -- out Output serial data
  signal reg_add_2i : std_logic_vector(6 downto 0);   -- out Register address
  signal data_rx_2i : std_logic_vector(15 downto 0);  -- out Data read from bus
  signal slctr_2i   : std_logic;        -- out I2C-slave take control
  signal we_2i      : std_logic;        -- out Write enable
  signal ierr_sc_2i : std_logic;

begin  -- architecture test
  scl_ii    <= '0' when scl_i = '0' and scl_en_i = '1' else '1';
  sda_out_i <= sda_out_1i when slctr_1i = '1' else
               sda_out_2i when slctr_2i = '1' else 'Z';
  reg_add_i <= reg_add_1i when slctr_1i = '1' else
               reg_add_2i when slctr_2i = '1' else (others => 'Z');
  data_rx_i <= data_rx_1i when slctr_1i = '1' else
               data_rx_2i when slctr_2i = '1' else (others => 'Z');
  slctr_i   <= slctr_1i   when slctr_1i = '1' else
               slctr_2i   when slctr_2i = '1' else 'Z';
  we_i      <= we_1i      when slctr_1i = '1' else
               we_2i      when slctr_2i = '1' else 'Z';
  ierr_sc_i <= ierr_sc_1i when slctr_1i = '1' else
               ierr_sc_2i when slctr_2i = '1' else 'Z';

  dut1 : entity work.slave(rtl_all)
    port map (
      clk      => clk_i,                -- in  Clock
      rstb     => rstb_i,               -- in  Async reset
      scl      => scl_ii,               -- in  Serial clock (max 5 MHz)
      sda_in   => sda_in_i,             -- in  Input serial data
      hadd     => HADD,                 -- in  Hardware address
      data_tx  => data_tx_i,            -- in  Data to output
      bc_rst   => bc_rst_i,             -- in  Command: Reset BC
      csr1_clr => csr1_clr_i,           -- in  Command: Clear CSR1
      sda_out  => sda_out_1i,           -- out Output serial data
      reg_add  => reg_add_1i,           -- out Register address
      data_rx  => data_rx_1i,           -- out Data read from bus
      slctr    => slctr_1i,             -- out I2C-slave take control
      we       => we_1i,                -- out Write enable
      ierr_sc  => ierr_sc_1i);          -- out Error : Instruction error

  dut2 : entity work.slave(rtl_all)
    port map (
      clk      => clk_i,                -- in  Clock
      rstb     => rstb_i,               -- in  Async reset
      scl      => scl_ii,               -- in  Serial clock (max 5 MHz)
      sda_in   => sda_in_i,             -- in  Input serial data
      hadd     => NOTUS,                -- in  Hardware address
      data_tx  => data_tx_i,            -- in  Data to output
      bc_rst   => bc_rst_i,             -- in  Command: Reset BC
      csr1_clr => csr1_clr_i,           -- in  Command: Clear CSR1
      sda_out  => sda_out_2i,           -- out Output serial data
      reg_add  => reg_add_2i,           -- out Register address
      data_rx  => data_rx_2i,           -- out Data read from bus
      slctr    => slctr_2i,             -- out I2C-slave take control
      we       => we_2i,                -- out Write enable
      ierr_sc  => ierr_sc_2i);          -- out Error : Instruction error

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli : process is
  begin  -- process stimuli
    data_tx_i <= (others => '0');
    res_i     <= (others => '0');
    wait for 100 ns;
    rstb_i    <= '1';

--    for i in 2 to 4 loop
--      wait until rising_edge(clk_i);
--      buf_i <= to_unsigned(i, 16);
--      send(hadd    => HADD,
--           addr    => i,
--           bcast   => false,
--           rw      => false,
--           idat    => buf_i,
--           odat    => res_i,
--           scl     => scl_i,
--           scl_en  => scl_en_i,
--           sda_in  => sda_out_i,
--           sda_out => sda_in_i,
--           state   => state_i);
--      wait until rising_edge(clk_i);
--      assert to_unsigned(i, 7) = unsigned(reg_add_i) report
--        "Instruction sent " & integer'image(i) & " and decoded " &
--        integer'image(to_integer(unsigned(reg_add_i))) & " mis-match"
--        severity warning;
--      assert buf_i = unsigned(data_rx_i) report "data send " &
--        integer'image(to_integer(buf_i)) & " and recieved mis-match " &
--        integer'image(to_integer(unsigned(data_rx_i))) severity warning;
--      wait until rising_edge(clk_i);
--    end loop;  -- i

    for i in 2 to 4 loop
      wait until rising_edge(clk_i);
      data_tx_i <= std_logic_vector(to_unsigned(i, 16));
      send(hadd    => HADD,
           addr    => i,
           bcast   => false,
           rw      => true,
           idat    => buf_i,
           odat    => res_i,
           scl     => scl_i,
           sda_in  => sda_out_i,
           sda_out => sda_in_i,
           scl_en  => scl_en_i,
           scl_inh => scl_inh_i,
           state   => state_i);
      wait until rising_edge(clk_i);
      assert to_unsigned(i, 7) = unsigned(reg_add_i) report
        "Instruction sent " & integer'image(i) & " and decoded " &
        integer'image(to_integer(unsigned(reg_add_i))) & " mis-match"
        severity warning;
      assert res_i = unsigned(data_tx_i) report "data returned " &
        integer'image(to_integer(res_i)) & " and transmitted mis-match " &
        integer'image(to_integer(unsigned(data_tx_i))) severity warning;
      wait until rising_edge(clk_i);
    end loop;  -- i

    for i in 2 to 4 loop
      wait until rising_edge(clk_i);
      data_tx_i <= std_logic_vector(to_unsigned(i, 16));
      send(hadd    => NOTUS,
           addr    => i,
           bcast   => false,
           rw      => true,
           idat    => buf_i,
           odat    => res_i,
           scl     => scl_i,
           sda_in  => sda_out_i,
           sda_out => sda_in_i,
           scl_en  => scl_en_i,
           scl_inh => scl_inh_i,
           state   => state_i);
      wait until rising_edge(clk_i);
      assert to_unsigned(i, 7) = unsigned(reg_add_i) report
        "Instruction sent " & integer'image(i) & " and decoded " &
        integer'image(to_integer(unsigned(reg_add_i))) & " mis-match"
        severity warning;
      assert res_i = unsigned(data_tx_i) report "data returned " &
        integer'image(to_integer(res_i)) & " and transmitted mis-match " &
        integer'image(to_integer(unsigned(data_tx_i))) severity warning;
      wait until rising_edge(clk_i);
    end loop;  -- i


    wait;                               -- forever
  end process stimuli;

  -- purpose: Make the serial clock
  -- type   : combinational
  -- inputs : 
  -- outputs: scl_i
  sclk_gen : process is
  begin  -- process sclk_gen
    scl_i <= not scl_i;
    wait for SPERIOD / 2;
  end process sclk_gen;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_gen : process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;
end architecture test;
------------------------------------------------------------------------------
-- 
-- EOF
--
