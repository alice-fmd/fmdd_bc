------------------------------------------------------------------------------
-- Title      : Test bench of slave reciever
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : slave_rx_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/20
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/19  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.slave_rx_pack.all;
use work.serializer_bc_pack.all;

------------------------------------------------------------------------------
entity slave_rx_tb is
end entity slave_rx_tb;

------------------------------------------------------------------------------

architecture test of slave_rx_tb is
  constant PERIOD  : time := 25 ns;
  constant SPERIOD : time := 8 * PERIOD;  -- Must be at least 200 ns = 5MHz


  signal clk_i         : std_logic := '0';  -- Clock
  signal rstb_i        : std_logic := '0';  -- Async reset
  signal bc_rst_i      : std_logic := '0';  -- Sync reset
  signal csr1_clr_i    : std_logic := '0';  -- Command: Clear CSR1
  signal cnt_8_i       : std_logic;     -- Got 1 byte
  signal en_fec_rx_i   : std_logic := '0';  -- Start
  signal data_par_in_i : std_logic_vector(7 downto 0);  -- In data
  signal sda_in_i      : std_logic := '0';  -- Serial input data
  signal scl_i         : std_logic := '0';  -- Serial clock
  signal clear_i       : std_logic;     -- Clear shift register
  signal clear_ii      : std_logic;     -- Clear shift register
  signal enable_i      : std_logic;     -- Enable shift
  signal enable_ii     : std_logic;     -- Enable shift
  signal data_i        : std_logic_vector(15 downto 0);  -- output data
  signal reg_add_i     : std_logic_vector(6 downto 0);  -- Register address
  signal sda_out_i     : std_logic;     -- Serial data out
  signal sel_rx_i      : std_logic;     -- Select reciever
  signal finish_rx_i   : std_logic;     -- Reciever done
  signal we_i          : std_logic;     -- Write enable
  signal ierr_rx_i     : std_logic;     -- Error : instruction error
  signal i             : integer;
  signal scl_en_i      : std_logic := '0';
  signal scl_ii        : std_logic;

  -- purpose: Send a command
  procedure send (
    constant addr      : in  integer;    -- Address
    signal   clk       : in  std_logic;  -- Clock
    signal   scl       : in  std_logic;  -- Clock
    signal   scl_en    : out std_logic;  -- Enable scl
    signal   sda       : out std_logic;  -- Data
    signal   en_fec_rx : out std_logic)  -- Start
  is
    variable i         :     integer;    -- counter
    variable addr_i    :     unsigned(7 downto 0)  := to_unsigned(addr, 8);
    variable data_i    :     unsigned(15 downto 0) := to_unsigned(addr, 16);
  begin  -- procedure send
    wait until rising_edge(clk);
    en_fec_rx <= '1';
    wait until rising_edge(clk);
    en_fec_rx <= '0';
    wait until falling_edge(scl);
    wait for PERIOD / 2;
    scl_en    <= '1';
    for i in 7 downto 0 loop
      wait for SPERIOD / 4;
      sda     <= addr_i(i);
      wait until rising_edge(scl);
      wait until falling_edge(scl);
      wait for SPERIOD / 4;
    end loop;  -- i
    -- Now, we expect and acknowledge from slave_tx
    -- Wait until the next rising edge
    wait until rising_edge(scl);
    sda <= '1';
    wait until falling_edge(scl);
    wait for SPERIOD / 4;
    
    -- Next, we should put 8 bits and get acknowledge once
    for i in 15 downto 8 loop
      sda <= data_i(i);
      wait until rising_edge(scl);
      wait until falling_edge(scl);
      wait for SPERIOD / 4;
    end loop;  -- i
    sda   <= '1';                       -- Acknowledge
    wait until rising_edge(scl);
    wait until falling_edge(scl);
    wait for SPERIOD / 4;

    -- Next, we should get 8 bits and not acknowledge once
    for i in 7 downto 0 loop
      sda <= data_i(i);
      wait until rising_edge(scl);
      wait until falling_edge(scl);
      wait for SPERIOD / 4;
    end loop;  -- i
    sda    <= '0';
    wait until rising_edge(scl);
    wait until falling_edge(scl);
    wait for SPERIOD / 4;
    sda    <= '0';
    wait for SPERIOD / 4;
    wait until rising_edge(scl);
    wait for SPERIOD / 4;
    sda    <= '1';                      -- Stop condition
    wait for SPERIOD / 4;
    -- wait until rising_edge(scl);
    -- wait until falling_edge(scl);
    scl_en <= '0';

  end procedure send;
  
begin  -- architecture test
  scl_ii <= scl_i and scl_en_i;

  serial : entity work.serializer_bc(rtl)
    port map (
        clk      => clk_i,              -- in  clock
        rstb     => rstb_i,             -- in  Async reset
        data     => X"00",              -- in  Data from BC
        shiftin  => sda_in_i,           -- in  Bit to shift in (from right)
        enable   => enable_ii,          -- in  Enable shift-in and load
        load     => '0',                -- in  Load a byte into shift register
        clear    => clear_ii,           -- in  Sync reset.
        cnt_8    => cnt_8_i,            -- out Got 8 shifts
        shiftout => open,               -- out Bit shifted out (from left)
        q        => data_par_in_i);     -- out Data from RCU

  dut : entity work.slave_rx
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        bc_rst      => bc_rst_i,        -- in  Sync reset
        csr1_clr    => csr1_clr_i,      -- in  Command: Clear CSR1
        cnt_8       => cnt_8_i,         -- in  Got 1 byte
        en_fec_rx   => en_fec_rx_i,     -- in  Start
        data_par_in => data_par_in_i,   -- in  In data
        sda_in      => sda_in_i,        -- in  Serial input data
        scl         => scl_ii,          -- in  Serial clock
        clear       => clear_i,         -- out Clear shift register
        enable      => enable_i,        -- out Enable shift
        data        => data_i,          -- out output data
        reg_add     => reg_add_i,       -- out Register address
        sda_out     => sda_out_i,       -- out Serial data out
        sel_rx      => sel_rx_i,        -- out Select reciever
        finish_rx   => finish_rx_i,     -- out Reciever done
        we          => we_i,            -- out Write enable
        ierr_rx     => ierr_rx_i);      -- out Error : instruction error

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';

    for i in 2 to 6 loop
      send(addr      => i,
           clk       => clk_i,
           scl       => scl_i,
           scl_en    => scl_en_i,
           sda       => sda_in_i,
           en_fec_rx => en_fec_rx_i);
      -- wait until finish_rx_i = '1';
      -- wait until sel_rx_i = '0';
      wait until rising_edge(clk_i);
      wait until rising_edge(clk_i);
    end loop;  -- i

    wait;                               -- forever
  end process stimuli;

  -- purpose: Make the serial clock
  -- type   : combinational
  -- inputs : 
  -- outputs: scl_i
  sclk_gen: process is
  begin  -- process sclk_gen
    scl_i <= not scl_i;
    wait for SPERIOD / 2;
  end process sclk_gen;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_gen: process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;

  -- purpose: Syncronize some signals 
  sync_en : process (clk_i, rstb_i) is
  begin  -- process sync_en
    if rstb_i = '0' then                  -- asynchronous reset (active low)
      enable_ii   <= '0';
      clear_ii    <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if sel_rx_i = '1' then
        enable_ii <= enable_i;
        clear_ii  <= clear_i;
      else
        enable_ii <= '0';
        clear_ii  <= '0';
      end if;
    end if;
  end process sync_en;
  

end architecture test;
------------------------------------------------------------------------------
-- 
-- EOF
--
