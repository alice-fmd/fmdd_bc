------------------------------------------------------------------------------
-- Title      : Receive data via I2C
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : slave_rx.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-08-07
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/19  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.register_config.all;

entity slave_rx is
  port (
    clk         : in  std_logic;        -- Clock
    rstb        : in  std_logic;        -- Async reset
    bc_rst      : in  std_logic;        -- Sync reset
    csr1_clr    : in  std_logic;        -- Command: Clear CSR1
    cnt_8       : in  std_logic;        -- Got 1 byte
    en_fec_rx   : in  std_logic;        -- Start
    data_par_in : in  std_logic_vector(7 downto 0);   -- In data
    sda_in      : in  std_logic;        -- Serial input data
    scl         : in  std_logic;        -- Serial clock
    clear       : out std_logic;        -- Clear shift register
    enable      : out std_logic;        -- Enable shift
    data        : out std_logic_vector(15 downto 0);  -- output data
    reg_add     : out std_logic_vector(6 downto 0);   -- Register address
    sda_out     : out std_logic;        -- Serial data out
    sel_rx      : out std_logic;        -- Select reciever
    finish_rx   : out std_logic;        -- Reciever done
    we          : out std_logic;        -- Write enable
    ierr_rx     : out std_logic;        -- Error : instruction error
    state       : out std_logic_vector(2 downto 0));  -- State
end entity slave_rx;

architecture rtl of slave_rx is
  type state_t is (idle,
                   start,
                   wait_dt,
                   store_dt,
                   ack_1,
                   ack_2,
                   wait_stop);          -- States
  constant ST_IDLE      : std_logic_vector(2 downto 0) := "000";
  constant ST_START     : std_logic_vector(2 downto 0) := "001";
  constant ST_WAIT_DT   : std_logic_vector(2 downto 0) := "010";
  constant ST_STORE_DT  : std_logic_vector(2 downto 0) := "011";
  constant ST_ACK_1     : std_logic_vector(2 downto 0) := "100";
  constant ST_ACK_2     : std_logic_vector(2 downto 0) := "101";
  constant ST_WAIT_STOP : std_logic_vector(2 downto 0) := "110";
  
  signal st_i      : state_t;               -- State
  signal nx_st_i   : state_t;               -- Next state
  signal ierr_rx_i : std_logic;             -- Error
  signal clr_cnt_i : std_logic;
  signal en_cnt_i  : std_logic;
  signal en_reg_i  : std_logic;
  signal cnt_i     : unsigned(1 downto 0) := (others => '0');  -- Counter
  signal reg_add_i : integer range 0 to NUM_REG - 1 := 0;
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi            : block is
  begin  -- block combi
    ierr_rx   <= ierr_rx_i;
    ierr_rx_i <= '0' when regs(reg_add_i).sc and regs(reg_add_i).w else '1';
    reg_add   <= std_logic_vector(to_unsigned(reg_add_i, ADD_BITS));
    sel_rx    <= '0' when st_i = idle                              else '1';
    -- state     <= std_logic_vector(to_unsigned(state_t'pos(st_i), 3));
  end block combi;

  -- purpose: Update the state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st_i
  -- outputs: st_i
  update_state : process (clk, rstb) is
  begin  -- process update_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process update_state;

  -- purpose: final state machine to deal with recieving via I2C
  -- type   : combinational
  -- inputs : st_i, scl, sda_in, en_fec_rx, cnt_8, ierr_rx_i
  -- outputs: clr_cnt_i, en_cnt_i, enable, clear, finish_rx, sda_out, we
  rx_fsm : process (st_i, scl, sda_in, en_fec_rx, cnt_8, ierr_rx_i) is
  begin  -- process rx_fsm
    case st_i is

      when idle =>
        clear     <= '0';
        enable    <= '0';
        we        <= '0';
        sda_out   <= '1';
        finish_rx <= '0';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        state <= ST_IDLE;
        if en_fec_rx = '1' then
          nx_st_i <= start;
        else
          nx_st_i <= idle;
        end if;

      when start =>                     -- Start of recieving
        clear      <= '0';
        enable     <= '0';
        we         <= '0';
        sda_out    <= '1';
        finish_rx  <= '0';
        clr_cnt_i  <= '0';
        en_reg_i   <= '0';
        state <= ST_START;
        if scl = '0' then
          en_cnt_i <= '1';              -- Increment counter
          nx_st_i  <= wait_dt;
        else                            -- wait for falling edge
          en_cnt_i <= '0';
          nx_st_i  <= start;
        end if;

      when wait_dt =>
        clear     <= '0';
        enable    <= '0';
        we        <= '0';
        sda_out   <= '1';
        finish_rx <= '0';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        state <= ST_WAIT_DT;
        if scl = '1' then
          nx_st_i <= store_dt;          -- Rising edge
        else
          nx_st_i <= wait_dt;           -- Wait for rising edge
        end if;

      when store_dt =>
        enable     <= '1';                    -- shift in one bit
        we         <= '0';
        sda_out    <= '1';
        finish_rx  <= '0';
        clr_cnt_i  <= '0';
        en_cnt_i   <= '0';
        state <= ST_STORE_DT;
        if scl = '0' and cnt_8 = '1' then     -- Falling edge, 1 byte
          clear    <= '1';                    -- Clear counter
          en_reg_i <= '1';                    -- Output data
          nx_st_i  <= ack_1;
        elsif scl = '0' and cnt_8 = '0' then  -- Falling edge, more
          clear    <= '0';
          en_reg_i <= '0';
          nx_st_i  <= wait_dt;
        else                                  -- wait for falling edge
          clear    <= '0';
          en_reg_i <= '0';
          nx_st_i  <= store_dt;
        end if;
        clear      <= '0';
        en_reg_i   <= '0';

      when ack_1 =>
        clear       <= '1';
        enable      <= '0';
        clr_cnt_i   <= '0';
        en_cnt_i    <= '0';
        en_reg_i    <= '1';
        state <= ST_ACK_1;
        if ierr_rx_i = '1' then
          sda_out   <= '1';             -- Do not acknowledge error
          we        <= '0';
          finish_rx <= '0';
          nx_st_i   <= idle;
        elsif scl = '0' then
          sda_out   <= '0';             -- Acknowledge
          finish_rx <= '0';
          we        <= '0';
          nx_st_i   <= ack_1;           -- Wait for up edge
        elsif scl = '1' and sda_in = '1' then  -- Rcu will send more
          sda_out   <= '0';             -- Acknowledge
          finish_rx <= '0';
          we        <= '0';
          nx_st_i   <= ack_2;
        elsif scl = '1' and sda_in = '0' then  -- No more data
          sda_out   <= '0';             -- Acknowledge
          finish_rx <= '0';
          we        <= '1';
          nx_st_i   <= wait_stop;
        end if;
-- Old code starting at first elsif
--         else
--           sda_out <= '0';                      -- Acknowledge
--           finish_rx <= '0';
--           if scl = '0' then
--             we      <= '0';
--             nx_st_i <= ack_1;        -- Wait for up edge
--           elsif scl = '1' and sda_in = '1' then  -- Rcu will send more
--             we      <= '0';
--             nx_st_i <= ack_2;
--           elsif scl = '1' and sda_in = '0' then  -- No more data
--             we      <= '1';
--             nx_st_i <= wait_stop;
--           end if;
--         end if;

      when ack_2 =>
        clear      <= '0';
        enable     <= '0';
        we         <= '0';
        sda_out    <= '0';
        finish_rx  <= '0';
        clr_cnt_i  <= '0';
        en_reg_i   <= '0';
        state <= ST_ACK_2;
        if scl = '0' then
          en_cnt_i <= '1';
          nx_st_i  <= wait_dt;          -- Get more data 
        else
          en_cnt_i <= '0';
          nx_st_i  <= ack_2;            -- Wait for falling edge
        end if;

      when wait_stop =>
        clear       <= '0';
        enable      <= '0';
        we          <= '0';
        sda_out     <= '0';
        clr_cnt_i   <= '1';
        en_cnt_i    <= '0';
        en_reg_i    <= '0';
        state <= ST_WAIT_STOP;
        if sda_in = '1' and scl = '1' then
          finish_rx <= '1';
          nx_st_i   <= idle;            -- Stop condition
        else
          finish_rx <= '0';
          nx_st_i   <= wait_stop;       -- Wait for stop condition
        end if;

      when others =>
        clear     <= '0';
        enable    <= '0';
        we        <= '0';
        sda_out   <= '1';
        finish_rx <= '0';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        en_reg_i  <= '0';
        nx_st_i   <= idle;
        state     <= ST_IDLE;
    end case;
  end process rx_fsm;

  -- purpose: counter
  -- type   : sequential
  -- inputs : clk, rstb, en_cnt_i, clr_cnt_i
  -- outputs: cnt_i
  counter : process (clk, rstb) is
  begin  -- process counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i   <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clr_cnt_i = '1' then
        cnt_i <= (others => '0');
      elsif en_cnt_i = '1' then
        cnt_i <= cnt_i + 1;
      end if;
    end if;
  end process counter;

  -- purpose: output data and address
  -- type   : sequential
  -- inputs : clk, rstb, cnt_i, bc_rst, csr1_clr, data_par_in
  -- outputs: data, reg_add
  output : process (clk, rstb) is
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      data                  <= (others => '0');
      reg_add_i             <= 0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' or csr1_clr = '1' then
        data                <= (others => '0');
        reg_add_i           <= 0;
      elsif en_reg_i = '1' then
        if cnt_i = "01" then
          reg_add_i         <= to_integer(unsigned(data_par_in(6 downto 0)));
        elsif cnt_i = "10" then
          data(15 downto 8) <= data_par_in;
        elsif cnt_i = "11" then
          data(7 downto 0)  <= data_par_in;
        end if;
      end if;
    end if;
  end process output;
end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package slave_rx_pack is
  component slave_rx
    port (
      clk         : in  std_logic;      -- Clock
      rstb        : in  std_logic;      -- Async reset
      bc_rst      : in  std_logic;      -- Sync reset
      csr1_clr    : in  std_logic;      -- Command: Clear CSR1
      cnt_8       : in  std_logic;      -- Got 1 byte
      en_fec_rx   : in  std_logic;      -- Start
      data_par_in : in  std_logic_vector(7 downto 0);   -- In data
      sda_in      : in  std_logic;      -- Serial input data
      scl         : in  std_logic;      -- Serial clock
      clear       : out std_logic;      -- Clear shift register
      enable      : out std_logic;      -- Enable shift
      data        : out std_logic_vector(15 downto 0);  -- output data
      reg_add     : out std_logic_vector(6 downto 0);   -- Register address
      sda_out     : out std_logic;      -- Serial data out
      sel_rx      : out std_logic;      -- Select reciever
      finish_rx   : out std_logic;      -- Reciever done
      we          : out std_logic;      -- Write enable
      ierr_rx     : out std_logic;      -- Error : instruction error
      state       : out std_logic_vector(2 downto 0));  -- State 
  end component slave_rx;
end package slave_rx_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
