-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package i2c_simul_pack is
  constant SPERIOD : time := 8 * 25 ns;  -- PERIOD;

  -- State type for I2C debuggging
  type i2c_state_t is (start,
                       tx,
                       rx,
                       wait_ack,
                       ack,
                       send_ack,
                       no_ack,
                       stop);
  -- purpose: Make a start condition
  procedure start_condition (
    signal scl     : in  std_logic;     -- Clock
    signal scl_en  : out std_logic;     -- scl enable
    signal sda_out : out std_logic;     -- Output data
    signal state   : out i2c_state_t);

  -- purpose: Make a stop condition
  procedure stop_condition (
    signal scl     : in  std_logic;     -- Clock
    signal sda_out : out std_logic;     -- Output
    signal scl_inh : out std_logic;     -- Clock inhibit
    signal state   : out i2c_state_t);

  -- purpose: Send 1 byte via I2C
  procedure put_8 (
    constant data    : in  unsigned(7 downto 0);  -- Data to send
    constant last    : in  std_logic;             -- Wether this is the last
    signal   scl     : in  std_logic;             -- Clock
    signal   sda_in  : in  std_logic;             -- Input
    signal   sda_out : out std_logic;             -- Output
    signal   scl_inh : out std_logic;             -- Clock inhibit
    signal   state   : out i2c_state_t);

  -- purpose: Read one byte from I2C bus
  procedure get_8 (
    constant last    : in  std_logic;             -- Wether this is the last
    signal   scl     : in  std_logic;             -- Clock
    signal   sda_in  : in  std_logic;             -- Input data
    signal   sda_out : out std_logic;             -- Output data
    signal   scl_inh : out std_logic;             -- Clock inhibit
    signal   data    : out unsigned(7 downto 0);  -- Result
    signal   state   : out i2c_state_t);

  -- purpose: Send a command.
  procedure send (
    constant hadd    : in  std_logic_vector(4 downto 0);  -- Hardware address
    constant addr    : in  integer;                       -- Instruction code
    constant bcast   : in  boolean;                       -- Broadcast
    constant rw      : in  boolean;                       -- Read, not write
    signal   idat    : in  unsigned(15 downto 0);         -- Data
    signal   odat    : out unsigned(15 downto 0);         -- Data
    signal   scl     : in  std_logic;                     -- Clock
    signal   sda_in  : in  std_logic;                     -- Serial data input
    signal   sda_out : out std_logic;                     -- Serial data output
    signal   scl_en  : out std_logic;                     -- scl enable
    signal   scl_inh : out std_logic;                     -- scl inhibit
    signal   state   : out i2c_state_t);
end i2c_simul_pack;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package body i2c_simul_pack is
  -- purpose: Make a start condition
  procedure start_condition (
    signal scl     : in  std_logic;       -- Clock
    signal scl_en  : out std_logic;       -- scl enable
    signal sda_out : out std_logic;
    signal state   : out i2c_state_t) is  -- Output data
  begin  -- procedure start_condition
    state   <= start;
    wait until rising_edge(scl);
    sda_out <= '1';
    scl_en  <= '1';
    wait for SPERIOD / 4;                 -- Take down sda while scl high
    -- report "Start condition" severity note;
    sda_out <= '0';
    wait until falling_edge(scl);
  end procedure start_condition;

  -- purpose: Make a stop condition
  procedure stop_condition (
    signal scl     : in  std_logic;       -- Clock
    signal sda_out : out std_logic;
    signal scl_inh : out std_logic;       -- Clock enable
    signal state   : out i2c_state_t) is  -- Output
  begin  -- procedure stop_condition
    if scl = '1' then
      -- Wait for a falling edge, if we're at high
      wait until falling_edge(scl);
    end if;
    state   <= stop;
    wait for SPERIOD / 4;                 -- Take down sda while scl low
    scl_inh <= '0';
    sda_out <= '0';
    wait until rising_edge(scl);
    wait for SPERIOD / 4;                 -- Take up sda while scl high
    -- report "Stop condition" severity note;
    sda_out <= '1';                       -- Stop condition
    wait for SPERIOD / 4;                 -- Wait a bit
  end procedure stop_condition;

  -- purpose: Send 1 byte via I2C
  procedure put_8 (
    constant data    : in  unsigned(7 downto 0);  -- Data to send
    constant last    : in  std_logic;             -- Wether this is the last
    signal   scl     : in  std_logic;             -- Clock
    signal   sda_in  : in  std_logic;             -- Input
    signal   sda_out : out std_logic;
    signal   scl_inh : out std_logic;
    signal   state   : out i2c_state_t)           -- Output
  is
    variable i : integer := 0;                    -- counter
  begin  -- procedure send_8
    -- We assume we're on a falling edge
    state <= tx;
    for i in 7 downto 0 loop
      wait for SPERIOD / 4;
      -- report "put bit # " & integer'image(i) & "=" &
      --   std_logic'image(data(i)) severity note;
      sda_out <= data(i);
      wait until rising_edge(scl);
      wait until falling_edge(scl);
      wait for SPERIOD / 4;
    end loop;  -- i
    state <= wait_ack;
    if last = '0' then
      sda_out <= '1';
      wait until rising_edge(scl);
      assert sda_in = '0' report "No acknowledge" severity warning;
      wait until falling_edge(scl);
    else
      sda_out <= '0';
      wait until rising_edge(scl);
      assert sda_in = '0' report "No acknowledge on stop" severity warning;
      stop_condition(scl     => scl,
                     sda_out => sda_out,
                     scl_inh => scl_inh,          -- ignored for put
                     state   => state);
    end if;
  end procedure put_8;

  -- purpose: Read one byte from I2C bus
  procedure get_8 (
    constant last    : in  std_logic;             -- Wether this is the last
    signal   scl     : in  std_logic;             -- in  Clock
    signal   sda_in  : in  std_logic;             -- in  Input data
    signal   sda_out : out std_logic;             -- out Output data
    signal   scl_inh : out std_logic;             -- out Clock enable
    signal   data    : out unsigned(7 downto 0);  -- out Result
    signal   state   : out i2c_state_t) is        -- out State 
    variable i : integer := 0;                    -- counter
  begin  -- procedure get_8
    -- We assume we're on a falling edge
    state <= rx;
    for i in 7 downto 0 loop
      wait until rising_edge(scl);
      wait for SPERIOD / 4;
      -- report "get bit # " & integer'image(i) & "=" &
      --   std_logic'image(sda_in) severity note;
      data(i) <= sda_in;
      wait until falling_edge(scl);
      wait for SPERIOD / 4;
    end loop;  -- i
    if last = '0' then
      state   <= send_ack;
      sda_out <= '0';                             -- Acknowledge
      scl_inh <= '1';
      wait until rising_edge(scl);
      wait until falling_edge(scl);
      scl_inh <= '0';
      wait for SPERIOD / 4;
      sda_out <= '1';
    else
      state   <= no_ack;
      sda_out <= '1';                             -- Not acknowledge
      scl_inh <= '1';
      wait until rising_edge(scl);
      stop_condition(scl     => scl,
                     sda_out => sda_out,
                     scl_inh => scl_inh,
                     state   => state);
    end if;
  end procedure get_8;

  -- purpose: Send a command.
  procedure send (
    constant hadd    : in  std_logic_vector(4 downto 0);  -- Hardware address
    constant addr    : in  integer;                       -- Instruction code
    constant bcast   : in  boolean;                       -- Broadcast
    constant rw      : in  boolean;                       -- Read, not write
    signal   idat    : in  unsigned(15 downto 0);         -- Data
    signal   odat    : out unsigned(15 downto 0);         -- Data
    signal   scl     : in  std_logic;                     -- Clock
    signal   sda_in  : in  std_logic;                     -- Serial data input
    signal   sda_out : out std_logic;                     -- Serial data output
    signal   scl_en  : out std_logic;                     -- scl enable
    signal   scl_inh : out std_logic;                     -- scl inhibit
    signal   state   : out i2c_state_t) is                -- State
    variable byte_1 : unsigned(7 downto 0);               -- Address board
    variable byte_2 : unsigned(7 downto 0);               -- Instruction
  begin  -- procedure send
    scl_inh <= '0';
    if bcast then
      byte_1 := '1' & '1' & "00000" & '0';
    else
      if rw then
        byte_1 := '1' & '0' & unsigned(hadd) & '1';
      else
        byte_1 := '1' & '0' & unsigned(hadd) & '0';
      end if;
    end if;
    byte_2 := '0' & to_unsigned(addr, 7);

    start_condition(scl     => scl,
                    scl_en  => scl_en,
                    sda_out => sda_out,
                    state   => state);

    put_8(data    => byte_1,
          last    => '0',
          scl     => scl,
          sda_in  => sda_in,
          sda_out => sda_out,
          scl_inh => scl_inh,
          state   => state);
    put_8(data    => byte_2,
          last    => '0',
          scl     => scl,
          sda_in  => sda_in,
          sda_out => sda_out,
          scl_inh => scl_inh,
          state   => state);

    if rw then
      odat <= (others => '0');
      get_8(data    => odat(15 downto 8),
            last    => '0',
            scl     => scl,
            sda_in  => sda_in,
            sda_out => sda_out,
            scl_inh => scl_inh,
            state   => state);
      get_8(data    => odat(7 downto 0),
            last    => '1',
            scl     => scl,
            sda_in  => sda_in,
            sda_out => sda_out,
            scl_inh => scl_inh,
            state   => state);
    else
      put_8(data    => idat(15 downto 8),
            last    => '0',
            scl     => scl,
            sda_in  => sda_in,
            sda_out => sda_out,
            scl_inh => scl_inh,
            state   => state);
      put_8(data    => idat(7 downto 0),
            last    => '1',
            scl     => scl,
            sda_in  => sda_in,
            sda_out => sda_out,
            scl_inh => scl_inh,
            state   => state);
    end if;
    scl_inh <= '0';
    scl_en  <= '0';
  end procedure send;
end i2c_simul_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
