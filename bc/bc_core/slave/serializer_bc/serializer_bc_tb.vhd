------------------------------------------------------------------------------
-- Title      : Test bench of serializer_bc
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : serializer_bc_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/29
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/19  1.0      cholm	Created
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.serializer_bc_pack.all;

------------------------------------------------------------------------------
entity serializer_bc_tb is
end entity serializer_bc_tb;

------------------------------------------------------------------------------
architecture test of serializer_bc_tb is
  constant PERIOD : time := 25 ns;

  signal clk_i      : std_logic := '0';  -- clock
  signal rstb_i     : std_logic := '0';  -- Async reset
  signal data_i     : std_logic_vector(7 downto 0);  -- Data from BC
  signal shiftin_i  : std_logic := '0';  -- Bit to shift in (from right)
  signal enable_i   : std_logic := '0';  -- Enable shift-in and load
  signal load_i     : std_logic := '0';  -- Load a byte into shift register
  signal clear_i    : std_logic := '0';  -- Sync reset.
  signal cnt_8_i    : std_logic;        -- Got 8 shifts
  signal shiftout_i : std_logic;        -- Bit shifted out (from left)
  signal q_i        : std_logic_vector(7 downto 0);  -- Data from RCU
  signal i          : integer;
begin  -- architecture test

  dut: entity work.serializer_bc(rtl)
    port map (
        clk      => clk_i,              -- in  clock
        rstb     => rstb_i,             -- in  Async reset
        data     => data_i,             -- in  Data from BC
        shiftin  => shiftin_i,          -- in  Bit to shift in (from right)
        enable   => enable_i,           -- in  Enable shift-in and load
        load     => load_i,             -- in  Load a byte into shift register
        clear    => clear_i,            -- in  Sync reset.
        cnt_8    => cnt_8_i,            -- out Got 8 shifts
        shiftout => shiftout_i,         -- out Bit shifted out (from left)
        q        => q_i);               -- out Data from RCU

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    data_i <= (others => '0');
    wait for 100 ns;
    rstb_i <= '1';
    wait for 100 ns;

    data_i   <= X"FF";
    wait until rising_edge(clk_i);
    enable_i <= '1';
    load_i   <= '1';
    wait until rising_edge(clk_i);
    enable_i <= '0';
    load_i   <= '0';

    for i in 0 to 7 loop
      shiftin_i <= std_logic(to_unsigned(i mod 2, 1)(0));
      wait until rising_edge(clk_i);       
      enable_i  <= '1';
      wait until rising_edge(clk_i);
      enable_i  <= '0';
    end loop;  -- i

    wait until rising_edge(clk_i);
    clear_i  <= '1';
    wait until rising_edge(clk_i);
    clear_i  <= '0';

    for i in 0 to 7 loop
      shiftin_i <= std_logic(to_unsigned(i mod 2, 1)(0));
      wait until rising_edge(clk_i);       
      enable_i  <= '1';
      wait until rising_edge(clk_i);
      enable_i  <= '0';
    end loop;  -- i

    wait;                               -- forever
  end process stimuli;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_gen: process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;
end architecture test;

------------------------------------------------------------------------------
-- 
-- EOF
--
