------------------------------------------------------------------------------
-- Title      : Serialize parallel data to I2C bus,
--              parallize serial data from I2C bus
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : serializer_bc.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/12
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This module takes care of serializing data from the BC to the
--              RCU transmitted over the I2C bus.  It also parallizes the
--              serial data from the RCU to the BC transmitted over the I2C
--              bus. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/19  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity serializer_bc is
  port (
    clk      : in  std_logic;           -- clock
    rstb     : in  std_logic;           -- Async reset
    data     : in  std_logic_vector(7 downto 0);  -- Data from BC
    shiftin  : in  std_logic;           -- Bit to shift in (from right)
    enable   : in  std_logic;           -- Enable shift-in and load
    load     : in  std_logic;           -- Load a byte into shift register
    clear    : in  std_logic;           -- Sync reset.
    cnt_8    : out std_logic;           -- Got 8 shifts
    shiftout : out std_logic;           -- Bit shifted out (from left)
    q        : out std_logic_vector(7 downto 0));  -- Data from RCU
end entity serializer_bc;

architecture rtl of serializer_bc is
  signal cnt_i    : unsigned(3 downto 0);          -- Counter
  signal reg_i    : std_logic_vector(7 downto 0);  -- Store
  -- signal en_i  : std_logic;
  signal old_en_i : std_logic;
begin  -- architecture rtl

  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    -- cnt_8    <= '1' when rstb /= '0' and (cnt_i = "1000") else '0';
    cnt_8    <= '1' when (cnt_i = "1000") else '0';
    q        <= reg_i;
    shiftout <= reg_i(7);
  end block combi;

  -- purpose: Detect up edge on enable
  -- type   : sequential
  -- inputs : clk, rstb, enable, old_en_i
  -- outputs: en_i
  enable_edge : process (clk, rstb) is
  begin  -- process enable_edge
    if rstb = '0' then                  -- asynchronous reset (active low)
      old_en_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      old_en_i <= enable;
    end if;
  end process enable_edge;

  -- purpose: Count to 8
  -- type   : sequential
  -- inputs : clk, rstb, clear, enable
  -- outputs: cnt_i
  counter: process (clk, rstb) is
  begin  -- process counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clear = '1' then               -- This should really be async. 
        cnt_i <= (others => '0');
      elsif enable = '1' and old_en_i = '0' then  -- Rising edge
        cnt_i <= cnt_i + 1;
      end if;
    end if;
  end process counter;

  -- purpose: Shift register
  -- type   : sequential
  -- inputs : clk, rstb, enable, load
  -- outputs: data_i
  shift_reg: process (clk, rstb)
    variable tmp_i : std_logic_vector(7 downto 0);
  begin  -- process shift_reg
    if rstb = '0' then                  -- asynchronous reset (active low)
      reg_i <= (others => '0');
      tmp_i := (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if enable = '1'  and old_en_i = '0' then   -- Rising edge
        if load = '1' then
          tmp_i := data;
        else
          tmp_i := tmp_i(6 downto 0) & shiftin;
        end if;
        reg_i <= tmp_i;
      end if;
    end if;
  end process shift_reg;
end architecture rtl;

------------------------------------------------------------------------------
--
-- This implementation is ugly, in that it uses the enable signal as a clock.
-- This will probably mean, that the timing constraints won't hold due to slow
-- set up time. The above implementation is equivilant but does not have this
-- problem. 
-- 
architecture rtl2 of serializer_bc is
  signal cnt_i : unsigned(3 downto 0);          -- Counter
  signal reg_i : std_logic_vector(7 downto 0);  -- Store
  signal clk_i : std_logic;
begin  -- architecture rtl

  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    cnt_8    <= '1' when rstb /= '0' and (cnt_i = "1000") else '0';
    q        <= reg_i;
    shiftout <= reg_i(7);
  end block combi;

  -- purpose: Detect up edge on enable
  -- type   : sequential
  -- inputs : clk, rstb, enable, old_en_i
  -- outputs: en_i
  enable_edge : process (clk, rstb) is
  begin  -- process enable_edge
    if rstb = '0' then                  -- asynchronous reset (active low)
      clk_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      clk_i <= enable;
    end if;
  end process enable_edge;

  -- purpose: Count to 8
  -- type   : sequential
  -- inputs : clk, rstb, clear, enable
  -- outputs: cnt_i
  counter: process (clk_i, rstb, clear) is
  begin  -- process counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i <= (others => '0');
    elsif clear'event and clear = '1' then
      cnt_i <= (others => '0');      
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      cnt_i <= cnt_i + 1;
    end if;
  end process counter;

  -- purpose: Shift register
  -- type   : sequential
  -- inputs : clk, rstb, enable, load
  -- outputs: data_i
  shift_reg: process (clk_i, rstb)
    variable tmp_i : std_logic_vector(7 downto 0);
  begin  -- process shift_reg
    if rstb = '0' then                  -- asynchronous reset (active low)
      reg_i <= (others => '0');
      tmp_i := (others => '0');
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if load = '1' then
        tmp_i := data;
      else
        tmp_i := tmp_i(6 downto 0) & shiftin;
      end if;
      reg_i <= tmp_i;
    end if;
  end process shift_reg;
end architecture rtl2;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package serializer_bc_pack is
  component serializer_bc
    port (
      clk      : in  std_logic;         -- clock
      rstb     : in  std_logic;         -- Async reset
      data     : in  std_logic_vector(7 downto 0);  -- Data from BC
      shiftin  : in  std_logic;         -- Bit to shift in (from right)
      enable   : in  std_logic;         -- Enable shift-in and load
      load     : in  std_logic;         -- Load a byte into shift register
      clear    : in  std_logic;         -- Sync reset.
      cnt_8    : out std_logic;         -- Got 8 shifts
      shiftout : out std_logic;         -- Bit shifted out (from left)
      q        : out std_logic_vector(7 downto 0));  -- Data from RCU
  end component serializer_bc;
end package serializer_bc_pack;

------------------------------------------------------------------------------
-- 
-- EOF
--
