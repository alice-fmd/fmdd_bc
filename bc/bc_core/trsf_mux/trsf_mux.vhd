------------------------------------------------------------------------------
-- Title      : Select transfer output
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : trsf_mux.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Select between TSM and R transfer data
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/15  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity trsf_mux is
  port (
    mtrsf_en   : in  std_logic;         -- TSM: transfer enable
    mtrsf      : in  std_logic;         -- TSM: Transfer
    mdstb      : in  std_logic;         -- TSM: Data strobe
    mdata_out  : in  std_logic_vector(39 downto 0);  -- TSM: data
    rtrsf_en   : in  std_logic;         -- RO: Transfer enable
    rtrsf      : in  std_logic;         -- RO: Transfer
    rdstb      : in  std_logic;         -- RO: Data strobe
    rdata_out  : in  std_logic_vector(39 downto 0);  -- RO: data
    rmtrsf_en  : out std_logic;         -- TSM/RO: transfer enable
    rmtrsf     : out std_logic;
    rmdstb     : out std_logic;         -- TSM/RO: Data strobe
    rmdata_out : out std_logic_vector(39 downto 0));  -- TSM/RO : data
end entity trsf_mux;

architecture rtl of trsf_mux is
begin  -- architecture rtl
  -- purpose: Select output
  mux : block is
  begin  -- block mux
    rmtrsf_en  <= mtrsf_en  when mtrsf_en = '1' else rtrsf_en;
    rmtrsf     <= mtrsf     when mtrsf_en = '1' else rtrsf;
    rmdstb     <= mdstb     when mtrsf_en = '1' else rdstb;
    rmdata_out <= mdata_out when mtrsf_en = '1' else rdata_out;
  end block mux;
end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package trsf_mux_pack is
  component trsf_mux
    port (
      mtrsf_en   : in  std_logic;       -- TSM: transfer enable
      mtrsf      : in  std_logic;       -- TSM: Transfer
      mdstb      : in  std_logic;       -- TSM: Data strobe
      mdata_out  : in  std_logic_vector(39 downto 0);  -- TSM: data
      rtrsf_en   : in  std_logic;       -- RO: Transfer enable
      rtrsf      : in  std_logic;       -- RO: Transfer
      rdstb      : in  std_logic;       -- RO: Data strobe
      rdata_out  : in  std_logic_vector(39 downto 0);  -- RO: data
      rmtrsf_en  : out std_logic;       -- TSM/RO: transfer enable
      rmtrsf     : out std_logic;
      rmdstb     : out std_logic;       -- TSM/RO: Data strobe
      rmdata_out : out std_logic_vector(39 downto 0));  -- TSM/RO : data
  end component trsf_mux;
end package trsf_mux_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
