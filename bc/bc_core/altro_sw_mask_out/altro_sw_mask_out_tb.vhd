------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

------------------------------------------------------------------------------
entity altro_sw_mask_out_tb is
end entity altro_sw_mask_out_tb;

------------------------------------------------------------------------------
architecture test of altro_sw_mask_out_tb is
  signal altro_sw_i       : std_logic;  -- ALTRO switch
  signal csr2_i           : std_logic_vector(10 downto 0);  -- Config/Status 2
  signal tsm_out_i        : std_logic_vector(7 downto 0);  -- TSM decoded
  signal tsm_acqon_i      : std_logic;  -- TSM ACQ enable
  signal tsm_isol_i       : std_logic;  -- TSM card isolated
  signal rdoclk_en_i      : std_logic;  -- R/O clock enable
  signal adcclk_en_i      : std_logic;  -- ADC clock enable
  signal adc_add0_i       : std_logic;  -- ADC address 0
  signal adc_add1_i       : std_logic;  -- ADC address 1
  signal tsm_on_i         : std_logic;  -- Test mode on
  signal card_isolation_i : std_logic;  -- Card isolated
  signal test_a_i         : std_logic;  -- Test bit A
  signal test_b_i         : std_logic;  -- Test bit B
  signal test_c_i         : std_logic;  -- Test bit C
  signal test_d_i         : std_logic;  -- Test bit D
  signal test_e_i         : std_logic;  -- Test bit E
  signal test_f_i         : std_logic;  -- Test bit F
  signal test_g_i         : std_logic;  -- Test bit G
  signal test_h_i         : std_logic;  -- Test bit H
begin  -- architecture test
  dut: entity work.altro_sw_mask_out
    port map (
        altro_sw       => altro_sw_i,        -- in  ALTRO switch
        csr2           => csr2_i,            -- in  Config/Status 2
        tsm_out        => tsm_out_i,         -- in  TSM decoded
        tsm_acqon      => tsm_acqon_i,       -- in  TSM ACQ enable
        tsm_isol       => tsm_isol_i,        -- in  TSM card isolated
        rdoclk_en      => rdoclk_en_i,       -- out R/O clock enable
        adcclk_en      => adcclk_en_i,       -- out ADC clock enable
        adc_add0       => adc_add0_i,        -- out ADC address 0
        adc_add1       => adc_add1_i,        -- out ADC address 1
        tsm_on         => tsm_on_i,          -- out Test mode on
        card_isolation => card_isolation_i,  -- out Card isolated
        test_a         => test_a_i,          -- out Test bit A
        test_b         => test_b_i,          -- out Test bit B
        test_c         => test_c_i,          -- out Test bit C
        test_d         => test_d_i,          -- out Test bit D
        test_e         => test_e_i,          -- out Test bit E
        test_f         => test_f_i,          -- out Test bit F
        test_g         => test_g_i,          -- out Test bit G
        test_h         => test_h_i);         -- out Test bit H

  stimuli : process is
  begin  -- process stimuli
    wait for 100 ns;
    altro_sw_i  <= '1';
    csr2_i      <= (others => '0');
    tsm_out_i   <= (others => '0');
    tsm_acqon_i <= '0';
    tsm_isol_i  <= '0';
    wait for 1 us;
    altro_sw_i  <= '0';
    wait for 100 ns;    
    wait;                               -- forever
  end process stimuli;

end architecture test;

------------------------------------------------------------------------------
