------------------------------------------------------------------------------
-- Title      : ALTRO mask
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : altro_sw_mask_out.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2007/04/03
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Mask out signals when ALTRO's not turned on
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity altro_sw_mask_out is
  port (
    altro_sw       : in  std_logic;                      -- ALTRO switch
    csr2           : in  std_logic_vector(10 downto 0);  -- Config/Status 2
    tsm_out        : in  std_logic_vector(7 downto 0);   -- TSM decoded
    tsm_acqon      : in  std_logic;                      -- TSM ACQ enable
    tsm_isol       : in  std_logic;                      -- TSM card isolated
    rdoclk_en      : out std_logic;                      -- R/O clock enable
    adcclk_en      : out std_logic;                      -- ADC clock enable
    adc_add0       : out std_logic;                      -- ADC address 0
    adc_add1       : out std_logic;                      -- ADC address 1
    tsm_on         : out std_logic;                      -- Test mode on
    card_isolation : out std_logic;                      -- Card isolated
    test_a         : out std_logic;                      -- Test bit A
    test_b         : out std_logic;                      -- Test bit B
    test_c         : out std_logic;                      -- Test bit C
    test_d         : out std_logic;                      -- Test bit D
    test_e         : out std_logic;                      -- Test bit E
    test_f         : out std_logic;                      -- Test bit F
    test_g         : out std_logic;                      -- Test bit G
    test_h         : out std_logic);                     -- Test bit H
end altro_sw_mask_out;

------------------------------------------------------------------------------
architecture rtl of altro_sw_mask_out is
begin  -- rtl
  -- purpose: Combinatorics
  combi : block
  begin  -- block combi
    rdoclk_en      <= altro_sw and csr2(2);
    adcclk_en      <= altro_sw and csr2(3);
    adc_add0       <= altro_sw and csr2(4);
    adc_add1       <= altro_sw and csr2(5);
    tsm_on         <= altro_sw and (csr2(9) or tsm_acqon);
    card_isolation <= altro_sw and (csr2(10) or tsm_isol);
    test_a         <= altro_sw and not tsm_out(0);
    test_b         <= altro_sw and not tsm_out(1);
    test_c         <= altro_sw and not tsm_out(2);
    test_d         <= altro_sw and not tsm_out(3);
    test_e         <= altro_sw and not tsm_out(4);
    test_f         <= altro_sw and not tsm_out(5);
    test_g         <= altro_sw and not tsm_out(6);
    test_h         <= altro_sw and not tsm_out(7);
  end block combi;
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package altro_sw_mask_out_pack is
  component altro_sw_mask_out
    port (
      altro_sw       : in  std_logic;
      csr2           : in  std_logic_vector(10 downto 0);
      tsm_out        : in  std_logic_vector(7 downto 0);
      tsm_acqon      : in  std_logic;
      tsm_isol       : in  std_logic;
      rdoclk_en      : out std_logic;
      adcclk_en      : out std_logic;
      adc_add0       : out std_logic;
      adc_add1       : out std_logic;
      tsm_on         : out std_logic;
      card_isolation : out std_logic;
      test_a         : out std_logic;
      test_b         : out std_logic;
      test_c         : out std_logic;
      test_d         : out std_logic;
      test_e         : out std_logic;
      test_f         : out std_logic;
      test_g         : out std_logic;
      test_h         : out std_logic);
  end component;
end altro_sw_mask_out_pack;
------------------------------------------------------------------------------
--
-- EOF
--
