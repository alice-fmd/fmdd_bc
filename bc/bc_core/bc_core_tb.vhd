------------------------------------------------------------------------------
-- Title      : Test bench for the BC core
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : bc_core_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2010-02-16
-- Platform   : 
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test bench for the BC core
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/13  1.0      cholm	Created
------------------------------------------------------------------------------

------------------------------------------------------------------------------
--
-- Test bench for BC core
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.bc_core_pack.all;
library ad7417_model;
use ad7417_model.ad7417_pack.all;

------------------------------------------------------------------------------
entity bc_core_tb is
end bc_core_tb;

------------------------------------------------------------------------------

architecture test of bc_core_tb is
  constant PERIOD : time                         := 25 ns;  -- A clock cycle
  constant HADD   : std_logic_vector(4 downto 0) := "00000";  -- Address
  constant ADD    : std_logic_vector(2 downto 0) := "000";  -- ADC address
  constant VENDOR : std_logic_vector(3 downto 0) := "0101";  -- ADC vendor Id

  signal clk_i            : std_logic                     := '0';
  signal sclk_i           : std_logic                     := '1';
  signal rstb_i           : std_logic                     := '0';
  signal cstb_i           : std_logic                     := '0';
  signal write_i          : std_logic                     := '0';
  signal dstb_i           : std_logic                     := '0';
  signal trsf_i           : std_logic                     := '0';
  signal l1_trg_i         : std_logic                     := '0';
  signal l2_trg_i         : std_logic                     := '0';
  signal ackn_i           : std_logic                     := '0';
  signal din_i            : std_logic_vector(39 downto 0) := (others => '0');
  signal scl_i            : std_logic                     := '0';
  signal sda_in_i         : std_logic                     := '0';
  signal sda_out_i        : std_logic;
  signal paps_error_i     : std_logic                     := '0';
  signal alps_error_i     : std_logic                     := '0';
  signal al_err_i         : std_logic                     := '0';
  signal debug_sw_i       : std_logic                     := '1';
  signal bc_ackn_en_i     : std_logic;
  signal bc_ackn_i        : std_logic;
  signal lastst_al_i      : std_logic;
  signal endtrans_i       : std_logic;
  signal bc_dolo_en_i     : std_logic;
  signal bc_cs_i          : std_logic;
  signal al_cs_i          : std_logic;
  signal wr_al_i          : std_logic;
  signal dout_i           : std_logic_vector(15 downto 0);
  signal bc_int_i         : std_logic;
  signal bc_error_i       : std_logic;
  signal altro_sw_i       : std_logic;
  signal pasa_sw_i        : std_logic;
  signal al_rst_i         : std_logic;
  signal rdoclk_en_i      : std_logic;
  signal adcclk_en_i      : std_logic;
  signal adc_add0_i       : std_logic;
  signal adc_add1_i       : std_logic;
  signal tsm_on_i         : std_logic;
  signal card_isolation_i : std_logic;
  signal test_a_i         : std_logic;
  signal test_b_i         : std_logic;
  signal test_c_i         : std_logic;
  signal test_d_i         : std_logic;
  signal test_e_i         : std_logic;
  signal test_f_i         : std_logic;
  signal test_g_i         : std_logic;
  signal test_h_i         : std_logic;
  signal mscl_i           : std_logic;
  signal msda_i           : std_logic;
  signal rmtrsf_en_i      : std_logic;
  signal rmtrsf_i         : std_logic;
  signal rmdstb_i         : std_logic;
  signal rmdata_out_i     : std_logic_vector(39 downto 0);
  signal mem_wren_i       : std_logic;
  signal sclk_edge_i      : std_logic;
  signal evl_cstb_i       : std_logic;
  signal alevl_rdtrx_i    : std_logic;
  signal bc_master_i      : std_logic;
  signal evl_addr_i       : std_logic_vector(39 downto 20);
  signal l0_i             : std_logic                     := '0';
  signal fmdd_stat_i      : std_logic_vector(15 downto 0) := (others => '0');
  signal mebs_i           : std_logic_vector(4 downto 0);
  signal meb_cnt_i        : std_logic_vector(3 downto 0)  := "0000";
  signal al_rpinc_i       : std_logic;
  signal debug_i          : std_logic_vector(15 downto 0);
  
  -- purpose: Send a command (as from the bus) to decoder
  procedure send (
    constant code  : in  integer;       -- Instructon code
    constant bcast : in  std_logic;     -- Broad cast
    signal   ackn  : in  std_logic;     -- End
    signal   clk   : in  std_logic;     -- Clock
    signal   cstb  : out std_logic;     -- Control strobe
    signal   din   : out std_logic_vector(39 downto 0))
  is                                    -- Data
    variable aout  :     std_logic_vector(39 downto 20) := (others => '0');

  begin
    report "instruction is " & integer'image(code) severity note;
    aout(26 downto 20) := std_logic_vector(to_unsigned(code, 7));
    aout(36 downto 32) := HADD;
    aout(37)           := '1';
    aout(38)           := bcast;
    aout(39)           := (aout(20) xor aout(21) xor aout(22) xor aout(23) xor
                           aout(24) xor aout(25) xor aout(26) xor aout(27) xor
                           aout(28) xor aout(29) xor aout(30) xor aout(31) xor
                           aout(32) xor aout(33) xor aout(34) xor aout(35) xor
                           aout(36) xor aout(37) xor aout(38));
    wait until rising_edge(clk);
    din(39 downto 20) <= aout;
    din(15 downto 0)  <= std_logic_vector(to_unsigned(code, 16));
    cstb              <= '1';
    wait for 1 * PERIOD;
    if bcast = '0' then
      wait until rising_edge(ackn);
    else
      wait for 6 * PERIOD;
    end if;
    cstb              <= '0';
    din               <= (others => 'Z');
  end send;
  
begin  -- test
  -- purpose: Pull up data lines
  pull_up           : block
  begin  -- block pull_up
    mscl_i <= 'H';
    msda_i <= 'H';
  end block pull_up;


  DUT : entity work.bc_core(rtl2)
    port map (
        clk            => clk_i,             -- Clock
        sclk           => sclk_i,            -- Slow clock
        rstb           => rstb_i,            -- Reset
        cstb           => cstb_i,            -- Control strobe 
        write          => write_i,           -- Write
        dstb           => dstb_i,            -- Data strobe
        trsf           => trsf_i,            -- Data transfer
        l1_trg         => l1_trg_i,          -- L1 trigger
        l2_trg         => l2_trg_i,          -- L2 trigger
        ackn           => ackn_i,            -- External acknowlegde
        hadd           => HADD,              -- Card address
        din            => din_i,             -- Bus input
        scl            => scl_i,             -- I2C clock
        sda_in         => sda_in_i,          -- I2C data in
        sda_out        => sda_out_i,         -- I2C data out
        paps_error     => paps_error_i,      -- PASA power supply error
        alps_error     => alps_error_i,      -- ALTRO power supply error
        al_err         => al_err_i,          -- ALTRO error
        debug_sw       => debug_sw_i,        -- Debug switch
        bc_ackn_en     => bc_ackn_en_i,      -- Acknowledge enable 
        bc_ackn        => bc_ackn_i,         -- Acknowledge 
        lastst_al      => lastst_al_i,       -- Last state
        endtrans       => endtrans_i,        -- End of transfer 
        bc_dolo_en     => bc_dolo_en_i,      -- GTL output enable
        bc_cs          => bc_cs_i,           -- BC card select
        al_cs          => al_cs_i,           -- ALTRO card select
        wr_al          => wr_al_i,           -- Write to ALTRO
        dout           => dout_i,            -- Data out
        bc_int         => bc_int_i,          -- Interrupt
        bc_error       => bc_error_i,        -- BC error
        altro_sw       => altro_sw_i,        -- ALTRO switch
        pasa_sw        => pasa_sw_i,         -- PASA switch 
        al_rst         => al_rst_i,          -- Reset ALTRO
        rdoclk_en      => rdoclk_en_i,       -- Enable readout clock
        adcclk_en      => adcclk_en_i,       -- Enable ADC clock
        adc_add0       => adc_add0_i,        -- ADC address 0
        adc_add1       => adc_add1_i,        -- ADC address 1
        tsm_on         => tsm_on_i,          -- Test mode on
        card_isolation => card_isolation_i,  -- Card isolated
        test_a         => test_a_i,          -- Test mode A
        test_b         => test_b_i,          -- Test mode B
        test_c         => test_c_i,          -- Test mode C
        test_d         => test_d_i,          -- Test mode D
        test_e         => test_e_i,          -- Test mode E
        test_f         => test_f_i,          -- Test mode F
        test_g         => test_g_i,          -- Test mode G
        test_h         => test_h_i,          -- Test mode H
        mscl           => mscl_i,            -- Monitor clock
        msda           => msda_i,            -- monitor data out
        rmtrsf_en      => rmtrsf_en_i,       -- R/M transfer enable 
        rmtrsf         => rmtrsf_i,          -- R/M transfer
        rmdstb         => rmdstb_i,          -- R/M data strobe
        rmdata_out     => rmdata_out_i,      -- R/M output daa
        mem_wren       => mem_wren_i,        -- ???
        sclk_edge      => sclk_edge_i,       -- Slow clock edge
        evl_cstb       => evl_cstb_i,        -- Event length control strobe
        alevl_rdtrx    => alevl_rdtrx_i,     -- Event length r/w
        bc_master      => bc_master_i,       -- BC is master
        evl_addr       => evl_addr_i,        -- Event length address
        l0             => l0_i,
        fmdd_stat      => fmdd_stat_i,
        mebs           => mebs_i,
        meb_cnt        => meb_cnt_i,
        al_rpinc       => al_rpinc_i,
        debug          => debug_i);

  adc : ad7417
    generic map (
        VENDOR => VENDOR,               -- Vendor ID
        ADD    => ADD,                  -- Address
        PERIOD => 6.6 us,               -- Period of clk
        T      => X"dead",              -- T value
        ADC1   => X"beaf",              -- ADC1 value
        ADC2   => X"1111",              -- ADC2 value
        ADC3   => X"0000",              -- ADC3 value
        ADC4   => X"aaaa")              -- ADC4 value
    port map (
        scl    => mscl_i,               -- inout I2C Clock
        sda    => msda_i);              -- inout I2C data

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
    variable i : integer range 0 to 128;
  begin  -- process stimuli
    rstb_i <= '0';
    wait for 100 ns;
    rstb_i <= '1';
    wait for 4 * PERIOD;
    wait until rising_edge(clk_i);

    for i in 1 to 64 loop
      if i >= 27 and i <= 31 then
        next;
      elsif (i >= 6 and i <= 14) or i = 32 or  i = 33 then
        write_i       <= '0';           -- These register are read-only
      else
        write_i       <= '1';
      end if;
      wait until rising_edge(clk_i);
      send(code  => i,
           bcast => '0',                -- broadcast_i,
           clk   => clk_i,
           ackn  => bc_ackn_i,
           cstb  => cstb_i,
           din   => din_i);
      wait for 10 * PERIOD;
    end loop;  -- i

    wait;                               -- forever 
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
end test;

------------------------------------------------------------------------------
--
-- EOF
--
