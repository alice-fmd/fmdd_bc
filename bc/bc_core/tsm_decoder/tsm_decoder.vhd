------------------------------------------------------------------------------
-- Title      : Test mode decoder
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : tsm_decoder.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Decode test mode setup.
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tsm_decoder is
  port (
    clk     : in  std_logic;                      -- Clock
    rstb    : in  std_logic;                      -- Async reset
    tsm_in  : in  std_logic_vector(2 downto 0);   -- Test mode word in
    enable  : in  std_logic;                      -- Enable test mode decoder
    tsm_out : out std_logic_vector(7 downto 0));  -- Test mode word out
end tsm_decoder;
------------------------------------------------------------------------------
architecture rtl of tsm_decoder is
begin  -- rtl

  -- purpose: Decode input
  -- type   : sequential
  -- inputs : clk, rstb, tsm_in
  -- outputs: tsm_out
  decode              : process (clk, rstb)
    variable tsm_in_i : integer;
  begin  -- process decode
    if rstb = '0' then                  -- asynchronous reset (active low)
      tsm_out <= (others => '0');

    elsif clk'event and clk = '1' then  -- rising clock edge
      if enable = '1' then
        tsm_in_i := to_integer(unsigned(tsm_in));

        case tsm_in_i is
          when 0      => tsm_out <= "00000001";
          when 1      => tsm_out <= "00000010";
          when 2      => tsm_out <= "00000100";
          when 3      => tsm_out <= "00001000";
          when 4      => tsm_out <= "00010000";
          when 5      => tsm_out <= "00100000";
          when 6      => tsm_out <= "01000000";
          when 7      => tsm_out <= "10000000";
          when others => null;
        end case;

      else
        tsm_out <= (others => '0');

      end if;
    end if;
  end process decode;

end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package tsm_decoder_pack is
  component tsm_decoder
    port (
      clk     : in  std_logic;
      rstb    : in  std_logic;
      tsm_in  : in  std_logic_vector(2 downto 0);
      enable  : in  std_logic;
      tsm_out : out std_logic_vector(7 downto 0));
  end component;
end tsm_decoder_pack;
------------------------------------------------------------------------------
--
-- EOF
--
