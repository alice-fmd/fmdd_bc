------------------------------------------------------------------------------
--
-- Test bench of altrobusinterface
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.altrobusinterface_pack.all;

------------------------------------------------------------------------------

entity altrobusinterface_tb is
end altrobusinterface_tb;

------------------------------------------------------------------------------
architecture test of altrobusinterface_tb is
  constant PERIOD : time                         := 25 ns;  -- A clock cycle
  constant HADD   : std_logic_vector(4 downto 0) := "00000";

  signal clk_i        : std_logic                      := '0';
  signal rstb_i       : std_logic                      := '1';
  signal cstb_i       : std_logic                      := '0';
  signal valid_i      : std_logic                      := '0';
  signal write_i      : std_logic                      := '0';
  signal din_i        : std_logic_vector(39 downto 20) := (others => 'Z');
  signal exec_i       : std_logic;
  signal ackn_en_if_i : std_logic;
  signal ackn_if_i    : std_logic;
  signal lastst_al_i  : std_logic;
  signal endtrans_i   : std_logic;
  signal bc_dolo_en_i : std_logic;
  signal par_error_i  : std_logic;
  signal bc_cs_i      : std_logic;
  signal al_cs_i      : std_logic;
  signal wr_al_i      : std_logic;
  signal icode_i      : std_logic_vector(6 downto 0);
  signal wr_bc_i      : std_logic;
  signal bcast_i      : std_logic;
  signal i            : integer;
  signal broadcast_i  : std_logic                      := '0';
  signal al_rpinc_i   : std_logic;

  procedure send (
    constant bcal  : in  std_logic;     -- BC not ALTRO
    constant code  : in  integer;       -- Instructon code
    constant bcast : in  std_logic;     -- Broad cast
    signal   ackn  : in  std_logic;     -- End
    signal   clk   : in  std_logic;     -- Clock
    signal   cstb  : out std_logic;     -- Control strobe
    signal   valid : out std_logic;     -- Valid instruction
    signal   data  : out std_logic_vector(39 downto 20)) is
    variable dout  :     std_logic_vector(39 downto 20) := (others => '0');
    -- variable bcal_i : std_logic := '1' when bcal else '0';

  begin
    report "instruction is " & integer'image(code) severity note;
    dout(26 downto 20) := std_logic_vector(to_unsigned(code, 7));
    dout(36 downto 32) := HADD;
    dout(37)           := bcal; -- _i; -- '1' when bcal else '0';
    dout(38)           := bcast;
    dout(39)           := (dout(20) xor dout(21) xor dout(22) xor dout(23) xor
                           dout(24) xor dout(25) xor dout(26) xor dout(27) xor
                           dout(28) xor dout(29) xor dout(30) xor dout(31) xor
                           dout(32) xor dout(33) xor dout(34) xor dout(35) xor
                           dout(36) xor dout(37) xor dout(38));
    wait until rising_edge(clk);
    report "bcast is " & std_logic'image(bcast) severity note;
    data   <= dout;
    cstb   <= '1';
    wait for 1 * PERIOD;
    valid <= '1';
    if bcast = '0' then
      wait until rising_edge(ackn);
    else
      wait for 6 * PERIOD; 
    end if;
    cstb   <= '0';
    data   <= (others => 'Z');
    valid <= '0';
  end send;
  
begin  -- test

  DUT : altrobusinterface
    port map (
        clk        => clk_i,
        rstb       => rstb_i,
        cstb       => cstb_i,
        valid      => valid_i,
        write      => write_i,
        din        => din_i,
        hadd       => HADD,
        exec       => exec_i,
        ackn_en_if => ackn_en_if_i,
        ackn_if    => ackn_if_i,
        lastst_al  => lastst_al_i,
        endtrans   => endtrans_i,
        bc_dolo_en => bc_dolo_en_i,
        par_error  => par_error_i,
        bc_cs      => bc_cs_i,
        al_cs      => al_cs_i,
        wr_al      => wr_al_i,
        icode      => icode_i,
        wr_bc      => wr_bc_i,
        bcast      => bcast_i,
        al_rpinc   => al_rpinc_i);

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    din_i    <= (others => 'Z');
    rstb_i   <= '0';
    wait for 100 ns;
    rstb_i   <= '1';
    wait for 4 * PERIOD;
    wait until rising_edge(clk_i);

    for i in 1 to 4 loop
      if (i = 3 or i = 4) then
        broadcast_i <= '1';
      else
        broadcast_i <= '0';
      end if;
      if (i = 2 or i = 4) then
        write_i     <= '1';
      else
        write_i     <= '0';
      end if;
      wait until rising_edge(clk_i);
      send(bcal  => '1',
           code  => i,
           bcast => broadcast_i,
           clk   => clk_i,
           ackn  => ackn_if_i,
           cstb  => cstb_i,
           valid => valid_i,
           data  => din_i);
      wait for 10 * PERIOD;
    end loop;  -- i

    send(bcal  => '0',
         code  => 25, -- to_integer("0011001"),
         bcast => '1',
         clk   => clk_i,
         ackn  => ackn_if_i,
         cstb  => cstb_i,
         valid => valid_i,
         data  => din_i);
    wait for 10 * PERIOD;
    
    wait;                               -- forever 
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
  

end test;

------------------------------------------------------------------------------
