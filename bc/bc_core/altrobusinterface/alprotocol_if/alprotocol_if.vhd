------------------------------------------------------------------------------
-- Title      : ALTRO bus protocol handler
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : alprotocol_if.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Handle the ALTRO bus protocol for the FMDD BC
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity alprotocol_if is
  port (
    clk        : in  std_logic;         -- clock
    rstb       : in  std_logic;         -- Async reset
    cstb       : in  std_logic;         -- Control strobe
    valid      : in  std_logic;         -- Valid input
    bc_cs      : in  std_logic;         -- BC card select
    bcast      : in  std_logic;         -- Broad cast
    write      : in  std_logic;         -- Write pulse
    freeze     : out std_logic;         -- Freeze it
    exec       : out std_logic;         -- Execute flag
    ackn_en    : out std_logic;         -- Acknowledge enable
    ackn       : out std_logic;         -- Acknowledge
    lastst_al  : out std_logic;         -- Last for 1 cycle
    endtrans   : out std_logic;         -- End of transmission
    bc_dolo_en : out std_logic);        -- Enable GTL out
end alprotocol_if;

------------------------------------------------------------------------------
architecture rtl of alprotocol_if is
  type state is (s_idle, s_cstb, s_decode, s_exec, s_en1, s_ackn, s_en2);
  signal st          : state;           -- State
  signal endtrans_i  : std_logic;
  signal endtrans_ii : std_logic;
  signal exec_i      : std_logic;
begin  -- rtl
  combi: block
  begin  -- block end_trans_block
    exec        <= exec_i;
    endtrans    <= endtrans_i;
    endtrans_ii <= '1' when (st = s_en2) else '0';
  end block combi;

  -- purpose: State machine to handle protocol
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  fsm : process (clk, rstb)
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      ackn        <= '0';
      ackn_en     <= '0';
      freeze      <= '0';
      exec_i      <= '0';
      st          <= s_idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      case st is
        when s_idle =>
          ackn    <= '0';
          ackn_en <= '0';
          freeze  <= '0';
          exec_i  <= '0';
          if cstb = '1' then
            st    <= s_cstb;
          else
            st    <= s_idle;
          end if;

        when s_cstb =>
          ackn    <= '0';
          ackn_en <= '0';
          freeze  <= '1';
          exec_i  <= '0';
          if cstb = '1' then
            st    <= s_decode;
          else
            st    <= s_idle;
          end if;

        when s_decode =>
          ackn    <= '0';
          ackn_en <= '0';
          freeze  <= '0';
          exec_i  <= '0';
          if bc_cs = '1' and cstb = '1' then
            st    <= s_exec;
          elsif bc_cs = '0' and cstb = '1' then
            st    <= s_decode;
          else
            st    <= s_idle;
          end if;

        when s_exec =>
          ackn     <= '0';
          ackn_en  <= '0';
          freeze   <= '0';
          if valid = '1' then
            exec_i <= '1';
            st     <= s_en1;
          elsif cstb = '1' then
            exec_i <= '0';
            st     <= s_exec;
          else
            exec_i <= '0';
            st     <= s_idle;
          end if;

        when s_en1 =>
          ackn      <= '0';
          freeze    <= '0';
          exec_i    <= '0';
          st        <= s_ackn;
          if bcast = '1' then
            ackn_en <= '0';
          else
            ackn_en <= '1';
          end if;

        when s_ackn =>
          freeze    <= '0';
          exec_i    <= '0';
          if bcast = '1' then
            ackn    <= '0';
            ackn_en <= '0';
          else
            ackn    <= '1';
            ackn_en <= '1';
          end if;
          if cstb = '0' then
            st      <= s_en2;
          else
            st      <= s_ackn;
          end if;

        when s_en2 =>
          ackn      <= '0';
          freeze    <= '0';
          exec_i    <= '0';
          st        <= s_idle;
          if bcast = '1' then
            ackn_en <= '0';
          else
            ackn_en <= '1';
          end if;

        when others =>
          ackn    <= '0';
          ackn_en <= '0';
          freeze  <= '0';
          exec_i  <= '0';
          st      <= s_idle;
      end case;
    end if;
  end process fsm;

  -- purpose: Flag to GTL drivers.
  -- type   : sequential
  -- inputs : clk, rstb, endtrans, exec_i
  -- outputs: bc_dolo_en
  gtl_output : process (clk, rstb)
  begin  -- process gtl_output
    if rstb = '0' then                  -- asynchronous reset (active low)
      bc_dolo_en   <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if endtrans_i = '1' then
        bc_dolo_en <= '0';
      elsif exec_i = '1' then
        bc_dolo_en <= not write;
      end if;
    end if;
  end process gtl_output;

  -- purpose: Output lastst_al and end of transmission
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: endtrans, lastst_al
  end_of_trans : process (clk, rstb)
  begin  -- process end_of_trans
    if rstb = '0' then                  -- asynchronous reset (active low)
      endtrans_i <= '0';
      lastst_al  <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      endtrans_i <= endtrans_ii;
      lastst_al  <= endtrans_i;
    end if;
  end process end_of_trans;

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package alprotocol_if_pack is
  component alprotocol_if
    port (
      clk        : in  std_logic;
      rstb       : in  std_logic;
      cstb       : in  std_logic;
      valid      : in  std_logic;
      bc_cs      : in  std_logic;
      bcast      : in  std_logic;       -- Broad cast
      write      : in  std_logic;
      freeze     : out std_logic;
      exec       : out std_logic;
      ackn_en    : out std_logic;
      ackn       : out std_logic;
      lastst_al  : out std_logic;
      endtrans   : out std_logic;
      bc_dolo_en : out std_logic);
  end component;
end alprotocol_if_pack;
------------------------------------------------------------------------------
--
-- EOF
--

------------------------------------------------------------------------------
-- EOF
--
