------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.alprotocol_if_pack.all;

------------------------------------------------------------------------------
entity alprotocol_if_tb is
end alprotocol_if_tb;

------------------------------------------------------------------------------

architecture test of alprotocol_if_tb is
  constant PERIOD : time                         := 25 ns;  -- A clock cycle

  signal clk_i        : std_logic := '0';
  signal rstb_i       : std_logic := '1';
  signal cstb_i       : std_logic := '1';
  signal cstb_ii      : std_logic := '0';
  signal valid_i      : std_logic := '0';
  signal bc_cs_i      : std_logic := '0';
  signal bcast_i      : std_logic := '0';
  signal write_i      : std_logic := '0';
  signal freeze_i     : std_logic;
  signal exec_i       : std_logic;
  signal ackn_en_i    : std_logic;
  signal ackn_i       : std_logic;
  signal lastst_al_i  : std_logic;
  signal endtrans_i   : std_logic;
  signal bc_dolo_en_i : std_logic;

  procedure send (
    signal clk   : in  std_logic;       -- Clock
    signal ackn  : in  std_logic;       -- End
    signal bcast : in  std_logic;       -- Broadcast
    signal valid : out std_logic;       -- Broad cast
    signal cstb  : out std_logic;       -- Control strobe
    signal bc_cs : out std_logic) is    -- Freeze
  begin
    wait until rising_edge(clk);
    cstb  <= '0';
    bc_cs <= '1';
    wait for 1 * PERIOD;
    valid <= '1';
    if bcast = '0' then
      wait until rising_edge(ackn);
    else
      wait for 6 * PERIOD; 
    end if;
    cstb  <= '1';
    bc_cs <= '0';
    valid <= '0';
  end send;

begin  -- test
  DUT : alprotocol_if
    port map (
        clk        => clk_i,
        rstb       => rstb_i,
        cstb       => cstb_ii,
        valid      => valid_i,
        bc_cs      => bc_cs_i,
        bcast      => bcast_i,
        write      => write_i,
        freeze     => freeze_i,
        exec       => exec_i,
        ackn_en    => ackn_en_i,
        ackn       => ackn_i,
        lastst_al  => lastst_al_i,
        endtrans   => endtrans_i,
        bc_dolo_en => bc_dolo_en_i);

  cstb_ii <= not cstb_i;
  
  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    bcast_i <= '0';
    write_i <= '0';
    rstb_i  <= '0';
    wait for 100 ns;
    rstb_i  <= '1';
    wait for 4 * PERIOD;
    wait until rising_edge(clk_i);

    for i in 0 to 3 loop
      if i = 1 then
        bcast_i <= '1';
      end if;
      if i = 2 then
        bcast_i <= '0';
        write_i <= '1';
      end if;
      if i = 3 then
        bcast_i <= '1';
      end if;
      send(clk   => clk_i,
           ackn  => ackn_i,
           bcast => bcast_i,
           cstb  => cstb_i,
           valid => valid_i,
           bc_cs => bc_cs_i);
      wait for 10 * PERIOD;
    end loop;  -- i

    wait;                               -- forever 
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
  

end test;

------------------------------------------------------------------------------
