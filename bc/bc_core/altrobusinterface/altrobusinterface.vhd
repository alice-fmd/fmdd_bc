------------------------------------------------------------------------------
-- Title      : ALTRO Bus interface
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : altrobusinterface.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2008/07/17
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Handles the input interface to the ALTRO bus
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.alprotocol_if_pack.all;
use work.interfacebus_pack.all;

entity altrobusinterface is
  port (
    clk        : in  std_logic;         -- Clock
    rstb       : in  std_logic;         -- Async reset
    cstb       : in  std_logic;         -- Control strobe
    valid      : in  std_logic;         -- Valid instruction
    write      : in  std_logic;         -- Write
    din        : in  std_logic_vector(39 downto 20);  -- Data in
    hadd       : in  std_logic_vector(4 downto 0);  -- Board address
    exec       : out std_logic;         -- Execute
    ackn_en_if : out std_logic;         -- Acknowledge enable
    ackn_if    : out std_logic;         -- Acknowledge
    lastst_al  : out std_logic;         -- In last state
    endtrans   : out std_logic;         -- End of transmission
    bc_dolo_en : out std_logic;         -- Enable GTL out
    par_error  : out std_logic;         -- Parity error
    bc_cs      : out std_logic;         -- BC card select
    al_cs      : out std_logic;         -- ALTRO card select
    wr_al      : out std_logic;         -- ALTRO write
    icode      : out std_logic_vector(6 downto 0);  -- Instruction code
    wr_bc      : out std_logic;         -- BC Write
    bcast      : out std_logic;         -- Broadcast
    al_rpinc   : out std_logic);        -- ALTRO read-pointer increment
end altrobusinterface;

------------------------------------------------------------------------------
architecture rtl of altrobusinterface is
  signal freeze_i : std_logic;          -- Freeze it.
  signal bc_cs_i : std_logic;           -- Internal BC card select
  signal bcast_i : std_logic;           -- Internal broadcast
  
begin  -- rtl

  -- purpose: Combinatorics
  combi: block
  begin  -- block combi
    bc_cs <= bc_cs_i;
    bcast <= bcast_i;
  end block combi;

  alprotocol_if_i: alprotocol_if
    port map (
        clk        => clk,
        rstb       => rstb,
        cstb       => cstb,
        valid      => valid,
        bc_cs      => bc_cs_i,
        bcast      => bcast_i,
        write      => write,
        freeze     => freeze_i,
        exec       => exec,
        ackn_en    => ackn_en_if,
        ackn       => ackn_if,
        lastst_al  => lastst_al,
        endtrans   => endtrans,
        bc_dolo_en => bc_dolo_en);

  interfacebus_i: interfacebus
    port map (
        clk     => clk,
        rstb    => rstb,
        freeze  => freeze_i,
        write   => write,
        cstb    => cstb,
        din     => din,
        hadd    => hadd,
        par_err => par_error,
        bc_cs   => bc_cs_i,
        al_cs   => al_cs,
        bcast   => bcast_i,
        wr_al   => wr_al,
        wr_bc   => wr_bc,
        icode   => icode,
        al_rpinc => al_rpinc);
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
package altrobusinterface_pack is
  component altrobusinterface
    port (
      clk        : in  std_logic;       -- Clock
      rstb       : in  std_logic;       -- Async reset
      cstb       : in  std_logic;       -- Control strobe
      valid      : in  std_logic;       -- Valid instruction
      write      : in  std_logic;       -- Write
      din        : in  std_logic_vector(39 downto 20);  -- Data in
      hadd       : in  std_logic_vector(4 downto 0);  -- Board address
      exec       : out std_logic;       -- Execute
      ackn_en_if : out std_logic;       -- Acknowledge enable
      ackn_if    : out std_logic;       -- Acknowledge
      lastst_al  : out std_logic;       -- In last state
      endtrans   : out std_logic;       -- End of transmission
      bc_dolo_en : out std_logic;       -- Enable GTL out
      par_error  : out std_logic;       -- Parity error
      bc_cs      : out std_logic;       -- BC card select
      al_cs      : out std_logic;       -- ALTRO card select
      wr_al      : out std_logic;       -- ALTRO write
      icode      : out std_logic_vector(6 downto 0);  -- Instruction code
      wr_bc      : out std_logic;       -- BC Write
      bcast      : out std_logic;       -- Broadcast
      al_rpinc   : out std_logic);      -- ALTRO read-pointer increment
  end component altrobusinterface;
end altrobusinterface_pack;
------------------------------------------------------------------------------
--
-- EOF
--
