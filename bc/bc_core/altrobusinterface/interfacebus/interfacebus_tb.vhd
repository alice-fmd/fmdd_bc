------------------------------------------------------------------------------
--
-- Test bench for interfacebus entity
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.interfacebus_pack.all;

------------------------------------------------------------------------------
entity interfacebus_tb is
end interfacebus_tb;

------------------------------------------------------------------------------
architecture test of interfacebus_tb is
  constant PERIOD : time                         := 25 ns;  -- A clock cycle
  constant HADD   : std_logic_vector(4 downto 0) := "00000";

  signal clk_i     : std_logic := '0';
  signal rstb_i    : std_logic := '1';
  signal freeze_i  : std_logic := '0';
  signal write_i   : std_logic := '1';
  signal cstb_i    : std_logic := '1';
  signal din_i     : std_logic_vector(39 downto 20);
  signal par_err_i : std_logic;
  signal bc_cs_i   : std_logic;
  signal al_cs_i   : std_logic;
  signal bcast_i   : std_logic;
  signal wr_al_i   : std_logic;
  signal wr_bc_i   : std_logic;
  signal icode_i   : std_logic_vector(6 downto 0);
  signal i         : integer;

  procedure send (
    constant code   : in  integer;      -- Instructon code
    constant bcast  : in  std_logic;    -- Broad cast
    signal   clk    : in  std_logic;    -- Clock
    signal   cstb   : out std_logic;    -- Control strobe
    signal   freeze : out std_logic;    -- Freeze
    signal   data   : out std_logic_vector(39 downto 20)) is
    variable dout   :     std_logic_vector(39 downto 20) := (others => '0');

  begin
    dout(26 downto 20) := std_logic_vector(to_unsigned(code, 7));
    dout(36 downto 32) := HADD;
    dout(37)           := not bcast;
    dout(38)           := bcast;
    dout(39)           := (dout(20) xor dout(21) xor dout(22) xor dout(23) xor
                           dout(24) xor dout(25) xor dout(26) xor dout(27) xor
                           dout(28) xor dout(29) xor dout(30) xor dout(31) xor
                           dout(32) xor dout(33) xor dout(34) xor dout(35) xor
                           dout(36) xor dout(37) xor dout(38));
    wait until rising_edge(clk);
    data   <= dout;
    cstb   <= '0';
    wait for 2 * PERIOD;
    freeze <= '1';
    wait for 2 * PERIOD;
    cstb   <= '1';
    data   <= (others => 'Z');
    freeze <= '0';
  end send;

begin  -- test

  DUT: interfacebus
    port map (
        clk     => clk_i,
        rstb    => rstb_i,
        freeze  => freeze_i,
        write   => write_i,
        cstb    => cstb_i,
        din     => din_i,
        hadd    => HADD,
        par_err => par_err_i,
        bc_cs   => bc_cs_i,
        al_cs   => al_cs_i,
        bcast   => bcast_i,
        wr_al   => wr_al_i,
        wr_bc   => wr_bc_i,
        icode   => icode_i);

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    din_i    <= (others => 'Z');
    rstb_i   <= '0';
    wait for 100 ns;
    rstb_i   <= '1';
    wait for 4 * PERIOD;
    wait until rising_edge(clk_i);

    for i in 1 to 10 loop
      send(code   => i,
           bcast  => '0',
           clk    => clk_i,
           cstb   => cstb_i,
           freeze => freeze_i,
           data   => din_i);
      wait for 4 * PERIOD;
    end loop;  -- i    
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;

end test;

------------------------------------------------------------------------------
