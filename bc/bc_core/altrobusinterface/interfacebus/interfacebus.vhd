------------------------------------------------------------------------------
-- Title      : Interface to the ALTRO bus 
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : interfacebus.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2008/07/17
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Interface to the ALTRO bus
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.register_config.all;

entity interfacebus is
  port (
    clk      : in  std_logic;           -- Clock
    rstb     : in  std_logic;           -- Async reset
    freeze   : in  std_logic;           -- Freeze
    write    : in  std_logic;           -- Write flag
    cstb     : in  std_logic;           -- control strobe
    din      : in  std_logic_vector(39 downto 20);  -- Data input
    hadd     : in  std_logic_vector(4 downto 0);  -- Hardware address
    par_err  : out std_logic;           -- Parity error
    bc_cs    : out std_logic;           -- BC card select
    al_cs    : out std_logic;           -- Altro card select
    bcast    : out std_logic;           -- Broad cast
    wr_al    : out std_logic;           -- Write ALTRO flag
    wr_bc    : out std_logic;           -- Write BC flag
    icode    : out std_logic_vector(6 downto 0);  -- Instruction code
    al_rpinc : out std_logic);          -- ALTRO read-pointer increment
end interfacebus;

------------------------------------------------------------------------------
architecture rtl of interfacebus is
  signal par_odd_i       : std_logic;   -- Odd parity
  signal par_even_i      : std_logic;   -- Even parity
  signal expl_addr_i     : std_logic;   -- Wether we're explicitly addressed
  signal addr_or_bcast_i : std_logic;   -- Addressed or broadcast
  signal bc_selected_i   : std_logic;   -- Whether BC's are selected
  signal bc_cs_i         : std_logic;   -- BC card selected internal
  signal bc_addressed_i  : std_logic;   -- This BC addressed
  signal clear_i         : std_logic;   -- Clear on rstb _and_ cstb
  signal al_rpinc_i      : std_logic;   -- ALTRO read-pointer increment
  signal rpinc_i         : std_logic;
begin  -- rtl

  -- purpose: Lumb combinatorics together in a block
  combinatorics : block
  begin  -- block combinatorics
    par_odd_i       <= (din(20) xor din(21) xor din(22) xor din(23) xor
                        din(24) xor din(25) xor din(26) xor din(27) xor
                        din(28) xor din(29) xor din(30) xor din(31) xor
                        din(32) xor din(33) xor din(34) xor din(35) xor
                        din(36) xor din(37) xor din(38) xor din(39));
    par_even_i      <= not par_odd_i;
    addr_or_bcast_i <= din(38) or expl_addr_i;
    bc_selected_i   <= din(37) and addr_or_bcast_i;
    bc_cs_i         <= par_even_i and bc_selected_i;
    bc_addressed_i  <= freeze and bc_cs_i;
    clear_i         <= rstb and cstb;
  end block combinatorics;


  -- purpose: Check if we're addressed explicitly
  -- type   : combinational
  -- inputs : din, hadd
  -- outputs: expl_addr_i
  address_compare: process (din, hadd)
  begin  -- process address_compare
    if din(36 downto 32) = hadd then
      expl_addr_i <= '1';
    else
      expl_addr_i <= '0';
    end if;
    if (din(37) = '0') and (din(24 downto 20) = add_al_rpinc) then
      al_rpinc_i <= '1';
    else
      al_rpinc_i <= '0';
    end if;
  end process address_compare;


  -- purpose: Registers 1
  -- type   : sequential
  -- inputs : clk, clear_i, freeze
  -- outputs: par_err, bc_cs, al_cs
  regs1 : process (clk, clear_i) is
  begin  -- process regs1
    if clear_i = '0' then               -- asynchronous reset (active low)
      par_err    <= '0';
      bc_cs      <= '0';
      al_cs      <= '0';
      al_rpinc   <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if freeze = '1' then
        par_err  <= par_odd_i and bc_selected_i;
        al_cs    <= not din(37);        -- ALTRO selected
        bc_cs    <= bc_cs_i;            -- BC selected 
        al_rpinc <= al_rpinc_i;
      end if;
    end if;
  end process regs1;

  -- purpose: Select wr_bc
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: wr_bc
  wr_bc_proc : process (clk, rstb)
  begin  -- process wr_bc_proc
    if rstb = '0' then                  -- asynchronous reset (active low)
      wr_bc     <= '0';
      icode     <= (others => '0');
      bcast     <= '0';
      wr_al     <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_addressed_i = '1' then
        wr_bc   <= write;
        icode   <= din(26 downto 20);
        bcast   <= din(38);
      end if;
      if freeze = '1' then
        wr_al   <= write;               -- Write to ALTRO's
      end if;
    end if;
  end process wr_bc_proc;
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package interfacebus_pack is
  component interfacebus
    port (
      clk      : in  std_logic;         -- Clock
      rstb     : in  std_logic;         -- Async reset
      freeze   : in  std_logic;         -- Freeze
      write    : in  std_logic;         -- Write flag
      cstb     : in  std_logic;         -- control strobe
      din      : in  std_logic_vector(39 downto 20);  -- Data input
      hadd     : in  std_logic_vector(4 downto 0);  -- Hardware address
      par_err  : out std_logic;         -- Parity error
      bc_cs    : out std_logic;         -- BC card select
      al_cs    : out std_logic;         -- Altro card select
      bcast    : out std_logic;         -- Broad cast
      wr_al    : out std_logic;         -- Write ALTRO flag
      wr_bc    : out std_logic;         -- Write BC flag
      icode    : out std_logic_vector(6 downto 0);  -- Instruction code
      al_rpinc : out std_logic);        -- ALTRO read-pointer increment
  end component interfacebus;
end interfacebus_pack;

------------------------------------------------------------------------------
--
-- EOF
--
