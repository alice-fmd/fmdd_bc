------------------------------------------------------------------------------
-- Title      : Interface to Monitor ADCs
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : interface_adc.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2009-11-18
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Read out Monitor ADCs
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.sequencer_pack.all;
use work.rom_pack.all;
use work.master_pack.all;

entity interface_adc is
  generic (
    MSCL_DIV : natural := 32);          -- slow clock division
  port (
    clk      : in    std_logic;         -- Clock
    rstb     : in    std_logic;         -- Async reset
    stcnv    : in    std_logic;         -- Command: Start conversion
    we_adc   : out   std_logic;         -- Write enable for ADC registers
    end_seq  : out   std_logic;         -- End of conversion flag
    wadd_adc : out   std_logic_vector(4 downto 0);  -- ADC register address
    mscl     : out   std_logic;         -- Monitor ADC I2C clock
    msda     : inout std_logic;         -- Monitor ADC I2C data
    data_adc : out   std_logic_vector(15 downto 0);  -- Data from ADC
    state    : out   std_logic_vector(4 downto 0));  -- Master state
end interface_adc;

------------------------------------------------------------------------------
architecture rtl of interface_adc is
  signal address_rom_i : std_logic_vector(9 downto 0)  := (others => '0');
  signal data_rom_i    : std_logic_vector(10 downto 0) := (others => '0');
  signal data_valid_i  : std_logic;     -- From master: Data is valid
  signal en_add_i      : std_logic;     -- From master: Enable address
  signal error_i       : std_logic;     -- From master: Error flag
  signal ready_i       : std_logic;     -- From master: Is ready
  signal s_i           : std_logic;     -- From master: ???
  signal width_i       : std_logic;     -- From master: ??? 
  signal data_seq_i    : std_logic_vector(7 downto 0)  := (others => '0');
  signal new_data_i    : std_logic;     -- From seq: New instruction
  signal rw_i          : std_logic;     -- From seq: Read/Write flag
  signal start_i       : std_logic;     -- From seq: Start
  signal stop_i        : std_logic;     -- From seq: Stop
begin  -- rtl
  -- FIXME: Perhaps we should use an LPM_ROM component instead.

  instructions_rom : entity work.rom(rtl)
    port map (
      rstb   => rstb,
      inclk  => clk,
      outclk => clk,
      add    => address_rom_i,
      data   => data_rom_i);

  seq : sequencer
    port map (
      clk          => clk,
      rstb         => rstb,
      data_valid   => data_valid_i,
      data_rom     => data_rom_i,
      en_add       => en_add_i,
      error        => error_i,
      ready        => ready_i,
      s            => s_i,
      stcnv        => stcnv,
      width        => width_i,
      data         => data_seq_i,
      address_rom  => address_rom_i,
      address_regs => wadd_adc,
      new_data     => new_data_i,
      rw           => rw_i,
      start        => start_i,
      stop         => stop_i,
      we_adc       => we_adc,
      end_seq      => end_seq);

  i2c_master : master
    generic map (
      MSCL_DIV => MSCL_DIV)
    port map (
      clk         => clk,               -- Clock
      rstb        => rstb,              -- Reset
      data_par_in => data_seq_i,        -- Data from sequencer
      new_data    => new_data_i,        -- Got new data
      rw          => rw_i,              -- Read/write flag
      start       => start_i,           -- Start it
      stop        => stop_i,            -- Stop it
      width       => width_i,           -- ???
      en_add      => en_add_i,          -- Enable address change
      data_adc    => data_adc,          -- Data for registers
      ready_seq   => ready_i,           -- Ready to sequencer
      data_valid  => data_valid_i,      -- Valid data
      error       => error_i,           -- Error flag
      s           => s_i,               -- ???
      scl         => mscl,              -- I2C clock
      sda         => msda,              -- I2C data
      state       => state);            -- master state
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package interface_adc_pack is
  component interface_adc
    generic (
      MSCL_DIV : natural := 66);        -- slow clock division
    port (
      clk      : in    std_logic;
      rstb     : in    std_logic;
      stcnv    : in    std_logic;
      we_adc   : out   std_logic;
      end_seq  : out   std_logic;
      wadd_adc : out   std_logic_vector(4 downto 0);
      mscl     : out   std_logic;
      msda     : inout std_logic;
      data_adc : out   std_logic_vector(15 downto 0);
      state    : out   std_logic_vector(4 downto 0));
  end component;
end interface_adc_pack;
------------------------------------------------------------------------------
--
-- EOF
--
