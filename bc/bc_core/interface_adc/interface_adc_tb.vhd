------------------------------------------------------------------------------
-- Title      : Test bench for interface_adc
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : interface_adc_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2006/10/05
-- Platform   : 
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test interface_adc
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface_adc_pack.all;
library ad7417_model;
use ad7417_model.ad7417_pack.all;

------------------------------------------------------------------------------
entity interface_adc_tb is
end interface_adc_tb;

------------------------------------------------------------------------------
architecture test of interface_adc_tb is
  constant PERIOD   : time                         := 25 ns;  -- A clock cycle
  constant ADD      : std_logic_vector(2 downto 0) := "000";
  constant VENDOR   : std_logic_vector(3 downto 0) := "0101";
  constant MSCL_DIV : natural                      := 32;  -- 8;
  constant MPERIOD  : time                         := 6.6 us;  -- 1.65 us;

  signal clk_i      : std_logic := '0';
  signal rstb_i     : std_logic := '0';
  signal stcnv_i    : std_logic := '0';
  signal we_adc_i   : std_logic;
  signal end_seq_i  : std_logic;
  signal wadd_adc_i : std_logic_vector(4 downto 0) := (others => '0');
  signal mscl_i     : std_logic;
  signal msda_i     : std_logic;
  signal data_adc_i : std_logic_vector(15 downto 0) := (others => '0');

  -- purpose: convert integer to std_logic_vector
  function int2slv (
    constant x   : integer;             -- Value
    constant l   : natural)             -- Size
    return std_logic_vector is
    variable ret : std_logic_vector(l-1 downto 0);
  begin  -- function int2slv
    ret := std_logic_vector(to_unsigned(x, l));
    return ret;
  end function int2slv;
begin  -- test
  -- purpose: Pull up data lines
  pull_up           : block
  begin  -- block pull_up
    mscl_i <= 'H';
    msda_i <= 'H';
  end block pull_up;

  DUT : interface_adc
    generic map (
      MSCL_DIV   => MSCL_DIV)                  -- 4 times faster than real life
    port map (
        clk      => clk_i,
        rstb     => rstb_i,
        stcnv    => stcnv_i,
        we_adc   => we_adc_i,
        end_seq  => end_seq_i,
        wadd_adc => wadd_adc_i,
        mscl     => mscl_i,
        msda     => msda_i,
        data_adc => data_adc_i);

  adcs  : for i in 0 to 3 generate
  begin  -- generate adcs
    adc : ad7417
      generic map (
        VENDOR => VENDOR,
        ADD    => int2slv(i, 3),
        PERIOD => MPERIOD,              -- 4 times faster than real life
        T      => to_unsigned(5 * i + 1, 16),
        ADC1   => to_unsigned(5 * i + 2, 16),
        ADC2   => to_unsigned(5 * i + 3, 16),
        ADC3   => to_unsigned(5 * i + 4, 16),
        ADC4   => to_unsigned(5 * i + 5, 16))
      port map (
        scl    => mscl_i,
        sda    => msda_i);
  end generate adcs;

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i  <= '1';

    wait for 4 * PERIOD;

    -- Start conversion
    wait until rising_edge(clk_i);
    stcnv_i <= '1';
    wait until rising_edge(clk_i);
    stcnv_i <= '0';
    
    wait;                               -- forever 
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;

end test;
------------------------------------------------------------------------------
--
-- EOF
--
