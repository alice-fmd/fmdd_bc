------------------------------------------------------------------------------
-- Title      : Master Monitor ADC I2C bus
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : master_sm.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2009-11-18
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Master statemacine for the Monitor ADC I2C bus.
--              If the clk input is connect to the fast 40MHz clock, then the
--              clk_en should be connected to rising_edge detector of the
--              slower clock.  If clk is connected to the slow clock, then
--              clk_en should be connected to VCC ('1'). 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/08  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity master_sm is
  port (
    clk         : in  std_logic;        -- Clock
    clk_en      : in  std_logic;        -- Slow clock enable
    rstb        : in  std_logic;        -- Async reset
    cnt_2       : in  std_logic_vector(1 downto 0);   -- Count to 4
    cnt_8       : in  std_logic;        -- Got one byte from serial data
    new_data    : in  std_logic;        -- New instruction from sequencer
    rw          : in  std_logic;        -- Read/Write flag from sequencer
    sda_ser     : in  std_logic;        -- Serial data from serializer
    start       : in  std_logic;        -- Write start condtion
    stop        : in  std_logic;        -- Write stop condition
    sda         : in  std_logic;        -- Serial data before tri-state
    width       : in  std_logic;        -- 1 or 2 bytes
    clear       : out std_logic;        -- Clear the serializer
    data_valid  : out std_logic;        -- Got a valid instruction
    en_cnt_2    : out std_logic;        -- Enable 2bit counter
    enable      : out std_logic;        -- Shift in serializer
    load        : out std_logic;        -- Load word into serializer
    ready       : out std_logic;        -- Ready for more data;
    s_en        : out std_logic;        -- Enable word counter
    scl         : out std_logic;        -- Output clock
    sda_in      : out std_logic;        -- Serial data input;
    sel_sda_in  : out std_logic;        -- Enable to tristate buffer;
    sel_sda_out : out std_logic;        -- Enable to tristate buffer
    valid       : out std_logic;        -- Valid state (?)
    error       : out std_logic;        -- Error flag
    state       : out std_logic_vector(4 downto 0));  -- State
end master_sm;

------------------------------------------------------------------------------
architecture rtl of master_sm is
  type st_t is (idle,
                start_1,
                start_2,
                latch_1,
                latch_2,
                latch_3,
                latch_4,
                ack_1,
                ack_2,
                ack_3,
                stop_1,
                stop_2,
                stop_3,
                latch_data_1,
                latch_data_2,
                latch_data_3,
                latch_data_4,
                ack_m_1,
                ack_m_2,
                ack_m_3,
                ack_m_4,
                n_ack_m_2,
                n_ack_m_3,
                n_ack_m_4);                            -- States
  type   sel_state is (sel_master, sel_ser, sel_adc);  -- Selection states
  signal st      : st_t;                               -- State
  signal nx_st   : st_t;                               -- Next state
  signal sel_sda : sel_state;                          -- SDA state
  signal scl_i   : std_logic;                          -- Serial clock internal
  signal sda_m_i : std_logic;                          -- Serial from master

  constant c_idle         : std_logic_vector(4 downto 0) := '0' & X"0";
  constant c_start_1      : std_logic_vector(4 downto 0) := '0' & X"1";
  constant c_start_2      : std_logic_vector(4 downto 0) := '0' & X"2";
  constant c_latch_1      : std_logic_vector(4 downto 0) := '0' & X"3";
  constant c_latch_2      : std_logic_vector(4 downto 0) := '0' & X"4";
  constant c_latch_3      : std_logic_vector(4 downto 0) := '0' & X"5";
  constant c_latch_4      : std_logic_vector(4 downto 0) := '0' & X"6";
  constant c_ack_1        : std_logic_vector(4 downto 0) := '0' & X"7";
  constant c_ack_2        : std_logic_vector(4 downto 0) := '0' & X"8";
  constant c_ack_3        : std_logic_vector(4 downto 0) := '0' & X"9";
  constant c_stop_1       : std_logic_vector(4 downto 0) := '0' & X"A";
  constant c_stop_2       : std_logic_vector(4 downto 0) := '0' & X"B";
  constant c_stop_3       : std_logic_vector(4 downto 0) := '0' & X"C";
  constant c_latch_data_1 : std_logic_vector(4 downto 0) := '0' & X"D";
  constant c_latch_data_2 : std_logic_vector(4 downto 0) := '0' & X"E";
  constant c_latch_data_3 : std_logic_vector(4 downto 0) := '0' & X"F";
  constant c_latch_data_4 : std_logic_vector(4 downto 0) := '1' & X"0";
  constant c_ack_m_1      : std_logic_vector(4 downto 0) := '1' & X"1";
  constant c_ack_m_2      : std_logic_vector(4 downto 0) := '1' & X"2";
  constant c_ack_m_3      : std_logic_vector(4 downto 0) := '1' & X"3";
  constant c_ack_m_4      : std_logic_vector(4 downto 0) := '1' & X"4";
  constant c_n_ack_m_2    : std_logic_vector(4 downto 0) := '1' & X"5";
  constant c_n_ack_m_3    : std_logic_vector(4 downto 0) := '1' & X"6";
  constant c_n_ack_m_4    : std_logic_vector(4 downto 0) := '1' & X"7";
  constant c_others       : std_logic_vector(4 downto 0) := '1' & X"8";
begin  -- rtl

  -- purpose: Update the state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st
  -- outputs: st
  update_state : process (clk, rstb)
  begin  -- process update_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clk_en = '1' then
        st <= nx_st;
      end if;
    end if;
  end process update_state;


  -- purpose: Output/input data from I2C
  -- type   : combinational
  -- inputs : st, start, stop, cnt_8, rw, width, new_data, cnt_2, sda
  -- outputs: 
  fsm : process (st, start, stop, cnt_8, rw, width, new_data, cnt_2, sda)
  begin  -- process fsm

    nx_st <= st;

    case st is
      when idle =>
        state      <= c_idle;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '1';
        sel_sda    <= sel_master;
        valid      <= '0';
        if start = '1' then             -- Got start condition
          nx_st <= start_1;
        else
          nx_st <= idle;
        end if;

      when start_1 =>
        state      <= c_start_1;
        clear      <= '1';              -- Clear the serializer
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= start_2;

      when start_2 =>
        state      <= c_start_2;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '1';              -- Load in 1 byte in serializer
        error      <= '0';
        load       <= '1';              -- Load in 1 byte in serializer
        ready      <= '1';
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '0';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= latch_1;

      when latch_1 =>
        state      <= c_latch_1;        -- Do nothing?
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '0';
        sel_sda    <= sel_ser;          -- Select serial
        valid      <= '0';
        nx_st      <= latch_2;

      when latch_2 =>
        state      <= c_latch_2;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';
        sel_sda    <= sel_ser;          -- Select serial
        valid      <= '0';
        nx_st      <= latch_3;

      when latch_3 =>
        state      <= c_latch_3;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '1';
        sel_sda    <= sel_ser;          -- Select serial
        valid      <= '0';
        nx_st      <= latch_4;

      when latch_4 =>
        state      <= c_latch_4;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '1';              -- Shift in one bit
        error      <= '0';
        load       <= '0';
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '0';
        if cnt_8 = '1' then             -- Got 8 bits from serializer
          ready   <= '1';
          sel_sda <= sel_adc;           -- Let ADC respond
          nx_st   <= ack_1;             -- Wait for ack.
          if cnt_2 = "00" then
            valid <= '1';               -- Got two words
          else
            valid <= '0';
          end if;
        else
          ready   <= '0';
          sel_sda <= sel_ser;
          valid   <= '0';
          nx_st   <= latch_1;           -- Go back, and wait for data
        end if;

      when ack_1 =>
        state      <= c_ack_1;          -- wait for ack from ADC
        clear      <= '1';              -- Clear serializer
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';              -- Don't care
        sel_sda    <= sel_adc;          -- ADC should respond
        valid      <= '0';
        nx_st      <= ack_2;

      when ack_2 =>
        state      <= c_ack_2;          -- Wait for respond
        clear      <= '1';              -- Clear seralizier
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';              -- Don't care
        sel_sda    <= sel_adc;          -- ADC should respond
        valid      <= '0';
        nx_st      <= ack_3;

      when ack_3 =>
        state      <= c_ack_3;          -- Wait for ADC to ack
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';              -- Don't care
        sel_sda    <= sel_adc;          -- ADC need to send data
        valid      <= '0';
        nx_st      <= stop_1;

      when stop_1 =>
        state      <= c_stop_1;         -- We should set a stop
        clear      <= '0';
        data_valid <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '0';              -- Don't care
        valid      <= '0';
        if sda = '1' then
          error    <= '1';              -- Error - no ack
          en_cnt_2 <= '0';
          enable   <= '0';
          load     <= '0';
          sel_sda  <= sel_master;
          nx_st    <= idle;
        elsif stop = '1' then
          error    <= '0';
          en_cnt_2 <= '0';
          enable   <= '0';
          load     <= '0';
          sel_sda  <= sel_master;
          nx_st    <= stop_2;
        elsif rw = '0' and stop = '0' and new_data = '1' then
          error    <= '0';
          en_cnt_2 <= '1';              -- count to 2
          enable   <= '1';              -- Load data into serializer
          load     <= '1';              -- Load data into serializer
          sel_sda  <= sel_ser;
          nx_st    <= latch_1;          -- Write data into slave
        elsif rw = '1' and stop = '0' and new_data = '1' then
          error    <= '0';
          en_cnt_2 <= '0';
          enable   <= '1';              -- shift 1 bit
          load     <= '0';
          sel_sda  <= sel_adc;
          nx_st    <= latch_data_1;     -- Read data from ADC
        else
          error    <= '0';
          en_cnt_2 <= '0';
          enable   <= '0';              -- shift 1 bit
          load     <= '0';
          sel_sda  <= sel_adc;
          nx_st    <= stop_1;           -- Wait
        end if;

      when stop_2 =>
        state      <= c_stop_2;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';              -- Got it all 
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '0';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= stop_3;

      when stop_3 =>
        state      <= c_stop_3;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '1';              -- Got it all 
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= idle;             -- We're done

      when latch_data_1 =>
        state      <= c_latch_data_1;   -- Reading data from ADC
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '0';
        sel_sda    <= sel_adc;          -- Read from ADC
        valid      <= '0';
        nx_st      <= latch_data_2;

      when latch_data_2 =>
        state      <= c_latch_data_2;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';
        sel_sda    <= sel_adc;          -- Read from ADC
        valid      <= '0';
        nx_st      <= latch_data_3;

      when latch_data_3 =>
        state      <= c_latch_data_3;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';
        sel_sda    <= sel_adc;          -- Read from ADC
        valid      <= '0';
        nx_st      <= latch_data_4;

      when latch_data_4 =>
        state      <= c_latch_data_4;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '1';              -- Shift one bit
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '0';
        valid      <= '0';
        if cnt_8 = '0' then             -- Not 8 bits yet
          ready   <= '0';
          sel_sda <= sel_adc;           -- Read from ADC
          nx_st   <= latch_data_1;      -- Read more
        else
          ready   <= '1';               -- Got one bye
          sel_sda <= sel_master;
          nx_st   <= ack_m_1;           -- Make ack
        end if;

      when ack_m_1 =>
        state      <= c_ack_m_1;
        clear      <= '1';                 -- Clear shift register
        data_valid <= '1';                 -- Valid data
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        scl_i      <= '0';                 -- FIXME: Should this be '1'?
        sel_sda    <= sel_master;
        valid      <= '0';
        if width = '0' or stop = '1' then  -- Only one word needed
          s_en    <= '0';
          sda_m_i <= '1';
          nx_st   <= n_ack_m_2;            -- No reply to ADC
        else
          s_en    <= '1';                  -- Count to 2
          sda_m_i <= '0';                  -- Read more
          nx_st   <= ack_m_2;              -- Reply to ADC
        end if;

      when ack_m_2 =>
        state      <= c_ack_m_2;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= ack_m_3;

      when ack_m_3 =>
        state      <= c_ack_m_3;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '0';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= ack_m_4;

      when ack_m_4 =>
        state      <= c_ack_m_4;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        error      <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '0';
        sel_sda    <= sel_master;
        valid      <= '0';
        if new_data = '1' then          -- Got more from sequencer
          enable <= '1';                -- Shift 1 bit in serializer
          load   <= '0';
          nx_st  <= latch_data_1;       -- Read more from ADC
        else
          enable <= '0';
          load   <= '0';
          nx_st  <= ack_m_4;            -- Wait 
        end if;

      when n_ack_m_2 =>
        state      <= c_n_ack_m_2;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '1';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= n_ack_m_3;

      when n_ack_m_3 =>
        state      <= c_n_ack_m_3;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '1';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= n_ack_m_4;

      when n_ack_m_4 =>
        state      <= c_n_ack_m_4;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '0';
        sda_m_i    <= '1';
        sel_sda    <= sel_master;
        valid      <= '0';
        if width = '0' or stop = '1' then
          nx_st <= stop_2;              -- no more data to write, end process
        else
          nx_st <= n_ack_m_4;           -- Wait
        end if;

      when others =>
        state      <= c_others;
        clear      <= '0';
        data_valid <= '0';
        en_cnt_2   <= '0';
        enable     <= '0';
        error      <= '0';
        load       <= '0';
        ready      <= '0';
        s_en       <= '0';
        scl_i      <= '1';
        sda_m_i    <= '1';
        sel_sda    <= sel_master;
        valid      <= '0';
        nx_st      <= idle;
    end case;
  end process fsm;

  -- purpose: Select input or output
  -- type   : sequential
  -- inputs : clk, rstb, sel_sda
  -- outputs: sel_sda_in, sel_sda_out, sda_in, scl
  select_inout : process (clk, rstb)
  begin  -- process select_inout
    if rstb = '0' then                  -- asynchronous reset (active low)
      sel_sda_in  <= '0';
      sel_sda_out <= '0';
      sda_in      <= '1';
      scl         <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clk_en = '1' then
        case sel_sda is
          when sel_master =>            -- We are master
            sel_sda_in  <= '1';
            sel_sda_out <= '0';
            sda_in      <= sda_m_i;
            scl         <= scl_i;
          when sel_ser =>               -- Serial data out
            sel_sda_in  <= '1';
            sel_sda_out <= '0';
            sda_in      <= sda_ser;
            scl         <= scl_i;
          when sel_adc =>               -- Data from ADC
            sel_sda_in  <= '0';
            sel_sda_out <= '1';
            sda_in      <= '1';
            scl         <= scl_i;
          when others => null;
        end case;
      end if;
    end if;
  end process select_inout;
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package master_sm_pack is
  component master_sm
    port (
      clk         : in  std_logic;
      clk_en      : in  std_logic;
      rstb        : in  std_logic;
      cnt_2       : in  std_logic_vector(1 downto 0);
      cnt_8       : in  std_logic;
      new_data    : in  std_logic;
      rw          : in  std_logic;
      sda_ser     : in  std_logic;
      start       : in  std_logic;
      stop        : in  std_logic;
      sda         : in  std_logic;
      width       : in  std_logic;
      clear       : out std_logic;
      data_valid  : out std_logic;
      en_cnt_2    : out std_logic;
      enable      : out std_logic;
      load        : out std_logic;
      ready       : out std_logic;
      s_en        : out std_logic;
      scl         : out std_logic;
      sda_in      : out std_logic;
      sel_sda_in  : out std_logic;
      sel_sda_out : out std_logic;
      valid       : out std_logic;
      error       : out std_logic;
      state       : out std_logic_vector(4 downto 0));
  end component;
end master_sm_pack;

------------------------------------------------------------------------------
--
-- EOF
--

