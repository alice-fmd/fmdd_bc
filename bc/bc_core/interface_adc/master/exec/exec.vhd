------------------------------------------------------------------------------
-- Title      : Executor
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : exec.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Concatenate bytes to output data of 2 bytes.
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity exec is
  port (
    clk        : in  std_logic;                       -- Clock
    rstb       : in  std_logic;                       -- Async reset
    data       : in  std_logic_vector(7 downto 0);    -- Input bytes
    data_valid : in  std_logic;                       -- Valid data
    s          : in  std_logic;                       -- 1st or 2nd word
    width      : in  std_logic;                       -- 1 or 2 bytes
    en_add     : out std_logic;                       -- Enable data output
    out_reg    : out std_logic_vector(15 downto 0));  -- Output 2 bytes
end exec;

------------------------------------------------------------------------------
architecture rtl of exec is

begin  -- rtl

  -- purpose: Output register data from bytes
  -- type   : sequential
  -- inputs : clk, rstb, data, data_valid, s, width
  -- outputs: en_add, out_reg
  doit : process (clk, rstb)
  begin  -- process doit
    if rstb = '0' then                  -- asynchronous reset (active low)
      en_add  <= '0';
      out_reg <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge

      if data_valid = '1' and width = '0' then
        -- Just one word
        en_add               <= '0';
        out_reg(15 downto 8) <= (others => '0');
        out_reg(7 downto 0)  <= data;
      elsif data_valid = '1' and width = '1' and s = '0' then
        -- Get first word. 
        en_add               <= '0';
        out_reg(15 downto 8) <= data;
      elsif data_valid = '1' and width = '1' and s = '1' then
        -- Get second word. 
        en_add               <= '1';
        out_reg(7 downto 0)  <= data;
      end if;
    end if;
  end process doit;

end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package exec_pack is
  component exec
    port (
      clk        : in  std_logic;
      rstb       : in  std_logic;
      data       : in  std_logic_vector(7 downto 0);
      data_valid : in  std_logic;
      s          : in  std_logic;
      width      : in  std_logic;
      en_add     : out std_logic;
      out_reg    : out std_logic_vector(15 downto 0));
  end component;
end exec_pack;
------------------------------------------------------------------------------
--
-- EOF
--
