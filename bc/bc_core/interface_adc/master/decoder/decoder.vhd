------------------------------------------------------------------------------
-- Title      : Get width of data
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : decoder.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Read the last three bits of the address to figure out the
--              width of the read back data:
--
--              * 000 -> temperature, 16 bits
--              * 001 -> configuration, 8 bits
--              * 010 -> T_hyst setpoint, 16 bits
--              * 011 -> T_oti setpoint, 16 bits
--              * 100 -> ADC,  16 bits
--              * 101 -> config2, 8 bits
--
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity decoder is
  port (
    clk    : in  std_logic;                     -- Clock
    rstb   : in  std_logic;                     -- Async reset
    clk_en : in  std_logic;                     -- Slow clock enable
    valid  : in  std_logic;                     -- Valid data
    data   : in  std_logic_vector(2 downto 0);  -- The address
    rw     : in  std_logic;                     -- Read or Write flag
    width  : out std_logic);                    -- 1 or 2 bytes
end decoder;

------------------------------------------------------------------------------
architecture rtl of decoder is
  signal one_i : boolean;
begin  -- rtl
  -- purpose: Figure out if we need 16 bits or not
  get_it: block
  begin  -- block get_it
    one_i <= (data = "001" or data = "101");
  end block get_it;
  

  -- purpose: Output the width
  -- type   : sequential
  -- inputs : clk, rstb, one_i
  -- outputs: width
  output: process (clk, rstb)
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      width <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clk_en = '1' and valid = '1' and rw = '0' then
        if one_i then
          width <= '0';                 -- 1 byte
        else
          width <= '1';                 -- 2 bytes
        end if;
      end if;
    end if;
  end process output;
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package decoder_pack is
  component decoder
    port (
      clk    : in  std_logic;
      rstb   : in  std_logic;
      clk_en : in  std_logic;
      valid  : in  std_logic;
      data   : in  std_logic_vector(2 downto 0);
      rw     : in  std_logic;
      width  : out std_logic); 
  end component;
end decoder_pack;

------------------------------------------------------------------------------
--
-- EOF
--
