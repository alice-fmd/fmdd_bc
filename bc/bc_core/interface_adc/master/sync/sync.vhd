------------------------------------------------------------------------------
-- Title      : Syncronise output
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : sync.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Syncronise output
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity sync is
  port (
    clk  : in  std_logic;               -- Clock
    rstb : in  std_logic;               -- Async reset
    dstb : in  std_logic;               -- Data to sync
    x2   : out std_logic);              -- Output
end sync;

------------------------------------------------------------------------------
architecture rtl of sync is
  signal r1_i : std_logic;
  signal r2_i : std_logic;
begin  -- rtl
  -- purpose: Output the sync'ed signal
  output: block
  begin  -- block output
    x2 <= not ((not r1_i) or r2_i);
  end block output;
  
  -- purpose: Register and sync
  -- type   : sequential
  -- inputs : clk, rstb, dstb
  -- outputs: r1_i, r2_i
  sync_it: process (clk, rstb)
  begin  -- process sync_it
    if rstb = '0' then                  -- asynchronous reset (active low)
      r1_i <= '0';
      r2_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      r1_i <= dstb;
      r2_i <= r1_i;
    end if;
  end process sync_it;
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package sync_pack is
  component sync
    port (
      clk  : in  std_logic;
      rstb : in  std_logic;
      dstb : in  std_logic;
      x2   : out std_logic);
  end component;
end sync_pack;

------------------------------------------------------------------------------
--
-- EOF
--
