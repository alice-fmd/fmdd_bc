------------------------------------------------------------------------------
-- Title      : Test bench for clock_scl
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : clock_scl_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2006/09/07
-- Platform   : 
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test clock_scl
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.clock_scl_pack.all;

------------------------------------------------------------------------------

entity clock_scl_tb is
end clock_scl_tb;

------------------------------------------------------------------------------

architecture test of clock_scl_tb is
  constant PERIOD : time := 25 ns;      -- A clock cycle

  signal clk_i     : std_logic := '0';
  signal rstb_i    : std_logic := '0';
  signal clk_scl_i : std_logic;
  signal clk_en_i  : std_logic;
begin  -- test

  DUT: clock_scl
    port map (
        clk     => clk_i,
        rstb    => rstb_i,
        clk_scl => clk_scl_i,
        clk_en  => clk_en_i);

  
  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 1 us;
    rstb_i  <= '1';

    wait;                               -- forever 
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;

end test;

------------------------------------------------------------------------------
--
-- EOF
--

