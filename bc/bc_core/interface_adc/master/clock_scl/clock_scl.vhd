------------------------------------------------------------------------------
-- Title      : Make I2C clock
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : clock_scl.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Make an I2C clock.  The generic parameter is the length of a
--              half clock cycle _minus_ 1, in number of input clock cycles.
--              That is,
--
--                                  DIV = 32
--                  clk_scl half-period = 33 clk period
--                       clk_scl period = 66 clk period
--                    clk_scl frequency = 1 / 66 clk period
--
--               If clk's perdiod is 25 ns (a 40 MHz clock), then
--
--                                  DIV = 32
--                  clk_scl half-period = 825 ns
--                       clk_scl period = 1.65 us
--                    clk_scl frequency ~ 606 kHz

------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity clock_scl is
  -- The numbers below is from the TPC code - I have no idea how they
  -- got them
  -- 
  -- Divide by 66 - > T = 6.6us, F = 150kHz
  generic (DIV :     integer := 32);
  port (
    clk        : in  std_logic;         -- Fast (40MHz) clock
    rstb       : in  std_logic;         -- Async reset
    clk_scl    : out std_logic;         -- Output clock
    clk_en     : out std_logic);
end clock_scl;

------------------------------------------------------------------------------
architecture rtl of clock_scl is
  signal     count_i : integer;
begin  -- rtl
  -- purpose: Make slow clock
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: clk_scl
  doit               : process (clk, rstb)
    variable oclk_i  : std_logic;
  begin  -- process doit
    if rstb = '0' then                  -- asynchronous reset (active low)
      count_i    <= 0;
      clk_scl    <= '0';
      clk_en     <= '0';
      oclk_i   := '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if count_i = DIV then
        count_i  <= 0;
        oclk_i := not oclk_i;
        if oclk_i = '1' then
          clk_en <= '1';
        else
          clk_en <= '0';
        end if;
      else
        clk_en   <= '0';
        count_i  <= count_i + 1;
      end if;
      clk_scl    <= oclk_i;
    end if;
  end process doit;

end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package clock_scl_pack is
  component clock_scl
    generic (
      DIV     :     integer := 32);
    port (
      clk     : in  std_logic;
      rstb    : in  std_logic;
      clk_scl : out std_logic;
      clk_en  : out std_logic);
  end component;
end clock_scl_pack;
------------------------------------------------------------------------------
--
-- EOF
--
