------------------------------------------------------------------------------
-- Title      : Test of master for Monitor ADC
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : master_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2006/09/12
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test of the master of the Monitor ADCs. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/10  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.master_pack.all;
use work.clock_scl_pack.all;
use work.ad7417_pack.all;

------------------------------------------------------------------------------

entity master_tb is
end master_tb;

------------------------------------------------------------------------------

architecture test of master_tb is
  constant PERIOD : time                         := 25 ns;
  constant ADD    : std_logic_vector(2 downto 0) := "000";

  signal clk_i         : std_logic;
  signal rstb_i        : std_logic;
  signal data_par_in_i : std_logic_vector(7 downto 0);
  signal new_data_i    : std_logic;
  signal rw_i          : std_logic;
  signal start_i       : std_logic;
  signal stop_i        : std_logic;
  signal width_i       : std_logic;
  signal en_add_i      : std_logic;
  signal data_adc_i    : std_logic_vector(15 downto 0);
  signal ready_seq_i   : std_logic;
  signal data_valid_i  : std_logic;
  signal error_i       : std_logic;
  signal s_i           : std_logic;
  signal scl_i         : std_logic;
  signal sda_i         : std_logic;

begin  -- test
  DUT: master
    port map (
        clk         => clk_i,
        rstb        => rstb_i,
        data_par_in => data_par_in_i,
        new_data    => new_data_i,
        rw          => rw_i,
        start       => start_i,
        stop        => stop_i,
        width       => width_i,
        en_add      => en_add_i,
        data_adc    => data_adc_i,
        ready_seq   => ready_seq_i,
        data_valid  => data_valid_i,
        error       => error_i,
        s           => s_i,
        scl         => scl_i,
        sda         => sda_i);

  adc: ad7417
    generic map (
        ADD => ADD)
    port map (
        scl => scl_i,
        sda => sda_i);


  -- purpose: Make clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clock_gen : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
  
end test;

------------------------------------------------------------------------------
