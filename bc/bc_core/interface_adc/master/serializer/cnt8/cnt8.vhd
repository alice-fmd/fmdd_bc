------------------------------------------------------------------------------
-- Title      : Enable after count of 8
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : cnt8.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Keep the signal en_cnt (cnt = 8) high until clear
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/08  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cnt8 is
  port (
    clk    : in  std_logic;                     -- Clock
    rstb   : in  std_logic;                     -- Async reset
    clear  : in  std_logic;                     -- Sync clear
    cnt    : in  std_logic_vector(3 downto 0);  -- Counter
    en_cnt : out std_logic);                    -- Counted to 8
end cnt8;

------------------------------------------------------------------------------
architecture rtl of cnt8 is
  type state is (s0, s1);               -- States
  signal st     : state;                -- State
  signal nx_st  : state;                -- Next state
  signal cnt8_i : std_logic;            -- Counted to 8
begin  -- rtl
  -- purpose: Combinatorics
  combi         : block
  begin  -- block combi 
    cnt8_i <= '1' when cnt = "1000" else '0';
  end block combi ;
  
  -- purpose: update the state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st
  -- outputs: st
  update_state: process (clk, rstb)
  begin  -- process update_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st <= s0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st <= nx_st;
    end if;
  end process update_state;

  -- purpose: Get the next state based on input
  -- type   : combinational
  -- inputs : st, cnt8_i, clear
  -- outputs: en_cnt
  up_or_clr: process (st, cnt8_i, clear)
  begin  -- process up_or_clr
    nx_st <= st;
    case st is
      when s0 =>
        en_cnt <= '0';
        if cnt8_i = '1' then
          nx_st <= s1;
        else
          nx_st <= s0;
        end if;
      when s1 =>
        en_cnt <= '1';
        if clear = '1' then
          nx_st <= s0;
        else
          nx_st <= s1;
        end if;
      when others => null;
    end case;
  end process up_or_clr;
end rtl;

------------------------------------------------------------------------------
-- Alternative implementation, where normal clock is used, and the slow clock
-- is used as an enable only. 
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity cnt8_en is
  port (
    clk    : in  std_logic;                     -- Clock
    rstb   : in  std_logic;                     -- Async reset
    clear  : in  std_logic;                     -- Sync clear
    enable : in  std_logic;                     -- Enable
    cnt    : in  std_logic_vector(3 downto 0);  -- Counter
    en_cnt : out std_logic);                    -- counted to 8
end cnt8_en;

------------------------------------------------------------------------------
architecture rtl of cnt8_en is
  type state is (s0, s1);               -- States
  signal st     : state;                -- State
  signal nx_st  : state;                -- Next state
  signal cnt8_i : std_logic;            -- Counted to 8
begin  -- rtl
  -- purpose: Combinatorics
  combi         : block
  begin  -- block combi 
    cnt8_i <= '1' when cnt = "1000" else '0';
  end block combi ;

  -- purpose: update the state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st
  -- outputs: st
  update_state: process (clk, rstb)
  begin  -- process update_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st <= s0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if enable = '1' then
        st <= nx_st;
      end if;
    end if;
  end process update_state;

  -- purpose: Get the next state based on input
  -- type   : combinational
  -- inputs : st, cnt8_i, clear
  -- outputs: en_cnt
  up_or_clr: process (st, cnt8_i, clear, enable)
  begin  -- process up_or_clr
    if enable = '1' then      
      nx_st <= st;
      case st is
        when s0 =>
          en_cnt <= '0';
          if cnt8_i = '1' then
            nx_st <= s1;
          else
            nx_st <= s0;
          end if;
        when s1 =>
          en_cnt <= '1';
          if clear = '1' then
            nx_st <= s0;
          else
            nx_st <= s1;
          end if;
        when others => null;
      end case;
    end if;
  end process up_or_clr;
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package cnt8_pack is
  component cnt8
    port (
      clk    : in  std_logic;
      rstb   : in  std_logic;
      clear  : in  std_logic;
      cnt    : in  std_logic_vector(3 downto 0);
      en_cnt : out std_logic);
  end component;
  component cnt8_en
    port (
      clk    : in  std_logic;
      rstb   : in  std_logic;
      clear  : in  std_logic;
      enable : in  std_logic;
      cnt    : in  std_logic_vector(3 downto 0);
      en_cnt : out std_logic); 
  end component;
end cnt8_pack;

------------------------------------------------------------------------------
--
-- EOF
--
