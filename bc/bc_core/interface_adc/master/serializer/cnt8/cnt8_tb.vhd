------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.cnt8_pack.all;
use work.clock_scl_pack.all;

------------------------------------------------------------------------------
entity cnt8_tb is
end cnt8_tb;

------------------------------------------------------------------------------

architecture test of cnt8_tb is
  constant PERIOD : time := 25 ns;      -- A clock cycle

  signal clk_i       : std_logic            := '0';
  signal sclk_i      : std_logic;
  signal rstb_i      : std_logic            := '0';
  signal clear_i     : std_logic            := '0';
  signal enable_i    : std_logic            := '0';
  signal cnt_i       : std_logic_vector(3 downto 0);
  signal ucnt_i      : unsigned(3 downto 0) := "0000";
  signal en_cnt_i    : std_logic;
  signal en_en_cnt_i : std_logic;
  signal i           : integer;

begin  -- test

  DUT: cnt8
    port map (
        clk    => sclk_i,
        rstb   => rstb_i,
        clear  => clear_i,
        cnt    => cnt_i,
        en_cnt => en_cnt_i);

  DUT_en : cnt8_en
    port map (
        clk    => clk_i,
        rstb   => rstb_i,
        clear  => clear_i,
        enable => enable_i,
        cnt    => cnt_i,
        en_cnt => en_en_cnt_i);


  scl_clk: clock_scl
    generic map (
        DIV => 32)
    port map (
        clk     => clk_i,
        rstb    => rstb_i,
        clk_scl => sclk_i,
        clk_en  => enable_i);


  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 1 us;
    rstb_i  <= '1';

    wait until rising_edge(sclk_i);
    for i in 0 to 10 loop
      wait until rising_edge(sclk_i);
      ucnt_i <= ucnt_i + 1;
      wait until rising_edge(sclk_i);
    end loop;  -- i
    wait until rising_edge(sclk_i);
    wait until rising_edge(sclk_i);

    ucnt_i <= "0000";
    clear_i <= '1';
    wait until rising_edge(sclk_i);
    clear_i <= '0';

    -- wait;                               -- forever 
  end process stimuli;

  cnt_i <= std_logic_vector(ucnt_i);
  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
  
end test;

------------------------------------------------------------------------------
