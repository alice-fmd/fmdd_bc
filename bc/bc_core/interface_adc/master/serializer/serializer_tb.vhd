------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.serializer_pack.all;
------------------------------------------------------------------------------
entity serializer_tb is
end serializer_tb;

------------------------------------------------------------------------------

architecture test of serializer_tb is
  constant PERIOD : time    := 25 ns;   -- Clock cycle

  signal clk_i          : std_logic                    := '0';
  signal rstb_i         : std_logic                    := '0';
  signal clear_i        : std_logic                    := '0';
  signal load_i         : std_logic                    := '0';
  signal enable_i       : std_logic                    := '0';
  signal data_ser_in_i  : std_logic                    := '1';
  signal data_par_in_i  : std_logic_vector(7 downto 0) := "10101010";
  signal data_ser_out_i : std_logic;
  signal data_par_out_i : std_logic_vector(7 downto 0);
  signal en_cnt_i       : std_logic;
  signal i              : integer;
begin  -- test

  DUT: serializer
    port map (
        clk          => clk_i,
        rstb         => rstb_i,
        clear        => clear_i,
        load         => load_i,
        enable       => enable_i,
        data_ser_in  => data_ser_in_i,
        data_par_in  => data_par_in_i,
        data_ser_out => data_ser_out_i,
        data_par_out => data_par_out_i,
        en_cnt       => en_cnt_i);

  
  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';
    wait for 100 ns;

    wait until rising_edge(clk_i);
    enable_i <= '1';
    load_i   <= '1';
    wait until rising_edge(clk_i);
    load_i   <= '0';
    enable_i <= '0';

    
    wait until rising_edge(clk_i);
    wait until rising_edge(clk_i);
    for i in 0 to 10 loop
      wait until rising_edge(clk_i);
      enable_i  <= '1';
      wait until rising_edge(clk_i);
      enable_i  <= '0';
    end loop;  -- i

    wait until rising_edge(clk_i);
    clear_i <= '1';  
    wait until rising_edge(clk_i);
    clear_i <= '0';

    wait;                               -- forever
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;

end test;

------------------------------------------------------------------------------
