------------------------------------------------------------------------------
-- Title      : Serialize data into bits
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : serializer.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Serialize the Monitor ADC I2C instructions into bits
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/08  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cnt8_pack.all;

entity serializer is
  port (
    clk          : in  std_logic;       -- Clock
    rstb         : in  std_logic;       -- Async reset
    clear        : in  std_logic;       -- Sync clear
    load         : in  std_logic;       -- Load into shift register
    enable       : in  std_logic;       -- Shift one to the left
    data_ser_in  : in  std_logic;       -- Serial data input
    data_par_in  : in  std_logic_vector(7 downto 0);  -- Parallel data input
    data_ser_out : out std_logic;       -- Serial data out
    data_par_out : out std_logic_vector(7 downto 0);  -- Parallel data out
    en_cnt       : out std_logic);      -- Counted to 8
end serializer;

------------------------------------------------------------------------------
architecture rtl of serializer is
  -- 4 bit counter
  signal cnt_i : std_logic_vector(3 downto 0) := (others => '0');
begin  -- rtl
  -- Output en_cnt once we've counted to 8.
  got_it: cnt8
    port map (
        clk    => clk,
        rstb   => rstb,
        clear  => clear,
        cnt    => cnt_i,
        en_cnt => en_cnt);
  
  -- purpose: Shift register
  -- type   : sequential
  -- inputs : clk, rstb, enable, data_par_in, data_ser_in
  -- outputs: data_par_out, data_ser_out
  shift_it         : process (clk, rstb)
    variable tmp_i : std_logic_vector(7 downto 0);
  begin  -- process shift_it
    if rstb = '0' then                -- asynchronous reset (active low)
      data_par_out <= (others => '0');
      data_ser_out <= '0';
      tmp_i := (others        => '0');

    elsif clk'event and clk = '1' then  -- rising clock edge
      if enable = '1' then
        if load = '1' then
          tmp_i             := data_par_in;
        else
          tmp_i(7 downto 1) := tmp_i(6 downto 0);
          tmp_i(0)          := data_ser_in;
        end if;
        data_par_out <= tmp_i;
        data_ser_out <= tmp_i(7);
      end if;
    end if;
  end process shift_it;

  -- purpose: 4 bit counter
  -- type   : sequential
  -- inputs : clk, rstb, clear, enable
  -- outputs: cnt_i
  counter: process (clk, rstb)
    variable tmp_i : unsigned(3 downto 0);
  begin  -- process counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i <= (others => '0');
      tmp_i := (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clear = '1' then
        tmp_i := (others => '0');
      elsif enable = '1' then
        if tmp_i = "1111" then
          tmp_i := (others => '0');
        else
          tmp_i := tmp_i + 1;
        end if;
      end if;
      cnt_i <= std_logic_vector(tmp_i);
    end if;
  end process counter;
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package serializer_pack is
  component serializer
    port (
      clk          : in  std_logic;
      rstb         : in  std_logic;
      clear        : in  std_logic;
      load         : in  std_logic;
      enable       : in  std_logic;
      data_ser_in  : in  std_logic;
      data_par_in  : in  std_logic_vector(7 downto 0);
      data_ser_out : out std_logic;
      data_par_out : out std_logic_vector(7 downto 0);
      en_cnt       : out std_logic);
  end component;
end serializer_pack;
------------------------------------------------------------------------------
--
-- EOF
--
