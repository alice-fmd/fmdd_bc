------------------------------------------------------------------------------
-- Title      : Master of Monitor ADC I2C bus
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : master.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2009-11-18
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Control master of Monitor ADC I2C bus
--
-- This component is taking care of communicating with the Monitor ADC's on
-- the front end board.  The communication protocol is serial I2C. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sync_pack.all;
use work.exec_pack.all;
use work.clock_scl_pack.all;
use work.decoder_pack.all;
use work.serializer_pack.all;
use work.master_sm_pack.all;

entity master is
  generic (
    MSCL_DIV : natural := 32);          -- Slow clock division
  port (
    clk         : in    std_logic;      -- Clock
    rstb        : in    std_logic;      -- Async reset
    data_par_in : in    std_logic_vector(7 downto 0);   -- instructions input
    new_data    : in    std_logic;      -- Got new instruction
    rw          : in    std_logic;      -- Read/Write flag
    start       : in    std_logic;      -- Start processing
    stop        : in    std_logic;      -- Stop processing
    width       : out   std_logic;      -- 1 or 2 bytes
    en_add      : out   std_logic;      -- Enable for address
    data_adc    : out   std_logic_vector(15 downto 0);  -- Data to registter
    ready_seq   : out   std_logic;      -- Ready flag to sequencer
    data_valid  : out   std_logic;      -- Valid data
    error       : out   std_logic;      -- Error flag
    s           : out   std_logic;      -- 1st or 2nd word
    scl         : out   std_logic;      -- serial clock
    sda         : inout std_logic;      -- Serial data
    state       : out   std_logic_vector(4 downto 0));
end master;

------------------------------------------------------------------------------
architecture rtl of master is
  signal clk_scl_i      : std_logic;    -- Slower clock
  signal clk_en_i       : std_logic;    -- Rising edge slower clock
  signal cnt_2_i        : std_logic_vector(1 downto 0);  -- Count to 3
  signal s_en_i         : std_logic;    -- Enable s counter
  signal en_2_i         : std_logic;    -- Enable 2bit counter
  signal s_i            : std_logic;    -- 1st or 2nd word
  signal width_i        : std_logic;    -- 1 or 2 words
  signal data_par_out_i : std_logic_vector(7 downto 0);  -- Output
  signal master_ready_i : std_logic;    -- master is ready
  signal data_valid_i   : std_logic;    -- Got one word
  signal clear_i        : std_logic;    -- Clear from master
  signal load_i         : std_logic;    -- Load from master
  signal enable_i       : std_logic;    -- Enable from master
  signal data_ser_out_i : std_logic;    -- to master
  signal en_cnt_i       : std_logic;    -- Have all 8 from serializer
  signal data_ser_in_i  : std_logic;    -- to master
  signal sda_i          : std_logic;    -- Internal serial data
  signal sel_sda_in_i   : std_logic;    -- Select Serial data in
  signal sel_sda_out_i  : std_logic;    -- Select Serial data out
  signal sda_in_i       : std_logic;    -- Ouput serial data
  signal valid_i        : std_logic;    -- Got valid instructions
  signal scl_i          : std_logic;    -- Serial clock to tristate
  signal en_data2_i     : std_logic;    -- Enable data on bustri2
  signal en_tri2_i      : std_logic;    -- Enable tri-data on bustri2
  signal en_data1_i     : std_logic;    -- Enable tri-data on bustri1

begin  -- rtl

  clock_scale : clock_scl
    generic map (
      DIV     => MSCL_DIV)
    port map (
      clk     => clk,
      rstb    => rstb,
      clk_scl => clk_scl_i,
      clk_en  => clk_en_i);

  serialize : serializer                
    port map (
        clk          => clk_scl_i,
        rstb         => rstb,
        clear        => clear_i,
        load         => load_i,
        enable       => enable_i,
        data_ser_in  => data_ser_in_i,
        data_par_in  => data_par_in,
        data_ser_out => data_ser_out_i,
        data_par_out => data_par_out_i,
        en_cnt       => en_cnt_i);

  mst : master_sm
    port map (
        clk         => clk_scl_i,
        clk_en      => '1',             -- clk_en_i,
        rstb        => rstb,
        cnt_2       => cnt_2_i,
        cnt_8       => en_cnt_i,
        new_data    => new_data,        -- From squencer
        rw          => rw,
        sda_ser     => data_ser_out_i,  -- Output from serializer
        start       => start,           -- From squencer
        stop        => stop,            -- From squencer
        sda         => sda_i,           -- Before tri-state
        width       => width_i,         -- From serializer
        clear       => clear_i,         -- Clear serializer
        data_valid  => data_valid_i,    -- Got Valid data
        en_cnt_2    => en_2_i,          -- Enable count-to-2
        enable      => enable_i,        -- Shift-in in serializer
        load        => load_i,          -- Load new word in serializer
        ready       => master_ready_i,  -- Ready for more
        s_en        => s_en_i,          -- Enable s counter
        scl         => scl_i,           -- Output slow clock (to tri-state)
        sda_in      => sda_in_i,        -- Serial data in (to tri-state)
        sel_sda_in  => sel_sda_in_i,    -- Select serial data input
        sel_sda_out => sel_sda_out_i,   -- Select serial data output
        valid       => valid_i,         -- Valid data
        error       => error,           -- Error flag
        state       => state);

  decode : decoder
    port map (
      clk    => clk_scl_i,
      rstb   => rstb,
      clk_en => '1', -- clk_en_i,
      valid  => valid_i,
      data   => data_par_in(2 downto 0),
      rw     => rw,
      width  => width_i);

  executor : exec
    port map (
      clk        => clk,
      rstb       => rstb,
      data       => data_par_out_i,
      data_valid => data_valid_i,
      s          => s_i,
      width      => width_i,
      en_add     => en_add,
      out_reg    => data_adc);

  sync_it : sync
    port map (
      clk  => clk,
      rstb => rstb,
      dstb => master_ready_i,
      x2   => ready_seq);


  -- purpose: Implement a tri-state buffer - uni-directional (open collector)
  mytri1: block
  begin  -- block mytri1
    scl <= '0' when en_data1_i = '1' else 'Z';
  end block mytri1;


  -- purpose: Implement a tri-state buffer - bi-directional
  mytri2 : block
  begin  -- block mytri2
    sda           <= '0' when en_data2_i = '1' else 'Z';
    data_ser_in_i <= sda when en_tri2_i = '1'  else 'Z';
  end block mytri2;

  -- purpose: combinatorics
  combi : block
  begin  -- block combi
    s          <= s_i;
    width      <= width_i;
    data_valid <= data_valid_i;
    en_data2_i <= '1' when sda_in_i = '0' and sel_sda_in_i = '1' else '0';
    en_tri2_i  <= sel_sda_out_i;
    en_data1_i <= '1' when scl_i = '0'                           else '0';
    sda_i      <= sda;
  end block combi;


  -- purpose: Flip S on clk_scl
  -- type   : sequential
  -- inputs : clk, rstb, clk_en_i, s_en_i, start
  -- outputs: s
  s_counter        : process (clk_scl_i, rstb)
    variable cnt_i : std_logic;
  begin  -- process s_counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      s_i <= '0';
      cnt_i   := '0';
    elsif clk_scl_i'event and clk_scl_i = '1' then  -- rising clock edge
      if start = '1' then
        cnt_i := '0';
      elsif s_en_i = '1' and clk_en_i = '1' then
        cnt_i := not cnt_i;
      end if;
      s_i <= cnt_i;
    end if;
  end process s_counter;

  -- purpose: count up to 1 from 0
  -- type   : sequential
  -- inputs : clk, rstb, start_i
  -- outputs:  clk_en_i, en_2_i, start
  count_2          : process (clk, rstb)
    variable cnt_i : integer range 0 to 3;
  begin  -- process count_2
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i     := 0;
      cnt_2_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if start = '1' then
        cnt_i   := 0;
      elsif en_2_i = '1' and clk_en_i = '1' then
        if cnt_i = 3 then
          cnt_i := 0;
        else
          cnt_i := cnt_i + 1;
        end if;
      end if;
      cnt_2_i <= std_logic_vector(to_unsigned(cnt_i,2));
    end if;
  end process count_2;
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package master_pack is
  component master
    generic (
      MSCL_DIV : natural := 32);        -- Slow clock division
    port (
      clk         : in    std_logic;
      rstb        : in    std_logic;
      data_par_in : in    std_logic_vector(7 downto 0);
      new_data    : in    std_logic;
      rw          : in    std_logic;
      start       : in    std_logic;
      stop        : in    std_logic;
      width       : out   std_logic;
      en_add      : out   std_logic;
      data_adc    : out   std_logic_vector(15 downto 0);
      ready_seq   : out   std_logic;
      data_valid  : out   std_logic;
      error       : out   std_logic;
      s           : out   std_logic;
      scl         : out   std_logic;
      sda         : inout std_logic;
      state       : out   std_logic_vector(4 downto 0));
  end component;
end master_pack;
------------------------------------------------------------------------------
--
-- EOF
--
