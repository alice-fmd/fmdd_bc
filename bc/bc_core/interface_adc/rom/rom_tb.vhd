------------------------------------------------------------------------------
--
-- Test bench for ROM of Monitor instructions
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.rom_pack.all;

------------------------------------------------------------------------------
entity rom_tb is
end rom_tb;

------------------------------------------------------------------------------

architecture test of rom_tb is
  constant PERIOD : time    := 25 ns;   -- Clock cycle
  constant MAX    : integer := 2 ** 6 - 1;

  signal clk_i  : std_logic := '0';
  signal add_i  : std_logic_vector(9 downto 0) := (others => '0');
  signal data_i : std_logic_vector(10 downto 0);
  signal i      : integer;              -- Counter

begin  -- test

  DUT: rom
    port map (
        inclk  => clk_i,
        outclk => clk_i,
        add    => add_i,
        data   => data_i);

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 100 ns;

    for i in 0 to MAX loop
      wait until rising_edge(clk_i);
      add_i <= std_logic_vector(to_unsigned(i, 7));
      wait until rising_edge(clk_i);
    end loop;  -- i

    wait;                               -- forever
  end process stimuli;
  
  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
end test;

------------------------------------------------------------------------------
--
-- EOF
-- 
