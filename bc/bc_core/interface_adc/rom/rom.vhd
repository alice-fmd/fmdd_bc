------------------------------------------------------------------------------
-- Title      : Monitor ADC instructions
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : rom.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2008-08-15
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: ROM of Monitor ADC instructions
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom is
  port (
    rstb   : in  std_logic;                       -- Reset
    inclk  : in  std_logic;                       -- Input clock
    outclk : in  std_logic;                       -- Output clock
    add    : in  std_logic_vector(9 downto 0);    -- Adress
    data   : out std_logic_vector(10 downto 0));  -- Data
end rom;

------------------------------------------------------------------------------
architecture rtl3 of rom is
  constant MAX   : natural := 281;          -- Size of ROM
  signal   add_i : integer range 0 to MAX;
begin  -- rtl3
  -- purpose: Register address
  -- type   : sequential
  -- inputs : inclk, add
  -- outputs: data
  take_address : process (inclk, rstb)
  begin  -- process output
    if rstb = '0' then
      add_i <= 0;
    elsif inclk'event and inclk = '1' then  -- rising clock edge
      add_i <= to_integer(unsigned(add));
    end if;
  end process take_address;

  --
  make_output: process (outclk)
  begin  -- process make_output
    if outclk'event and outclk = '1' then  -- rising clock edge
      -- ADC 0
      case add_i is
        when 0   => data <= ("110" & "00000000");            -- Start
        when 1   => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 2   => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 3   => data <= ("010" & "000" & "00010");       -- Cfg T
        when 4   => data <= ("001" & "00000000");            -- Stop
        when 5   => data <= ("110" & "00000000");            -- Start
        when 6   => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 7   => data <= ("010" & "00000" & "000");       -- write -> T
        when 8   => data <= ("001" & "00000000");            -- Stop
        when 9   => data <= ("110" & "00000000");            -- Start
        when 10  => data <= ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
        when 11  => data <= ("011" & "00000000");            -- Read
        when 12  => data <= ("011" & "00000000");            -- Read
        when 13  => data <= ("001" & "00000000");            -- Stop
        when 14  => data <= ("110" & "00000000");            -- Start
        when 15  => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 16  => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 17  => data <= ("010" & "001" & "00010");       -- Cfg ADC1
        when 18  => data <= ("001" & "00000000");            -- Stop
        when 19  => data <= ("110" & "00000000");            -- Start
        when 20  => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 21  => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 22  => data <= ("001" & "00000000");            -- Stop
        when 23  => data <= ("110" & "00000000");            -- Start
        when 24  => data <= ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
        when 25  => data <= ("011" & "00000000");            -- Read
        when 26  => data <= ("011" & "00000000");            -- Read
        when 27  => data <= ("001" & "00000000");            -- Stop
        when 28  => data <= ("110" & "00000000");            -- Start
        when 29  => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 30  => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 31  => data <= ("010" & "010" & "00010");       -- Cfg ADC2
        when 32  => data <= ("001" & "00000000");            -- Stop
        when 33  => data <= ("110" & "00000000");            -- Start
        when 34  => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 35  => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 36  => data <= ("001" & "00000000");            -- Stop
        when 37  => data <= ("110" & "00000000");            -- Start
        when 38  => data <= ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
        when 39  => data <= ("011" & "00000000");            -- Read
        when 40  => data <= ("011" & "00000000");            -- Read
        when 41  => data <= ("001" & "00000000");            -- Stop
        when 42  => data <= ("110" & "00000000");            -- Start
        when 43  => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 44  => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 45  => data <= ("010" & "011" & "00010");       -- Cfg ADC3
        when 46  => data <= ("001" & "00000000");            -- Stop
        when 47  => data <= ("110" & "00000000");            -- Start
        when 48  => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 49  => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 50  => data <= ("001" & "00000000");            -- Stop
        when 51  => data <= ("110" & "00000000");            -- Start
        when 52  => data <= ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
        when 53  => data <= ("011" & "00000000");            -- Read
        when 54  => data <= ("011" & "00000000");            -- Read
        when 55  => data <= ("001" & "00000000");            -- Stop
        when 56  => data <= ("110" & "00000000");            -- Start
        when 57  => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 58  => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 59  => data <= ("010" & "100" & "00010");       -- Cfg ADC4
        when 60  => data <= ("001" & "00000000");            -- Stop
        when 61  => data <= ("110" & "00000000");            -- Start
        when 62  => data <= ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
        when 63  => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 64  => data <= ("001" & "00000000");            -- Stop
        when 65  => data <= ("110" & "00000000");            -- Start
        when 66  => data <= ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
        when 67  => data <= ("011" & "00000000");            -- Read
        when 68  => data <= ("011" & "00000000");            -- Read
        when 69  => data <= ("001" & "00000000");            -- Stop
                    -- ADC 1
        when 70  => data <= ("110" & "00000000");            -- Start
        when 71  => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 72  => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 73  => data <= ("010" & "000" & "00010");       -- Cfg T
        when 74  => data <= ("001" & "00000000");            -- Stop
        when 75  => data <= ("110" & "00000000");            -- Start
        when 76  => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 77  => data <= ("010" & "00000" & "000");       -- write -> T
        when 78  => data <= ("001" & "00000000");            -- Stop
        when 79  => data <= ("110" & "00000000");            -- Start
        when 80  => data <= ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
        when 81  => data <= ("011" & "00000000");            -- Read
        when 82  => data <= ("011" & "00000000");            -- Read
        when 83  => data <= ("001" & "00000000");            -- Stop
        when 84  => data <= ("110" & "00000000");            -- Start
        when 85  => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 86  => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 87  => data <= ("010" & "001" & "00010");       -- Cfg ADC1
        when 88  => data <= ("001" & "00000000");            -- Stop
        when 89  => data <= ("110" & "00000000");            -- Start
        when 90  => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 91  => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 92  => data <= ("001" & "00000000");            -- Stop
        when 93  => data <= ("110" & "00000000");            -- Start
        when 94  => data <= ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
        when 95  => data <= ("011" & "00000000");            -- Read
        when 96  => data <= ("011" & "00000000");            -- Read
        when 97  => data <= ("001" & "00000000");            -- Stop
        when 98  => data <= ("110" & "00000000");            -- Start
        when 99  => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 100 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 101 => data <= ("010" & "010" & "00010");       -- Cfg ADC2
        when 102 => data <= ("001" & "00000000");            -- Stop
        when 103 => data <= ("110" & "00000000");            -- Start
        when 104 => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 105 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 106 => data <= ("001" & "00000000");            -- Stop
        when 107 => data <= ("110" & "00000000");            -- Start
        when 108 => data <= ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
        when 109 => data <= ("011" & "00000000");            -- Read
        when 110 => data <= ("011" & "00000000");            -- Read
        when 111 => data <= ("001" & "00000000");            -- Stop
        when 112 => data <= ("110" & "00000000");            -- Start
        when 113 => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 114 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 115 => data <= ("010" & "011" & "00010");       -- Cfg ADC3
        when 116 => data <= ("001" & "00000000");            -- Stop
        when 117 => data <= ("110" & "00000000");            -- Start
        when 118 => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 119 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 120 => data <= ("001" & "00000000");            -- Stop
        when 121 => data <= ("110" & "00000000");            -- Start
        when 122 => data <= ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
        when 123 => data <= ("011" & "00000000");            -- Read
        when 124 => data <= ("011" & "00000000");            -- Read
        when 125 => data <= ("001" & "00000000");            -- Stop
        when 126 => data <= ("110" & "00000000");            -- Start
        when 127 => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 128 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 129 => data <= ("010" & "100" & "00010");       -- Cfg ADC4
        when 130 => data <= ("001" & "00000000");            -- Stop
        when 131 => data <= ("110" & "00000000");            -- Start
        when 132 => data <= ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
        when 133 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 134 => data <= ("001" & "00000000");            -- Stop
        when 135 => data <= ("110" & "00000000");            -- Start
        when 136 => data <= ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
        when 137 => data <= ("011" & "00000000");            -- Read
        when 138 => data <= ("011" & "00000000");            -- Read
        when 139 => data <= ("001" & "00000000");            -- Stop
                    -- ADC 2
        when 140 => data <= ("110" & "00000000");            -- Start
        when 141 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 142 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 143 => data <= ("010" & "000" & "00010");       -- Cfg T
        when 144 => data <= ("001" & "00000000");            -- Stop
        when 145 => data <= ("110" & "00000000");            -- Start
        when 146 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 147 => data <= ("010" & "00000" & "000");       -- write -> T
        when 148 => data <= ("001" & "00000000");            -- Stop
        when 149 => data <= ("110" & "00000000");            -- Start
        when 150 => data <= ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
        when 151 => data <= ("011" & "00000000");            -- Read
        when 152 => data <= ("011" & "00000000");            -- Read
        when 153 => data <= ("001" & "00000000");            -- Stop
        when 154 => data <= ("110" & "00000000");            -- Start
        when 155 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 156 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 157 => data <= ("010" & "001" & "00010");       -- Cfg ADC1
        when 158 => data <= ("001" & "00000000");            -- Stop
        when 159 => data <= ("110" & "00000000");            -- Start
        when 160 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 161 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 162 => data <= ("001" & "00000000");            -- Stop
        when 163 => data <= ("110" & "00000000");            -- Start
        when 164 => data <= ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
        when 165 => data <= ("011" & "00000000");            -- Read
        when 166 => data <= ("011" & "00000000");            -- Read
        when 167 => data <= ("001" & "00000000");            -- Stop
        when 168 => data <= ("110" & "00000000");            -- Start
        when 169 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 170 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 171 => data <= ("010" & "010" & "00010");       -- Cfg ADC2
        when 172 => data <= ("001" & "00000000");            -- Stop
        when 173 => data <= ("110" & "00000000");            -- Start
        when 174 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 175 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 176 => data <= ("001" & "00000000");            -- Stop
        when 177 => data <= ("110" & "00000000");            -- Start
        when 178 => data <= ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
        when 179 => data <= ("011" & "00000000");            -- Read
        when 180 => data <= ("011" & "00000000");            -- Read
        when 181 => data <= ("001" & "00000000");            -- Stop
        when 182 => data <= ("110" & "00000000");            -- Start
        when 183 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 184 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 185 => data <= ("010" & "011" & "00010");       -- Cfg ADC3
        when 186 => data <= ("001" & "00000000");            -- Stop
        when 187 => data <= ("110" & "00000000");            -- Start
        when 188 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 189 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 190 => data <= ("001" & "00000000");            -- Stop
        when 191 => data <= ("110" & "00000000");            -- Start
        when 192 => data <= ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
        when 193 => data <= ("011" & "00000000");            -- Read
        when 194 => data <= ("011" & "00000000");            -- Read
        when 195 => data <= ("001" & "00000000");            -- Stop
        when 196 => data <= ("110" & "00000000");            -- Start
        when 197 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 198 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 199 => data <= ("010" & "100" & "00010");       -- Cfg ADC4
        when 200 => data <= ("001" & "00000000");            -- Stop
        when 201 => data <= ("110" & "00000000");            -- Start
        when 202 => data <= ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
        when 203 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 204 => data <= ("001" & "00000000");            -- Stop
        when 205 => data <= ("110" & "00000000");            -- Start
        when 206 => data <= ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
        when 207 => data <= ("011" & "00000000");            -- Read
        when 208 => data <= ("011" & "00000000");            -- Read
        when 209 => data <= ("001" & "00000000");            -- Stop
                    -- ADC 3
        when 210 => data <= ("110" & "00000000");            -- Start
        when 211 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 212 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 213 => data <= ("010" & "000" & "00010");       -- Cfg T
        when 214 => data <= ("001" & "00000000");            -- Stop
        when 215 => data <= ("110" & "00000000");            -- Start
        when 216 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 217 => data <= ("010" & "00000" & "000");       -- write -> T
        when 218 => data <= ("001" & "00000000");            -- Stop
        when 219 => data <= ("110" & "00000000");            -- Start
        when 220 => data <= ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
        when 221 => data <= ("011" & "00000000");            -- Read
        when 222 => data <= ("011" & "00000000");            -- Read
        when 223 => data <= ("001" & "00000000");            -- Stop
        when 224 => data <= ("110" & "00000000");            -- Start
        when 225 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 226 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 227 => data <= ("010" & "001" & "00010");       -- Cfg ADC1
        when 228 => data <= ("001" & "00000000");            -- Stop
        when 229 => data <= ("110" & "00000000");            -- Start
        when 230 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 231 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 232 => data <= ("001" & "00000000");            -- Stop
        when 233 => data <= ("110" & "00000000");            -- Start
        when 234 => data <= ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
        when 235 => data <= ("011" & "00000000");            -- Read
        when 236 => data <= ("011" & "00000000");            -- Read
        when 237 => data <= ("001" & "00000000");            -- Stop
        when 238 => data <= ("110" & "00000000");            -- Start
        when 239 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 240 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 241 => data <= ("010" & "010" & "00010");       -- Cfg ADC2
        when 242 => data <= ("001" & "00000000");            -- Stop
        when 243 => data <= ("110" & "00000000");            -- Start
        when 244 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 245 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 246 => data <= ("001" & "00000000");            -- Stop
        when 247 => data <= ("110" & "00000000");            -- Start
        when 248 => data <= ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
        when 249 => data <= ("011" & "00000000");            -- Read
        when 250 => data <= ("011" & "00000000");            -- Read
        when 251 => data <= ("001" & "00000000");            -- Stop
        when 252 => data <= ("110" & "00000000");            -- Start
        when 253 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 254 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 255 => data <= ("010" & "011" & "00010");       -- Cfg ADC3
        when 256 => data <= ("001" & "00000000");            -- Stop
        when 257 => data <= ("110" & "00000000");            -- Start
        when 258 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 259 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 260 => data <= ("001" & "00000000");            -- Stop
        when 261 => data <= ("110" & "00000000");            -- Start
        when 262 => data <= ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
        when 263 => data <= ("011" & "00000000");            -- Read
        when 264 => data <= ("011" & "00000000");            -- Read
        when 265 => data <= ("001" & "00000000");            -- Stop
        when 266 => data <= ("110" & "00000000");            -- Start
        when 267 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 268 => data <= ("010" & "00000" & "001");       -- write -> cfg1
        when 269 => data <= ("010" & "100" & "00010");       -- Cfg ADC4
        when 270 => data <= ("001" & "00000000");            -- Stop
        when 271 => data <= ("110" & "00000000");            -- Start
        when 272 => data <= ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
        when 273 => data <= ("010" & "00000" & "100");       -- write -> ADC
        when 274 => data <= ("001" & "00000000");            -- Stop
        when 275 => data <= ("110" & "00000000");            -- Start
        when 276 => data <= ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
        when 277 => data <= ("011" & "00000000");            -- Read
        when 278 => data <= ("011" & "00000000");            -- Read
        when 279 => data <= ("001" & "00000000");            -- Stop
                    -- End of instructions 
        when 280 => data <= ("101" & "00000000");            -- End-of-block
                    -- Others
        when others => data <= (others => '0');
      end case;
    end if;
  end process make_output;
end rtl3;

------------------------------------------------------------------------------
architecture rtl2 of rom is
  subtype data_t is std_logic_vector(10 downto 0);
  constant MAX    : natural    := 281;            -- Size of ROM
  type data_array_t is array (0 to MAX) of data_t;  -- Data array

  -- purpose: Initialize ROM
  function init_rom
    return data_array_t is
    variable tmp : data_array_t
             := (others => (others => '0'));
  begin  -- function init_rom
    -- ADC 0
    tmp(0)   := ("110" & "00000000");            -- Start
    tmp(1)   := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(2)   := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(3)   := ("010" & "000" & "00010");       -- Cfg T
    tmp(4)   := ("001" & "00000000");            -- Stop
    tmp(5)   := ("110" & "00000000");            -- Start
    tmp(6)   := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(7)   := ("010" & "00000" & "000");       -- write -> T
    tmp(8)   := ("001" & "00000000");            -- Stop
    tmp(9)   := ("110" & "00000000");            -- Start
    tmp(10)  := ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
    tmp(11)  := ("011" & "00000000");            -- Read
    tmp(12)  := ("011" & "00000000");            -- Read
    tmp(13)  := ("001" & "00000000");            -- Stop
    tmp(14)  := ("110" & "00000000");            -- Start
    tmp(15)  := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(16)  := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(17)  := ("010" & "001" & "00010");       -- Cfg ADC1
    tmp(18)  := ("001" & "00000000");            -- Stop
    tmp(19)  := ("110" & "00000000");            -- Start
    tmp(20)  := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(21)  := ("010" & "00000" & "100");       -- write -> ADC
    tmp(22)  := ("001" & "00000000");            -- Stop
    tmp(23)  := ("110" & "00000000");            -- Start
    tmp(24)  := ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
    tmp(25)  := ("011" & "00000000");            -- Read
    tmp(26)  := ("011" & "00000000");            -- Read
    tmp(27)  := ("001" & "00000000");            -- Stop
    tmp(28)  := ("110" & "00000000");            -- Start
    tmp(29)  := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(30)  := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(31)  := ("010" & "010" & "00010");       -- Cfg ADC2
    tmp(32)  := ("001" & "00000000");            -- Stop
    tmp(33)  := ("110" & "00000000");            -- Start
    tmp(34)  := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(35)  := ("010" & "00000" & "100");       -- write -> ADC
    tmp(36)  := ("001" & "00000000");            -- Stop
    tmp(37)  := ("110" & "00000000");            -- Start
    tmp(38)  := ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
    tmp(39)  := ("011" & "00000000");            -- Read
    tmp(40)  := ("011" & "00000000");            -- Read
    tmp(41)  := ("001" & "00000000");            -- Stop
    tmp(42)  := ("110" & "00000000");            -- Start
    tmp(43)  := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(44)  := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(45)  := ("010" & "011" & "00010");       -- Cfg ADC3
    tmp(46)  := ("001" & "00000000");            -- Stop
    tmp(47)  := ("110" & "00000000");            -- Start
    tmp(48)  := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(49)  := ("010" & "00000" & "100");       -- write -> ADC
    tmp(50)  := ("001" & "00000000");            -- Stop
    tmp(51)  := ("110" & "00000000");            -- Start
    tmp(52)  := ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
    tmp(53)  := ("011" & "00000000");            -- Read
    tmp(54)  := ("011" & "00000000");            -- Read
    tmp(55)  := ("001" & "00000000");            -- Stop
    tmp(56)  := ("110" & "00000000");            -- Start
    tmp(57)  := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(58)  := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(59)  := ("010" & "100" & "00010");       -- Cfg ADC4
    tmp(60)  := ("001" & "00000000");            -- Stop
    tmp(61)  := ("110" & "00000000");            -- Start
    tmp(62)  := ("010" & "0101" & "000" & "0");  -- Write & vendor & adc & we
    tmp(63)  := ("010" & "00000" & "100");       -- write -> ADC
    tmp(64)  := ("001" & "00000000");            -- Stop
    tmp(65)  := ("110" & "00000000");            -- Start
    tmp(66)  := ("010" & "0101" & "000" & "1");  -- Write & vendor & adc & re
    tmp(67)  := ("011" & "00000000");            -- Read
    tmp(68)  := ("011" & "00000000");            -- Read
    tmp(69)  := ("001" & "00000000");            -- Stop
    -- ADC 1
    tmp(70)  := ("110" & "00000000");            -- Start
    tmp(71)  := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(72)  := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(73)  := ("010" & "000" & "00010");       -- Cfg T
    tmp(74)  := ("001" & "00000000");            -- Stop
    tmp(75)  := ("110" & "00000000");            -- Start
    tmp(76)  := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(77)  := ("010" & "00000" & "000");       -- write -> T
    tmp(78)  := ("001" & "00000000");            -- Stop
    tmp(79)  := ("110" & "00000000");            -- Start
    tmp(80)  := ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
    tmp(81)  := ("011" & "00000000");            -- Read
    tmp(82)  := ("011" & "00000000");            -- Read
    tmp(83)  := ("001" & "00000000");            -- Stop
    tmp(84)  := ("110" & "00000000");            -- Start
    tmp(85)  := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(86)  := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(87)  := ("010" & "001" & "00010");       -- Cfg ADC1
    tmp(88)  := ("001" & "00000000");            -- Stop
    tmp(89)  := ("110" & "00000000");            -- Start
    tmp(90)  := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(91)  := ("010" & "00000" & "100");       -- write -> ADC
    tmp(92)  := ("001" & "00000000");            -- Stop
    tmp(93)  := ("110" & "00000000");            -- Start
    tmp(94)  := ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
    tmp(95)  := ("011" & "00000000");            -- Read
    tmp(96)  := ("011" & "00000000");            -- Read
    tmp(97)  := ("001" & "00000000");            -- Stop
    tmp(98)  := ("110" & "00000000");            -- Start
    tmp(99)  := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(100) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(101) := ("010" & "010" & "00010");       -- Cfg ADC2
    tmp(102) := ("001" & "00000000");            -- Stop
    tmp(103) := ("110" & "00000000");            -- Start
    tmp(104) := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(105) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(106) := ("001" & "00000000");            -- Stop
    tmp(107) := ("110" & "00000000");            -- Start
    tmp(108) := ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
    tmp(109) := ("011" & "00000000");            -- Read
    tmp(110) := ("011" & "00000000");            -- Read
    tmp(111) := ("001" & "00000000");            -- Stop
    tmp(112) := ("110" & "00000000");            -- Start
    tmp(113) := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(114) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(115) := ("010" & "011" & "00010");       -- Cfg ADC3
    tmp(116) := ("001" & "00000000");            -- Stop
    tmp(117) := ("110" & "00000000");            -- Start
    tmp(118) := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(119) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(120) := ("001" & "00000000");            -- Stop
    tmp(121) := ("110" & "00000000");            -- Start
    tmp(122) := ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
    tmp(123) := ("011" & "00000000");            -- Read
    tmp(124) := ("011" & "00000000");            -- Read
    tmp(125) := ("001" & "00000000");            -- Stop
    tmp(126) := ("110" & "00000000");            -- Start
    tmp(127) := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(128) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(129) := ("010" & "100" & "00010");       -- Cfg ADC4
    tmp(130) := ("001" & "00000000");            -- Stop
    tmp(131) := ("110" & "00000000");            -- Start
    tmp(132) := ("010" & "0101" & "001" & "0");  -- Write & vendor & adc & we
    tmp(133) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(134) := ("001" & "00000000");            -- Stop
    tmp(135) := ("110" & "00000000");            -- Start
    tmp(136) := ("010" & "0101" & "001" & "1");  -- Write & vendor & adc & re
    tmp(137) := ("011" & "00000000");            -- Read
    tmp(138) := ("011" & "00000000");            -- Read
    tmp(139) := ("001" & "00000000");            -- Stop
    -- ADC 2
    tmp(140) := ("110" & "00000000");            -- Start
    tmp(141) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(142) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(143) := ("010" & "000" & "00010");       -- Cfg T
    tmp(144) := ("001" & "00000000");            -- Stop
    tmp(145) := ("110" & "00000000");            -- Start
    tmp(146) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(147) := ("010" & "00000" & "000");       -- write -> T
    tmp(148) := ("001" & "00000000");            -- Stop
    tmp(149) := ("110" & "00000000");            -- Start
    tmp(150) := ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
    tmp(151) := ("011" & "00000000");            -- Read
    tmp(152) := ("011" & "00000000");            -- Read
    tmp(153) := ("001" & "00000000");            -- Stop
    tmp(154) := ("110" & "00000000");            -- Start
    tmp(155) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(156) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(157) := ("010" & "001" & "00010");       -- Cfg ADC1
    tmp(158) := ("001" & "00000000");            -- Stop
    tmp(159) := ("110" & "00000000");            -- Start
    tmp(160) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(161) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(162) := ("001" & "00000000");            -- Stop
    tmp(163) := ("110" & "00000000");            -- Start
    tmp(164) := ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
    tmp(165) := ("011" & "00000000");            -- Read
    tmp(166) := ("011" & "00000000");            -- Read
    tmp(167) := ("001" & "00000000");            -- Stop
    tmp(168) := ("110" & "00000000");            -- Start
    tmp(169) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(170) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(171) := ("010" & "010" & "00010");       -- Cfg ADC2
    tmp(172) := ("001" & "00000000");            -- Stop
    tmp(173) := ("110" & "00000000");            -- Start
    tmp(174) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(175) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(176) := ("001" & "00000000");            -- Stop
    tmp(177) := ("110" & "00000000");            -- Start
    tmp(178) := ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
    tmp(179) := ("011" & "00000000");            -- Read
    tmp(180) := ("011" & "00000000");            -- Read
    tmp(181) := ("001" & "00000000");            -- Stop
    tmp(182) := ("110" & "00000000");            -- Start
    tmp(183) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(184) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(185) := ("010" & "011" & "00010");       -- Cfg ADC3
    tmp(186) := ("001" & "00000000");            -- Stop
    tmp(187) := ("110" & "00000000");            -- Start
    tmp(188) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(189) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(190) := ("001" & "00000000");            -- Stop
    tmp(191) := ("110" & "00000000");            -- Start
    tmp(192) := ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
    tmp(193) := ("011" & "00000000");            -- Read
    tmp(194) := ("011" & "00000000");            -- Read
    tmp(195) := ("001" & "00000000");            -- Stop
    tmp(196) := ("110" & "00000000");            -- Start
    tmp(197) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(198) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(199) := ("010" & "100" & "00010");       -- Cfg ADC4
    tmp(200) := ("001" & "00000000");            -- Stop
    tmp(201) := ("110" & "00000000");            -- Start
    tmp(202) := ("010" & "0101" & "010" & "0");  -- Write & vendor & adc & we
    tmp(203) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(204) := ("001" & "00000000");            -- Stop
    tmp(205) := ("110" & "00000000");            -- Start
    tmp(206) := ("010" & "0101" & "010" & "1");  -- Write & vendor & adc & re
    tmp(207) := ("011" & "00000000");            -- Read
    tmp(208) := ("011" & "00000000");            -- Read
    tmp(209) := ("001" & "00000000");            -- Stop
    -- ADC 3
    tmp(210) := ("110" & "00000000");            -- Start
    tmp(211) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(212) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(213) := ("010" & "000" & "00010");       -- Cfg T
    tmp(214) := ("001" & "00000000");            -- Stop
    tmp(215) := ("110" & "00000000");            -- Start
    tmp(216) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(217) := ("010" & "00000" & "000");       -- write -> T
    tmp(218) := ("001" & "00000000");            -- Stop
    tmp(219) := ("110" & "00000000");            -- Start
    tmp(220) := ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
    tmp(221) := ("011" & "00000000");            -- Read
    tmp(222) := ("011" & "00000000");            -- Read
    tmp(223) := ("001" & "00000000");            -- Stop
    tmp(224) := ("110" & "00000000");            -- Start
    tmp(225) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(226) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(227) := ("010" & "001" & "00010");       -- Cfg ADC1
    tmp(228) := ("001" & "00000000");            -- Stop
    tmp(229) := ("110" & "00000000");            -- Start
    tmp(230) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(231) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(232) := ("001" & "00000000");            -- Stop
    tmp(233) := ("110" & "00000000");            -- Start
    tmp(234) := ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
    tmp(235) := ("011" & "00000000");            -- Read
    tmp(236) := ("011" & "00000000");            -- Read
    tmp(237) := ("001" & "00000000");            -- Stop
    tmp(238) := ("110" & "00000000");            -- Start
    tmp(239) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(240) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(241) := ("010" & "010" & "00010");       -- Cfg ADC2
    tmp(242) := ("001" & "00000000");            -- Stop
    tmp(243) := ("110" & "00000000");            -- Start
    tmp(244) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(245) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(246) := ("001" & "00000000");            -- Stop
    tmp(247) := ("110" & "00000000");            -- Start
    tmp(248) := ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
    tmp(249) := ("011" & "00000000");            -- Read
    tmp(250) := ("011" & "00000000");            -- Read
    tmp(251) := ("001" & "00000000");            -- Stop
    tmp(252) := ("110" & "00000000");            -- Start
    tmp(253) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(254) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(255) := ("010" & "011" & "00010");       -- Cfg ADC3
    tmp(256) := ("001" & "00000000");            -- Stop
    tmp(257) := ("110" & "00000000");            -- Start
    tmp(258) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(259) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(260) := ("001" & "00000000");            -- Stop
    tmp(261) := ("110" & "00000000");            -- Start
    tmp(262) := ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
    tmp(263) := ("011" & "00000000");            -- Read
    tmp(264) := ("011" & "00000000");            -- Read
    tmp(265) := ("001" & "00000000");            -- Stop
    tmp(266) := ("110" & "00000000");            -- Start
    tmp(267) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(268) := ("010" & "00000" & "001");       -- write -> cfg1
    tmp(269) := ("010" & "100" & "00010");       -- Cfg ADC4
    tmp(270) := ("001" & "00000000");            -- Stop
    tmp(271) := ("110" & "00000000");            -- Start
    tmp(272) := ("010" & "0101" & "011" & "0");  -- Write & vendor & adc & we
    tmp(273) := ("010" & "00000" & "100");       -- write -> ADC
    tmp(274) := ("001" & "00000000");            -- Stop
    tmp(275) := ("110" & "00000000");            -- Start
    tmp(276) := ("010" & "0101" & "011" & "1");  -- Write & vendor & adc & re
    tmp(277) := ("011" & "00000000");            -- Read
    tmp(278) := ("011" & "00000000");            -- Read
    tmp(279) := ("001" & "00000000");            -- Stop
    -- End of instructions 
    tmp(280) := ("101" & "00000000");            -- End-of-block

    -- Return
    return tmp;
  end function init_rom;

  signal data_i : data_array_t := init_rom;
  signal add_i : integer range 0 to MAX;

begin  -- rtl
  -- purpose: Register address
  -- type   : sequential
  -- inputs : inclk, add
  -- outputs: data
  take_address: process (inclk, rstb)
  begin  -- process output
    if rstb = '0' then
      add_i <= 0;
    elsif inclk'event and inclk = '1' then  -- rising clock edge
      add_i <= to_integer(unsigned(add));
    end if;
  end process take_address;

  -- purpose: Output data
  -- type   : sequential
  -- inputs : outclk, add_i
  -- outputs: data
  make_output: process (outclk)
  begin  -- process make_output
    if outclk'event and outclk = '1' then  -- rising clock edge
      assert add_i < MAX report "Whoops, addr too big: " &
        integer'image(add_i) severity warning;
      if (add_i > MAX or add_i < 0) then
        data <= (others => '0');
      else
        data <= data_i(add_i);
      end if;      
    end if;
  end process make_output;
end rtl2;

------------------------------------------------------------------------------
architecture rtl of rom is
  subtype data_t is std_logic_vector(10 downto 0);
  constant MAX    : natural    := 281;            -- Size of ROM
  type data_array is array (0 to MAX) of data_t;  -- Data array
  attribute romstyle : string;
  attribute romstyle of rtl : architecture is "M-RAM";
  constant data_i : data_array := (
    -- ADC 0
    0      => ("110" & "00000000"),               -- Start
    1      => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    2      => ("010" & "00000" & "001"),          -- write -> cfg1
    3      => ("010" & "000" & "00010"),          -- Cfg T
    4      => ("001" & "00000000"),               -- Stop
    5      => ("110" & "00000000"),               -- Start
    6      => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    7      => ("010" & "00000" & "000"),          -- write -> T
    8      => ("001" & "00000000"),               -- Stop
    9      => ("110" & "00000000"),               -- Start
    10     => ("010" & "0101" & "000" & "1"),     -- Write & vendor & adc & re
    11     => ("011" & "00000000"),               -- Read
    12     => ("011" & "00000000"),               -- Read
    13     => ("001" & "00000000"),               -- Stop
    14     => ("110" & "00000000"),               -- Start
    15     => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    16     => ("010" & "00000" & "001"),          -- write -> cfg1
    17     => ("010" & "001" & "00010"),          -- Cfg ADC1
    18     => ("001" & "00000000"),               -- Stop
    19     => ("110" & "00000000"),               -- Start
    20     => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    21     => ("010" & "00000" & "100"),          -- write -> ADC
    22     => ("001" & "00000000"),               -- Stop
    23     => ("110" & "00000000"),               -- Start
    24     => ("010" & "0101" & "000" & "1"),     -- Write & vendor & adc & re
    25     => ("011" & "00000000"),               -- Read
    26     => ("011" & "00000000"),               -- Read
    27     => ("001" & "00000000"),               -- Stop
    28     => ("110" & "00000000"),               -- Start
    29     => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    30     => ("010" & "00000" & "001"),          -- write -> cfg1
    31     => ("010" & "010" & "00010"),          -- Cfg ADC2
    32     => ("001" & "00000000"),               -- Stop
    33     => ("110" & "00000000"),               -- Start
    34     => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    35     => ("010" & "00000" & "100"),          -- write -> ADC
    36     => ("001" & "00000000"),               -- Stop
    37     => ("110" & "00000000"),               -- Start
    38     => ("010" & "0101" & "000" & "1"),     -- Write & vendor & adc & re
    39     => ("011" & "00000000"),               -- Read
    40     => ("011" & "00000000"),               -- Read
    41     => ("001" & "00000000"),               -- Stop
    42     => ("110" & "00000000"),               -- Start
    43     => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    44     => ("010" & "00000" & "001"),          -- write -> cfg1
    45     => ("010" & "011" & "00010"),          -- Cfg ADC3
    46     => ("001" & "00000000"),               -- Stop
    47     => ("110" & "00000000"),               -- Start
    48     => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    49     => ("010" & "00000" & "100"),          -- write -> ADC
    50     => ("001" & "00000000"),               -- Stop
    51     => ("110" & "00000000"),               -- Start
    52     => ("010" & "0101" & "000" & "1"),     -- Write & vendor & adc & re
    53     => ("011" & "00000000"),               -- Read
    54     => ("011" & "00000000"),               -- Read
    55     => ("001" & "00000000"),               -- Stop
    56     => ("110" & "00000000"),               -- Start
    57     => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    58     => ("010" & "00000" & "001"),          -- write -> cfg1
    59     => ("010" & "100" & "00010"),          -- Cfg ADC4
    60     => ("001" & "00000000"),               -- Stop
    61     => ("110" & "00000000"),               -- Start
    62     => ("010" & "0101" & "000" & "0"),     -- Write & vendor & adc & we
    63     => ("010" & "00000" & "100"),          -- write -> ADC
    64     => ("001" & "00000000"),               -- Stop
    65     => ("110" & "00000000"),               -- Start
    66     => ("010" & "0101" & "000" & "1"),     -- Write & vendor & adc & re
    67     => ("011" & "00000000"),               -- Read
    68     => ("011" & "00000000"),               -- Read
    69     => ("001" & "00000000"),               -- Stop
    -- ADC 1
    70     => ("110" & "00000000"),               -- Start
    71     => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    72     => ("010" & "00000" & "001"),          -- write -> cfg1
    73     => ("010" & "000" & "00010"),          -- Cfg T
    74     => ("001" & "00000000"),               -- Stop
    75     => ("110" & "00000000"),               -- Start
    76     => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    77     => ("010" & "00000" & "000"),          -- write -> T
    78     => ("001" & "00000000"),               -- Stop
    79     => ("110" & "00000000"),               -- Start
    80     => ("010" & "0101" & "001" & "1"),     -- Write & vendor & adc & re
    81     => ("011" & "00000000"),               -- Read
    82     => ("011" & "00000000"),               -- Read
    83     => ("001" & "00000000"),               -- Stop
    84     => ("110" & "00000000"),               -- Start
    85     => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    86     => ("010" & "00000" & "001"),          -- write -> cfg1
    87     => ("010" & "001" & "00010"),          -- Cfg ADC1
    88     => ("001" & "00000000"),               -- Stop
    89     => ("110" & "00000000"),               -- Start
    90     => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    91     => ("010" & "00000" & "100"),          -- write -> ADC
    92     => ("001" & "00000000"),               -- Stop
    93     => ("110" & "00000000"),               -- Start
    94     => ("010" & "0101" & "001" & "1"),     -- Write & vendor & adc & re
    95     => ("011" & "00000000"),               -- Read
    96     => ("011" & "00000000"),               -- Read
    97     => ("001" & "00000000"),               -- Stop
    98     => ("110" & "00000000"),               -- Start
    99     => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    100    => ("010" & "00000" & "001"),          -- write -> cfg1
    101    => ("010" & "010" & "00010"),          -- Cfg ADC2
    102    => ("001" & "00000000"),               -- Stop
    103    => ("110" & "00000000"),               -- Start
    104    => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    105    => ("010" & "00000" & "100"),          -- write -> ADC
    106    => ("001" & "00000000"),               -- Stop
    107    => ("110" & "00000000"),               -- Start
    108    => ("010" & "0101" & "001" & "1"),     -- Write & vendor & adc & re
    109    => ("011" & "00000000"),               -- Read
    110    => ("011" & "00000000"),               -- Read
    111    => ("001" & "00000000"),               -- Stop
    112    => ("110" & "00000000"),               -- Start
    113    => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    114    => ("010" & "00000" & "001"),          -- write -> cfg1
    115    => ("010" & "011" & "00010"),          -- Cfg ADC3
    116    => ("001" & "00000000"),               -- Stop
    117    => ("110" & "00000000"),               -- Start
    118    => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    119    => ("010" & "00000" & "100"),          -- write -> ADC
    120    => ("001" & "00000000"),               -- Stop
    121    => ("110" & "00000000"),               -- Start
    122    => ("010" & "0101" & "001" & "1"),     -- Write & vendor & adc & re
    123    => ("011" & "00000000"),               -- Read
    124    => ("011" & "00000000"),               -- Read
    125    => ("001" & "00000000"),               -- Stop
    126    => ("110" & "00000000"),               -- Start
    127    => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    128    => ("010" & "00000" & "001"),          -- write -> cfg1
    129    => ("010" & "100" & "00010"),          -- Cfg ADC4
    130    => ("001" & "00000000"),               -- Stop
    131    => ("110" & "00000000"),               -- Start
    132    => ("010" & "0101" & "001" & "0"),     -- Write & vendor & adc & we
    133    => ("010" & "00000" & "100"),          -- write -> ADC
    134    => ("001" & "00000000"),               -- Stop
    135    => ("110" & "00000000"),               -- Start
    136    => ("010" & "0101" & "001" & "1"),     -- Write & vendor & adc & re
    137    => ("011" & "00000000"),               -- Read
    138    => ("011" & "00000000"),               -- Read
    139    => ("001" & "00000000"),               -- Stop
    -- ADC 2
    140    => ("110" & "00000000"),               -- Start
    141    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    142    => ("010" & "00000" & "001"),          -- write -> cfg1
    143    => ("010" & "000" & "00010"),          -- Cfg T
    144    => ("001" & "00000000"),               -- Stop
    145    => ("110" & "00000000"),               -- Start
    146    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    147    => ("010" & "00000" & "000"),          -- write -> T
    148    => ("001" & "00000000"),               -- Stop
    149    => ("110" & "00000000"),               -- Start
    150    => ("010" & "0101" & "010" & "1"),     -- Write & vendor & adc & re
    151    => ("011" & "00000000"),               -- Read
    152    => ("011" & "00000000"),               -- Read
    153    => ("001" & "00000000"),               -- Stop
    154    => ("110" & "00000000"),               -- Start
    155    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    156    => ("010" & "00000" & "001"),          -- write -> cfg1
    157    => ("010" & "001" & "00010"),          -- Cfg ADC1
    158    => ("001" & "00000000"),               -- Stop
    159    => ("110" & "00000000"),               -- Start
    160    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    161    => ("010" & "00000" & "100"),          -- write -> ADC
    162    => ("001" & "00000000"),               -- Stop
    163    => ("110" & "00000000"),               -- Start
    164    => ("010" & "0101" & "010" & "1"),     -- Write & vendor & adc & re
    165    => ("011" & "00000000"),               -- Read
    166    => ("011" & "00000000"),               -- Read
    167    => ("001" & "00000000"),               -- Stop
    168    => ("110" & "00000000"),               -- Start
    169    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    170    => ("010" & "00000" & "001"),          -- write -> cfg1
    171    => ("010" & "010" & "00010"),          -- Cfg ADC2
    172    => ("001" & "00000000"),               -- Stop
    173    => ("110" & "00000000"),               -- Start
    174    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    175    => ("010" & "00000" & "100"),          -- write -> ADC
    176    => ("001" & "00000000"),               -- Stop
    177    => ("110" & "00000000"),               -- Start
    178    => ("010" & "0101" & "010" & "1"),     -- Write & vendor & adc & re
    179    => ("011" & "00000000"),               -- Read
    180    => ("011" & "00000000"),               -- Read
    181    => ("001" & "00000000"),               -- Stop
    182    => ("110" & "00000000"),               -- Start
    183    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    184    => ("010" & "00000" & "001"),          -- write -> cfg1
    185    => ("010" & "011" & "00010"),          -- Cfg ADC3
    186    => ("001" & "00000000"),               -- Stop
    187    => ("110" & "00000000"),               -- Start
    188    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    189    => ("010" & "00000" & "100"),          -- write -> ADC
    190    => ("001" & "00000000"),               -- Stop
    191    => ("110" & "00000000"),               -- Start
    192    => ("010" & "0101" & "010" & "1"),     -- Write & vendor & adc & re
    193    => ("011" & "00000000"),               -- Read
    194    => ("011" & "00000000"),               -- Read
    195    => ("001" & "00000000"),               -- Stop
    196    => ("110" & "00000000"),               -- Start
    197    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    198    => ("010" & "00000" & "001"),          -- write -> cfg1
    199    => ("010" & "100" & "00010"),          -- Cfg ADC4
    200    => ("001" & "00000000"),               -- Stop
    201    => ("110" & "00000000"),               -- Start
    202    => ("010" & "0101" & "010" & "0"),     -- Write & vendor & adc & we
    203    => ("010" & "00000" & "100"),          -- write -> ADC
    204    => ("001" & "00000000"),               -- Stop
    205    => ("110" & "00000000"),               -- Start
    206    => ("010" & "0101" & "010" & "1"),     -- Write & vendor & adc & re
    207    => ("011" & "00000000"),               -- Read
    208    => ("011" & "00000000"),               -- Read
    209    => ("001" & "00000000"),               -- Stop
    -- ADC 3
    210    => ("110" & "00000000"),               -- Start
    211    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    212    => ("010" & "00000" & "001"),          -- write -> cfg1
    213    => ("010" & "000" & "00010"),          -- Cfg T
    214    => ("001" & "00000000"),               -- Stop
    215    => ("110" & "00000000"),               -- Start
    216    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    217    => ("010" & "00000" & "000"),          -- write -> T
    218    => ("001" & "00000000"),               -- Stop
    219    => ("110" & "00000000"),               -- Start
    220    => ("010" & "0101" & "011" & "1"),     -- Write & vendor & adc & re
    221    => ("011" & "00000000"),               -- Read
    222    => ("011" & "00000000"),               -- Read
    223    => ("001" & "00000000"),               -- Stop
    224    => ("110" & "00000000"),               -- Start
    225    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    226    => ("010" & "00000" & "001"),          -- write -> cfg1
    227    => ("010" & "001" & "00010"),          -- Cfg ADC1
    228    => ("001" & "00000000"),               -- Stop
    229    => ("110" & "00000000"),               -- Start
    230    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    231    => ("010" & "00000" & "100"),          -- write -> ADC
    232    => ("001" & "00000000"),               -- Stop
    233    => ("110" & "00000000"),               -- Start
    234    => ("010" & "0101" & "011" & "1"),     -- Write & vendor & adc & re
    235    => ("011" & "00000000"),               -- Read
    236    => ("011" & "00000000"),               -- Read
    237    => ("001" & "00000000"),               -- Stop
    238    => ("110" & "00000000"),               -- Start
    239    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    240    => ("010" & "00000" & "001"),          -- write -> cfg1
    241    => ("010" & "010" & "00010"),          -- Cfg ADC2
    242    => ("001" & "00000000"),               -- Stop
    243    => ("110" & "00000000"),               -- Start
    244    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    245    => ("010" & "00000" & "100"),          -- write -> ADC
    246    => ("001" & "00000000"),               -- Stop
    247    => ("110" & "00000000"),               -- Start
    248    => ("010" & "0101" & "011" & "1"),     -- Write & vendor & adc & re
    249    => ("011" & "00000000"),               -- Read
    250    => ("011" & "00000000"),               -- Read
    251    => ("001" & "00000000"),               -- Stop
    252    => ("110" & "00000000"),               -- Start
    253    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    254    => ("010" & "00000" & "001"),          -- write -> cfg1
    255    => ("010" & "011" & "00010"),          -- Cfg ADC3
    256    => ("001" & "00000000"),               -- Stop
    257    => ("110" & "00000000"),               -- Start
    258    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    259    => ("010" & "00000" & "100"),          -- write -> ADC
    260    => ("001" & "00000000"),               -- Stop
    261    => ("110" & "00000000"),               -- Start
    262    => ("010" & "0101" & "011" & "1"),     -- Write & vendor & adc & re
    263    => ("011" & "00000000"),               -- Read
    264    => ("011" & "00000000"),               -- Read
    265    => ("001" & "00000000"),               -- Stop
    266    => ("110" & "00000000"),               -- Start
    267    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    268    => ("010" & "00000" & "001"),          -- write -> cfg1
    269    => ("010" & "100" & "00010"),          -- Cfg ADC4
    270    => ("001" & "00000000"),               -- Stop
    271    => ("110" & "00000000"),               -- Start
    272    => ("010" & "0101" & "011" & "0"),     -- Write & vendor & adc & we
    273    => ("010" & "00000" & "100"),          -- write -> ADC
    274    => ("001" & "00000000"),               -- Stop
    275    => ("110" & "00000000"),               -- Start
    276    => ("010" & "0101" & "011" & "1"),     -- Write & vendor & adc & re
    277    => ("011" & "00000000"),               -- Read
    278    => ("011" & "00000000"),               -- Read
    279    => ("001" & "00000000"),               -- Stop
    -- End of instructions 
    280    => ("101" & "00000000"),               -- End-of-block
    others => "00000000000");

  signal add_i : integer range 0 to MAX;

begin  -- rtl
  -- purpose: Register address
  -- type   : sequential
  -- inputs : inclk, add
  -- outputs: data
  take_address: process (inclk, rstb)
  begin  -- process output
    if rstb = '0' then
      add_i <= 0;
    elsif inclk'event and inclk = '1' then  -- rising clock edge
      add_i <= to_integer(unsigned(add));
    end if;
  end process take_address;

  -- purpose: Output data
  -- type   : sequential
  -- inputs : outclk, add_i
  -- outputs: data
  make_output: process (outclk,rstb)
  begin  -- process make_output
    if rstb = '0' then
      data <= (others => '0');
    elsif outclk'event and outclk = '1' then  -- rising clock edge
      assert add_i < MAX report "Whoops, addr too big: " &
        integer'image(add_i) severity warning;
      if (add_i > MAX or add_i < 0) then
        data <= (others => '0');
      else
        data <= data_i(add_i);
      end if;      
    end if;
  end process make_output;
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package rom_pack is
  component rom
    port (
      rstb   : in  std_logic;           -- Reset
      inclk  : in  std_logic;
      outclk : in  std_logic;
      add    : in  std_logic_vector(9 downto 0);
      data   : out std_logic_vector(10 downto 0));
  end component;
end rom_pack;

------------------------------------------------------------------------------
--
-- EOF
-- 
