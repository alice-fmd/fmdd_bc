------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sequencer_pack.all;

------------------------------------------------------------------------------
entity sequencer_tb is
end sequencer_tb;

------------------------------------------------------------------------------
architecture test of sequencer_tb is
  constant PERIOD : time := 25 ns;      -- A clock cycle

  signal clk_i          : std_logic := '0';
  signal rstb_i         : std_logic := '0';
  signal data_valid_i   : std_logic := '0';
  signal data_rom_i     : std_logic_vector(10 downto 0) := (others => '0');
  signal en_add_i       : std_logic := '0';
  signal error_i        : std_logic := '0';
  signal ready_i        : std_logic := '0';
  signal s_i            : std_logic := '0';
  signal stcnv_i        : std_logic := '0';
  signal width_i        : std_logic := '0';
  signal data_i         : std_logic_vector(7 downto 0);
  signal address_rom_i  : std_logic_vector(9 downto 0);
  signal address_regs_i : std_logic_vector(4 downto 0);
  signal new_data_i     : std_logic;
  signal rw_i           : std_logic;
  signal start_i        : std_logic;
  signal stop_i         : std_logic;
  signal we_adc_i       : std_logic;
  signal end_seq_i      : std_logic;

begin  -- test

  DUT: sequencer
    port map (
        clk          => clk_i,
        rstb         => rstb_i,
        data_valid   => data_valid_i,
        data_rom     => data_rom_i,
        en_add       => en_add_i,
        error        => error_i,
        ready        => ready_i,
        s            => s_i,
        stcnv        => stcnv_i,
        width        => width_i,
        data         => data_i,
        address_rom  => address_rom_i,
        address_regs => address_regs_i,
        new_data     => new_data_i,
        rw           => rw_i,
        start        => start_i,
        stop         => stop_i,
        we_adc       => we_adc_i,
        end_seq      => end_seq_i);

  

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i  <= '1';

    wait for 4 * PERIOD;

    -- Start conversion
    wait until rising_edge(clk_i);
    stcnv_i <= '1';
    wait until rising_edge(clk_i);
    stcnv_i <= '0';
    
    wait;                               -- forever 
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
  
end test;

------------------------------------------------------------------------------
--
-- EOF
--
