------------------------------------------------------------------------------
-- Title      : Sequence out Monitor ADC instructions
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : sequencer.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/12
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Takes Monitor ADC instructions and outputs them to I2C master.
--              This component also takes care of decoding whether it's read
--              or write, as well as moving the register pointer. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.register_config.all;
-- library lpm;
-- use lpm.lpm_components.all;

entity sequencer is
  port (
    clk          : in  std_logic;       -- clock
    rstb         : in  std_logic;       -- Async reset
    data_valid   : in  std_logic;       -- Validate data
    data_rom     : in  std_logic_vector(10 downto 0);  -- I2C code from ROM
    en_add       : in  std_logic;       -- Address enable
    error        : in  std_logic;       -- Error flag from master 
    ready        : in  std_logic;       -- Ready flag from master
    s            : in  std_logic;       -- Serial data from master
    stcnv        : in  std_logic;       -- Start conversion command
    width        : in  std_logic;       -- Width from master (?)
    data         : out std_logic_vector(7 downto 0);  -- Data for master
    address_rom  : out std_logic_vector(9 downto 0);  -- index in ROM
    address_regs : out std_logic_vector(4 downto 0);  -- Register address
    new_data     : out std_logic;       -- Got new data
    rw           : out std_logic;       -- Read/Write flag
    start        : out std_logic;       -- Start to master
    stop         : out std_logic;       -- Stop to master
    we_adc       : out std_logic;       -- Write enable for Monitor ADCs
    end_seq      : out std_logic);      -- end of sequencing flag
end sequencer;

------------------------------------------------------------------------------
architecture rtl of sequencer is
  constant seq_start   : unsigned(2 downto 0) := "110";
  constant seq_stop    : unsigned(2 downto 0) := "001";
  constant seq_data_wr : unsigned(2 downto 0) := "010";
  constant seq_data_rd : unsigned(2 downto 0) := "011";
  constant seq_end     : unsigned(2 downto 0) := "101";
  constant add_rom_i   : unsigned(9 downto 0) := (others => '0');
  constant add_reg_i   : unsigned(4 downto 0) := to_unsigned(BASE_ADC, 5);
  constant vendor      : unsigned(3 downto 0) := "0101";

  type state is (idle, read_rom, load_data, wait_ready, write_regs,
                 inc_cnt_regs, inc_cnt_rom, wait_1, wait_2, wait_3);
  signal st                : state;
  signal nx_st             : state;     -- Next state
  signal start_i           : boolean;
  signal stop_i            : boolean;
  signal rw_i              : boolean;
  signal end_seq_i         : boolean;
  signal wait_seq_i        : boolean;
  signal rom_en_i          : boolean;
  -- signal address_i      : boolean;   -- Addressed
  signal load_add_regs_i   : std_logic;
  signal cnt_en_add_regs_i : std_logic;
  signal load_rom_i        : std_logic;
  signal cnt_en_rom_i      : std_logic;
  signal instr_i           : unsigned(2 downto 0) := (others => '0');

  alias instr   : std_logic_vector(2 downto 0) is data_rom(10 downto 8);

begin  -- rtl
  -- purpose: Set combinatorical stuff
  combi         : block
  begin  -- block combi
    rom_en_i    <= (st /= idle);
    instr_i     <= unsigned(instr) when rstb /= '0' else "000";
    start_i     <= ((instr_i = seq_start) and rom_en_i);
    stop_i      <= ((instr_i = seq_stop) and rom_en_i);
    rw_i        <= ((instr_i = seq_data_rd) and rom_en_i);
    end_seq_i   <= ((instr_i = seq_end) and rom_en_i);
    wait_seq_i  <= not (((instr_i = seq_start) or
                        (instr_i = seq_stop) or
                        (instr_i = seq_data_wr) or
                        (instr_i = seq_data_rd) or
                        (instr_i = seq_end)));
    -- output
    start       <= '1'             when start_i     else '0';
    stop        <= '1'             when stop_i      else '0';
    rw          <= '1'             when rw_i        else '0';
    end_seq     <= '1'             when end_seq_i   else '0';
  end block combi;


  -- purpose: Set the next state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st
  -- outputs: st
  next_state : process (clk, rstb)
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st <= nx_st;
    end if;
  end process next_state;

  -- purpose: Get the next state
  -- type   : combinational
  -- inputs : clk
  -- outputs: 
  -- fsm : process (clk, rstb)
  fsm : process (st,                    -- State
                 stcnv,                 -- Command to start
                 ready,                 -- master ready for more
                 width,                 -- one or two bytes
                 en_add,                -- Enable address counter
                 data_rom,              -- Data from ROM
                 error,                 -- Error from master
                 wait_seq_i,            -- Wait for sequencer
                 rw_i,                  -- Read or write 
                 start_i,               -- Start pattern
                 stop_i,                -- Stop pattern
                 data_valid,            -- Valid data from master
                 s,                     -- 1st or 2nd word
                 end_seq_i)             -- Done
  begin  -- process fsm
    nx_st <= st;

    case st is
      when idle                      =>
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= (others => '0');
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
        if stcnv = '1' then
          nx_st           <= read_rom;
        else
          nx_st           <= idle;
        end if;

      when read_rom                  =>  -- Get ready to read from
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= (others => '0');
        load_add_regs_i   <= '1';       -- Set initial value of address
        load_rom_i        <= '1';       -- Set initial value of ROM index
        new_data          <= '0';
        we_adc            <= '0';
        if error = '1' then
          nx_st           <= idle;
        else
          nx_st           <= load_data;
        end if;

      when load_data =>                 -- Get the next word
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= data_rom(7 downto 0);  -- From ROM
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
        if error = '1' then
          nx_st           <= idle;
        else
          nx_st           <= wait_ready;
        end if;

      when wait_ready =>                -- Wait until master is ready
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= data_rom(7 downto 0);  -- From ROM
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '1';
        we_adc            <= '0';
--         report
--           " rw="    & boolean'image(rw_i)    &
--           " ready=" & std_logic'image(ready) &
--           " start=" & boolean'image(start_i) &
--           " stop="  & boolean'image(stop_i)   severity note;
        if error = '1' then
          nx_st           <= idle;
        elsif wait_seq_i then
          nx_st           <= inc_cnt_rom;
        elsif ready = '0' then          -- Ready from master on finish 
          nx_st           <= wait_ready;
        elsif (not rw_i and ready = '1') or start_i or stop_i then
          nx_st           <= inc_cnt_rom;
        elsif (rw_i and ready = '1') then
          nx_st           <= write_regs;
        else
          nx_st           <= wait_ready;
        end if;

      when write_regs =>                -- Check if the data is valid
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= data_rom(7 downto 0);  -- From ROM
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
        if error = '1' then
          we_adc          <= '0';
          nx_st           <= idle;
        elsif data_valid = '1' and width = '1' and s = '0' then
          we_adc          <= '0';
          nx_st           <= inc_cnt_rom;  -- Get next instr
        elsif data_valid = '1' and en_add = '1' then
          we_adc          <= '1';
          nx_st           <= inc_cnt_regs;  -- Increment address
        else
          we_adc          <= '0';
          nx_st           <= write_regs;
        end if;

      when inc_cnt_regs =>              -- Get next register address
        cnt_en_add_regs_i <= '1';
        cnt_en_rom_i      <= '0';
        data              <= data_rom(7 downto 0);  -- From ROM
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
        if error = '1' then
          nx_st           <= idle;
        else
          nx_st           <= inc_cnt_rom;
        end if;

      when inc_cnt_rom =>               -- Get next instruction
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '1';
        data              <= data_rom(7 downto 0);  -- From ROM
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
        if error = '1' then
          nx_st           <= idle;
        elsif end_seq_i then
          nx_st           <= idle;
        else
          nx_st           <= wait_1;
        end if;

      when wait_1 =>                    -- Wait 
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= data_rom(7 downto 0);  -- From ROM
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
        if error = '1' then
          nx_st           <= idle;
        elsif end_seq_i then
          nx_st           <= idle;
        else
          nx_st           <= wait_2;
        end if;

      when wait_2 =>                    -- Wait
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= data_rom(7 downto 0);  -- From ROM
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
        if error = '1' then
          nx_st           <= idle;
        elsif end_seq_i then
          nx_st           <= idle;
        else
          nx_st           <= wait_3;
        end if;

      when wait_3 =>                    -- Wait
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= data_rom(7 downto 0);  -- From ROM
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
        if error = '1' then
          nx_st           <= idle;
        elsif end_seq_i then
          nx_st           <= idle;
        else
          nx_st           <= load_data;
        end if;

      when others                    =>
        cnt_en_add_regs_i <= '0';
        cnt_en_rom_i      <= '0';
        data              <= (others => '0');
        load_add_regs_i   <= '0';
        load_rom_i        <= '0';
        new_data          <= '0';
        we_adc            <= '0';
    end case;

  end process fsm;


  -- purpose: Counter for ROM address
  -- type   : sequential
  -- inputs : clk, rstb, load_rom_i, add_rom_i, cnt_en_rom_i
  -- outputs: address_rom
  counter_rom      : process (clk, rstb)
    variable cnt_i : unsigned(9 downto 0);
  begin  -- process counter_rom
    if rstb = '0' then                  -- asynchronous reset (active low)
      address_rom <= (others => '0');
      cnt_i     := (others     => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if load_rom_i = '1' then
        cnt_i   := add_rom_i;
      elsif cnt_en_rom_i = '1' then
        if cnt_i = "111111111" then
          cnt_i := (others     => '0');
        else
          cnt_i := cnt_i + 1;
        end if;
      end if;
      address_rom <= std_logic_vector(cnt_i);
    end if;
  end process counter_rom;

  -- purpose: Counter for Registers addresses
  -- type   : sequential
  -- inputs : clk, rstb, load_rom_i, add_rom_i, cnt_en_rom_i
  -- outputs: address_rom
  counter_regs     : process (clk, rstb)
    variable cnt_i : unsigned(4 downto 0);
  begin  -- process counter_regs
    if rstb = '0' then                  -- asynchronous reset (active low)
      address_regs <= (others => '0');
      cnt_i     := (others    => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if load_add_regs_i = '1' then
        cnt_i   := add_reg_i;
      elsif cnt_en_add_regs_i = '1' then
        if cnt_i = "11111" then
          cnt_i := (others    => '0');
        else
          cnt_i := cnt_i + 1;
        end if;
      end if;
      address_regs <= std_logic_vector(cnt_i);
    end if;
  end process counter_regs;


-- counter_rom: LPM_COUNTER
-- generic map (
-- LPM_WIDTH => 7,
-- LPM_DIRECTION => "UP")
-- port map (
-- data => add_rom_i,
-- clock => clk,
-- cnt_en => cnt_en_rom_i,
-- sload => load_rom_i,
-- aclr => rstb,
-- q => address_rom_i);

-- counter_regs: LPM_COUNTER
-- generic map (
-- LPM_WIDTH => 4,
-- LPM_DIRECTION => "UP")
-- port map (
-- data => std_logic_vector(add_reg_i),
-- clock => clk,
-- cnt_en => cnt_en_add_regs_i,
-- sload => load_add_regs_i,
-- aclr => rstb,
-- q => address_regs_i);
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package sequencer_pack is
  component sequencer
    port (
      clk          : in  std_logic;
      rstb         : in  std_logic;
      data_valid   : in  std_logic;
      data_rom     : in  std_logic_vector(10 downto 0);
      en_add       : in  std_logic;
      error        : in  std_logic;
      ready        : in  std_logic;
      s            : in  std_logic;
      stcnv        : in  std_logic;
      width        : in  std_logic;
      data         : out std_logic_vector(7 downto 0);
      address_rom  : out std_logic_vector(9 downto 0);
      address_regs : out std_logic_vector(4 downto 0);
      new_data     : out std_logic;
      rw           : out std_logic;
      start        : out std_logic;
      stop         : out std_logic;
      we_adc       : out std_logic;
      end_seq      : out std_logic);
  end component;
end sequencer_pack;
------------------------------------------------------------------------------
--
-- EOF
--
