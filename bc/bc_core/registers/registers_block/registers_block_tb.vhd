------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.registers_block_pack.all;
use work.register_config.all;

------------------------------------------------------------------------------
entity registers_block_tb is
end registers_block_tb;

------------------------------------------------------------------------------

architecture test of registers_block_tb is
  constant PERIOD        : time                         := 25 ns;

  signal clk_i         : std_logic                     := '0';
  signal rstb_i        : std_logic                     := '1';
  -- ADC interface 
  signal add_adc_i     : std_logic_vector(4 downto 0)  := (others => '0');
  signal we_adc_i      : std_logic                     := '0';
  signal data_adc_i    : std_logic_vector(15 downto 0) := (others => '0');
  -- Write interface 
  signal add_i         : std_logic_vector(6 downto 0)  := (others => '0');
  signal we_i          : std_logic                     := '0';
  signal din_i         : std_logic_vector(15 downto 0) := (others => '0');
  -- Read interface
  signal add_al_i      : std_logic_vector(6 downto 0)  := (others => '0');
  signal add_sc_i      : std_logic_vector(6 downto 0)  := (others => '0');
  signal dout_al_i     : std_logic_vector(15 downto 0);
  signal dout_sc_i     : std_logic_vector(15 downto 0);
  -- Misc
  signal hadd_i        : std_logic_vector(4 downto 0)  := (others => '0');
  -- Counter input
  signal dstbcnt_in_i  : std_logic_vector(7 downto 0)  := (others => '0');
  signal l1cnt_in_i    : std_logic_vector(15 downto 0) := (others => '0');
  signal l2cnt_in_i    : std_logic_vector(15 downto 0) := (others => '0');
  signal sclkcnt_in_i  : std_logic_vector(15 downto 0) := (others => '0');
  signal l0cnt_in_i    : std_logic_vector(15 downto 0) := (others => '0');
  -- Error input
  signal fmdd_stat_i   : std_logic_vector(15 downto 0) := (others => '0');
  signal par_error_i   : std_logic                     := '0';
  signal paps_error_i  : std_logic                     := '0';
  signal alps_error_i  : std_logic                     := '0';
  signal al_error_i    : std_logic                     := '0';
  signal missed_sclk_i : std_logic                     := '0';
  signal ierr_sc_i     : std_logic                     := '0';
  signal ierr_al_i     : std_logic                     := '0';
  -- Input for registers
  signal cnt_lat_i     : std_logic                     := '0';
  signal cnt_clr_i     : std_logic                     := '0';
  signal csr1_clr_i    : std_logic                     := '0';
  signal bc_rst_i      : std_logic                     := '0';
  signal end_seq_i     : std_logic                     := '0';
  signal st_cnv_i      : std_logic                     := '0';
  -- Output of registers
  signal cnv_mode_i    : std_logic;
  signal csr2_i        : std_logic_vector(15 downto 0);
  signal csr3_i        : std_logic_vector(15 downto 0);
  signal bc_int_i      : std_logic;
  signal bc_error_i    : std_logic;
  signal mebs_i        : std_logic_vector(4 downto 0);
  signal meb_cnt_i     : std_logic_vector(3 downto 0)  := "0100";
  signal i             : integer;
  signal adc_value_i   : integer                       := BASE_ADC;

  type state is (idle, set, output, finish);
  signal st : state;

  -- purpose: Send and instruction to register block
  procedure write_reg (
    constant code    : in  integer;                        -- Instruction code
    signal   add     : out std_logic_vector(6 downto 0);   -- Address
    signal   data    : out std_logic_vector(15 downto 0);  -- Data
    signal   write   : out std_logic) is                   -- Write
    variable write_i :     std_logic;
  begin  -- write_reg
    if regs(code).w then
      add     <= std_logic_vector(to_unsigned(code, 7));
      data    <= std_logic_vector(to_unsigned(code, 16));
      write <= '0', '1' after PERIOD, '0' after 2*PERIOD;
    end if;
  end write_reg;

  -- purpose: Send command
  procedure read_reg (constant code  : in  integer;  -- Instruction code
                      constant delay : in  time;
                      constant len   : in  time;
                      signal   add   : out std_logic_vector(6 downto 0)) is
  begin  -- read_reg
    add <= (others => '0'),
           std_logic_vector(to_unsigned(code,7)) after delay,
           (others => '0') after delay + len;
  end read_reg;
  
  -- purpose: Send a command 
  procedure command (
    signal cmd : out std_logic;         -- Command pin
    signal clk : in  std_logic) is      -- Clock
  begin  -- cmd
    wait until rising_edge(clk);
    cmd <= '1';
    wait until rising_edge(clk);
    cmd <= '0';
    wait for 2 * PERIOD;
  end command;


  -- purpose: Convert std_logic_vector to a string
  function slv2str (
    constant v   : std_logic_vector)
    return string
  is
    variable ret : string(v'length downto 1);
    variable i   : integer;
  begin  -- function slv2str
    ret := (others => 'X');

    for i in v'range loop
      case v(i) is
        when 'U'    => ret(i+1) := 'U';
        when 'X'    => ret(i+1) := 'X';
        when '0'    => ret(i+1) := '0';
        when '1'    => ret(i+1) := '1';
        when 'Z'    => ret(i+1) := 'Z';
        when 'W'    => ret(i+1) := 'W';
        when 'L'    => ret(i+1) := 'L';
        when 'H'    => ret(i+1) := 'H';
        when '-'    => ret(i+1) := '-';
        when others => ret(i+1) := 'X';
      end case;
    end loop;  -- i
    return ret;
  end function slv2str;

  -- Purpose start a fake conversion
  procedure convert (
    constant start_val : in  integer range 0 to 1023;  -- Start value
    signal   clk       : in  std_logic;                -- Clock
    signal   end_seq   : in  std_logic;                -- End conversion
    signal   st_cnv    : out std_logic;                -- Start conversion
    signal   adc_val   : out integer) is               -- Base ADC value
  begin
    -- Signal start conversion
    report "Starting conversion with base=" &
      integer'image(start_val) severity note; 
    adc_val <= start_val;
    wait until rising_edge(clk);
    command(cmd => st_cnv, clk => clk);

    wait until end_seq = '1';
    wait until end_seq = '0';
    wait until rising_edge(clk);
  end;

begin  -- test

  dut : entity work.registers_block(rtl4)
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        -- ADC interface 
        add_adc     => add_adc_i,       -- in  ADC address
        we_adc      => we_adc_i,        -- in  Write enable from monitor ADC
        data_adc    => data_adc_i,      -- in  Monitor ADC data
        -- Write interface 
        add         => add_i,           -- in  Register address
        we          => we_i,            -- in  Write enable
        din         => din_i,           -- in  Register data
        -- Read interface
        add_al      => add_al_i,        -- in  ALTRO output address 
        add_sc      => add_sc_i,        -- in  I2C output address 
        dout_al     => dout_al_i,       -- in  ALTRO output data 
        dout_sc     => dout_sc_i,       -- in  I2C output data
        -- Misc 
        hadd        => hadd_i,          -- in  Card address
        -- Counters
        dstbcnt_in  => dstbcnt_in_i,    -- in  Data strobe counts
        l0cnt_in    => l0cnt_in_i,      -- in  L0 counters
        l1cnt_in    => l1cnt_in_i,      -- in  L1 counts
        l2cnt_in    => l2cnt_in_i,      -- in  L2 counts
        sclkcnt_in  => sclkcnt_in_i,    -- in  Slow clock counts
        -- Errors
        fmdd_stat   => fmdd_stat_i,     -- in  FMDD status
        par_error   => par_error_i,     -- in  Parity error
        paps_error  => paps_error_i,    -- in  PASA power sup. error
        alps_error  => alps_error_i,    -- in  ALTRO power sup. error
        al_error    => al_error_i,      -- in  ALTRO error
        missed_sclk => missed_sclk_i,   -- in  Missed slow clock alarm
        ierr_sc     => ierr_sc_i,       -- in  I2C instruction error
        ierr_al     => ierr_al_i,       -- in  Bus instruction error
        -- Commands 
        cnt_lat     => cnt_lat_i,       -- in  Command: Latch counters
        cnt_clr     => cnt_clr_i,       -- in  Command: Clear counters
        csr1_clr    => csr1_clr_i,      -- in  Command: Clear CSR1
        bc_rst      => bc_rst_i,        -- in  Command: Reset BC
        end_seq     => end_seq_i,       -- in  End of Monitor flag
        st_cnv      => st_cnv_i,        -- in  Command: Start monitor
        -- Output from registers 
        cnv_mode    => cnv_mode_i,      -- out Continuous conversion flag
        csr2        => csr2_i,          -- out Config/Status 2
        csr3        => csr3_i,          -- out Config/Status 3
        -- dout        => dout_i,          -- out Data out
        bc_int      => bc_int_i,        -- out BC interrupt
        bc_error    => bc_error_i,      -- out BC error
        mebs        => mebs_i,
        meb_cnt     => meb_cnt_i);


  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    rstb_i <= '0';
    wait for 100 ns;
    rstb_i <= '1';
    wait for 5 * PERIOD;
    wait until rising_edge(clk_i);

    -- Write thresholds
    for i in adc_map'range loop
      wait until rising_edge(clk_i);
      write_reg(code => adc_map(i)-5,
                add => add_i,
                data => din_i,
                write=> we_i);
      wait for 5 * PERIOD;
    end loop;  -- i

    wait until rising_edge(clk_i);
    report "Sending the FMD command" severity note;
    write_reg(code => add_fmdd_cmd,
              add => add_i,
              data => din_i,
              write=> we_i);
    wait for 5 * PERIOD;
    
    -- Read counters, and other registers
    for i in add_l1cnt to add_mebs loop
      wait until rising_edge(clk_i);
      read_reg(code  => i,
               delay => 1 ns,
               len   => 50 ns,
               add   => add_al_i);
      read_reg(code  => i-3,
               delay => 10 ns,
               len   => 50 ns,
               add   => add_sc_i);
      wait for 5 * PERIOD;
    end loop;  -- i
    
    -- Clear status register
    command(cmd =>  csr1_clr_i, clk => clk_i);

    -- Lat counters
    command(cmd => cnt_lat_i, clk => clk_i);

    -- Read counters
    for i in add_l1cnt to add_dstbcnt loop
      wait until rising_edge(clk_i);
      read_reg(code  => i,
               delay => 1 ns,
               len   => 50 ns,
               add   => add_al_i);
      read_reg(code  => i,
               delay => 10 ns,
               len   => 50 ns,
               add   => add_sc_i);
      wait for 5 * PERIOD;
    end loop;  -- i

    -- Clear counters
    command(cmd => cnt_clr_i, clk => clk_i);

    -- Clear counters
    command(cmd => bc_rst_i, clk => clk_i);


    -- Signal start conversion
    convert (start_val => 6,
             clk       => clk_i,
             end_seq   => end_seq_i,
             st_cnv    => st_cnv_i,
             adc_val   => adc_value_i);
    assert bc_int_i = '0' report "Unexpected interrupt" severity warning;
    -- Signal start conversion
    convert (start_val => to_integer(unsigned(regs(add_t1).def)),
             clk       => clk_i,
             end_seq   => end_seq_i,
             st_cnv    => st_cnv_i,
             adc_val   => adc_value_i);
    write_reg(code  => add_t1,
              add   => add_i,
              data  => din_i,
              write => we_i);
    assert bc_int_i = '0' report "Unexpected interrupt" severity warning;
    -- Signal start conversion
    convert (start_val => 6,
             clk       => clk_i,
             end_seq   => end_seq_i,
             st_cnv    => st_cnv_i,
             adc_val   => adc_value_i);
    assert bc_int_i = '0' report "Unexpected interrupt" severity warning;
    -- Signal start conversion
    convert (start_val => to_integer(unsigned(regs(add_t1).def)),
             clk       => clk_i,
             end_seq   => end_seq_i,
             st_cnv    => st_cnv_i,
             adc_val   => adc_value_i);
    assert bc_int_i = '0' report "Unexpected interrupt" severity warning;
    -- Signal start conversion
    convert (start_val => to_integer(unsigned(regs(add_t1).def)),
             clk       => clk_i,
             end_seq   => end_seq_i,
             st_cnv    => st_cnv_i,
             adc_val   => adc_value_i);
    assert bc_int_i = '0' report "Unexpected interrupt" severity warning;
    -- Signal start conversion
    convert (start_val => to_integer(unsigned(regs(add_t1).def)),
             clk       => clk_i,
             end_seq   => end_seq_i,
             st_cnv    => st_cnv_i,
             adc_val   => adc_value_i);
    assert bc_int_i = '0' report "Unexpected interrupt" severity warning;
    -- Signal start conversion
    convert (start_val => to_integer(unsigned(regs(add_t1).def)),
             clk       => clk_i,
             end_seq   => end_seq_i,
             st_cnv    => st_cnv_i,
             adc_val   => adc_value_i);
    assert bc_int_i = '1' report "Missing interrupt" severity warning;
    -- Signal start conversion
    convert (start_val => 6,
             clk       => clk_i,
             end_seq   => end_seq_i,
             st_cnv    => st_cnv_i,
             adc_val   => adc_value_i);
    assert bc_int_i = '0' report "Unexpected interrupt" severity warning;
    

    -- Read ADC's
    for i in adc_map'range loop
      wait until rising_edge(clk_i);
      read_reg(code  => adc_map(i),
               delay => 1 ns,
               len   => 25 ns,
               add   => add_sc_i);
    end loop;  -- i
    
    wait;                               -- forever 
  end process stimuli;

  -- purpose: Count up counters
  -- type   : sequential
  -- inputs : clk, rstb_i
  -- outputs: 
  count_up             : process (clk_i, rstb_i)
    variable l1cnt_i   : unsigned(15 downto 0);
    variable l2cnt_i   : unsigned(15 downto 0);
    variable sclkcnt_i : unsigned(15 downto 0);
    variable dstbcnt_i : unsigned(7 downto 0);
  begin  -- process count_up
    if rstb_i = '0' then                -- asynchronous reset (active low)
      l1cnt_in_i   <= (others => '0');
      l2cnt_in_i   <= (others => '0');
      sclkcnt_in_i <= (others => '0');
      dstbcnt_in_i <= (others => '0');
      l1cnt_i     := (others  => '0');
      l2cnt_i     := (others  => '0');
      sclkcnt_i   := (others  => '0');
      dstbcnt_i   := (others  => '0');
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if l1cnt_i = X"FFFF" or cnt_clr_i = '1' then
        l1cnt_i   := (others  => '0');
      else
        l1cnt_i   := l1cnt_i + 1;
      end if;
      if l2cnt_i = X"FFFF" or cnt_clr_i = '1' then
        l2cnt_i   := (others  => '0');
      else
        l2cnt_i   := l2cnt_i + 1;
      end if;
      if sclkcnt_i = X"FFFF" or cnt_clr_i = '1' then
        sclkcnt_i := (others  => '0');
      else
        sclkcnt_i := sclkcnt_i + 1;
      end if;
      if dstbcnt_i = X"FF" or cnt_clr_i = '1' then
        dstbcnt_i := (others  => '0');
      else
        dstbcnt_i := dstbcnt_i + 1;
      end if;
      l1cnt_in_i   <= std_logic_vector(l1cnt_i);
      l2cnt_in_i   <= std_logic_vector(l2cnt_i);
      sclkcnt_in_i <= std_logic_vector(sclkcnt_i);
      dstbcnt_in_i <= std_logic_vector(dstbcnt_i);
    end if;
  end process count_up;

  -- purpose: Decrement meb counter
  -- type   : combinational
  -- inputs : 
  -- outputs: meb_cnt_i
  meb_counter: process
  begin  -- process meb_counter
    wait for 4 us;
    if (meb_cnt_i = "0000")  then
      meb_cnt_i <= "0100";
    else
      meb_cnt_i <= std_logic_vector(unsigned(meb_cnt_i) - 1);
    end if;
  end process meb_counter;
  
  -- purpose: Make fake ADC values
  -- type   : sequential
  -- inputs : clk, rstb_i
  -- outputs: 
  fake_adcs        : process (clk_i, rstb_i)
    variable cur_i : integer range 0 to 255;
    variable val_i : integer range 0 to 1023;
  begin  -- process fake_adcs
    if rstb_i = '0' then                  -- asynchronous reset (active low)
      we_adc_i   <= '0';
      end_seq_i  <= '0';
      add_adc_i  <= (others => '0');
      data_adc_i <= (others => '0');
      cur_i      := BASE_ADC;
      val_i      := adc_value_i;
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      we_adc_i   <= '0';
      end_seq_i  <= '0';
      add_adc_i  <= add_adc_i;
      data_adc_i <= data_adc_i;
      
      case st is
        when idle =>
          if st_cnv_i = '1' then
            st <= set;
            cur_i := BASE_ADC;
            val_i := adc_value_i;
          end if;

        when set                             =>
          add_adc_i               <= std_logic_vector(to_unsigned(cur_i, 5));
          data_adc_i(15 downto 6) <= std_logic_vector(to_unsigned(val_i, 10));
          data_adc_i(5 downto 0)  <= (others => '0');
          st                      <= output;

        when output =>
          -- report "Writing to ADC register @ " &
          --   integer'image(cur_i) & "=" & integer'image(val_i) severity note;
          we_adc_i   <= '1';
          cur_i := cur_i + 1;
          val_i := val_i + 1;
          if (cur_i >= BASE_ADC + NUM_ADC) then
             st       <= finish;
          else
            st       <= set;
          end if;

        when finish             =>
          add_adc_i  <= (others => '0');
          data_adc_i <= (others => '0');
          end_seq_i  <= '1';
          st         <= idle;

        when others => null;
      end case;
    end if;
  end process fake_adcs;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen            : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i          <= not clk_i;
  end process clock_gen;

end test;
------------------------------------------------------------------------------
--
-- EOF
--
