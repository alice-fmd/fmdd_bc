------------------------------------------------------------------------------
-- Title      : Handle register I/O
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : registers_block.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2010-01-13
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Component to handle register I/O
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.register_config.all;
use work.int_mstable_pack.all;

entity registers_block is
  port (
    clk         : in  std_logic;        --! Clock
    rstb        : in  std_logic;        --! Async reset
    -- ADC interface 
    add_adc     : in  std_logic_vector(4 downto 0);    --! ADC address
    we_adc      : in  std_logic;        --! Write enable from monitor ADC
    data_adc    : in  std_logic_vector(15 downto 0);   --! Monitor ADC data
    -- Write interface 
    add         : in  std_logic_vector(6 downto 0);    --! Register address
    we          : in  std_logic;        --! Write enable
    din         : in  std_logic_vector(15 downto 0);   --! Register data
    -- Read interface 
    add_al      : in  std_logic_vector(6 downto 0);    --! Data out
    add_sc      : in  std_logic_vector(6 downto 0);    --! Data out
    dout_al     : out std_logic_vector(15 downto 0);   --! Data out
    dout_sc     : out std_logic_vector(15 downto 0);   --! Data out
    -- Misc input
    hadd        : in  std_logic_vector(4 downto 0);    --! Card address
    -- Counters input 
    dstbcnt_in  : in  std_logic_vector(7 downto 0);    --! Data strobe counts
    l0cnt_in    : in  std_logic_vector(15 downto 0);   --! L0 counters
    l1cnt_in    : in  std_logic_vector(15 downto 0);   --! L1 counts
    l2cnt_in    : in  std_logic_vector(15 downto 0);   --! L2 counts
    sclkcnt_in  : in  std_logic_vector(15 downto 0);   --! Slow clock counts
    -- Errors 
    par_error   : in  std_logic;        --! Parity error
    paps_error  : in  std_logic;        --! PASA power sup. error
    alps_error  : in  std_logic;        --! ALTRO power sup. error
    al_error    : in  std_logic;        --! ALTRO error
    missed_sclk : in  std_logic;        --! Missed slow clock alarm
    ierr_sc     : in  std_logic;        --! I2C instruction error
    ierr_al     : in  std_logic;        --! Bus instruction error
    -- Commands 
    cnt_lat     : in  std_logic;        --! Command: Latch counters
    cnt_clr     : in  std_logic;        --! Command: Clear counters
    csr1_clr    : in  std_logic;        --! Command: Clear CSR1
    bc_rst      : in  std_logic;        --! Command: Reset BC
    end_seq     : in  std_logic;        --! End of Monitor flag
    st_cnv      : in  std_logic;        --! Command: Start monitor
    fmdd_stat   : in  std_logic_vector(15 downto 0);   --! FMDD status
    -- Output of registers
    cnv_mode    : out std_logic;        --! Continuous conversion flag
    csr2        : out std_logic_vector(15 downto 0);   --! Config/Status 2
    csr3        : out std_logic_vector(15 downto 0);   --! Config/Status 3
    tsm_word    : out std_logic_vector(8 downto 0);    --! TSM:  word
    us_ratio    : out std_logic_vector(15 downto 0);   --! TSM: Under sampling
    bc_int      : out std_logic;        --! BC interrupt
    bc_error    : out std_logic;        --! BC error
    hold_wait   : out std_logic_vector(15 downto 0);   --! FMD: Wait to hold
    l1_timeout  : out std_logic_vector(15 downto 0);   --! FMD: L1 timeout
    l2_timeout  : out std_logic_vector(15 downto 0);   --! FMD: L2 timeout
    shift_div   : out std_logic_vector(15 downto 0);   --! FMD: Shift clk
    strips      : out std_logic_vector(15 downto 0);   --! FMD: Strips
    cal_level   : out std_logic_vector(15 downto 0);   --! FMD: Cal pulse
    shape_bias  : out std_logic_vector(15 downto 0);   --! FMD: Shape bias
    vfs         : out std_logic_vector(15 downto 0);   --! FMD: Shape ref
    vfp         : out std_logic_vector(15 downto 0);   --! FMD: Preamp ref
    sample_div  : out std_logic_vector(15 downto 0);   --! FMD: Sample clk
    fmdd_cmd    : out std_logic_vector(15 downto 0);   --! FMD: Commands
    cal_iter    : out std_logic_vector(15 downto 0);   --! FMD: cal events
    mebs        : out std_logic_vector(4 downto 0);    --! MEB config
    meb_cnt     : in  std_logic_vector(3 downto 0);    --! MEB counter
    cal_delay   : out std_logic_vector(15 downto 0));  --! FMD: xtra L1 delay
end registers_block;


-------------------------------------------------------------------------------
architecture rtl3 of registers_block is
  -- Cached values 
  signal t1_th_i        : std_logic_vector(9 downto 0);  --! First ADC T
  signal flash_i_th_i   : std_logic_vector(9 downto 0);  --! I  3.3 V
  signal al_dig_i_th_i  : std_logic_vector(9 downto 0);  --! I  2.5 V altro dig
  signal al_ana_i_th_i  : std_logic_vector(9 downto 0);  --! I  2.5 V altro ana
  signal va_rec_ip_th_i : std_logic_vector(9 downto 0);  --! I  2.5 V VA
  signal t1_i           : std_logic_vector(9 downto 0);  --! First ADC T
  signal flash_i_i      : std_logic_vector(9 downto 0);  --! I  3.3 V
  signal al_dig_i_i     : std_logic_vector(9 downto 0);  --! I  2.5 V altro dig
  signal al_ana_i_i     : std_logic_vector(9 downto 0);  --! I  2.5 V altro ana
  signal va_rec_ip_i    : std_logic_vector(9 downto 0);  --! I  2.5 V VA
  signal t2_th_i        : std_logic_vector(9 downto 0);  --! Second ADC T
  signal va_sup_ip_th_i : std_logic_vector(9 downto 0);  --! I  1.5 V VA
  signal va_rec_im_th_i : std_logic_vector(9 downto 0);  --! I -2.0 V
  signal va_sup_im_th_i : std_logic_vector(9 downto 0);  --! I -2.0 V VA
  signal gtl_u_th_i     : std_logic_vector(9 downto 0);  --! U  2.5 V Digital
  signal t2_i           : std_logic_vector(9 downto 0);  --! Second ADC T
  signal va_sup_ip_i    : std_logic_vector(9 downto 0);  --! I  1.5 V VA
  signal va_rec_im_i    : std_logic_vector(9 downto 0);  --! I -2.0 V
  signal va_sup_im_i    : std_logic_vector(9 downto 0);  --! I -2.0 V VA
  signal gtl_u_i        : std_logic_vector(9 downto 0);  --! U  2.5 V Digital 
  signal t3_th_i        : std_logic_vector(9 downto 0);  --! Third ADC T
  signal t1sens_th_i    : std_logic_vector(9 downto 0);  --! Temperature sens. 1
  signal t2sens_th_i    : std_logic_vector(9 downto 0);  --! Temperature sens. 2
  signal al_dig_u_th_i  : std_logic_vector(9 downto 0);  --! U  2.5 altro dig
  signal al_ana_u_th_i  : std_logic_vector(9 downto 0);  --! U  2.5 altro ana
  signal t3_i           : std_logic_vector(9 downto 0);  --! Third ADC T
  signal t1sens_i       : std_logic_vector(9 downto 0);  --! Temperature sens. 1
  signal t2sens_i       : std_logic_vector(9 downto 0);  --! Temperature sens. 2
  signal al_dig_u_i     : std_logic_vector(9 downto 0);  --! U  2.5 altro dig
  signal al_ana_u_i     : std_logic_vector(9 downto 0);  --! U  2.5 altro ana
  signal t4_th_i        : std_logic_vector(9 downto 0);  --! Forth ADC T
  signal va_rec_up_th_i : std_logic_vector(9 downto 0);  --! U  2.5 VA (m)
  signal va_sup_up_th_i : std_logic_vector(9 downto 0);  --! U  1.5 VA (m)
  signal va_sup_um_th_i : std_logic_vector(9 downto 0);  --! U -2.0 VA (m)
  signal va_rec_um_th_i : std_logic_vector(9 downto 0);  --! U -2.0 (m)
  signal t4_i           : std_logic_vector(9 downto 0);  --! Forth ADC T
  signal va_rec_up_i    : std_logic_vector(9 downto 0);  --! U  2.5 VA (m)
  signal va_sup_up_i    : std_logic_vector(9 downto 0);  --! U  1.5 VA (m)
  signal va_sup_um_i    : std_logic_vector(9 downto 0);  --! U -2.0 VA (m)
  signal va_rec_um_i    : std_logic_vector(9 downto 0);  --! U -2.0 (m)
  signal l1cnt_i        : std_logic_vector(15 downto 0);  --! L1 counter
  signal l2cnt_i        : std_logic_vector(15 downto 0);  --! L2 counter
  signal sclkcnt_i      : std_logic_vector(15 downto 0);  --! Slow clock counter
  -- signal dstbcnt_i   : std_logic_vector(7 downto 0);  --! Data strobe counter
  signal tsm_word_i     : std_logic_vector(8 downto 0);  --! Test mode words
  signal us_ratio_i     : std_logic_vector(15 downto 0) := (others => '0');
  signal csr0_i         : std_logic_vector(10 downto 0) := (others => '0');
  signal csr1_i         : std_logic_vector(13 downto 0);  --! Config/Status 1
  signal csr2_i         : std_logic_vector(15 downto 0);  --! Config/Status 2
  signal csr3_i         : std_logic_vector(15 downto 0);  --! Config/Status 3
  signal l0cnt_i        : std_logic_vector(15 downto 0);  --! L0 counters
  signal hold_wait_i    : std_logic_vector(15 downto 0);  --! FMD: Wait to hold
  signal l1_timeout_i   : std_logic_vector(15 downto 0);  --! FMD: L1 timeout
  signal l2_timeout_i   : std_logic_vector(15 downto 0);  --! FMD: L2 timeout
  signal shift_div_i    : std_logic_vector(15 downto 0);  --! FMD: Shift clk
  signal strips_i       : std_logic_vector(15 downto 0);  --! FMD: Strips
  signal cal_level_i    : std_logic_vector(15 downto 0);  --! FMD: Cal pulse
  signal cal_iter_i     : std_logic_vector(15 downto 0);  --! FMD: Cal events
  signal shape_bias_i   : std_logic_vector(15 downto 0);  --! FMD: Shape bias
  signal vfs_i          : std_logic_vector(15 downto 0);  --! FMD: Shape ref
  signal vfp_i          : std_logic_vector(15 downto 0);  --! FMD: Preamp ref
  signal sample_div_i   : std_logic_vector(15 downto 0);  --! FMD: Sample clk
  signal fmdd_cmd_i     : std_logic_vector(15 downto 0);  --! FMD: Commands
  signal mebs_i         : std_logic_vector(4 downto 0);  --! MEB config
  signal cal_delay_i    : std_logic_vector(15 downto 0);  --! CAL xtra delay

  -- Register values
  signal t1_th_r        : std_logic_vector(9 downto 0);  --! First ADC T
  signal flash_i_th_r   : std_logic_vector(9 downto 0);  --! I  3.3 V
  signal al_dig_i_th_r  : std_logic_vector(9 downto 0);  --! I  2.5 V altro dig
  signal al_ana_i_th_r  : std_logic_vector(9 downto 0);  --! I  2.5 V altro ana
  signal va_rec_ip_th_r : std_logic_vector(9 downto 0);  --! I  2.5 V VA
  signal t1_r           : std_logic_vector(9 downto 0);  --! First ADC T
  signal flash_i_r      : std_logic_vector(9 downto 0);  --! I  3.3 V
  signal al_dig_i_r     : std_logic_vector(9 downto 0);  --! I  2.5 V altro dig
  signal al_ana_i_r     : std_logic_vector(9 downto 0);  --! I  2.5 V altro ana
  signal va_rec_ip_r    : std_logic_vector(9 downto 0);  --! I  2.5 V VA
  signal t2_th_r        : std_logic_vector(9 downto 0);  --! Second ADC T
  signal va_sup_ip_th_r : std_logic_vector(9 downto 0);  --! I  1.5 V VA
  signal va_rec_im_th_r : std_logic_vector(9 downto 0);  --! I -2.0 V
  signal va_sup_im_th_r : std_logic_vector(9 downto 0);  --! I -2.0 V VA
  signal gtl_u_th_r     : std_logic_vector(9 downto 0);  --! U  2.5 V Digital
  signal t2_r           : std_logic_vector(9 downto 0);  --! Second ADC T
  signal va_sup_ip_r    : std_logic_vector(9 downto 0);  --! I  1.5 V VA
  signal va_rec_im_r    : std_logic_vector(9 downto 0);  --! I -2.0 V
  signal va_sup_im_r    : std_logic_vector(9 downto 0);  --! I -2.0 V VA
  signal gtl_u_r        : std_logic_vector(9 downto 0);  --! U  2.5 V Digital
  signal t3_th_r        : std_logic_vector(9 downto 0);  --! Third ADC T
  signal t1sens_th_r    : std_logic_vector(9 downto 0);  --! Temperature sens. 1
  signal t2sens_th_r    : std_logic_vector(9 downto 0);  --! Temperature sens. 2
  signal al_dig_u_th_r  : std_logic_vector(9 downto 0);  --! U  2.5 altro dig
  signal al_ana_u_th_r  : std_logic_vector(9 downto 0);  --! U  2.5 altro analog
  signal t3_r           : std_logic_vector(9 downto 0);  --! Third ADC T
  signal t1sens_r       : std_logic_vector(9 downto 0);  --! Temperature sens. 1
  signal t2sens_r       : std_logic_vector(9 downto 0);  --! Temperature sens. 2
  signal al_dig_u_r     : std_logic_vector(9 downto 0);  --! U  2.5 altro dig
  signal al_ana_u_r     : std_logic_vector(9 downto 0);  --! U  2.5 altro analog
  signal t4_th_r        : std_logic_vector(9 downto 0);  --! Th: Forth ADC T
  signal va_rec_up_th_r : std_logic_vector(9 downto 0);  --! Th: U  2.5 VA (m)
  signal va_sup_up_th_r : std_logic_vector(9 downto 0);  --! Th: U  1.5 VA (m)
  signal va_sup_um_th_r : std_logic_vector(9 downto 0);  --! Th: U -2.0 VA (m)
  signal va_rec_um_th_r : std_logic_vector(9 downto 0);  --! Th: U -2.0 (m)
  signal t4_r           : std_logic_vector(9 downto 0);  --! Cu: Forth ADC T
  signal va_rec_up_r    : std_logic_vector(9 downto 0);  --! Cu: U  2.5 VA (m)
  signal va_sup_up_r    : std_logic_vector(9 downto 0);  --! Cu: U  1.5 VA (m)
  signal va_sup_um_r    : std_logic_vector(9 downto 0);  --! Cu: U -2.0 VA (m)
  signal va_rec_um_r    : std_logic_vector(9 downto 0);  --! Cu: U -2.0 (m)
  signal l1cnt_r        : std_logic_vector(15 downto 0);  --! L1 counter
  signal l2cnt_r        : std_logic_vector(15 downto 0);  --! L2 counter
  signal sclkcnt_r      : std_logic_vector(15 downto 0);  --! Slow clock counter
  signal dstbcnt_r      : std_logic_vector(7 downto 0);  --! Data strobe counter
  signal tsm_word_r     : std_logic_vector(8 downto 0);  --! Test mode words
  signal us_ratio_r     : std_logic_vector(15 downto 0);  --! Under sampling ra
  signal csr0_r         : std_logic_vector(10 downto 0) := (others => '0');
  signal csr1_r         : std_logic_vector(13 downto 0) := (others => '0');
  signal csr2_r         : std_logic_vector(15 downto 0);  --! Config/Status 2
  signal csr3_r         : std_logic_vector(15 downto 0);  --! Config/Status 3
  signal fmdd_stat_r    : std_logic_vector(15 downto 0);  --! FMDD status
  signal l0cnt_r        : std_logic_vector(15 downto 0);  --! L0 counters
  signal hold_wait_r    : std_logic_vector(15 downto 0);  --! FMD: Wait to hold
  signal l1_timeout_r   : std_logic_vector(15 downto 0);  --! FMD: L1 timeout
  signal l2_timeout_r   : std_logic_vector(15 downto 0);  --! FMD: L2 timeout
  signal shift_div_r    : std_logic_vector(15 downto 0);  --! FMD: Shift clk
  signal strips_r       : std_logic_vector(15 downto 0);  --! FMD: Strips
  signal cal_level_r    : std_logic_vector(15 downto 0);  --! FMD: Cal pulse
  signal cal_iter_r     : std_logic_vector(15 downto 0);  --! FMD: Cal events
  signal shape_bias_r   : std_logic_vector(15 downto 0);  --! FMD: Shape bias
  signal vfs_r          : std_logic_vector(15 downto 0);  --! FMD: Shape ref
  signal vfp_r          : std_logic_vector(15 downto 0);  --! FMD: Preamp ref
  signal sample_div_r   : std_logic_vector(15 downto 0);  --! FMD: Sample clk
  signal fmdd_cmd_r     : std_logic_vector(15 downto 0);  --! FMD: Commands
  signal mebs_r         : std_logic_vector(8 downto 0);  --! MEB config
  signal cal_delay_r    : std_logic_vector(15 downto 0);  --! CAL xtra delay

  -- Temporaries
  signal cnv_mode_i    : std_logic;
  signal bc_error_i    : std_logic;
  signal bc_int_i      : std_logic;
  signal meb_cnt_i     : std_logic_vector(3 downto 0);
  signal csr1_ii       : std_logic_vector(13 downto 0);  --! Config/Status 1
  signal csr1_clrrst_i : std_logic;

  -- Parameters for interrupt glitch filters
  constant INT_TIMES  : integer := 2;     -- Times out of bounds before int
  constant AUTO_CLEAR : boolean := true;  -- Wether to clear interrupt when OK

  signal ints_i : std_logic_vector(19 downto 0);

begin  -- rtl
  -- purpose: Assign interrupt lines
  t1_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => t1_r,
              threshold => t1_th_r,
              int       => ints_i(0));
  t2_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => t2_r,
              threshold => t2_th_r,
              int       => ints_i(1));
  t3_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => t3_r,
              threshold => t3_th_r,
              int       => ints_i(2));
  t4_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => t4_r,
              threshold => t4_th_r,
              int       => ints_i(3));
  t1sens_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => t1sens_r,
              threshold => t1sens_th_r,
              int       => ints_i(4));
  t2sens_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => t2sens_r,
              threshold => t2sens_th_r,
              int       => ints_i(5));
  -- Analog currents
  al_ana_i_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => al_ana_i_r,
              threshold => al_ana_i_th_r,
              int       => ints_i(6));
  va_sup_ip_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => va_sup_ip_r,
              threshold => va_sup_ip_th_r,
              int       => ints_i(7));
  va_rec_im_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => va_rec_im_r,
              threshold => va_rec_im_th_r,
              int       => ints_i(8));
  va_sup_im_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => va_sup_im_r,
              threshold => va_sup_im_th_r,
              int       => ints_i(9));
  -- Digitial currents
  al_dig_i_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => al_dig_i_r,
              threshold => al_dig_i_th_r,
              int       => ints_i(10));
  flash_i_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => flash_i_r,
              threshold => flash_i_th_r,
              int       => ints_i(11));
  va_rec_ip_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => va_rec_ip_r,
              threshold => va_rec_ip_th_r,
              int       => ints_i(12));
  -- Analog voltages
  va_sup_um_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => va_sup_um_r,
              threshold => va_sup_um_th_r,
              int       => ints_i(13));
  va_rec_um_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => true,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => va_rec_um_r,
              threshold => va_rec_um_th_r,
              int       => ints_i(14));
  al_ana_u_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => false,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => al_ana_u_r,
              threshold => al_ana_u_th_r,
              int       => ints_i(15));
  va_sup_up_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => false,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => va_sup_up_r,
              threshold => va_sup_up_th_r,
              int       => ints_i(16));
  -- Digital voltages
  gtl_u_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => false,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => gtl_u_r,
              threshold => gtl_u_th_r,
              int       => ints_i(17));
  al_dig_u_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => false,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => al_dig_u_r,
              threshold => al_dig_u_th_r,
              int       => ints_i(18));
  va_rec_up_ms : int_mstable
    generic map (TIMES => INT_TIMES,
                 WIDTH => 10,
                 OVER  => false,
                 AUTO  => AUTO_CLEAR) 
    port map (clk       => clk,
              rstb      => rstb,
              rst       => csr1_clrrst_i,
              enable    => end_seq,
              value     => va_rec_up_r,
              threshold => va_rec_up_th_r,
              int       => ints_i(19));

  -- purpose: Register monitor ADC input
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  adc_register : process (clk, rstb)
    variable idx_adc_i : integer;
    variable add_adc_i : integer;
    variable val_adc_i : std_logic_vector(9 downto 0) := (others => '0');
  begin  -- process adc_registers

    if rstb = '0' then                  -- asynchronous reset (active low)
      t1_i        <= regs(add_t1).def(9 downto 0);
      flash_i_i   <= regs(add_flash_i).def(9 downto 0);
      al_dig_i_i  <= regs(add_al_dig_i).def(9 downto 0);
      al_ana_i_i  <= regs(add_al_ana_i).def(9 downto 0);
      va_rec_ip_i <= regs(add_va_rec_ip).def(9 downto 0);
      t2_i        <= regs(add_t2).def(9 downto 0);
      va_sup_ip_i <= regs(add_va_sup_ip).def(9 downto 0);
      va_rec_im_i <= regs(add_va_rec_im).def(9 downto 0);
      va_sup_im_i <= regs(add_va_sup_im).def(9 downto 0);
      gtl_u_i     <= regs(add_gtl_u).def(9 downto 0);
      t3_i        <= regs(add_t3).def(9 downto 0);
      t1sens_i    <= regs(add_t1sens).def(9 downto 0);
      t2sens_i    <= regs(add_t2sens).def(9 downto 0);
      al_dig_u_i  <= regs(add_al_dig_u).def(9 downto 0);
      al_ana_u_i  <= regs(add_al_ana_u).def(9 downto 0);
      t4_i        <= regs(add_t4).def(9 downto 0);
      va_rec_up_i <= regs(add_va_rec_up).def(9 downto 0);
      va_sup_up_i <= regs(add_va_sup_up).def(9 downto 0);
      va_sup_um_i <= regs(add_va_sup_um).def(9 downto 0);
      va_rec_um_i <= regs(add_va_rec_um).def(9 downto 0);
      idx_adc_i   := 0;
      add_adc_i   := 0;
      val_adc_i   := (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_adc = '1' then
        idx_adc_i := to_integer(unsigned(add_adc));
        assert idx_adc_i >= BASE_ADC and idx_adc_i < BASE_ADC+NUM_ADC
          report "Index " & integer'image(idx_adc_i) & " out of range"
          severity failure;
        add_adc_i := adc_map(idx_adc_i);
        val_adc_i := data_adc(15 downto 6);

        case add_adc_i is
          when add_t1        => t1_i        <= val_adc_i;  -- First ADC T     
          when add_flash_i   => flash_i_i   <= val_adc_i;  -- I  3.3 V        
          when add_al_dig_i  => al_dig_i_i  <= val_adc_i;  -- I  2.5 V dig
          when add_al_ana_i  => al_ana_i_i  <= val_adc_i;  -- I  2.5 V ana
          when add_va_rec_ip => va_rec_ip_i <= val_adc_i;  -- I  2.5 V VA      
          when add_t2        => t2_i        <= val_adc_i;  -- Second ADC T    
          when add_va_sup_ip => va_sup_ip_i <= val_adc_i;  -- I  1.5 V VA     
          when add_va_rec_im => va_rec_im_i <= val_adc_i;  -- I -2.0 V        
          when add_va_sup_im => va_sup_im_i <= val_adc_i;  -- I -2.0 V VA      
          when add_gtl_u     => gtl_u_i     <= val_adc_i;  --    2.5 V GTL
          when add_t3        => t3_i        <= val_adc_i;  -- Third ADC T     
          when add_t1sens    => t1sens_i    <= val_adc_i;  -- Temp sens. 1     
          when add_t2sens    => t2sens_i    <= val_adc_i;  -- Temp sens. 2     
          when add_al_dig_u  => al_dig_u_i  <= val_adc_i;  -- U  2.5 altro dig
          when add_al_ana_u  => al_ana_u_i  <= val_adc_i;  -- U  2.5 altro ana
          when add_t4        => t4_i        <= val_adc_i;  -- Forth ADC T  
          when add_va_rec_up => va_rec_up_i <= val_adc_i;  -- U  2.5 VA (m)
          when add_va_sup_up => va_sup_up_i <= val_adc_i;  -- U  1.5 VA (m)
          when add_va_sup_um => va_sup_um_i <= val_adc_i;  -- U -2.0 VA (m)
          when add_va_rec_um => va_rec_um_i <= val_adc_i;  -- U -2.0 (m)   
          when others        => null;
        end case;
      end if;
    end if;
  end process adc_register;


  -- purpose: Register registers
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  reg_register : process (clk, rstb)
    variable add_i : integer;
  begin  -- process reg_register
    if rstb = '0' then                  -- asynchronous reset (active low)
      t1_th_i             <= regs(add_t1_th).def(9 downto 0);
      flash_i_th_i        <= regs(add_flash_i_th).def(9 downto 0);
      al_dig_i_th_i       <= regs(add_al_dig_i_th).def(9 downto 0);
      al_ana_i_th_i       <= regs(add_al_ana_i_th).def(9 downto 0);
      va_rec_ip_th_i      <= regs(add_va_rec_ip_th).def(9 downto 0);
      t2_th_i             <= regs(add_t2_th).def(9 downto 0);
      va_sup_ip_th_i      <= regs(add_va_sup_ip_th).def(9 downto 0);
      va_rec_im_th_i      <= regs(add_va_rec_im_th).def(9 downto 0);
      va_sup_im_th_i      <= regs(add_va_sup_im_th).def(9 downto 0);
      gtl_u_th_i          <= regs(add_gtl_u_th).def(9 downto 0);
      t3_th_i             <= regs(add_t3_th).def(9 downto 0);
      t1sens_th_i         <= regs(add_t1sens_th).def(9 downto 0);
      t2sens_th_i         <= regs(add_t2sens_th).def(9 downto 0);
      al_dig_u_th_i       <= regs(add_al_dig_u_th).def(9 downto 0);
      al_ana_u_th_i       <= regs(add_al_ana_u_th).def(9 downto 0);
      t4_th_i             <= regs(add_t4_th).def(9 downto 0);
      va_rec_up_th_i      <= regs(add_va_rec_up_th).def(9 downto 0);
      va_sup_up_th_i      <= regs(add_va_sup_up_th).def(9 downto 0);
      va_sup_um_th_i      <= regs(add_va_sup_um_th).def(9 downto 0);
      va_rec_um_th_i      <= regs(add_va_rec_um_th).def(9 downto 0);
      tsm_word_i          <= regs(add_tsm_word).def(8 downto 0);
      us_ratio_i          <= regs(add_us_ratio).def;
      csr0_i              <= regs(add_csr0).def(10 downto 0);
      csr1_i(11 downto 0) <= regs(add_csr1).def(11 downto 0);
      csr2_i              <= regs(add_csr2).def;
      csr3_i(14 downto 0) <= regs(add_csr3).def(14 downto 0);
      hold_wait_i         <= regs(add_hold_wait).def;
      l1_timeout_i        <= regs(add_l1_timeout).def;
      l2_timeout_i        <= regs(add_l2_timeout).def;
      shift_div_i         <= regs(add_shift_div).def;
      strips_i            <= regs(add_strips).def;
      cal_level_i         <= regs(add_cal_level).def;
      cal_iter_i          <= regs(add_cal_iter).def;
      shape_bias_i        <= regs(add_shape_bias).def;
      vfs_i               <= regs(add_vfs).def;
      vfp_i               <= regs(add_vfp).def;
      sample_div_i        <= regs(add_sample_div).def;
      fmdd_cmd_i          <= regs(add_fmdd_cmd).def;
      mebs_i              <= regs(add_mebs).def(8 downto 4);
      cal_delay_i         <= regs(add_cal_delay).def;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' then
        t1_th_i             <= regs(add_t1_th).def(9 downto 0);
        flash_i_th_i        <= regs(add_flash_i_th).def(9 downto 0);
        al_dig_i_th_i       <= regs(add_al_dig_i_th).def(9 downto 0);
        al_ana_i_th_i       <= regs(add_al_ana_i_th).def(9 downto 0);
        va_rec_ip_th_i      <= regs(add_va_rec_ip_th).def(9 downto 0);
        t2_th_i             <= regs(add_t2_th).def(9 downto 0);
        va_sup_ip_th_i      <= regs(add_va_sup_ip_th).def(9 downto 0);
        va_rec_im_th_i      <= regs(add_va_rec_im_th).def(9 downto 0);
        va_sup_im_th_i      <= regs(add_va_sup_im_th).def(9 downto 0);
        gtl_u_th_i          <= regs(add_gtl_u_th).def(9 downto 0);
        t3_th_i             <= regs(add_t3_th).def(9 downto 0);
        t1sens_th_i         <= regs(add_t1sens_th).def(9 downto 0);
        t2sens_th_i         <= regs(add_t2sens_th).def(9 downto 0);
        al_dig_u_th_i       <= regs(add_al_dig_u_th).def(9 downto 0);
        al_ana_u_th_i       <= regs(add_al_ana_u_th).def(9 downto 0);
        t4_th_i             <= regs(add_t4_th).def(9 downto 0);
        va_rec_up_th_i      <= regs(add_va_rec_up_th).def(9 downto 0);
        va_sup_up_th_i      <= regs(add_va_sup_up_th).def(9 downto 0);
        va_sup_um_th_i      <= regs(add_va_sup_um_th).def(9 downto 0);
        va_rec_um_th_i      <= regs(add_va_rec_um_th).def(9 downto 0);
        tsm_word_i          <= regs(add_tsm_word).def(8 downto 0);
        us_ratio_i          <= regs(add_us_ratio).def;
        csr0_i              <= regs(add_csr0).def(10 downto 0);
        csr1_i(11 downto 0) <= regs(add_csr1).def(11 downto 0);
        csr2_i              <= regs(add_csr2).def;
        csr3_i(14 downto 0) <= regs(add_csr3).def(14 downto 0);
        hold_wait_i         <= regs(add_hold_wait).def;
        l1_timeout_i        <= regs(add_l1_timeout).def;
        l2_timeout_i        <= regs(add_l2_timeout).def;
        shift_div_i         <= regs(add_shift_div).def;
        strips_i            <= regs(add_strips).def;
        cal_level_i         <= regs(add_cal_level).def;
        cal_iter_i          <= regs(add_cal_iter).def;
        shape_bias_i        <= regs(add_shape_bias).def;
        vfs_i               <= regs(add_vfs).def;
        vfp_i               <= regs(add_vfp).def;
        sample_div_i        <= regs(add_sample_div).def;
        fmdd_cmd_i          <= regs(add_fmdd_cmd).def;
        mebs_i              <= regs(add_mebs).def(8 downto 4);
        cal_delay_i         <= regs(add_cal_delay).def;
      elsif csr1_clr = '1' then
        csr1_i(11 downto 0) <= regs(add_csr1).def(11 downto 0);
      elsif we = '1' then
        add_i := to_integer(unsigned(add));

        case add_i is
          when add_t1_th        => t1_th_i             <= din(9 downto 0);
          when add_flash_i_th   => flash_i_th_i        <= din(9 downto 0);
          when add_al_dig_i_th  => al_dig_i_th_i       <= din(9 downto 0);
          when add_al_ana_i_th  => al_ana_i_th_i       <= din(9 downto 0);
          when add_va_rec_ip_th => va_rec_ip_th_i      <= din(9 downto 0);
          when add_t2_th        => t2_th_i             <= din(9 downto 0);
          when add_va_sup_ip_th => va_sup_ip_th_i      <= din(9 downto 0);
          when add_va_rec_im_th => va_rec_im_th_i      <= din(9 downto 0);
          when add_va_sup_im_th => va_sup_im_th_i      <= din(9 downto 0);
          when add_gtl_u_th     => gtl_u_th_i          <= din(9 downto 0);
          when add_t3_th        => t3_th_i             <= din(9 downto 0);
          when add_t1sens_th    => t1sens_th_i         <= din(9 downto 0);
          when add_t2sens_th    => t2sens_th_i         <= din(9 downto 0);
          when add_al_dig_u_th  => al_dig_u_th_i       <= din(9 downto 0);
          when add_al_ana_u_th  => al_ana_u_th_i       <= din(9 downto 0);
          when add_t4_th        => t4_th_i             <= din(9 downto 0);
          when add_va_rec_up_th => va_rec_up_th_i      <= din(9 downto 0);
          when add_va_sup_up_th => va_sup_up_th_i      <= din(9 downto 0);
          when add_va_sup_um_th => va_sup_um_th_i      <= din(9 downto 0);
          when add_va_rec_um_th => va_rec_um_th_i      <= din(9 downto 0);
          when add_tsm_word     => tsm_word_i          <= din(8 downto 0);
          when add_us_ratio     => us_ratio_i          <= din(15 downto 0);
          when add_csr0         => csr0_i              <= din(10 downto 0);
          when add_csr1         => csr1_i(11 downto 0) <= din(11 downto 0);
          when add_csr2         => csr2_i(10 downto 0) <= din(10 downto 0);
          when add_csr3         => csr3_i(14 downto 0) <= din(14 downto 0);
          when add_hold_wait    => hold_wait_i         <= din(15 downto 0);
          when add_l1_timeout   => l1_timeout_i        <= din(15 downto 0);
          when add_l2_timeout   => l2_timeout_i        <= din(15 downto 0);
          when add_shift_div    => shift_div_i         <= din(15 downto 0);
          when add_strips       => strips_i            <= din(15 downto 0);
          when add_cal_level    => cal_level_i         <= din(15 downto 0);
          when add_cal_iter     => cal_iter_i          <= din(15 downto 0);
          when add_shape_bias   => shape_bias_i        <= din(15 downto 0);
          when add_vfs          => vfs_i               <= din(15 downto 0);
          when add_vfp          => vfp_i               <= din(15 downto 0);
          when add_sample_div   => sample_div_i        <= din(15 downto 0);
          when add_fmdd_cmd     => fmdd_cmd_i          <= din(15 downto 0);
          when add_mebs         => mebs_i              <= din(8 downto 4);
          when add_cal_delay    => cal_delay_i         <= din(15 downto 0);
          when others           => null;
        end case;
      else
        -- Clear after one clock cycle 
        fmdd_cmd_i <= regs(add_fmdd_cmd).def;
      end if;
    end if;
  end process reg_register;

  -- purpose: Output data to ALTRO bus
  -- type   : combinational
  -- inputs : add_al
  -- outputs: dout_al
  output_al : process (clk)                -- (add)
    variable add_al_i : integer range 0 to 2**7-1;
  begin  -- process output
    if rstb = '0' then
      add_al_i := 0;
    else
      add_al_i := to_integer(unsigned(add_al));
    end if;

    dout_al <= (others => '0');
    case add_al_i is
      when add_free         => dout_al              <= version;
      when add_t1_th        => dout_al(9 downto 0)  <= t1_th_r;
      when add_flash_i_th   => dout_al(9 downto 0)  <= flash_i_th_r;
      when add_al_dig_i_th  => dout_al(9 downto 0)  <= al_dig_i_th_r;
      when add_al_ana_i_th  => dout_al(9 downto 0)  <= al_ana_i_th_r;
      when add_va_rec_ip_th => dout_al(9 downto 0)  <= va_rec_ip_th_r;
      when add_t1           => dout_al(9 downto 0)  <= t1_r;
      when add_flash_i      => dout_al(9 downto 0)  <= flash_i_r;
      when add_al_dig_i     => dout_al(9 downto 0)  <= al_dig_i_r;
      when add_al_ana_i     => dout_al(9 downto 0)  <= al_ana_i_r;
      when add_va_rec_ip    => dout_al(9 downto 0)  <= va_rec_ip_r;
      when add_t2_th        => dout_al(9 downto 0)  <= t2_th_r;
      when add_va_sup_ip_th => dout_al(9 downto 0)  <= va_sup_ip_th_r;
      when add_va_rec_im_th => dout_al(9 downto 0)  <= va_rec_im_th_r;
      when add_va_sup_im_th => dout_al(9 downto 0)  <= va_sup_im_th_r;
      when add_gtl_u_th     => dout_al(9 downto 0)  <= gtl_u_th_r;
      when add_t2           => dout_al(9 downto 0)  <= t2_r;
      when add_va_sup_ip    => dout_al(9 downto 0)  <= va_sup_ip_r;
      when add_va_rec_im    => dout_al(9 downto 0)  <= va_rec_im_r;
      when add_va_sup_im    => dout_al(9 downto 0)  <= va_sup_im_r;
      when add_gtl_u        => dout_al(9 downto 0)  <= gtl_u_r;
      when add_t3_th        => dout_al(9 downto 0)  <= t3_th_r;
      when add_t1sens_th    => dout_al(9 downto 0)  <= t1sens_th_r;
      when add_t2sens_th    => dout_al(9 downto 0)  <= t2sens_th_r;
      when add_al_dig_u_th  => dout_al(9 downto 0)  <= al_dig_u_th_r;
      when add_al_ana_u_th  => dout_al(9 downto 0)  <= al_ana_u_th_r;
      when add_t3           => dout_al(9 downto 0)  <= t3_r;
      when add_t1sens       => dout_al(9 downto 0)  <= t1sens_r;
      when add_t2sens       => dout_al(9 downto 0)  <= t2sens_r;
      when add_al_dig_u     => dout_al(9 downto 0)  <= al_dig_u_r;
      when add_al_ana_u     => dout_al(9 downto 0)  <= al_ana_u_r;
      when add_t4_th        => dout_al(9 downto 0)  <= t4_th_r;
      when add_va_rec_up_th => dout_al(9 downto 0)  <= va_rec_up_th_r;
      when add_va_sup_up_th => dout_al(9 downto 0)  <= va_sup_up_th_r;
      when add_va_sup_um_th => dout_al(9 downto 0)  <= va_sup_um_th_r;
      when add_va_rec_um_th => dout_al(9 downto 0)  <= va_rec_um_th_r;
      when add_t4           => dout_al(9 downto 0)  <= t4_r;
      when add_va_rec_up    => dout_al(9 downto 0)  <= va_rec_up_r;
      when add_va_sup_up    => dout_al(9 downto 0)  <= va_sup_up_r;
      when add_va_sup_um    => dout_al(9 downto 0)  <= va_sup_um_r;
      when add_va_rec_um    => dout_al(9 downto 0)  <= va_rec_um_r;
      when add_tsm_word     => dout_al(8 downto 0)  <= tsm_word_r;
      when add_us_ratio     => dout_al              <= us_ratio_r;
      when add_l1cnt        => dout_al              <= l1cnt_r;
      when add_l2cnt        => dout_al              <= l2cnt_r;
      when add_sclkcnt      => dout_al              <= sclkcnt_r;
      when add_dstbcnt      => dout_al(7 downto 0)  <= dstbcnt_in;
      when add_csr0         => dout_al(10 downto 0) <= csr0_r;
      when add_csr1         => dout_al(13 downto 0) <= csr1_r;
      when add_csr2         => dout_al              <= csr2_r;
      when add_csr3         => dout_al              <= csr3_r;
      when add_fmdd_stat    => dout_al              <= fmdd_stat_r;
      when add_l0cnt        => dout_al              <= l0cnt_r;
      when add_hold_wait    => dout_al              <= hold_wait_r;
      when add_l1_timeout   => dout_al              <= l1_timeout_r;
      when add_l2_timeout   => dout_al              <= l2_timeout_r;
      when add_shift_div    => dout_al              <= shift_div_r;
      when add_strips       => dout_al              <= strips_r;
      when add_cal_level    => dout_al              <= cal_level_r;
      when add_cal_iter     => dout_al              <= cal_iter_r;
      when add_shape_bias   => dout_al              <= shape_bias_r;
      when add_vfs          => dout_al              <= vfs_r;
      when add_vfp          => dout_al              <= vfp_r;
      when add_sample_div   => dout_al              <= sample_div_r;
      when add_mebs         => dout_al(8 downto 0)  <= mebs_r;
      when add_cal_delay    => dout_al(15 downto 0) <= cal_delay_r;
      when others           => dout_al              <= (others => '0');
    end case;  -- AL select
  end process output_al;

  -- purpose: Output data to I2C bus
  -- type   : combinational
  -- inputs : add_sc
  -- outputs: dout_sc
  output_sc : process (clk)                -- (add)
    variable add_sc_i : integer range 0 to 2**7-1;
  begin  -- process output
    if rstb = '0' then
      add_sc_i := 0;
    else
      add_sc_i := to_integer(unsigned(add_sc));
    end if;

    dout_sc <= (others => '0');
    case add_sc_i is
      when add_free         => dout_sc              <= version;
      when add_t1_th        => dout_sc(9 downto 0)  <= t1_th_r;
      when add_flash_i_th   => dout_sc(9 downto 0)  <= flash_i_th_r;
      when add_al_dig_i_th  => dout_sc(9 downto 0)  <= al_dig_i_th_r;
      when add_al_ana_i_th  => dout_sc(9 downto 0)  <= al_ana_i_th_r;
      when add_va_rec_ip_th => dout_sc(9 downto 0)  <= va_rec_ip_th_r;
      when add_t1           => dout_sc(9 downto 0)  <= t1_r;
      when add_flash_i      => dout_sc(9 downto 0)  <= flash_i_r;
      when add_al_dig_i     => dout_sc(9 downto 0)  <= al_dig_i_r;
      when add_al_ana_i     => dout_sc(9 downto 0)  <= al_ana_i_r;
      when add_va_rec_ip    => dout_sc(9 downto 0)  <= va_rec_ip_r;
      when add_t2_th        => dout_sc(9 downto 0)  <= t2_th_r;
      when add_va_sup_ip_th => dout_sc(9 downto 0)  <= va_sup_ip_th_r;
      when add_va_rec_im_th => dout_sc(9 downto 0)  <= va_rec_im_th_r;
      when add_va_sup_im_th => dout_sc(9 downto 0)  <= va_sup_im_th_r;
      when add_gtl_u_th     => dout_sc(9 downto 0)  <= gtl_u_th_r;
      when add_t2           => dout_sc(9 downto 0)  <= t2_r;
      when add_va_sup_ip    => dout_sc(9 downto 0)  <= va_sup_ip_r;
      when add_va_rec_im    => dout_sc(9 downto 0)  <= va_rec_im_r;
      when add_va_sup_im    => dout_sc(9 downto 0)  <= va_sup_im_r;
      when add_gtl_u        => dout_sc(9 downto 0)  <= gtl_u_r;
      when add_t3_th        => dout_sc(9 downto 0)  <= t3_th_r;
      when add_t1sens_th    => dout_sc(9 downto 0)  <= t1sens_th_r;
      when add_t2sens_th    => dout_sc(9 downto 0)  <= t2sens_th_r;
      when add_al_dig_u_th  => dout_sc(9 downto 0)  <= al_dig_u_th_r;
      when add_al_ana_u_th  => dout_sc(9 downto 0)  <= al_ana_u_th_r;
      when add_t3           => dout_sc(9 downto 0)  <= t3_r;
      when add_t1sens       => dout_sc(9 downto 0)  <= t1sens_r;
      when add_t2sens       => dout_sc(9 downto 0)  <= t2sens_r;
      when add_al_dig_u     => dout_sc(9 downto 0)  <= al_dig_u_r;
      when add_al_ana_u     => dout_sc(9 downto 0)  <= al_ana_u_r;
      when add_t4_th        => dout_sc(9 downto 0)  <= t4_th_r;
      when add_va_rec_up_th => dout_sc(9 downto 0)  <= va_rec_up_th_r;
      when add_va_sup_up_th => dout_sc(9 downto 0)  <= va_sup_up_th_r;
      when add_va_sup_um_th => dout_sc(9 downto 0)  <= va_sup_um_th_r;
      when add_va_rec_um_th => dout_sc(9 downto 0)  <= va_rec_um_th_r;
      when add_t4           => dout_sc(9 downto 0)  <= t4_r;
      when add_va_rec_up    => dout_sc(9 downto 0)  <= va_rec_up_r;
      when add_va_sup_up    => dout_sc(9 downto 0)  <= va_sup_up_r;
      when add_va_sup_um    => dout_sc(9 downto 0)  <= va_sup_um_r;
      when add_va_rec_um    => dout_sc(9 downto 0)  <= va_rec_um_r;
      -- when add_tsm_word     => dout_sc(8 downto 0)  <= tsm_word_r;
      -- when add_us_ratio     => dout_sc              <= us_ratio_r;
      when add_l1cnt        => dout_sc              <= l1cnt_r;
      when add_l2cnt        => dout_sc              <= l2cnt_r;
      -- when add_sclkcnt      => dout_sc              <= sclkcnt_r;
      -- when add_dstbcnt      => dout_sc(7 downto 0)  <= dstbcnt_in;
      when add_csr0         => dout_sc(10 downto 0) <= csr0_r;
      when add_csr1         => dout_sc(13 downto 0) <= csr1_r;
      when add_csr2         => dout_sc              <= csr2_r;
      when add_csr3         => dout_sc              <= csr3_r;
      -- when add_fmdd_stat    => dout_sc              <= fmdd_stat_r;
      when add_l0cnt        => dout_sc              <= l0cnt_r;
      -- when add_hold_wait    => dout_sc              <= hold_wait_r;
      -- when add_l1_timeout   => dout_sc              <= l1_timeout_r;
      -- when add_l2_timeout   => dout_sc              <= l2_timeout_r;
      -- when add_shift_div    => dout_sc              <= shift_div_r;
      -- when add_strips       => dout_sc              <= strips_r;
      -- when add_cal_level    => dout_sc              <= cal_level_r;
      -- when add_cal_iter     => dout_sc              <= cal_iter_r;
      -- when add_shape_bias   => dout_sc              <= shape_bias_r;
      -- when add_vfs          => dout_sc              <= vfs_r;
      -- when add_vfp          => dout_sc              <= vfp_r;
      -- when add_sample_div   => dout_sc              <= sample_div_r;
      -- when add_mebs         => dout_sc(8 downto 0)  <= mebs_r;
      -- when add_cal_delay    => dout_sc(15 downto 0) <= cal_delay_r;
      when others           => dout_sc              <= (others => '0');
    end case;  -- I2C select
  end process output_sc;

  -- purpose: Deal with commands type : sequential inputs : clk, rstb
  -- outputs:
  ans_cmds : process (clk, rstb)
  begin  -- process ans_cmds
    if rstb = '0' then                  -- asynchronous reset (active low)
      l0cnt_i   <= regs(add_l0cnt).def;
      l1cnt_i   <= regs(add_l1cnt).def;
      l2cnt_i   <= regs(add_l2cnt).def;
      sclkcnt_i <= regs(add_sclkcnt).def;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' or cnt_clr = '1' then
        l0cnt_i   <= regs(add_l0cnt).def;
        l1cnt_i   <= regs(add_l1cnt).def;
        l2cnt_i   <= regs(add_l2cnt).def;
        sclkcnt_i <= regs(add_sclkcnt).def;
      elsif cnt_lat = '1' then
        l0cnt_i   <= l0cnt_in;
        l1cnt_i   <= l1cnt_in;
        l2cnt_i   <= l2cnt_in;
        sclkcnt_i <= sclkcnt_in;
      end if;
    end if;
  end process ans_cmds;

  -- purpose: Process errors, etc.
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  errors : process (clk, rstb)
  begin  -- process errors
    if rstb = '0' then                  -- asynchronous reset (active low)
      csr1_ii    <= regs(add_csr1).def(csr1_ii'length-1 downto 0);
      csr3_i(15) <= '0';
      meb_cnt_i  <= regs(add_mebs).def(3 downto 0);
    elsif clk'event and clk = '1' then  -- rising clock edge
      meb_cnt_i <= meb_cnt;

      if bc_rst = '1' then
        csr3_i(15) <= '0';
      elsif st_cnv = '1' or cnv_mode_i = '1' then
        csr3_i(15) <= '0';
      elsif end_seq = '1' then
        csr3_i(15) <= '1';
      end if;

      if csr1_clrrst_i = '1' then
        csr1_ii <= regs(add_csr1).def(csr1_ii'length-1 downto 0);
      else
        -- Reset to zero.  The real setting depends on the constant AUTO_CLEAR
        csr1_ii(13 downto 0) <= (others => '0');

        -- Temperatures - bit 0 (T - hard)
        if ((ints_i(0) = '1') or        -- T1
            (ints_i(1) = '1') or        -- T2
            (ints_i(2) = '1') or        -- T3
            (ints_i(3) = '1')) then     -- T4
          csr1_ii(0) <= '1';
        end if;

        -- Temperature sensors - bit 1 (AV - soft)
        if ((ints_i(13) = '1') or       -- VA_SUP_UM
            (ints_i(14) = '1')          -- VA_REC_UM
            ) then     
          csr1_ii(1) <= '1';
        end if;

        -- Negative supplies - bit 2 (AC - hard)
        if ((ints_i(4) = '1') or        -- T1SENS
            (ints_i(5) = '1') or        -- T2SENS
            (ints_i(8) = '1') or          -- VA_REC_IM
            (ints_i(9) = '1')           -- VA_SUP_IM
            ) then
          csr1_ii(2) <= '1';
        end if;

        -- Analog voltages and currents - bit 3 (DV - soft)
        if ((ints_i(15) = '1') or       -- AL_ANA_U
            (ints_i(16) = '1') or       -- VA_SUP_UP
            (ints_i(19) = '1') or         -- VA_REC_UP
            (ints_i(17) = '1') or       -- GTL_U
            (ints_i(18) = '1')          -- AL_DIG_U
            ) then
          csr1_ii(3) <= '1';
        end if;

        -- Digital volatages and currents - bit 4 (DC - hard)
        if ((ints_i(6)  = '1') or        -- AL_ANA_I
            (ints_i(10) = '1') or       -- AL_DIG_I
            (ints_i(11) = '1') or       -- FLASH_I
            (ints_i(7)  = '1') or        -- VA_SUP_IP
            (ints_i(12) = '1')          -- VA_REC_IP
            ) then
          csr1_ii(4) <= '1';
        end if;

        -- We clear above
        -- csr1_ii(13 downto 5) <= (others => '0');
        if paps_error  = '1' then csr1_ii(5)   <= '1'; end if;  -- Hard
        if alps_error  = '1' then csr1_ii(6)   <= '1'; end if;  -- Hard
        if missed_sclk = '1' then csr1_ii(7)   <= '1'; end if;  -- Soft
        if par_error   = '1' then csr1_ii(8)   <= '1'; end if;  -- Nothing
        if ierr_al     = '1' then csr1_ii(9)   <= '1'; end if;  -- Nothing
        if al_error    = '1' then csr1_ii(10)  <= '1'; end if;  -- Nothing
        if ierr_sc     = '1' then csr1_ii(11)  <= '1'; end if;  -- Nothing 
        if bc_error_i  = '1' then csr1_ii(12)  <= '1'; end if;  -- Nothing
        if bc_int_i    = '1' then csr1_ii(13)  <= '1'; end if;  -- Interrupt
      end if;  -- bc_rst   = '0'
    end if;  -- if clk'event
  end process errors;


  -- purpose: Collect combinatorics
  combi : block
  begin  -- block combi
    t1_th_r              <= t1_th_i;
    flash_i_th_r         <= flash_i_th_i;
    al_dig_i_th_r        <= al_dig_i_th_i;
    al_ana_i_th_r        <= al_ana_i_th_i;
    va_rec_ip_th_r       <= va_rec_ip_th_i;
    t1_r                 <= t1_i;
    flash_i_r            <= flash_i_i;
    al_dig_i_r           <= al_dig_i_i;
    al_ana_i_r           <= al_ana_i_i;
    va_rec_ip_r          <= va_rec_ip_i;
    t2_th_r              <= t2_th_i;
    va_sup_ip_th_r       <= va_sup_ip_th_i;
    va_rec_im_th_r       <= va_rec_im_th_i;
    va_sup_im_th_r       <= va_sup_im_th_i;
    gtl_u_th_r           <= gtl_u_th_i;
    t2_r                 <= t2_i;
    va_sup_ip_r          <= va_sup_ip_i;
    va_rec_im_r          <= va_rec_im_i;
    va_sup_im_r          <= va_sup_im_i;
    gtl_u_r              <= gtl_u_i;
    t3_th_r              <= t3_th_i;
    t1sens_th_r          <= t1sens_th_i;
    t2sens_th_r          <= t2sens_th_i;
    al_dig_u_th_r        <= al_dig_u_th_i;
    al_ana_u_th_r        <= al_ana_u_th_i;
    t3_r                 <= t3_i;
    t1sens_r             <= t1sens_i;
    t2sens_r             <= t2sens_i;
    al_dig_u_r           <= al_dig_u_i;
    al_ana_u_r           <= al_ana_u_i;
    t4_th_r              <= t4_th_i;
    va_rec_up_th_r       <= va_rec_up_th_i;
    va_sup_up_th_r       <= va_sup_up_th_i;
    va_sup_um_th_r       <= va_sup_um_th_i;
    va_rec_um_th_r       <= va_rec_um_th_i;
    t4_r                 <= t4_i;
    va_rec_up_r          <= va_rec_up_i;
    va_sup_up_r          <= va_sup_up_i;
    va_sup_um_r          <= va_sup_um_i;
    va_rec_um_r          <= va_rec_um_i;
    l1cnt_r              <= l1cnt_i;
    l2cnt_r              <= l2cnt_i;
    sclkcnt_r            <= sclkcnt_i;
    dstbcnt_r            <= dstbcnt_in;
    tsm_word_r           <= tsm_word_i;
    us_ratio_r           <= us_ratio_i;
    csr0_r               <= csr0_i;
    csr1_r(13 downto 12) <= csr1_ii(13 downto 12);
    csr1_i(13 downto 12) <= csr1_ii(13 downto 12);
    csr1_r(11 downto 0)  <= csr1_i(11 downto 0) or csr1_ii(11 downto 0);
    csr2_i(15 downto 11) <= hadd;
    csr2_r(15 downto 11) <= hadd;
    csr2_r(10 downto 0)  <= csr2_i(10 downto 0);
    csr3_r               <= csr3_i;
    fmdd_stat_r          <= fmdd_stat;     -- FMDD status
    l0cnt_r              <= l0cnt_i;       -- L0 counters
    hold_wait_r          <= hold_wait_i;   -- FMD: Wait to hold
    l1_timeout_r         <= l1_timeout_i;  -- FMD: L1 timeout
    l2_timeout_r         <= l2_timeout_i;  -- FMD: L2 timeout
    shift_div_r          <= shift_div_i;   -- FMD: Shift clk
    strips_r             <= strips_i;      -- FMD: Strips
    cal_level_r          <= cal_level_i;   -- FMD: Cal pulse
    cal_iter_r           <= cal_iter_i;    -- FMD: Cal events
    shape_bias_r         <= shape_bias_i;  -- FMD: Shape bias
    vfs_r                <= vfs_i;         -- FMD: Shape ref
    vfp_r                <= vfp_i;         -- FMD: Preamp ref
    sample_div_r         <= sample_div_i;  -- FMD: Sample clk
    fmdd_cmd_r           <= fmdd_cmd_i;    -- FMD: Commands
    mebs_r               <= mebs_i & meb_cnt_i;
    cal_delay_r          <= cal_delay_i;

    -- Interrupt and error
    bc_int_i <= '1' when
                ((csr1_r(7 downto 0) and csr0_r(7 downto 0)) /= X"00")
                else '0';
    bc_error_i <= '1' when
                  ((csr1_r(9 downto 8) and csr0_r(9 downto 8)) /= "00")
                  else '0';
    cnv_mode_i <= csr0_r(10);

    -- output
    bc_int     <= bc_int_i;
    bc_error   <= bc_error_i;
    cnv_mode   <= cnv_mode_i;
    csr2       <= csr2_r;
    csr3       <= csr3_r;
    tsm_word   <= tsm_word_r;
    us_ratio   <= us_ratio_r;
    -- Valid for 2 clock cycles
    fmdd_cmd   <= fmdd_cmd_r or fmdd_cmd_i;
    hold_wait  <= hold_wait_r;
    l1_timeout <= l1_timeout_r;
    l2_timeout <= l2_timeout_r;
    shift_div  <= shift_div_r;
    strips     <= strips_r;
    cal_level  <= cal_level_r;
    cal_iter   <= cal_iter_r;
    shape_bias <= shape_bias_r;
    vfs        <= vfs_r;
    vfp        <= vfp_r;
    sample_div <= sample_div_r;
    -- mebs       <= mebs_r(8 downto 4);
    mebs       <= mebs_i;
    cal_delay  <= cal_delay_r;

  end block combi;
end rtl3;
------------------------------------------------------------------------------
architecture rtl2 of registers_block is
  -- Cached values 
  signal t1_th_i        : std_logic_vector(9 downto 0);  --! First ADC T
  signal flash_i_th_i   : std_logic_vector(9 downto 0);  --! I  3.3 V
  signal al_dig_i_th_i  : std_logic_vector(9 downto 0);  --! I  2.5 V altro dig
  signal al_ana_i_th_i  : std_logic_vector(9 downto 0);  --! I  2.5 V altro ana
  signal va_rec_ip_th_i : std_logic_vector(9 downto 0);  --! I  2.5 V VA
  signal t1_i           : std_logic_vector(9 downto 0);  --! First ADC T
  signal flash_i_i      : std_logic_vector(9 downto 0);  --! I  3.3 V
  signal al_dig_i_i     : std_logic_vector(9 downto 0);  --! I  2.5 V altro dig
  signal al_ana_i_i     : std_logic_vector(9 downto 0);  --! I  2.5 V altro ana
  signal va_rec_ip_i    : std_logic_vector(9 downto 0);  --! I  2.5 V VA
  signal t2_th_i        : std_logic_vector(9 downto 0);  --! Second ADC T
  signal va_sup_ip_th_i : std_logic_vector(9 downto 0);  --! I  1.5 V VA
  signal va_rec_im_th_i : std_logic_vector(9 downto 0);  --! I -2.0 V
  signal va_sup_im_th_i : std_logic_vector(9 downto 0);  --! I -2.0 V VA
  signal gtl_u_th_i     : std_logic_vector(9 downto 0);  --! U  2.5 V Digital
  signal t2_i           : std_logic_vector(9 downto 0);  --! Second ADC T
  signal va_sup_ip_i    : std_logic_vector(9 downto 0);  --! I  1.5 V VA
  signal va_rec_im_i    : std_logic_vector(9 downto 0);  --! I -2.0 V
  signal va_sup_im_i    : std_logic_vector(9 downto 0);  --! I -2.0 V VA
  signal gtl_u_i        : std_logic_vector(9 downto 0);  --! U  2.5 V Digital 
  signal t3_th_i        : std_logic_vector(9 downto 0);  --! Third ADC T
  signal t1sens_th_i    : std_logic_vector(9 downto 0);  --! Temperature sens. 1
  signal t2sens_th_i    : std_logic_vector(9 downto 0);  --! Temperature sens. 2
  signal al_dig_u_th_i  : std_logic_vector(9 downto 0);  --! U  2.5 altro dig
  signal al_ana_u_th_i  : std_logic_vector(9 downto 0);  --! U  2.5 altro ana
  signal t3_i           : std_logic_vector(9 downto 0);  --! Third ADC T
  signal t1sens_i       : std_logic_vector(9 downto 0);  --! Temperature sens. 1
  signal t2sens_i       : std_logic_vector(9 downto 0);  --! Temperature sens. 2
  signal al_dig_u_i     : std_logic_vector(9 downto 0);  --! U  2.5 altro dig
  signal al_ana_u_i     : std_logic_vector(9 downto 0);  --! U  2.5 altro ana
  signal t4_th_i        : std_logic_vector(9 downto 0);  --! Forth ADC T
  signal va_rec_up_th_i : std_logic_vector(9 downto 0);  --! U  2.5 VA (m)
  signal va_sup_up_th_i : std_logic_vector(9 downto 0);  --! U  1.5 VA (m)
  signal va_sup_um_th_i : std_logic_vector(9 downto 0);  --! U -2.0 VA (m)
  signal va_rec_um_th_i : std_logic_vector(9 downto 0);  --! U -2.0 (m)
  signal t4_i           : std_logic_vector(9 downto 0);  --! Forth ADC T
  signal va_rec_up_i    : std_logic_vector(9 downto 0);  --! U  2.5 VA (m)
  signal va_sup_up_i    : std_logic_vector(9 downto 0);  --! U  1.5 VA (m)
  signal va_sup_um_i    : std_logic_vector(9 downto 0);  --! U -2.0 VA (m)
  signal va_rec_um_i    : std_logic_vector(9 downto 0);  --! U -2.0 (m)
  signal l1cnt_i        : std_logic_vector(15 downto 0);  --! L1 counter
  signal l2cnt_i        : std_logic_vector(15 downto 0);  --! L2 counter
  signal sclkcnt_i      : std_logic_vector(15 downto 0);  --! Slow clock counter
  -- signal dstbcnt_i   : std_logic_vector(7 downto 0);  --! Data strobe counter
  signal tsm_word_i     : std_logic_vector(8 downto 0);  --! Test mode words
  signal us_ratio_i     : std_logic_vector(15 downto 0) := (others => '0');
  signal csr0_i         : std_logic_vector(10 downto 0) := (others => '0');
  signal csr1_i         : std_logic_vector(13 downto 0);  --! Config/Status 1
  signal csr2_i         : std_logic_vector(15 downto 0);  --! Config/Status 2
  signal csr3_i         : std_logic_vector(15 downto 0);  --! Config/Status 3
  signal l0cnt_i        : std_logic_vector(15 downto 0);  --! L0 counters
  signal hold_wait_i    : std_logic_vector(15 downto 0);  --! FMD: Wait to hold
  signal l1_timeout_i   : std_logic_vector(15 downto 0);  --! FMD: L1 timeout
  signal l2_timeout_i   : std_logic_vector(15 downto 0);  --! FMD: L2 timeout
  signal shift_div_i    : std_logic_vector(15 downto 0);  --! FMD: Shift clk
  signal strips_i       : std_logic_vector(15 downto 0);  --! FMD: Strips
  signal cal_level_i    : std_logic_vector(15 downto 0);  --! FMD: Cal pulse
  signal cal_iter_i     : std_logic_vector(15 downto 0);  --! FMD: Cal events
  signal shape_bias_i   : std_logic_vector(15 downto 0);  --! FMD: Shape bias
  signal vfs_i          : std_logic_vector(15 downto 0);  --! FMD: Shape ref
  signal vfp_i          : std_logic_vector(15 downto 0);  --! FMD: Preamp ref
  signal sample_div_i   : std_logic_vector(15 downto 0);  --! FMD: Sample clk
  signal fmdd_cmd_i     : std_logic_vector(15 downto 0);  --! FMD: Commands
  signal mebs_i         : std_logic_vector(4 downto 0);  --! MEB config

  -- Register values
  signal t1_th_r        : std_logic_vector(9 downto 0);  --! First ADC T
  signal flash_i_th_r   : std_logic_vector(9 downto 0);  --! I  3.3 V
  signal al_dig_i_th_r  : std_logic_vector(9 downto 0);  --! I  2.5 V altro dig
  signal al_ana_i_th_r  : std_logic_vector(9 downto 0);  --! I  2.5 V altro ana
  signal va_rec_ip_th_r : std_logic_vector(9 downto 0);  --! I  2.5 V VA
  signal t1_r           : std_logic_vector(9 downto 0);  --! First ADC T
  signal flash_i_r      : std_logic_vector(9 downto 0);  --! I  3.3 V
  signal al_dig_i_r     : std_logic_vector(9 downto 0);  --! I  2.5 V altro dig
  signal al_ana_i_r     : std_logic_vector(9 downto 0);  --! I  2.5 V altro ana
  signal va_rec_ip_r    : std_logic_vector(9 downto 0);  --! I  2.5 V VA
  signal t2_th_r        : std_logic_vector(9 downto 0);  --! Second ADC T
  signal va_sup_ip_th_r : std_logic_vector(9 downto 0);  --! I  1.5 V VA
  signal va_rec_im_th_r : std_logic_vector(9 downto 0);  --! I -2.0 V
  signal va_sup_im_th_r : std_logic_vector(9 downto 0);  --! I -2.0 V VA
  signal gtl_u_th_r     : std_logic_vector(9 downto 0);  --! U  2.5 V Digital
  signal t2_r           : std_logic_vector(9 downto 0);  --! Second ADC T
  signal va_sup_ip_r    : std_logic_vector(9 downto 0);  --! I  1.5 V VA
  signal va_rec_im_r    : std_logic_vector(9 downto 0);  --! I -2.0 V
  signal va_sup_im_r    : std_logic_vector(9 downto 0);  --! I -2.0 V VA
  signal gtl_u_r        : std_logic_vector(9 downto 0);  --! U  2.5 V Digital
  signal t3_th_r        : std_logic_vector(9 downto 0);  --! Third ADC T
  signal t1sens_th_r    : std_logic_vector(9 downto 0);  --! Temperature sens. 1
  signal t2sens_th_r    : std_logic_vector(9 downto 0);  --! Temperature sens. 2
  signal al_dig_u_th_r  : std_logic_vector(9 downto 0);  --! U  2.5 altro dig
  signal al_ana_u_th_r  : std_logic_vector(9 downto 0);  --! U  2.5 altro analog
  signal t3_r           : std_logic_vector(9 downto 0);  --! Third ADC T
  signal t1sens_r       : std_logic_vector(9 downto 0);  --! Temperature sens. 1
  signal t2sens_r       : std_logic_vector(9 downto 0);  --! Temperature sens. 2
  signal al_dig_u_r     : std_logic_vector(9 downto 0);  --! U  2.5 altro dig
  signal al_ana_u_r     : std_logic_vector(9 downto 0);  --! U  2.5 altro analog
  signal t4_th_r        : std_logic_vector(9 downto 0);  --! Th: Forth ADC T
  signal va_rec_up_th_r : std_logic_vector(9 downto 0);  --! Th: U  2.5 VA (m)
  signal va_sup_up_th_r : std_logic_vector(9 downto 0);  --! Th: U  1.5 VA (m)
  signal va_sup_um_th_r : std_logic_vector(9 downto 0);  --! Th: U -2.0 VA (m)
  signal va_rec_um_th_r : std_logic_vector(9 downto 0);  --! Th: U -2.0 (m)
  signal t4_r           : std_logic_vector(9 downto 0);  --! Cu: Forth ADC T
  signal va_rec_up_r    : std_logic_vector(9 downto 0);  --! Cu: U  2.5 VA (m)
  signal va_sup_up_r    : std_logic_vector(9 downto 0);  --! Cu: U  1.5 VA (m)
  signal va_sup_um_r    : std_logic_vector(9 downto 0);  --! Cu: U -2.0 VA (m)
  signal va_rec_um_r    : std_logic_vector(9 downto 0);  --! Cu: U -2.0 (m)
  signal l1cnt_r        : std_logic_vector(15 downto 0);  --! L1 counter
  signal l2cnt_r        : std_logic_vector(15 downto 0);  --! L2 counter
  signal sclkcnt_r      : std_logic_vector(15 downto 0);  --! Slow clock counter
  signal dstbcnt_r      : std_logic_vector(7 downto 0);  --! Data strobe counter
  signal tsm_word_r     : std_logic_vector(8 downto 0);  --! Test mode words
  signal us_ratio_r     : std_logic_vector(15 downto 0);  --! Under sampling ra
  signal csr0_r         : std_logic_vector(10 downto 0) := (others => '0');
  signal csr1_r         : std_logic_vector(13 downto 0) := (others => '0');
  signal csr2_r         : std_logic_vector(15 downto 0);  --! Config/Status 2
  signal csr3_r         : std_logic_vector(15 downto 0);  --! Config/Status 3
  signal fmdd_stat_r    : std_logic_vector(15 downto 0);  --! FMDD status
  signal l0cnt_r        : std_logic_vector(15 downto 0);  --! L0 counters
  signal hold_wait_r    : std_logic_vector(15 downto 0);  --! FMD: Wait to hold
  signal l1_timeout_r   : std_logic_vector(15 downto 0);  --! FMD: L1 timeout
  signal l2_timeout_r   : std_logic_vector(15 downto 0);  --! FMD: L2 timeout
  signal shift_div_r    : std_logic_vector(15 downto 0);  --! FMD: Shift clk
  signal strips_r       : std_logic_vector(15 downto 0);  --! FMD: Strips
  signal cal_level_r    : std_logic_vector(15 downto 0);  --! FMD: Cal pulse
  signal cal_iter_r     : std_logic_vector(15 downto 0);  --! FMD: Cal events
  signal shape_bias_r   : std_logic_vector(15 downto 0);  --! FMD: Shape bias
  signal vfs_r          : std_logic_vector(15 downto 0);  --! FMD: Shape ref
  signal vfp_r          : std_logic_vector(15 downto 0);  --! FMD: Preamp ref
  signal sample_div_r   : std_logic_vector(15 downto 0);  --! FMD: Sample clk
  signal fmdd_cmd_r     : std_logic_vector(15 downto 0);  --! FMD: Commands
  signal mebs_r         : std_logic_vector(8 downto 0);  --! MEB config

  -- Temporaries
  signal cnv_mode_i    : std_logic;
  signal bc_error_i    : std_logic;
  signal bc_int_i      : std_logic;
  signal meb_cnt_i     : std_logic_vector(3 downto 0);
  signal csr1_ii       : std_logic_vector(13 downto 0);  --! Config/Status 1
  signal csr1_clrrst_i : std_logic;

  -- Parameters for interrupt glitch filters
  constant INT_TIMES  : integer := 3;     -- Times out of bounds before int
  constant AUTO_CLEAR : boolean := true;  -- Wether to clear interrupt when OK

  type   intarray_t is array (natural range <>) of std_logic_vector(9 downto 0);
  signal intval_i : intarray_t(19 downto 0);
  signal intthr_i : intarray_t(19 downto 0);
  signal ints_i   : std_logic_vector(19 downto 0);


  -- purpose: Get a register value
  procedure get_reg (constant add : in  integer;  -- Address
                     signal dout  : out std_logic_vector(15 downto 0)) is
  begin  -- set_reg
    dout <= (others => '0');
    case add is
      when add_free         => dout              <= version;
      when add_t1_th        => dout(9 downto 0)  <= t1_th_r;
      when add_flash_i_th   => dout(9 downto 0)  <= flash_i_th_r;
      when add_al_dig_i_th  => dout(9 downto 0)  <= al_dig_i_th_r;
      when add_al_ana_i_th  => dout(9 downto 0)  <= al_ana_i_th_r;
      when add_va_rec_ip_th => dout(9 downto 0)  <= va_rec_ip_th_r;
      when add_t1           => dout(9 downto 0)  <= t1_r;
      when add_flash_i      => dout(9 downto 0)  <= flash_i_r;
      when add_al_dig_i     => dout(9 downto 0)  <= al_dig_i_r;
      when add_al_ana_i     => dout(9 downto 0)  <= al_ana_i_r;
      when add_va_rec_ip    => dout(9 downto 0)  <= va_rec_ip_r;
      when add_t2_th        => dout(9 downto 0)  <= t2_th_r;
      when add_va_sup_ip_th => dout(9 downto 0)  <= va_sup_ip_th_r;
      when add_va_rec_im_th => dout(9 downto 0)  <= va_rec_im_th_r;
      when add_va_sup_im_th => dout(9 downto 0)  <= va_sup_im_th_r;
      when add_gtl_u_th     => dout(9 downto 0)  <= gtl_u_th_r;
      when add_t2           => dout(9 downto 0)  <= t2_r;
      when add_va_sup_ip    => dout(9 downto 0)  <= va_sup_ip_r;
      when add_va_rec_im    => dout(9 downto 0)  <= va_rec_im_r;
      when add_va_sup_im    => dout(9 downto 0)  <= va_sup_im_r;
      when add_gtl_u        => dout(9 downto 0)  <= gtl_u_r;
      when add_t3_th        => dout(9 downto 0)  <= t3_th_r;
      when add_t1sens_th    => dout(9 downto 0)  <= t1sens_th_r;
      when add_t2sens_th    => dout(9 downto 0)  <= t2sens_th_r;
      when add_al_dig_u_th  => dout(9 downto 0)  <= al_dig_u_th_r;
      when add_al_ana_u_th  => dout(9 downto 0)  <= al_ana_u_th_r;
      when add_t3           => dout(9 downto 0)  <= t3_r;
      when add_t1sens       => dout(9 downto 0)  <= t1sens_r;
      when add_t2sens       => dout(9 downto 0)  <= t2sens_r;
      when add_al_dig_u     => dout(9 downto 0)  <= al_dig_u_r;
      when add_al_ana_u     => dout(9 downto 0)  <= al_ana_u_r;
      when add_t4_th        => dout(9 downto 0)  <= t4_th_r;
      when add_va_rec_up_th => dout(9 downto 0)  <= va_rec_up_th_r;
      when add_va_sup_up_th => dout(9 downto 0)  <= va_sup_up_th_r;
      when add_va_sup_um_th => dout(9 downto 0)  <= va_sup_um_th_r;
      when add_va_rec_um_th => dout(9 downto 0)  <= va_rec_um_th_r;
      when add_t4           => dout(9 downto 0)  <= t4_r;
      when add_va_rec_up    => dout(9 downto 0)  <= va_rec_up_r;
      when add_va_sup_up    => dout(9 downto 0)  <= va_sup_up_r;
      when add_va_sup_um    => dout(9 downto 0)  <= va_sup_um_r;
      when add_va_rec_um    => dout(9 downto 0)  <= va_rec_um_r;
      when add_tsm_word     => dout(8 downto 0)  <= tsm_word_r;
      when add_us_ratio     => dout              <= us_ratio_r;
      when add_l1cnt        => dout              <= l1cnt_r;
      when add_l2cnt        => dout              <= l2cnt_r;
      when add_sclkcnt      => dout              <= sclkcnt_r;
      when add_dstbcnt      => dout(7 downto 0)  <= dstbcnt_in;
      when add_csr0         => dout(10 downto 0) <= csr0_r;
      when add_csr1         => dout(13 downto 0) <= csr1_r;
      when add_csr2         => dout              <= csr2_r;
      when add_csr3         => dout              <= csr3_r;
      when add_fmdd_stat    => dout              <= fmdd_stat_r;
      when add_l0cnt        => dout              <= l0cnt_r;
      when add_hold_wait    => dout              <= hold_wait_r;
      when add_l1_timeout   => dout              <= l1_timeout_r;
      when add_l2_timeout   => dout              <= l2_timeout_r;
      when add_shift_div    => dout              <= shift_div_r;
      when add_strips       => dout              <= strips_r;
      when add_cal_level    => dout              <= cal_level_r;
      when add_cal_iter     => dout              <= cal_iter_r;
      when add_shape_bias   => dout              <= shape_bias_r;
      when add_vfs          => dout              <= vfs_r;
      when add_vfp          => dout              <= vfp_r;
      when add_sample_div   => dout              <= sample_div_r;
      when add_mebs         => dout(8 downto 0)  <= mebs_r;
      when others           => dout              <= (others => '0');
    end case;
  end get_reg;

begin  -- rtl
  -- purpose: Assign interrupt lines
  assign_ints : block
  begin  -- block assign_ints
    intval_i(0)   <= t1_r;                  -- Under threshold
    intval_i(1)   <= t2_r;                  -- Under threshold
    intval_i(2)   <= t3_r;                  -- Under threshold
    intval_i(3)   <= t4_r;                  -- Under threshold
    intval_i(4)   <= t1sens_r;              -- Under threshold
    intval_i(5)   <= t2sens_r;              -- Under threshold
    -- Analog currents
    intval_i(6)   <= al_ana_i_r;            -- Under threshold
    intval_i(7)   <= va_sup_ip_r;           -- Under threshold
    intval_i(8)   <= va_rec_im_r;           -- Under threshold
    intval_i(9)   <= va_sup_im_r;           -- Under threshold
    -- Digitial currents
    intval_i(10)  <= al_dig_i_r;            -- Under threshold
    intval_i(11)  <= flash_i_r;             -- Under threshold
    intval_i(12)  <= va_rec_ip_r;           -- Under threshold
    -- Analog voltages
    intval_i(13)  <= va_sup_um_r;           -- Under threshold
    intval_i(14)  <= va_rec_um_r;           -- Under threshold
    intval_i(15)  <= al_ana_u_r;            -- Over threshold
    intval_i(16)  <= va_sup_up_r;           -- Over threshold
    -- Digital voltages
    intval_i(17)  <= gtl_u_r;               -- Over threshold
    intval_i(18)  <= al_dig_u_r;            -- Over threshold
    intval_i(19)  <= va_rec_up_r;           -- Over threshold
    --
    intthr_i(0)   <= t1_th_r;               -- Under threshold
    intthr_i(1)   <= t2_th_r;               -- Under threshold
    intthr_i(2)   <= t3_th_r;               -- Under threshold
    intthr_i(3)   <= t4_th_r;               -- Under threshold
    intthr_i(4)   <= t1sens_th_r;           -- Under threshold
    intthr_i(5)   <= t2sens_th_r;           -- Under threshold
    --                                                           
    intthr_i(6)   <= al_ana_i_th_r;         -- Under threshold
    intthr_i(7)   <= va_sup_ip_th_r;        -- Under threshold
    intthr_i(8)   <= va_rec_im_th_r;        -- Under threshold
    intthr_i(9)   <= va_sup_im_th_r;        -- Under threshold
    --                                                           
    intthr_i(10)  <= al_dig_i_th_r;         -- Under threshold
    intthr_i(11)  <= flash_i_th_r;          -- Under threshold
    intthr_i(12)  <= va_rec_ip_th_r;        -- Under threshold
    --                                                           
    intthr_i(13)  <= va_sup_um_th_r;        -- Under threshold
    intthr_i(14)  <= va_rec_um_th_r;        -- Under threshold
    intthr_i(15)  <= al_ana_u_th_r;         -- Over threshold 
    intthr_i(16)  <= va_sup_up_th_r;        -- Over threshold 
    --                                                           
    intthr_i(17)  <= gtl_u_th_r;            -- Over threshold 
    intthr_i(18)  <= al_dig_u_th_r;         -- Over threshold 
    intthr_i(19)  <= va_rec_up_th_r;        -- Over threshold 
    --
    csr1_clrrst_i <= (bc_rst or csr1_clr);  -- Clear or reset
  end block assign_ints;

  -- Generate over-threshold meta stability filters
  over_thr_int : for i in 0 to 14 generate
    over_filter : int_mstable
      generic map (TIMES => INT_TIMES,
                   WIDTH => 10,
                   OVER  => true,
                   AUTO  => AUTO_CLEAR) 
      port map (clk       => clk,
                rstb      => rstb,
                rst       => csr1_clrrst_i,
                enable    => end_seq,
                value     => intval_i(i),
                threshold => intthr_i(i),
                int       => ints_i(i));
  end generate over_thr_int;

  -- Generate under-threshold meta stability filters
  under_thr_int : for i in 15 to 19 generate
    under_filter : int_mstable
      generic map (TIMES => INT_TIMES,
                   WIDTH => 10,
                   OVER  => false,
                   AUTO  => AUTO_CLEAR) 
      port map (clk       => clk,
                rstb      => rstb,
                rst       => csr1_clrrst_i,
                enable    => end_seq,
                value     => intval_i(i),
                threshold => intthr_i(i),
                int       => ints_i(i));
  end generate under_thr_int;

  -- purpose: Register monitor ADC input
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  adc_register : process (clk, rstb)
    variable idx_adc_i : integer;
    variable add_adc_i : integer;
    variable val_adc_i : std_logic_vector(9 downto 0) := (others => '0');
  begin  -- process adc_registers

    if rstb = '0' then                  -- asynchronous reset (active low)
      t1_i        <= regs(add_t1).def(9 downto 0);
      flash_i_i   <= regs(add_flash_i).def(9 downto 0);
      al_dig_i_i  <= regs(add_al_dig_i).def(9 downto 0);
      al_ana_i_i  <= regs(add_al_ana_i).def(9 downto 0);
      va_rec_ip_i <= regs(add_va_rec_ip).def(9 downto 0);
      t2_i        <= regs(add_t2).def(9 downto 0);
      va_sup_ip_i <= regs(add_va_sup_ip).def(9 downto 0);
      va_rec_im_i <= regs(add_va_rec_im).def(9 downto 0);
      va_sup_im_i <= regs(add_va_sup_im).def(9 downto 0);
      gtl_u_i     <= regs(add_gtl_u).def(9 downto 0);
      t3_i        <= regs(add_t3).def(9 downto 0);
      t1sens_i    <= regs(add_t1sens).def(9 downto 0);
      t2sens_i    <= regs(add_t2sens).def(9 downto 0);
      al_dig_u_i  <= regs(add_al_dig_u).def(9 downto 0);
      al_ana_u_i  <= regs(add_al_ana_u).def(9 downto 0);
      t4_i        <= regs(add_t4).def(9 downto 0);
      va_rec_up_i <= regs(add_va_rec_up).def(9 downto 0);
      va_sup_up_i <= regs(add_va_sup_up).def(9 downto 0);
      va_sup_um_i <= regs(add_va_sup_um).def(9 downto 0);
      va_rec_um_i <= regs(add_va_rec_um).def(9 downto 0);
      idx_adc_i   := 0;
      add_adc_i   := 0;
      val_adc_i   := (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_adc = '1' then
        idx_adc_i := to_integer(unsigned(add_adc));
        assert idx_adc_i >= BASE_ADC and idx_adc_i < BASE_ADC+NUM_ADC
          report "Index " & integer'image(idx_adc_i) & " out of range"
          severity failure;
        add_adc_i := adc_map(idx_adc_i);
        val_adc_i := data_adc(15 downto 6);

        case add_adc_i is
          when add_t1        => t1_i        <= val_adc_i;  -- First ADC T     
          when add_flash_i   => flash_i_i   <= val_adc_i;  -- I  3.3 V        
          when add_al_dig_i  => al_dig_i_i  <= val_adc_i;  -- I  2.5 V dig
          when add_al_ana_i  => al_ana_i_i  <= val_adc_i;  -- I  2.5 V ana
          when add_va_rec_ip => va_rec_ip_i <= val_adc_i;  -- I  2.5 V VA      
          when add_t2        => t2_i        <= val_adc_i;  -- Second ADC T    
          when add_va_sup_ip => va_sup_ip_i <= val_adc_i;  -- I  1.5 V VA     
          when add_va_rec_im => va_rec_im_i <= val_adc_i;  -- I -2.0 V        
          when add_va_sup_im => va_sup_im_i <= val_adc_i;  -- I -2.0 V VA      
          when add_gtl_u     => gtl_u_i     <= val_adc_i;  --    2.5 V GTL
          when add_t3        => t3_i        <= val_adc_i;  -- Third ADC T     
          when add_t1sens    => t1sens_i    <= val_adc_i;  -- Temp sens. 1     
          when add_t2sens    => t2sens_i    <= val_adc_i;  -- Temp sens. 2     
          when add_al_dig_u  => al_dig_u_i  <= val_adc_i;  -- U  2.5 altro dig
          when add_al_ana_u  => al_ana_u_i  <= val_adc_i;  -- U  2.5 altro ana
          when add_t4        => t4_i        <= val_adc_i;  -- Forth ADC T  
          when add_va_rec_up => va_rec_up_i <= val_adc_i;  -- U  2.5 VA (m)
          when add_va_sup_up => va_sup_up_i <= val_adc_i;  -- U  1.5 VA (m)
          when add_va_sup_um => va_sup_um_i <= val_adc_i;  -- U -2.0 VA (m)
          when add_va_rec_um => va_rec_um_i <= val_adc_i;  -- U -2.0 (m)   
          when others        => null;
        end case;
      end if;
    end if;
  end process adc_register;


  -- purpose: Register registers
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  reg_register : process (clk, rstb)
    variable add_i : integer;
  begin  -- process reg_register
    if rstb = '0' then                  -- asynchronous reset (active low)
      t1_th_i             <= regs(add_t1_th).def(9 downto 0);
      flash_i_th_i        <= regs(add_flash_i_th).def(9 downto 0);
      al_dig_i_th_i       <= regs(add_al_dig_i_th).def(9 downto 0);
      al_ana_i_th_i       <= regs(add_al_ana_i_th).def(9 downto 0);
      va_rec_ip_th_i      <= regs(add_va_rec_ip_th).def(9 downto 0);
      t2_th_i             <= regs(add_t2_th).def(9 downto 0);
      va_sup_ip_th_i      <= regs(add_va_sup_ip_th).def(9 downto 0);
      va_rec_im_th_i      <= regs(add_va_rec_im_th).def(9 downto 0);
      va_sup_im_th_i      <= regs(add_va_sup_im_th).def(9 downto 0);
      gtl_u_th_i          <= regs(add_gtl_u_th).def(9 downto 0);
      t3_th_i             <= regs(add_t3_th).def(9 downto 0);
      t1sens_th_i         <= regs(add_t1sens_th).def(9 downto 0);
      t2sens_th_i         <= regs(add_t2sens_th).def(9 downto 0);
      al_dig_u_th_i       <= regs(add_al_dig_u_th).def(9 downto 0);
      al_ana_u_th_i       <= regs(add_al_ana_u_th).def(9 downto 0);
      t4_th_i             <= regs(add_t4_th).def(9 downto 0);
      va_rec_up_th_i      <= regs(add_va_rec_up_th).def(9 downto 0);
      va_sup_up_th_i      <= regs(add_va_sup_up_th).def(9 downto 0);
      va_sup_um_th_i      <= regs(add_va_sup_um_th).def(9 downto 0);
      va_rec_um_th_i      <= regs(add_va_rec_um_th).def(9 downto 0);
      tsm_word_i          <= regs(add_tsm_word).def(8 downto 0);
      us_ratio_i          <= regs(add_us_ratio).def;
      csr0_i              <= regs(add_csr0).def(10 downto 0);
      csr1_i(11 downto 0) <= regs(add_csr1).def(11 downto 0);
      csr2_i              <= regs(add_csr2).def;
      csr3_i(14 downto 0) <= regs(add_csr3).def(14 downto 0);
      hold_wait_i         <= regs(add_hold_wait).def;
      l1_timeout_i        <= regs(add_l1_timeout).def;
      l2_timeout_i        <= regs(add_l2_timeout).def;
      shift_div_i         <= regs(add_shift_div).def;
      strips_i            <= regs(add_strips).def;
      cal_level_i         <= regs(add_cal_level).def;
      cal_iter_i          <= regs(add_cal_iter).def;
      shape_bias_i        <= regs(add_shape_bias).def;
      vfs_i               <= regs(add_vfs).def;
      vfp_i               <= regs(add_vfp).def;
      sample_div_i        <= regs(add_sample_div).def;
      fmdd_cmd_i          <= regs(add_fmdd_cmd).def;
      mebs_i              <= regs(add_mebs).def(8 downto 4);
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' then
        t1_th_i             <= regs(add_t1_th).def(9 downto 0);
        flash_i_th_i        <= regs(add_flash_i_th).def(9 downto 0);
        al_dig_i_th_i       <= regs(add_al_dig_i_th).def(9 downto 0);
        al_ana_i_th_i       <= regs(add_al_ana_i_th).def(9 downto 0);
        va_rec_ip_th_i      <= regs(add_va_rec_ip_th).def(9 downto 0);
        t2_th_i             <= regs(add_t2_th).def(9 downto 0);
        va_sup_ip_th_i      <= regs(add_va_sup_ip_th).def(9 downto 0);
        va_rec_im_th_i      <= regs(add_va_rec_im_th).def(9 downto 0);
        va_sup_im_th_i      <= regs(add_va_sup_im_th).def(9 downto 0);
        gtl_u_th_i          <= regs(add_gtl_u_th).def(9 downto 0);
        t3_th_i             <= regs(add_t3_th).def(9 downto 0);
        t1sens_th_i         <= regs(add_t1sens_th).def(9 downto 0);
        t2sens_th_i         <= regs(add_t2sens_th).def(9 downto 0);
        al_dig_u_th_i       <= regs(add_al_dig_u_th).def(9 downto 0);
        al_ana_u_th_i       <= regs(add_al_ana_u_th).def(9 downto 0);
        t4_th_i             <= regs(add_t4_th).def(9 downto 0);
        va_rec_up_th_i      <= regs(add_va_rec_up_th).def(9 downto 0);
        va_sup_up_th_i      <= regs(add_va_sup_up_th).def(9 downto 0);
        va_sup_um_th_i      <= regs(add_va_sup_um_th).def(9 downto 0);
        va_rec_um_th_i      <= regs(add_va_rec_um_th).def(9 downto 0);
        tsm_word_i          <= regs(add_tsm_word).def(8 downto 0);
        us_ratio_i          <= regs(add_us_ratio).def;
        csr0_i              <= regs(add_csr0).def(10 downto 0);
        csr1_i(11 downto 0) <= regs(add_csr1).def(11 downto 0);
        csr2_i              <= regs(add_csr2).def;
        csr3_i(14 downto 0) <= regs(add_csr3).def(14 downto 0);
        hold_wait_i         <= regs(add_hold_wait).def;
        l1_timeout_i        <= regs(add_l1_timeout).def;
        l2_timeout_i        <= regs(add_l2_timeout).def;
        shift_div_i         <= regs(add_shift_div).def;
        strips_i            <= regs(add_strips).def;
        cal_level_i         <= regs(add_cal_level).def;
        cal_iter_i          <= regs(add_cal_iter).def;
        shape_bias_i        <= regs(add_shape_bias).def;
        vfs_i               <= regs(add_vfs).def;
        vfp_i               <= regs(add_vfp).def;
        sample_div_i        <= regs(add_sample_div).def;
        fmdd_cmd_i          <= regs(add_fmdd_cmd).def;
        mebs_i              <= regs(add_mebs).def(8 downto 4);
      elsif csr1_clr = '1' then
        csr1_i(11 downto 0) <= regs(add_csr1).def(11 downto 0);
      elsif we = '1' then
        add_i := to_integer(unsigned(add));

        case add_i is
          when add_t1_th        => t1_th_i             <= din(9 downto 0);
          when add_flash_i_th   => flash_i_th_i        <= din(9 downto 0);
          when add_al_dig_i_th  => al_dig_i_th_i       <= din(9 downto 0);
          when add_al_ana_i_th  => al_ana_i_th_i       <= din(9 downto 0);
          when add_va_rec_ip_th => va_rec_ip_th_i      <= din(9 downto 0);
          when add_t2_th        => t2_th_i             <= din(9 downto 0);
          when add_va_sup_ip_th => va_sup_ip_th_i      <= din(9 downto 0);
          when add_va_rec_im_th => va_rec_im_th_i      <= din(9 downto 0);
          when add_va_sup_im_th => va_sup_im_th_i      <= din(9 downto 0);
          when add_gtl_u_th     => gtl_u_th_i          <= din(9 downto 0);
          when add_t3_th        => t3_th_i             <= din(9 downto 0);
          when add_t1sens_th    => t1sens_th_i         <= din(9 downto 0);
          when add_t2sens_th    => t2sens_th_i         <= din(9 downto 0);
          when add_al_dig_u_th  => al_dig_u_th_i       <= din(9 downto 0);
          when add_al_ana_u_th  => al_ana_u_th_i       <= din(9 downto 0);
          when add_t4_th        => t4_th_i             <= din(9 downto 0);
          when add_va_rec_up_th => va_rec_up_th_i      <= din(9 downto 0);
          when add_va_sup_up_th => va_sup_up_th_i      <= din(9 downto 0);
          when add_va_sup_um_th => va_sup_um_th_i      <= din(9 downto 0);
          when add_va_rec_um_th => va_rec_um_th_i      <= din(9 downto 0);
          when add_tsm_word     => tsm_word_i          <= din(8 downto 0);
          when add_us_ratio     => us_ratio_i          <= din(15 downto 0);
          when add_csr0         => csr0_i              <= din(10 downto 0);
          when add_csr1         => csr1_i(11 downto 0) <= din(11 downto 0);
          when add_csr2         => csr2_i(10 downto 0) <= din(10 downto 0);
          when add_csr3         => csr3_i(14 downto 0) <= din(14 downto 0);
          when add_hold_wait    => hold_wait_i         <= din(15 downto 0);
          when add_l1_timeout   => l1_timeout_i        <= din(15 downto 0);
          when add_l2_timeout   => l2_timeout_i        <= din(15 downto 0);
          when add_shift_div    => shift_div_i         <= din(15 downto 0);
          when add_strips       => strips_i            <= din(15 downto 0);
          when add_cal_level    => cal_level_i         <= din(15 downto 0);
          when add_cal_iter     => cal_iter_i          <= din(15 downto 0);
          when add_shape_bias   => shape_bias_i        <= din(15 downto 0);
          when add_vfs          => vfs_i               <= din(15 downto 0);
          when add_vfp          => vfp_i               <= din(15 downto 0);
          when add_sample_div   => sample_div_i        <= din(15 downto 0);
          when add_fmdd_cmd     => fmdd_cmd_i          <= din(15 downto 0);
          when add_mebs         => mebs_i              <= din(8 downto 4);
          when others           => null;
        end case;
      else
        -- Clear after one clock cycle 
        fmdd_cmd_i <= regs(add_fmdd_cmd).def;
      end if;
    end if;
  end process reg_register;

  -- purpose: Output data
  -- type   : combinational
  -- inputs : add
  -- outputs: 
  output : process (clk)                -- (add)
    variable add_al_i : integer;
    variable add_sc_i : integer;
  begin  -- process output
    if rstb = '0' then
      add_al_i := 0;
      add_sc_i := 0;
    else
      add_al_i := to_integer(unsigned(add_al));
      add_sc_i := to_integer(unsigned(add_sc));
    end if;

    get_reg(add => add_al_i, dout => dout_al);
    get_reg(add => add_sc_i, dout => dout_sc);
  end process output;

  -- purpose: Deal with commands type : sequential inputs : clk, rstb
  -- outputs:
  ans_cmds : process (clk, rstb)
  begin  -- process ans_cmds
    if rstb = '0' then                  -- asynchronous reset (active low)
      l0cnt_i   <= regs(add_l0cnt).def;
      l1cnt_i   <= regs(add_l1cnt).def;
      l2cnt_i   <= regs(add_l2cnt).def;
      sclkcnt_i <= regs(add_sclkcnt).def;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' or cnt_clr = '1' then
        l0cnt_i   <= regs(add_l0cnt).def;
        l1cnt_i   <= regs(add_l1cnt).def;
        l2cnt_i   <= regs(add_l2cnt).def;
        sclkcnt_i <= regs(add_sclkcnt).def;
      elsif cnt_lat = '1' then
        l0cnt_i   <= l0cnt_in;
        l1cnt_i   <= l1cnt_in;
        l2cnt_i   <= l2cnt_in;
        sclkcnt_i <= sclkcnt_in;
      end if;
    end if;
  end process ans_cmds;

  -- purpose: Process errors, etc.
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  errors : process (clk, rstb)
  begin  -- process errors
    if rstb = '0' then                  -- asynchronous reset (active low)
      csr1_ii    <= regs(add_csr1).def(csr1_ii'length-1 downto 0);
      csr3_i(15) <= '0';
      meb_cnt_i  <= regs(add_mebs).def(3 downto 0);
    elsif clk'event and clk = '1' then  -- rising clock edge
      meb_cnt_i <= meb_cnt;

      if bc_rst = '1' then
        csr3_i(15) <= '0';
      elsif st_cnv = '1' or cnv_mode_i = '1' then
        csr3_i(15) <= '0';
      elsif end_seq = '1' then
        csr3_i(15) <= '1';
      end if;

      if csr1_clrrst_i = '1' then
        csr1_ii <= regs(add_csr1).def(csr1_ii'length-1 downto 0);
      else
        -- Reset to zero.  The real setting depends on the constant AUTO_CLEAR
        csr1_ii(13 downto 0) <= (others => '0');

        -- Temperatures 
        if ((ints_i(0) = '1') or (ints_i(1) = '1') or
            (ints_i(2) = '1') or (ints_i(3) = '1') or
            (ints_i(4) = '1') or (ints_i(5) = '1')) then  -- Sensor
          csr1_ii(0) <= '1';
        end if;

        -- Analog currents
        if ((ints_i(6) = '1') or (ints_i(7) = '1') or
            (ints_i(8) = '1') or (ints_i(9) = '1')) then
          csr1_ii(2) <= '1';
        end if;

        -- Digitial current
        if ((ints_i(10) = '1') or (ints_i(11) = '1') or
            (ints_i(12) = '1')) then
          csr1_ii(4) <= '1';
        end if;

        -- Analog voltages
        if ((ints_i(13) = '1') or (ints_i(14) = '1') or
            (ints_i(15) = '1') or (ints_i(16) = '1')) then
          csr1_ii(1) <= '1';
        end if;

        -- Digitial voltages
        if ((ints_i(17) = '1') or (ints_i(18) = '1') or
            (ints_i(19) = '1')) then 
          csr1_ii(3) <= '1';
        end if;

        -- We clear above
        -- csr1_ii(13 downto 5) <= (others => '0');
        if paps_error = '1' then csr1_ii(5)  <= '1'; end if;
        if alps_error = '1' then csr1_ii(6)  <= '1'; end if;
        if missed_sclk = '1' then csr1_ii(7) <= '1'; end if;
        if par_error = '1' then csr1_ii(8)   <= '1'; end if;
        if ierr_al = '1' then csr1_ii(9)     <= '1'; end if;
        if al_error = '1' then csr1_ii(10)   <= '1'; end if;
        if ierr_sc = '1' then csr1_ii(11)    <= '1'; end if;
        if bc_error_i = '1' then csr1_ii(12) <= '1'; end if;
        if bc_int_i = '1' then csr1_ii(13)   <= '1'; end if;
      end if;  -- bc_rst   = '0'
    end if;  -- if clk'event
  end process errors;


  -- purpose: Collect combinatorics
  combi : block
  begin  -- block combi
    t1_th_r              <= t1_th_i;
    flash_i_th_r         <= flash_i_th_i;
    al_dig_i_th_r        <= al_dig_i_th_i;
    al_ana_i_th_r        <= al_ana_i_th_i;
    va_rec_ip_th_r       <= va_rec_ip_th_i;
    t1_r                 <= t1_i;
    flash_i_r            <= flash_i_i;
    al_dig_i_r           <= al_dig_i_i;
    al_ana_i_r           <= al_ana_i_i;
    va_rec_ip_r          <= va_rec_ip_i;
    t2_th_r              <= t2_th_i;
    va_sup_ip_th_r       <= va_sup_ip_th_i;
    va_rec_im_th_r       <= va_rec_im_th_i;
    va_sup_im_th_r       <= va_sup_im_th_i;
    gtl_u_th_r           <= gtl_u_th_i;
    t2_r                 <= t2_i;
    va_sup_ip_r          <= va_sup_ip_i;
    va_rec_im_r          <= va_rec_im_i;
    va_sup_im_r          <= va_sup_im_i;
    gtl_u_r              <= gtl_u_i;
    t3_th_r              <= t3_th_i;
    t1sens_th_r          <= t1sens_th_i;
    t2sens_th_r          <= t2sens_th_i;
    al_dig_u_th_r        <= al_dig_u_th_i;
    al_ana_u_th_r        <= al_ana_u_th_i;
    t3_r                 <= t3_i;
    t1sens_r             <= t1sens_i;
    t2sens_r             <= t2sens_i;
    al_dig_u_r           <= al_dig_u_i;
    al_ana_u_r           <= al_ana_u_i;
    t4_th_r              <= t4_th_i;
    va_rec_up_th_r       <= va_rec_up_th_i;
    va_sup_up_th_r       <= va_sup_up_th_i;
    va_sup_um_th_r       <= va_sup_um_th_i;
    va_rec_um_th_r       <= va_rec_um_th_i;
    t4_r                 <= t4_i;
    va_rec_up_r          <= va_rec_up_i;
    va_sup_up_r          <= va_sup_up_i;
    va_sup_um_r          <= va_sup_um_i;
    va_rec_um_r          <= va_rec_um_i;
    l1cnt_r              <= l1cnt_i;
    l2cnt_r              <= l2cnt_i;
    sclkcnt_r            <= sclkcnt_i;
    dstbcnt_r            <= dstbcnt_in;
    tsm_word_r           <= tsm_word_i;
    us_ratio_r           <= us_ratio_i;
    csr0_r               <= csr0_i;
    csr1_r(13 downto 12) <= csr1_ii(13 downto 12);
    csr1_i(13 downto 12) <= csr1_ii(13 downto 12);
    csr1_r(11 downto 0)  <= csr1_i(11 downto 0) or csr1_ii(11 downto 0);
    csr2_i(15 downto 11) <= hadd;
    csr2_r(15 downto 11) <= hadd;
    csr2_r(10 downto 0)  <= csr2_i(10 downto 0);
    csr3_r               <= csr3_i;
    fmdd_stat_r          <= fmdd_stat;     -- FMDD status
    l0cnt_r              <= l0cnt_i;       -- L0 counters
    hold_wait_r          <= hold_wait_i;   -- FMD: Wait to hold
    l1_timeout_r         <= l1_timeout_i;  -- FMD: L1 timeout
    l2_timeout_r         <= l2_timeout_i;  -- FMD: L2 timeout
    shift_div_r          <= shift_div_i;   -- FMD: Shift clk
    strips_r             <= strips_i;      -- FMD: Strips
    cal_level_r          <= cal_level_i;   -- FMD: Cal pulse
    cal_iter_r           <= cal_iter_i;    -- FMD: Cal events
    shape_bias_r         <= shape_bias_i;  -- FMD: Shape bias
    vfs_r                <= vfs_i;         -- FMD: Shape ref
    vfp_r                <= vfp_i;         -- FMD: Preamp ref
    sample_div_r         <= sample_div_i;  -- FMD: Sample clk
    fmdd_cmd_r           <= fmdd_cmd_i;    -- FMD: Commands
    mebs_r               <= mebs_i & meb_cnt_i;


    -- Interrupt and error
    bc_int_i <= '1' when
                ((csr1_r(7 downto 0) and csr0_r(7 downto 0)) /= X"00")
                else '0';
    bc_error_i <= '1' when
                  ((csr1_r(9 downto 8) and csr0_r(9 downto 8)) /= "00")
                  else '0';
    cnv_mode_i <= csr0_r(10);

    -- output
    bc_int     <= bc_int_i;
    bc_error   <= bc_error_i;
    cnv_mode   <= cnv_mode_i;
    csr2       <= csr2_r;
    csr3       <= csr3_r;
    tsm_word   <= tsm_word_r;
    us_ratio   <= us_ratio_r;
    -- Valid for 2 clock cycles
    fmdd_cmd   <= fmdd_cmd_r or fmdd_cmd_i;
    hold_wait  <= hold_wait_r;
    l1_timeout <= l1_timeout_r;
    l2_timeout <= l2_timeout_r;
    shift_div  <= shift_div_r;
    strips     <= strips_r;
    cal_level  <= cal_level_r;
    cal_iter   <= cal_iter_r;
    shape_bias <= shape_bias_r;
    vfs        <= vfs_r;
    vfp        <= vfp_r;
    sample_div <= sample_div_r;
    mebs       <= mebs_r(8 downto 4);

  end block combi;
end rtl2;
------------------------------------------------------------------------------
architecture rtl of registers_block is
  -- Cached values 
  -- signal adc_reg_i    : adc_reg_t;
  -- signal t_th_i       : unsigned(9 downto 0);  --! Temperature threshold
  -- signal av_th_i      : unsigned(9 downto 0);  --! Analog voltage threshold
  -- signal ac_th_i      : unsigned(9 downto 0);  --! Analog current threshold
  -- signal dv_th_i      : unsigned(9 downto 0);  --! Digitial voltage threshold
  -- signal dc_th_i      : unsigned(9 downto 0);  --! Digitial currrent thres
  signal t1_th_i        : unsigned(9 downto 0);  --! Th: First ADC T           
  signal flash_i_th_i   : unsigned(9 downto 0);  --! Th: I  3.3 V              
  signal al_dig_i_th_i  : unsigned(9 downto 0);  --! Th: I  2.5 V altro digital
  signal al_ana_i_th_i  : unsigned(9 downto 0);  --! Th: I  2.5 V altro analog 
  signal va_rec_ip_th_i : unsigned(9 downto 0);  --! Th: I  2.5 V VA           
  signal t1_i           : unsigned(9 downto 0);  --! Cu: First ADC T           
  signal flash_i_i      : unsigned(9 downto 0);  --! Cu: I  3.3 V              
  signal al_dig_i_i     : unsigned(9 downto 0);  --! Cu: I  2.5 V altro digital
  signal al_ana_i_i     : unsigned(9 downto 0);  --! Cu: I  2.5 V altro analog 
  signal va_rec_ip_i    : unsigned(9 downto 0);  --! Cu: I  2.5 V VA           
  signal t2_th_i        : unsigned(9 downto 0);  --! Th: Second ADC T           
  signal va_sup_ip_th_i : unsigned(9 downto 0);  --! Th: I  1.5 V VA            
  signal va_rec_im_th_i : unsigned(9 downto 0);  --! Th: I -2.0 V               
  signal va_sup_im_th_i : unsigned(9 downto 0);  --! Th: I -2.0 V VA            
  signal gtl_u_th_i     : unsigned(9 downto 0);  --! Th:    2.5 V Digital driver
  signal t2_i           : unsigned(9 downto 0);  --! Cu: Second ADC T           
  signal va_sup_ip_i    : unsigned(9 downto 0);  --! Cu: I  1.5 V VA            
  signal va_rec_im_i    : unsigned(9 downto 0);  --! Cu: I -2.0 V               
  signal va_sup_im_i    : unsigned(9 downto 0);  --! Cu: I -2.0 V VA            
  signal gtl_u_i        : unsigned(9 downto 0);  --! Cu:    2.5 V Digital driver
  signal t3_th_i        : unsigned(9 downto 0);  --! Th: Third ADC T         
  signal t1sens_th_i    : unsigned(9 downto 0);  --! Th: Temperature sens. 1 
  signal t2sens_th_i    : unsigned(9 downto 0);  --! Th: Temperature sens. 2 
  signal al_dig_u_th_i  : unsigned(9 downto 0);  --! Th: U  2.5 altro digital
  signal al_ana_u_th_i  : unsigned(9 downto 0);  --! Th: U  2.5 altro analog 
  signal t3_i           : unsigned(9 downto 0);  --! Cu: Third ADC T         
  signal t1sens_i       : unsigned(9 downto 0);  --! Cu: Temperature sens. 1 
  signal t2sens_i       : unsigned(9 downto 0);  --! Cu: Temperature sens. 2 
  signal al_dig_u_i     : unsigned(9 downto 0);  --! Cu: U  2.5 altro digital
  signal al_ana_u_i     : unsigned(9 downto 0);  --! Cu: U  2.5 altro analog 
  signal t4_th_i        : unsigned(9 downto 0);  --! Th: Forth ADC T  
  signal va_rec_up_th_i : unsigned(9 downto 0);  --! Th: U  2.5 VA (m)
  signal va_sup_up_th_i : unsigned(9 downto 0);  --! Th: U  1.5 VA (m)
  signal va_sup_um_th_i : unsigned(9 downto 0);  --! Th: U -2.0 VA (m)
  signal va_rec_um_th_i : unsigned(9 downto 0);  --! Th: U -2.0 (m)   
  signal t4_i           : unsigned(9 downto 0);  --! Cu: Forth ADC T  
  signal va_rec_up_i    : unsigned(9 downto 0);  --! Cu: U  2.5 VA (m)
  signal va_sup_up_i    : unsigned(9 downto 0);  --! Cu: U  1.5 VA (m)
  signal va_sup_um_i    : unsigned(9 downto 0);  --! Cu: U -2.0 VA (m)
  signal va_rec_um_i    : unsigned(9 downto 0);  --! Cu: U -2.0 (m)   
  signal l1cnt_i        : unsigned(15 downto 0);  --! L1 counter
  signal l2cnt_i        : unsigned(15 downto 0);  --! L2 counter
  signal sclkcnt_i      : unsigned(15 downto 0);  --! Slow clock counter
  -- signal dstbcnt_i   : unsigned(7 downto 0);  --! Data strobe counter
  signal tsm_word_i     : unsigned(8 downto 0);  --! Test mode words
  signal us_ratio_i     : unsigned(15 downto 0) := (others => '0');
  signal csr0_i         : unsigned(10 downto 0) := (others => '0');
  signal csr1_i         : unsigned(13 downto 0);  --! Config/Status 1
  signal csr2_i         : unsigned(15 downto 0);  --! Config/Status 2
  signal csr3_i         : unsigned(15 downto 0);  --! Config/Status 3
  signal l0cnt_i        : unsigned(15 downto 0);  --! L0 counters
  signal hold_wait_i    : unsigned(15 downto 0);  --! FMD: Wait to hold
  signal l1_timeout_i   : unsigned(15 downto 0);  --! FMD: L1 timeout
  signal l2_timeout_i   : unsigned(15 downto 0);  --! FMD: L2 timeout
  signal shift_div_i    : unsigned(15 downto 0);  --! FMD: Shift clk
  signal strips_i       : unsigned(15 downto 0);  --! FMD: Strips
  signal cal_level_i    : unsigned(15 downto 0);  --! FMD: Cal pulse
  signal cal_iter_i     : unsigned(15 downto 0);  --! FMD: Cal events
  signal shape_bias_i   : unsigned(15 downto 0);  --! FMD: Shape bias
  signal vfs_i          : unsigned(15 downto 0);  --! FMD: Shape ref
  signal vfp_i          : unsigned(15 downto 0);  --! FMD: Preamp ref
  signal sample_div_i   : unsigned(15 downto 0);  --! FMD: Sample clk
  signal fmdd_cmd_i     : unsigned(15 downto 0);  --! FMD: Commands
  signal mebs_i         : unsigned(4 downto 0);  --! MEB config

  -- Register values
  -- signal temp_r       : unsigned(9 downto 0);  -- Temperature
  -- signal av_r         : unsigned(9 downto 0);  -- Analog voltage
  -- signal ac_r         : unsigned(9 downto 0);  -- Analog current
  -- signal dv_r         : unsigned(9 downto 0);  -- Digital voltage
  -- signal dc_r         : unsigned(9 downto 0);  -- Digital current
  -- signal t_th_r       : unsigned(9 downto 0);  -- Temperature threshold
  -- signal av_th_r      : unsigned(9 downto 0);  -- Analog voltage threshold
  -- signal ac_th_r      : unsigned(9 downto 0);  -- Analog current threshold
  -- signal dv_th_r      : unsigned(9 downto 0);  -- Digitial voltage threshold
  -- signal dc_th_r      : unsigned(9 downto 0);  -- Digitial currrent thres
  signal t1_th_r        : unsigned(9 downto 0);  --! Th: First ADC T           
  signal flash_i_th_r   : unsigned(9 downto 0);  --! Th: I  3.3 V              
  signal al_dig_i_th_r  : unsigned(9 downto 0);  --! Th: I  2.5 V altro digital
  signal al_ana_i_th_r  : unsigned(9 downto 0);  --! Th: I  2.5 V altro analog 
  signal va_rec_ip_th_r : unsigned(9 downto 0);  --! Th: I  2.5 V VA           
  signal t1_r           : unsigned(9 downto 0);  --! Cu: First ADC T           
  signal flash_i_r      : unsigned(9 downto 0);  --! Cu: I  3.3 V              
  signal al_dig_i_r     : unsigned(9 downto 0);  --! Cu: I  2.5 V altro digital
  signal al_ana_i_r     : unsigned(9 downto 0);  --! Cu: I  2.5 V altro analog 
  signal va_rec_ip_r    : unsigned(9 downto 0);  --! Cu: I  2.5 V VA           
  signal t2_th_r        : unsigned(9 downto 0);  --! Th: Second ADC T           
  signal va_sup_ip_th_r : unsigned(9 downto 0);  --! Th: I  1.5 V VA            
  signal va_rec_im_th_r : unsigned(9 downto 0);  --! Th: I -2.0 V               
  signal va_sup_im_th_r : unsigned(9 downto 0);  --! Th: I -2.0 V VA            
  signal gtl_u_th_r     : unsigned(9 downto 0);  --! Th:    2.5 V Digital driver
  signal t2_r           : unsigned(9 downto 0);  --! Cu: Second ADC T           
  signal va_sup_ip_r    : unsigned(9 downto 0);  --! Cu: I  1.5 V VA            
  signal va_rec_im_r    : unsigned(9 downto 0);  --! Cu: I -2.0 V               
  signal va_sup_im_r    : unsigned(9 downto 0);  --! Cu: I -2.0 V VA            
  signal gtl_u_r        : unsigned(9 downto 0);  --! Cu:    2.5 V Digital driver
  signal t3_th_r        : unsigned(9 downto 0);  --! Th: Third ADC T          
  signal t1sens_th_r    : unsigned(9 downto 0);  --! Th: Temperature sens. 1  
  signal t2sens_th_r    : unsigned(9 downto 0);  --! Th: Temperature sens. 2  
  signal al_dig_u_th_r  : unsigned(9 downto 0);  --! Th: U  2.5 altro digital
  signal al_ana_u_th_r  : unsigned(9 downto 0);  --! Th: U  2.5 altro analog 
  signal t3_r           : unsigned(9 downto 0);  --! Cu: Third ADC T          
  signal t1sens_r       : unsigned(9 downto 0);  --! Cu: Temperature sens. 1  
  signal t2sens_r       : unsigned(9 downto 0);  --! Cu: Temperature sens. 2  
  signal al_dig_u_r     : unsigned(9 downto 0);  --! Cu: U  2.5 altro digital 
  signal al_ana_u_r     : unsigned(9 downto 0);  --! Cu: U  2.5 altro analog 
  signal t4_th_r        : unsigned(9 downto 0);  --! Th: Forth ADC T  
  signal va_rec_up_th_r : unsigned(9 downto 0);  --! Th: U  2.5 VA (m)
  signal va_sup_up_th_r : unsigned(9 downto 0);  --! Th: U  1.5 VA (m)
  signal va_sup_um_th_r : unsigned(9 downto 0);  --! Th: U -2.0 VA (m)
  signal va_rec_um_th_r : unsigned(9 downto 0);  --! Th: U -2.0 (m)   
  signal t4_r           : unsigned(9 downto 0);  --! Cu: Forth ADC T  
  signal va_rec_up_r    : unsigned(9 downto 0);  --! Cu: U  2.5 VA (m)
  signal va_sup_up_r    : unsigned(9 downto 0);  --! Cu: U  1.5 VA (m)
  signal va_sup_um_r    : unsigned(9 downto 0);  --! Cu: U -2.0 VA (m)
  signal va_rec_um_r    : unsigned(9 downto 0);  --! Cu: U -2.0 (m)   
  signal l1cnt_r        : unsigned(15 downto 0);  --! L1 counter
  signal l2cnt_r        : unsigned(15 downto 0);  --! L2 counter
  signal sclkcnt_r      : unsigned(15 downto 0);  --! Slow clock counter
  signal dstbcnt_r      : unsigned(7 downto 0);  --! Data strobe counter
  signal tsm_word_r     : unsigned(8 downto 0);  --! Test mode words
  signal us_ratio_r     : unsigned(15 downto 0);  --! Under sampling ratio
  signal csr0_r         : unsigned(10 downto 0) := (others => '0');
  signal csr1_r         : unsigned(13 downto 0) := (others => '0');
  signal csr2_r         : unsigned(15 downto 0);  --! Config/Status 2
  signal csr3_r         : unsigned(15 downto 0);  --! Config/Status 3
  signal fmdd_stat_r    : unsigned(15 downto 0);  --! FMDD status
  signal l0cnt_r        : unsigned(15 downto 0);  --! L0 counters
  signal hold_wait_r    : unsigned(15 downto 0);  --! FMD: Wait to hold
  signal l1_timeout_r   : unsigned(15 downto 0);  --! FMD: L1 timeout
  signal l2_timeout_r   : unsigned(15 downto 0);  --! FMD: L2 timeout
  signal shift_div_r    : unsigned(15 downto 0);  --! FMD: Shift clk
  signal strips_r       : unsigned(15 downto 0);  --! FMD: Strips
  signal cal_level_r    : unsigned(15 downto 0);  --! FMD: Cal pulse
  signal cal_iter_r     : unsigned(15 downto 0);  --! FMD: Cal events
  signal shape_bias_r   : unsigned(15 downto 0);  --! FMD: Shape bias
  signal vfs_r          : unsigned(15 downto 0);  --! FMD: Shape ref
  signal vfp_r          : unsigned(15 downto 0);  --! FMD: Preamp ref
  signal sample_div_r   : unsigned(15 downto 0);  --! FMD: Sample clk
  signal fmdd_cmd_r     : unsigned(15 downto 0);  --! FMD: Commands
  signal meb_r          : unsigned(8 downto 0);  --! MEB config

  -- Temporaries
  signal csr1_ii       : unsigned(13 downto 0);         -- Config/Status 1
  signal cnv_mode_i    : std_logic;
  signal bc_error_i    : std_logic;
  signal bc_int_i      : std_logic;
  signal ints_i        : std_logic_vector(4 downto 0);  -- Last interrupt state
  signal old_end_cnv_i : std_logic;                     -- Last end_cnv
  signal meb_cnt_i     : unsigned(3 downto 0);          -- registered meb count

  -- purpose: Cast to std_logic_vector
  function u2slv (constant x : unsigned) return std_logic_vector is
  begin  -- function u2slv
    return std_logic_vector(x);
  end function u2slv;
  -- purpose: Cast to unsigned
  function slv2u (constant x : std_logic_vector) return unsigned is
  begin  -- function u2slv
    return unsigned(x);
  end function slv2u;
begin  -- rtl

  -- purpose: Register monitor ADC input
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  adc_register : process (clk, rstb)
    variable idx_adc_i : integer;
    variable add_adc_i : integer;
    variable val_adc_i : unsigned(9 downto 0) := (others => '0');
  begin  -- process adc_registers

    if rstb = '0' then                  -- asynchronous reset (active low)
      -- adc_reg_i(add_temp) <= df_temp;
      -- adc_reg_i(add_av)   <= df_av;
      -- adc_reg_i(add_ac)   <= df_av - df_ac;
      -- adc_reg_i(add_dv)   <= df_dv;
      -- adc_reg_i(add_dc)   <= df_dv - df_dc;
      t1_i        <= slv2u(regs(add_t1).def(9 downto 0));
      flash_i_i   <= slv2u(regs(add_flash_i).def(9 downto 0));
      al_dig_i_i  <= slv2u(regs(add_al_dig_i).def(9 downto 0));
      al_ana_i_i  <= slv2u(regs(add_al_ana_i).def(9 downto 0));
      va_rec_ip_i <= slv2u(regs(add_va_rec_ip).def(9 downto 0));
      t2_i        <= slv2u(regs(add_t2).def(9 downto 0));
      va_sup_ip_i <= slv2u(regs(add_va_sup_ip).def(9 downto 0));
      va_rec_im_i <= slv2u(regs(add_va_rec_im).def(9 downto 0));
      va_sup_im_i <= slv2u(regs(add_va_sup_im).def(9 downto 0));
      gtl_u_i     <= slv2u(regs(add_gtl_u).def(9 downto 0));
      t3_i        <= slv2u(regs(add_t3).def(9 downto 0));
      t1sens_i    <= slv2u(regs(add_t1sens).def(9 downto 0));
      t2sens_i    <= slv2u(regs(add_t2sens).def(9 downto 0));
      al_dig_u_i  <= slv2u(regs(add_al_dig_u).def(9 downto 0));
      al_ana_u_i  <= slv2u(regs(add_al_ana_u).def(9 downto 0));
      t4_i        <= slv2u(regs(add_t4).def(9 downto 0));
      va_rec_up_i <= slv2u(regs(add_va_rec_up).def(9 downto 0));
      va_sup_up_i <= slv2u(regs(add_va_sup_up).def(9 downto 0));
      va_sup_um_i <= slv2u(regs(add_va_sup_um).def(9 downto 0));
      va_rec_um_i <= slv2u(regs(add_va_rec_um).def(9 downto 0));
      idx_adc_i   := 0;
      add_adc_i   := 0;
      val_adc_i   := (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_adc = '1' then
        idx_adc_i := to_integer(unsigned(add_adc));
        assert idx_adc_i >= BASE_ADC and idx_adc_i < BASE_ADC+NUM_ADC
          report "Index " & integer'image(idx_adc_i) & " out of range"
          severity failure;
        add_adc_i := adc_map(idx_adc_i);
        val_adc_i := unsigned(data_adc(15 downto 6));

        -- if add_adc_i >= BASE_ADC and add_adc_i < BASE_ADC + NUM_ADC then
        --  adc_reg_i(add_adc_i) <= unsigned(data_adc(15 downto 6));
        -- end if;
        case add_adc_i is
          when add_t1        => t1_i        <= val_adc_i;  -- First ADC T     
          when add_flash_i   => flash_i_i   <= val_adc_i;  -- I  3.3 V        
          when add_al_dig_i  => al_dig_i_i  <= val_adc_i;  -- I  2.5 V dig
          when add_al_ana_i  => al_ana_i_i  <= val_adc_i;  -- I  2.5 V ana
          when add_va_rec_ip => va_rec_ip_i <= val_adc_i;  -- I  2.5 V VA      
          when add_t2        => t2_i        <= val_adc_i;  -- Second ADC T    
          when add_va_sup_ip => va_sup_ip_i <= val_adc_i;  -- I  1.5 V VA     
          when add_va_rec_im => va_rec_im_i <= val_adc_i;  -- I -2.0 V        
          when add_va_sup_im => va_sup_im_i <= val_adc_i;  -- I -2.0 V VA      
          when add_gtl_u     => gtl_u_i     <= val_adc_i;  --    2.5 V GTL
          when add_t3        => t3_i        <= val_adc_i;  -- Third ADC T     
          when add_t1sens    => t1sens_i    <= val_adc_i;  -- Temp sens. 1     
          when add_t2sens    => t2sens_i    <= val_adc_i;  -- Temp sens. 2     
          when add_al_dig_u  => al_dig_u_i  <= val_adc_i;  -- U  2.5 altro dig
          when add_al_ana_u  => al_ana_u_i  <= val_adc_i;  -- U  2.5 altro ana
          when add_t4        => t4_i        <= val_adc_i;  -- Forth ADC T  
          when add_va_rec_up => va_rec_up_i <= val_adc_i;  -- U  2.5 VA (m)
          when add_va_sup_up => va_sup_up_i <= val_adc_i;  -- U  1.5 VA (m)
          when add_va_sup_um => va_sup_um_i <= val_adc_i;  -- U -2.0 VA (m)
          when add_va_rec_um => va_rec_um_i <= val_adc_i;  -- U -2.0 (m)   
          when others        => null;
        end case;

      end if;
    end if;
  end process adc_register;


  -- purpose: Register registers
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  reg_register : process (clk, rstb)
    variable add_i  : integer;
    variable data_i : unsigned(15 downto 0);
  begin  -- process reg_register
    if rstb = '0' then                  -- asynchronous reset (active low)
      t1_th_i             <= slv2u(regs(add_t1_th).def(9 downto 0));
      flash_i_th_i        <= slv2u(regs(add_flash_i_th).def(9 downto 0));
      al_dig_i_th_i       <= slv2u(regs(add_al_dig_i_th).def(9 downto 0));
      al_ana_i_th_i       <= slv2u(regs(add_al_ana_i_th).def(9 downto 0));
      va_rec_ip_th_i      <= slv2u(regs(add_va_rec_ip_th).def(9 downto 0));
      t2_th_i             <= slv2u(regs(add_t2_th).def(9 downto 0));
      va_sup_ip_th_i      <= slv2u(regs(add_va_sup_ip_th).def(9 downto 0));
      va_rec_im_th_i      <= slv2u(regs(add_va_rec_im_th).def(9 downto 0));
      va_sup_im_th_i      <= slv2u(regs(add_va_sup_im_th).def(9 downto 0));
      gtl_u_th_i          <= slv2u(regs(add_gtl_u_th).def(9 downto 0));
      t3_th_i             <= slv2u(regs(add_t3_th).def(9 downto 0));
      t1sens_th_i         <= slv2u(regs(add_t1sens_th).def(9 downto 0));
      t2sens_th_i         <= slv2u(regs(add_t2sens_th).def(9 downto 0));
      al_dig_u_th_i       <= slv2u(regs(add_al_dig_u_th).def(9 downto 0));
      al_ana_u_th_i       <= slv2u(regs(add_al_ana_u_th).def(9 downto 0));
      t4_th_i             <= slv2u(regs(add_t4_th).def(9 downto 0));
      va_rec_up_th_i      <= slv2u(regs(add_va_rec_up_th).def(9 downto 0));
      va_sup_up_th_i      <= slv2u(regs(add_va_sup_up_th).def(9 downto 0));
      va_sup_um_th_i      <= slv2u(regs(add_va_sup_um_th).def(9 downto 0));
      va_rec_um_th_i      <= slv2u(regs(add_va_rec_um_th).def(9 downto 0));
      tsm_word_i          <= slv2u(regs(add_tsm_word).def(8 downto 0));
      us_ratio_i          <= slv2u(regs(add_us_ratio).def);
      csr0_i              <= slv2u(regs(add_csr0).def(10 downto 0));
      csr1_i(11 downto 0) <= slv2u(regs(add_csr1).def(11 downto 0));
      csr2_i              <= slv2u(regs(add_csr2).def);
      csr3_i(14 downto 0) <= slv2u(regs(add_csr3).def(14 downto 0));
      hold_wait_i         <= slv2u(regs(add_hold_wait).def);
      l1_timeout_i        <= slv2u(regs(add_l1_timeout).def);
      l2_timeout_i        <= slv2u(regs(add_l2_timeout).def);
      shift_div_i         <= slv2u(regs(add_shift_div).def);
      strips_i            <= slv2u(regs(add_strips).def);
      cal_level_i         <= slv2u(regs(add_cal_level).def);
      cal_iter_i          <= slv2u(regs(add_cal_iter).def);
      shape_bias_i        <= slv2u(regs(add_shape_bias).def);
      vfs_i               <= slv2u(regs(add_vfs).def);
      vfp_i               <= slv2u(regs(add_vfp).def);
      sample_div_i        <= slv2u(regs(add_sample_div).def);
      fmdd_cmd_i          <= slv2u(regs(add_fmdd_cmd).def);
      mebs_i              <= slv2u(regs(add_mebs).def(8 downto 4));
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' then
        -- t_th_i              <= df_temp;
        -- av_th_i             <= slv2u(regs(add_av_th).def(9 downto 0));
        -- ac_th_i             <= slv2u(regs(add_ac_th).def(9 downto 0));
        -- dv_th_i             <= slv2u(regs(add_dv_th).def(9 downto 0));
        -- dc_th_i             <= slv2u(regs(add_dc_th).def(9 downto 0));
        t1_th_i             <= slv2u(regs(add_t1_th).def(9 downto 0));
        flash_i_th_i        <= slv2u(regs(add_flash_i_th).def(9 downto 0));
        al_dig_i_th_i       <= slv2u(regs(add_al_dig_i_th).def(9 downto 0));
        al_ana_i_th_i       <= slv2u(regs(add_al_ana_i_th).def(9 downto 0));
        va_rec_ip_th_i      <= slv2u(regs(add_va_rec_ip_th).def(9 downto 0));
        t2_th_i             <= slv2u(regs(add_t2_th).def(9 downto 0));
        va_sup_ip_th_i      <= slv2u(regs(add_va_sup_ip_th).def(9 downto 0));
        va_rec_im_th_i      <= slv2u(regs(add_va_rec_im_th).def(9 downto 0));
        va_sup_im_th_i      <= slv2u(regs(add_va_sup_im_th).def(9 downto 0));
        gtl_u_th_i          <= slv2u(regs(add_gtl_u_th).def(9 downto 0));
        t3_th_i             <= slv2u(regs(add_t3_th).def(9 downto 0));
        t1sens_th_i         <= slv2u(regs(add_t1sens_th).def(9 downto 0));
        t2sens_th_i         <= slv2u(regs(add_t2sens_th).def(9 downto 0));
        al_dig_u_th_i       <= slv2u(regs(add_al_dig_u_th).def(9 downto 0));
        al_ana_u_th_i       <= slv2u(regs(add_al_ana_u_th).def(9 downto 0));
        t4_th_i             <= slv2u(regs(add_t4_th).def(9 downto 0));
        va_rec_up_th_i      <= slv2u(regs(add_va_rec_up_th).def(9 downto 0));
        va_sup_up_th_i      <= slv2u(regs(add_va_sup_up_th).def(9 downto 0));
        va_sup_um_th_i      <= slv2u(regs(add_va_sup_um_th).def(9 downto 0));
        va_rec_um_th_i      <= slv2u(regs(add_va_rec_um_th).def(9 downto 0));
        tsm_word_i          <= slv2u(regs(add_tsm_word).def(8 downto 0));
        us_ratio_i          <= slv2u(regs(add_us_ratio).def);
        csr0_i              <= slv2u(regs(add_csr0).def(10 downto 0));
        csr1_i(11 downto 0) <= slv2u(regs(add_csr1).def(11 downto 0));
        csr2_i              <= slv2u(regs(add_csr2).def);
        csr3_i(14 downto 0) <= slv2u(regs(add_csr3).def(14 downto 0));
        hold_wait_i         <= slv2u(regs(add_hold_wait).def);
        l1_timeout_i        <= slv2u(regs(add_l1_timeout).def);
        l2_timeout_i        <= slv2u(regs(add_l2_timeout).def);
        shift_div_i         <= slv2u(regs(add_shift_div).def);
        strips_i            <= slv2u(regs(add_strips).def);
        cal_level_i         <= slv2u(regs(add_cal_level).def);
        cal_iter_i          <= slv2u(regs(add_cal_iter).def);
        shape_bias_i        <= slv2u(regs(add_shape_bias).def);
        vfs_i               <= slv2u(regs(add_vfs).def);
        vfp_i               <= slv2u(regs(add_vfp).def);
        sample_div_i        <= slv2u(regs(add_sample_div).def);
        fmdd_cmd_i          <= slv2u(regs(add_fmdd_cmd).def);
        mebs_i              <= slv2u(regs(add_mebs).def(8 downto 4));
      elsif csr1_clr = '1' then
        csr1_i(11 downto 0) <= slv2u(regs(add_csr1).def(11 downto 0));
      elsif we = '1' then
        add_i  := to_integer(unsigned(add));
        data_i := unsigned(din(15 downto 0));

        case add_i is
          -- when add_t_th       => t_th_i              <= data_i(9 downto 0);
          -- when add_av_th      => av_th_i             <= data_i(9 downto 0);
          -- when add_ac_th      => ac_th_i             <= data_i(9 downto 0);
          -- when add_dv_th      => dv_th_i             <= data_i(9 downto 0);
          -- when add_dc_th      => dc_th_i             <= data_i(9 downto 0);
          when add_t1_th        => t1_th_i             <= data_i(9 downto 0);
          when add_flash_i_th   => flash_i_th_i        <= data_i(9 downto 0);
          when add_al_dig_i_th  => al_dig_i_th_i       <= data_i(9 downto 0);
          when add_al_ana_i_th  => al_ana_i_th_i       <= data_i(9 downto 0);
          when add_va_rec_ip_th => va_rec_ip_th_i      <= data_i(9 downto 0);
          when add_t2_th        => t2_th_i             <= data_i(9 downto 0);
          when add_va_sup_ip_th => va_sup_ip_th_i      <= data_i(9 downto 0);
          when add_va_rec_im_th => va_rec_im_th_i      <= data_i(9 downto 0);
          when add_va_sup_im_th => va_sup_im_th_i      <= data_i(9 downto 0);
          when add_gtl_u_th     => gtl_u_th_i          <= data_i(9 downto 0);
          when add_t3_th        => t3_th_i             <= data_i(9 downto 0);
          when add_t1sens_th    => t1sens_th_i         <= data_i(9 downto 0);
          when add_t2sens_th    => t2sens_th_i         <= data_i(9 downto 0);
          when add_al_dig_u_th  => al_dig_u_th_i       <= data_i(9 downto 0);
          when add_al_ana_u_th  => al_ana_u_th_i       <= data_i(9 downto 0);
          when add_t4_th        => t4_th_i             <= data_i(9 downto 0);
          when add_va_rec_up_th => va_rec_up_th_i      <= data_i(9 downto 0);
          when add_va_sup_up_th => va_sup_up_th_i      <= data_i(9 downto 0);
          when add_va_sup_um_th => va_sup_um_th_i      <= data_i(9 downto 0);
          when add_va_rec_um_th => va_rec_um_th_i      <= data_i(9 downto 0);
          when add_tsm_word     => tsm_word_i          <= data_i(8 downto 0);
          when add_us_ratio     => us_ratio_i          <= data_i;
          when add_csr0         => csr0_i              <= data_i(10 downto 0);
          when add_csr1         => csr1_i(11 downto 0) <= data_i(11 downto 0);
          when add_csr2         => csr2_i(10 downto 0) <= data_i(10 downto 0);
          when add_csr3         => csr3_i(14 downto 0) <= data_i(14 downto 0);
          when add_hold_wait    => hold_wait_i         <= data_i;
          when add_l1_timeout   => l1_timeout_i        <= data_i;
          when add_l2_timeout   => l2_timeout_i        <= data_i;
          when add_shift_div    => shift_div_i         <= data_i;
          when add_strips       => strips_i            <= data_i;
          when add_cal_level    => cal_level_i         <= data_i;
          when add_cal_iter     => cal_iter_i          <= data_i;
          when add_shape_bias   => shape_bias_i        <= data_i;
          when add_vfs          => vfs_i               <= data_i;
          when add_vfp          => vfp_i               <= data_i;
          when add_sample_div   => sample_div_i        <= data_i;
          when add_fmdd_cmd     => fmdd_cmd_i          <= data_i;
          when add_mebs         => mebs_i              <= data_i(8 downto 4);
          when others           => null;
        end case;
      else
        -- Clear after one clock cycle 
        fmdd_cmd_i <= slv2u(regs(add_fmdd_cmd).def);
      end if;
    end if;
  end process reg_register;


  -- purpose: Output data
  -- type   : combinational
  -- inputs : add
  -- outputs: 
  output : process (clk)                -- (add)
    variable add_al_i : integer;
    variable add_sc_i : integer;
  begin  -- process output
    if rstb = '0' then
      add_al_i := 0;
      add_sc_i := 0;
    else
      add_al_i := to_integer(unsigned(add_al));
      add_sc_i := to_integer(unsigned(add_sc));
    end if;

    dout_al <= (others => '0');

    case add_al_i is
      when add_free         => dout_al              <= version;
      when add_t1_th        => dout_al(9 downto 0)  <= u2slv(t1_th_r);
      when add_flash_i_th   => dout_al(9 downto 0)  <= u2slv(flash_i_th_r);
      when add_al_dig_i_th  => dout_al(9 downto 0)  <= u2slv(al_dig_i_th_r);
      when add_al_ana_i_th  => dout_al(9 downto 0)  <= u2slv(al_ana_i_th_r);
      when add_va_rec_ip_th => dout_al(9 downto 0)  <= u2slv(va_rec_ip_th_r);
      when add_t1           => dout_al(9 downto 0)  <= u2slv(t1_r);
      when add_flash_i      => dout_al(9 downto 0)  <= u2slv(flash_i_r);
      when add_al_dig_i     => dout_al(9 downto 0)  <= u2slv(al_dig_i_r);
      when add_al_ana_i     => dout_al(9 downto 0)  <= u2slv(al_ana_i_r);
      when add_va_rec_ip    => dout_al(9 downto 0)  <= u2slv(va_rec_ip_r);
      when add_t2_th        => dout_al(9 downto 0)  <= u2slv(t2_th_r);
      when add_va_sup_ip_th => dout_al(9 downto 0)  <= u2slv(va_sup_ip_th_r);
      when add_va_rec_im_th => dout_al(9 downto 0)  <= u2slv(va_rec_im_th_r);
      when add_va_sup_im_th => dout_al(9 downto 0)  <= u2slv(va_sup_im_th_r);
      when add_gtl_u_th     => dout_al(9 downto 0)  <= u2slv(gtl_u_th_r);
      when add_t2           => dout_al(9 downto 0)  <= u2slv(t2_r);
      when add_va_sup_ip    => dout_al(9 downto 0)  <= u2slv(va_sup_ip_r);
      when add_va_rec_im    => dout_al(9 downto 0)  <= u2slv(va_rec_im_r);
      when add_va_sup_im    => dout_al(9 downto 0)  <= u2slv(va_sup_im_r);
      when add_gtl_u        => dout_al(9 downto 0)  <= u2slv(gtl_u_r);
      when add_t3_th        => dout_al(9 downto 0)  <= u2slv(t3_th_r);
      when add_t1sens_th    => dout_al(9 downto 0)  <= u2slv(t1sens_th_r);
      when add_t2sens_th    => dout_al(9 downto 0)  <= u2slv(t2sens_th_r);
      when add_al_dig_u_th  => dout_al(9 downto 0)  <= u2slv(al_dig_u_th_r);
      when add_al_ana_u_th  => dout_al(9 downto 0)  <= u2slv(al_ana_u_th_r);
      when add_t3           => dout_al(9 downto 0)  <= u2slv(t3_r);
      when add_t1sens       => dout_al(9 downto 0)  <= u2slv(t1sens_r);
      when add_t2sens       => dout_al(9 downto 0)  <= u2slv(t2sens_r);
      when add_al_dig_u     => dout_al(9 downto 0)  <= u2slv(al_dig_u_r);
      when add_al_ana_u     => dout_al(9 downto 0)  <= u2slv(al_ana_u_r);
      when add_t4_th        => dout_al(9 downto 0)  <= u2slv(t4_th_r);
      when add_va_rec_up_th => dout_al(9 downto 0)  <= u2slv(va_rec_up_th_r);
      when add_va_sup_up_th => dout_al(9 downto 0)  <= u2slv(va_sup_up_th_r);
      when add_va_sup_um_th => dout_al(9 downto 0)  <= u2slv(va_sup_um_th_r);
      when add_va_rec_um_th => dout_al(9 downto 0)  <= u2slv(va_rec_um_th_r);
      when add_t4           => dout_al(9 downto 0)  <= u2slv(t4_r);
      when add_va_rec_up    => dout_al(9 downto 0)  <= u2slv(va_rec_up_r);
      when add_va_sup_up    => dout_al(9 downto 0)  <= u2slv(va_sup_up_r);
      when add_va_sup_um    => dout_al(9 downto 0)  <= u2slv(va_sup_um_r);
      when add_va_rec_um    => dout_al(9 downto 0)  <= u2slv(va_rec_um_r);
      when add_tsm_word     => dout_al(8 downto 0)  <= u2slv(tsm_word_r);
      when add_us_ratio     => dout_al              <= u2slv(us_ratio_r);
      when add_l1cnt        => dout_al              <= u2slv(l1cnt_r);
      when add_l2cnt        => dout_al              <= u2slv(l2cnt_r);
      when add_sclkcnt      => dout_al              <= u2slv(sclkcnt_r);
      when add_dstbcnt      => dout_al(7 downto 0)  <= dstbcnt_in;
      when add_csr0         => dout_al(10 downto 0) <= u2slv(csr0_r);
      when add_csr1         => dout_al(13 downto 0) <= u2slv(csr1_r);
      when add_csr2         => dout_al              <= u2slv(csr2_r);
      when add_csr3         => dout_al              <= u2slv(csr3_r);
      when add_fmdd_stat    => dout_al              <= u2slv(fmdd_stat_r);
      when add_l0cnt        => dout_al              <= u2slv(l0cnt_r);
      when add_hold_wait    => dout_al              <= u2slv(hold_wait_r);
      when add_l1_timeout   => dout_al              <= u2slv(l1_timeout_r);
      when add_l2_timeout   => dout_al              <= u2slv(l2_timeout_r);
      when add_shift_div    => dout_al              <= u2slv(shift_div_r);
      when add_strips       => dout_al              <= u2slv(strips_r);
      when add_cal_level    => dout_al              <= u2slv(cal_level_r);
      when add_cal_iter     => dout_al              <= u2slv(cal_iter_r);
      when add_shape_bias   => dout_al              <= u2slv(shape_bias_r);
      when add_vfs          => dout_al              <= u2slv(vfs_r);
      when add_vfp          => dout_al              <= u2slv(vfp_r);
      when add_sample_div   => dout_al              <= u2slv(sample_div_r);
      when add_mebs         => dout_al(8 downto 0)  <= u2slv(meb_r);
      when others           => dout_al              <= (others => '0');
    end case;

    dout_sc <= (others => '0');
    case add_sc_i is
      when add_free         => dout_sc              <= version;
      when add_t1_th        => dout_sc(9 downto 0)  <= u2slv(t1_th_r);
      when add_flash_i_th   => dout_sc(9 downto 0)  <= u2slv(flash_i_th_r);
      when add_al_dig_i_th  => dout_sc(9 downto 0)  <= u2slv(al_dig_i_th_r);
      when add_al_ana_i_th  => dout_sc(9 downto 0)  <= u2slv(al_ana_i_th_r);
      when add_va_rec_ip_th => dout_sc(9 downto 0)  <= u2slv(va_rec_ip_th_r);
      when add_t1           => dout_sc(9 downto 0)  <= u2slv(t1_r);
      when add_flash_i      => dout_sc(9 downto 0)  <= u2slv(flash_i_r);
      when add_al_dig_i     => dout_sc(9 downto 0)  <= u2slv(al_dig_i_r);
      when add_al_ana_i     => dout_sc(9 downto 0)  <= u2slv(al_ana_i_r);
      when add_va_rec_ip    => dout_sc(9 downto 0)  <= u2slv(va_rec_ip_r);
      when add_t2_th        => dout_sc(9 downto 0)  <= u2slv(t2_th_r);
      when add_va_sup_ip_th => dout_sc(9 downto 0)  <= u2slv(va_sup_ip_th_r);
      when add_va_rec_im_th => dout_sc(9 downto 0)  <= u2slv(va_rec_im_th_r);
      when add_va_sup_im_th => dout_sc(9 downto 0)  <= u2slv(va_sup_im_th_r);
      when add_gtl_u_th     => dout_sc(9 downto 0)  <= u2slv(gtl_u_th_r);
      when add_t2           => dout_sc(9 downto 0)  <= u2slv(t2_r);
      when add_va_sup_ip    => dout_sc(9 downto 0)  <= u2slv(va_sup_ip_r);
      when add_va_rec_im    => dout_sc(9 downto 0)  <= u2slv(va_rec_im_r);
      when add_va_sup_im    => dout_sc(9 downto 0)  <= u2slv(va_sup_im_r);
      when add_gtl_u        => dout_sc(9 downto 0)  <= u2slv(gtl_u_r);
      when add_t3_th        => dout_sc(9 downto 0)  <= u2slv(t3_th_r);
      when add_t1sens_th    => dout_sc(9 downto 0)  <= u2slv(t1sens_th_r);
      when add_t2sens_th    => dout_sc(9 downto 0)  <= u2slv(t2sens_th_r);
      when add_al_dig_u_th  => dout_sc(9 downto 0)  <= u2slv(al_dig_u_th_r);
      when add_al_ana_u_th  => dout_sc(9 downto 0)  <= u2slv(al_ana_u_th_r);
      when add_t3           => dout_sc(9 downto 0)  <= u2slv(t3_r);
      when add_t1sens       => dout_sc(9 downto 0)  <= u2slv(t1sens_r);
      when add_t2sens       => dout_sc(9 downto 0)  <= u2slv(t2sens_r);
      when add_al_dig_u     => dout_sc(9 downto 0)  <= u2slv(al_dig_u_r);
      when add_al_ana_u     => dout_sc(9 downto 0)  <= u2slv(al_ana_u_r);
      when add_t4_th        => dout_sc(9 downto 0)  <= u2slv(t4_th_r);
      when add_va_rec_up_th => dout_sc(9 downto 0)  <= u2slv(va_rec_up_th_r);
      when add_va_sup_up_th => dout_sc(9 downto 0)  <= u2slv(va_sup_up_th_r);
      when add_va_sup_um_th => dout_sc(9 downto 0)  <= u2slv(va_sup_um_th_r);
      when add_va_rec_um_th => dout_sc(9 downto 0)  <= u2slv(va_rec_um_th_r);
      when add_t4           => dout_sc(9 downto 0)  <= u2slv(t4_r);
      when add_va_rec_up    => dout_sc(9 downto 0)  <= u2slv(va_rec_up_r);
      when add_va_sup_up    => dout_sc(9 downto 0)  <= u2slv(va_sup_up_r);
      when add_va_sup_um    => dout_sc(9 downto 0)  <= u2slv(va_sup_um_r);
      when add_va_rec_um    => dout_sc(9 downto 0)  <= u2slv(va_rec_um_r);
      when add_tsm_word     => dout_sc(8 downto 0)  <= u2slv(tsm_word_r);
      when add_us_ratio     => dout_sc              <= u2slv(us_ratio_r);
      when add_l1cnt        => dout_sc              <= u2slv(l1cnt_r);
      when add_l2cnt        => dout_sc              <= u2slv(l2cnt_r);
      when add_sclkcnt      => dout_sc              <= u2slv(sclkcnt_r);
      when add_dstbcnt      => dout_sc(7 downto 0)  <= dstbcnt_in;
      when add_csr0         => dout_sc(10 downto 0) <= u2slv(csr0_r);
      when add_csr1         => dout_sc(13 downto 0) <= u2slv(csr1_r);
      when add_csr2         => dout_sc              <= u2slv(csr2_r);
      when add_csr3         => dout_sc              <= u2slv(csr3_r);
      when add_fmdd_stat    => dout_sc              <= u2slv(fmdd_stat_r);
      when add_l0cnt        => dout_sc              <= u2slv(l0cnt_r);
      when add_hold_wait    => dout_sc              <= u2slv(hold_wait_r);
      when add_l1_timeout   => dout_sc              <= u2slv(l1_timeout_r);
      when add_l2_timeout   => dout_sc              <= u2slv(l2_timeout_r);
      when add_shift_div    => dout_sc              <= u2slv(shift_div_r);
      when add_strips       => dout_sc              <= u2slv(strips_r);
      when add_cal_level    => dout_sc              <= u2slv(cal_level_r);
      when add_cal_iter     => dout_sc              <= u2slv(cal_iter_r);
      when add_shape_bias   => dout_sc              <= u2slv(shape_bias_r);
      when add_vfs          => dout_sc              <= u2slv(vfs_r);
      when add_vfp          => dout_sc              <= u2slv(vfp_r);
      when add_sample_div   => dout_sc              <= u2slv(sample_div_r);
      when add_mebs         => dout_sc(8 downto 0)  <= u2slv(meb_r);
      when others           => dout_sc              <= (others => '0');
    end case;
  end process output;

  -- purpose: Deal with commands type : sequential inputs : clk, rstb
  -- outputs:
  ans_cmds : process (clk, rstb)
  begin  -- process ans_cmds
    if rstb = '0' then                  -- asynchronous reset (active low)
      l0cnt_i   <= (others => '0');
      l1cnt_i   <= (others => '0');
      l2cnt_i   <= (others => '0');
      sclkcnt_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_rst = '1' or cnt_clr = '1' then
        l0cnt_i   <= (others => '0');
        l1cnt_i   <= (others => '0');
        l2cnt_i   <= (others => '0');
        sclkcnt_i <= (others => '0');
      elsif cnt_lat = '1' then
        l0cnt_i   <= unsigned(l0cnt_in);
        l1cnt_i   <= unsigned(l1cnt_in);
        l2cnt_i   <= unsigned(l2cnt_in);
        sclkcnt_i <= unsigned(sclkcnt_in);
      end if;
    end if;
  end process ans_cmds;

  -- purpose: Process errors, etc.
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  errors : process (clk, rstb)
  begin  -- process errors
    if rstb = '0' then                  -- asynchronous reset (active low)
      csr1_ii(13 downto 0) <= (others => '0');
      csr3_i(15)           <= '0';
      ints_i(4 downto 0)   <= (others => '0');
      old_end_cnv_i        <= '0';
      meb_cnt_i            <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      meb_cnt_i <= unsigned(meb_cnt);

      if bc_rst = '1' then
        csr1_ii(13 downto 0) <= (others => '0');
        csr3_i(15)           <= '0';
        ints_i(4 downto 0)   <= (others => '0');
      else
        old_end_cnv_i <= end_seq;
        if st_cnv = '1' or cnv_mode_i = '1' then
          csr3_i(15) <= '0';
        elsif end_seq = '1' then
          csr3_i(15) <= '1';
        end if;
        if csr1_clr = '1' then
          csr1_ii(13 downto 0) <= (others => '0');
          ints_i(4 downto 0)   <= (others => '0');
        else
          if end_seq = '1' and old_end_cnv_i = '0' then
            csr1_ii(4 downto 0) <= (others => '0');

            -- Temperatures 
            if (t1_r > t1_th_r or             -- Chip 1
                t2_r > t2_th_r or             -- Chip 2
                t3_r > t3_th_r or             -- Chip 3
                t4_r > t4_th_r or             -- Chip 4
                t1sens_r > t1sens_th_r or     -- Sensor 
                t2sens_r > t2sens_th_r) then  -- Sensor
              ints_i(0) <= '1';
              if ints_i(0) = '1' then         -- glitch filtering
                csr1_ii(0) <= '1';
              end if;
            else
              ints_i(0) <= '0';
            end if;

            -- Analog voltages
            if (al_ana_u_r < al_ana_u_th_r or       -- Altro analog 
                va_sup_up_r < va_sup_up_th_r or     -- VA 1.5
                va_sup_um_r > va_sup_um_th_r or     -- VA -2.0
                va_rec_um_r > va_rec_um_th_r) then  -- VA -2.0
              ints_i(1) <= '1';
              if ints_i(1) = '1' then               -- glitch filtering
                csr1_ii(1) <= '1';
              end if;
            else
              ints_i(1) <= '0';
            end if;

            -- Analog currents
            if (al_ana_i_r > al_ana_i_th_r or
                va_sup_ip_r > va_sup_ip_th_r or
                va_rec_im_r > va_rec_im_th_r or
                va_sup_im_r > va_sup_im_th_r) then
              ints_i(2) <= '1';
              if ints_i(2) = '1' then   -- Glitch filtering
                csr1_ii(2) <= '1';
              end if;
            else
              ints_i(2) <= '0';
            end if;

            -- Digitial voltages
            if (gtl_u_r < gtl_u_th_r or             -- Driver digital
                al_dig_u_r < al_dig_u_th_r or       -- ALTRO digital 
                va_rec_up_r < va_rec_up_th_r) then  -- VA1 digital
              ints_i(3) <= '1';
              if ints_i(3) = '1' then               -- glitch filtering
                csr1_ii(3) <= '1';
              end if;
            else
              ints_i(3) <= '0';
            end if;

            -- Digitial current
            if (al_dig_i_r > al_dig_i_th_r or       -- ALTRO dig
                flash_i_r > flash_i_th_r or         -- ALTRO 3.3
                va_rec_ip_r > va_rec_ip_th_r) then  -- VA 2.5
              ints_i(4) <= '1';
              if ints_i(4) = '1' then               -- glitch filtering
                csr1_ii(4) <= '1';
              end if;
            else
              ints_i(4) <= '0';
            end if;

            -- if temp_r > t_th_r  then csr1_ii(0)  <= '1'; end if;
            -- if av_r   < av_th_r then csr1_ii(1)  <= '1'; end if;
            -- if ac_r   > ac_th_r then csr1_ii(2)  <= '1'; end if;
            -- if dv_r   < dv_th_r then csr1_ii(3)  <= '1'; end if;
            -- if dc_r   > dc_th_r then csr1_ii(4)  <= '1'; end if;
          end if;

          csr1_ii(13 downto 5) <= (others => '0');

          if paps_error = '1' then csr1_ii(5)  <= '1'; end if;
          if alps_error = '1' then csr1_ii(6)  <= '1'; end if;
          if missed_sclk = '1' then csr1_ii(7) <= '1'; end if;
          if par_error = '1' then csr1_ii(8)   <= '1'; end if;
          if ierr_al = '1' then csr1_ii(9)     <= '1'; end if;
          if al_error = '1' then csr1_ii(10)   <= '1'; end if;
          if ierr_sc = '1' then csr1_ii(11)    <= '1'; end if;
          if bc_error_i = '1' then csr1_ii(12) <= '1'; end if;
          if bc_int_i = '1' then csr1_ii(13)   <= '1'; end if;
        end if;  -- csr1_clr = '0'
      end if;  -- bc_rst   = '0'
    end if;  -- if clk'event
  end process errors;


  -- purpose: Collect combinatorics
  combi : block
  begin  -- block combi
    -- t_th_r               <= t_th_i;
    -- av_th_r              <= av_th_i;
    -- ac_th_r              <= ac_th_i;
    -- dv_th_r              <= dv_th_i;
    -- dc_th_r              <= dc_th_i;
    -- temp_r               <= adc_reg_i(add_temp);
    -- av_r                 <= adc_reg_i(add_av);
    -- ac_r                 <= adc_reg_i(add_av) - adc_reg_i(add_ac);
    -- dv_r                 <= adc_reg_i(add_dv);
    -- dc_r                 <= adc_reg_i(add_dv) - adc_reg_i(add_dc);
    t1_th_r              <= t1_th_i;
    flash_i_th_r         <= flash_i_th_i;
    al_dig_i_th_r        <= al_dig_i_th_i;
    al_ana_i_th_r        <= al_ana_i_th_i;
    va_rec_ip_th_r       <= va_rec_ip_th_i;
    t1_r                 <= t1_i;
    flash_i_r            <= flash_i_i;
    al_dig_i_r           <= al_dig_i_i;
    al_ana_i_r           <= al_ana_i_i;
    va_rec_ip_r          <= va_rec_ip_i;
    t2_th_r              <= t2_th_i;
    va_sup_ip_th_r       <= va_sup_ip_th_i;
    va_rec_im_th_r       <= va_rec_im_th_i;
    va_sup_im_th_r       <= va_sup_im_th_i;
    gtl_u_th_r           <= gtl_u_th_i;
    t2_r                 <= t2_i;
    va_sup_ip_r          <= va_sup_ip_i;
    va_rec_im_r          <= va_rec_im_i;
    va_sup_im_r          <= va_sup_im_i;
    gtl_u_r              <= gtl_u_i;
    t3_th_r              <= t3_th_i;
    t1sens_th_r          <= t1sens_th_i;
    t2sens_th_r          <= t2sens_th_i;
    al_dig_u_th_r        <= al_dig_u_th_i;
    al_ana_u_th_r        <= al_ana_u_th_i;
    t3_r                 <= t3_i;
    t1sens_r             <= t1sens_i;
    t2sens_r             <= t2sens_i;
    al_dig_u_r           <= al_dig_u_i;
    al_ana_u_r           <= al_ana_u_i;
    t4_th_r              <= t4_th_i;
    va_rec_up_th_r       <= va_rec_up_th_i;
    va_sup_up_th_r       <= va_sup_up_th_i;
    va_sup_um_th_r       <= va_sup_um_th_i;
    va_rec_um_th_r       <= va_rec_um_th_i;
    t4_r                 <= t4_i;
    va_rec_up_r          <= va_rec_up_i;
    va_sup_up_r          <= va_sup_up_i;
    va_sup_um_r          <= va_sup_um_i;
    va_rec_um_r          <= va_rec_um_i;
    l1cnt_r              <= l1cnt_i;
    l2cnt_r              <= l2cnt_i;
    sclkcnt_r            <= sclkcnt_i;
    dstbcnt_r            <= unsigned(dstbcnt_in);
    tsm_word_r           <= tsm_word_i;
    us_ratio_r           <= us_ratio_i;
    csr0_r               <= csr0_i;
    csr1_r(13 downto 12) <= csr1_ii(13 downto 12);
    csr1_i(13 downto 12) <= csr1_ii(13 downto 12);
    csr1_r(11 downto 0)  <= csr1_i(11 downto 0) or csr1_ii(11 downto 0);
    csr2_i(15 downto 11) <= unsigned(hadd);
    csr2_r(15 downto 11) <= unsigned(hadd);
    csr2_r(10 downto 0)  <= csr2_i(10 downto 0);
    csr3_r               <= csr3_i;
    fmdd_stat_r          <= unsigned(fmdd_stat);  -- FMDD status
    l0cnt_r              <= l0cnt_i;              -- L0 counters
    hold_wait_r          <= hold_wait_i;          -- FMD: Wait to hold
    l1_timeout_r         <= l1_timeout_i;         -- FMD: L1 timeout
    l2_timeout_r         <= l2_timeout_i;         -- FMD: L2 timeout
    shift_div_r          <= shift_div_i;          -- FMD: Shift clk
    strips_r             <= strips_i;             -- FMD: Strips
    cal_level_r          <= cal_level_i;          -- FMD: Cal pulse
    cal_iter_r           <= cal_iter_i;           -- FMD: Cal events
    shape_bias_r         <= shape_bias_i;         -- FMD: Shape bias
    vfs_r                <= vfs_i;                -- FMD: Shape ref
    vfp_r                <= vfp_i;                -- FMD: Preamp ref
    sample_div_r         <= sample_div_i;         -- FMD: Sample clk
    fmdd_cmd_r           <= fmdd_cmd_i;           -- FMD: Commands
    meb_r(8 downto 4)    <= mebs_i;
    meb_r(3 downto 0)    <= meb_cnt_i;

    -- Interrupt and error
    bc_int_i <= '1' when
                ((csr1_r(7 downto 0) and csr0_r(7 downto 0)) /= X"00")
                else '0';
    bc_error_i <= '1' when
                  ((csr1_r(9 downto 8) and csr0_r(9 downto 8)) /= "00")
                  else '0';
    cnv_mode_i <= csr0_r(10);
    -- output
    bc_int     <= bc_int_i;
    bc_error   <= bc_error_i;
    cnv_mode   <= cnv_mode_i;
    csr2       <= u2slv(csr2_r);
    csr3       <= u2slv(csr3_r);
    tsm_word   <= u2slv(tsm_word_r);
    us_ratio   <= u2slv(us_ratio_r);
    -- Valid for 2 clock cycles
    fmdd_cmd   <= u2slv(fmdd_cmd_i or fmdd_cmd_r);
    hold_wait  <= u2slv(hold_wait_r);        -- FMD: Wait to hold
    l1_timeout <= u2slv(l1_timeout_r);       -- FMD: L1 timeout
    l2_timeout <= u2slv(l2_timeout_r);       -- FMD: L2 timeout
    shift_div  <= u2slv(shift_div_r);        -- FMD: Shift clk
    strips     <= u2slv(strips_r);           -- FMD: Strips
    cal_level  <= u2slv(cal_level_r);        -- FMD: Cal pulse
    cal_iter   <= u2slv(cal_iter_r);         -- FMD: Cal events
    shape_bias <= u2slv(shape_bias_r);       -- FMD: Shape bias
    vfs        <= u2slv(vfs_r);              -- FMD: Shape ref
    vfp        <= u2slv(vfp_r);              -- FMD: Preamp ref
    sample_div <= u2slv(sample_div_r);       -- FMD: Sample clk
    mebs       <= u2slv(meb_r(8 downto 4));  -- MEB: Config

  end block combi;
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package registers_block_pack is
  component registers_block
    port (
      clk         : in  std_logic;
      rstb        : in  std_logic;
      add_adc     : in  std_logic_vector(4 downto 0);
      we_adc      : in  std_logic;
      data_adc    : in  std_logic_vector(15 downto 0);
      add         : in  std_logic_vector(6 downto 0);
      we          : in  std_logic;
      din         : in  std_logic_vector(15 downto 0);
      add_al      : in  std_logic_vector(6 downto 0);
      add_sc      : in  std_logic_vector(6 downto 0);
      dout_al     : out std_logic_vector(15 downto 0);
      dout_sc     : out std_logic_vector(15 downto 0);
      hadd        : in  std_logic_vector(4 downto 0);
      dstbcnt_in  : in  std_logic_vector(7 downto 0);
      l0cnt_in    : in  std_logic_vector(15 downto 0);
      l1cnt_in    : in  std_logic_vector(15 downto 0);
      l2cnt_in    : in  std_logic_vector(15 downto 0);
      sclkcnt_in  : in  std_logic_vector(15 downto 0);
      par_error   : in  std_logic;
      paps_error  : in  std_logic;
      alps_error  : in  std_logic;
      al_error    : in  std_logic;
      missed_sclk : in  std_logic;
      ierr_sc     : in  std_logic;
      ierr_al     : in  std_logic;
      cnt_lat     : in  std_logic;
      cnt_clr     : in  std_logic;
      csr1_clr    : in  std_logic;
      bc_rst      : in  std_logic;
      end_seq     : in  std_logic;
      st_cnv      : in  std_logic;
      fmdd_stat   : in  std_logic_vector(15 downto 0);
      cnv_mode    : out std_logic;
      csr2        : out std_logic_vector(15 downto 0);
      csr3        : out std_logic_vector(15 downto 0);
      tsm_word    : out std_logic_vector(8 downto 0);
      us_ratio    : out std_logic_vector(15 downto 0);
      bc_int      : out std_logic;
      bc_error    : out std_logic;
      hold_wait   : out std_logic_vector(15 downto 0);
      l1_timeout  : out std_logic_vector(15 downto 0);
      l2_timeout  : out std_logic_vector(15 downto 0);
      shift_div   : out std_logic_vector(15 downto 0);
      strips      : out std_logic_vector(15 downto 0);
      cal_level   : out std_logic_vector(15 downto 0);
      shape_bias  : out std_logic_vector(15 downto 0);
      vfs         : out std_logic_vector(15 downto 0);
      vfp         : out std_logic_vector(15 downto 0);
      sample_div  : out std_logic_vector(15 downto 0);
      fmdd_cmd    : out std_logic_vector(15 downto 0);
      cal_iter    : out std_logic_vector(15 downto 0);
      mebs        : out std_logic_vector(4 downto 0);
      meb_cnt     : in  std_logic_vector(3 downto 0);
      cal_delay   : out std_logic_vector(15 downto 0));
  end component;
end registers_block_pack;


------------------------------------------------------------------------------
--
-- EOF
--
