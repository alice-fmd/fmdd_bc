------------------------------------------------------------------------------
-- Title      : Handle register I/O
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : registers_block.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2008-08-15
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Component to handle register I/O
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity int_mstable is
  generic (TIMES : integer range 2 to 8 := 3;  -- Times out of bounds before int
           WIDTH : natural              := 10;
           OVER  : boolean              := true;   -- Over/under threshold
           AUTO  : boolean              := true);  -- Auto clear interrupt
  port (clk       : in  std_logic;      -- Clock
        rstb      : in  std_logic;      -- Async. reset
        rst       : in  std_logic;      -- Sync. reset
        enable    : in  std_logic;      -- Make comparison
        value     : in  std_logic_vector(WIDTH-1 downto 0);  -- Value
        threshold : in  std_logic_vector(WIDTH-1 downto 0);  -- Threshold
        int       : out std_logic);     -- Interrupt
end int_mstable;

------------------------------------------------------------------------------  
architecture rtl of int_mstable is
  signal out_of_bounds_i : std_logic_vector(TIMES-1 downto 0) := (others =>'0');
  constant BAD : std_logic_vector(TIMES-1 downto 0) := (others => '1');
begin  -- rtl
  -- purpose: Collect combinatorics
  combinatorics : block
  begin  -- block combinatorics
    int   <= '1' when (out_of_bounds_i = BAD) else '0';
  end block combinatorics;

  -- purpose: Check value against threshold on enables, and shift down
  -- type   : sequential
  -- inputs : clk, rstb, enable
  -- outputs: out_of_bounds
  checker: process (clk, rstb)
    variable val_i : unsigned(WIDTH-1 downto 0) := (others => '0');
    variable thr_i : unsigned(WIDTH-1 downto 0) := (others => '0');
  begin  -- process checker
    if rstb = '0' then                  -- asynchronous reset (active low)
      out_of_bounds_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if rst = '1' then
        out_of_bounds_i <= (others => '0');
      elsif enable = '1' then
        val_i := unsigned(value);
        thr_i := unsigned(threshold);
        out_of_bounds_i(TIMES-1 downto 1) <= out_of_bounds_i(TIMES-2 downto 0);
        if AUTO then
          out_of_bounds_i(0) <= '0';
        end if;
        if OVER then
          if val_i > thr_i then
            out_of_bounds_i(0) <= '1';
          end if;
        else
          if val_i < thr_i then
            out_of_bounds_i(0) <= '1';
          end if;          
        end if;
      end if;
    end if;
  end process checker;
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package int_mstable_pack is
  component int_mstable
    generic (TIMES : integer range 2 to 8;  -- Times out of bounds before int
             WIDTH : natural;
             OVER  : boolean;               -- Over/under threshold
             AUTO  : boolean);              -- Auto clear interrupt
    port (clk       : in  std_logic;    -- Clock
          rstb      : in  std_logic;    -- Async. reset
          rst       : in  std_logic;    -- Sync. reset
          enable    : in  std_logic;    -- Make comparison
          value     : in  std_logic_vector(WIDTH-1 downto 0);  -- Value
          threshold : in  std_logic_vector(WIDTH-1 downto 0);  -- Threshold
          int       : out std_logic);   -- Interrupt
  end component;
end int_mstable_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
