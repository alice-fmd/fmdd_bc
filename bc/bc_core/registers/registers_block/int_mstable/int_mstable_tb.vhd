-------------------------------------------------------------------------------
-- Title      : Testbench for design "int_mstable"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : int_mstable_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Created    : 2008-08-10
-- Last update: 2008-08-15
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2008 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2008-08-10  1.0      cholm	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.int_mstable_pack.all;

-------------------------------------------------------------------------------
entity int_mstable_tb is
end int_mstable_tb;

-------------------------------------------------------------------------------

architecture test of int_mstable_tb is
  constant PERIOD        : time := 25 ns;
  constant ENABLE_PERIOD : time := 100 ns;
  
  -- component generics
  constant TIMES : integer := 2;     -- How many times it must be out of range
  constant OVER  : boolean := true;     -- Over/under threshold
  constant WIDTH : natural := 10;       -- Width of registers
  constant AUTO  : boolean := true;     -- Number of inputs

  -- component ports
  signal clk_i        : std_logic := '0';  -- [in]  Clock
  signal rstb_i       : std_logic := '0';  -- [in]  Async Reset
  signal rst_i        : std_logic := '0';  -- [in]  Sync. reset
  signal enable_i     : std_logic := '0';  -- [in]  Enable comparison
  signal int_i        : std_logic_vector(2 downto 0);

  subtype val_t is std_logic_vector(WIDTH-1 downto 0);
  type vals_t is array (natural range <>) of val_t;

  signal val_i : vals_t(2 downto 0) := ((others => '0'),
                                        (others => '0'),
                                        (others => '0'));
  signal thr_i : vals_t(2 downto 0) := ((others => '1'),
                                        (others => '1'),
                                        (others => '1'));
  
  -- purpose: Convert int to 10bit std_logic_vector
  function int2slv (constant x : integer)
    return std_logic_vector is
  begin  -- int2slv
    return std_logic_vector(to_unsigned(x, 10));
  end int2slv;

begin  -- test

  duts : for i in 0 to 2 generate
    filter_mstable_1 : int_mstable
      generic map (TIMES => TIMES,      -- Times out of bounds before int
                   WIDTH => WIDTH,
                   OVER  => OVER,       -- Over/under threshold
                   AUTO  => AUTO)       -- Auto clear interrupt
      port map (clk       => clk_i,     -- [in]  Clock
                rstb      => rstb_i,    -- [in]  Async. reset
                rst       => rst_i,     -- [in]  Sync. reset
                enable    => enable_i,  -- [in]  Make comparison
                value     => val_i(i),  -- [in]  Value
                threshold => thr_i(i),  -- [in]  Threshold
                int       => int_i(i));      -- [out] Interrupt
  end generate duts;

  -- clock generation
  clk_i           <= not clk_i after PERIOD / 2;
  
  -- waveform generation
  stim: process
  begin
    wait for 100 ns;
    rstb_i <= '1';

    wait for 100 ns;
    val_i(0) <= int2slv(100);
    thr_i(0) <= int2slv(50);

    wait for 4 * ENABLE_PERIOD;
    thr_i(0) <= int2slv(200);
    
    wait; -- for ever;
  end process stim;

  -- purpose: Periodically enable
  -- type   : combinational
  -- inputs : 
  -- outputs: enable_i
  enab: process
  begin  -- process enab
    wait for ENABLE_PERIOD;
    wait until rising_edge(clk_i);
    wait for 3 * PERIOD / 4;
    enable_i <= '1', '0' after PERIOD;
  end process enab;
end test;

-------------------------------------------------------------------------------
--
-- EOF
--
