------------------------------------------------------------------------------
-- Title      : Data strobe counter
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : dstb_counter.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Counts data strobes from ALTRO's
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dstb_counter is
  port (
    clk      : in  std_logic;                      -- Clock
    rstb     : in  std_logic;                      -- Async reset
    cnt_clr  : in  std_logic;                      -- sync reset
    al_trsf  : in  std_logic;                      -- Transfer from ALTRO's
    dstb     : in  std_logic;                      -- Data strobe from ALTRO's
    dstb_cnt : out std_logic_vector(7 downto 0));  -- Counts
end dstb_counter;

------------------------------------------------------------------------------
architecture rtl of dstb_counter is
  type state is (s0, s1, s2, s3);          -- States
  signal st_i     : state;                 -- State
  signal nx_st_i  : state;                 -- Next state
  signal enable_i : std_logic;             -- Enable output
  signal clear_i  : std_logic;             -- Clear counter;
  signal cnt_i    : unsigned(7 downto 0);  -- Counter

begin  -- rtl
  -- purpose: Switch to next state
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  next_state: process (clk, rstb)
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= s0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;
  
  -- purpose: Get the next state
  -- type   : combinational
  -- inputs : st_i, al_trsf
  -- outputs: 
  fsm: process (st_i, al_trsf)
  begin  -- process fsm
    nx_st_i <= st_i;

    case st_i is
      when s0 =>
        enable_i  <= '0';
        clear_i   <= '0';
        if al_trsf = '1' then
          nx_st_i <= s1;
        else
          nx_st_i <= s0;
        end if;

      when s1 =>
        enable_i  <= '0';
        clear_i   <= '0';
        if al_trsf = '0' then
          nx_st_i <= s2;
        else
          nx_st_i <= s1;
        end if;

      when s2 =>
        enable_i <= '1';
        clear_i  <= '0';
        nx_st_i <=  s3;

      when s3 =>
        enable_i <=  '0';
        clear_i <= '1';
        nx_st_i <= s0;

      when others => 
        enable_i <= '0';
        clear_i  <= '0';
        nx_st_i  <= s0;
    end case;
  end process fsm;

  -- purpose: Output count
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: dstb_cnt
  output: process (clk, rstb)
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      dstb_cnt <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if cnt_clr = '1' then
        dstb_cnt <= (others => '0');
      elsif enable_i = '1' then
        dstb_cnt <= std_logic_vector(cnt_i);
      end if;
    end if;
  end process output;

  -- purpose: count dstb's
  -- type   : sequential
  -- inputs : dstb, clear_i, al_trsf
  -- outputs: cnt_i
  counter: process (dstb, clear_i, rstb)
  begin  -- process counter
    if clear_i = '1' or rstb = '0' then
      cnt_i <= (others => '0');
    elsif dstb'event and dstb = '1' then  -- rising clock edge
      if al_trsf = '1' then
        if cnt_i = X"FF" then
          cnt_i <= (others => '0');
        else
          cnt_i <= cnt_i + 1;
        end if;
      end if;
    end if;
  end process counter;
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package dstb_counter_pack is
  component dstb_counter
    port (
      clk      : in  std_logic;
      rstb     : in  std_logic;
      cnt_clr  : in  std_logic;
      al_trsf  : in  std_logic;
      dstb     : in  std_logic;
      dstb_cnt : out std_logic_vector(7 downto 0));
  end component;
end dstb_counter_pack;
------------------------------------------------------------------------------
--
-- EOF
--
