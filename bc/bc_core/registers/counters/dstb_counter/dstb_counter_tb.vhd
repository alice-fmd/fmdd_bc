------------------------------------------------------------------------------
--
-- Test bench for DSTB counter
library ieee;
use ieee.std_logic_1164.all;
use work.dstb_counter_pack.all;

------------------------------------------------------------------------------
entity dstb_counter_tb is
end dstb_counter_tb;

------------------------------------------------------------------------------
architecture test of dstb_counter_tb is
  constant PERIOD     : time      := 25 ns;
  signal   clk_i      : std_logic := '0';
  signal   rstb_i     : std_logic := '1';
  signal   cnt_clr_i  : std_logic := '0';
  signal   al_trsf_i  : std_logic := '0';
  signal   dstb_i     : std_logic := '0';
  signal   dstb_cnt_i : std_logic_vector(7 downto 0);
  signal   i          : integer;
begin  -- test

  DUT: dstb_counter
    port map (
        clk      => clk_i,
        rstb     => rstb_i,
        cnt_clr  => cnt_clr_i,
        al_trsf  => al_trsf_i,
        dstb     => dstb_i,
        dstb_cnt => dstb_cnt_i);

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    rstb_i  <= '0';
    wait for 100 ns;
    rstb_i  <= '1';
    wait for 4 * PERIOD;

    wait until rising_edge(clk_i);
    al_trsf_i <= '1';
    for i in 0 to 20 loop
      wait for 2 * PERIOD / 3;
      dstb_i <= '1';
      wait for PERIOD;
      dstb_i <= '0';
    end loop;  -- i
    wait until rising_edge(clk_i);
    al_trsf_i <= '0';

    wait;                               -- forever 
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
  

end test;

------------------------------------------------------------------------------
