------------------------------------------------------------------------------
--
-- Test bench for trigger counter
--
library ieee;
use ieee.std_logic_1164.all;
use work.triggercounter_pack.all;

------------------------------------------------------------------------------
entity triggercounter_tb is
end triggercounter_tb;

------------------------------------------------------------------------------

architecture test of triggercounter_tb is
  constant PERIOD    : time    := 25 ns;  -- A clock cycle
  constant MAX : integer := 2**16+1;
  signal   clk_i     : std_logic := '0';
  signal   rstb_i    : std_logic := '0';
  signal   clr_i     : std_logic := '0';
  signal   trg_i     : std_logic := '0';
  signal   trg_cnt_i : std_logic_vector(15 downto 0);
  signal   i         : integer := 0;      -- Count up
  
  -- purpose: Make a trigger to be counted
  procedure make_trg (
    signal clk : in  std_logic;         -- Clock
    signal trg : out std_logic) is      -- Trigger
  begin  -- make_trg
    wait until rising_edge(clk);
    trg <= '1';
    wait for 3 * PERIOD;
    trg <= '0';
    wait until rising_edge(clk);
  end make_trg;

  -- purpose: Clear counter
  procedure clr_cnt (
    signal clk : in std_logic;          -- Clock
    signal clr : out std_logic) is   -- Clear
  begin  -- clr_cnt
    wait until rising_edge(clk);
    clr <= '1';
    wait for 2 * PERIOD;
    clr <= '0';
    wait until rising_edge(clk);    
  end clr_cnt;
begin  -- test
  DUT                : triggercounter
    port map (
      clk     => clk_i,
      rstb    => rstb_i,
      clr     => clr_i,
      trg     => trg_i,
      trg_cnt => trg_cnt_i);

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli : process
  begin  -- process stimuli
    rstb_i <= '0';
    clr_i  <= '0';
    wait for 100 ns;
    rstb_i <= '1';
    wait for 4 * PERIOD;
    wait until rising_edge(clk_i);


    for i  in 0 to 4 loop
      make_trg(clk => clk_i, trg => trg_i);
    end loop;  -- i

    clr_cnt(clk => clk_i, clr => clr_i);
               
    wait for 4 * PERIOD;

    for i  in 0 to MAX loop
      make_trg(clk => clk_i, trg => trg_i);
    end loop;  -- i
    
    wait;
  end process stimuli;
  
  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;

end test;

------------------------------------------------------------------------------
--
-- EOF
--
