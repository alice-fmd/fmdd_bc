------------------------------------------------------------------------------
-- Title      : Count a trigger
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : triggercounter.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Counts triggers.  Note, that there's a glitch filter to ensure
--              that a trigger is at least 75 ns long (3 clock cycles).
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity triggercounter is
  port (
    clk     : in  std_logic;                       -- Clock
    rstb    : in  std_logic;                       -- Async reset
    clr     : in  std_logic;                       -- Sync reset
    trg     : in  std_logic;                       -- Trigger
    trg_cnt : out std_logic_vector(15 downto 0));  -- Counter
end triggercounter;

------------------------------------------------------------------------------
architecture rtl of triggercounter is
  signal reg1_i    : std_logic;         -- First register
  signal reg2_i    : std_logic;         -- Second registers
  signal reg3_i    : std_logic;         -- Third register
  signal enable_i  : std_logic;         -- Enable for counter
  signal last_i    : std_logic;
  signal counter_i : unsigned(15 downto 0);
begin  -- rtl
  -- purpose: Combinatorics
  combi            : block
  begin  -- block combi
    last_i   <= not (reg1_i and reg2_i);
    enable_i <= not (last_i or reg3_i);
    trg_cnt  <= std_logic_vector(counter_i);
  end block combi;

  -- Meta-stability and glitch filter 
  meta_glitch_filter: process (clk, rstb)
  begin  -- process meta_glitch_filter
    if rstb = '0' then                  -- asynchronous reset (active low)
      reg1_i <= '0';
      reg2_i <= '0';
      reg3_i <= '0';
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      reg1_i <= trg;
      reg2_i <= reg1_i;
      reg3_i <= reg2_i;
    end if;
  end process meta_glitch_filter;

  -- purpose: Count triggers
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  count_ti: process (clk, rstb)
  begin  -- process count_ti
    if rstb = '0' then                  -- asynchronous reset (active low)
      counter_i <= X"0000";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if enable_i = '1' then
        if counter_i = X"FFFF" then
          report "Would over flow, going back to 0" severity note;
          counter_i <= X"0000";
        else
          counter_i <= counter_i + 1;          
        end if;
      elsif clr = '1' then
        counter_i <= X"0000";
      end if;
    end if;
  end process count_ti;
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package triggercounter_pack is
  component triggercounter
    port (
      clk     : in  std_logic;
      rstb    : in  std_logic;
      clr     : in  std_logic;
      trg     : in  std_logic;
      trg_cnt : out std_logic_vector(15 downto 0));
  end component;
end triggercounter_pack;
------------------------------------------------------------------------------
--
-- EOF
--
