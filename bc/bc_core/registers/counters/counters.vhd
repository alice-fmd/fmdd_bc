------------------------------------------------------------------------------
-- Title      : Container of counters
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : counters.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2008-08-20
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Just a simple container of counters
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------

------------------------------------------------------------------------------
--
-- Various counters
--
library ieee;
use ieee.std_logic_1164.all;
use work.triggercounter_pack.all;
use work.dstb_counter_pack.all;
use work.sclk_counter_pack.all;

entity counters is
  port (
    clk         : in  std_logic;                      -- Clock
    sclk        : in  std_logic;                      -- Slow clock
    rstb        : in  std_logic;                      -- Async reset
    clr_cnt     : in  std_logic;                      -- Sync reset
    l0_trg      : in  std_logic;                      -- L0 trigger
    l1_trg      : in  std_logic;                      -- L1 trigger
    l2_trg      : in  std_logic;                      -- L2 trigger
    csr3        : in  std_logic_vector(7 downto 0);   -- Config/Status 3
    adcclk_en   : in  std_logic;                      -- ADC clock enable
    dstb        : in  std_logic;                      -- Data strobe
    al_trsf     : in  std_logic;                      -- Transfer from ALTRO\s
    l0_cnt      : out std_logic_vector(15 downto 0);  -- L0 counter
    l1_cnt      : out std_logic_vector(15 downto 0);  -- L1 counter
    l2_cnt      : out std_logic_vector(15 downto 0);  -- L2 counter
    sclk_cnt    : out std_logic_vector(15 downto 0);  -- Slow clock counter
    missed_sclk : out std_logic;                      -- Missed sclk alarm
    dstb_cnt    : out std_logic_vector(7 downto 0));  -- data strobe counter
end counters;

-------------------------------------------------------------------------------
architecture rtl2 of counters is
  signal l0b_i   : std_logic;
  signal rpinc_i : std_logic;
begin  -- rtl2
  l0b_i       <= not l0_trg;            -- Invert :-)
  rpinc_i     <= not sclk;              -- Invert :-)
  missed_sclk <= '0';                   -- always false
  
  l0_counter : triggercounter
    port map (
        clk     => clk,
        rstb    => rstb,
        clr     => clr_cnt,
        trg     => l0b_i,
        trg_cnt => l0_cnt);

  l1_counter: triggercounter
    port map (
        clk     => clk,
        rstb    => rstb,
        clr     => clr_cnt,
        trg     => l1_trg,
        trg_cnt => l1_cnt);
  
  l2_counter: triggercounter
    port map (
        clk     => clk,
        rstb    => rstb,
        clr     => clr_cnt,
        trg     => l2_trg,
        trg_cnt => l2_cnt);

  rpinc_counter: triggercounter
    port map (
        clk     => clk,
        rstb    => rstb,
        clr     => clr_cnt,
        trg     => rpinc_i,
        trg_cnt => sclk_cnt);

  data_counter: dstb_counter
    port map (
        clk      => clk,
        rstb     => rstb,
        cnt_clr  => clr_cnt,
        al_trsf  => al_trsf,
        dstb     => dstb,
        dstb_cnt => dstb_cnt);
end rtl2;

------------------------------------------------------------------------------
architecture rtl of counters is
  signal l0b_i : std_logic;
begin  -- rtl
  l0b_i <= not l0_trg;         -- Invert :-)

  l0_counter : triggercounter
    port map (
        clk     => clk,
        rstb    => rstb,
        clr     => clr_cnt,
        trg     => l0b_i,
        trg_cnt => l0_cnt);

  l1_counter: triggercounter
    port map (
        clk     => clk,
        rstb    => rstb,
        clr     => clr_cnt,
        trg     => l1_trg,
        trg_cnt => l1_cnt);
  
  l2_counter: triggercounter
    port map (
        clk     => clk,
        rstb    => rstb,
        clr     => clr_cnt,
        trg     => l2_trg,
        trg_cnt => l2_cnt);

  slow_counter: entity work.sclk_counter(rtl)
    port map (
        rclk        => clk,
        sclk        => sclk,
        rstb        => rstb,
        cnt_clr     => clr_cnt,
        csr3        => csr3,
        adcclk_en   => adcclk_en,
        sclk_cnt    => sclk_cnt,
        missed_sclk => missed_sclk);

  data_counter: dstb_counter
    port map (
        clk      => clk,
        rstb     => rstb,
        cnt_clr  => clr_cnt,
        al_trsf  => al_trsf,
        dstb     => dstb,
        dstb_cnt => dstb_cnt);
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package counters_pack is
  component counters
    port (
      clk         : in  std_logic;      -- Clock
      sclk        : in  std_logic;      -- Slow clock
      rstb        : in  std_logic;      -- Async reset
      clr_cnt     : in  std_logic;      -- Sync reset
      l0_trg      : in  std_logic;      -- L0 trigger
      l1_trg      : in  std_logic;      -- L1 trigger
      l2_trg      : in  std_logic;      -- L2 trigger
      csr3        : in  std_logic_vector(7 downto 0);  -- Config/Status 3
      adcclk_en   : in  std_logic;      -- ADC clock enable
      dstb        : in  std_logic;      -- Data strobe
      al_trsf     : in  std_logic;      -- Transfer from ALTROs
      l0_cnt      : out std_logic_vector(15 downto 0);  -- L0 counter
      l1_cnt      : out std_logic_vector(15 downto 0);  -- L1 counter
      l2_cnt      : out std_logic_vector(15 downto 0);  -- L2 counter
      sclk_cnt    : out std_logic_vector(15 downto 0);  -- Slow clock counter
      missed_sclk : out std_logic;      -- Missed sclk alarm
      dstb_cnt    : out std_logic_vector(7 downto 0));  -- data strobe counter
  end component counters;
end counters_pack;
------------------------------------------------------------------------------
--
-- EOF
--
