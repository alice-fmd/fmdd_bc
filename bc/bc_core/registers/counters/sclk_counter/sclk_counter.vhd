------------------------------------------------------------------------------
-- Title      : Count slow (10MHz) clock cycles
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : sclk_counter.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2008-08-20
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Count number of fast (40MHz) clock cycles per slow clock
--              (10MHz) clock cycle.  Raise error if too many fast clocks was
--              counted 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sclk_counter is
  port (
    rclk        : in  std_logic;                      -- Fast (40MHz) clock
    sclk        : in  std_logic;                      -- Slow (10MHz) clock
    rstb        : in  std_logic;                      -- Async reset
    cnt_clr     : in  std_logic;                      -- Clear (sync)
    csr3        : in  std_logic_vector(7 downto 0);   -- Config/Status 3
    adcclk_en   : in  std_logic;                      -- ADC clock enable
    sclk_cnt    : out std_logic_vector(15 downto 0);  -- Counter
    missed_sclk : out std_logic);                     -- Error flag
end sclk_counter;

------------------------------------------------------------------------------
architecture rtl2 of sclk_counter is
  signal clk_counter_i  : unsigned(7 downto 0);   -- Clock counter
  signal sclk_counter_i : unsigned(15 downto 0);  -- Slow clock counter
  signal maxed_i        : std_logic;    -- Clock counter at max (csr3)
  signal sclk_clr_i     : std_logic;    -- Slow clock counter clear
  signal sclk_reg_i     : std_logic;    -- Registered sclk
  signal sclk_reg_ii    : std_logic;    -- Registered old sclk
  signal sclk_edge_i    : std_logic;    -- Slow clock edge;
begin  -- rtl
  -- purpose: Combinatorics
  combi : block
  begin  -- block combi
    maxed_i     <= '1' when std_logic_vector(clk_counter_i) = csr3 else '0';
    missed_sclk <= maxed_i and adcclk_en;

    -- Used to be - won't work properly
    -- sclk_edge_i <= sclk_reg_i xor sclk;
    sclk_edge_i <= sclk_reg_i xor sclk_reg_ii;
    sclk_clr_i  <= rstb and not cnt_clr;

    sclk_cnt <= std_logic_vector(sclk_counter_i);
  end block combi;

  -- purpose: sclk edge detector
  -- type   : sequential
  -- inputs : rclk, rstb
  -- outputs: 
  sclk_edge : process (rclk, rstb)
  begin  -- process sclk_edge
    if rstb = '0' then                    -- asynchronous reset (active low)
      sclk_reg_i <= '0';
    elsif rclk'event and rclk = '1' then  -- rising clock edge
      sclk_reg_i  <= sclk;
      sclk_reg_ii <= sclk_reg_i;
    end if;
  end process sclk_edge;

  -- purpose: Count fast clocks
  -- type   : sequential
  -- inputs : rclk, sclk_edge_i
  -- outputs: 
  clk_count : process (rclk, sclk_edge_i)
  begin  -- process clk_cnt
    if sclk_edge_i = '1' then             -- asynchronous reset (active low)
      clk_counter_i   <= X"00";
    elsif rclk'event and rclk = '1' then  -- rising clock edge
      if clk_counter_i = X"FF" then
        clk_counter_i <= X"00";
      else
        clk_counter_i <= clk_counter_i + 1;
      end if;
    end if;
  end process clk_count;

  -- purpose: Count slow clocks
  -- type   : sequential
  -- inputs : sclk, sclk_clr_i
  -- outputs: 
  sclk_count : process (sclk, sclk_clr_i)
  begin  -- process sclk_cnt
    if sclk_clr_i = '0' then              -- asynchronous reset (active low)
      sclk_counter_i     <= X"0000";
    elsif sclk'event and sclk = '1' then  -- rising clock edge
      if sclk_counter_i = X"FFFF" then
        sclk_counter_i <= X"0000";
      else
        sclk_counter_i <= sclk_counter_i + 1;
      end if;
    end if;
  end process sclk_count;
end rtl2;

------------------------------------------------------------------------------
architecture rtl of sclk_counter is
  signal clk_counter_i  : unsigned(7 downto 0);   -- Clock counter
  signal sclk_counter_i : unsigned(15 downto 0);  -- Slow clock counter
  signal maxed_i        : std_logic;    -- Clock counter at max (csr3)
  signal sclk_clr_i     : std_logic;    -- Slow clock counter clear
  signal sclk_reg_i     : std_logic;    -- Registered sclk
  signal sclk_reg_ii    : std_logic;    -- Registered old sclk
  signal sclk_edge_i    : std_logic;    -- Slow clock edge;
begin  -- rtl
  -- purpose: Combinatorics
  combi : block
  begin  -- block combi
    maxed_i     <= '1' when std_logic_vector(clk_counter_i) >= csr3 else '0';
    missed_sclk <= maxed_i and adcclk_en;
    sclk_cnt    <= std_logic_vector(sclk_counter_i);
  end block combi;

  -- purpose: sclk edge detector
  -- type   : sequential
  -- inputs : rclk, rstb
  -- outputs: 
  sclk_edge : process (rclk, rstb)
  begin  -- process sclk_edge
    if rstb = '0' then                    -- asynchronous reset (active low)
      sclk_reg_i <= '0';
    elsif rclk'event and rclk = '1' then  -- rising clock edge
      sclk_reg_i  <= sclk;
      sclk_reg_ii <= sclk_reg_i;
      sclk_edge_i <= sclk_reg_i xor sclk_reg_ii;
    end if;
  end process sclk_edge;

  -- purpose: Count fast clocks
  -- type   : sequential
  -- inputs : rclk, sclk_edge_i
  -- outputs: 
  clk_count : process (rclk, rstb)
  begin  -- process clk_cnt
    if rstb = '0' then             -- asynchronous reset (active low)
      clk_counter_i   <= X"00";
    elsif rclk'event and rclk = '1' then  -- rising clock edge
      if sclk_edge_i = '1' then             -- asynchronous reset (active low)
        clk_counter_i   <= X"00";
      elsif clk_counter_i = X"FF" then
        clk_counter_i <= X"00";
      else
        clk_counter_i <= clk_counter_i + 1;
      end if;
    end if;
  end process clk_count;

  -- purpose: Count slow clocks
  -- type   : sequential
  -- inputs : sclk, sclk_clr_i
  -- outputs: 
  sclk_count : process (sclk, rstb)
  begin  -- process sclk_cnt
    if rstb = '0' then              -- asynchronous reset (active low)
      sclk_counter_i     <= X"0000";
    elsif sclk'event and sclk = '1' then  -- rising clock edge
      if cnt_clr = '1' then              -- asynchronous reset (active low)
        sclk_counter_i     <= X"0000";
      elsif sclk_counter_i = X"FFFF" then
        sclk_counter_i <= X"0000";
      else
        sclk_counter_i <= sclk_counter_i + 1;
      end if;
    end if;
  end process sclk_count;
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package sclk_counter_pack is
  component sclk_counter
    port (
      rclk        : in  std_logic;
      sclk        : in  std_logic;
      rstb        : in  std_logic;
      cnt_clr     : in  std_logic;
      csr3        : in  std_logic_vector(7 downto 0);
      adcclk_en   : in  std_logic;
      sclk_cnt    : out std_logic_vector(15 downto 0);
      missed_sclk : out std_logic);
  end component;
end sclk_counter_pack;

------------------------------------------------------------------------------
--
-- EOF
--
