------------------------------------------------------------------------------
--
-- Test bench for slow clock counter
--                                      
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sclk_counter_pack.all;

------------------------------------------------------------------------------
entity sclk_counter_tb is
end sclk_counter_tb;

------------------------------------------------------------------------------
architecture test of sclk_counter_tb is
  constant PERIOD        : time                         := 25 ns;
  constant DIV           : unsigned(7 downto 0)         := X"08";
  signal   rclk_i        : std_logic                    := '0';
  signal   sclk_i        : std_logic                    := '0';
  signal   rstb_i        : std_logic                    := '1';
  signal   cnt_clr_i     : std_logic                    := '0';
  signal   csr3_i        : std_logic_vector(7 downto 0) := "00100000";
  signal   adcclk_en_i   : std_logic                    := '1';
  signal   sclk_cnt_i    : std_logic_vector(15 downto 0);
  signal   missed_sclk_i : std_logic;
  signal   counter_i     : unsigned(7 downto 0)         := DIV;
  signal   inhibit_i     : std_logic                    := '0';
begin  -- test

  DUT: sclk_counter
    port map (
        rclk        => rclk_i,
        sclk        => sclk_i,
        rstb        => rstb_i,
        cnt_clr     => cnt_clr_i,
        csr3        => csr3_i,
        adcclk_en   => adcclk_en_i,
        sclk_cnt    => sclk_cnt_i,
        missed_sclk => missed_sclk_i);

  -- purpose: Provide stimule
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process
  begin  -- process stimuli
    rstb_i <= '0';
    wait for 4 * PERIOD;
    rstb_i <= '1';

    wait for 4 * 8 * PERIOD;
    inhibit_i <= '1';
    wait until missed_sclk_i = '1';
    wait until rising_edge(rclk_i);
    cnt_clr_i <= '1';
    wait until rising_edge(rclk_i);
    cnt_clr_i <= '0';
    wait until rising_edge(rclk_i);
    rstb_i <= '0';
    wait until rising_edge(rclk_i);
    rstb_i <= '1';
    
    wait for 10 * 8 * PERIOD;
    inhibit_i <= '0';
    
    wait until sclk_cnt_i = csr3_i;

    wait; -- forever
  end process stimuli;

  -- purpose: make slow clock
  -- type   : sequential
  sclock_gen: process (rclk_i, inhibit_i)
  begin  -- process sclock_gen
    if inhibit_i = '1' then
      counter_i <= DIV;
    elsif rclk_i'event and rclk_i = '1' then  -- rising clock edge
      if counter_i > DIV / 2 + 1 then
        sclk_i    <= '0';
        counter_i <= counter_i - 1;
      elsif counter_i > 1 then
        sclk_i    <= '1';
        counter_i <= counter_i - 1;
      else
        sclk_i    <= '0';
        counter_i <= DIV;
      end if;
    end if;
  end process sclock_gen;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    rclk_i        <= not rclk_i;
  end process clock_gen;

end test;

------------------------------------------------------------------------------
