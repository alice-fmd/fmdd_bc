------------------------------------------------------------------------------
-- Title      : Container of registers
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : registers.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2009-04-16
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Container of registers
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.registers_block_pack.all;
use work.counters_pack.all;

entity registers is
  port (
    clk         : in  std_logic;        -- Clock
    sclk        : in  std_logic;        -- Slow (10Mhz) clock
    rstb        : in  std_logic;        -- Async reset
    -- Write
    we          : in  std_logic;        -- Write enable
    wadd        : in  std_logic_vector(6 downto 0);   -- Address 
    wdata       : in  std_logic_vector(15 downto 0);  -- Input data
    -- Read
    add_al      : in  std_logic_vector(6 downto 0);
    add_sc      : in  std_logic_vector(6 downto 0);
    dout_al     : out std_logic_vector(15 downto 0);
    dout_sc     : out std_logic_vector(15 downto 0);
    -- Monitor
    end_seq     : in  std_logic;        -- End of Monitor flag
    we_adc      : in  std_logic;        -- Monitor adc write enable
    add_adc     : in  std_logic_vector(4 downto 0);   -- Monitor ADC address
    data_adc    : in  std_logic_vector(15 downto 0);  -- Monitor data
    cnv_mode    : out std_logic;        -- Continous conversion flag
    -- Input for registers
    fmdd_stat   : in  std_logic_vector(15 downto 0);  -- FMDD status
    l0          : in  std_logic;        -- L0 trigger (for counter)
    l1_trg      : in  std_logic;        -- L1 trigger
    l2_trg      : in  std_logic;        -- L2 trigger
    dstb        : in  std_logic;        -- Data strobe
    al_trsf     : in  std_logic;        -- Data transfer
    par_error   : in  std_logic;        -- Error: Parity 
    paps_error  : in  std_logic;        -- Error: PASA power sup. error
    alps_error  : in  std_logic;        -- Error: ALTRO power sup. error
    ierr_sc     : in  std_logic;        -- Error: instruction from I2C
    ierr_al     : in  std_logic;        -- Error: instruction from bus
    al_error    : in  std_logic;        -- Error: from ALTRO
    bc_rst      : in  std_logic;        -- Command: Reset BC
    csr1_clr    : in  std_logic;        -- Command: Clear Config/status 1
    cnt_lat     : in  std_logic;        -- Command: Latch counters
    cnt_clr     : in  std_logic;        -- Command: Clear counters
    st_cnv      : in  std_logic;        -- Command: start conversion
    -- Misc
    hadd        : in  std_logic_vector(4 downto 0);   -- Card addreess
    -- Output of registers
    missed_sclk : out std_logic;        -- Error: Missed slow clock
    bc_int      : out std_logic;        -- Error: BC interrupt
    bc_error    : out std_logic;        -- Error : BC error
    csr2        : out std_logic_vector(15 downto 0);  -- Config/Status 2
    csr3        : out std_logic_vector(15 downto 0);  -- Config/Status 3
    tsm_word    : out std_logic_vector(8 downto 0);   -- Test mode word
    us_ratio    : out std_logic_vector(15 downto 0);  -- Under sampling ratio
    hold_wait   : out std_logic_vector(15 downto 0);  -- FMD: Wait to hold
    l1_timeout  : out std_logic_vector(15 downto 0);  -- FMD: L1 timeout
    l2_timeout  : out std_logic_vector(15 downto 0);  -- FMD: L2 timeout
    shift_div   : out std_logic_vector(15 downto 0);  -- FMD: Shift clk
    strips      : out std_logic_vector(15 downto 0);  -- FMD: Strips
    cal_level   : out std_logic_vector(15 downto 0);  -- FMD: Cal pulse
    shape_bias  : out std_logic_vector(15 downto 0);  -- FMD: Shape bias
    vfs         : out std_logic_vector(15 downto 0);  -- FMD: Shape ref
    vfp         : out std_logic_vector(15 downto 0);  -- FMD: Preamp ref
    sample_div  : out std_logic_vector(15 downto 0);  -- FMD: Sample clk
    fmdd_cmd    : out std_logic_vector(15 downto 0);  -- FMD: Commands
    cal_iter    : out std_logic_vector(15 downto 0);  -- FMD: cal events.
    mebs        : out std_logic_vector(4 downto 0);   -- MEB config
    meb_cnt     : in  std_logic_vector(3 downto 0);   -- MEB counter
    cal_delay   : out std_logic_vector(15 downto 0));  -- FMD: xtra L1 delay
end registers;

------------------------------------------------------------------------------
architecture rtl of registers is
  signal missed_sclk_i : std_logic;
  signal csr2_i        : std_logic_vector(15 downto 0);
  signal csr3_i        : std_logic_vector(15 downto 0);
  signal dstbcnt_i     : std_logic_vector(7 downto 0);
  signal sclkcnt_i     : std_logic_vector(15 downto 0);
  signal l2cnt_i       : std_logic_vector(15 downto 0);
  signal l1cnt_i       : std_logic_vector(15 downto 0);
  signal l0cnt_i       : std_logic_vector(15 downto 0);

begin  -- rtl
  -- purpose: Collect combinatorics
  combi                : block
  begin  -- block combi
    missed_sclk <= missed_sclk_i;
    csr2        <= csr2_i;
    csr3        <= csr3_i;
  end block combi;

  reg_block : entity work.registers_block(rtl3)
    port map (
        clk         => clk,             -- in  Clock
        rstb        => rstb,            -- in  Async reset
        -- ADC interface 
        add_adc     => add_adc,         -- in  ADC address
        we_adc      => we_adc,          -- in  Write enable from monitor ADC
        data_adc    => data_adc,        -- in  Monitor ADC data
        end_seq     => end_seq,         -- in  End of Monitor flag
        cnv_mode    => cnv_mode,        -- out Continuous conversion flag
        -- Write interface 
        add         => wadd,            -- in  Register address
        we          => we,              -- in  Write enable
        din         => wdata,           -- in  Register data
        -- Read interface
        add_al      => add_al,          -- in  ALTRO address input
        add_sc      => add_sc,          -- in  I2C address input
        dout_al     => dout_al,         -- out ALTRO Data out
        dout_sc     => dout_sc,         -- out I2C Data out
        -- Misc input
        hadd        => hadd,            -- in  Card address
        -- Counters
        dstbcnt_in  => dstbcnt_i,       -- in  Data strobe counts
        l0cnt_in    => l0cnt_i,         -- in  L0 counts
        l1cnt_in    => l1cnt_i,         -- in  L1 counts
        l2cnt_in    => l2cnt_i,         -- in  L2 counts
        sclkcnt_in  => sclkcnt_i,       -- in  Slow clock counts
        -- Errors 
        par_error   => par_error,       -- in  Parity error
        paps_error  => paps_error,      -- in  PASA power sup. error
        alps_error  => alps_error,      -- in  ALTRO power sup. error
        al_error    => al_error,        -- in  ALTRO error
        missed_sclk => missed_sclk_i,   -- in  Missed slow clock alarm
        ierr_sc     => ierr_sc,         -- in  I2C instruction error
        ierr_al     => ierr_al,         -- in  Bus instruction error
        fmdd_stat   => fmdd_stat,       -- in  FMDD status
        -- Commands 
        cnt_lat     => cnt_lat,         -- in  Command: Latch counters
        cnt_clr     => cnt_clr,         -- in  Command: Clear counters
        csr1_clr    => csr1_clr,        -- in  Command: Clear CSR1
        bc_rst      => bc_rst,          -- in  Command: Reset BC
        st_cnv      => st_cnv,          -- in  Command: Start monitor
        -- Output of registers
        csr2        => csr2_i,          -- out Config/Status 2
        csr3        => csr3_i,          -- out Config/Status 3
        tsm_word    => tsm_word,        -- out TSM:  word
        us_ratio    => us_ratio,        -- out TSM: Under sampling
        bc_int      => bc_int,          -- out BC interrupt
        bc_error    => bc_error,        -- out BC error
        -- Various FMDD output of registers
        hold_wait   => hold_wait,       -- out FMD: Wait to hold
        l1_timeout  => l1_timeout,      -- out FMD: L1 timeout
        l2_timeout  => l2_timeout,      -- out FMD: L2 timeout
        shift_div   => shift_div,       -- out FMD: Shift clk
        strips      => strips,          -- out FMD: Strips
        cal_level   => cal_level,       -- out FMD: Cal pulse
        shape_bias  => shape_bias,      -- out FMD: Shape bias
        vfs         => vfs,             -- out FMD: Shape ref
        vfp         => vfp,             -- out FMD: Preamp ref
        sample_div  => sample_div,      -- out FMD: Sample clk
        fmdd_cmd    => fmdd_cmd,        -- out FMD: Commands
        cal_iter    => cal_iter,        -- out FMD: Cal. events. 
        mebs        => mebs,            -- out MEB config
        meb_cnt     => meb_cnt,         -- in  MEB counter
        cal_delay   => cal_delay);      -- out CAL: xtra L1 delay 

  cnts : entity work.counters(rtl2)
    port map (
        clk         => clk,
        sclk        => sclk,
        rstb        => rstb,
        clr_cnt     => cnt_clr,
        l0_trg      => l0,
        l1_trg      => l1_trg,
        l2_trg      => l2_trg,
        csr3        => csr3_i(7 downto 0),
        adcclk_en   => csr2_i(3),
        dstb        => dstb,
        al_trsf     => al_trsf,
        l0_cnt      => l0cnt_i,
        l1_cnt      => l1cnt_i,
        l2_cnt      => l2cnt_i,
        sclk_cnt    => sclkcnt_i,
        missed_sclk => missed_sclk_i,
        dstb_cnt    => dstbcnt_i);
end rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package registers_pack is
  component registers
    port (
      clk         : in  std_logic;
      sclk        : in  std_logic;
      rstb        : in  std_logic;
      we          : in  std_logic;
      wadd        : in  std_logic_vector(6 downto 0);
      wdata       : in  std_logic_vector(15 downto 0);
      add_al      : in  std_logic_vector(6 downto 0);
      add_sc      : in  std_logic_vector(6 downto 0);
      dout_al     : out std_logic_vector(15 downto 0);
      dout_sc     : out std_logic_vector(15 downto 0);
      end_seq     : in  std_logic;
      we_adc      : in  std_logic;
      add_adc     : in  std_logic_vector(4 downto 0);
      data_adc    : in  std_logic_vector(15 downto 0);
      cnv_mode    : out std_logic;
      fmdd_stat   : in  std_logic_vector(15 downto 0);
      l0          : in  std_logic;
      l1_trg      : in  std_logic;
      l2_trg      : in  std_logic;
      dstb        : in  std_logic;
      al_trsf     : in  std_logic;
      par_error   : in  std_logic;
      paps_error  : in  std_logic;
      alps_error  : in  std_logic;
      ierr_sc     : in  std_logic;
      ierr_al     : in  std_logic;
      al_error    : in  std_logic;
      bc_rst      : in  std_logic;
      csr1_clr    : in  std_logic;
      cnt_lat     : in  std_logic;
      cnt_clr     : in  std_logic;
      st_cnv      : in  std_logic;
      hadd        : in  std_logic_vector(4 downto 0);
      missed_sclk : out std_logic;
      bc_int      : out std_logic;
      bc_error    : out std_logic;
      csr2        : out std_logic_vector(15 downto 0);
      csr3        : out std_logic_vector(15 downto 0);
      tsm_word    : out std_logic_vector(8 downto 0);
      us_ratio    : out std_logic_vector(15 downto 0);
      hold_wait   : out std_logic_vector(15 downto 0);
      l1_timeout  : out std_logic_vector(15 downto 0);
      l2_timeout  : out std_logic_vector(15 downto 0);
      shift_div   : out std_logic_vector(15 downto 0);
      strips      : out std_logic_vector(15 downto 0);
      cal_level   : out std_logic_vector(15 downto 0);
      shape_bias  : out std_logic_vector(15 downto 0);
      vfs         : out std_logic_vector(15 downto 0);
      vfp         : out std_logic_vector(15 downto 0);
      sample_div  : out std_logic_vector(15 downto 0);
      fmdd_cmd    : out std_logic_vector(15 downto 0);
      cal_iter    : out std_logic_vector(15 downto 0);
      mebs        : out std_logic_vector(4 downto 0);
      meb_cnt     : in  std_logic_vector(3 downto 0);
      cal_delay   : out std_logic_vector(15 downto 0)); 
  end component;
end registers_pack;
------------------------------------------------------------------------------
--
-- EOF
--

