------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interfacedec_pack.all;
use work.register_config.all;

------------------------------------------------------------------------------
entity interfacedec_tb is
end interfacedec_tb;

------------------------------------------------------------------------------

architecture test of interfacedec_tb is
  constant PERIOD : time                         := 25 ns;  -- A clock cycle
  constant HADD   : std_logic_vector(4 downto 0) := "00000";

  signal clk_i        : std_logic                     := '0';
  signal rstb_i       : std_logic                     := '1';
  signal add_al_i      : std_logic_vector(6 downto 0)  := (others => '0');
  signal data_al_i    : std_logic_vector(15 downto 0) := (others => '0');
  signal wr_al_i      : std_logic                     := '0';
  signal bcast_al_i   : std_logic                     := '0';
  signal add_sc_i     : std_logic_vector(6 downto 0)  := (others => '0');
  signal data_sc_i    : std_logic_vector(15 downto 0) := (others => '0');
  signal wr_sc_i      : std_logic                     := '0';
  signal bcast_sc_i   : std_logic                     := '0';
  signal exec_i       : std_logic                     := '0';
  signal ackn_en_if_i : std_logic                     := '0';
  signal slctr_i      : std_logic                     := '0';
  signal lastst_al_i  : std_logic                     := '0';
  signal ierr_sc_i    : std_logic                     := '0';
  signal cnt_lat_i    : std_logic;
  signal cnt_clr_i    : std_logic;
  signal csr1_clr_i   : std_logic;
  signal al_rst_i     : std_logic;
  signal bc_rst_i     : std_logic;
  signal st_cnv_i     : std_logic;
  signal sc_evl_i     : std_logic;
  signal evl_rdo_i    : std_logic;
  signal st_tsm_i     : std_logic;
  signal acq_rdo_i    : std_logic;
  signal ierr_al_i    : std_logic;
  signal add_i        : std_logic_vector(6 downto 0)  := (others => '0');
  signal data_i       : std_logic_vector(15 downto 0) := (others => '0');
  signal we_reg_i     : std_logic;

  -- purpose: Send a command (as from the bus) to decoder
  procedure send (
    signal   add       : out std_logic_vector(6 downto 0);  -- Address
    signal   data      : out std_logic_vector(15 downto 0);  -- data
    signal   exec      : out std_logic;  -- Excute
    signal   lastst_al : out std_logic;  -- Bus last state
    constant code      : in  integer) is  -- Instruction code
  begin  -- send
    report "Sending ALTRO instruction " & integer'image(code) severity note;
    add       <= std_logic_vector(to_unsigned(code, 7)),
                 (others => '0') after 6.1*PERIOD;
    data      <= std_logic_vector(to_unsigned(code, 16)),
                 (others => '0') after 6.1*PERIOD;
    exec      <= '0', '1' after 2*PERIOD, '0' after 3*PERIOD;
    lastst_al <= '0', '1' after 5*PERIOD, '0' after 6*PERIOD;
  end send;

    -- purpose: Send a command (as from the I2C bus) to decoder
  procedure i2c_send (
    signal   add   : out std_logic_vector(6 downto 0);   -- Address
    signal   data  : out std_logic_vector(15 downto 0);  -- data
    signal   slctr : out std_logic;                      -- Control
    constant code  : in  integer) is                     -- Instruction code
  begin  -- send
    report "Sending I2C instruction " & integer'image(code) severity note;
    add <= std_logic_vector(to_unsigned(code, 7)),
                 (others => '0') after 6.1*PERIOD;
    data <= std_logic_vector(to_unsigned(code, 16)),
                 (others => '0') after 6.1*PERIOD;
    slctr <= '0', '1' after PERIOD, '0' after 3 * PERIOD;
  end i2c_send;

begin  -- test

  DUT : entity work.interfacedec(rtl2)
    port map (
      clk        => clk_i,
      rstb       => rstb_i,
      add_al     => add_al_i,
      data_al    => data_al_i,
      wr_al      => wr_al_i,
      bcast_al   => bcast_al_i,
      add_sc     => add_sc_i,
      data_sc    => data_sc_i,
      wr_sc      => wr_sc_i,
      bcast_sc   => bcast_sc_i,
      exec       => exec_i,
      ackn_en_if => ackn_en_if_i,
      slctr      => slctr_i,
      lastst_al  => lastst_al_i,
      ierr_sc    => ierr_sc_i,
      cnt_lat    => cnt_lat_i,
      cnt_clr    => cnt_clr_i,
      csr1_clr   => csr1_clr_i,
      al_rst     => al_rst_i,
      bc_rst     => bc_rst_i,
      st_cnv     => st_cnv_i,
      sc_evl     => sc_evl_i,
      evl_rdo    => evl_rdo_i,
      st_tsm     => st_tsm_i,
      acq_rdo    => acq_rdo_i,
      ierr_al    => ierr_al_i,
      add        => add_i,
      data       => data_i,
      we_reg     => we_reg_i);

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    rstb_i <= '0';
    wait for 100 ns;
    rstb_i <= '1';
    wait for 4 * PERIOD;
    wait until rising_edge(clk_i);

    for i in 1 to MAX_REG loop
      if regs(i).w then
        wr_al_i <= '1';                 -- These register are read-only
        wr_sc_i <= '1';
      else
        wr_al_i <= '0';
        wr_sc_i <= '0';
      end if;
      wait until rising_edge(clk_i);
      send(code      => i,
           lastst_al => lastst_al_i,
           add       => add_al_i,
           data      => data_al_i,
           exec      => exec_i);
      wait for PERIOD;
      if regs(i+2).sc then
        i2c_send(code  => i+2,
                 add   => add_sc_i,
                 data  => data_sc_i,
                 slctr => slctr_i);
      end if;
      wait for 9 * PERIOD;
    end loop;  -- i

    wait;                               -- forever 
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;

end test;
------------------------------------------------------------------------------
--
-- EOF
--
