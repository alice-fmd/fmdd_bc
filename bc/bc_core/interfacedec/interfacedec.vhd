------------------------------------------------------------------------------
-- Title      : Decode instruction
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : interfacedec.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2008-08-11
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Decode instructions from bus or I2C
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.register_config.all;

entity interfacedec is
  port (
    clk        : in  std_logic;         -- clock
    rstb       : in  std_logic;         -- Async reset
    add_al     : in  std_logic_vector(6 downto 0);  -- Bus Address
    data_al    : in  std_logic_vector(15 downto 0);  -- Bus Data
    wr_al      : in  std_logic;         -- Bus Write
    bcast_al   : in  std_logic;         -- Bus Broadcast
    add_sc     : in  std_logic_vector(6 downto 0);  -- I2C Address
    data_sc    : in  std_logic_vector(15 downto 0);  -- I2C Data
    wr_sc      : in  std_logic;         -- I2C Write from I2C
    bcast_sc   : in  std_logic;         -- I2C Broadcast from I2C
    exec       : in  std_logic;         -- Bus enable from bus
    ackn_en_if : in  std_logic;         -- Bus Acknowledge enable
    slctr      : in  std_logic;         -- I2C select
    lastst_al  : in  std_logic;         -- BUS Last state
    ierr_sc    : in  std_logic;         -- I2C Instruction error
    cnt_lat    : out std_logic;         -- Command: Latch counter
    cnt_clr    : out std_logic;         -- Command: Clear counters
    csr1_clr   : out std_logic;         -- Command: Clear status
    al_rst     : out std_logic;         -- Command: Reset ALTROs
    bc_rst     : out std_logic;         -- Command: Clear BC
    st_cnv     : out std_logic;         -- Command: Start monitoring
    sc_evl     : out std_logic;         -- Command: Scan event length
    evl_rdo    : out std_logic;         -- Command: readout event length
    st_tsm     : out std_logic;         -- Command: Start test mode
    acq_rdo    : out std_logic;         -- Command: Acquire and R/O
    ierr_al    : out std_logic;         -- Instruction error
    add        : out std_logic_vector(6 downto 0);  -- Address
    data       : out std_logic_vector(15 downto 0);  -- Data
    we_reg     : out std_logic);        -- We're addressed
end interfacedec;

------------------------------------------------------------------------------
architecture rtl2 of interfacedec is

  -- Internal signals, etc.
  signal icode_i   : integer;           -- Interal icode
  -- signal icode_ii   : add_t;         -- Temporary
  signal al_rst_i  : std_logic;         -- Reset altro's
  signal al_rst_ii : std_logic;         -- Reset altro's delayed
  signal cmd_en_i  : std_logic;         -- Command enable
  signal sc_evl_i  : std_logic;         -- Internal bus cmd - sc_evl     
  signal evl_rdo_i : std_logic;         -- Internal bus cmd - evl_rdo    
  signal st_tsm_i  : std_logic;         -- Internal bus cmd - st_tsm     
  signal acq_rdo_i : std_logic;         -- Internal bus cmd - acq_rdo    
  signal add_al_i  : add_t;
  signal add_sc_i  : add_t;
  signal err_al_i  : std_logic;
  signal err_sc_i  : std_logic;
  signal cmd_al_i  : std_logic;
  signal cmd_sc_i  : std_logic;
  
  -- purpose: Check an address against errors
  procedure check_add (constant add   : in  add_t;         -- Address
                       constant sc    : in  std_logic;     -- Slow control
                       signal   we    : in  std_logic;     -- Write enable
                       signal   bcast : in  std_logic;     -- Broadcast
                       signal   comd  : out std_logic;     -- Is a command
                       signal   err   : out std_logic) is  -- Error
    variable sc_err    : boolean   := false;
    variable wr_err    : boolean   := false;
    variable bcast_err : boolean   := false;
    variable cmd_err   : boolean   := false;
    variable comd_i    : std_logic := '0';
    variable err_i     : std_logic := '0';
    -- variable instr_err : boolean := false;
  begin  -- check_add
    if (add >= 0) and (add <= MAX_REG) then
      sc_err    := sc = '1' and (not regs(add).sc);
      wr_err    := we = '1' and (not regs(add).w);
      bcast_err := bcast = '1' and (not regs(add).bcast);
      -- instr_err := '1' when ((add < 0) or (add > MAX_REG)) else '0';
      case add is
        -- Shared commands
        when add_cntlat  => comd_i := we;                  -- Latch counters
        when add_cntclr  => comd_i := we;                  -- Clear counters 
        when add_csr1clr => comd_i := we;                  -- Clear CSR1 
        when add_alrst   => comd_i := we;                  -- Reset altro's 
        when add_bcrst   => comd_i := we;                  -- Reset BC 
        when add_stcnv   => comd_i := we;                  -- Start conversion 
                            -- BUS only commands
        when add_scevl   =>             -- Scan event length
          comd_i  := (not sc) and we;
          cmd_err := (we = '0');
        when add_evlrdo =>              -- Read out event length
          comd_i  := (not sc) and we;
          cmd_err := (we = '0');
        when add_sttsm =>               -- Start test mode
          comd_i  := (not sc) and we;
          cmd_err := (we = '0');
        when add_acqrdo =>              -- Read out
          comd_i  := (not sc) and we;
          cmd_err := (we = '0');
        when others =>                  -- Others
          comd_i := '0';
      end case;
    end if;
    if (sc_err or wr_err or bcast_err or cmd_err) then
      err_i := '1';
    else
      err_i := '0';
    end if;
    report "Check gives comd=" & std_logic'image(comd_i)
      & " err=" & std_logic'image(err_i) severity note;
    comd <= comd_i;
    err  <= err_i;
  end check_add;
begin  -- rtl


  -- purpose: Decode instructions
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  decoder : process (clk, rstb)
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      cmd_sc_i <= '0';
      cmd_al_i <= '0';
      err_sc_i <= '0';
      err_al_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge

      check_add(add   => add_al_i,
                sc    => '0',
                we    => wr_al,
                bcast => bcast_al,
                comd  => cmd_al_i,
                err   => err_al_i);
      check_add(add   => add_sc_i,
                sc    => '1',
                we    => wr_sc,
                bcast => bcast_sc,
                comd  => cmd_sc_i,
                err   => err_sc_i);
    end if;
  end process decoder;


  -- purpose: Decode common commands, shared between both the I2C and bus
  --          intereface 
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  common_cmds : process (clk, rstb)
  begin  -- process common_cmds
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_lat  <= '0';
      cnt_clr  <= '0';
      csr1_clr <= '0';
      al_rst_i <= '0';
      st_cnv   <= '0';
      bc_rst   <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      case icode_i is
        when add_cntlat  => cnt_lat  <= cmd_en_i;  -- Latch counters 
        when add_cntclr  => cnt_clr  <= cmd_en_i;  -- Clear counters 
        when add_csr1clr => csr1_clr <= cmd_en_i;  -- Clear CSR1 
        when add_alrst   => al_rst_i <= cmd_en_i;  -- Reset altro's 
        when add_bcrst   => bc_rst   <= cmd_en_i;  -- Reset BC 
        when add_stcnv   => st_cnv   <= cmd_en_i;  -- Start conversion 
        when others      =>             -- Others
          cnt_lat  <= '0';
          cnt_clr  <= '0';
          csr1_clr <= '0';
          al_rst_i <= '0';
          st_cnv   <= '0';
          bc_rst   <= '0';
      end case;
    end if;
  end process common_cmds;


  -- purpose: Decode bus-only commands
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  bus_cmds : process(add_al_i)
  begin  -- process bus_cmds
    sc_evl_i      <= '0';
    evl_rdo_i     <= '0';
    st_tsm_i      <= '0';
    acq_rdo_i     <= '0';

    case add_al_i is
      when add_scevl  => sc_evl_i  <= wr_al;  -- Scan event length
      when add_evlrdo => evl_rdo_i <= wr_al;  -- Read out event length
      when add_sttsm  => st_tsm_i  <= wr_al;  -- Start test mode 
      when add_acqrdo => acq_rdo_i <= wr_al;  -- Read out 
      when others     =>  null;               -- Others
    end case;
  end process bus_cmds;

  -- purpose: Keep al_rst down for at least 2 clock cycles
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  al_reset_keep : process (clk, rstb)
  begin  -- process al_reset_keep
    if rstb = '0' then                  -- asynchronous reset (active low)
      al_rst_ii <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      al_rst_ii <= al_rst_i;
    end if;
  end process al_reset_keep;

  -- purpose: Combinatorics
  combi : block
  begin  -- block combi
    -- Internal address
    -- add_al_i <= 0 when rstb = '0' else to_integer(unsigned(add_al));
    -- add_sc_i <= 0 when rstb = '0' else to_integer(unsigned(add_sc));
    add_al_i <= to_integer(unsigned(add_al));
    add_sc_i <= to_integer(unsigned(add_sc));
    icode_i  <= add_sc_i when slctr = '1' else add_al_i;

    -- Data and address output 
    add      <= add_sc                   when slctr = '1' else add_al;
    data     <= data_sc                  when slctr = '1' else data_al;
    we_reg   <= (wr_sc and not ierr_sc)  when slctr = '1' else
                (wr_al and exec);
    cmd_en_i <= (cmd_sc_i and not err_sc_i and not ierr_sc )
                when slctr = '1' else 
                (cmd_al_i and not err_al_i and exec);

    -- Bus commands 
    sc_evl  <= sc_evl_i and lastst_al;
    evl_rdo <= evl_rdo_i and lastst_al;
    st_tsm  <= st_tsm_i and lastst_al;
    acq_rdo <= acq_rdo_i and lastst_al;
    al_rst  <= al_rst_ii or al_rst_i;

    -- Error
    ierr_al <= err_sc_i or err_al_i;
  end block combi;
end rtl2;

------------------------------------------------------------------------------
architecture rtl of interfacedec is

  -- Internal signals, etc.
  signal comd_i        : std_logic;     -- Command enable
  signal ierr_al_dec_i : std_logic;     -- Decoder error
  signal icode_i       : integer;       -- Interal icode
  -- signal icode_ii   : add_t;         -- Temporary
  signal al_rst_i      : std_logic;     -- Reset altro's
  signal al_rst_ii     : std_logic;     -- Reset altro's delayed
  signal cmd_en_i      : std_logic;     -- Command enable
  signal sc_evl_i      : std_logic;     -- Internal bus cmd - sc_evl     
  signal evl_rdo_i     : std_logic;     -- Internal bus cmd - evl_rdo    
  signal st_tsm_i      : std_logic;     -- Internal bus cmd - st_tsm     
  signal acq_rdo_i     : std_logic;     -- Internal bus cmd - acq_rdo    
  signal ierr_al_cmd_i : std_logic;     -- Internal bus cmd error
  signal add_al_i      : add_t;
  signal add_sc_i      : add_t;
begin  -- rtl


  -- purpose: Decode instructions
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  decoder : process (clk, rstb)
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      comd_i            <= '0';
      ierr_al_dec_i     <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge

      case icode_i is
        when add_zero =>       -- Not used, no error
          comd_i        <= '0';
          ierr_al_dec_i <= '0';

          -- when add_t_th  |           -- Write-able thresholds
          -- add_av_th |
          -- add_ac_th |
          -- add_dv_th |
          -- add_dc_th =>
        when add_t1_th |
          add_flash_i_th |
          add_al_dig_i_th |
          add_al_ana_i_th |
          add_va_rec_ip_th |
          add_t2_th |
          add_va_sup_ip_th |
          add_va_rec_im_th |
          add_va_sup_im_th |
          add_gtl_u_th |
          add_t3_th |
          add_t1sens_th |
          add_t2sens_th |
          add_al_dig_u_th |
          add_al_ana_u_th |
          add_t4_th |
          add_va_rec_up_th |
          add_va_sup_up_th |
          add_va_sup_um_th |
          add_va_rec_um_th =>
          comd_i        <= '0';
          ierr_al_dec_i <= not slctr and not wr_al and bcast_al;

          -- when add_temp    |         -- Read-only registers
          -- add_av      |
          -- add_ac      |
          -- add_dv      |
          -- add_dc      |
        when add_t1 |
          add_flash_i |
          add_al_dig_i |
          add_al_ana_i |
          add_va_rec_ip |
          add_t2 |
          add_va_sup_ip |
          add_va_rec_im |
          add_va_sup_im |
          add_gtl_u |
          add_t3 |
          add_t1sens |
          add_t2sens |
          add_al_dig_u |
          add_al_ana_u |
          add_t4 |
          add_va_rec_up |
          add_va_sup_up |
          add_va_sup_um |
          add_va_rec_um |
          add_l1cnt |
          add_l2cnt |
          add_dstbcnt |
          add_sclkcnt |
          add_free =>
          comd_i        <= '0';
          ierr_al_dec_i <= not slctr and (wr_al or (not wr_al and bcast_al));

        when add_tsm_word |            -- Write-able bus-only registers
          add_us_ratio =>
          comd_i        <= '0';
          ierr_al_dec_i <= not slctr and not wr_al and bcast_al;

        when add_csr0 |                -- Write-able registers
          add_csr1 |
          add_csr2 |
          add_csr3 =>
          comd_i        <= '0';
          ierr_al_dec_i <= not slctr and not wr_al and bcast_al;

        when add_cntlat  |             -- Commands 
          add_cntclr  |
          add_csr1clr |
          add_alrst   |
          add_bcrst   |
          add_stcnv =>
          ierr_al_dec_i <= not slctr and not wr_al;
          if slctr = '1' then
            comd_i      <= wr_sc;
          else
            comd_i      <= wr_al;
          end if;

        when add_evlrdo |              -- Bus-only commands 
          add_acqrdo |
          add_scevl  |
          add_sttsm =>
          ierr_al_dec_i <= not slctr and not wr_al;
          if slctr = '1' then
            comd_i      <= '0';
          else
            comd_i      <= wr_al;
          end if;

        when add_fmdd_stat |           -- FMDD status - read only 
                           add_l0cnt =>             -- L0 counters read only
          comd_i        <= '0';
          ierr_al_dec_i <= not slctr and (wr_al or (not wr_al and bcast_al));
          

        when add_hold_wait |                  -- FMD: Wait to hold
                           add_l1_timeout |   -- FMD: L1 timeout
                           add_l2_timeout |   -- FMD: L2 timeout
                           add_shift_div |    -- FMD: Shift clk
                           add_strips |       -- FMD: Strips
                           add_cal_level |    -- FMD: Cal pulse
                           add_cal_iter |     -- FMD: Cal pulse
                           add_mebs |         -- FMD: Multi-event buffer
                           add_shape_bias |   -- FMD: Shape bias
                           add_vfs |          -- FMD: Shape ref
                           add_vfp |          -- FMD: Preamp ref
                           add_sample_div =>  -- FMD: Sample clk
          comd_i        <= '0';
          ierr_al_dec_i <= not slctr and not wr_al and bcast_al;

        when add_fmdd_cmd =>            -- FMD: Commands  - write-only
          comd_i        <= '0';
          ierr_al_dec_i <= not slctr and not wr_al;
          
        when others =>
          comd_i        <= '0';
          ierr_al_dec_i <= '0';

      end case;
    end if;
  end process decoder;


  -- purpose: Decode common commands, shared between both the I2C and bus
  --          intereface 
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  common_cmds : process (clk, rstb)
  begin  -- process common_cmds
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_lat  <= '0';
      cnt_clr  <= '0';
      csr1_clr <= '0';
      al_rst_i <= '0';
      st_cnv   <= '0';
      bc_rst   <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge

      case icode_i is
        when add_cntlat =>              -- Latch counters 
          cnt_lat  <= cmd_en_i;
          cnt_clr  <= '0';
          csr1_clr <= '0';
          al_rst_i <= '0';
          bc_rst   <= '0';
          st_cnv   <= '0';

        when add_cntclr =>              -- Clear counters 
          cnt_lat  <= '0';
          cnt_clr  <= cmd_en_i;
          csr1_clr <= '0';
          al_rst_i <= '0';
          bc_rst   <= '0';
          st_cnv   <= '0';

        when add_csr1clr =>             -- Clear CSR1 
          cnt_lat  <= '0';
          cnt_clr  <= '0';
          csr1_clr <= cmd_en_i;
          al_rst_i <= '0';
          bc_rst   <= '0';
          st_cnv   <= '0';

        when add_alrst =>               -- Reset altro's 
          cnt_lat  <= '0';
          cnt_clr  <= '0';
          csr1_clr <= '0';
          al_rst_i <= cmd_en_i;
          bc_rst   <= '0';
          st_cnv   <= '0';

        when add_bcrst =>               -- Reset BC 
          cnt_lat  <= '0';
          cnt_clr  <= '0';
          csr1_clr <= '0';
          al_rst_i <= '0';
          bc_rst   <= cmd_en_i;
          st_cnv   <= '0';

        when add_stcnv =>               -- Start conversion 
          cnt_lat  <= '0';
          cnt_clr  <= '0';
          csr1_clr <= '0';
          al_rst_i <= '0';
          bc_rst   <= '0';
          st_cnv   <= cmd_en_i;

        when others =>                  -- Others 
          cnt_lat  <= '0';
          cnt_clr  <= '0';
          csr1_clr <= '0';
          al_rst_i <= '0';
          bc_rst   <= '0';
          st_cnv   <= '0';
      end case;
    end if;
  end process common_cmds;


  -- purpose: Decode bus-only commands
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  bus_cmds : process(add_al_i)
  begin  -- process bus_cmds
    
    case add_al_i is
      when add_scevl =>                 -- Scan event length
        sc_evl_i      <= wr_al;
        evl_rdo_i     <= '0';
        st_tsm_i      <= '0';
        acq_rdo_i     <= '0';
        ierr_al_cmd_i <= not wr_al;     -- instruction error Altro Bus decoder

      when add_evlrdo =>                -- Read out event length
        sc_evl_i      <= '0';
        evl_rdo_i     <= wr_al;
        st_tsm_i      <= '0';
        acq_rdo_i     <= '0';
        ierr_al_cmd_i <= not wr_al;

      when add_sttsm =>                 -- Start test mode 
        sc_evl_i      <= '0';
        evl_rdo_i     <= '0';
        st_tsm_i      <= wr_al;
        acq_rdo_i     <= '0';
        ierr_al_cmd_i <= not wr_al;

      when add_acqrdo =>                -- Read out 
        sc_evl_i      <= '0';
        evl_rdo_i     <= '0';
        st_tsm_i      <= '0';
        acq_rdo_i     <= wr_al;
        ierr_al_cmd_i <= not wr_al;

      when others =>                    -- Others
        sc_evl_i      <= '0';
        evl_rdo_i     <= '0';
        st_tsm_i      <= '0';
        acq_rdo_i     <= '0';
        ierr_al_cmd_i <= '0';
    end case;
  end process bus_cmds;

  -- purpose: Keep al_rst down for at least 2 clock cycles
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  al_reset_keep : process (clk, rstb)
  begin  -- process al_reset_keep
    if rstb = '0' then                  -- asynchronous reset (active low)
      al_rst_ii <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      al_rst_ii <= al_rst_i;
    end if;
  end process al_reset_keep;

  -- purpose: Combinatorics
  combi : block
  begin  -- block combi
    -- Internal address
    -- add_al_i <= 0 when rstb = '0' else to_integer(unsigned(add_al));
    -- add_sc_i <= 0 when rstb = '0' else to_integer(unsigned(add_sc));
    add_al_i <= to_integer(unsigned(add_al));
    add_sc_i <= to_integer(unsigned(add_sc));
    icode_i  <= add_sc_i when slctr = '1' else add_al_i;

    -- Data and address output 
    add      <= add_sc                   when slctr = '1' else add_al;
    data     <= data_sc                  when slctr = '1' else data_al;
    we_reg   <= (wr_sc and not ierr_sc)  when slctr = '1' else
                (wr_al and exec);
    cmd_en_i <= (comd_i and not ierr_sc) when slctr = '1' else
                (comd_i and exec);

    -- Bus commands 
    sc_evl  <= sc_evl_i and lastst_al;
    evl_rdo <= evl_rdo_i and lastst_al;
    st_tsm  <= st_tsm_i and lastst_al;
    acq_rdo <= acq_rdo_i and lastst_al;
    al_rst  <= al_rst_ii or al_rst_i;

    -- Error
    ierr_al <= ierr_al_dec_i or ierr_al_cmd_i;
  end block combi;
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package interfacedec_pack is
  component interfacedec
    port (
      clk        : in  std_logic;
      rstb       : in  std_logic;
      add_al     : in  std_logic_vector(6 downto 0);
      data_al    : in  std_logic_vector(15 downto 0);
      wr_al      : in  std_logic;
      bcast_al   : in  std_logic;
      add_sc     : in  std_logic_vector(6 downto 0);
      data_sc    : in  std_logic_vector(15 downto 0);
      wr_sc      : in  std_logic;
      bcast_sc   : in  std_logic;
      exec       : in  std_logic;
      ackn_en_if : in  std_logic;
      slctr      : in  std_logic;
      lastst_al  : in  std_logic;
      ierr_sc    : in  std_logic;
      cnt_lat    : out std_logic;
      cnt_clr    : out std_logic;
      csr1_clr   : out std_logic;
      al_rst     : out std_logic;
      bc_rst     : out std_logic;
      st_cnv     : out std_logic;
      sc_evl     : out std_logic;
      evl_rdo    : out std_logic;
      st_tsm     : out std_logic;
      acq_rdo    : out std_logic;
      ierr_al    : out std_logic;
      add        : out std_logic_vector(6 downto 0);
      data       : out std_logic_vector(15 downto 0);
      we_reg     : out std_logic);
  end component;
end interfacedec_pack;
------------------------------------------------------------------------------
--
-- EOF
--
