------------------------------------------------------------------------------
-- Title      : Manage writes to RAM
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : mem_wr.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Manage writes to RAM
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/15  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mem_wr is
  port (
    clk         : in  std_logic;                      -- Clock
    rstb        : in  std_logic;                      -- Async reset
    sclk_edge   : in  std_logic;                      -- Falling edge on sclk
    missed_sclk : in  std_logic;                      -- Error: missed sclk
    st_tsm      : in  std_logic;                      -- Command: Start TSM
    us_ratio    : in  std_logic_vector(15 downto 0);  -- TSM: Under sample
    tsm_word    : in  std_logic_vector(8 downto 0);   -- TSM: mask
    mem_wren    : out std_logic;                      -- Enable for RAM write
    mem_wraddr  : out std_logic_vector(8 downto 0);   -- Write address
    tsm_acqon   : out std_logic;                      -- TSM: acquiring
    tsm_isol    : out std_logic);                     -- TSM : card isolate
end entity mem_wr;

------------------------------------------------------------------------------
architecture rtl of mem_wr is
  type state_t is (s0, s1, s2);                   -- State typye
  signal st_i           : state_t;                -- State;
  signal cnt_add_i      : unsigned(8 downto 0);   -- Write address
  signal cnt_sclk_i     : unsigned(15 downto 0);  -- sclk counter;
  signal mem_wren_i     : std_logic;              -- Write enable
  signal clr_cnt_sclk_i : std_logic;              -- Clear sclk counter;
  signal clr_cnt_add_i  : std_logic;              -- Clear address counter
  signal en_cnt_sclk_i  : std_logic;              -- Enable slow counter
begin  -- architecture rtl
  -- purpose: Combinatorics
  combi                 : block is
  begin  -- block combi
    mem_wren   <= mem_wren_i;
    mem_wraddr <= std_logic_vector(cnt_add_i);
    tsm_isol   <= '1' when (not (st_i = s0) or st_tsm = '1') else '0';
    tsm_acqon  <= '1' when (not (st_i = s0))                 else '0';
  end block combi;


  -- purpose: Final state machine
  -- type   : sequential
  -- inputs : clk, rstb, missed_sclk, st_tsm, mem_wraddr_i, cnt_sclk_i,
  --          us_ratio, tsm_word 
  -- outputs: en_cnt_sclk_i, clr_cnt_sclk_i, clr_cnt_add_i, mem_wren_i
  fsm : process (clk, rstb) is
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      en_cnt_sclk_i    <= '0';
      clr_cnt_sclk_i   <= '1';
      clr_cnt_add_i    <= '1';
      mem_wren_i       <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if missed_sclk = '1' then
        en_cnt_sclk_i  <= '0';
        clr_cnt_sclk_i <= '1';
        clr_cnt_add_i  <= '1';
        mem_wren_i     <= '0';
      else

        case st_i is
          when s0 =>
            en_cnt_sclk_i  <= '0';
            clr_cnt_sclk_i <= '1';
            clr_cnt_add_i  <= '1';
            mem_wren_i     <= '0';
            if st_tsm = '1' then
              st_i         <= s1;
            else
              st_i         <= s0;
            end if;

          when s1 =>
            en_cnt_sclk_i    <= '1';
            clr_cnt_add_i    <= '0';
            if cnt_sclk_i = unsigned(us_ratio) then
              mem_wren_i     <= '1';
              clr_cnt_sclk_i <= '1';
              st_i           <= s2;
            else
              mem_wren_i     <= '0';
              clr_cnt_sclk_i <= '0';
              st_i           <= s1;
            end if;

          when s2 =>
            en_cnt_sclk_i  <= '0';
            clr_cnt_sclk_i <= '1';
            clr_cnt_add_i  <= '0';
            mem_wren_i     <= '0';
            if cnt_add_i = unsigned(tsm_word) - 1 then
              st_i         <= s0;
            else
              st_i         <= s1;
            end if;

          when others =>
            en_cnt_sclk_i  <= '0';
            clr_cnt_sclk_i <= '1';
            clr_cnt_add_i  <= '1';
            mem_wren_i     <= '0';
            st_i           <= s0;
        end case;
      end if;
    end if;
  end process fsm;


  -- purpose: Count sclk's
  -- type   : sequential
  -- inputs : clk, rstb, clr_cnt_sclk_i, sclk_edge, en_cnt_sclk_i
  -- outputs: cnt_sclk_i
  count_sclk : process (clk, rstb) is
  begin  -- process count_sclk
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_sclk_i   <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clr_cnt_sclk_i = '1' then
        cnt_sclk_i <= (others => '0');
      elsif en_cnt_sclk_i = '1' and sclk_edge = '1' then
        cnt_sclk_i <= cnt_sclk_i + 1;
      end if;
    end if;
  end process count_sclk;

  -- purpose: Count add's
  -- type   : sequential
  -- inputs : clk_i, rstb, clr_cnt_add_i, add_edge, en_cnt_add_i
  -- outputs: cnt_add_i
  count_add : process (clk, rstb) is
  begin  -- process count_add
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_add_i   <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clr_cnt_add_i = '1' then
        cnt_add_i <= (others => '0');
      elsif mem_wren_i = '1' then
        cnt_add_i <= cnt_add_i + 1;
      end if;
    end if;
  end process count_add;
end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package mem_wr_pack is
  component mem_wr
    port (
      clk         : in  std_logic;      -- Clock
      rstb        : in  std_logic;      -- Async reset
      sclk_edge   : in  std_logic;      -- Falling edge on sclk
      missed_sclk : in  std_logic;      -- Error: missed sclk
      st_tsm      : in  std_logic;      -- Command: Start TSM
      us_ratio    : in  std_logic_vector(15 downto 0);  -- TSM: Under sample
      tsm_word    : in  std_logic_vector(8 downto 0);  -- TSM: mask
      mem_wren    : out std_logic;      -- Enable for RAM write
      mem_wraddr  : out std_logic_vector(8 downto 0);  -- Write address
      tsm_acqon   : out std_logic;      -- TSM: acquiring
      tsm_isol    : out std_logic);     -- TSM : card isolate
  end component mem_wr;
end package mem_wr_pack;
------------------------------------------------------------------------------
--
-- EOF
--
