------------------------------------------------------------------------------
-- Title      : Test bench of test mode writer
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : mem_wr_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/18
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test mode writer - test bench
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm	Created
------------------------------------------------------------------------------

------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.mem_wr_pack.all;

------------------------------------------------------------------------------
entity mem_wr_tb is
end entity mem_wr_tb;

------------------------------------------------------------------------------

architecture test of mem_wr_tb is
  constant PERIOD : time := 25 ns;

  signal clk_i         : std_logic                     := '0';  -- Clock
  signal sclk_i        : std_logic                     := '0';  -- Clock
  signal sclk_ii       : std_logic                     := '0';  -- Clock
  signal sclk_iii      : std_logic                     := '0';  -- Clock
  signal rstb_i        : std_logic                     := '0';  -- Async reset
  -- Falling edge on sclk
  signal sclk_edge_i   : std_logic                     := '0';
  -- Error: missed sclk
  signal missed_sclk_i : std_logic                     := '0';
  -- Command: Start TSM
  signal st_tsm_i      : std_logic                     := '0';
  -- TSM: Under sample
  signal us_ratio_i    : std_logic_vector(15 downto 0) := X"0001";
    -- TSM: mask
  signal tsm_word_i    : std_logic_vector(8 downto 0)  := '0' & X"0F";
  signal mem_wren_i    : std_logic;     -- Enable for RAM write
  signal mem_wraddr_i  : std_logic_vector(8 downto 0);  -- Write address
  signal tsm_acqon_i   : std_logic;     -- TSM: acquiring
  signal tsm_isol_i    : std_logic;     -- TSM : card isolate

begin  -- architecture test

  dut: entity work.mem_wr
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        sclk_edge   => sclk_edge_i,     -- in  Falling edge on sclk
        missed_sclk => missed_sclk_i,   -- in  Error: missed sclk
        st_tsm      => st_tsm_i,        -- in  Command: Start TSM
        us_ratio    => us_ratio_i,      -- in  TSM: Under sample
        tsm_word    => tsm_word_i,      -- in  TSM: mask
        mem_wren    => mem_wren_i,      -- out Enable for RAM write
        mem_wraddr  => mem_wraddr_i,    -- out Write address
        tsm_acqon   => tsm_acqon_i,     -- out TSM: acquiring
        tsm_isol    => tsm_isol_i);     -- out TSM : card isolate


  -- purpose: Make stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 200 ns;
    rstb_i <= '1';

    wait for 100 ns;
    wait until rising_edge(clk_i);
    st_tsm_i <= '1';
    wait until rising_edge(clk_i);
    st_tsm_i <= '0';

    wait;                               -- forever
  end process stimuli;


  -- purpose: Make a 40 MHz clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clock_gen: process is
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;

  -- purpose: Make a slow clock
  -- type   : combinational
  -- inputs : 
  -- outputs: sclk_i
  sclk_gen: process is
  begin  -- process sclk_gen
    sclk_i <= not sclk_i;
    wait for 4 * PERIOD / 2;
  end process sclk_gen;

  -- purpose: Make a slow clock edge detect
  -- type   : sequential
  -- inputs : clk_i, rstb_i, sclk_i
  -- outputs: sclk_edge_i
  edge_clock: process (clk_i, rstb_i) is
  begin  -- process edge_clock
    if rstb_i = '0' then                  -- asynchronous reset (active low)
      sclk_ii  <= '0';
      sclk_iii <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      sclk_ii  <= sclk_i;
      sclk_iii <= sclk_ii;
    end if;
  end process edge_clock;
  sclk_edge_i <= not sclk_ii and sclk_iii;
  
end architecture test;

------------------------------------------------------------------------------
