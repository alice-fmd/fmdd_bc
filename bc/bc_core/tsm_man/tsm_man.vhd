------------------------------------------------------------------------------
-- Title      : Test mode manager
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : tsm_man.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-07-21
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test mode manager
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/15  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.mem_wr_pack.all;
use work.mem_rdo_pack.all;

entity tsm_man is
  port (
    clk         : in  std_logic;        -- Clock
    sclk        : in  std_logic;        -- Slow clock (10MHz)
    rstb        : in  std_logic;        -- Async reset
    st_tsm      : in  std_logic;        -- Command: Start test mode
    din         : in  std_logic_vector(39 downto 0);  -- Data in
    acq_rdo     : in  std_logic;        -- Command: Acquire and read-out
    missed_sclk : in  std_logic;        -- Error: Missing sclk
    us_ratio    : in  std_logic_vector(15 downto 0);  -- TSM: Under sampling
    tsm_word    : in  std_logic_vector(8 downto 0);  -- TSM: mask
    mem_wren    : out std_logic;        -- TSM: Memory write enable
    tsm_acqon   : out std_logic;        -- TSM: aquiring
    tsm_isol    : out std_logic;        -- TSM: Card isolated
    sclk_edge   : out std_logic;        -- TSM: Edge of sclk detected
    data_out    : out std_logic_vector(39 downto 0);  -- TSM: Output data
    mtrsf_en    : out std_logic;        -- TSM: transfer enable
    mtrsf       : out std_logic;        -- TSM: transfer
    mdstb       : out std_logic);       -- TSM: data strobe
end entity tsm_man;

architecture rtl of tsm_man is
  signal ram_in_i      : std_logic_vector(39 downto 0);  -- Input to RAM;
  signal sclk_edge_i   : std_logic;     -- Negative edge on sclk
  signal tsm_acqon_i   : std_logic;     -- TSM: acq on
  signal mem_wren_i    : std_logic;     -- Memory write enable
  signal mem_wren_ii   : std_logic;     -- Registered memory write enable
  signal mem_wraddr_i  : std_logic_vector(8 downto 0);  -- Write address
  signal mem_rdaddr_i  : std_logic_vector(8 downto 0);  -- Read address
  signal mem_wraddr_ii : std_logic_vector(8 downto 0);  -- Reg. write address
  signal mem_rdaddr_ii : std_logic_vector(8 downto 0);  -- Reg. read address
  signal sclk_i        : std_logic;     -- Registered sclk
  signal sclk_ii       : std_logic;     -- Registered sclk

  type ram_t is array (511 downto 0) of std_logic_vector(39 downto 0);
  signal ram_i : ram_t := (others => (others => '0'));  -- RAM
  
begin  -- architecture rtl

  mem_writer : mem_wr
    port map (
        clk         => clk,             -- in  Clock
        rstb        => rstb,            -- in  Async reset
        sclk_edge   => sclk_edge_i,     -- in  Falling edge on sclk
        missed_sclk => missed_sclk,     -- in  Error: missed sclk
        st_tsm      => st_tsm,          -- in  Command: Start TSM
        us_ratio    => us_ratio,        -- in  TSM: Under sample
        tsm_word    => tsm_word,        -- in  TSM: mask
        mem_wren    => mem_wren_i,      -- out Enable for RAM write
        mem_wraddr  => mem_wraddr_i,    -- out Write address
        tsm_acqon   => tsm_acqon_i,     -- out TSM: acquiring
        tsm_isol    => tsm_isol);       -- out TSM : card isolate

  mem_reader : mem_rdo
    port map (
        clk        => clk,              -- in  Clock
        rstb       => rstb,             -- in  Async reset
        acq_rdo    => acq_rdo,          -- in  Command: Start readout
        tsm_word   => tsm_word,         -- in  TSM: mask
        mem_rdaddr => mem_rdaddr_i,     -- out Read out address
        mtrsf_en   => mtrsf_en,         -- out TSM: Enable transfer
        mtrsf      => mtrsf,            -- out TSM: transfer
        dstb       => mdstb);           -- out TSM : data strobe

  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combbi
    ram_in_i    <= (din(29 downto 20) &
                    din(0) & din(2) & din(4) & din(6) & din(8) &
                    din(10) & din(12) & din(14) & din(16) & din(18) &
                    din(30) & din(31) & din(32) & din(33) & din(34) &
                    din(35) & din(36) & din(37) & din(38) & din(39) & 
                    din(1) & din(3) & din(5) & din(7) & din(9) &
                    din(11) & din(13) & din(15) & din(17) & din(19));
    sclk_edge   <= sclk_edge_i;
    sclk_edge_i <= not sclk_i and sclk_ii;
    tsm_acqon   <= tsm_acqon_i;
    mem_wren    <= mem_wren_i;
  end block combi;

  -- purpose: Detect falling edge on sclk
  -- type   : sequential
  -- inputs : clk, rstb, sclk
  -- outputs: sclk_ii, sclk_i
  nedge_detect : process (clk, rstb) is
  begin  -- process nedge_detect
    if rstb = '0' then                  -- asynchronous reset (active low)
      sclk_i  <= '0';
      sclk_ii <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if tsm_acqon_i = '1' then
        sclk_i  <= '0';
        sclk_ii <= '0';
      else
        sclk_i  <= sclk;
        sclk_ii <= sclk_i;
      end if;
    end if;
  end process nedge_detect;

  -- purpose: Output data from ram
  -- type   : sequential
  -- inputs : clk, rstb, mem_rdaddr_i, mem_wraddr_i, mem_rdaddr_i, mem_wr_en_i
  -- outputs: mem_wraddr_ii, mem_rdaddr_ii, mem_wr_en_ii, data_out
  -- FIXME: Should probably be a LPM_RAM_DP instead, unless Quartus actually
  -- recognises this as a RAM block. 
  ram                   : process (clk, rstb)
    variable in_addr_i  : integer;
    variable out_addr_i : integer;
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      data_out           <= (others => '0');
      mem_wraddr_ii      <= (others => '0');
      mem_rdaddr_ii      <= (others => '0');
      mem_wren_ii        <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      in_addr_i  := to_integer(unsigned(mem_wraddr_ii));
      out_addr_i := to_integer(unsigned(mem_rdaddr_i));
      data_out           <= ram_i(out_addr_i);
      mem_wraddr_ii      <= mem_wraddr_i;
      mem_rdaddr_ii      <= mem_rdaddr_i;
      mem_wren_ii        <= mem_wren_i;
      if mem_wren_ii = '1' then
        ram_i(in_addr_i) <= ram_in_i;
      end if;
    end if;
  end process ram;

end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package tsm_man_pack is
  component tsm_man
    port (
      clk         : in  std_logic;      -- Clock
      sclk        : in  std_logic;      -- Slow clock (10MHz)
      rstb        : in  std_logic;      -- Async reset
      st_tsm      : in  std_logic;      -- Command: Start test mode
      din         : in  std_logic_vector(39 downto 0);  -- Data in
      acq_rdo     : in  std_logic;      -- Command: Acquire and read-out
      missed_sclk : in  std_logic;      -- Error: Missing sclk
      us_ratio    : in  std_logic_vector(15 downto 0);  -- TSM: Under sampling
      tsm_word    : in  std_logic_vector(8 downto 0);  -- TSM: mask
      mem_wren    : out std_logic;      -- TSM: Memory write enable
      tsm_acqon   : out std_logic;      -- TSM: aquiring
      tsm_isol    : out std_logic;      -- TSM: Card isolated
      sclk_edge   : out std_logic;      -- TSM: Edge of sclk detected
      data_out    : out std_logic_vector(39 downto 0);  -- TSM: Output data
      mtrsf_en    : out std_logic;      -- TSM: transfer enable
      mtrsf       : out std_logic;      -- TSM: transfer
      mdstb       : out std_logic);     -- TSM : data strobe
  end component tsm_man;
end package tsm_man_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
