------------------------------------------------------------------------------
-- Title      : Test bench of TSM memory readout module
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : mem_rdo_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/18
-- Platform   : APEX1K
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test bench of TSM memory readout module
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mem_rdo_pack.all;

------------------------------------------------------------------------------
entity mem_rdo_tb is
end entity mem_rdo_tb;

------------------------------------------------------------------------------

architecture test of mem_rdo_tb is
  signal PERIOD : time := 25 ns;

  signal clk_i        : std_logic                    := '0';  -- Clock
  signal rstb_i       : std_logic                    := '0';  -- Async reset
  -- Command: Start readout
  signal acq_rdo_i    : std_logic                    := '0';
    -- TSM: mask
  signal tsm_word_i   : std_logic_vector(8 downto 0) := '0'&X"0F";
  signal mem_rdaddr_i : std_logic_vector(8 downto 0);  -- Read out address
  signal mtrsf_en_i   : std_logic;      -- TSM: Enable transfer
  signal mtrsf_i      : std_logic;      -- TSM: transfer
  signal dstb_i       : std_logic;      -- TSM : data strobe

begin  -- architecture test

  dut: entity work.mem_rdo
    port map (
        clk        => clk_i,            -- in  Clock
        rstb       => rstb_i,           -- in  Async reset
        acq_rdo    => acq_rdo_i,        -- in  Command: Start readout
        tsm_word   => tsm_word_i,       -- in  TSM: mask
        mem_rdaddr => mem_rdaddr_i,     -- out Read out address
        mtrsf_en   => mtrsf_en_i,       -- out TSM: Enable transfer
        mtrsf      => mtrsf_i,          -- out TSM: transfer
        dstb       => dstb_i);          -- out TSM : data strobe


  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 200 ns;
    rstb_i <= '1';

    wait for 100 ns;
    wait until rising_edge(clk_i);
    acq_rdo_i <= '1';
    wait until rising_edge(clk_i);
    acq_rdo_i <= '0';
    

    wait;                               -- forever
  end process stimuli;

  -- purpose: Make a 40 MHz clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clock_gen: process is
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;
end architecture test;
------------------------------------------------------------------------------
-- 
-- EOF
--
