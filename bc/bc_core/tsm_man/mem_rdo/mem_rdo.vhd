------------------------------------------------------------------------------
-- Title      : Readout memory
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : mem_rdo.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test mode readout memory
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/16  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mem_rdo is
  port (
    clk        : in  std_logic;                     -- Clock
    rstb       : in  std_logic;                     -- Async reset
    acq_rdo    : in  std_logic;                     -- Command: Start readout
    tsm_word   : in  std_logic_vector(8 downto 0);  -- TSM: mask
    mem_rdaddr : out std_logic_vector(8 downto 0);  -- Read out address
    mtrsf_en   : out std_logic;                     -- TSM: Enable transfer
    mtrsf      : out std_logic;                     -- TSM: transfer
    dstb       : out std_logic);                    -- TSM : data strobe
end entity mem_rdo;

architecture rtl of mem_rdo is
  type state_t is (idle,
                   mem_read,
                   last_mem_cycle,
                   last_dstb_trans,
                   close_trans,
                   close_trans_en);     -- State type
  signal st_i      : state_t;           -- State
  signal cnt_clr_i : std_logic;         -- Clear counter
  signal cnt_i     : unsigned(8 downto 0);  -- Counter
  signal dstb_en_i : std_logic;         -- DSTB enable
  signal dstb_i    : std_logic;         -- DSTB internal
  signal max_i     : integer; --  range 0 to 2**8;
begin  -- architecture rtl
  -- purpose: Combinatorics
  combi            : block is
  begin  -- block combi
    max_i <= 0 when rstb = '0' else to_integer(unsigned(tsm_word)) - 1;
    dstb  <= dstb_i;                    -- FIXME: Need to make clock 
    -- dstb <= (dstb_i and not clk);    -- This is dangerous, but perhaps need
  end block combi;

  -- purpose: State machine
  -- type   : sequential
  -- inputs : clk, rstb, acq_rdo, cnt_i, tsm_word
  -- outputs: cnt_clr_i, mtrsf_en, mtrsf, dstb_en_i
  fsm : process (clk, rstb) is
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      mtrsf_en  <= '0';
      mtrsf     <= '0';
      dstb_en_i <= '0';
      cnt_clr_i <= '1';
      st_i      <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge

      case st_i is
        when idle =>
          mtrsf_en  <= '0';
          mtrsf     <= '0';
          dstb_en_i <= '0';
          cnt_clr_i <= '1';
          if acq_rdo = '1' then
            st_i    <= mem_read;
          else
            st_i    <= idle;            -- stay in state
          end if;

        when mem_read =>
          mtrsf_en  <= '1';
          mtrsf     <= '0';
          dstb_en_i <= '0';
          cnt_clr_i <= '0';
          st_i      <= last_mem_cycle;

        when last_mem_cycle =>
          mtrsf_en  <= '1';
          mtrsf     <= '1';
          dstb_en_i <= '1';
          cnt_clr_i <= '0';
          if cnt_i = max_i then
            st_i    <= last_dstb_trans;
          else
            st_i    <= last_mem_cycle;  -- stay in state
          end if;

        when last_dstb_trans =>
          mtrsf_en  <= '1';
          mtrsf     <= '1';
          dstb_en_i <= '0';
          cnt_clr_i <= '0';
          st_i      <= close_trans;

        when close_trans =>
          mtrsf_en  <= '1';
          mtrsf     <= '1';
          dstb_en_i <= '0';
          cnt_clr_i <= '1';
          st_i      <= close_trans_en;

        when close_trans_en =>
          mtrsf_en  <= '1';
          mtrsf     <= '0';
          dstb_en_i <= '0';
          cnt_clr_i <= '1';
          st_i      <= idle;

        when others =>
          mtrsf_en  <= '0';
          mtrsf     <= '0';
          dstb_en_i <= '0';
          cnt_clr_i <= '1';
          st_i      <= idle;
      end case;
    end if;
  end process fsm;

  -- purpose: Counter
  -- type   : sequential
  -- inputs : clk, rstb, cnt_clr_i
  -- outputs: cnt_i
  counter: process (clk, rstb) is
  begin  -- process counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if cnt_clr_i = '1' then
        cnt_i <= (others => '0');
      else
        cnt_i <= cnt_i + 1;
      end if;
    end if;
  end process counter;

  -- purpose: next address
  -- type   : sequential
  -- inputs : clk, rstb, cnt_i, dstb_en_i
  -- outputs: mem_rdaddr
  address: process (clk, rstb) is
  begin  -- process address
    if rstb = '0' then                  -- asynchronous reset (active low)
      mem_rdaddr <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if dstb_en_i = '1' then
        mem_rdaddr <= std_logic_vector(cnt_i);
      end if;
    end if;
  end process address;

  -- purpose: Make data strobe
  -- type   : sequential
  -- inputs : clk, rstb, dstb_en_i
  -- outputs: dstb_i
  data_strobe: process (clk, rstb) is
  begin  -- process data_strobe
    if rstb = '0' then                  -- asynchronous reset (active low)
      dstb_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if dstb_en_i = '1' then
        dstb_i <= '1';
      else
        dstb_i <= '0';
      end if;
    end if;
  end process data_strobe;
end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package mem_rdo_pack is
  component mem_rdo
    port (
      clk        : in  std_logic;       -- Clock
      rstb       : in  std_logic;       -- Async reset
      acq_rdo    : in  std_logic;       -- Command: Start readout
      tsm_word   : in  std_logic_vector(8 downto 0);  -- TSM: mask
      mem_rdaddr : out std_logic_vector(8 downto 0);  -- Read out address
      mtrsf_en   : out std_logic;       -- TSM: Enable transfer
      mtrsf      : out std_logic;       -- TSM: transfer
      dstb       : out std_logic);      -- TSM : data strobe
  end component mem_rdo;
end package mem_rdo_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
