------------------------------------------------------------------------------
-- Title      : Board controller core
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : bc_core.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2010-06-15
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
--
-- Description: Piece together the core of the board controller
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.interfacedec_pack.all;
use work.altrobusinterface_pack.all;
use work.registers_pack.all;
use work.tsm_decoder_pack.all;
use work.altro_sw_mask_out_pack.all;
use work.interface_adc_pack.all;
use work.tsm_man_pack.all;
use work.evl_man_pack.all;
use work.trsf_mux_pack.all;
use work.slave_pack.all;

entity bc_core is
  port (
    -- Control bus
    clk            : in    std_logic;   -- Clock
    sclk           : in    std_logic;   -- Slow clock
    rstb           : in    std_logic;   -- Async reset
    cstb           : in    std_logic;   -- Control strobe
    write          : in    std_logic;   -- Write flag
    dstb           : in    std_logic;   -- Data strobe
    trsf           : in    std_logic;   -- Data transfer
    l1_trg         : in    std_logic;   -- L1 trigger
    l2_trg         : in    std_logic;   -- L2 trigger
    ackn           : in    std_logic;   -- Acknowledge
    hadd           : in    std_logic_vector(4 downto 0);   -- Card address
    -- bus
    din            : in    std_logic_vector(39 downto 0);  -- ALTRO bus
    -- I2C input
    scl            : in    std_logic;   -- I2C clock
    sda_in         : in    std_logic;   -- I2C serial data in
    sda_out        : out   std_logic;   -- I2C serial data out
    slctr          : out   std_logic;   -- I2C serial control
    -- Misc. input
    paps_error     : in    std_logic;   -- Pasa power supp. error
    alps_error     : in    std_logic;   -- ALTRO power supp. error
    al_err         : in    std_logic;   -- ALTRO error
    debug_sw       : in    std_logic;   -- Debug switch
    -- Bus interface 
    bc_ackn_en     : out   std_logic;   -- Acknowledge enable
    bc_ackn        : out   std_logic;   -- Acknowledge
    lastst_al      : out   std_logic;   -- Last state bus interface
    endtrans       : out   std_logic;   -- End of transmission
    bc_dolo_en     : out   std_logic;   -- GTL out enable
    bc_cs          : out   std_logic;   -- BC card select
    al_cs          : out   std_logic;   -- ALTRO card select
    wr_al          : out   std_logic;   -- Write ALTRO
    dout           : out   std_logic_vector(15 downto 0);  -- Output data
    bc_int         : out   std_logic;   -- Interrupt
    bc_error       : out   std_logic;   -- BC error
    -- Misc HW output
    altro_sw       : out   std_logic;   -- ALTRO switch
    pasa_sw        : out   std_logic;   -- PASA switch
    al_rst         : out   std_logic;   -- ALTRO reset
    rdoclk_en      : out   std_logic;   -- Readout clock enable
    adcclk_en      : out   std_logic;   -- ADC clock enable
    adc_add0       : out   std_logic;   -- ADC address 0
    adc_add1       : out   std_logic;   -- ADC address 1
    tsm_on         : out   std_logic;   -- Test mode on
    card_isolation : out   std_logic;   -- Card isolated
    test_a         : out   std_logic;   -- Test word A
    test_b         : out   std_logic;   -- Test word B
    test_c         : out   std_logic;   -- Test word C
    test_d         : out   std_logic;   -- Test word D
    test_e         : out   std_logic;   -- Test word E
    test_f         : out   std_logic;   -- Test word F
    test_g         : out   std_logic;   -- Test word G
    test_h         : out   std_logic;   -- Test word H
    -- Monitor interface
    mscl           : out   std_logic;   -- Monitor clock
    msda           : inout std_logic;   -- Monitor serial data out
    -- Test mode, event length interface 
    rmtrsf_en      : out   std_logic;   -- R/M transfer enable
    rmtrsf         : out   std_logic;   -- R/M transfer
    rmdstb         : out   std_logic;   -- R/M data strobe
    rmdata_out     : out   std_logic_vector(39 downto 0);  -- R/M data
    mem_wren       : out   std_logic;   -- TSM: memory write
    sclk_edge      : out   std_logic;   -- Slow clock edge
    evl_cstb       : out   std_logic;   -- EVL: control strobe
    alevl_rdtrx    : out   std_logic;   -- EVL: read/write
    bc_master      : out   std_logic;   -- EVL: BC is master
    evl_addr       : out   std_logic_vector(39 downto 20);  -- EVL: addr
    -- FMD 
    fmdd_stat      : in    std_logic_vector(15 downto 0);  -- FMDD status
    l0             : in    std_logic;   -- L0 trigger (for counter)
    hold_wait      : out   std_logic_vector(15 downto 0);  -- FMD: Wait to hold
    l1_timeout     : out   std_logic_vector(15 downto 0);  -- FMD: L1 timeout
    l2_timeout     : out   std_logic_vector(15 downto 0);  -- FMD: L2 timeout
    shift_div      : out   std_logic_vector(15 downto 0);  -- FMD: Shift clk
    strips         : out   std_logic_vector(15 downto 0);  -- FMD: Strips
    cal_level      : out   std_logic_vector(15 downto 0);  -- FMD: Cal pulse
    shape_bias     : out   std_logic_vector(15 downto 0);  -- FMD: Shape bias
    vfs            : out   std_logic_vector(15 downto 0);  -- FMD: Shape ref
    vfp            : out   std_logic_vector(15 downto 0);  -- FMD: Preamp ref
    sample_div     : out   std_logic_vector(15 downto 0);  -- FMD: Sample clk
    fmdd_cmd       : out   std_logic_vector(15 downto 0);  -- FMD: Commands
    cal_iter       : out   std_logic_vector(15 downto 0);  -- FMD: events
    cal_delay      : out   std_logic_vector(15 downto 0);  -- CAL: xtra L1 delay
    -- Multi event buffer interface
    mebs           : out   std_logic_vector(4 downto 0);   -- MEB config
    meb_cnt        : in    std_logic_vector(3 downto 0);   -- MEB counter
    al_rpinc       : out   std_logic;   -- ALTRO Read* inc.
    -- Misc
    debug          : out   std_logic_vector(15 downto 0));  -- Debug pins

end bc_core;

------------------------------------------------------------------------------
--! This architecture (implementation) of the bc_core entity does not contain
--! code for "Test Mode" or "Scan Event Length".  It's a simplification of the
--! rtl architecture - KISS (Keep It Simple Stupid).
architecture rtl2 of bc_core is
  signal add_al_i      : std_logic_vector(6 downto 0);  -- Instruction code
  signal wr_bc_i       : std_logic;     -- Write for BC
  signal bc_ackn_en_i  : std_logic;     -- Acknowledge enable
  -- signal bc_ackn_i  : std_logic;     -- Acknowledge 
  signal valid_i       : std_logic;     -- Valid instruction 
  signal lastst_al_i   : std_logic;     -- Last state
  signal bcast_al_i    : std_logic;     -- Bus: Broadcast
  signal ierr_al_i     : std_logic;     -- Bus: Instruction error
  signal wadd_i        : std_logic_vector(6 downto 0);  -- Bus: Decoded address
  signal wdata_i       : std_logic_vector(15 downto 0);  -- Bus: Decoded data
  signal we_reg_i      : std_logic;     -- Bus: Write enable
  signal par_error_i   : std_logic;     -- Error: Parity error
  signal add_sc_i      : std_logic_vector(6 downto 0);  -- I2C: Address
  signal data_sc_i     : std_logic_vector(15 downto 0);  -- I2C: Data 
  signal wr_sc_i       : std_logic;     -- I2C: Write enable
  signal bcast_sc_i    : std_logic;     -- I2C: Broadcast
  signal ierr_sc_i     : std_logic;     -- I2C: Insruction error
  signal slctr_i       : std_logic;     -- I2C: take control
  signal exec_i        : std_logic;     -- Execute
  signal cnt_lat_i     : std_logic;     -- Command, latch counters
  signal cnt_clr_i     : std_logic;     -- Command, clear counters
  signal csr1_clr_i    : std_logic;     -- Command, clear CSR1
  signal bc_rst_i      : std_logic;     -- Command, reset BC
  signal st_cnv_i      : std_logic;     -- Command, start conversion
  signal csr2_i        : std_logic_vector(15 downto 0);  -- Config/Status 2
  -- signal csr3_i        : std_logic_vector(15 downto 0);  -- Config/Status 3
  signal start_mon_i   : std_logic;     -- ADC: Enable interfaceadc
  signal end_seq_i     : std_logic;     -- ADC: end of Monitor
  signal add_adc_i     : std_logic_vector(4 downto 0);  -- ADC: Monitor address
  signal data_adc_i    : std_logic_vector(15 downto 0);  -- ADC: Monitor data
  signal we_adc_i      : std_logic;     -- ADC: Monitor write enable
  signal cnv_mode_i    : std_logic;     -- ADC: Continous conversion
  signal dout_al_i     : std_logic_vector(15 downto 0);  -- Output data
  signal dout_sc_i     : std_logic_vector(15 downto 0);  -- Output data
  signal al_rpinc_i    : std_logic;     -- ALTRO read ptr++
  signal adc_state_i   : std_logic_vector(4 downto 0);  -- ADC I2C master state
  signal fmdd_stat_i   : std_logic_vector(15 downto 0);
  signal slave_state_i : std_logic_vector(10 downto 0);
  signal bc_int_i      : std_logic;     -- CTRL: Interrupt
  signal altro_sw_i    : std_logic_vector(3 downto 0);  -- Cascading register
  
begin  -- rtl2
  combi : block
  begin  -- block combi
    -- Signals that should be driven always at the same value, since the
    -- test-mode and scan event-lenght feature is gone.
    adc_add0       <= '0';
    adc_add1       <= '0';
    tsm_on         <= '0';
    card_isolation <= '0';
    test_a         <= '1';
    test_b         <= '1';
    test_c         <= '1';
    test_d         <= '1';
    test_e         <= '1';
    test_f         <= '1';
    test_g         <= '1';
    test_h         <= '1';
    rmtrsf_en      <= '0';
    rmtrsf         <= '0';
    rmdstb         <= '0';
    rmdata_out     <= (others => '0');
    mem_wren       <= '0';
    sclk_edge      <= '0';
    evl_cstb       <= '0';
    alevl_rdtrx    <= '0';
    bc_master      <= '0';
    bc_int         <= bc_int_i;
    evl_addr       <= (others => '0');         -- "00000000000000010001";
    -- Other signals
    bc_ackn_en     <= bc_ackn_en_i;            --! Ctrl: Enable acknowledge
    valid_i        <= not ierr_al_i;           --! Bus: Valid instruction
    lastst_al      <= lastst_al_i;             --! Last state
    altro_sw       <= '1';                     --! Never turn off altro_sw
    --altro_sw <= '0' when (altro_sw_i(3) = '1' and
    --                      altro_sw_i(2) = '1' and
    --                      altro_sw_i(1) = '1' and
    --                      altro_sw_i(0) = '1') else '1';
    pasa_sw        <= csr2_i(1) and debug_sw;  --! Pasa switch
    start_mon_i    <= cnv_mode_i or st_cnv_i;  --! Start monitor flag.
    dout           <= dout_al_i;               --! Output data 
    bcast_sc_i     <= '0';
    rdoclk_en      <= '1';                     --! Never turn off the rdo clk.
    adcclk_en      <= '1';                     --! Never turn on the adc clk.
    slctr          <= slctr_i;
    al_rpinc       <= al_rpinc_i;
    fmdd_stat_i    <= fmdd_stat;
    -- fmdd_stat_i(4 downto 0)  <= adc_state_i;
    -- fmdd_stat_i(15 downto 5) <= slave_state_i;    -- fmdd_stat(15 downto 5);

    -- Debug output
    debug(10 downto 0)  <= slave_state_i;
    debug(15 downto 11) <= (others => '0');
  end block combi;

  -- purpose: Turn off power regulators in case of an interrupt.
  -- type   : sequential
  -- inputs : clk, rstb, bc_int
  -- outputs: altro_sw
  power_off : process (clk, rstb)
  begin  -- process power_off
    if rstb = '0' then                  -- asynchronous reset (active low)
      altro_sw_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bc_int_i = '1' and csr2_i(0) = '1' then
        altro_sw_i(0) <= altro_sw_i(1);
        altro_sw_i(1) <= altro_sw_i(2);
        altro_sw_i(2) <= altro_sw_i(3);
        altro_sw_i(3) <= '1';
      else
        altro_sw_i <= (others => '0');
      end if;
    end if;
  end process power_off;

  bus_interface : altrobusinterface
    port map (
      clk        => clk,                -- Ctrl: Clock
      rstb       => rstb,               -- Ctrl: Async reset
      cstb       => cstb,               -- Ctrl: Control strobe
      write      => write,              -- Ctrl: Write enable
      ackn_en_if => bc_ackn_en_i,       -- Ctrl: Acknowledge output
      ackn_if    => bc_ackn,            -- Ctrl: Acknowledge output
      wr_al      => wr_al,              -- Ctrl: Write enable
      wr_bc      => wr_bc_i,            -- Ctrl: Write enabled
      valid      => valid_i,            -- Validate instruction 
      din        => din(39 downto 20),  -- Bus: code 
      icode      => add_al_i,           -- Bus: Instruction code
      bcast      => bcast_al_i,         -- Bus: broadcast
      hadd       => hadd,               -- Hardware address
      exec       => exec_i,             -- Execute flag
      lastst_al  => lastst_al_i,        -- Last state
      endtrans   => endtrans,           -- End of transmission flag
      bc_dolo_en => bc_dolo_en,         -- GTL: Enable output
      par_error  => par_error_i,        -- Error: Parity error
      bc_cs      => bc_cs,              -- Card selected for BC
      al_cs      => al_cs,              -- Card selected for ALTRO
      al_rpinc   => al_rpinc_i);        -- ALTRO read-pointer increment

  i2c_slave : entity work.slave(rtl_all)
    port map (
      clk      => clk,                  -- in  Clock
      rstb     => rstb,                 -- in  Async reset
      scl      => scl,                  -- in  Serial clock (max 5 MHz)
      sda_in   => sda_in,               -- in  Input serial data
      hadd     => hadd,                 -- in  Hardware address
      data_tx  => dout_sc_i,            -- in  Data to output
      bc_rst   => bc_rst_i,             -- in  Command: Reset BC
      csr1_clr => csr1_clr_i,           -- in  Command: Clear CSR1
      sda_out  => sda_out,              -- out Output serial data
      reg_add  => add_sc_i,             -- out Register address
      data_rx  => data_sc_i,            -- out Data read from bus
      slctr    => slctr_i,              -- out I2C-slave take control
      we       => wr_sc_i,              -- out Write enable
      ierr_sc  => ierr_sc_i,            -- out Error : Instruction error
      state    => slave_state_i);       -- out States

  decoder : interfacedec
    port map (
      clk        => clk,                -- Ctrl: Clock
      rstb       => rstb,               -- Ctrl: Async reset
      wr_al      => wr_bc_i,            -- Ctrl: Write enable
      add_al     => add_al_i,           -- Bus: Instruction code
      data_al    => din(15 downto 0),   -- Bus: Data
      bcast_al   => bcast_al_i,         -- Bus: Broadcast
      ackn_en_if => bc_ackn_en_i,       -- Bus: Acknowledge enable out
      ierr_al    => ierr_al_i,          -- Error, Bus: instruction error
      add_sc     => add_sc_i,           -- I2C: Address
      data_sc    => data_sc_i,          -- I2C: Data
      wr_sc      => wr_sc_i,            -- I2C: Write enable
      bcast_sc   => bcast_sc_i,         -- I2C: Broadcast
      slctr      => slctr_i,            -- I2C: control
      ierr_sc    => ierr_sc_i,          -- Error, I2C: Instruction error
      exec       => exec_i,             -- Execute flag
      lastst_al  => lastst_al_i,        -- Last state on ALTRO
      cnt_lat    => cnt_lat_i,          -- Command: Latch counters
      cnt_clr    => cnt_clr_i,          -- Command: Clear counters
      csr1_clr   => csr1_clr_i,         -- Command: Clear CSR1
      al_rst     => al_rst,             -- Command: Reset ALTROs
      bc_rst     => bc_rst_i,           -- Command: Reset BC
      st_cnv     => st_cnv_i,           -- Command: Start conversion
      sc_evl     => open,               -- Command: Scan event length
      evl_rdo    => open,               -- Command: Event length readout
      st_tsm     => open,               -- Command: Start test mode
      acq_rdo    => open,               -- Command: Do acquisition and readout
      add        => wadd_i,             -- Register address
      data       => wdata_i,            -- Bus: Output data
      we_reg     => we_reg_i);          -- Write enable to registers

  monitors : interface_adc
    port map (
      clk      => clk,                  -- Ctrl: clock
      rstb     => rstb,                 -- Ctrl: Async reset
      stcnv    => start_mon_i,          -- Command: start monitor
      we_adc   => we_adc_i,             -- Write enable from ADC
      end_seq  => end_seq_i,            -- End-of monitoring flag
      wadd_adc => add_adc_i,            -- Register address of ADC data
      mscl     => mscl,                 -- Monitor: clock
      msda     => msda,                 -- Monitor: Data
      data_adc => data_adc_i,           -- Data from ADC
      state    => adc_state_i); 

  regs : entity work.registers
    port map (
      clk         => clk,               -- in  Clock
      sclk        => al_rpinc_i,        -- in  Changed meaning in counters
      rstb        => rstb,              -- in  Async reset
      -- Write 
      we          => we_reg_i,          -- in  Write enable
      wadd        => wadd_i,            -- in  Input write address 
      wdata       => wdata_i,           -- in  Input write data
      -- Read 
      add_al      => add_al_i,          -- in  Input read address from AL bus
      add_sc      => add_sc_i,          -- in  Input read address from SC bus
      dout_al     => dout_al_i,         -- out Data output
      dout_sc     => dout_sc_i,         -- out Data output
      -- Monitor ADCs
      end_seq     => end_seq_i,         -- in  End of Monitor flag
      we_adc      => we_adc_i,          -- in  Monitor adc write enable
      add_adc     => add_adc_i,         -- in  Monitor ADC address
      data_adc    => data_adc_i,        -- in  Monitor data
      cnv_mode    => cnv_mode_i,        -- out Continous conversion flag
      -- Misc inputs for registers
      fmdd_stat   => fmdd_stat_i,       -- in  FMDD status
      l0          => l0,                -- in  L0 trigger (for counter)
      l1_trg      => l1_trg,            -- in  L1 trigger
      l2_trg      => l2_trg,            -- in  L2 trigger
      dstb        => dstb,              -- in  Data strobe
      al_trsf     => trsf,              -- in  Data transfer
      par_error   => par_error_i,       -- in  Error: Parity 
      paps_error  => paps_error,        -- in  Error: PASA power sup. error
      alps_error  => alps_error,        -- in  Error: ALTRO power sup. error
      ierr_sc     => ierr_sc_i,         -- in  Error: instruction from I2C
      ierr_al     => ierr_al_i,         -- in  Error: instruction from bus
      al_error    => al_err,            -- in  Error: from ALTRO
      bc_rst      => bc_rst_i,          -- in  Command: Reset BC
      csr1_clr    => csr1_clr_i,        -- in  Command: Clear Config/status 1
      cnt_lat     => cnt_lat_i,         -- in  Command: Latch counters
      cnt_clr     => cnt_clr_i,         -- in  Command: Clear counters
      st_cnv      => st_cnv_i,          -- in  Command: start conversion
      -- Misc
      hadd        => hadd,              -- in  Card addreess
      -- Output stuff from registers
      missed_sclk => open,              -- out Error: Missed slow clock
      bc_int      => bc_int_i,          -- out Error: BC interrupt
      bc_error    => bc_error,          -- out Error : BC error
      csr2        => csr2_i,            -- out Config/Status 2
      csr3        => open,  -- csr3_i,          -- out Config/Status 3
      tsm_word    => open,              -- out Test mode word
      us_ratio    => open,              -- out Under sampling ratio
      hold_wait   => hold_wait,         -- out FMD: Wait to hold
      l1_timeout  => l1_timeout,        -- out FMD: L1 timeout
      l2_timeout  => l2_timeout,        -- out FMD: L2 timeout
      shift_div   => shift_div,         -- out FMD: Shift clk
      strips      => strips,            -- out FMD: Strips
      cal_level   => cal_level,         -- out FMD: Cal pulse
      shape_bias  => shape_bias,        -- out FMD: Shape bias
      vfs         => vfs,               -- out FMD: Shape ref
      vfp         => vfp,               -- out FMD: Preamp ref
      sample_div  => sample_div,        -- out FMD: Sample clk
      fmdd_cmd    => fmdd_cmd,          -- out FMD: Commands
      cal_iter    => cal_iter,          -- out FMD: Calib events
      mebs        => mebs,              -- out MEB: config
      meb_cnt     => meb_cnt,           -- out MEB: counter
      cal_delay   => cal_delay);        -- out CAL: xtra L1 delay
end rtl2;

------------------------------------------------------------------------------
architecture rtl of bc_core is
  signal add_al_i      : std_logic_vector(6 downto 0);  -- Instruction code
  signal wr_bc_i       : std_logic;     -- Write for BC
  signal bc_ackn_en_i  : std_logic;     -- Acknowledge enable
  -- signal bc_ackn_i  : std_logic;     -- Acknowledge 
  signal valid_i       : std_logic;     -- Valid instruction 
  signal lastst_al_i   : std_logic;     -- Last state
  signal bcast_al_i    : std_logic;     -- Bus: Broadcast
  signal ierr_al_i     : std_logic;     -- Bus: Instruction error
  signal wadd_i        : std_logic_vector(6 downto 0);  -- Bus: Decoded address
  signal wdata_i       : std_logic_vector(15 downto 0);  -- Bus: Decoded data
  signal we_reg_i      : std_logic;     -- Bus: Write enable
  signal altro_sw_i    : std_logic;     -- ALTRO switch
  signal missed_sclk_i : std_logic;     -- Error: Missed slow clock
  signal par_error_i   : std_logic;     -- Error: Parity error
  signal add_sc_i      : std_logic_vector(6 downto 0);  -- I2C: Address
  signal data_sc_i     : std_logic_vector(15 downto 0);  -- I2C: Data 
  signal wr_sc_i       : std_logic;     -- I2C: Write enable
  signal bcast_sc_i    : std_logic;     -- I2C: Broadcast
  signal ierr_sc_i     : std_logic;     -- I2C: Insruction error
  signal slctr_i       : std_logic;     -- I2C: take control
  signal exec_i        : std_logic;     -- Execute
  signal cnt_lat_i     : std_logic;     -- Command, latch counters
  signal cnt_clr_i     : std_logic;     -- Command, clear counters
  signal csr1_clr_i    : std_logic;     -- Command, clear CSR1
  signal bc_rst_i      : std_logic;     -- Command, reset BC
  signal st_cnv_i      : std_logic;     -- Command, start conversion
  signal sc_evl_i      : std_logic;     -- Command, scan event length
  signal evl_rdo_i     : std_logic;     -- Command, event length r/o
  signal st_tsm_i      : std_logic;     -- Command, start test mode
  signal acq_rdo_i     : std_logic;     -- Command, ACQ
  signal csr2_i        : std_logic_vector(15 downto 0);  -- Config/Status 2
  signal csr3_i        : std_logic_vector(15 downto 0);  -- Config/Status 3
  signal start_mon_i   : std_logic;     -- ADC: Enable interfaceadc
  signal end_seq_i     : std_logic;     -- ADC: end of Monitor
  signal add_adc_i     : std_logic_vector(4 downto 0);  -- ADC: Monitor address
  signal data_adc_i    : std_logic_vector(15 downto 0);  -- ADC: Monitor data
  signal we_adc_i      : std_logic;     -- ADC: Monitor write enable
  signal cnv_mode_i    : std_logic;     -- ADC: Continous conversion
  signal tsm_word_i    : std_logic_vector(8 downto 0);  -- TSM: Test mode word
  signal us_ratio_i    : std_logic_vector(15 downto 0);  -- TSM: undersample
  signal tsm_dec_en_i  : std_logic;     -- TSM: decode enable
  signal tsm_acqon_i   : std_logic;     -- TSM: Acquisition is on
  signal tsm_isol_i    : std_logic;     -- TSM: Card is isolated
  signal tsm_out_i     : std_logic_vector(7 downto 0);  -- TSM decode out
  signal mtrsf_en_i    : std_logic;     -- TSM: transfer enable
  signal mtrsf_i       : std_logic;     -- TSM: Transfer
  signal mdstb_i       : std_logic;     -- TSM: Data strobe
  signal mdata_out_i   : std_logic_vector(39 downto 0);  -- TSM: data
  signal rtrsf_en_i    : std_logic;     -- EVL: Transfer enable
  signal rtrsf_i       : std_logic;     -- EVL: Transfer
  signal rdstb_i       : std_logic;     -- EVL: Data strobe
  signal rdata_out_i   : std_logic_vector(39 downto 0);  -- EVL: data
  signal rmtrsf_en_i   : std_logic;     -- TSM/EVL: Transfer enable
  signal trsf_i        : std_logic;     -- Bus/TSM/EVL: Transfer
  signal dout_al_i     : std_logic_vector(15 downto 0);  -- Output data
  signal dout_sc_i     : std_logic_vector(15 downto 0);  -- Output data
  
begin  -- rtl
  combi : block
  begin  -- block combi
    bc_ackn_en   <= bc_ackn_en_i;              -- Ctrl: Enable acknowledge
    valid_i      <= not ierr_al_i;             -- Bus: Valid instruction
    lastst_al    <= lastst_al_i;               -- Last state
    altro_sw_i   <= csr2_i(0) and debug_sw;    -- ALTRO switch
    altro_sw     <= altro_sw_i;                -- ALTRO switch
    pasa_sw      <= csr2_i(1) and debug_sw;    -- Pasa switch
    tsm_dec_en_i <= tsm_acqon_i or csr2_i(9);  -- PASA switch
    start_mon_i  <= cnv_mode_i or st_cnv_i;    -- Start monitor flag.
    rmtrsf_en    <= rmtrsf_en_i;               -- TSM/RO: Transfer enable
    trsf_i       <= not rmtrsf_en_i or trsf;
    dout         <= dout_al_i;                 -- Output data 
    bcast_sc_i   <= '0';
    rdoclk_en    <= '1';                       -- Never turn off the rclk.
    slctr        <= slctr_i;
    debug        <= (others => '0');
  end block combi;


  bus_interface : altrobusinterface
    port map (
      clk        => clk,                -- Ctrl: Clock
      rstb       => rstb,               -- Ctrl: Async reset
      cstb       => cstb,               -- Ctrl: Control strobe
      write      => write,              -- Ctrl: Write enable
      ackn_en_if => bc_ackn_en_i,       -- Ctrl: Acknowledge output
      ackn_if    => bc_ackn,            -- Ctrl: Acknowledge output
      wr_al      => wr_al,              -- Ctrl: Write enable
      wr_bc      => wr_bc_i,            -- Ctrl: Write enabled
      valid      => valid_i,            -- Validate instruction 
      din        => din(39 downto 20),  -- Bus: code 
      icode      => add_al_i,           -- Bus: Instruction code
      bcast      => bcast_al_i,         -- Bus: broadcast
      hadd       => hadd,               -- Hardware address
      exec       => exec_i,             -- Execute flag
      lastst_al  => lastst_al_i,        -- Last state
      endtrans   => endtrans,           -- End of transmission flag
      bc_dolo_en => bc_dolo_en,         -- GTL: Enable output
      par_error  => par_error_i,        -- Error: Parity error
      bc_cs      => bc_cs,              -- Card selected for BC
      al_cs      => al_cs,              -- Card selected for ALTRO
      al_rpinc   => al_rpinc);          -- ALTRO read-pointer increment

  i2c_slave : entity work.slave
    port map (
      clk      => clk,                  -- in  Clock
      rstb     => rstb,                 -- in  Async reset
      scl      => scl,                  -- in  Serial clock (max 5 MHz)
      sda_in   => sda_in,               -- in  Input serial data
      hadd     => hadd,                 -- in  Hardware address
      data_tx  => dout_sc_i,            -- in  Data to output
      bc_rst   => bc_rst_i,             -- in  Command: Reset BC
      csr1_clr => csr1_clr_i,           -- in  Command: Clear CSR1
      sda_out  => sda_out,              -- out Output serial data
      reg_add  => add_sc_i,             -- out Register address
      data_rx  => data_sc_i,            -- out Data read from bus
      slctr    => slctr_i,              -- out I2C-slave take control
      we       => wr_sc_i,              -- out Write enable
      ierr_sc  => ierr_sc_i);           -- out Error : Instruction error

  decoder : interfacedec
    port map (
      clk        => clk,                -- Ctrl: Clock
      rstb       => rstb,               -- Ctrl: Async reset
      wr_al      => wr_bc_i,            -- Ctrl: Write enable
      add_al     => add_al_i,           -- Bus: Instruction code
      data_al    => din(15 downto 0),   -- Bus: Data
      bcast_al   => bcast_al_i,         -- Bus: Broadcast
      ackn_en_if => bc_ackn_en_i,       -- Bus: Acknowledge enable out
      data       => wdata_i,            -- Bus: Output data
      ierr_al    => ierr_al_i,          -- Error, Bus: instruction error
      add_sc     => add_sc_i,           -- I2C: Address
      data_sc    => data_sc_i,          -- I2C: Data
      wr_sc      => wr_sc_i,            -- I2C: Write enable
      bcast_sc   => bcast_sc_i,         -- I2C: Broadcast
      slctr      => slctr_i,            -- I2C: control
      ierr_sc    => ierr_sc_i,          -- Error, I2C: Instruction error
      exec       => exec_i,             -- Execute flag
      lastst_al  => lastst_al_i,        -- Last state on ALTRO
      cnt_lat    => cnt_lat_i,          -- Command: Latch counters
      cnt_clr    => cnt_clr_i,          -- Command: Clear counters
      csr1_clr   => csr1_clr_i,         -- Command: Clear CSR1
      al_rst     => al_rst,             -- Command: Reset ALTROs
      bc_rst     => bc_rst_i,           -- Command: Reset BC
      st_cnv     => st_cnv_i,           -- Command: Start conversion
      sc_evl     => sc_evl_i,           -- Command: Scan event length
      evl_rdo    => evl_rdo_i,          -- Command: Event length readout
      st_tsm     => st_tsm_i,           -- Command: Start test mode
      acq_rdo    => acq_rdo_i,          -- Command: Do acquisition and readout
      add        => wadd_i,             -- Register Address
      we_reg     => we_reg_i);          -- Write enable to registers

  monitors : interface_adc
    port map (
      clk      => clk,                  -- Ctrl: clock
      rstb     => rstb,                 -- Ctrl: Async reset
      stcnv    => start_mon_i,          -- Command: start monitor
      we_adc   => we_adc_i,             -- Write enable from ADC
      end_seq  => end_seq_i,            -- End-of monitoring flag
      wadd_adc => add_adc_i,            -- Register address of ADC data
      mscl     => mscl,                 -- Monitor: clock
      msda     => msda,                 -- Monitor: Data
      data_adc => data_adc_i);          -- Data from ADC

  regs : entity work.registers
    port map (
      clk         => clk,               -- in  Clock
      sclk        => sclk,              -- in  Slow (10Mhz) clock
      rstb        => rstb,              -- in  Async reset
      -- Write
      we          => we_reg_i,          -- in  Write enable
      wadd        => wadd_i,            -- in  Address 
      wdata       => wdata_i,           -- in  Input data
      -- Read 
      add_al      => add_al_i,          -- in  Input read address from AL bus
      add_sc      => add_sc_i,          -- in  Input read address from SC bus
      dout_al     => dout_al_i,         -- out Data output
      dout_sc     => dout_sc_i,         -- out Data output
      -- Conters
      l0          => l0,                -- in  L0 trigger (for counter)
      l1_trg      => l1_trg,            -- in  L1 trigger
      l2_trg      => l2_trg,            -- in  L2 trigger
      dstb        => dstb,              -- in  Data strobe
      al_trsf     => trsf_i,            -- in  Data transfer
      -- Errors
      par_error   => par_error_i,       -- in  Error: Parity 
      paps_error  => paps_error,        -- in  Error: PASA power sup. error
      alps_error  => alps_error,        -- in  Error: ALTRO power sup. error
      ierr_sc     => ierr_sc_i,         -- in  Error: instruction from I2C
      ierr_al     => ierr_al_i,         -- in  Error: instruction from bus
      al_error    => al_err,            -- in  Error: from ALTRO
      missed_sclk => missed_sclk_i,     -- out Error: Missed slow clock
      bc_int      => bc_int,            -- out Error: BC interrupt
      bc_error    => bc_error,          -- out Error : BC error
      fmdd_stat   => fmdd_stat,         -- in  FMDD status
      -- Commands
      bc_rst      => bc_rst_i,          -- in  Command: Reset BC
      csr1_clr    => csr1_clr_i,        -- in  Command: Clear Config/status 1
      cnt_lat     => cnt_lat_i,         -- in  Command: Latch counters
      cnt_clr     => cnt_clr_i,         -- in  Command: Clear counters
      st_cnv      => st_cnv_i,          -- in  Command: start conversion
      -- Monitor ADC
      we_adc      => we_adc_i,          -- in  Monitor adc write enable
      add_adc     => add_adc_i,         -- in  Monitor ADC address
      data_adc    => data_adc_i,        -- in  Monitor data
      cnv_mode    => cnv_mode_i,        -- out Continous conversion flag
      end_seq     => end_seq_i,         -- in  End of Monitor flag
      -- Misc
      hadd        => hadd,              -- in  Card addreess
      -- Output of registers
      csr2        => csr2_i,            -- out Config/Status 2
      csr3        => csr3_i,            -- out Config/Status 3
      tsm_word    => tsm_word_i,        -- out Test mode word
      us_ratio    => us_ratio_i,        -- out Under sampling ratio
      hold_wait   => hold_wait,         -- out FMD: Wait to hold
      l1_timeout  => l1_timeout,        -- out FMD: L1 timeout
      l2_timeout  => l2_timeout,        -- out FMD: L2 timeout
      shift_div   => shift_div,         -- out FMD: Shift clk
      strips      => strips,            -- out FMD: Strips
      cal_level   => cal_level,         -- out FMD: Cal pulse
      shape_bias  => shape_bias,        -- out FMD: Shape bias
      vfs         => vfs,               -- out FMD: Shape ref
      vfp         => vfp,               -- out FMD: Preamp ref
      sample_div  => sample_div,        -- out FMD: Sample clk
      fmdd_cmd    => fmdd_cmd,          -- out FMD: Commands
      cal_iter    => cal_iter,          -- out FMD: Calib events
      mebs        => mebs,              -- out MEB: config
      meb_cnt     => meb_cnt,           -- out MEB: counter
      cal_delay   => cal_delay);

  tsm_manager : tsm_man
    port map (
      clk         => clk,               -- in  Clock
      sclk        => sclk,              -- in  Slow clock (10MHz)
      rstb        => rstb,              -- in  Async reset
      st_tsm      => st_tsm_i,          -- in  Command: Start test mode
      din         => din,               -- in  Data in
      acq_rdo     => acq_rdo_i,         -- in  Command: Acquire and read-out
      missed_sclk => missed_sclk_i,     -- in  Error: Missing sclk
      us_ratio    => us_ratio_i,        -- in  TSM: Under sampling
      tsm_word    => tsm_word_i,        -- in  TSM: mask
      mem_wren    => mem_wren,          -- out TSM: Memory write enable
      tsm_acqon   => tsm_acqon_i,       -- out TSM: aquiring
      tsm_isol    => tsm_isol_i,        -- out TSM: Card isolated
      sclk_edge   => sclk_edge,         -- out TSM: Edge of sclk detected
      data_out    => mdata_out_i,       -- out TSM: Output data
      mtrsf_en    => mtrsf_en_i,        -- out TSM: transfer enable
      mtrsf       => mtrsf_i,           -- out TSM: transfer
      mdstb       => mdstb_i);          -- out TSM : data strobe

  evl_manager : evl_man
    port map (
      clk         => clk,               -- in  Clock
      rstb        => rstb,              -- in  Async reset
      evl_rdo     => evl_rdo_i,         -- in  Command: Read out event length
      sc_evl      => sc_evl_i,          -- in  Command: Scan event length
      ackn        => ackn,              -- in  Ctrl: acknowledge from ALTRO's
      alm_wd      => csr3_i(14 downto 8),  -- in  Watch-dog, time-out on CSTB
      al_evl      => din(7 downto 0),   -- in  ALTRO event length mask
      hadd        => hadd,              -- in  Hardware address
      evl_cstb    => evl_cstb,          -- out EVL: control strobe
      evltrsf_en  => rtrsf_en_i,        -- out EVL length transfer enable
      evl_trsf    => rtrsf_i,           -- out EVL: transfer
      evlreg_dstb => rdstb_i,           -- out EVL: registered data strobe
      evl_word    => rdata_out_i,       -- out EVL: output
      evl_addr    => evl_addr,          -- out EVL: Address
      alevl_rdtrx => alevl_rdtrx,       -- out EVL: Read transfer
      bc_master   => bc_master);        -- out Make BC master of local bus

  out_multiplex : trsf_mux
    port map (
      mtrsf_en   => mtrsf_en_i,         -- in  TSM: transfer enable
      mtrsf      => mtrsf_i,            -- in  TSM: Transfer
      mdstb      => mdstb_i,            -- in  TSM: Data strobe
      mdata_out  => mdata_out_i,        -- in  TSM: data
      rtrsf_en   => rtrsf_en_i,         -- in  EVL: Transfer enable
      rtrsf      => rtrsf_i,            -- in  EVL: Transfer
      rdstb      => rdstb_i,            -- in  EVL: Data strobe
      rdata_out  => rdata_out_i,        -- in  EVL: data
      rmtrsf_en  => rmtrsf_en_i,        -- out TSM/EVL: transfer enable
      rmtrsf     => rmtrsf,             -- out TSM/EVL: transfer
      rmdstb     => rmdstb,             -- out TSM/EVL: Data strobe
      rmdata_out => rmdata_out);        -- out TSM/EVL : data

  tsm_dec : tsm_decoder
    port map (
      clk     => clk,                   -- Ctrl: clock
      rstb    => rstb,                  -- Ctrl: Async reset
      tsm_in  => csr2_i(8 downto 6),    -- Test mode config
      enable  => tsm_dec_en_i,          -- Enable test mode
      tsm_out => tsm_out_i);            -- Test mode word masked

  al_sw_mask : altro_sw_mask_out
    port map (
      altro_sw       => altro_sw_i,           -- ALTRO switch
      csr2           => csr2_i(10 downto 0),  -- Config/Status register 2
      tsm_out        => tsm_out_i,            -- Test mode word masked
      tsm_acqon      => tsm_acqon_i,          -- Do test-mode acquistion
      tsm_isol       => tsm_isol_i,           -- Isolated
      rdoclk_en      => open,  -- rdoclk_en,    -- Enable read-out clock
      adcclk_en      => adcclk_en,            -- Enable ADC clock
      adc_add0       => adc_add0,             -- ADC address bit 0
      adc_add1       => adc_add1,             -- ADC address bit 1
      tsm_on         => tsm_on,               -- Test mode is on
      card_isolation => card_isolation,       -- Isolated card
      test_a         => test_a,               -- Test mode bit A
      test_b         => test_b,               -- Test mode bit B
      test_c         => test_c,               -- Test mode bit C
      test_d         => test_d,               -- Test mode bit D
      test_e         => test_e,               -- Test mode bit E
      test_f         => test_f,               -- Test mode bit F
      test_g         => test_g,               -- Test mode bit G
      test_h         => test_h);              -- Test mode bit H

end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package bc_core_pack is
  component bc_core
    port (
      clk            : in    std_logic;  -- Clock
      sclk           : in    std_logic;  -- Slow clock
      rstb           : in    std_logic;  -- Async reset
      cstb           : in    std_logic;  -- Control strobe
      write          : in    std_logic;  -- Write flag
      dstb           : in    std_logic;  -- Data strobe
      trsf           : in    std_logic;  -- Data transfer
      l1_trg         : in    std_logic;  -- L1 trigger
      l2_trg         : in    std_logic;  -- L2 trigger
      ackn           : in    std_logic;  -- Acknowledge
      hadd           : in    std_logic_vector(4 downto 0);   -- Card address
      din            : in    std_logic_vector(39 downto 0);  -- ALTRO bus
      scl            : in    std_logic;  -- I2C clock
      sda_in         : in    std_logic;  -- I2C serial data in
      sda_out        : out   std_logic;  -- I2C serial data out
      slctr          : out   std_logic;  -- I2C serial control
      paps_error     : in    std_logic;  -- Pasa power supp. error
      alps_error     : in    std_logic;  -- ALTRO power supp. error
      al_err         : in    std_logic;  -- ALTRO error
      debug_sw       : in    std_logic;  -- Debug switch
      bc_ackn_en     : out   std_logic;  -- Acknowledge enable
      bc_ackn        : out   std_logic;  -- Acknowledge
      lastst_al      : out   std_logic;  -- Last state bus interface
      endtrans       : out   std_logic;  -- End of transmission
      bc_dolo_en     : out   std_logic;  -- GTL out enable
      bc_cs          : out   std_logic;  -- BC card select
      al_cs          : out   std_logic;  -- ALTRO card select
      wr_al          : out   std_logic;  -- Write ALTRO
      dout           : out   std_logic_vector(15 downto 0);  -- Output data
      bc_int         : out   std_logic;  -- Interrupt
      bc_error       : out   std_logic;  -- BC error
      altro_sw       : out   std_logic;  -- ALTRO switch
      pasa_sw        : out   std_logic;  -- PASA switch
      al_rst         : out   std_logic;  -- ALTRO reset
      rdoclk_en      : out   std_logic;  -- Readout clock enable
      adcclk_en      : out   std_logic;  -- ADC clock enable
      adc_add0       : out   std_logic;  -- ADC address 0
      adc_add1       : out   std_logic;  -- ADC address 1
      tsm_on         : out   std_logic;  -- Test mode on
      card_isolation : out   std_logic;  -- Card isolated
      test_a         : out   std_logic;  -- Test word A
      test_b         : out   std_logic;  -- Test word B
      test_c         : out   std_logic;  -- Test word C
      test_d         : out   std_logic;  -- Test word D
      test_e         : out   std_logic;  -- Test word E
      test_f         : out   std_logic;  -- Test word F
      test_g         : out   std_logic;  -- Test word G
      test_h         : out   std_logic;  -- Test word H
      mscl           : out   std_logic;  -- Monitor clock
      msda           : inout std_logic;  -- Monitor serial data out
      rmtrsf_en      : out   std_logic;  -- R/M transfer enable
      rmtrsf         : out   std_logic;  -- R/M transfer
      rmdstb         : out   std_logic;  -- R/M data strobe
      rmdata_out     : out   std_logic_vector(39 downto 0);  -- R/M data
      mem_wren       : out   std_logic;  -- TSM: memory write
      sclk_edge      : out   std_logic;  -- Slow clock edge
      evl_cstb       : out   std_logic;  -- EVL: control strobe
      alevl_rdtrx    : out   std_logic;  -- EVL: read/write
      bc_master      : out   std_logic;  -- EVL: BC is master
      evl_addr       : out   std_logic_vector(39 downto 20);  -- EVL: addr
      fmdd_stat      : in    std_logic_vector(15 downto 0);  -- FMDD status
      l0             : in    std_logic;  -- L0 trigger (for counter)
      hold_wait      : out   std_logic_vector(15 downto 0);  -- FMD: Wait hold
      l1_timeout     : out   std_logic_vector(15 downto 0);  -- FMD: L1 timeout
      l2_timeout     : out   std_logic_vector(15 downto 0);  -- FMD: L2 timeout
      shift_div      : out   std_logic_vector(15 downto 0);  -- FMD: Shift clk
      strips         : out   std_logic_vector(15 downto 0);  -- FMD: Strips
      cal_level      : out   std_logic_vector(15 downto 0);  -- FMD: Cal pulse
      shape_bias     : out   std_logic_vector(15 downto 0);  -- FMD: Shape bias
      vfs            : out   std_logic_vector(15 downto 0);  -- FMD: Shape ref
      vfp            : out   std_logic_vector(15 downto 0);  -- FMD: Preamp ref
      sample_div     : out   std_logic_vector(15 downto 0);  -- FMD: Sample clk
      fmdd_cmd       : out   std_logic_vector(15 downto 0);  -- FMD: Commands
      cal_iter       : out   std_logic_vector(15 downto 0);
      cal_delay      : out   std_logic_vector(15 downto 0);  -- CAL: xtra L1
      mebs           : out   std_logic_vector(4 downto 0);   -- MEB config
      meb_cnt        : in    std_logic_vector(3 downto 0);   -- MEB counter
      al_rpinc       : out   std_logic;  -- ALTRO Read* inc.
      debug          : out   std_logic_vector(15 downto 0));
  end component bc_core;
end bc_core_pack;
------------------------------------------------------------------------------
--
-- EOF
--
