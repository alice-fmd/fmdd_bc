------------------------------------------------------------------------------
-- Title      : Read event length from a channel
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : read_evl.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Read event length from a channel
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity read_evl is
  port (
    clk      : in  std_logic;           -- Clock
    rstb     : in  std_logic;           -- Async reset
    chrd_st  : in  std_logic;           -- Start read-out of channel
    ackn     : in  std_logic;           -- Acknowledge from ALTRO
    alm_wd   : in  std_logic_vector(6 downto 0);
                                        -- Watch dog rclk cycle intervention
    cstb     : out std_logic;           -- Control strobe
    shift_en : out std_logic;           -- Shift enable
    accept   : out std_logic;           -- Accept this channel
    next_ch  : out std_logic);          -- Request next channel
end entity read_evl;

architecture rtl of read_evl is
  type state_t is (idle, read_in, store, skip, wait_ack, next_chan);  -- States
  signal st_i       : state_t;          -- State
  signal nx_st_i    : state_t;          -- Next state
  signal cnt_i      : unsigned(6 downto 0);  -- Counter;
  signal clr_cnt_i  : std_logic;        -- Clear counter
  signal en_cnt_i   : std_logic;        -- Enable counter
  signal cstb_i     : std_logic;
  signal watchdog_i : unsigned(6 downto 0);

begin  -- architecture  rtl
  watchdog_i <= unsigned(alm_wd);
  
  -- purpose: update the next state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st_i
  -- outputs: st_i
  next_state : process (clk, rstb) is
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;


  -- purpose: Get the next state
  -- type   : combinational
  -- inputs : st_i, chrd_st, sckn, cnt_i, alm_wd
  -- outputs: cstb_i, shift_en, clr_cnt_i, en_cnt_i, accept, next_ch
  fsm : process (st_i, chrd_st, ackn, cnt_i, alm_wd) is
  begin  -- process fsm
    case st_i is
      when idle =>
        cstb_i    <= '0';
        shift_en  <= '0';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        accept    <= '0';
        next_ch   <= '0';
        if chrd_st = '1' then
          nx_st_i <= read_in;
        else
          nx_st_i <= idle;
        end if;

      when read_in =>
        cstb_i    <= '1';
        shift_en  <= '0';
        clr_cnt_i <= '0';
        en_cnt_i  <= '1';
        accept    <= '0';
        next_ch   <= '0';
        if ackn = '1' then
          nx_st_i <= store;
        elsif cnt_i = watchdog_i then
          nx_st_i <= skip;
        else
          nx_st_i <= read_in;
        end if;

      when store =>
        cstb_i    <= '0';
        shift_en  <= '1';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        accept    <= '1';
        next_ch   <= '0';
        nx_st_i   <= wait_ack;

      when skip =>
        cstb_i    <= '0';
        shift_en  <= '1';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        accept    <= '0';
        next_ch   <= '0';
        nx_st_i   <= wait_ack;

      when wait_ack =>
        cstb_i    <= '0';
        shift_en  <= '0';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        accept    <= '0';
        next_ch   <= '0';
        if ackn = '1' then
          nx_st_i <= wait_ack;
        else
          nx_st_i <= next_chan;
        end if;

      when next_chan =>
        cstb_i    <= '0';
        shift_en  <= '0';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        accept    <= '0';
        next_ch   <= '1';
        nx_st_i   <= idle;

      when others =>
        cstb_i    <= '0';
        shift_en  <= '0';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        accept    <= '0';
        next_ch   <= '0';
        nx_st_i   <= idle;

    end case;
  end process fsm;

  -- purpose: Watch dog couter
  -- type   : sequential
  -- inputs : clk, rstb, clr_cnt_i, en_cnt_i
  -- outputs: cnt_i
  wdog_counter : process (clk, rstb) is
  begin  -- process wdog_counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i     <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
        if clr_cnt_i = '1' then
          cnt_i <= (others => '0');
        elsif en_cnt_i = '1' then
          cnt_i <= cnt_i + 1;
        end if;
      end if;
  end process wdog_counter;

  -- purpose: Syncronise CSTB
  -- type   : sequential
  -- inputs : clk, rstb, cstb_i
  -- outputs: cstb
  cstb_out : process (clk, rstb) is
  begin  -- process cstb_out
    if rstb = '0' then                  -- asynchronous reset (active low)
      cstb <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      cstb <= cstb_i;
    end if;
  end process cstb_out;
end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package read_evl_pack is
  component read_evl
    port (
      clk      : in  std_logic;         -- Clock
      rstb     : in  std_logic;         -- Async reset
      chrd_st  : in  std_logic;         -- Start read-out of channel
      ackn     : in  std_logic;         -- Acknowledge from ALTRO
      alm_wd   : in  std_logic_vector(6 downto 0);
      cstb     : out std_logic;         -- Control strobe
      shift_en : out std_logic;         -- Shift enable
      accept   : out std_logic;         -- Accept this channel
      next_ch  : out std_logic);        -- Request next channel
  end component read_evl;
end package read_evl_pack;

------------------------------------------------------------------------------
-- 
-- EOF
--
