------------------------------------------------------------------------------
-- Title      : Test bench of read event length 
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : read_evl_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/18
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test bench of read event length 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm   Created
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.read_evl_pack.all;

------------------------------------------------------------------------------
entity read_evl_tb is
end entity read_evl_tb;

------------------------------------------------------------------------------

architecture tes of read_evl_tb is
  constant PERIOD : time := 25 ns;

  signal clk_i      : std_logic                    := '0';  -- Clock
  signal rstb_i     : std_logic                    := '0';  -- Async reset
  signal chrd_st_i  : std_logic                    := '0';
                                        -- Start read-out of channel
  signal ackn_i     : std_logic                    := '0';
                                        -- Acknowledge from ALTRO
  signal alm_wd_i   : std_logic_vector(6 downto 0) := "0001111";
  signal cstb_i     : std_logic;        -- Control strobe
  signal shift_en_i : std_logic;        -- Shift enable
  signal accept_i   : std_logic;        -- Accept this channel
  signal next_ch_i  : std_logic;        -- Request next channel

begin  -- architecture tes

  dut : entity work.read_evl
    port map (
      clk      => clk_i,                -- in  Clock
      rstb     => rstb_i,               -- in  Async reset
      chrd_st  => chrd_st_i,            -- in  Start read-out of channel
      ackn     => ackn_i,               -- in  Acknowledge from ALTRO
      alm_wd   => alm_wd_i,             -- in  
      cstb     => cstb_i,               -- out Control strobe
      shift_en => shift_en_i,           -- out Shift enable
      accept   => accept_i,             -- out Accept this channel
      next_ch  => next_ch_i);           -- out Request next channel


  -- purpose: Make stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli : process is
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';
    wait for 100 ns;

    wait until rising_edge(clk_i);
    chrd_st_i <= '1';
    wait until rising_edge(clk_i);
    chrd_st_i <= '0';

    wait until cstb_i = '1';
    ackn_i <= '1';
    wait until rising_edge(clk_i);
    wait until rising_edge(clk_i);
    ackn_i <= '0';

    wait;                               -- forever
  end process stimuli;

  -- purpose: make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_gen : process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;
end architecture tes;

------------------------------------------------------------------------------
