------------------------------------------------------------------------------
-- Title      : Test of event length read-out
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : evl_man_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/18
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Tests the BC-helper mode for read-out.
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm	Created
------------------------------------------------------------------------------

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.evl_man_pack.all;

------------------------------------------------------------------------------
entity evl_man_tb is
end entity evl_man_tb;

------------------------------------------------------------------------------

architecture test of evl_man_tb is
  constant PERIOD : time := 25 ns;      -- Clock cycle

  signal clk_i         : std_logic := '0';  -- Clock
  signal rstb_i        : std_logic := '0';  -- Async reset
  signal evl_rdo_i     : std_logic := '0';  -- Command: Read out event length
  signal sc_evl_i      : std_logic := '0';  -- Command: Scan event length
  signal ackn_i        : std_logic := '0';  -- Ctrl: acknowledge from ALTRO's
  signal alm_wd_i      : std_logic_vector(6 downto 0);
  signal al_evl_i      : std_logic_vector(7 downto 0);  -- ALTRO ev. len. mask
  signal hadd_i        : std_logic_vector(4 downto 0);  -- Hardware address
  signal evl_cstb_i    : std_logic;     -- EVL: control strobe
  signal evltrsf_en_i  : std_logic;     -- EVL length transfer enable
  signal evl_trsf_i    : std_logic;     -- EVL: transfer
  signal evlreg_dstb_i : std_logic;     -- EVL: registered data strobe
  signal evl_word_i    : std_logic_vector(39 downto 0);  -- EVL: output
  signal evl_addr_i    : std_logic_vector(39 downto 20);  -- EVL: Address
  signal alevl_rdtrx_i : std_logic;     -- EVL: Read transfer
  signal bc_master_i   : std_logic;     -- Make BC master of local bus
  signal i             : integer;

begin  -- architecture test

  dut: entity work.evl_man
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        evl_rdo     => evl_rdo_i,       -- in  Command: Read out event length
        sc_evl      => sc_evl_i,        -- in  Command: Scan event length
        ackn        => ackn_i,          -- in  Ctrl: acknowledge from ALTRO's
        alm_wd      => alm_wd_i,        -- in  
        al_evl      => al_evl_i,        -- in  ALTRO event length mask
        hadd        => hadd_i,          -- in  Hardware address
        evl_cstb    => evl_cstb_i,      -- out EVL: control strobe
        evltrsf_en  => evltrsf_en_i,    -- out EVL length transfer enable
        evl_trsf    => evl_trsf_i,      -- out EVL: transfer
        evlreg_dstb => evlreg_dstb_i,   -- out EVL: registered data strobe
        evl_word    => evl_word_i,      -- out EVL: output
        evl_addr    => evl_addr_i,      -- out EVL: Address
        alevl_rdtrx => alevl_rdtrx_i,   -- out EVL: Read transfer
        bc_master   => bc_master_i);    -- out Make BC master of local bus

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli : process is
  begin  -- process stimuli
    hadd_i   <= "00000";
    alm_wd_i <= "0000100";
    wait for 100 ns;
    rstb_i   <= '1';

    wait for 100 ns;
    wait until rising_edge(clk_i);
    sc_evl_i <= '1';
    wait until rising_edge(clk_i);
    sc_evl_i <= '0';

    for i in 0 to 127 loop
      wait until evl_cstb_i = '1';
      al_evl_i <= std_logic_vector(to_unsigned(i mod 4, 8));
      wait until rising_edge(clk_i);
      ackn_i <= '1';
      wait until evl_cstb_i = '0';
      wait until rising_edge(clk_i);
      ackn_i <= '0';
    end loop;  -- i

    wait until bc_master_i = '0';
    wait until rising_edge(clk_i);
    evl_rdo_i <= '1';
    wait until rising_edge(clk_i);
    evl_rdo_i <= '0';
    
    wait until evltrsf_en_i = '0';
    wait;                               -- forever 
  end process stimuli;

  -- purpose: Make clock
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  clk_gen: process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;

end architecture test;

------------------------------------------------------------------------------
-- 
-- EOF
--
