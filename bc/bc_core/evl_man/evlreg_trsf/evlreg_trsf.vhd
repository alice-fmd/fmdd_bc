------------------------------------------------------------------------------
-- Title      : Event length status register read-out
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : evlreg_trsf.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Event length status register read-out
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity evlreg_trsf is
  port (
    clk        : in  std_logic;                       -- Clock
    rstb       : in  std_logic;                       -- Async reset
    evl_rdo    : in  std_logic;                       -- Start read-out
    evl_reg    : in  std_logic_vector(127 downto 0);  -- Shift register
    evltrsf_en : out std_logic;                       -- Transfer enable
    trsf       : out std_logic;                       -- Transfer pulse
    dstb       : out std_logic;                       -- EVL: data strobe
    result     : out std_logic_vector(33 downto 0));  -- Result
end entity evlreg_trsf;

architecture rtl of evlreg_trsf is
  type state_t is (idle,
                   trsf_en_on,
                   trsf_on,
                   dstb1,
                   dstb2,
                   dstb3,
                   dstb4,
                   trsf_off,
                   trsf_en_off);        -- States
  signal st_i      : state_t;           -- State
  signal dstb_en_i : std_logic;         -- Enable DSTB

begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    -- dstb <= dstb_en_i and not clk;   -- this is tricky
    dstb <= dstb_en_i;                  -- FIXME: Need to make clockk
  end block combi;

  -- purpose: Make a read-out train
  -- type   : sequential
  -- inputs : clk, rstb, evl_rdo
  -- outputs: result
  fsm : process (clk, rstb) is
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      evltrsf_en <= '0';
      trsf       <= '0';
      result     <= (others => '0');
      dstb_en_i  <= '0';
      st_i       <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge

      case st_i is
        when idle =>
          evltrsf_en <= '0';
          trsf       <= '0';
          result     <= "00" & evl_reg(31 downto 0);
          dstb_en_i  <= '0';
          if evl_rdo = '1' then
            st_i     <= trsf_en_on;
          else
            st_i     <= idle;
          end if;

        when trsf_en_on =>
          evltrsf_en <= '1';
          trsf       <= '0';
          result     <= "00" & evl_reg(31 downto 0);
          dstb_en_i  <= '0';
          st_i       <= trsf_on;

        when trsf_on =>
          evltrsf_en <= '1';
          trsf       <= '1';
          result     <= "00" & evl_reg(31 downto 0);
          dstb_en_i  <= '0';
          st_i       <= dstb1;

        when dstb1 =>
          evltrsf_en <= '1';
          trsf       <= '1';
          result     <= "00" & evl_reg(31 downto 0);
          dstb_en_i  <= '1';
          st_i       <= dstb2;

        when dstb2 =>
          evltrsf_en <= '1';
          trsf       <= '1';
          result     <= "00" & evl_reg(63 downto 32);
          dstb_en_i  <= '1';
          st_i       <= dstb3;

        when dstb3 =>
          evltrsf_en <= '1';
          trsf       <= '1';
          result     <= "00" & evl_reg(95 downto 64);
          dstb_en_i  <= '1';
          st_i       <= dstb4;

        when dstb4 =>
          evltrsf_en <= '1';
          trsf       <= '1';
          result     <= "00" & evl_reg(127 downto 96);
          dstb_en_i  <= '1';
          st_i       <= trsf_off;

        when trsf_off =>
          evltrsf_en <= '1';
          trsf       <= '1';
          result     <= "00" & evl_reg(31 downto 0);
          dstb_en_i  <= '0';
          st_i       <= trsf_en_off;

        when trsf_en_off =>
          evltrsf_en <= '1';
          trsf       <= '0';
          result     <= "00" & evl_reg(31 downto 0);
          dstb_en_i  <= '0';
          st_i       <= idle;

        when others =>
          evltrsf_en <= '0';
          trsf       <= '0';
          result     <= "00" & evl_reg(31 downto 0);
          dstb_en_i  <= '0';
          st_i       <= idle;
      end case;
    end if;
  end process fsm;

end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package evlreg_trsf_pack is
  component evlreg_trsf
    port (
      clk        : in  std_logic;                       -- Clock
      rstb       : in  std_logic;                       -- Async reset
      evl_rdo    : in  std_logic;                       -- Start read-out
      evl_reg    : in  std_logic_vector(127 downto 0);  -- Shift register
      evltrsf_en : out std_logic;                       -- Transfer enable
      trsf       : out std_logic;                       -- Transfer pulse
      dstb       : out std_logic;                       -- EVL: data strobe
      result     : out std_logic_vector(33 downto 0));  -- Result
  end component evlreg_trsf;
end package evlreg_trsf_pack;

------------------------------------------------------------------------------
--
-- EOF
--
