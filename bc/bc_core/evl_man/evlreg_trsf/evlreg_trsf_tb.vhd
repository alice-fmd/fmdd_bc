------------------------------------------------------------------------------
-- Title      : Test bench of event lenght transferer
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : evlreg_trsf_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/18
-- Platform   : ACEX1K
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test bench of event lenght transferer
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm	Created
------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use work.evlreg_trsf_pack.all;

------------------------------------------------------------------------------

entity evlreg_trsf_tb is

end entity evlreg_trsf_tb;

------------------------------------------------------------------------------
architecture test of evlreg_trsf_tb is
  constant PERIOD : time := 25 ns;

  signal clk_i        : std_logic := '0';                -- Clock
  signal rstb_i       : std_logic := '0';                -- Async reset
  signal evl_rdo_i    : std_logic := '0';                -- Start read-out
  signal evl_reg_i    : std_logic_vector(127 downto 0);  -- Shift register
  signal evltrsf_en_i : std_logic;                       -- Transfer enable
  signal trsf_i       : std_logic;                       -- Transfer pulse
  signal dstb_i       : std_logic;                       -- EVL: data strobe
  signal result_i     : std_logic_vector(33 downto 0);   -- Result

begin  -- architecture test

  dut: entity work.evlreg_trsf
    port map (
        clk        => clk_i,            -- in  Clock
        rstb       => rstb_i,           -- in  Async reset
        evl_rdo    => evl_rdo_i,        -- in  Start read-out
        evl_reg    => evl_reg_i,        -- in  Shift register
        evltrsf_en => evltrsf_en_i,     -- out Transfer enable
        trsf       => trsf_i,           -- out Transfer pulse
        dstb       => dstb_i,           -- out EVL: data strobe
        result     => result_i);        -- out Result

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';

    evl_reg_i(31 downto 0)   <= X"aaaaaaaa";
    evl_reg_i(63 downto 32)  <= X"bbbbbbbb";
    evl_reg_i(95 downto 64)  <= X"cccccccc";
    evl_reg_i(127 downto 96) <= X"dddddddd";
    wait for 100 ns;

    wait until rising_edge(clk_i);
    evl_rdo_i <= '1';
    wait until rising_edge(clk_i);
    evl_rdo_i <= '0';

    wait until trsf_i = '0';

    wait;                               -- forever
  end process stimuli;

  -- purpose: make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_gen: process is
  begin  -- process clk_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clk_gen;
end architecture test;


------------------------------------------------------------------------------
-- 
-- EOF
--
