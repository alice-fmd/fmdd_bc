------------------------------------------------------------------------------
-- Title      : Counter of channels
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : ch_counter.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Counter of channels
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ch_counter is
  port (
    clk         : in  std_logic;        -- Clock
    rstb        : in  std_logic;        -- Async reset
    sc_evl      : in  std_logic;        -- Command: SCan EVent Length
    next_ch     : in  std_logic;        -- Enable for counter
    chrd_st     : out std_logic;        -- Enable
    ch_counter  : out std_logic_vector(6 downto 0);  -- Channel #
    alevl_rdtrx : out std_logic;        -- EVL: radout transfer
    bc_master   : out std_logic);       -- make BC master of local bus
end entity ch_counter;

--
-- First architecture - everything is registered.
--
architecture rtl of ch_counter is
  type state_t is (idle, isolate, start, output, inc, close);  -- States
  signal st_i  : state_t;                                      -- State;
  signal cnt_i : unsigned(6 downto 0);                         -- Counter

begin  -- architecture rtl
  -- purpose: combinatorics
  combi: block is
  begin  -- block combi
    ch_counter <= std_logic_vector(cnt_i);
  end block combi;

  -- purpose: State machine that outputs new channel #
  -- type   : sequential
  -- inputs : clk, rstb, sc_evl, next_ch, cnt_i
  -- outputs: chrd_st, clr_cnt_i, en_cnt_i, bc_master, alevl_rdtrx
  fsm : process (clk, rstb) is
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i        <= idle;
      chrd_st     <= '0';
      bc_master   <= '0';
      alevl_rdtrx <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge

      case st_i is
        when idle                =>
          chrd_st     <= '0';
          bc_master   <= '0';
          alevl_rdtrx <= '0';
          cnt_i       <= (others => '0');
          if sc_evl = '1' then
            st_i      <= isolate;
          else
            st_i      <= idle;
          end if;

        when isolate             =>
          chrd_st     <= '1';
          bc_master   <= '1';
          alevl_rdtrx <= '0';
          cnt_i       <= (others => '0');
          st_i        <= start;

        when start             =>
          chrd_st     <= '0';
          bc_master   <= '1';
          alevl_rdtrx <= '1';
          cnt_i       <= cnt_i;
          st_i        <= output;

        when output             =>
          chrd_st     <= '0';
          bc_master   <= '1';
          alevl_rdtrx <= '1';
          cnt_i       <= cnt_i;

          if next_ch = '1' and cnt_i /= "1111111" then
            st_i <= inc;
          elsif next_ch = '1' and cnt_i = "1111111" then
            st_i <= close;
          else
            st_i <= output;
          end if;

        when inc             =>
          chrd_st     <= '1';
          bc_master   <= '1';
          alevl_rdtrx <= '1';
          cnt_i       <= cnt_i + 1;
          st_i        <= start;

        when close             =>
          chrd_st     <= '0';
          bc_master   <= '1';
          alevl_rdtrx <= '0';
          cnt_i       <= (others => '0');
          st_i        <= idle;
          

        when others              =>
          chrd_st     <= '0';
          bc_master   <= '0';
          alevl_rdtrx <= '0';
          cnt_i       <= (others => '0');
          st_i        <= idle;

      end case;
    end if;
  end process fsm;
end architecture rtl;

--
-- Second architecture - original
--
architecture rtl2 of ch_counter is
  type state_t is (idle, isolate, start, output, inc, close);  -- States
  signal st_i      : state_t;           -- State;
  signal nx_st_i   : state_t;           -- next State;
  signal clr_cnt_i : std_logic;         -- Clear counter
  signal en_cnt_i  : std_logic;         -- Enable counter
  signal cnt_i     : unsigned(6 downto 0);  -- Counter

begin  -- architecture rtl2
  -- purpose: combinatorics
  combi: block is
  begin  -- block combi
    ch_counter <= std_logic_vector(cnt_i);
  end block combi;
  
  -- purpose: Switch to next state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st_i
  -- outputs: st_i
  next_state: process (clk, rstb) is
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;


  -- purpose: Get the next state
  -- type   : combinational
  -- inputs : st_i, sc_evl, next_ch, cnt_i
  -- outputs: clr_cnt_i, en_cnt_i, bc_master, chrd_st, alevl_rdtrx
  fsm: process (st_i, sc_evl, next_ch, cnt_i) is
  begin  -- process fsm
    case st_i is

      when idle =>
        chrd_st   <= '0';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        if sc_evl = '1' then
          nx_st_i <= isolate;
        else
          nx_st_i <= idle;
        end if;

      when isolate =>
        chrd_st   <= '0';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        nx_st_i   <= start;

      when start =>
        chrd_st   <= '1';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        nx_st_i   <= output;

      when output =>
        chrd_st   <= '0';
        clr_cnt_i <= '0';
        en_cnt_i  <= '0';
        if next_ch = '1' and cnt_i /= "1111111" then
          nx_st_i <= inc;
        elsif next_ch = '1' and cnt_i = "1111111" then
          nx_st_i <= close;
        else
          nx_st_i <= output;
        end if;

      when inc =>
        chrd_st   <= '0';
        clr_cnt_i <= '0';
        en_cnt_i  <= '1';
        nx_st_i   <= start;

      when close  =>
        chrd_st   <= '0';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        nx_st_i   <= idle;

      when others =>
        chrd_st   <= '0';
        clr_cnt_i <= '1';
        en_cnt_i  <= '0';
        nx_st_i   <= idle;        
    end case;
  end process fsm;

  -- purpose: Output control signals
  -- type   : sequential
  -- inputs : clk, rstb, st_i
  -- outputs: bc_master, alevl_rdtrx
  ctrl_out : process (clk, rstb) is
  begin  -- process ctrl_out
    if rstb = '0' then                  -- asynchronous reset (active low)
      bc_master   <= '0';
      alevl_rdtrx <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge

      case st_i is
        when idle =>
          bc_master   <= '0';
          alevl_rdtrx <= '0';

        when isolate =>
          bc_master   <= '1';
          alevl_rdtrx <= '0';

        when start =>
          bc_master   <= '1';
          alevl_rdtrx <= '1';

        when output =>
          bc_master   <= '1';
          alevl_rdtrx <= '1';

        when inc =>
          bc_master   <= '1';
          alevl_rdtrx <= '1';

        when close =>
          bc_master   <= '1';
          alevl_rdtrx <= '0';
          
        when others =>
          bc_master   <= '0';
          alevl_rdtrx <= '0';
      end case;
    end if;
  end process ctrl_out;

  -- purpose: Counter
  -- type   : sequential
  -- inputs : clk, rstb, clr_cnt_i, en_cnt_i
  -- outputs: cnt_i
  count_it: process (clk, rstb) is
  begin  -- process count_it
    if rstb = '0' then                    -- asynchronous reset (active low)
      cnt_i <= (others => '0');
    elsif clk'event and clk = '1' then    -- rising clock edge
      if clr_cnt_i = '1' then
        cnt_i <= (others => '0');
      elsif en_cnt_i = '1' then
        cnt_i <= cnt_i + 1;
      end if;
    end if;
  end process count_it;
end architecture rtl2;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package ch_counter_pack is
  component ch_counter
    port (
      clk         : in  std_logic;      -- Clock
      rstb        : in  std_logic;      -- Async reset
      sc_evl      : in  std_logic;      -- Command: SCan EVent Length
      next_ch     : in  std_logic;      -- Enable for counter
      chrd_st     : out std_logic;      -- Enable
      ch_counter  : out std_logic_vector(6 downto 0);  -- Channel #
      alevl_rdtrx : out std_logic;      -- EVL: radout transfer
      bc_master   : out std_logic);     -- make BC master of local bus
  end component ch_counter;
end package ch_counter_pack;

------------------------------------------------------------------------------
-- 
-- EOF
--
