------------------------------------------------------------------------------
-- Title      : Test bench of channel counter
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : ch_counter_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test it.  Note, that we have to architectures for this entity.
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------------------------------------------------------------------------------

entity ch_counter_tb is
end entity ch_counter_tb;

------------------------------------------------------------------------------

architecture test of ch_counter_tb is
  constant PERIOD : time    := 25 ns;
  constant MAX    : integer := 128;

  signal clk_i          : std_logic            := '0';  -- Clock
  signal rstb_i         : std_logic            := '0';  -- Async reset
  -- Command: SCan EVent Length
  signal sc_evl_i       : std_logic            := '0';
  signal next_ch_i      : std_logic            := '0';  -- Enable for counter
  signal chrd_st_i      : std_logic;    -- Enable
  signal ch_counter_i   : std_logic_vector(6 downto 0);  -- Channel #
  signal alevl_rdtrx_i  : std_logic;    -- EVL: radout transfer
  signal bc_master_i    : std_logic;    -- make BC master of local bus
  signal chrd_st_i2     : std_logic;    -- Enable
  signal ch_counter_i2  : std_logic_vector(6 downto 0);  -- Channel #
  signal alevl_rdtrx_i2 : std_logic;    -- EVL: radout transfer
  signal bc_master_i2   : std_logic;    -- make BC master of local bus
  signal cnt_i          : unsigned(6 downto 0) := (others => '0');
  signal i              : integer;

  -- for dut : ch_counter
  --   use entity work.ch_counter(rtl);

  -- for dut2 : ch_counter
  --   use entity work.ch_counter(rtl2);
  
  
begin  -- architecture test

  dut2: entity work.ch_counter(rtl2)
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        sc_evl      => sc_evl_i,        -- in  Command: SCan EVent Length
        next_ch     => next_ch_i,       -- in  Enable for counter
        chrd_st     => chrd_st_i2,       -- out Enable
        ch_counter  => ch_counter_i2,    -- out Channel #
        alevl_rdtrx => alevl_rdtrx_i2,   -- out EVL: radout transfer
        bc_master   => bc_master_i2);    -- out make BC master of local bus

  dut: entity work.ch_counter(rtl)
    port map (
        clk         => clk_i,           -- in  Clock
        rstb        => rstb_i,          -- in  Async reset
        sc_evl      => sc_evl_i,        -- in  Command: SCan EVent Length
        next_ch     => next_ch_i,       -- in  Enable for counter
        chrd_st     => chrd_st_i,       -- out Enable
        ch_counter  => ch_counter_i,    -- out Channel #
        alevl_rdtrx => alevl_rdtrx_i,   -- out EVL: radout transfer
        bc_master   => bc_master_i);    -- out make BC master of local bus
  
  -- purpose: Make stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 200 ns;
    rstb_i <= '1';
    wait for 200 ns;

    wait until rising_edge(clk_i);
    sc_evl_i <= '1';
    wait until rising_edge(clk_i);
    sc_evl_i <= '0';

    wait until chrd_st_i = '1';

    for i in 0 to MAX loop
      wait until rising_edge(clk_i);
      next_ch_i <= '1';
      wait until rising_edge(clk_i);
      next_ch_i <= '0';
      wait until chrd_st_i = '1' and chrd_st_i2 = '1';
      wait until rising_edge(clk_i);
      -- wait until rising_edge(clk_i);
    end loop;  -- i

    wait until bc_master_i = '0';
    
    wait;                               -- Forever
  end process stimuli;

  -- purpose: Make clock
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  clock_gen: process is
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;

end architecture test;

------------------------------------------------------------------------------
-- 
-- EOF
--
