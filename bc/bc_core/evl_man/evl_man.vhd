------------------------------------------------------------------------------
-- Title      : Event length readout manager
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : evl_man.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-07-21
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This component reads the event length, as stored in the ALTRO's
--              When the RCU issues the scan event length command to the BC,
--              the BC will go and ask each ALTRO (the channel counter below)
--              for it's last event length (register ADEVL, address 0x11).  If
--              the stored event length is larger than 1 (in 40 bit words),
--              then a '1' (XOR below) will be shifted into the shift
--              register.
--
--              When the RCU then issues the read-out event length command,
--              the BC will respond (via a TRSF, DSTB) with a bit pattern of
--              ALTRO chips that have more than 1 word stored.  this is done
--              by reading out 4 * 32bit words (the read-out component below)
--              from the shift register.   In this way, the RCU can optimise
--              what ALTRO's it needs to read out.    The point is, ofcourse,
--              that the BC's can in parallel query each of it's ALTRO's. 
--               
--              Note, this doesn't make much sense for the FMD, as
--              1: The detector is almost always `black'.
--              2: There are only 3 * 4 = 12 ALTRO's per RCU. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/18  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.read_evl_pack.all;
use work.ch_counter_pack.all;
use work.evlreg_trsf_pack.all;

entity evl_man is
  port (
    clk         : in  std_logic;        -- Clock
    rstb        : in  std_logic;        -- Async reset
    evl_rdo     : in  std_logic;        -- Command: Read out event length
    sc_evl      : in  std_logic;        -- Command: Scan event length
    ackn        : in  std_logic;        -- Ctrl: acknowledge from ALTRO's
    alm_wd      : in  std_logic_vector(6 downto 0);
    al_evl      : in  std_logic_vector(7 downto 0);  -- ALTRO event length mask
    hadd        : in  std_logic_vector(4 downto 0);  -- Hardware address
    evl_cstb    : out std_logic;        -- EVL: control strobe
    evltrsf_en  : out std_logic;        -- EVL length transfer enable
    evl_trsf    : out std_logic;        -- EVL: transfer
    evlreg_dstb : out std_logic;        -- EVL: registered data strobe
    evl_word    : out std_logic_vector(39 downto 0);  -- EVL: output
    evl_addr    : out std_logic_vector(39 downto 20);  -- EVL: Address
    alevl_rdtrx : out std_logic;        -- EVL: Read transfer
    bc_master   : out std_logic);       -- Make BC master of local bus
end entity evl_man;

architecture rtl of evl_man is
  signal al_evl_or_i : std_logic;       -- Or of al_evl(7 downto 1)
  signal chrd_st_i   : std_logic;       -- Start reading
  signal next_ch_i   : std_logic;       -- Next channel
  signal accept_i    : std_logic;       -- Accept this
  signal shift_in_i  : std_logic;       -- Shift in bit to shift reg.  
  signal shift_en_i  : std_logic;       -- Shift in bit to shift reg.  
  signal ch_cnt_i    : std_logic_vector(6 downto 0);
  signal parity_i    : std_logic;
  signal result_i    : std_logic_vector(33 downto 0);
  signal evl_reg_i   : std_logic_vector(127 downto 0);  -- Buffer

begin  -- architecture rtl
  -- purpose: collect combinatorics
  combi : block is
  begin  -- block combi
    al_evl_or_i <= (al_evl(7) or al_evl(6) or al_evl(5) or al_evl(4) or
                    al_evl(3) or al_evl(2) or al_evl(1));
    shift_in_i  <= al_evl_or_i and accept_i;
    parity_i    <= (hadd(4) xor hadd(3) xor hadd(2) xor hadd(1) xor hadd(0) xor
                    ch_cnt_i(6) xor ch_cnt_i(5) xor ch_cnt_i(4) xor
                    ch_cnt_i(3) xor ch_cnt_i(2) xor ch_cnt_i(1) xor
                    ch_cnt_i(0));
    evl_addr    <= (parity_i & "00" & hadd & ch_cnt_i & "10001");
    evl_word    <= hadd & '0' & result_i;
  end block combi;

  -- purpose: Shift register
  -- type   : sequential
  -- inputs : clk, rstb, shift_in_i, shift_en_i
  -- outputs: evl_reg_i
  shift_it         : process (clk, rstb)
    variable tmp_i : std_logic_vector(127 downto 0);
  begin  -- process shift_it
    if rstb = '0' then                -- asynchronous reset (active low)
      evl_reg_i <= (others => '0');
      tmp_i := (others        => '0');

    elsif clk'event and clk = '1' then  -- rising clock edge
      if shift_en_i = '1' then          -- Right shift
        tmp_i(126 downto 0) := tmp_i(127 downto 1);
        tmp_i(127)          := shift_in_i;
      elsif sc_evl = '1' then
        tmp_i   := (others => '0');
      end if;
      evl_reg_i <= tmp_i;
    end if;
  end process shift_it;

  ch_cnt : ch_counter
    port map (
        clk         => clk,             -- in  Clock
        rstb        => rstb,            -- in  Async reset
        sc_evl      => sc_evl,          -- in  Command: SCan EVent Length
        next_ch     => next_ch_i,       -- in  Enable for counter
        chrd_st     => chrd_st_i,       -- out Enable
        ch_counter  => ch_cnt_i,        -- out Channel #
        alevl_rdtrx => alevl_rdtrx,     -- out EVL: radout transfer
        bc_master   => bc_master);      -- out make BC master of local bus


  read_it : read_evl
    port map (
        clk      => clk,                -- in  Clock
        rstb     => rstb,               -- in  Async reset
        chrd_st  => chrd_st_i,          -- in  Start read-out of channel
        ackn     => ackn,               -- in  Acknowledge from ALTRO
        alm_wd   => alm_wd,             -- in  Watch dog rclk
        cstb     => evl_cstb,           -- out Control strobe
        shift_en => shift_en_i,         -- out Shift enable
        accept   => accept_i,           -- out Accept this channel
        next_ch  => next_ch_i);         -- out Request next channel

  transfer : evlreg_trsf
    port map (
        clk        => clk,              -- in  Clock
        rstb       => rstb,             -- in  Async reset
        evl_rdo    => evl_rdo,          -- in  Start read-out
        evl_reg    => evl_reg_i,        -- in  Shift register
        evltrsf_en => evltrsf_en,       -- out Transfer enable
        trsf       => evl_trsf,         -- out Transfer pulse
        dstb       => evlreg_dstb,      -- out EVL: data strobe
        result     => result_i);        -- out Result

end architecture rtl;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package evl_man_pack is
  component evl_man
    port (
      clk         : in  std_logic;      -- Clock
      rstb        : in  std_logic;      -- Async reset
      evl_rdo     : in  std_logic;      -- Command: Read out event length
      sc_evl      : in  std_logic;      -- Command: Scan event length
      ackn        : in  std_logic;      -- Ctrl: acknowledge from ALTRO's
      alm_wd      : in  std_logic_vector(6 downto 0);
      al_evl      : in  std_logic_vector(7 downto 0);  -- ALTRO ev. length mask
      hadd        : in  std_logic_vector(4 downto 0);  -- Hardware address
      evl_cstb    : out std_logic;      -- EVL: control strobe
      evltrsf_en  : out std_logic;      -- EVL length transfer enable
      evl_trsf    : out std_logic;      -- EVL: transfer
      evlreg_dstb : out std_logic;      -- EVL: registered data strobe
      evl_word    : out std_logic_vector(39 downto 0);  -- EVL: output
      evl_addr    : out std_logic_vector(39 downto 20);  -- EVL: Address
      alevl_rdtrx : out std_logic;      -- EVL: Read transfer
      bc_master   : out std_logic);     -- Make BC master of local bus
  end component evl_man;
end package evl_man_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
