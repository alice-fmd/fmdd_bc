------------------------------------------------------------------------------
-- Title      : ALTRO switch mask of signals
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : altro_sw_mask_in.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2008-07-24
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: This component splits out the bus signals in BC and ALTRO
--              signals  
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/21  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity altro_sw_mask_in is
  port (
    clk         : in  std_logic;        -- Clock
    rstb        : in  std_logic;        -- Async reset
    altro_sw    : in  std_logic;        -- ALTRO switch
    alps_errorb : in  std_logic;        -- ALTRO power supply error from HW
    l1b         : in  std_logic;        -- Level 1 (-)
    l2b         : in  std_logic;        -- Level 2 (-)
    cstbb       : in  std_logic;        -- Control strobe (-)
    writeb      : in  std_logic;        -- Write (-)
    al_dolo_enb : in  std_logic;        -- ALTRO GTL enable (-)
    al_trsf_enb : in  std_logic;        -- ALTRO Transfer enable (-)
    trsfb       : in  std_logic;        -- Transfer
    dstbb       : in  std_logic;        -- Data strobe (-)
    or_rstb     : in  std_logic;        -- Sync or Async reset (-)
    acknb       : in  std_logic;        -- Acknowledge (-)
    hadd        : in  std_logic_vector(4 downto 0);  -- Card address
    pasa_sw     : in  std_logic;        -- PASA switch
    paps_errorb : in  std_logic;        -- PASA power sup. error (-)
    al_errorb   : in  std_logic;        -- ALTRO error (-)
    alps_error  : out std_logic;        -- ALTRO power sup. error (+)
    l1          : out std_logic;        -- Level 1 (+)
    l2          : out std_logic;        -- Level 2 (+)
    cstb        : out std_logic;        -- Control strobe (+)
    write       : out std_logic;        -- Write (+)
    al_dolo_en  : out std_logic;        -- ALTRO data transfer enable (+)
    al_trsf_en  : out std_logic;        -- ALTRO transfer enable (+)
    trsf        : out std_logic;        -- Transfer
    dstb        : out std_logic;        -- Data strobe
    ackn        : out std_logic;        -- Acknowledge (+)
    ackn_chrdo  : out std_logic;        -- Acknowledge during readout (+)
    rst_fbc     : out std_logic;        -- Front-end reset
    bcout_add   : out std_logic_vector(4 downto 0);  -- Output hardware address
    paps_error  : out std_logic;        -- PASA power supl. error (+)
    al_error    : out std_logic);       -- ALTRO error (+)
end entity altro_sw_mask_in;

architecture rtl of altro_sw_mask_in is
  signal cstb_i         : std_logic;
  signal cstb_ii        : std_logic;
  signal ackn_i         : std_logic;
  signal ackn_ii        : std_logic;
  signal ackn_chrdo_i   : std_logic;
  signal alps_error_i   : std_logic;
  signal alps_error_ii  : std_logic;
  signal alps_error_iii : std_logic;
  signal paps_error_i   : std_logic;
  signal paps_error_ii  : std_logic;
  signal paps_error_iii : std_logic;
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    l1           <= altro_sw and not l1b;
    l2           <= altro_sw and not l2b;
    write        <= altro_sw and not writeb;
    al_dolo_en   <= altro_sw and not al_dolo_enb;
    al_trsf_en   <= altro_sw and not al_trsf_enb;
    trsf         <= altro_sw and not trsfb;
    rst_fbc      <= altro_sw and or_rstb;
    dstb         <= altro_sw and not dstbb;
    cstb_i       <= altro_sw and not cstbb;
    ackn_i       <= altro_sw and not acknb;
    -- ackn_chrdo   <= ackn_chrdo_i;
    al_error     <= altro_sw and not al_errorb;
    alps_error_i <= altro_sw and not alps_errorb;
    paps_error_i <= altro_sw and not paps_errorb;
    bcout_add    <= hadd when altro_sw = '1' else (others => '0');
    alps_error   <= alps_error_ii and alps_error_iii;
    paps_error   <= paps_error_ii and paps_error_iii;
  end block combi;

  -- purpose: ackn 2 clock cycle metastability filter
  -- type   : sequential
  -- inputs : clk, rstb, ackn_i
  -- outputs: ackn, ack_chrdo
  ack_stability : process (clk, rstb) is
  begin  -- process ack_stability
    if rstb = '0' then                  -- asynchronous reset (active low)
      ackn_chrdo_i <= '0';
      ackn         <= '0';
      ackn_chrdo   <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      ackn_chrdo_i <= ackn_i;
      ackn         <= ackn_chrdo_i;
      ackn_chrdo   <= ackn_chrdo_i;
    end if;
  end process ack_stability;
  -- purpose: Meta stability filter onn the CSTB (2 clock cycles)
  -- type   : sequential
  -- inputs : clk, rstb, cstb_i
  -- outputs: cstb, cstb_ii
  ms_cstb : process (clk, rstb) is
  begin  -- process ms_cstb
    if rstb = '0' then                  -- asynchronous reset (active low)
      cstb_ii        <= '0';
      cstb           <= '0';
      alps_error_ii  <= '0';
      alps_error_iii <= '0';
      paps_error_ii  <= '0';
      paps_error_iii <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      cstb_ii        <= cstb_i;
      cstb           <= cstb_ii;
      alps_error_ii  <= alps_error_i;
      alps_error_iii <= alps_error_ii;
      paps_error_ii  <= paps_error_i;
      paps_error_iii <= paps_error_ii;
    end if;
  end process ms_cstb;
end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package altro_sw_mask_in_pack is
  component altro_sw_mask_in
    port (
      clk         : in  std_logic;      -- Clock
      rstb        : in  std_logic;      -- Async reset
      altro_sw    : in  std_logic;      -- ALTRO switch
      alps_errorb : in  std_logic;      -- ALTRO power supply error from HW (-)
      l1b         : in  std_logic;      -- Level 1 (-)
      l2b         : in  std_logic;      -- Level 2 (-)
      cstbb       : in  std_logic;      -- Control strobe (-)
      writeb      : in  std_logic;      -- Write (-)
      al_dolo_enb : in  std_logic;      -- ALTRO GTL enable (-)
      al_trsf_enb : in  std_logic;      -- ALTRO Transfer enable (-)
      trsfb       : in  std_logic;      -- Transfer (-)
      dstbb       : in  std_logic;      -- Data strobe (-)
      or_rstb     : in  std_logic;      -- Sync or Async reset (-)
      acknb       : in  std_logic;      -- Acknowledge (-)
      hadd        : in  std_logic_vector(4 downto 0);  -- Card address
      pasa_sw     : in  std_logic;      -- PASA switch
      paps_errorb : in  std_logic;      -- PASA power sup. error (-)
      al_errorb   : in  std_logic;      -- ALTRO error (-)
      alps_error  : out std_logic;      -- ALTRO power sup. error (+)
      l1          : out std_logic;      -- Level 1 (+)
      l2          : out std_logic;      -- Level 2 (+)
      cstb        : out std_logic;      -- Control strobe (+)
      write       : out std_logic;      -- Write (+)
      al_dolo_en  : out std_logic;      -- ALTRO data transfer enable (+)
      al_trsf_en  : out std_logic;      -- ALTRO transfer enable (+)
      trsf        : out std_logic;      -- Transfer (+)
      dstb        : out std_logic;      -- Data strobe (+)
      ackn        : out std_logic;      -- Acknowledge (+)
      ackn_chrdo  : out std_logic;      -- Acknowledge during readout (+)
      rst_fbc     : out std_logic;      -- Front-end reset (+)
      bcout_add   : out std_logic_vector(4 downto 0);
                                        -- Output hardware address
      paps_error  : out std_logic;      -- PASA power supl. error (+)
      al_error    : out std_logic);     -- ALTRO error (+)
  end component altro_sw_mask_in;
end package altro_sw_mask_in_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
