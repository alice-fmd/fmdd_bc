
# NC-Sim Command File
# TOOL:	ncsim	06.10-s003
#
#
# You can restore this configuration with:
#
#     ncsim bc_i2c -cdslib tools/cds.lib -hdlvar tools/hdl.var -logfile tools/nc.log -append_log -nocopyright -gui -input ./bc/bc_i2c.tcl -input /home/hehi/cholm/alice/fee/fmdd_bc/bc/bc_i2c.tcl
#

set tcl_prompt1 {puts -nonewline "ncsim> "}
set tcl_prompt2 {puts -nonewline "> "}
set vlog_format %h
set vhdl_format %v
set real_precision 6
set display_unit auto
set time_unit module
set heap_garbage_size -200
set heap_garbage_time 0
set assert_report_level note
set assert_stop_level error
set autoscope yes
set assert_1164_warnings yes
set pack_assert_off {}
set severity_pack_assert_off {note warning}
set assert_output_stop_level failed
set tcl_debug_level 0
set relax_path_name 0
set vhdl_vcdmap XX01ZX01X
set intovf_severity_level ERROR
set probe_screen_format 0
set rangecnst_severity_level ERROR
set textio_severity_level ERROR
set vital_timing_checks_on 1
set vlog_code_show_force 0
set assert_count_attempts 0
alias . run
alias quit exit
stop -create  -time 60 US -relative -modulo 60 US
stop -create  -time 10 US -relative -modulo 20 US
database -open -shm -into waves.shm waves -default
probe -create -database waves : -all -variables -depth all -waveform
probe -create -database waves :dut:core:i2c_slave:decoder:for_us:cin_used :dut:core:i2c_slave:decoder:for_us:clock_enable_mode :dut:core:i2c_slave:decoder:for_us:lut_mask :dut:core:i2c_slave:decoder:for_us:operation_mode :dut:core:i2c_slave:decoder:for_us:output_mode :dut:core:i2c_slave:decoder:for_us:packed_mode :dut:core:i2c_slave:decoder:for_us:x_on_violation :dut:core:i2c_slave:decoder:slctr:cin_used :dut:core:i2c_slave:decoder:slctr:clock_enable_mode :dut:core:i2c_slave:decoder:slctr:lut_mask :dut:core:i2c_slave:decoder:slctr:operation_mode :dut:core:i2c_slave:decoder:slctr:output_mode :dut:core:i2c_slave:decoder:slctr:packed_mode :dut:core:i2c_slave:decoder:slctr:x_on_violation :dut:core:i2c_slave:decoder:clear:cin_used :dut:core:i2c_slave:decoder:clear:clock_enable_mode :dut:core:i2c_slave:decoder:clear:lut_mask :dut:core:i2c_slave:decoder:clear:operation_mode :dut:core:i2c_slave:decoder:clear:output_mode :dut:core:i2c_slave:decoder:clear:packed_mode :dut:core:i2c_slave:decoder:clear:x_on_violation :dut:core:i2c_slave:decoder:enable:cin_used :dut:core:i2c_slave:decoder:enable:clock_enable_mode :dut:core:i2c_slave:decoder:enable:lut_mask :dut:core:i2c_slave:decoder:enable:operation_mode :dut:core:i2c_slave:decoder:enable:output_mode :dut:core:i2c_slave:decoder:enable:packed_mode :dut:core:i2c_slave:decoder:enable:x_on_violation
probe -create -database waves :dut:core:i2c_slave:decoder:slctr:cin_used :dut:core:i2c_slave:decoder:slctr:x_on_violation :dut:core:i2c_slave:decoder:clear:cin_used :dut:core:i2c_slave:decoder:clear:x_on_violation :dut:core:i2c_slave:decoder:enable:cin_used :dut:core:i2c_slave:decoder:enable:x_on_violation

simvision -input /home/hehi/cholm/alice/fee/fmdd_bc/bc/bc_i2c.tcl.svcf
