------------------------------------------------------------------------------
-- Title      : Configuration of registers
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : register_config.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : NBI
-- Last update: 2010-06-15
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Package for register handling, shared among many components
--              of the design. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @defgroup regs Registers
--! @ingroup bc
--! @section regs Registers and commands
--!
--! The register table is as follows:
--!
--! <table>
--!  <tr>
--!    <th>Name</th>
--!    <th>Addr</th>
--!    <th>BCast</th>
--!    <th>I2C</th>
--!    <th>Access</th>
--!    <th>Default</th>
--!    <th>Width</th>
--!    <th>Description</th></tr>
--!  <tr><td colspan="8"><i>Monitors</i></td></tr>
--!  <tr>
--!    <td>t1_th</td>
--!    <td>0x01</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x00A0</td>
--!    <td>10</td>
--!    <td>First ADC Temperature threshold</td></tr>
--!  <tr>
--!    <td>flash_i_th</td>
--!    <td>0x02</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>Flash current threshold</td></tr>
--!  <tr>
--!    <td>al_dig_i_th</td>
--!    <td>0x03</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>ALTRO digital current threshold</td></tr>
--!  <tr>
--!    <td>al_ana_i_th</td>
--!    <td>0x04</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>ALTRO analog current threshold</td></tr>
--!  <tr>
--!    <td>va_rec_ip_th</td>
--!    <td>0x05</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>VA positive rec. cur. threshold</td></tr>
--!  <tr>
--!    <td>t1</td>
--!    <td>0x06</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x00A0</td>
--!    <td>10</td>
--!    <td>First ADC Temperature</td></tr>
--!  <tr>
--!    <td>flash_i</td>
--!    <td>0x07</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03ff</td>
--!    <td>10</td>
--!    <td>Flash current</td></tr>
--!  <tr>
--!    <td>al_dig_i</td>
--!    <td>0x08</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03ff</td>
--!    <td>10</td>
--!    <td>ALTRO digitial current</td></tr>
--!  <tr>
--!    <td>al_ana_i</td>
--!    <td>0x09</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03ff</td>
--!    <td>10</td>
--!    <td>ALTRO analog current</td></tr>
--!  <tr>
--!    <td>va_rec_ip</td>
--!    <td>0x0A</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03ff</td>
--!    <td>10</td>
--!    <td>VA positive rec. current</td></tr>
--!  <tr>
--!    <td>t2_th</td>
--!    <td>0x2D</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x00A0</td>
--!    <td>10</td>
--!    <td>Second ADC Temperature threhold</td></tr>
--!  <tr>
--!    <td>va_sup_ip_th</td>
--!    <td>0x2E</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>VA positive sup. cur. threshold</td></tr>
--!  <tr>
--!    <td>va_rec_im_th</td>
--!    <td>0x2F</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>VA negative rec. cur. threshold</td></tr>
--!  <tr>
--!    <td>va_sup_im_th</td>
--!    <td>0x30</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>VA negative sup. cur. threshold</td></tr>
--!  <tr>
--!    <td>gtl_u_th</td>
--!    <td>0x31</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>Digital driver voltage threshold</td></tr>
--!  <tr>
--!    <td>t2</td>
--!    <td>0x32</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x00A0</td>
--!    <td>10</td>
--!    <td>Second ADC Temperature</td></tr>
--!  <tr>
--!    <td>va_sup_ip</td>
--!    <td>0x33</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03ff</td>
--!    <td>10</td>
--!    <td>VA positive sup. cur.</td></tr>
--!  <tr>
--!    <td>va_rec_im</td>
--!    <td>0x34</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03ff</td>
--!    <td>10</td>
--!    <td>VA negative rec. cur.</td></tr>
--!  <tr>
--!    <td>va_sup_im</td>
--!    <td>0x35</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03ff</td>
--!    <td>10</td>
--!    <td>VA negative sup. cur.</td></tr>
--!  <tr>
--!    <td>gtl_u</td>
--!    <td>0x36</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03ff</td>
--!    <td>10</td>
--!    <td>Digital driver voltage</td></tr>
--!  <tr>
--!    <td>t3_th</td>
--!    <td>0x37</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x00A0</td>
--!    <td>10</td>
--!    <td>Third ADC Temperature threshold</td></tr>
--!  <tr>
--!    <td>t1sens_th</td>
--!    <td>0x38</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x01FF</td>
--!    <td>10</td>
--!    <td>Temperature sens. 1 threshold</td></tr>
--!  <tr>
--!    <td>t2sens_th</td>
--!    <td>0x39</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x01FF</td>
--!    <td>10</td>
--!    <td>Temperature sens. 2 threshold</td></tr>
--!  <tr>
--!    <td>al_dig_u_th</td>
--!    <td>0x3A</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>ALTRO digital voltage threshold</td></tr>
--!  <tr>
--!    <td>al_ana_u_th</td>
--!    <td>0x3B</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>ALTRO analog voltage threshold</td></tr>
--!  <tr>
--!    <td>t3</td>
--!    <td>0x3C</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x00A0</td>
--!    <td>10</td>
--!    <td>Third ADC Temperature</td></tr>
--!  <tr>
--!    <td>t1sens</td>
--!    <td>0x3D</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x01FF</td>
--!    <td>10</td>
--!    <td>Temperature sens. 1</td></tr>
--!  <tr>
--!    <td>t2sens</td>
--!    <td>0x3E</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x01FF</td>
--!    <td>10</td>
--!    <td>Temperature sens. 2</td></tr>
--!  <tr>
--!    <td>al_dig_u</td>
--!    <td>0x3F</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>ALTRO digital voltage</td></tr>
--!  <tr>
--!    <td>al_ana_u</td>
--!    <td>0x40</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>ALTRO analog voltage</td></tr>
--!  <tr>
--!    <td>t4_th</td>
--!    <td>0x41</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x00A0</td>
--!    <td>10</td>
--!    <td>Forth ADC Temperature threshold</td></tr>
--!  <tr>
--!    <td>va_rec_up_th</td>
--!    <td>0x42</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>VA positive rec. voltage threshold</td></tr>
--!  <tr>
--!    <td>va_sup_up_th</td>
--!    <td>0x43</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>VA positive sup. voltage threshold</td></tr>
--!  <tr>
--!    <td>va_sup_um_th</td>
--!    <td>0x44</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>VA negative sup. voltage threshold</td></tr>
--!  <tr>
--!    <td>va_rec_um_th</td>
--!    <td>0x45</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>VA negative rec. voltage threshold</td></tr>
--!  <tr>
--!    <td>t4</td>
--!    <td>0x46</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x00A0</td>
--!    <td>10</td>
--!    <td>Forth ADC Temperature</td></tr>  
--!  <tr>
--!    <td>va_rec_up</td>
--!    <td>0x47</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>VA positive rec. voltage</td></tr>
--!  <tr>
--!    <td>va_sup_up</td>
--!    <td>0x48</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>10</td>
--!    <td>VA positive sup. voltage</td></tr>
--!  <tr>
--!    <td>va_sup_um</td>
--!    <td>0x49</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>VA negative sup. voltage</td></tr>
--!  <tr>
--!    <td>va_rec_um</td>
--!    <td>0x4A</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x03FF</td>
--!    <td>10</td>
--!    <td>VA negative rec. voltage</td></tr>
--!  <tr><td colspan="8"><i>Counters</i></td></tr>                                          
--!  <tr>
--!    <td>l1cnt</td>
--!    <td>0x0B</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>16</td>
--!    <td>L1 trigger CouNTer</td></tr>
--!  <tr>
--!    <td>l2cnt</td>
--!    <td>0x0C</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>16</td>
--!    <td>L2 trigger CouNTer</td></tr>
--!  <tr>
--!    <td>sclkcnt</td>
--!    <td>0x0D</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>16</td>
--!    <td>Sampling CLK CouNTer</td></tr>
--!  <tr>
--!    <td>dstbcnt</td>
--!    <td>0x0E</td>
--!    <td>&nbsp;</td>
--!    <td>X</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>16</td>
--!    <td>DSTB CouNTer</td></tr>
--!  <tr><td colspan="8"><i>Test mode</i></td></tr>                                         
--!  <tr>
--!    <td>tsm_word</td>
--!    <td>0x0F</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0001</td>
--!    <td> 9</td>
--!    <td>Test mode word</td></tr>
--!  <tr>
--!    <td>us_ratio</td>
--!    <td>0x10</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0001</td>
--!    <td>16</td>
--!    <td>Undersampling ratio.</td></tr>
--!  <tr><td colspan="8"><i>Configuration and status</i></td></tr>
--!  <tr>
--!    <td>csr0</td>
--!    <td>0x11</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x07FF</td>
--!    <td>11</td>
--!    <td>Config/Status Reg. 0</td></tr>
--!  <tr>
--!    <td>csr1</td>
--!    <td>0x12</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td>14</td>
--!    <td>Config/Status Reg. 1</td></tr>
--!  <tr>
--!    <td>csr2</td>
--!    <td>0x13</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x000F</td>
--!    <td>16</td>
--!    <td>Config/Status Reg. 2</td></tr>
--!  <tr>
--!    <td>csr3</td>
--!    <td>0x14</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x2220</td>
--!    <td>16</td>
--!    <td>Config/Status Reg. 3</td></tr>
--!  <tr>
--!    <td>free</td>
--!    <td>0x15</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Free</td></tr>
--!  <tr><td colspan="8"><i>Comands:</i></td>                                               
--!  <tr>
--!    <td>cntlat</td>
--!    <td>0x16</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Latch counters</td></tr>
--!  <tr>
--!    <td>cntclr</td>
--!    <td>0x17</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Clear counters</td></tr>
--!  <tr>
--!    <td>csr1clr</td>
--!    <td>0x18</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Clear CSR1</td></tr>
--!  <tr>
--!    <td>alrst</td>
--!    <td>0x19</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>rstb ALTROs</td></tr>
--!  <tr>
--!    <td>bcrst</td>
--!    <td>0x1A</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>rstb BC</td></tr>
--!  <tr>
--!    <td>stcnv</td>
--!    <td>0x1B</td>
--!    <td>X</td>
--!    <td>X</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Start conversion</td></tr>
--!  <tr>
--!    <td>scevl</td>
--!    <td>0x1C</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Scan event length</td></tr>
--!  <tr>
--!    <td>evlrdo</td>
--!    <td>0x1D</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Read event length</td></tr>
--!  <tr>
--!    <td>sttsm</td>
--!    <td>0x1E</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Start test mode</td></tr>
--!  <tr>
--!    <td>acqrdo</td>
--!    <td>0x1F</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td> 0</td>
--!    <td>Read acq. memory</td></tr>
--!  <tr><td colspan="8"><i>FMD</i></td></tr>
--!  <tr>
--!    <td>fmdd_stat</td>
--!    <td>0x20</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>16</td>
--!    <td>FMDD status</td></tr>
--!  <tr>
--!    <td>l0cnt</td>
--!    <td>0x21</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>r</td>
--!    <td>0x0000</td>
--!    <td>16</td>
--!    <td>L0 counters</td></tr>
--!  <tr>
--!    <td>hold_wait</td>
--!    <td>0x22</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0020</td>
--!    <td>16</td>
--!    <td>Wait to hold</td></tr>
--!  <tr>
--!    <td>l1_timeout</td>
--!    <td>0x23</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x00F0</td>
--!    <td>16</td>
--!    <td>L1 timeout</td></tr>
--!  <tr>
--!    <td>l2_timeout</td>
--!    <td>0x24</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0E00</td>
--!    <td>16</td>
--!    <td>L2 timeout</td></tr>
--!  <tr>
--!    <td>shift_div</td>
--!    <td>0x25</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x1000</td>
--!    <td>16</td>
--!    <td>Shift clk</td></tr>
--!  <tr>
--!    <td>strips</td>
--!    <td>0x26</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x7F00</td>
--!    <td>16</td>
--!    <td>Strips</td></tr>
--!  <tr>
--!    <td>cal_level</td>
--!    <td>0x27</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x2000</td>
--!    <td>16</td>
--!    <td>Cal pulse</td></tr>
--!  <tr>
--!    <td>shape_bias</td>
--!    <td>0x28</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x2A20</td>
--!    <td>16</td>
--!    <td>Shape bias</td></tr>
--!  <tr>
--!    <td>vfs</td>
--!    <td>0x29</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x645D</td>
--!    <td>16</td>
--!    <td>Shape ref</td></tr>
--!  <tr>
--!    <td>vfp</td>
--!    <td>0x2A</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x878B</td>
--!    <td>16</td>
--!    <td>Preamp ref</td></tr>
--!  <tr>
--!    <td>sample_div</td>
--!    <td>0x2B</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0402</td>
--!    <td>16</td>
--!    <td>Sample clk</td></tr>
--!  <tr>
--!    <td>fmdd_cmd</td>
--!    <td>0x2C</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0000</td>
--!    <td>16</td>
--!    <td>Commands</td></tr>
--!  <tr>
--!    <td>cal_iter</td>
--!    <td>0x4B</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0064</td>
--!    <td>16</td>
--!    <td>Cal events</td></tr>
--!  <tr>
--!    <td>mebs</td>
--!    <td>0x4C</td>
--!    <td>X</td>
--!    <td>&nbsp;</td>
--!    <td>rw</td>
--!    <td>0x0144</td>
--!    <td> 9</td>
--!    <td>Multi-event config</td></tr>
--! </table>
--!
--! @subsection regs_mon Monitor registers
--!
--! The monitor registers fall in two groups: the threshold and the current
--! ADC value.   The current values are read-only, while the thresholds can be
--! read and written.  The conversion factors and offsets, as well as the units
--! are given below. Note, that only current value is described.  The
--! parameters are the same for the thresholds.
--!
--! <table>
--!  <tr><th>Name</th><th>Offset</th><th>Factor</th><th>Unit</th><th>Notes</th></tr>
--!  <tr><td>t1           </td><td>0</td><td>0.25</td><td>C</td><td>&nbsp;</td></tr>
--!  <tr><td>flash_i      </td><td>0</td><td>1</td><td>mA</td><td>3.3 V</td></tr>
--!  <tr><td>al_dig_i     </td><td>0</td><td>1</td><td>mA</td><td>2.5 V</td></tr>
--!  <tr><td>al_ana_i     </td><td>0</td><td>1</td><td>mA</td><td>2.5 V</td></tr>
--!  <tr><td>va_rec_ip    </td><td>0</td><td>1</td><td>mA</td><td>2.5 V</td></tr>
--!  <tr><td>t2           </td><td>0</td><td>0.25</td><td>C</td><td>&nbsp;</td></tr>
--!  <tr><td>va_sup_ip    </td><td>0</td><td>1</td><td>mA</td><td>1.5 V</td></tr>
--!  <tr><td>va_rec_im    </td><td>0</td><td>1</td><td>mA</td><td>-2.0 V</td></tr>
--!  <tr><td>va_sup_im    </td><td>0</td><td>1</td><td>mA</td><td>-2.0 V</td></tr>
--!  <tr><td>gtl_u        </td><td>0</td><td>3.3/512</td><td>V</td><td>2.5 V</td></tr>
--!  <tr><td>t3           </td><td>0</td><td>0.25</td><td>C</td><td>&nbsp;</td></tr>
--!  <tr><td>t1sens       </td><td>0</td><td>0.10</td><td>C</td><td>&nbsp;</td></tr>
--!  <tr><td>t2sens       </td><td>0</td><td>0.10</td><td>C</td><td>&nbsp;</td></tr>
--!  <tr><td>al_dig_u     </td><td>0</td><td>2.5/512</td><td>V</td><td>2.5 V</td></tr>
--!  <tr><td>al_ana_u     </td><td>0</td><td>2.5/512</td><td>V</td><td>2.5 V</td></tr>
--!  <tr><td>t4           </td><td>0</td><td>0.25</td><td>C</td><td>&nbsp;</td></tr>
--!  <tr><td>va_rec_up    </td><td>0</td><td>2.5/512</td><td>V</td><td>2.5 V</td></tr>
--!  <tr><td>va_sup_up    </td><td>0</td><td>1.5/512</td><td>V</td><td>1.5 V</td></tr>
--!  <tr><td>va_sup_um    </td><td>0</td><td>4.5/512</td><td>V</td><td>-2.0 V</td></tr>
--!  <tr><td>va_rec_um    </td><td>0</td><td>4.5/512</td><td>V</td><td>-2.0 V</td></tr>
--! </table>
--!
--! The current values are continously updated if continous conversion is
--! enabled in CSR0.   Otherwise, a single conversion can be initiated via the
--! command STCNV.
--!
--! The monitor values are mapped as follows to the 5 interrupt lines
--!
--! <table>
--!   <tr><td>al_dig_i</td><td rowspan=3>digital over-current</td></tr>
--!   <tr><td>flash_i</td></tr>
--!   <tr><td>va_rec_ip</td></tr>
--!   <tr><td>al_dig_u</td><td rowspan=3>digital under-voltage</td></tr>
--!   <tr><td>gtl_u</td></tr>
--!   <tr><td>va_rec_up</td></tr>
--!   <tr><td>al_ana_i</td><td rowspan=4>analog over-current</td></tr>
--!   <tr><td>va_sup_ip</td></tr>
--!   <tr><td>va_rec_im</td></tr>
--!   <tr><td>va_sup_im</td></tr>
--!   <tr><td>al_ana_u</td><td rowspan=4>analog under-voltage</td></tr>
--!   <tr><td>va_sup_up</td></tr>
--!   <tr><td>va_rec_um</td></tr>
--!   <tr><td>va_sup_um</td></tr>
--!   <tr><td>t1</td><td rowspan=6>over-temperature</td></tr>
--!   <tr><td>t2</td></tr>
--!   <tr><td>t3</td></tr>
--!   <tr><td>t4</td></tr>
--!   <tr><td>t1sens</td></tr>
--!   <tr><td>t2sens</td></tr>
--! </table>
--!
--! @subsection regs_cnt Counters
--!
--! The BC contains a number of counters for diagnostics.  It will count the
--! number of triggers (L0, L1, and L2) as well as the number of last seen
--! data strobes (40bit words transmitted by ALTROs), and possible missed
--! sample clocks.
--!
--! The current values of the counters is stored in registers by issuing the
--! command CNTLAT.   By sending this command in broadcast, one can compare
--! number of triggers received over all front-end cards.
--!
--! The counters are clear by the command CNTCLR.
--!
--! @subsection regs_tsm Test mode
--!
--! Test mode is disabled in the FMD digitiser BC firmware, and these registers
--! have no meaning.  They are kept only for backward compatibility.
--!
--! @subsection regs_csr Configuration and status registers
--!
--! The main operational parameters of the BC are the 4 configuration and
--! status registers CSR0, CSR1, CSR2, and CSR3.
--!
--! @par Configuration and status register 0
--!
--! This register holds the interrupt and error mask of the BC. The register is
--! segmented as follows:
--!
--! <table>
--!  <tr><th>Bit</th><th>Description</th></tr>
--!  <tr><td>10</td><td>Enable continous conversion</td></tr>
--!  <tr><td> 9</td><td>Enable BC instruction errors</td></tr>
--!  <tr><td> 8</td><td>Enable parity errors</td></tr>
--!  <tr><td> 7</td><td>Enable missed sample clock interrupt</td></tr>
--!  <tr><td> 6</td><td>Enable ALTRO power supply interrupt</td></tr>
--!  <tr><td> 5</td><td>Enable PASA power supply interrupt</td></tr>
--!  <tr><td> 4</td><td>Enable digital over-current interrupt</td></tr>
--!  <tr><td> 3</td><td>Enable digital under-voltage interrupt</td></tr>
--!  <tr><td> 2</td><td>Enable analog over-current interrupt</td></tr>
--!  <tr><td> 1</td><td>Enable analog under-voltage interrupt</td></tr>
--!  <tr><td> 0</td><td>Enable over-temperature interrupt</td></tr>
--! </table>
--!
--! @par Configuration and status 1
--!
--! This register holds the current status of the BC.  The errors and
--! interrupts of this register can be masked out by setting the corresponding
--! bit of CSR0 to '0'. This register is read-only.
--!
--! <table>
--!  <tr><th>Bit</th><th>Description</th></tr>
--!  <tr><td>13</td><td>Interrupt (OR of 8 interrupt lines)</td></tr>
--!  <tr><td>12</td><td>Error (OR of 4 interrupt lines)</td></tr>
--!  <tr><td>11</td><td>I2C bus error</td></tr>
--!  <tr><td>10</td><td>ALTRO error</td></tr>
--!  <tr><td> 9</td><td>BC instruction errors</td></tr>
--!  <tr><td> 9</td><td>BC instruction errors</td></tr>
--!  <tr><td> 8</td><td>Parity errors</td></tr>
--!  <tr><td> 7</td><td>Missed sample clock interrupt</td></tr>
--!  <tr><td> 6</td><td>ALTRO power supply interrupt</td></tr>
--!  <tr><td> 5</td><td>PASA power supply interrupt</td></tr>
--!  <tr><td> 4</td><td>Digital over-current interrupt</td></tr>
--!  <tr><td> 3</td><td>Digital under-voltage interrupt</td></tr>
--!  <tr><td> 2</td><td>Analog over-current interrupt</td></tr>
--!  <tr><td> 1</td><td>Analog under-voltage interrupt</td></tr>
--!  <tr><td> 0</td><td>Over-temperature interrupt</td></tr>
--! </table>
--!
--! @par Configuration and status 2
--!
--! This holds the hardware address, test-mode parameters, and enables/.
--!
--! <table>
--!  <tr><th>Bit(s)</th><th>Description</th></tr>
--!  <tr><td>15-11</td><td>Hardware address</td></tr>
--!  <tr><td>   10</td><td>Card isolated</td></tr>
--!  <tr><td>    9</td><td>Continous test mode</td></tr>
--!  <tr><td> 8- 6</td><td>ALTRO address</td></tr>
--!  <tr><td> 5- 4</td><td>Channel address</td></tr>
--!  <tr><td>    3</td><td>Sample clock enable</td></tr>
--!  <tr><td>    2</td><td>Readout clock enable</td></tr>
--!  <tr><td>    1</td><td>PASA switch</td></tr>
--!  <tr><td>    0</td><td>ALTRO switch</td></tr>
--! </table>
--!
--! Test mode is disabled in the FMD BC firmware, so bits 10-4 are meaningless.
--! Bit 0 is connected to the enable of the power regulator of both the ALTRO
--! and BC, so this bit <emph>must</emph> always be asserted.
--!
--! @par Configuration and status register 3
--!
--! 



--! @ingroup regs
--! This package holds the configruation for the registers of the board
--! controller.
--!
package register_config is
  --! Version number X"0113" -> Major: 0x01, minor: 0x13=19
  constant version : std_logic_vector(15 downto 0) := X"0302";  --! Version number

  constant ADD_BITS : integer := 7;                --! Number of addrss bits
  constant NUM_REG  : integer := 2**ADD_BITS - 1;  --! Number of registers
  
  subtype add_t is integer range 0 to NUM_REG;        --! Address type
  subtype regval_t is std_logic_vector(15 downto 0);  --! Register value
  
  -- Addresses of registers
  constant add_zero         : add_t := 16#00#;  -- Nothing
  -- Addresses of ADCs and thresholds
  constant add_t1_th        : add_t := 16#01#;  --! First ADC T           
  constant add_flash_i_th   : add_t := 16#02#;  --! I  3.3 V              
  constant add_al_dig_i_th  : add_t := 16#03#;  --! I  2.5 V altro digital
  constant add_al_ana_i_th  : add_t := 16#04#;  --! I  2.5 V altro analog 
  constant add_va_rec_ip_th : add_t := 16#05#;  --! I  2.5 V VA           
  constant add_t1           : add_t := 16#06#;  --! First ADC T           
  constant add_flash_i      : add_t := 16#07#;  --! I  3.3 V              
  constant add_al_dig_i     : add_t := 16#08#;  --! I  2.5 V altro digital
  constant add_al_ana_i     : add_t := 16#09#;  --! I  2.5 V altro analog 
  constant add_va_rec_ip    : add_t := 16#0A#;  --! I  2.5 V VA           
  constant add_t2_th        : add_t := 16#2D#;  --! Second ADC T           
  constant add_va_sup_ip_th : add_t := 16#2E#;  --! I  1.5 V VA            
  constant add_va_rec_im_th : add_t := 16#2F#;  --! I -2.0 V               
  constant add_va_sup_im_th : add_t := 16#30#;  --! I -2.0 V VA            
  constant add_gtl_u_th     : add_t := 16#31#;  --!    2.5 V Digital driver
  constant add_t2           : add_t := 16#32#;  --! Second ADC T           
  constant add_va_sup_ip    : add_t := 16#33#;  --! I  1.5 V VA            
  constant add_va_rec_im    : add_t := 16#34#;  --! I -2.0 V               
  constant add_va_sup_im    : add_t := 16#35#;  --! I -2.0 V VA            
  constant add_gtl_u        : add_t := 16#36#;  --!    2.5 V Digital driver
  constant add_t3_th        : add_t := 16#37#;  --! Third ADC T             
  constant add_t1sens_th    : add_t := 16#38#;  --! Temperature sens. 1     
  constant add_t2sens_th    : add_t := 16#39#;  --! Temperature sens. 2     
  constant add_al_dig_u_th  : add_t := 16#3A#;  --! U  2.5 altro digital (m)
  constant add_al_ana_u_th  : add_t := 16#3B#;  --! U  2.5 altro analog (m) 
  constant add_t3           : add_t := 16#3C#;  --! Third ADC T             
  constant add_t1sens       : add_t := 16#3D#;  --! Temperature sens. 1     
  constant add_t2sens       : add_t := 16#3E#;  --! Temperature sens. 2     
  constant add_al_dig_u     : add_t := 16#3F#;  --! U  2.5 altro digital (m)
  constant add_al_ana_u     : add_t := 16#40#;  --! U  2.5 altro analog (m) 
  constant add_t4_th        : add_t := 16#41#;  --! Forth ADC T  
  constant add_va_rec_up_th : add_t := 16#42#;  --! U  2.5 VA (m)
  constant add_va_sup_up_th : add_t := 16#43#;  --! U  1.5 VA (m)
  constant add_va_sup_um_th : add_t := 16#44#;  --! U -2.0 VA (m)
  constant add_va_rec_um_th : add_t := 16#45#;  --! U -2.0 (m)   
  constant add_t4           : add_t := 16#46#;  --! Forth ADC T  
  constant add_va_rec_up    : add_t := 16#47#;  --! U  2.5 VA (m)
  constant add_va_sup_up    : add_t := 16#48#;  --! U  1.5 VA (m)
  constant add_va_sup_um    : add_t := 16#49#;  --! U -2.0 VA (m)
  constant add_va_rec_um    : add_t := 16#4A#;  --! U -2.0 (m)   
  -- Counters 
  constant add_l1cnt        : add_t := 16#0B#;  --! L1 trigger CouNTer
  constant add_l2cnt        : add_t := 16#0C#;  --! L2 trigger CouNTer
  constant add_sclkcnt      : add_t := 16#0D#;  --! Sampling CLK CouNTer
  constant add_dstbcnt      : add_t := 16#0E#;  --! DSTB CouNTer
  -- Test mode 
  constant add_tsm_word     : add_t := 16#0F#;  --! Test mode word
  constant add_us_ratio     : add_t := 16#10#;  --! Undersampling ratio.
  -- Configuration and status 
  constant add_csr0         : add_t := 16#11#;  --! Config/Status Register 0
  constant add_csr1         : add_t := 16#12#;  --! Config/Status Register 1
  constant add_csr2         : add_t := 16#13#;  --! Config/Status Register 2
  constant add_csr3         : add_t := 16#14#;  --! Config/Status Register 3
  constant add_free         : add_t := 16#15#;  --! Free
  -- Comands:
  constant add_cntlat       : add_t := 16#16#;  --! Latch L1, L2, SCLK Counters
  constant add_cntclr       : add_t := 16#17#;  --! Clear counters
  constant add_csr1clr      : add_t := 16#18#;  --! Clear CSR1
  constant add_alrst        : add_t := 16#19#;  --! rstb ALTROs
  constant add_bcrst        : add_t := 16#1A#;  --! rstb BC
  constant add_stcnv        : add_t := 16#1B#;  --! Start conversion
  constant add_scevl        : add_t := 16#1C#;  --! Scan event length
  constant add_evlrdo       : add_t := 16#1D#;  --! Read event length
  constant add_sttsm        : add_t := 16#1E#;  --! Start test mode
  constant add_acqrdo       : add_t := 16#1F#;  --! Read acquisition memory
  -- FMD
  constant add_fmdd_stat    : add_t := 16#20#;  --! FMDD status
  constant add_l0cnt        : add_t := 16#21#;  --! L0 counters
  constant add_hold_wait    : add_t := 16#22#;  --! FMD: Wait to hold
  constant add_l1_timeout   : add_t := 16#23#;  --! FMD: L1 timeout
  constant add_l2_timeout   : add_t := 16#24#;  --! FMD: L2 timeout
  constant add_shift_div    : add_t := 16#25#;  --! FMD: Shift clk
  constant add_strips       : add_t := 16#26#;  --! FMD: Strips
  constant add_cal_level    : add_t := 16#27#;  --! FMD: Cal pulse
  constant add_shape_bias   : add_t := 16#28#;  --! FMD: Shape bias
  constant add_vfs          : add_t := 16#29#;  --! FMD: Shape ref
  constant add_vfp          : add_t := 16#2A#;  --! FMD: Preamp ref
  constant add_sample_div   : add_t := 16#2B#;  --! FMD: Sample clk
  constant add_fmdd_cmd     : add_t := 16#2C#;  --! FMD: Commands
  constant add_cal_iter     : add_t := 16#4B#;  --! FMD: Cal events
  constant add_mebs         : add_t := 16#4C#;  --! Multi-event count/config
  constant add_cal_delay    : add_t := 16#4D#;  --! Multi-event count/config

  constant MAX_REG : integer := add_cal_delay;  -- Last valid index

  -- Altro read-pointer increment - send by RCU at end of read-out
  constant add_al_rpinc : std_logic_vector(4 downto 0) := "11001";  -- 0x19
  

  type reg_t is
    record                              --! Record describing registers
      bcast : boolean;                  --! Allow broadcast
      sc    : boolean;                  --! Allow slow control
      w     : boolean;                  --! Allow write
      def   : regval_t;                 --! Default value
      -- mask  : regval_t;                 --! Bit mask
    end record reg_t;
  type regs_t is array (MAX_REG downto 0) of reg_t;

  -- List of registers with flags.
  --  - whether it can be broadcasted to or not
  --  - whether the I2C bus can access this register
  --  - whether the register can be written to
  --  - The default (boot-up) value
  --
  -- Default values for thresholds are from Kris based on what we've seen
  -- in the pit.  
  constant regs : regs_t := (
    add_zero         => (true, true, true, X"0000"),      -- 
    -- Values for ADC monitor registers 
    add_t1_th        => (true, true, true, X"00AF"),      -- ~ 40�C
    add_flash_i_th   => (true, true, true, X"0027"),      -- 
    add_al_dig_i_th  => (true, true, true, X"004e"),      -- 
    add_al_ana_i_th  => (true, true, true, X"0045"),      -- 
    add_va_rec_ip_th => (true, true, true, X"0075"),      -- 
    add_t1           => (false, true, false, X"00AA"),    -- 0x06
    add_flash_i      => (false, true, false, X"0027"),    -- 0x07
    add_al_dig_i     => (false, true, false, X"004e"),    -- 0x08
    add_al_ana_i     => (false, true, false, X"0045"),    -- 0x09
    add_va_rec_ip    => (false, true, false, X"0075"),    -- 0X0A
    add_t2_th        => (true, true, true, X"00AF"),      -- 0x2D
    add_va_sup_ip_th => (true, true, true, X"0089"),      -- 0X2E
    add_va_rec_im_th => (true, true, true, X"00a6"),      -- 0x2F
    add_va_sup_im_th => (true, true, true, X"0186"),      -- 0X30
    add_gtl_u_th     => (true, true, true, X"01d6"),      -- 0x31
    add_t2           => (false, true, false, X"00AA"),    -- 0x32
    add_va_sup_ip    => (false, true, false, X"0089"),    -- 0X33
    add_va_rec_im    => (false, true, false, X"00a6"),    -- 0x34
    add_va_sup_im    => (false, true, false, X"0186"),    -- 0X35
    add_gtl_u        => (false, true, false, X"01d6"),    -- 0x36
    add_t3_th        => (true, true, true, X"00AF"),      -- 0x37
    add_t1sens_th    => (true, true, true, X"01cf"),      -- ~50�C
    add_t2sens_th    => (true, true, true, X"01cf"),      -- ~50�C
    add_al_dig_u_th  => (true, true, true, X"01d6"),      -- 0X3A
    add_al_ana_u_th  => (true, true, true, X"01d6"),      -- 0x3B
    add_t3           => (false, true, false, X"00Aa"),    -- 0x3C
    add_t1sens       => (false, true, false, X"01bf"),    -- 0x3D
    add_t2sens       => (false, true, false, X"01bf"),    -- 0x3E
    add_al_dig_u     => (false, true, false, X"01d6"),    -- 0x3F
    add_al_ana_u     => (false, true, false, X"01d6"),    -- 0x40
    add_t4_th        => (true, true, true, X"00AF"),      -- 0x41
    add_va_rec_up_th => (true, true, true, X"01d6"),      -- 0x42
    add_va_sup_up_th => (true, true, true, X"0109"),      -- 0x43
    add_va_sup_um_th => (true, true, true, X"008F"),      -- 0x44
    add_va_rec_um_th => (true, true, true, X"008F"),      -- 0x45
    add_t4           => (false, true, false, X"00AA"),    -- 0x46
    add_va_rec_up    => (false, true, false, X"01d6"),    -- 0x47
    add_va_sup_up    => (false, true, false, X"0109"),    -- 0x48
    add_va_sup_um    => (false, true, false, X"008F"),    -- 0x49
    add_va_rec_um    => (false, true, false, X"008F"),    -- 0x4A
    -- Counters
    add_l1cnt        => (false, true, false, X"0000"),    -- 
    add_l2cnt        => (false, true, false, X"0000"),    -- 
    add_sclkcnt      => (false, false, false, X"0000"),   -- 
    add_dstbcnt      => (false, false, false, X"0000"),   -- 
    -- Test mode 
    add_tsm_word     => (true, false, true, X"0001"),     -- 
    add_us_ratio     => (true, false, true, X"0001"),     -- 
    -- Configuration and status 
    add_csr0         => (true, true, true, X"07F9"),      -- not AV(1), AC(2)
    add_csr1         => (true, true, true, X"0000"),      -- 
    add_csr2         => (true, true, true, X"000F"),      -- not ALTRO_SW
    add_csr3         => (true, true, true, X"2220"),      -- 
    add_free         => (false, true, false, version),    -- 
    -- Comands:
    add_cntlat       => (true, true, true, X"0000"),      -- 
    add_cntclr       => (true, true, true, X"0000"),      -- 
    add_csr1clr      => (true, true, true, X"0000"),      -- 
    add_alrst        => (true, true, true, X"0000"),      -- 
    add_bcrst        => (true, true, true, X"0000"),      -- 
    add_stcnv        => (true, true, true, X"0000"),      -- 
    add_scevl        => (true, false, true, X"0000"),     -- 
    add_evlrdo       => (true, false, true, X"0000"),     -- 
    add_sttsm        => (true, false, true, X"0000"),     -- 
    add_acqrdo       => (true, false, true, X"0000"),     -- 
    -- FMDD
    -- hold:       0x020  * 25ns =   32 * 25ns =  0.8us
    -- L1 timeout: 0x0f0  * 25ns =  240 * 25ns =  6.0us
    -- L2 timeout: 0xe00  * 25ns = 3584 * 25ns = 89.6us
    -- Shift clk:  0x1000 -> 16 * 25ns = 400ns ->  2.5MHz, no phase
    -- Sample clk: 0x0402 ->  4 * 25ns = 100ns -> 10.0MHz, 50ns phase
    add_fmdd_stat    => (true, false, false, X"0000"),    -- 0x20
    add_l0cnt        => (true, true, false, X"0000"),     -- 0x21
    add_hold_wait    => (true, false, true, X"0020"),     -- 0x22
    add_l1_timeout   => (true, false, true, X"00F0"),     -- 0x23
    add_l2_timeout   => (true, false, true, X"0E00"),     -- 0x24
    add_shift_div    => (true, false, true, X"1000"),     -- 0x25
    add_strips       => (true, false, true, X"7F00"),     -- 0x26
    add_cal_level    => (true, false, true, X"2000"),     -- 0x27
    add_shape_bias   => (true, false, true, X"2A20"),     -- 0x28
    add_vfs          => (true, false, true, X"645D"),     -- 0x29
    add_vfp          => (true, false, true, X"878B"),     -- 0x2A
    add_sample_div   => (true, false, true, X"0402"),     -- 0x2B
    add_fmdd_cmd     => (true, false, true, X"0000"),     -- 0x2C
    add_cal_iter     => (true, false, true, X"0064"),     -- 0x4B
    add_mebs         => (true, false, true, X"0144"),     -- 0x4C
    add_cal_delay    => (true, false, true, X"0030"),     -- 0x4D
    others           => (false, false, false, X"0000"));  -- 


  -- ADC registers 
  constant BASE_ADC : integer   := 6;   --! Base address for ADCs
  constant NUM_ADC  : integer   := 20;  --! number of ADC's
  type adc_map_t is array (BASE_ADC to NUM_ADC+BASE_ADC-1)
    of integer range 0 to 2**7-1;       --! Map type
  type adc_rmap_t is array (0 to add_va_rec_um)
    of integer range 0 to NUM_ADC+BASE_ADC-1;
  
  constant adc_map : adc_map_t := (add_t1,         -- T  First ADC T           
                                   add_flash_i,    -- DC I  3.3 V              
                                   add_al_dig_i,   -- DC I  2.5 V dig
                                   add_al_ana_i,   -- AC I  2.5 V ana 
                                   add_va_rec_ip,  -- DC I  2.5 V VA           
                                   add_t2,         -- T  Second ADC T
                                   add_va_sup_ip,  -- AC I  1.5 V VA
                                   add_va_rec_im,  -- AC I -2.0 V       
                                   add_va_sup_im,  -- AC I -2.0 V VA
                                   add_gtl_u,      -- DV    2.5 V GTL
                                   add_t3,         -- T  Third ADC T
                                   add_t1sens,     -- T  Temp sens. 1     
                                   add_t2sens,     -- T  Temp sens. 2     
                                   add_al_dig_u,   -- DV U  2.5 altro dig
                                   add_al_ana_u,   -- AV U  2.5 altro ana
                                   add_t4,         -- T  Forth ADC T  
                                   add_va_rec_up,  -- DV U  2.5 VA
                                   add_va_sup_up,  -- AV U  1.5 VA
                                   add_va_sup_um,  -- AV U -2.0 VA
                                   add_va_rec_um);  -- AV U -2.0
  constant adc_rmap : adc_rmap_t := (add_t1        => BASE_ADC,
                                     add_flash_i   => BASE_ADC+1,
                                     add_al_dig_i  => BASE_ADC+2,
                                     add_al_ana_i  => BASE_ADC+3,
                                     add_va_rec_ip => BASE_ADC+4,
                                     add_t2        => BASE_ADC+5,
                                     add_va_sup_ip => BASE_ADC+6,
                                     add_va_rec_im => BASE_ADC+7,
                                     add_va_sup_im => BASE_ADC+8,
                                     add_gtl_u     => BASE_ADC+9,
                                     add_t3        => BASE_ADC+10,
                                     add_t1sens    => BASE_ADC+11,
                                     add_t2sens    => BASE_ADC+12,
                                     add_al_dig_u  => BASE_ADC+13,
                                     add_al_ana_u  => BASE_ADC+14,
                                     add_t4        => BASE_ADC+15,
                                     add_va_rec_up => BASE_ADC+16,
                                     add_va_sup_up => BASE_ADC+17,
                                     add_va_sup_um => BASE_ADC+18,
                                     add_va_rec_um => BASE_ADC+19,
                                     others        => 0);
end register_config;
------------------------------------------------------------------------------
--
-- EOF
--
