------------------------------------------------------------------------------
-- Title      : Top-level of board controller code
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : bc.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-06-15
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Top-level of board controller code
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/24  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.bc_core_pack.all;
use work.drivers_pack.all;
use work.altro_sw_mask_in_pack.all;
use work.fmdd_pack.all;
use work.meb_watchdog_pack.all;

--! @mainpage The FMD Digitizer Board Controller Firmware
--!
--! This code takes care of
--!
--!  - monitoring voltages, currents, and temperatures.
--!  - handle L0 triggers, and sends the appropriate control
--!    signals to the VA1 pre-amplifier chips on the FMD hybrid
--!    cards.
--!  - controls the GTL bus transceivers for the ALTRO's on the
--!    card, and of course for it self.
--!
--! The code is based on the TPC FEC BC code. The original authors were
--!
--!  - Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--!  - Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--!
--! Some initial development, and a lot of guidance was given by
--!
--!  - Daniel Kirschner <daniel.kirschner@nbi.dk>
--!  - Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>


--! @defgroup bc Board controller code

--! Top-level entity of the board controller
--! @ingroup bc
entity bc is
  port (
    clk           : in    std_logic;    -- Clock
    rstb          : in    std_logic;    -- Async reset
    sclk          : in    std_logic;    -- Sample clock (10 MHz)
    -- Serial interface 
    scl           : in    std_logic;    -- I2C: Serial clock (<= 5MHz)
    sda_in        : in    std_logic;    -- I2C: data input
    sda_out       : out   std_logic;    -- I2C: data out
    -- Triggers 
    l0            : in    std_logic;    -- Level 0 trigger 
    l1b           : in    std_logic;    -- Level 1 trigger
    l2b           : in    std_logic;    -- Level 2 trigger
    -- Monitor 
    mscl          : out   std_logic;    -- ADC: clock
    msda          : inout std_logic;    -- ADC: data in/out
    -- Pasa 
    pasa_sw       : out   std_logic;    -- PASA switch
    paps_errorb   : in    std_logic;    -- PASA power supply error
    -- Test mode 
    rdoclk_en     : out   std_logic;    -- TSM: Readout clock enable
    adcclk_en     : out   std_logic;    -- TSM: ADC clock enable
    adc_add0      : out   std_logic;    -- ADC address bit 0
    adc_add1      : out   std_logic;    -- ADC address bit 1
    test_a        : out   std_logic;    -- TSM: mask a
    test_b        : out   std_logic;    -- TSM: mask b
    test_c        : out   std_logic;    -- TSM: mask c
    -- Hardware  stuff 
    rst_fbc       : out   std_logic;    -- Reset front-end card
    hadd          : in    std_logic_vector(4 downto 0);  -- Card address
    bcout_ad      : out   std_logic_vector(4 downto 0);  -- Out board address
    -- GTL enable in/out
    oeba_l        : out   std_logic;    -- GTL: enable in low bits
    oeab_l        : out   std_logic;    -- GTL: enable out low bits
    oeba_h        : out   std_logic;    -- GTL: enable in high bits
    oeab_h        : out   std_logic;    -- GTL: enable out high bits
    ctr_in        : out   std_logic;    -- GTL: enable ctrl bus in
    ctr_out       : out   std_logic;    -- GTL: enable ctrl bus out
    -- Bus 
    bd            : inout std_logic_vector(39 downto 0);  -- Bus: 40 bits
    -- Control 
    acknb         : inout std_logic;    -- CTL: Acknowledge
    errorb        : inout std_logic;    -- CTL: Error
    trsfb         : inout std_logic;    -- CTL: Transfer
    dstbb         : inout std_logic;    -- CTL: Data strobe
    writeb        : inout std_logic;    -- CTL: Write 
    cstbb         : inout std_logic;    -- Control strobe
    bc_int        : out   std_logic;    -- CTL: Interrupt
    -- Misc
    otib          : in    std_logic;    -- Not used
    mcst          : out   std_logic;    -- ADC : Montor start disabled='0';
    clk10         : in    std_logic;    -- Not used
    debug_on      : in    std_logic;    -- Not used (should be high)
    -- ALTRO 
    altro_sw      : out   std_logic;    -- ALTRO switch
    sample_clk    : out   std_logic_vector(2 downto 0);  -- Sample clock 
    altro_l1      : out   std_logic;    -- L1 for ALTROs (active low)
    altro_l2      : out   std_logic;    -- L2 for ALTROs (active low)
    alps_errorb   : in    std_logic;    -- ALTRO power supply error
    al_dolo_enb   : in    std_logic;    -- ALTRO data out enable
    al_trsf_enb   : in    std_logic;    -- ALTRO transfer enable
    al_ackn_enb   : in    std_logic;    -- ALTRO ackn enable (Not used)
    -- VA1:
    digital_reset : out   std_logic;    -- Reset VA1's (-)
    test_on       : out   std_logic;    -- Calibration mode on (-)
    pulser_enable : out   std_logic;    -- Enable step on pulser (-)
    shift_in      : out   std_logic_vector(2 downto 0);  -- shift reset (-)
    shift_clk     : out   std_logic_vector(2 downto 0);  -- shift register (-)
    hold          : out   std_logic_vector(2 downto 0);  -- Hold charge (-)
    -- DAC interface
    dac_addr      : out   std_logic_vector (11 downto 0);  -- DAC address
    dac_data      : out   std_logic_vector (7 downto 0);  -- DAC data
    -- L0 interface
    busy          : out   std_logic;    -- busy (LED on test)
    -- Debug
    debug         : out   std_logic_vector(14 downto 0));  -- Debug pins
end entity bc;

architecture rtl of bc is
  -- Control 
  signal cstb_i           : std_logic;  -- Control strobe
  signal write_i          : std_logic;  -- Write flag
  signal dstb_i           : std_logic;  -- Data strobe
  signal trsf_i           : std_logic;  -- Data transfer
  signal l1_i             : std_logic;  -- L1 trigger
  signal l2_i             : std_logic;  -- L2 trigger
  signal ackn_i           : std_logic;  -- Acknowledge
  -- Bus control
  signal bc_dolo_en_i     : std_logic;  -- GTL out enable
  signal bc_cs_i          : std_logic;  -- BC card select
  signal al_cs_i          : std_logic;  -- ALTRO card select
  signal wr_al_i          : std_logic;  -- Write ALTRO
  signal bc_ackn_en_i     : std_logic;  -- Acknowledge enable
  signal bc_ackn_i        : std_logic;  -- Acknowledge
  signal bc_int_i         : std_logic;  -- Interrupt
  signal bc_error_i       : std_logic;  -- BC error
  -- Bus 
  signal din_i            : std_logic_vector(39 downto 0);   -- ALTRO bus
  -- Serial 
  signal sda_out_i        : std_logic;  -- I2C serial data out
  signal slctr_i          : std_logic;  -- I2C serial control
  signal dout_i           : std_logic_vector(15 downto 0);   -- Output data
  -- Power supplies
  signal paps_error_i     : std_logic;  -- Pasa power supp. error
  signal alps_error_i     : std_logic;  -- ALTRO power supp. error
  -- Test mode or read out event length 
  signal rmtrsf_en_i      : std_logic;  -- R/M transfer enable
  signal rmtrsf_i         : std_logic;  -- R/M transfer
  signal rmdstb_i         : std_logic;  -- R/M data strobe
  signal rmdata_out_i     : std_logic_vector(39 downto 0);   -- R/M data
  -- Test mode 
  signal adcclk_en_i      : std_logic;  -- ADC clock enable
  signal tsm_on_i         : std_logic;  -- Test mode on
  signal card_isolation_i : std_logic;  -- Card isolated
  -- signal mem_wren_i       : std_logic;  -- TSM: memory write
  -- signal sclk_edge_i      : std_logic;  -- Slow clock edge
  -- Event length 
  signal evl_cstb_i       : std_logic;  -- EVL: control strobe
  signal alevl_rdtrx_i    : std_logic;  -- EVL: read/write
  signal bc_master_i      : std_logic;  -- EVL: BC is master
  signal evl_addr_i       : std_logic_vector(39 downto 20);  -- EVL : addr
  -- ALTRO stuff 
  signal al_error_i       : std_logic;  -- ALTRO error
  signal al_errorb_i      : std_logic;  -- ALTRO error
  signal al_dolo_en_i     : std_logic;  -- ALTRO data enable 
  signal al_trsf_en_i     : std_logic;  -- ALTRO transfer enable
  signal ackn_chrdo_i     : std_logic;  -- Acknowledge channel readout
  signal altro_sw_i       : std_logic;  -- ALTRO switch
  signal pasa_sw_i        : std_logic;  -- PASA switch
  signal al_rst_i         : std_logic;  -- ALTRO reset
  -- Misc. 
  -- signal ric_i            : std_logic;  -- Reading (not used?)
  -- signal read_data_i      : std_logic;  -- Reading (not used?)
  signal or_rst_i         : std_logic;  -- Internal/External reset
  -- signal lastst_al_i      : std_logic;  -- Last state bus interface
  -- signal endtrans_i       : std_logic;  -- End of transmission
  -- FMDD
  -- Registers 
  signal hold_wait_i      : std_logic_vector(15 downto 0);   -- FMD: Wait hold
  signal l1_timeout_i     : std_logic_vector(15 downto 0);   -- FMD: L1 timeout
  signal l2_timeout_i     : std_logic_vector(15 downto 0);   -- FMD: L2 timeout
  signal shift_div_i      : std_logic_vector(15 downto 0);   -- FMD: Shift clk
  signal strips_i         : std_logic_vector(15 downto 0);   -- FMD: Strips
  signal cal_level_i      : std_logic_vector(15 downto 0);   -- FMD: Cal pulse
  signal shape_bias_i     : std_logic_vector(15 downto 0);   -- FMD: Shape bias
  signal vfs_i            : std_logic_vector(15 downto 0);   -- FMD: Shape ref
  signal vfp_i            : std_logic_vector(15 downto 0);   -- FMD: Preamp ref
  signal sample_div_i     : std_logic_vector(15 downto 0);   -- FMD: Sample clk
  signal fmdd_cmd_i       : std_logic_vector(15 downto 0);   -- FMD: Commands
  signal fmdd_stat_i      : std_logic_vector(15 downto 0);   -- FMD: Commands
  signal cal_iter_i       : std_logic_vector(15 downto 0);   -- FMD: Cal event.
  signal cal_delay_i      : std_logic_vector(15 downto 0);   -- FMD: Cal event.
  -- VA1 interface
  signal hold_i           : std_logic;
  signal shift_clk_i      : std_logic;
  signal shift_in_i       : std_logic;
  signal sample_clk_i     : std_logic;
  signal digital_reset_i  : std_logic;
  signal bcout_ad_i       : std_logic_vector(4 downto 0);    -- Out address
  signal altro_l1_i       : std_logic;
  signal altro_l2_i       : std_logic;
  -- MEB
  signal al_rpinc_i       : std_logic;
  signal mebs_i           : std_logic_vector(4 downto 0);
  signal meb_cnt_i        : std_logic_vector(3 downto 0);
  signal meb_full_i       : std_logic;
  
  -- Dummies 
  -- signal tp_spare0 : std_logic;      -- Test pin 0
  -- signal tp_spare2 : std_logic;      -- Test pin 1
  -- signal tp_spare3 : std_logic;      -- Test pin 3
  -- signal tp_spare4 : std_logic;      -- Test pin 4
  -- signal tp_spare5 : std_logic;      -- Test pin 5
  signal test_d : std_logic;            -- TSM: mask d
  signal test_e : std_logic;            -- TSM: mask e
  signal test_f : std_logic;            -- TSM: mask f
  signal test_g : std_logic;            -- TSM: mask g
  signal test_h : std_logic;            -- TSM: mask h

  signal debug_i  : std_logic_vector(14 downto 0);
  signal debug_ii : std_logic_vector(15 downto 0);
  signal oeba_l_i : std_logic;
  signal oeab_l_i : std_logic;
  signal oeba_h_i : std_logic;
  signal oeab_h_i : std_logic;

  signal fmd_busy_i : std_logic;
  signal l2r_i      : std_logic;        -- Level 2 reject
  
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    bc_int        <= not bc_int_i;      -- Negating interrupt
    altro_sw      <= altro_sw_i;        -- Fan out of altro switch
    pasa_sw       <= pasa_sw_i;         -- Fan out of pasa switch
    adcclk_en     <= not adcclk_en_i;   -- Negate
    or_rst_i      <= not al_rst_i and rstb;  -- Either iternal or external
    -- sda_out       <= sda_out_i when slctr_i = '1' else 'Z';  -- Tri-state
    sda_out       <= sda_out_i when slctr_i = '1' else 'Z';  -- Tri-state
    mcst          <= '1';
    oeba_l        <= oeba_l_i;
    oeab_l        <= oeab_l_i;
    oeba_h        <= oeba_h_i;
    oeab_h        <= oeab_h_i;
    busy          <= fmd_busy_i or meb_full_i;
    -- tp_spare0  <= bc_ackn_en_i;
    -- tp_spare2  <= lastst_al_i;
    -- tp_spare3  <= read_data_i;
    -- tp_spare4  <= ric_i;
    -- tp_spare5  <= endtrans_i;
    altro_l1      <= altro_l1_i;
    altro_l2      <= altro_l2_i;
    shift_clk(0)  <= shift_clk_i;
    shift_clk(1)  <= shift_clk_i;
    shift_clk(2)  <= shift_clk_i;
    shift_in(0)   <= shift_in_i;
    shift_in(1)   <= shift_in_i;
    shift_in(2)   <= shift_in_i;
    sample_clk(0) <= sample_clk_i;
    sample_clk(1) <= sample_clk_i;
    sample_clk(2) <= sample_clk_i;

    hold(0)       <= hold_i;
    hold(1)       <= hold_i;
    hold(2)       <= hold_i;
    digital_reset <= digital_reset_i;
    -- Hack due to error in print-board
    bcout_ad(0)   <= '0';  -- bcout_ad_i(0);  -- 
    bcout_ad(1)   <= '0';  -- bcout_ad_i(1);  -- 
    bcout_ad(2)   <= '0';  -- bcout_ad_i(2);  -- 
    bcout_ad(3)   <= bcout_ad_i(0);  -- bcout_ad_i(3);  -- 
    bcout_ad(4)   <= bcout_ad_i(1);  -- bcout_ad_i(4);  --
    debug(12 downto 0) <= debug_i(12 downto 0);
    debug(13)     <= meb_full_i;
    debug(14)     <= fmd_busy_i or meb_full_i;
    -- Override the debug output from the FMD part.
    -- debug(0)           <= clk; -- slctr_i;
    -- debug(1)           <= scl;
    -- debug(2)           <= sda_in;
    -- debug(3)           <= sda_out_i when slctr_i = '1' else 'H';
    -- debug(14 downto 4) <= debug_ii(10 downto 0);
    -- debug(4)           <= clk; -- fmd_busy_i or meb_full_i;
    -- debug(13)          <= mebs_i(4);
    -- debug(14)          <= meb_full_i;
    -- debug         <= debug_i;
    -- Bus debugging
    -- debug(4 downto 0) <= debug_i(4 downto 0);
    -- debug(4 downto 0) <= hadd(4 downto 0);  -- dout_i(3 downto 0);
    -- debug(4)          <= debug_i(3);
    -- debug(5)          <= cstb_i;
    -- debug(6)          <= write_i;
    -- debug(7)          <= dstb_i;
    -- debug(8)          <= alps_error_i;  -- trsf_i;
    -- debug(9)          <= ackn_i;
    -- debug(10)         <= bc_ackn_i;
    -- debug(11)         <= bc_cs_i;
    -- debug(12)         <= al_cs_i;
    -- debug(13)         <= bc_error_i;
    -- debug(14)         <= al_error_i;
    -- debug(12)         <= oeab_l_i;
    -- debug(13)         <= oeba_h_i;
    -- debug(14)         <= oeab_h_i;
    -- debug(10 downto 6)  <= hadd;
    -- debug(14 downto 11) <= (others => '0');
  end block combi;

  core : entity work.bc_core(rtl2)         -- entity work.bc_core
    port map (
      -- Control bus 
      clk            => clk,               -- in  Clock
      sclk           => sclk,              -- in  Slow clock
      rstb           => rstb,              -- in  Async reset
      cstb           => cstb_i,            -- in  Control strobe
      write          => write_i,           -- in  Write flag
      dstb           => dstb_i,            -- in  Data strobe
      trsf           => trsf_i,            -- in  Data transfer
      l1_trg         => l1_i,              -- in  L1 trigger
      l2_trg         => l2_i,              -- in  L2 trigger
      ackn           => ackn_i,            -- in  Acknowledge
      hadd           => hadd,              -- in  Card address
      -- Bus
      din            => din_i,             -- in  ALTRO bus
      -- I2C input
      scl            => scl,               -- in  I2C clock
      sda_in         => sda_in,            -- in  I2C serial data in
      sda_out        => sda_out_i,         -- out I2C serial data out
      slctr          => slctr_i,           -- out I2C serial control
      -- Misc input
      paps_error     => paps_error_i,      -- in  Pasa power supp. error
      alps_error     => alps_error_i,      -- in  ALTRO power supp. error
      al_err         => al_error_i,        -- in  ALTRO error
      -- debug_sw       => debug_on,       -- in  Debug switch
      debug_sw       => '1',               -- in  Debug switch - fix 06/03/07
      -- Bus interface 
      bc_ackn_en     => bc_ackn_en_i,      -- out Acknowledge enable
      bc_ackn        => bc_ackn_i,         -- out Acknowledge
      lastst_al      => open,  -- lastst_al_i,       -- out Last state bus interface
      endtrans       => open,  -- endtrans_i,        -- out End of transmission
      bc_dolo_en     => bc_dolo_en_i,      -- out GTL out enable
      bc_cs          => bc_cs_i,           -- out BC card select
      al_cs          => al_cs_i,           -- out ALTRO card select
      wr_al          => wr_al_i,           -- out Write ALTRO
      dout           => dout_i,            -- out Output data
      bc_int         => bc_int_i,          -- out Interrupt
      bc_error       => bc_error_i,        -- out BC error
      -- ALTRO interface
      altro_sw       => altro_sw_i,        -- out ALTRO switch
      pasa_sw        => pasa_sw_i,         -- out PASA switch
      al_rst         => al_rst_i,          -- out ALTRO reset
      -- Test mode
      rdoclk_en      => rdoclk_en,         -- out Readout clock enable
      adcclk_en      => adcclk_en_i,       -- out ADC clock enable
      adc_add0       => adc_add0,          -- out ADC address 0
      adc_add1       => adc_add1,          -- out ADC address 1
      tsm_on         => tsm_on_i,          -- out Test mode on
      card_isolation => card_isolation_i,  -- out Card isolated
      test_a         => test_a,            -- out Test word A
      test_b         => test_b,            -- out Test word B
      test_c         => test_c,            -- out Test word C
      test_d         => open,  -- test_d,            -- out Test word D
      test_e         => open,  -- test_e,            -- out Test word E
      test_f         => open,  -- test_f,            -- out Test word F
      test_g         => open,  -- test_g,            -- out Test word G
      test_h         => open,  -- test_h,            -- out Test word H
      rmtrsf_en      => rmtrsf_en_i,       -- out R/M transfer enable
      rmtrsf         => rmtrsf_i,          -- out R/M transfer
      rmdstb         => rmdstb_i,          -- out R/M data strobe
      rmdata_out     => rmdata_out_i,      -- out R/M data
      mem_wren       => open,  -- mem_wren_i,        -- out TSM: memory write
      sclk_edge      => open,  -- sclk_edge_i,       -- out Slow clock edge
      evl_cstb       => evl_cstb_i,        -- out EVL: control strobe
      alevl_rdtrx    => alevl_rdtrx_i,     -- out EVL: read/write
      bc_master      => bc_master_i,       -- out EVL: BC is master
      evl_addr       => evl_addr_i,        -- out EVL: addr
      -- Monitor interface 
      mscl           => mscl,              -- out Monitor clock
      msda           => msda,              -- inout Monitor serial data out
      -- Multi-event buffer interface
      al_rpinc       => al_rpinc_i,
      mebs           => mebs_i,
      meb_cnt        => meb_cnt_i,
      -- FMDD
      fmdd_stat      => fmdd_stat_i,       -- in  FMDD status
      l0             => l0,                -- in  L0 trigger (for counter)
      hold_wait      => hold_wait_i,       -- out FMD: Wait to hold
      l1_timeout     => l1_timeout_i,      -- out FMD: L1 timeout
      l2_timeout     => l2_timeout_i,      -- out FMD: L2 timeout
      shift_div      => shift_div_i,       -- out FMD: Shift clk
      strips         => strips_i,          -- out FMD: Strips
      cal_level      => cal_level_i,       -- out FMD: Cal pulse
      shape_bias     => shape_bias_i,      -- out FMD: Shape bias
      vfs            => vfs_i,             -- out FMD: Shape ref
      vfp            => vfp_i,             -- out FMD: Preamp ref
      sample_div     => sample_div_i,      -- out FMD: Sample clk
      fmdd_cmd       => fmdd_cmd_i,        -- out FMD: Commands
      cal_iter       => cal_iter_i,        -- out FMD: Calibration events
      cal_delay      => cal_delay_i,       -- out CAL: xtra L1 delay
      debug          => debug_ii);         -- out Debug

  fmd : entity work.fmdd
    port map (
      clk           => clk,              -- in  Clock
      sclk          => sclk,             -- Sample clock 
      rstb          => rstb,             -- in  Async reset
      -- Triggers ----------------------------------------------------------
      l0            => l0,               -- in  L0 trigger
      l1b           => l1b,              -- in  L1 trigger
      l2b           => l2b,              -- in  L2 trigger
      hold_wait     => hold_wait_i,      -- in  Wait to hold
      l1_timeout    => l1_timeout_i,     -- in  L1 timeout
      l2_timeout    => l2_timeout_i,     -- in  L2 timeout
      busy          => fmd_busy_i,       -- out Busy signal
      l2r           => l2r_i,            -- out Level 2 reject
      -- VA1 readout -------------------------------------------------------
      shift_div     => shift_div_i,      -- in  Shift clock params
      shift_in      => shift_in_i,       -- out Shift reg. reset to VA1s
      shift_clk     => shift_clk_i,      -- out Shift clock to VA1s
      strips        => strips_i,         -- in  Strip range
      digital_reset => digital_reset_i,  -- out Reset VA1s
      hold          => hold_i,           -- out Hold values in VA1
      -- Calibration pulser ------------------------------------------------
      cal_level     => cal_level_i,      -- in  Cal. level
      cal_iter      => cal_iter_i,       -- in Calibration events
      cal_on        => test_on,          -- out Calib mode on
      cal_enable    => pulser_enable,    -- out Enable cal step
      cal_delay     => cal_delay_i,      -- in  xtra L1 delay
      -- Bias voltages -----------------------------------------------------
      shape_bias    => shape_bias_i,     -- in  Shape bias
      vfs           => vfs_i,            -- in  Shape ref. 
      vfp           => vfp_i,            -- in  Pre-amp. ref.
      dac_addr      => dac_addr,         -- out DAC address
      dac_data      => dac_data,         -- out DAC value
      -- ALTRO interface ---------------------------------------------------
      sample_div    => sample_div_i,     -- in  Sample clock params
      sample_clk    => sample_clk_i,     -- out Sample clock to ALTROs
      altro_l1      => altro_l1_i,       -- out L1 to ALTROs
      altro_l2      => altro_l2_i,       -- out L2 to ALTROs
      -- Misc --------------------------------------------------------------
      command       => fmdd_cmd_i,       -- in  Commands
      -- Misc --------------------------------------------------------------
      status        => fmdd_stat_i,      -- out 
      debug         => debug_i);         -- out Status

  busybox : meb_watchdog
    port map (
      clk      => clk,
      rstb     => rstb,
      l1       => altro_l1_i,
      l2r      => l2r_i,
      al_rpinc => al_rpinc_i,
      enable   => mebs_i(4),
      mebs     => mebs_i(3 downto 0),
      meb_cnt  => meb_cnt_i,
      full     => meb_full_i);

  mask : entity work.altro_sw_mask_in
    port map (
      clk         => clk,               -- in  Clock
      rstb        => rstb,              -- in  Async reset
      altro_sw    => altro_sw_i,        -- in  ALTRO switch
      alps_errorb => alps_errorb,       -- in  ALTRO power supply error from HW
      l1b         => altro_l1_i,        -- in  Level 1 (-)
      l2b         => altro_l2_i,        -- in  Level 2 (-)
      cstbb       => cstbb,             -- in  Control strobe (-)
      writeb      => writeb,            -- in  Write (-)
      al_dolo_enb => al_dolo_enb,       -- in  ALTRO GTL enable (-)
      al_trsf_enb => al_trsf_enb,       -- in  ALTRO Transfer enable (-)
      trsfb       => trsfb,             -- in  Transfer
      dstbb       => dstbb,             -- in  Data strobe (-)
      or_rstb     => or_rst_i,          -- in  Sync or Async reset (-)
      acknb       => acknb,             -- in  Acknowledge (-)
      hadd        => hadd,              -- in  Card address
      pasa_sw     => pasa_sw_i,         -- in  PASA switch
      paps_errorb => paps_errorb,       -- in  PASA power sup. error (-)
      al_errorb   => al_errorb_i,       -- in  ALTRO error (-)
      alps_error  => alps_error_i,      -- out ALTRO power sup. error (+)
      l1          => l1_i,              -- out Level 1 (+)
      l2          => l2_i,              -- out Level 2 (+)
      cstb        => cstb_i,            -- out Control strobe (+)
      write       => write_i,           -- out Write (+)
      al_dolo_en  => al_dolo_en_i,      -- out ALTRO data transfer enable (+)
      al_trsf_en  => al_trsf_en_i,      -- out ALTRO transfer enable (+)
      trsf        => trsf_i,            -- out Transfer
      dstb        => dstb_i,            -- out Data strobe
      ackn        => ackn_i,            -- out Acknowledge (+)
      ackn_chrdo  => ackn_chrdo_i,      -- out Acknowledge during readout (+)
      rst_fbc     => rst_fbc,           -- out Front-end reset
      bcout_add   => bcout_ad_i,        -- out Output hardware address
      paps_error  => paps_error_i,      -- out PASA power supl. error (+)
      al_error    => al_error_i);       -- out ALTRO error (+)


  driv : entity work.drivers
    port map (
      clk            => clk,            -- in  Clock
      rstb           => rstb,           -- in  Async reset
      tsm_on         => tsm_on_i,       -- in  TSM: on
      al_cs          => al_cs_i,        -- in  ALTRO card select
      bc_cs          => bc_cs_i,        -- in  BC card select
      cstb           => cstb_i,         -- in  Bus: Control strobe
      wr_al          => wr_al_i,        -- in  Bus: Write
      al_dolo_en     => al_dolo_en_i,   -- in  Bus: ALTRO data enable
      bc_dolo_en     => bc_dolo_en_i,   -- in  Bus: BC data enable
      al_trsf_en     => al_trsf_en_i,   -- in  Bus: ALTRO transfer enable
      bc_master      => bc_master_i,    -- in  Bus: BC is local master
      card_isolation => card_isolation_i,  -- in  Bus: Card is isolated
      bc_ackn        => bc_ackn_i,      -- in  Bus: Acknowledge from BC
      bc_ack_en      => bc_ackn_en_i,   -- in  Bus: Acknowledge from BC enable
      bc_error       => bc_error_i,     -- in  Bus: Error
      dout           => dout_i,         -- in  Bus: data out
      alevl_rdtrx    => alevl_rdtrx_i,  -- in  EVL: Transmit
      evl_cstb       => evl_cstb_i,     -- in  EVL: Control strobe
      evl_addr       => evl_addr_i,     -- in  EVL: address out
      rmtrsf_en      => rmtrsf_en_i,    -- in  TSM/EVL: Transfer enable
      ackn_chrdo     => ackn_chrdo_i,   -- in  TSM/EVL: Acknowledge
      rmtrsf         => rmtrsf_i,       -- in  TSM/EVL: transfer
      rmdstb         => rmdstb_i,       -- in  TSM/EVL: Data strob
      rmdata_out     => rmdata_out_i,   -- in  TSM/EVL: data out
      altro_sw       => altro_sw_i,     -- in  HW: ALTRO switch
      sda_out        => sda_out_i,      -- in  I2C: serial data out
      oeba_l         => oeba_l_i,       -- out GTL: in (low bits)
      oeab_l         => oeab_l_i,       -- out GTL: out (low bits)
      oeba_h         => oeba_h_i,       -- out GTL: in (high bits)
      oeab_h         => oeab_h_i,       -- out GTL: out (high bits)
      read_data      => open, -- read_data_i,    -- out Read data 
      ric            => open, -- ric_i,          -- out ???
      ctr_in         => ctr_in,         -- out Control in
      ctr_out        => ctr_out,        -- out Control out
      al_errorb      => al_errorb_i,    -- out ALTRO error
      din            => din_i,          -- out Bus: Data input
      bd             => bd,             -- inout Bus: tri-state 
      acknb          => acknb,          -- inout Ctrl: Acknowledge tri-state
      errorb         => errorb,         -- inout Ctrl: Control strobe tri-state
      trsfb          => trsfb,          -- inout Ctrl: Transfer tri-state
      dstbb          => dstbb,          -- inout Ctrl: Data strobe tri-state
      writeb         => writeb,         -- inout Ctrl: Write tri-state
      cstbb          => cstbb);         -- inout Ctrl: Control strobe 
end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package bc_pack is
  component bc
    port (
      clk           : in    std_logic;  -- Clock
      rstb          : in    std_logic;  -- Async reset
      sclk          : in    std_logic;  -- Sample clock (10 MHz)
      scl           : in    std_logic;  -- I2C: Serial clock (<= 5MHz)
      sda_in        : in    std_logic;  -- I2C: data input
      sda_out       : out   std_logic;  -- I2C: data out
      l0            : in    std_logic;  -- Level 0 trigger 
      l1b           : in    std_logic;  -- Level 1 trigger
      l2b           : in    std_logic;  -- Level 2 trigger
      mscl          : out   std_logic;  -- ADC: clock
      msda          : inout std_logic;  -- ADC: data in/out
      pasa_sw       : out   std_logic;  -- PASA switch
      paps_errorb   : in    std_logic;  -- PASA power supply error
      rdoclk_en     : out   std_logic;  -- TSM: Readout clock enable
      adcclk_en     : out   std_logic;  -- TSM: ADC clock enable
      adc_add0      : out   std_logic;  -- ADC address bit 0
      adc_add1      : out   std_logic;  -- ADC address bit 1
      test_a        : out   std_logic;  -- TSM: mask a
      test_b        : out   std_logic;  -- TSM: mask b
      test_c        : out   std_logic;  -- TSM: mask c
      rst_fbc       : out   std_logic;  -- Reset front-end card
      hadd          : in    std_logic_vector(4 downto 0);  -- Card address
      bcout_ad      : out   std_logic_vector(4 downto 0);  -- Out board address
      oeba_l        : out   std_logic;  -- GTL: enable in low bits
      oeab_l        : out   std_logic;  -- GTL: enable out low bits
      oeba_h        : out   std_logic;  -- GTL: enable in high bits
      oeab_h        : out   std_logic;  -- GTL: enable out high bits
      ctr_in        : out   std_logic;  -- GTL: enable ctrl bus in
      ctr_out       : out   std_logic;  -- GTL: enable ctrl bus out
      bd            : inout std_logic_vector(39 downto 0);  -- Bus: 40 bits
      acknb         : inout std_logic;  -- CTL: Acknowledge
      errorb        : inout std_logic;  -- CTL: Error
      trsfb         : inout std_logic;  -- CTL: Transfer
      dstbb         : inout std_logic;  -- CTL: Data strobe
      writeb        : inout std_logic;  -- CTL: Write 
      cstbb         : inout std_logic;  -- Control strobe
      bc_int        : out   std_logic;  -- CTL: Interrupt
      otib          : in    std_logic;  -- Not used
      mcst          : out   std_logic;  -- ADC : Montor start disabled='0';
      clk10         : in    std_logic;  -- Not used
      debug_on      : in    std_logic;  -- Not used (should be high)
      altro_sw      : out   std_logic;  -- ALTRO switch
      sample_clk    : out   std_logic_vector(2 downto 0);  -- Sample clock 
      altro_l1      : out   std_logic;  -- L1 for ALTROs (active low)
      altro_l2      : out   std_logic;  -- L2 for ALTROs (active low)
      alps_errorb   : in    std_logic;  -- ALTRO power supply error
      al_dolo_enb   : in    std_logic;  -- ALTRO data out enable
      al_trsf_enb   : in    std_logic;  -- ALTRO transfer enable
      al_ackn_enb   : in    std_logic;  -- ALTRO ackn enable (Not used)
      digital_reset : out   std_logic;  -- Reset VA1's
      test_on       : out   std_logic;  -- Calibration mode on.
      pulser_enable : out   std_logic;  -- Enable step on pulser
      shift_in      : out   std_logic_vector(2 downto 0);  -- shift reset
      shift_clk     : out   std_logic_vector(2 downto 0);  -- shift register
      hold          : out   std_logic_vector(2 downto 0);  -- Hold charge
      dac_addr      : out   std_logic_vector (11 downto 0);  -- DAC address
      dac_data      : out   std_logic_vector (7 downto 0);  -- DAC data
      busy          : out   std_logic;  -- busy (LED on test)
      debug         : out   std_logic_vector(14 downto 0));  -- Debug pins
  end component bc;
end package bc_pack;
------------------------------------------------------------------------------
--
-- EOF
--
