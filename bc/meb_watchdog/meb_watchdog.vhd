------------------------------------------------------------------------------
-- Title      : Data strobe counter
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : dstb_counter.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2008-09-02
-- Platform   : ACEX1K - EP1K100QC208-1
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Counts data strobes from ALTRO's
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/07  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @brief Multi-event buffer watchdog.
--! @ingroup bc
--! This entity implements a "busy-box".
--!
--! On reception of an L2 accept trigger the internal counter of free event
--! buffers is decremented.   When the command @c RPINC is sent from the RCU to
--! the ALTROs (in broadcast), after the RCU has finished reading out a full
--! event,  the internal counter is incremented.  If the internal counter
--! reaches zero, the output signal @a full is asserted.
--! 
--! The @a full output can be masked out by the input signal @a enable.  When
--! @a full is asserted, the busy signal should be asserted thereby blocking
--! all triggers until a new @a RPINC is seen, meaning a buffer is free in the
--! ALTROs. 
--! 
--! The number of buffers can be set freely in the range from 0 to 15.
--! However, it should never exceed the number of buffers configured in the
--! ALTROs (the number of RCU buffers doesn't matter).
--!
--! In practise, this means that the maximum should be in the range 1-4 for 4
--! configured buffers in the ALTROs, and 1-8 for 8 configured buffers in the
--! ALTROs. It's probably a good idea to leave out one, such that the ranges
--! will be 1-3 and 1-7.
entity meb_watchdog is
  port (
    clk      : in  std_logic;           --! Clock
    rstb     : in  std_logic;           --! A-sync. reset (active low)
    l1       : in  std_logic;           --! L1 trigger (active low)
    l2r      : in  std_logic;           --! L2 reject (active low)
    al_rpinc : in  std_logic;           --! ALTRO read-pointer increment
    enable   : in  std_logic;           --! Enable the watch dog
    mebs     : in  std_logic_vector(3 downto 0);  --! Number of MEBs
    meb_cnt  : out std_logic_vector(3 downto 0);  --! MEB counter
    full     : out std_logic);          --! MEBs full
end meb_watchdog;

-------------------------------------------------------------------------------
architecture rtl of meb_watchdog is
  -- type   edge_st_t is (idle, up_edge, down_edge);  -- State of edge detect
  -- signal rpinc_st_i  : edge_st_t;                  -- State of edge detect
  -- signal l1_st_i    : edge_st_t;                  -- State of edge detect

  signal rpinc_i      : std_logic;      --! registered @a al_rpinc
  signal old_rpinc_i  : std_logic;      --! Previous value of @a rpinc_i
  signal l1_i        : std_logic;      --! Registered trigger
  signal old_l1_i    : std_logic;      --! Previous value of trigger
  signal cnt_i        : unsigned(3 downto 0) := (others => '1');  --! counter.
  signal max_i        : unsigned(3 downto 0);  --! Max value
  signal stored_max_i : unsigned(3 downto 0);  --! Max value
  signal increment_i  : std_logic;      --! Increment counter
  signal decrement_i  : std_logic;      --! Decrement counter

  -- purpose: Convert unsigned to string
  function u2str (x: unsigned)
    return string is
  begin  -- u2str
    return integer'image(to_integer(x));
  end u2str;
  
begin  -- rtl
  --! Collect combinatorics
  combinatorics : block
  begin  -- block combinatorics
    full         <= '1' when cnt_i = 0 and enable = '1' else '0';
  end block combinatorics;

  --! Edge detect on rpinc and l1
  --! @param clk Clock
  --! @param rstb Async. reset
  --! @return increment, decrement
  edge_detect : process (clk, rstb)
  begin  -- process edge_detect
    if rstb = '0' then                  -- asynchronous reset (active low)
      old_rpinc_i  <= '0';
      old_l1_i    <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge
      rpinc_i     <= al_rpinc;
      old_rpinc_i <= rpinc_i;

      l1_i       <= l1; 
      old_l1_i   <= l1_i;

      if (old_rpinc_i = '0' and rpinc_i = '1') or l2r = '1' then
        increment_i <= '1';
      else
        increment_i <= '0';
      end if;
      if old_l1_i = '1' and l1_i = '0' then
        decrement_i <= '1';
      else
        decrement_i <= '0';
      end if;
    end if;
  end process edge_detect;
  -- increment_i <= '1' when old_rpinc_i = '0' and rpinc_i = '1' else '0';
  -- decrement_i <= '1' when old_l1_i = '1'   and l1_i = '0'   else '0';

  
  --! purpose: Look for L1 and internal decrement signal
  --! @param clk Clock
  --! @param rstb Async. reset
  --! @return cnt_i
  fsm : process (clk, rstb)
  begin  -- process fsm
    if rstb = '0' then  -- asynchronous reset (active low)
      cnt_i        <= unsigned(mebs);  -- max_i;
      max_i        <= unsigned(mebs);
      stored_max_i <= unsigned(mebs);
    elsif clk'event and clk = '1' then  -- rising clock edge
      stored_max_i <= unsigned(mebs);
      if increment_i = '1' then
        if cnt_i /= max_i then  -- max_i then
          cnt_i <= cnt_i + 1;
        end if;
      end if;
      if decrement_i = '1' then
        if cnt_i /= 0 then
          cnt_i <= cnt_i - 1;
        end if;
      end if;  -- if cnt_i /= max_i
      if ((max_i /= stored_max_i) and
          (increment_i = '0') and
          (decrement_i = '0') and
          (stored_max_i >= (max_i - cnt_i))) then
        report "Changing max from " & u2str(max_i) &
          " to " & u2str(stored_max_i) &
          " and setting counter from " & u2str(cnt_i) &
          " to " & u2str(stored_max_i - (max_i - cnt_i)) severity note;
        max_i <= stored_max_i;
        cnt_i <= stored_max_i - (max_i - cnt_i);
      end if;
    end if;  -- if enable
  end process fsm;

  --! Register output of counter
  --! @param clk Clock
  --! @param rstb Async. reset
  --! @return meb_cnt
  output: process (clk, rstb)
  begin  -- process output
    if rstb = '0' then                    -- asynchronous reset (active low)
      meb_cnt <= (others => '1');
    elsif clk'event and clk = '1' then    -- rising clock edge
      meb_cnt <= std_logic_vector(cnt_i);
    end if;
  end process output;
end rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Package of multi-event buffer watchdog
package meb_watchdog_pack is
  component meb_watchdog
    port (clk      : in  std_logic;     -- ! Clock
          rstb     : in  std_logic;     -- ! A-sync. reset (active low)
          l1       : in  std_logic;     -- ! L1 trigger (active low)
          l2r      : in  std_logic;     -- ! L2 reject (active low)
          al_rpinc : in  std_logic;     -- ! ALTRO read-pointer increment
          enable   : in  std_logic;     -- ! Enable the watch dog
          mebs     : in  std_logic_vector(3 downto 0);  -- ! Number of MEBs
          meb_cnt  : out std_logic_vector(3 downto 0);  -- ! MEB counter
          full     : out std_logic);    -- ! MEBs full
  end component;
end meb_watchdog_pack;
------------------------------------------------------------------------------
--
-- EOF
--
