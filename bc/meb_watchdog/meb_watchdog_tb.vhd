-------------------------------------------------------------------------------
-- Title      : Testbench for design "meb_watchdog"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : meb_watchdog_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Created    : 2008-07-17
-- Last update: 2008-07-24
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2008 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2008-07-17  1.0      cholm	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.meb_watchdog_pack.all;

-------------------------------------------------------------------------------
entity meb_watchdog_tb is
end meb_watchdog_tb;

-------------------------------------------------------------------------------

architecture test of meb_watchdog_tb is
  constant PERIOD : time := 25 ns;
  constant SLEEP  : time := 16 * PERIOD;
  
  -- component ports
  signal clk_i      : std_logic := '1';
  signal rstb_i     : std_logic := '0';
  signal l2a_i      : std_logic := '1';
  signal al_rpinc_i : std_logic := '0';
  signal enable_i   : std_logic := '1';
  signal mebs_i     : std_logic_vector(3 downto 0) := "0100";
  signal meb_cnt_i  : std_logic_vector(3 downto 0);
  signal full_i     : std_logic;

  -- purpose: Make an L2a
  procedure make_l2a (
    signal   clk    : in  std_logic;
    signal   l2a    : out std_logic) is
  begin  -- make_l2a
    wait until rising_edge(clk);
    l2a <= '0';
    wait for 4 * PERIOD;
    l2a <= '1';
    wait for 8 * PERIOD;
  end make_l2a;

  -- purpose: Make an L2a
  procedure make_rpinc (
    signal   clk    : in  std_logic;
    signal   rpinc  : out std_logic) is
  begin  -- make_l2a
    wait until rising_edge(clk);
    rpinc <= '1';
    wait for 4 * PERIOD;
    rpinc <= '0';
    wait until rising_edge(clk);
    wait for SLEEP;
  end make_rpinc;

begin  -- test

  -- component instantiation
  dut: meb_watchdog
    port map (
      clk      => clk_i,
      rstb     => rstb_i,
      l2a      => l2a_i,
      al_rpinc => al_rpinc_i,
      enable   => enable_i,
      mebs     => mebs_i,
      meb_cnt  => meb_cnt_i,
      full     => full_i);

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb_i <= '1';
    wait until rising_edge(clk_i);

    make_l2a(clk => clk_i, l2a => l2a_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);

    wait for 8 us - NOW;

    make_l2a(clk => clk_i, l2a => l2a_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    wait until rising_edge(clk_i);
    mebs_i <= "0010";
    wait until rising_edge(clk_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    
    wait for 16 us - NOW;
    
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_l2a(clk => clk_i, l2a => l2a_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);

    wait for 24 us - NOW;

    make_l2a(clk => clk_i, l2a => l2a_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);
    make_rpinc(clk => clk_i, rpinc => al_rpinc_i);

    wait;
  end process stim;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  clock_gen: process is
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;
  

end test;

-------------------------------------------------------------------------------
--
-- EOF
--
