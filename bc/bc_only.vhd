------------------------------------------------------------------------------
-- Title      : Top-level of board controller code
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : bc.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2010-02-24
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Top-level of board controller code
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/24  1.0      cholm   Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.bc_core_pack.all;
use work.drivers_pack.all;
use work.altro_sw_mask_in_pack.all;

entity bc_only is
  port (
    clk         : in    std_logic;      -- Clock
    rstb        : in    std_logic;      -- Async reset
    debug_sw    : in    std_logic;      -- Debug switch (should be high)
    hadd        : in    std_logic_vector(4 downto 0);  -- Card address
    sclk        : in    std_logic;      -- Sample clock (10 MHz)
    scl         : in    std_logic;      -- I2C: Serial clock (<= 5MHz)
    sda_in      : in    std_logic;      -- I2C: data input
    otib        : in    std_logic;      -- Not used
    alps_errorb : in    std_logic;      -- ALTRO power supply error
    l1b         : in    std_logic;      -- Level 1 trigger
    l2b         : in    std_logic;      -- Level 2 trigger
    al_dolo_enb : in    std_logic;      -- ALTRO data out enable
    al_trsf_enb : in    std_logic;      -- ALTRO transfer enable
    paps_errorb : in    std_logic;      -- PASA power supply error
    mscl        : out   std_logic;      -- ADC: clock
    msda        : inout std_logic;      -- ADC: data in/out
    bc_int      : out   std_logic;      -- CTL: Interrupt
    altro_sw    : out   std_logic;      -- ALTRO switch
    pasa_sw     : out   std_logic;      -- PASA switch
    rdoclk_en   : out   std_logic;      -- TSM: Readout clock enable
    adcclk_en   : out   std_logic;      -- TSM: ADC clock enable
    adc_add0    : out   std_logic;      -- ADC address bit 0
    adc_add1    : out   std_logic;      -- ADC address bit 1
    test_a      : out   std_logic;      -- TSM: mask a
    test_b      : out   std_logic;      -- TSM: mask b
    test_c      : out   std_logic;      -- TSM: mask c
    test_d      : out   std_logic;      -- TSM: mask d
    test_e      : out   std_logic;      -- TSM: mask e
    test_f      : out   std_logic;      -- TSM: mask f
    test_g      : out   std_logic;      -- TSM: mask g
    test_h      : out   std_logic;      -- TSM: mask h
    sda_out     : out   std_logic;      -- I2C: data out
    rst_fbc     : out   std_logic;      -- Reset front-end card
    bcout_ad    : out   std_logic_vector(4 downto 0);  -- Out board address
    oeba_l      : out   std_logic;      -- GTL: enable in low bits
    oeab_l      : out   std_logic;      -- GTL: enable out low bits
    oeba_h      : out   std_logic;      -- GTL: enable in high bits
    oeab_h      : out   std_logic;      -- GTL: enable out high bits
    ctr_in      : out   std_logic;      -- GTL: enable ctrl bus in
    ctr_out     : out   std_logic;      -- GTL: enable ctrl bus out
    bd          : inout std_logic_vector(39 downto 0);  -- Bus: 40 bits
    acknb       : inout std_logic;      -- CTL: Acknowledge
    errorb      : inout std_logic;      -- CTL: Error
    trsfb       : inout std_logic;      -- CTL: Transfer
    dstbb       : inout std_logic;      -- CTL: Data strobe
    writeb      : inout std_logic;      -- CTL: Write 
    cstbb       : inout std_logic;      -- Control strobe
    mcst        : out   std_logic;      -- ADC : Montor start disabled='0';
    tp_spare0   : out   std_logic;      -- Test pin 0
    tp_spare2   : out   std_logic;      -- Test pin 1
    tp_spare3   : out   std_logic;      -- Test pin 3
    tp_spare4   : out   std_logic;      -- Test pin 4
    tp_spare5   : out   std_logic);     -- Test pin 5
end entity bc_only;

architecture rtl of bc_only is
  signal cstb_i           : std_logic;  -- Control strobe
  signal write_i          : std_logic;  -- Write flag
  signal dstb_i           : std_logic;  -- Data strobe
  signal trsf_i           : std_logic;  -- Data transfer
  signal l1_i             : std_logic;  -- L1 trigger
  signal l2_i             : std_logic;  -- L2 trigger
  signal ackn_i           : std_logic;  -- Acknowledge
  signal din_i            : std_logic_vector(39 downto 0) := (others => '0');
  signal sda_out_i        : std_logic;  -- I2C serial data out
  signal paps_error_i     : std_logic;  -- Pasa power supp. error
  signal alps_error_i     : std_logic;  -- ALTRO power supp. error
  signal al_error_i       : std_logic;  -- ALTRO error
  signal debug_sw_i       : std_logic;  -- Debug switch
  signal bc_ackn_en_i     : std_logic;  -- Acknowledge enable
  signal bc_ackn_i        : std_logic;  -- Acknowledge
  signal lastst_al_i      : std_logic;  -- Last state bus interface
  signal endtrans_i       : std_logic;  -- End of transmission
  signal bc_dolo_en_i     : std_logic;  -- GTL out enable
  signal bc_cs_i          : std_logic;  -- BC card select
  signal al_cs_i          : std_logic;  -- ALTRO card select
  signal wr_al_i          : std_logic;  -- Write ALTRO
  signal dout_i           : std_logic_vector(15 downto 0) := (others => '0');
  signal bc_int_i         : std_logic;  -- Interrupt
  signal bc_error_i       : std_logic;  -- BC error
  signal altro_sw_i       : std_logic;  -- ALTRO switch
  signal pasa_sw_i        : std_logic;  -- PASA switch
  signal al_rst_i         : std_logic;  -- ALTRO reset
  signal adcclk_en_i      : std_logic;  -- ADC clock enable
  signal tsm_on_i         : std_logic;  -- Test mode on
  signal card_isolation_i : std_logic;  -- Card isolated
  signal rmtrsf_en_i      : std_logic;  -- R/M transfer enable
  signal rmtrsf_i         : std_logic;  -- R/M transfer
  signal rmdstb_i         : std_logic;  -- R/M data strobe
  signal rmdata_out_i     : std_logic_vector(39 downto 0) := (others => '0');
  signal mem_wren_i       : std_logic;  -- TSM: memory write
  signal sclk_edge_i      : std_logic;  -- Slow clock edge
  signal evl_cstb_i       : std_logic;  -- EVL: control strobe
  signal alevl_rdtrx_i    : std_logic;  -- EVL: read/write
  signal bc_master_i      : std_logic;  -- EVL: BC is master
  signal evl_addr_i       : std_logic_vector(39 downto 20) := (others => '0');
  signal or_rst_i         : std_logic;  -- Internal/External reset
  signal al_errorb_i      : std_logic;  -- ALTRO error
  signal al_dolo_en_i     : std_logic;  -- ALTRO data enable 
  signal al_trsf_en_i     : std_logic;  -- ALTRO transfer enable
  signal ackn_chrdo_i     : std_logic;  -- Acknowledge channel readout
  signal ric_i            : std_logic;  -- Reading (not used?)
  signal read_data_i      : std_logic;  -- Reading (not used?)
  -- FMDD
  signal fmdd_stat_i      : std_logic_vector(15 downto 0) := (others => '0');
  signal l0_i             : std_logic := '0';

begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi : block is
  begin  -- block combi
    bc_int      <= not bc_int_i;           -- Negating interrupt
    altro_sw    <= altro_sw_i;             -- Fan out of altro switch
    pasa_sw     <= pasa_sw_i;              -- Fan out of pasa switch
    adcclk_en   <= not adcclk_en_i;        -- Negate
    or_rst_i    <= not al_rst_i and rstb;  -- Either iternal or external
    sda_out     <= sda_out_i;              -- Fan out of sda_out
    mcst        <= '1';
    tp_spare0   <= bc_ackn_en_i;
    tp_spare2   <= lastst_al_i;
    tp_spare3   <= read_data_i;
    tp_spare4   <= ric_i;
    tp_spare5   <= endtrans_i;
    l0_i        <= '0';
    fmdd_stat_i <= (others => '0');
  end block combi;

  core : entity work.bc_core
    port map (
      clk            => clk,               -- in  Clock
      sclk           => sclk,              -- in  Slow clock
      rstb           => rstb,              -- in  Async reset
      cstb           => cstb_i,            -- in  Control strobe
      write          => write_i,           -- in  Write flag
      dstb           => dstb_i,            -- in  Data strobe
      trsf           => trsf_i,            -- in  Data transfer
      l1_trg         => l1_i,              -- in  L1 trigger
      l2_trg         => l2_i,              -- in  L2 trigger
      ackn           => ackn_i,            -- in  Acknowledge
      hadd           => hadd,              -- in  Card address
      din            => din_i,             -- in  ALTRO bus
      scl            => scl,               -- in  I2C clock
      sda_in         => sda_in,            -- in  I2C serial data in
      sda_out        => sda_out_i,         -- out I2C serial data out
      paps_error     => paps_error_i,      -- in  Pasa power supp. error
      alps_error     => alps_error_i,      -- in  ALTRO power supp. error
      al_err         => al_error_i,        -- in  ALTRO error
      debug_sw       => debug_sw,          -- in  Debug switch
      bc_ackn_en     => bc_ackn_en_i,      -- out Acknowledge enable
      bc_ackn        => bc_ackn_i,         -- out Acknowledge
      lastst_al      => lastst_al_i,       -- out Last state bus interface
      endtrans       => endtrans_i,        -- out End of transmission
      bc_dolo_en     => bc_dolo_en_i,      -- out GTL out enable
      bc_cs          => bc_cs_i,           -- out BC card select
      al_cs          => al_cs_i,           -- out ALTRO card select
      wr_al          => wr_al_i,           -- out Write ALTRO
      dout           => dout_i,            -- out Output data
      bc_int         => bc_int_i,          -- out Interrupt
      bc_error       => bc_error_i,        -- out BC error
      altro_sw       => altro_sw_i,        -- out ALTRO switch
      pasa_sw        => pasa_sw_i,         -- out PASA switch
      al_rst         => al_rst_i,          -- out ALTRO reset
      rdoclk_en      => rdoclk_en,         -- out Readout clock enable
      adcclk_en      => adcclk_en_i,       -- out ADC clock enable
      adc_add0       => adc_add0,          -- out ADC address 0
      adc_add1       => adc_add1,          -- out ADC address 1
      tsm_on         => tsm_on_i,          -- out Test mode on
      card_isolation => card_isolation_i,  -- out Card isolated
      test_a         => test_a,            -- out Test word A
      test_b         => test_b,            -- out Test word B
      test_c         => test_c,            -- out Test word C
      test_d         => test_d,            -- out Test word D
      test_e         => test_e,            -- out Test word E
      test_f         => test_f,            -- out Test word F
      test_g         => test_g,            -- out Test word G
      test_h         => test_h,            -- out Test word H
      mscl           => mscl,              -- out Monitor clock
      msda           => msda,              -- inout Monitor serial data out
      rmtrsf_en      => rmtrsf_en_i,       -- out R/M transfer enable
      rmtrsf         => rmtrsf_i,          -- out R/M transfer
      rmdstb         => rmdstb_i,          -- out R/M data strobe
      rmdata_out     => rmdata_out_i,      -- out R/M data
      mem_wren       => mem_wren_i,        -- out TSM: memory write
      sclk_edge      => sclk_edge_i,       -- out Slow clock edge
      evl_cstb       => evl_cstb_i,        -- out EVL: control strobe
      alevl_rdtrx    => alevl_rdtrx_i,     -- out EVL: read/write
      bc_master      => bc_master_i,       -- out EVL: BC is master
      evl_addr       => evl_addr_i,        -- out EVL : addr
      fmdd_stat      => fmdd_stat_i,       -- in  FMDD status
      l0             => l0_i,              -- in  L0 trigger (for counter)
      meb_cnt        => "0001");


  mask : entity work.altro_sw_mask_in
    port map (
      clk         => clk,               -- in  Clock
      rstb        => rstb,              -- in  Async reset
      altro_sw    => altro_sw_i,        -- in  ALTRO switch
      alps_errorb => alps_errorb,       -- in  ALTRO power supply error from HW
      l1b         => l1b,               -- in  Level 1 (-)
      l2b         => l2b,               -- in  Level 2 (-)
      cstbb       => cstbb,             -- in  Control strobe (-)
      writeb      => writeb,            -- in  Write (-)
      al_dolo_enb => al_dolo_enb,       -- in  ALTRO GTL enable (-)
      al_trsf_enb => al_trsf_enb,       -- in  ALTRO Transfer enable (-)
      trsfb       => trsfb,             -- in  Transfer
      dstbb       => dstbb,             -- in  Data strobe (-)
      or_rstb     => or_rst_i,          -- in  Sync or Async reset (-)
      acknb       => acknb,             -- in  Acknowledge (-)
      hadd        => hadd,              -- in  Card address
      pasa_sw     => pasa_sw_i,         -- in  PASA switch
      paps_errorb => paps_errorb,       -- in  PASA power sup. error (-)
      al_errorb   => al_errorb_i,       -- in  ALTRO error (-)
      alps_error  => alps_error_i,      -- out ALTRO power sup. error (+)
      l1          => l1_i,              -- out Level 1 (+)
      l2          => l2_i,              -- out Level 2 (+)
      cstb        => cstb_i,            -- out Control strobe (+)
      write       => write_i,           -- out Write (+)
      al_dolo_en  => al_dolo_en_i,      -- out ALTRO data transfer enable (+)
      al_trsf_en  => al_trsf_en_i,      -- out ALTRO transfer enable (+)
      trsf        => trsf_i,            -- out Transfer
      dstb        => dstb_i,            -- out Data strobe
      ackn        => ackn_i,            -- out Acknowledge (+)
      ackn_chrdo  => ackn_chrdo_i,      -- out Acknowledge during readout (+)
      rst_fbc     => rst_fbc,           -- out Front-end reset
      bcout_add   => bcout_ad,          -- out Output hardware address
      paps_error  => paps_error_i,      -- out PASA power supl. error (+)
      al_error    => al_error_i);       -- out ALTRO error (+)


  driv : entity work.drivers
    port map (
      clk            => clk,            -- in  Clock
      rstb           => rstb,           -- in  Async reset
      tsm_on         => tsm_on_i,       -- in  TSM: on
      al_cs          => al_cs_i,        -- in  ALTRO card select
      bc_cs          => bc_cs_i,        -- in  BC card select
      cstb           => cstb_i,         -- in  Bus: Control strobe
      wr_al          => wr_al_i,        -- in  Bus: Write
      al_dolo_en     => al_dolo_en_i,   -- in  Bus: ALTRO data enable
      bc_dolo_en     => bc_dolo_en_i,   -- in  Bus: BC data enable
      al_trsf_en     => al_trsf_en_i,   -- in  Bus: ALTRO transfer enable
      bc_master      => bc_master_i,    -- in  Bus: BC is local master
      card_isolation => card_isolation_i,  -- in  Bus: Card is isolated
      bc_ackn        => bc_ackn_i,      -- in  Bus: Acknowledge from BC
      bc_ack_en      => bc_ackn_en_i,   -- in  Bus: Acknowledge from BC enable
      bc_error       => bc_error_i,     -- in  Bus: Error
      dout           => dout_i,         -- in  Bus: data out
      alevl_rdtrx    => alevl_rdtrx_i,  -- in  EVL: Transmit
      evl_cstb       => evl_cstb_i,     -- in  EVL: Control strobe
      evl_addr       => evl_addr_i,     -- in  EVL: address out
      rmtrsf_en      => rmtrsf_en_i,    -- in  TSM/EVL: Transfer enable
      ackn_chrdo     => ackn_chrdo_i,   -- in  TSM/EVL: Acknowledge
      rmtrsf         => rmtrsf_i,       -- in  TSM/EVL: transfer
      rmdstb         => rmdstb_i,       -- in  TSM/EVL: Data strob
      rmdata_out     => rmdata_out_i,   -- in  TSM/EVL: data out
      altro_sw       => altro_sw_i,     -- in  HW: ALTRO switch
      sda_out        => sda_out_i,      -- in  I2C: serial data out
      oeba_l         => oeba_l,         -- out GTL: in (low bits)
      oeab_l         => oeab_l,         -- out GTL: out (low bits)
      oeba_h         => oeba_h,         -- out GTL: in (high bits)
      oeab_h         => oeab_h,         -- out GTL: out (high bits)
      read_data      => read_data_i,    -- out Read data 
      ric            => ric_i,          -- out ???
      ctr_in         => ctr_in,         -- out Control in
      ctr_out        => ctr_out,        -- out Control out
      al_errorb      => al_errorb_i,    -- out ALTRO error
      din            => din_i,          -- out Bus: Data input
      bd             => bd,             -- inout Bus: tri-state 
      acknb          => acknb,          -- inout Ctrl: Acknowledge tri-state
      errorb         => errorb,         -- inout Ctrl: Control strobe tri-state
      trsfb          => trsfb,          -- inout Ctrl: Transfer tri-state
      dstbb          => dstbb,          -- inout Ctrl: Data strobe tri-state
      writeb         => writeb,         -- inout Ctrl: Write tri-state
      cstbb          => cstbb);         -- inout Ctrl: Control strobe 
end architecture rtl;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package bc_only_pack is
  component bc_only
    port (
      clk         : in    std_logic;    -- Clock
      rstb        : in    std_logic;    -- Async reset
      debug_sw    : in    std_logic;    -- Debug switch (should be high)
      hadd        : in    std_logic_vector(4 downto 0);  -- Card address
      sclk        : in    std_logic;    -- Sample clock (10 MHz)
      scl         : in    std_logic;    -- I2C: Serial clock (<= 5MHz)
      sda_in      : in    std_logic;    -- I2C: data input
      otib        : in    std_logic;    -- Not used
      alps_errorb : in    std_logic;    -- ALTRO power supply error
      l1b         : in    std_logic;    -- Level 1 trigger
      l2b         : in    std_logic;    -- Level 2 trigger
      al_dolo_enb : in    std_logic;    -- ALTRO data out enable
      al_trsf_enb : in    std_logic;    -- ALTRO transfer enable
      paps_errorb : in    std_logic;    -- PASA power supply error
      mscl        : out   std_logic;    -- ADC: clock
      msda        : inout std_logic;    -- ADC: data in/out
      bc_int      : out   std_logic;    -- CTL: Interrupt
      altro_sw    : out   std_logic;    -- ALTRO switch
      pasa_sw     : out   std_logic;    -- PASA switch
      rdoclk_en   : out   std_logic;    -- TSM: Readout clock enable
      adcclk_en   : out   std_logic;    -- TSM: ADC clock enable
      adc_add0    : out   std_logic;    -- ADC address bit 0
      adc_add1    : out   std_logic;    -- ADC address bit 1
      test_a      : out   std_logic;    -- TSM: mask a
      test_b      : out   std_logic;    -- TSM: mask b
      test_c      : out   std_logic;    -- TSM: mask c
      test_d      : out   std_logic;    -- TSM: mask d
      test_e      : out   std_logic;    -- TSM: mask e
      test_f      : out   std_logic;    -- TSM: mask f
      test_g      : out   std_logic;    -- TSM: mask g
      test_h      : out   std_logic;    -- TSM: mask h
      sda_out     : out   std_logic;    -- I2C: data out
      rst_fbc     : out   std_logic;    -- Reset front-end card
      bcout_ad    : out   std_logic_vector(4 downto 0);  -- Out board address
      oeba_l      : out   std_logic;    -- GTL: enable in low bits
      oeab_l      : out   std_logic;    -- GTL: enable out low bits
      oeba_h      : out   std_logic;    -- GTL: enable in high bits
      oeab_h      : out   std_logic;    -- GTL: enable out high bits
      ctr_in      : out   std_logic;    -- GTL: enable ctrl bus in
      ctr_out     : out   std_logic;    -- GTL: enable ctrl bus out
      bd          : inout std_logic_vector(39 downto 0);  -- Bus: 40 bits
      acknb       : inout std_logic;    -- CTL: Acknowledge
      errorb      : inout std_logic;    -- CTL: Error
      trsfb       : inout std_logic;    -- CTL: Transfer
      dstbb       : inout std_logic;    -- CTL: Data strobe
      writeb      : inout std_logic;    -- CTL: Write 
      cstbb       : inout std_logic;    -- Control strobe
      mcst        : out   std_logic;    -- ADC : Montor start disabled='0';
      tp_spare0   : out   std_logic;    -- Test pin 0
      tp_spare2   : out   std_logic;    -- Test pin 1
      tp_spare3   : out   std_logic;    -- Test pin 3
      tp_spare4   : out   std_logic;    -- Test pin 4
      tp_spare5   : out   std_logic);   -- Test pin 5
  end component bc_only;
end package bc_only_pack;
------------------------------------------------------------------------------
--
-- EOF
--
