-------------------------------------------------------------------------------
-- Title      : va1 Simulator
-- Project    : 
-------------------------------------------------------------------------------
-- File       : va1.vhd
-- Author     : Daniel Georg Kirschner  <Daniel.G.Kirschner@physik.uni-giessen.de>
-- Company    : 
-- Created    : 2005-04-19
-- Last update: 2006/09/30
-- Platform   : 
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: digital part only, analog interface definition is provided
-------------------------------------------------------------------------------
-- Copyright (c) 2005 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2005-04-19  1.0      dk      Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

----------------------------------------------------------------------
entity va1 is
  generic (
    channels             :     positive  := 128;  -- number of channels
    gndV                 :     real      := 0.0;  -- analog ground (V)
    dvddV                :     real      := 1.3;  -- digital vdd (V)    
    dvssV                :     real      := -2.0;  -- digital vss (V)
    avssV                :     real      := -2.0;  -- analog vss (V)
    pre_biasA            :     real      := 500.0e-6;  -- pre amps prebias (A)
    sha_biasA            :     real      := 22.0e-6;  -- shaper shabias (A)
    ibufA                :     real      := 140.0e-6;  -- output buffer bias
    vfsV                 :     real      := 0.700;  -- Control Voltage
    calMIP               :     real      := 1.0;  -- calibration 1MIP
    avddV                :     real      := 1.3;  -- alalog vdd (V)
    dreset_stablity      :     time      := 100 ns;
    ckb_shiftb_stability :     time      := 50 ns
    );
  port (
    gnd                  : in  real      := gndV;  -- analog ground (V)
    dvdd                 : in  real      := dvddV;  -- digital vdd (V)
    dvss                 : in  real      := dvssV;  -- digital vss (V)
    delay_adjust         : in  real      := gndV;  -- not in use
    delay_on             : in  real      := dvssV;  -- not in use
    dummy_hold           : in  std_logic := '0';  -- not in use
    dreset               : in  std_logic;  -- digital reset
    dummy_dreset         : in  std_logic := '0';  -- not in use
    shift_in_b           : in  std_logic := '1';  -- shift in
    dummy_ck             : in  std_logic := '0';  -- not in use
    ckb                  : in  std_logic;  -- readout clock
    shift_out_b          : out std_logic := '1';  -- shift out
    test_on              : in  std_logic := '0';  -- test mode activate
    avss                 : in  real      := avssV;
    pre_bias             : in  real      := pre_biasA;
    sha_bias             : in  real      := sha_biasA;
    vref                 : in  real;    -- not in use
    ibuf                 : in  real      := ibufA;
    outm, outp           : out real;    -- amplifiers outm is invert.
    vfs                  : in  real      := vfsV;
    cal                  : in  real;
    avdd                 : in  real      := avddV;
    pad                  : in  std_logic_vector(channels-1 downto 0)
    );

end va1;


----------------------------------------------------------------------
architecture digital_behave of va1 is
  type analog_channels_t is array (channels downto 0) of real;
  signal analog_channels : analog_channels_t := (others => 1.0);

  type fsm_t is (idle, reset, start, counting, fail, wait_fini_overlap);
  signal fsm : fsm_t := idle;

  signal out_mux_reg  : std_logic_vector(channels downto 0) := (others => '1');
  signal in_demux_reg : std_logic_vector(channels downto 0) := (others => '1');

  signal demux_shift_b_i : std_logic := '1';
  signal demux_ckb_i     : std_logic := '1';
  signal omux_select     : integer   := 0;
  signal idemux_select   : integer   := 0;

  signal dreset_stable : boolean := true;
  signal i             : integer := 0;

begin  -- digital_behave

  -- Which mux entry to use 
  out_mux : process (out_mux_reg, in_demux_reg)
  begin  -- process out_mux
    for i in channels downto 1 loop
      if out_mux_reg(i) = '0' then
        omux_select <= i;
      end if;

      if in_demux_reg(i) = '0' then
        idemux_select <= i;
      end if;
    end loop;  -- i
  end process out_mux;

  -- Write to output 
  out_mux_2 : process(analog_channels, omux_select)
  begin  -- process M
    if omux_select = 0 then
      outm <= 0.0;
      outp <= 0.0;
    else
      outp <= analog_channels(OMUX_select);
      outm <= analog_channels(omux_select) * (-1.0); --
    end if;
  end process out_mux_2;

  shift_out_b <= out_mux_reg(channels);


  -- test mode enable logic
  demux_shift_b_i <= test_on and shift_in_b;
  demux_ckb_i     <= test_on and ckb;


  -- input demux (test mode)
  demux : process (idemux_select, cal)
  begin  -- process demux
    analog_channels(0)             <= 0.0;
    analog_channels(idemux_select) <= cal;
  end process demux;

  -- purpose: shift through output registers
  -- type   : sequential
  -- inputs : ckb, dreset
  -- outputs: 
  out_mux_shifter : process (ckb, dreset)
  begin  -- process OUT_MUX
    if dreset = '0' then                -- asynchronous reset (active low)
      out_mux_reg(channels downto 1) <= (others => '1');
    elsif ckb'event and ckb = '0' then  -- rising clock edge
      out_mux_reg(channels downto 1) <=
        out_mux_reg(channels-1 downto 1) & shift_in_b; --
    end if;
  end process out_mux_shifter;


  -- purpose: shift through input registers
  -- type   : sequential
  -- inputs : demux_ckb_i, dreset
  -- outputs: 
  in_demux_shifter : process (demux_ckb_i, dreset)
  begin  -- process IN_MUX_shifter
    if dreset = '0' then                -- asynchronous reset (active low)
      in_demux_reg(channels downto 1) <= (others => '1');
    elsif demux_ckb_i'event and demux_ckb_i = '0' then  -- rising clock edge
      in_demux_reg(channels downto 1) <=
        in_demux_reg(channels-1 downto 1) & demux_shift_b_i;
    end if;
  end process IN_DEMUX_shifter;


  dreset_stable <= dreset'stable(dreset_stablity);
  dreset_check : process

    -- Check dreset
  begin  -- process dreset-check
    wait on dreset;
    wait on dreset;
    -- assert dreset_stable
    --   report "VA 'dreset' stability violation" severity error;
  end process dreset_check;


  -- Check sequence 
  ckb_shift_sequence_check : process (ckb, shift_in_b, dreset)
    variable counts        : integer := 0;
    variable start_time    : time    := 0 ns;
    variable stop_time     : time    := 0 ns;
    variable delta_time    : time    := 0 ns;
  begin  -- process ckb_shift_in_b_overloap_check
    if dreset = '1' then
      fsm       <= idle;
      i         <= 0;
    else
      case fsm is
        when idle =>
          if shift_in_b = '0' and shift_in_b'event then
            start_time               := NOW;
          end if;
          if ckb = '0' then
            fsm <= start;
          end if;

        when start    =>
          assert shift_in_b = '0'
            report "VA : shift_in_b should be low on first ckb falling edge!"
            severity warning;
          if ckb = '1' then
            fsm <= wait_fini_overlap;
            i   <= 1;
          end if;
        when counting =>
          assert shift_in_b = '1'
            report "VA : shift_in_b low on more than one bit!"
            severity error;
          if ckb = '0' and ckb'event then
            i   <= i+1;
          end if;

          if i = channels then
            fsm <= idle;
          end if;
        when wait_fini_overlap =>
          if (ckb = '0' and shift_in_b = '0') then
            assert false
              report "VA : shift_in_b low on two cycles"
              severity error;
          end if;

          if shift_in_b = '1' then
            stop_time  := NOW;
            delta_time := stop_time - start_time;
            assert delta_time >= ckb_shiftb_stability
              report "VA : shift_in_b to ckb overlap timing violation!" &
              time'image(delta_time) severity error;
            fsm <= counting;
          end if;
        when others => null;
      end case;
    end if;
  end process ckb_shift_sequence_check;
end digital_behave;

----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package va1_pack is
  component va1
    generic (
      channels             :     positive;
      gndV                 :     real;
      dvddV                :     real;
      dvssV                :     real;
      avssV                :     real;
      pre_biasA            :     real;
      sha_biasA            :     real;
      ibufA                :     real;
      vfsV                 :     real;
      calMIP               :     real;
      avddV                :     real;
      dreset_stablity      :     time;
      ckb_shiftb_stability :     time);
    port (
      gnd                  : in  real      := gndV;
      dvdd                 : in  real      := dvddV;
      dvss                 : in  real      := dvssV;
      delay_adjust         : in  real      := gndV;
      delay_on             : in  real      := dvssV;
      dummy_hold           : in  std_logic := '0';
      dreset               : in  std_logic;
      dummy_dreset         : in  std_logic := '0';
      shift_in_b           : in  std_logic := '1';
      dummy_ck             : in  std_logic := '0';
      ckb                  : in  std_logic;
      shift_out_b          : out std_logic := '1';
      test_on              : in  std_logic := '0';
      avss                 : in  real      := avssV;
      pre_bias             : in  real      := pre_biasA;
      sha_bias             : in  real      := sha_biasA;
      vref                 : in  real;
      ibuf                 : in  real      := ibufA;
      outm, outp           : out real;
      vfs                  : in  real      := vfsV;
      cal                  : in  real;
      avdd                 : in  real      := avddV;
      pad                  : in  std_logic_vector(channels-1 downto 0));
  end component;
end va1_pack;

----------------------------------------------------------------------
--
-- EOF
--
