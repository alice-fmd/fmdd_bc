----------------------------------------------------------------------
--
-- Test bench for VA1_3 simulation
--
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

----------------------------------------------------------------------
entity va1_tb is
end va1_tb;

----------------------------------------------------------------------
architecture stim of va1_tb is

  constant SIM_TIME : time := 40000 ns;

  component va1
    generic (
      channels     :     positive;
      gndV         :     real;
      dvddV        :     real;
      dvssV        :     real;
      avssV        :     real;
      pre_biasA    :     real;
      sha_biasA    :     real;
      ibufA        :     real;
      vfsV         :     real;
      calMIP       :     real;
      avddV        :     real);
    port (
      gnd          : in  real      := gndV;
      dvdd         : in  real      := dvddV;
      dvss         : in  real      := dvssV;
      delay_adjust : in  real      := gndV;
      delay_on     : in  real      := dvssV;
      dummy_hold   : in  std_logic := '0';
      dreset       : in  std_logic;
      dummy_dreset : in  std_logic := '0';
      shift_in_b   : in  std_logic := '1';
      dummy_ck     : in  std_logic := '0';
      ckb          : in  std_logic;
      shift_out_b  : out std_logic := '1';
      test_on      : in  std_logic := '0';
      avss         : in  real      := avssV;
      pre_bias     : in  real      := pre_biasA;
      sha_bias     : in  real      := sha_biasA;
      vref         : in  real;
      ibuf         : in  real      := ibufA;
      outm, outp   : out real;
      vfs          : in  real      := vfsV;
      cal          : in  real;
      avdd         : in  real      := avddV;
      pad          : in  std_logic_vector(channels-2 downto 0));
  end component;

  constant channels       : positive  := 128;
  constant gndV           : real      := 0.0;
  constant dvddV          : real      := 1.3;
  constant dvssV          : real      := -2.0;
  constant avssV          : real      := -2.0;
  constant pre_biasA      : real      := 500.0e-6;
  constant sha_biasA      : real      := 22.0e-6;
  constant ibufA          : real      := 140.0e-6;
  constant vfsV           : real      := 0.700;
  constant calMIP         : real      := 1.0;
  constant avddV          : real      := 1.3;
  signal   gnd_i          : real      := gndV;
  signal   dvdd_i         : real      := dvddV;
  signal   dvss_i         : real      := dvssV;
  signal   delay_adjust_i : real      := gndV;
  signal   delay_on_i     : real      := dvssV;
  signal   dummy_hold_i   : std_logic := '0';
  signal   dreset_i       : std_logic;
  signal   dummy_dreset_i : std_logic := '0';
  signal   shift_in_b_i   : std_logic := '1';
  signal   dummy_ck_i     : std_logic := '0';
  signal   ckb_i          : std_logic := '0';
  signal   shift_out_b_i  : std_logic := '1';
  signal   test_on_i      : std_logic := '0';
  signal   avss_i         : real      := avssV;
  signal   pre_bias_i     : real      := pre_biasA;
  signal   sha_bias_i     : real      := sha_biasA;
  signal   vref_i         : real;
  signal   ibuf_i         : real      := ibufA;
  signal   outm_i, outp_i : real;
  signal   vfs_i          : real      := vfsV;
  signal   cal_i          : real;
  signal   avdd_i         : real      := avddV;
  signal   pad_i          : std_logic_vector(channels-2 downto 0);

begin  -- stim

  m_va1 : va1
    generic map (
      channels     => channels,
      gndV         => gndV,
      dvddV        => dvddV,
      dvssV        => dvssV,
      avssV        => avssV,
      pre_biasA    => pre_biasA,
      sha_biasA    => sha_biasA,
      ibufA        => ibufA,
      vfsV         => vfsV,
      calMIP       => calMIP,
      avddV        => avddV)
    port map (
      gnd          => gnd_i,
      dvdd         => dvdd_i,
      dvss         => dvss_i,
      delay_adjust => delay_adjust_i,
      delay_on     => delay_on_i,
      dummy_hold   => dummy_hold_i,
      dreset       => dreset_i,
      dummy_dreset => dummy_dreset_i,
      shift_in_b   => shift_in_b_i,
      dummy_ck     => dummy_ck_i,
      ckb          => ckb_i,
      shift_out_b  => shift_out_b_i,
      test_on      => test_on_i,
      avss         => avss_i,
      pre_bias     => pre_bias_i,
      sha_bias     => sha_bias_i,
      vref         => vref_i,
      ibuf         => ibuf_i,
      outm         => outm_i,
      outp         => outp_i,
      vfs          => vfs_i,
      cal          => cal_i,
      avdd         => avdd_i,
      pad          => pad_i);


  -- Provide stimuli 
  stim_it : process
  begin  -- process simit
    report "RESET";
    ckb_i    <= '1';
    wait for 10 ns;
    dreset_i <= '1';
    wait for 105 ns;
    dreset_i <= '0';
    wait for 10 ns;
    dreset_i <= '1';
    wait for 10 ns;

    report "CLOCK IT TROUGH";


    shift_in_b_i <= '0';
    wait for 5 ns;
    ckb_i        <= '0';
    wait for 10 ns;
    shift_in_b_i <= '1';

    for i in 0 to 256 loop
      ckb_i <= not ckb_i;
      wait for 20 ns;
    end loop;  -- i

    wait;
  end process stim_it;

  -- clocker: process
  -- begin                            -- process clocker
  --   ckb_i <= not ckb_i;
  --   wait for 10 ns;
  -- end process clocker;

  -- End simulation
  -- end_sim: process (ckb_i)
  -- begin                              -- process end_sim
  --   assert NOW<SIM_TIME report "END OF SIM" severity FAILURE;
  -- end process end_sim;
end stim;
----------------------------------------------------------------------
--
-- EOF
--
