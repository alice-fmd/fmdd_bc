-------------------------------------------------------------------------------
-- Title      : va1 Wrapper
-- Project    : 
-------------------------------------------------------------------------------
-- File       : va1_wrap.vhd
-- Author     : Daniel Georg Kirschner  <Daniel.G.Kirschner@physik.uni-giessen.de>
-- Company    : 
-- Created    : 2005-04-19
-- Last update: 2006/09/30
-- Platform   : 
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: stores a lot of definitions / saves typing
-------------------------------------------------------------------------------
-- Copyright (c) 2005 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2005-04-19  1.0      dk      Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library va1_model;
use va1_model.va1_pack.all;

entity va1_wrap is
  generic (
    channels : positive := 128);

  port (
    dummy_hold   : in  std_logic := '0';
    dreset       : in  std_logic;
    dummy_dreset : in  std_logic := '0';
    shift_in_b   : in  std_logic := '1';
    dummy_ck     : in  std_logic := '0';
    ckb          : in  std_logic;
    shift_out_b  : out std_logic := '1';
    test_on      : in  std_logic := '0';
    pad          : in  std_logic_vector(channels-1 downto 0));

end entity va1_wrap;

architecture wrap of va1_wrap is

  --constant channels             : positive := 128;
  constant gndV                 : real := 0.0;
  constant dvddV                : real := 1.3;
  constant dvssV                : real := -2.0;
  constant avssV                : real := -2.0;
  constant pre_biasA            : real := 500.0e-6;
  constant sha_biasA            : real := 22.0e-6;
  constant ibufA                : real := 140.0e-6;
  constant vfsV                 : real := 0.700;
  constant calMIP               : real := 1.0;
  constant avddV                : real := 1.3;
  constant dreset_stablity      : time := 100 ns;
  constant ckb_shiftb_stability : time := 50 ns;

  signal gnd          : real := gndV;
  signal dvdd         : real := dvddV;
  signal dvss         : real := dvssV;
  signal delay_adjust : real := gndV;
  signal delay_on     : real := dvssV;
  signal avss         : real := avssV;
  signal pre_bias     : real := pre_biasA;
  signal sha_bias     : real := sha_biasA;
  signal vref         : real;
  signal ibuf         : real := ibufA;
  signal outm, outp   : real;
  signal vfs          : real := vfsV;
  signal cal          : real;
  signal avdd         : real := avddV;
  

begin  -- architecture RTL

  va1_inst : va1
    generic map (
      channels             => channels,
      gndV                 => gndV,
      dvddV                => dvddV,
      dvssV                => dvssV,
      avssV                => avssV,
      pre_biasA            => pre_biasA,
      sha_biasA            => sha_biasA,
      ibufA                => ibufA,
      vfsV                 => vfsV,
      calMIP               => calMIP,
      avddV                => avddV,
      dreset_stablity      => dreset_stablity,
      ckb_shiftb_stability => ckb_shiftb_stability)
    port map (
      gnd          => gnd,
      dvdd         => dvdd,
      dvss         => dvss,
      delay_adjust => delay_adjust,
      delay_on     => delay_on,
      dummy_hold   => dummy_hold,
      dreset       => dreset,
      dummy_dreset => dummy_dreset,
      shift_in_b   => shift_in_b,
      dummy_ck     => dummy_ck,
      ckb          => ckb,
      shift_out_b  => shift_out_b,
      test_on      => test_on,
      avss         => avss,
      pre_bias     => pre_bias,
      sha_bias     => sha_bias,
      vref         => vref,
      ibuf         => ibuf,
      outm         => outm,
      outp         => outp,
      vfs          => vfs,
      cal          => cal,
      avdd         => avdd,
      pad          => pad);

end architecture wrap;

------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package va1_wrap_pack is
  component va1_wrap is
    generic (
      channels : positive);
    port (
      dummy_hold   : in  std_logic := '0';
      dreset       : in  std_logic;
      dummy_dreset : in  std_logic := '0';
      shift_in_b   : in  std_logic := '1';
      dummy_ck     : in  std_logic := '0';
      ckb          : in  std_logic;
      shift_out_b  : out std_logic := '1';
      test_on      : in  std_logic := '0';
      pad          : in  std_logic_vector(channels-1 downto 0));
  end component va1_wrap;  
end package va1_wrap_pack;

------------------------------------------------------------------------------
-- 
-- EOF
--
