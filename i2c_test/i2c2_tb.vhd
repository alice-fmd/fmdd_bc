library ieee;
use ieee.std_logic_1164.all;
library msmodule2_lib;
use msmodule2_lib.msm2_interface_i2c_pack.all;
library rcu_model;
use rcu_model.rcu_misc_pack.all;
library work;
use work.slave_pack.all;

entity i2c2_tb is

end i2c2_tb;

architecture test of i2c2_tb is
  constant PERIOD        : time := 1 sec / 40000000;  -- 25ns clock period
  constant SCL_DELAY     : time := 36 ns;  -- Delay in cables (fails >75)
  constant SDA_IN_DELAY  : time := 38 ns;  -- Delay in cables (fails >75)
  constant SDA_OUT_DELAY : time := 28 ns;  -- Delay in cables (fails >75)
  constant RCLK_DELAY    : time := 22.5 ns;  -- Delay in cables (fails >75)

  signal clk  : std_logic := '0';
  signal clkd : std_logic := '0';
  -- signal clkm : std_logic;
  signal rstb : std_logic;              -- Async reset

  signal scl      : std_logic;                     -- Serial clock
  signal sda_fbc  : std_logic_vector(1 downto 0);  -- serial data from BCs
  -- signal sda_ack  : std_logic;
  signal sda_frcu : std_logic;                     -- Serial data from RCU
  signal slctr    : std_logic_vector(1 downto 0);  -- Serial control from BCs
  signal sda_trcu : std_logic;                     -- Serial data to RCU
  signal sda_fbcg : std_logic_vector(1 downto 0);

  signal scld      : std_logic;         -- Delayed serial clock
  signal sda_frcud : std_logic;         -- Delayed serial data from RCU
  signal sda_trcud : std_logic;         -- Delayed serial data to RCU

  type   tx_data is array (1 downto 0) of std_logic_vector(15 downto 0);
  signal data_tx : tx_data := (0 => X"dead", 1 => X"beef");

  signal fec_al : std_logic_vector(31 downto 0) := X"00030003";
  signal rdol   : std_logic_vector(31 downto 0) := X"00030003";

  signal data4bc : std_logic_vector(15 downto 0) := X"aaaa";

  signal error : std_logic;

  signal exec       : std_logic                    := '0';
  signal rnw        : std_logic                    := '0';
  signal bcast      : std_logic                    := '0';
  signal branch     : std_logic                    := '0';
  signal fec_add    : std_logic_vector(3 downto 0) := X"0";
  signal reg_add    : std_logic_vector(7 downto 0) := X"00";
  signal seq_active : std_logic;
  signal rst        : std_logic                    := '1';

  -- signal err_fec_ack : std_logic;
  -- signal err_bc_add : std_logic;
  -- signal err_din1_ack : std_logic;
  -- signal err_din2_ack  : std_logic;
  signal err_reg : std_logic_vector(3 downto 0);

  signal B_in   : std_logic_vector(15 downto 0) := X"dead";
  signal result : std_logic_vector(15 downto 0) := (others => 'Z');

  signal i : integer;
  signal j : integer;

  -- purpose: Read one register via I2C from BC
  procedure read_one (
    signal   clk     : in  std_logic;   -- Clock
    signal   busy    : in  std_logic;
    constant fec     : in  integer range 0 to 15;            -- FEC number
    constant reg     : in  integer range 0 to 255;           -- Register
    signal   exec    : out std_logic;   -- Execute it
    signal   rnw     : out std_logic;   -- Read-not-write
    signal   bcast   : out std_logic;   -- Broadcast
    signal   branch  : out std_logic;   -- '1' for branch B, else branch 'A'
    signal   fec_add : out std_logic_vector(3 downto 0);     -- FEC #
    signal   reg_add : out std_logic_vector(7 downto 0)) is  -- Register #
  begin  -- read_one
    if busy = '1' then
      wait until busy = '0';
    end if;

    bcast   <= '0';
    rnw     <= '1';
    branch  <= '0';
    fec_add <= int2slv(fec, 4);
    reg_add <= int2slv(reg, 8);

    wait for 2 * PERIOD;
    wait until rising_edge(clk);
    exec <= '1';
    wait for 4 * PERIOD;
    exec <= '0';

    if busy = '1' then
      wait until busy = '0';
    end if;
  end read_one;
begin  -- test
  sda_fbcg(0) <= sda_fbc(0) when slctr(0) = '1' else 'Z';
  sda_fbcg(1) <= sda_fbc(1) when slctr(1) = '1' else 'Z';
  sda_trcu <= '1' when (sda_fbcg(0) /= '0' and
                        sda_fbcg(1) /= '0') else '0';
  scld      <= scl      after SCL_DELAY;
  sda_frcud <= sda_frcu after SDA_IN_DELAY;
  sda_trcud <= sda_trcu after SDA_OUT_DELAY;
  -- clkd      <= clk      after RCLK_DELAY;
  rst       <= not rstb;
  error     <= '1' when err_reg /= "0000" else '0';

  clocker_1 : clocker
    generic map (
      PERIOD => PERIOD)
    port map (
      clk => clk);                      -- [out]

  clocker_2 : clocker
    generic map (
      PERIOD => PERIOD,
      DELAY => RCLK_DELAY)
    port map (
      clk => clkd);                      -- [out]

  reseter_1 : reseter
    generic map (
      DUR   => 100 ns,
      DELAY => 0 ns)
    port map (
      rstb => rstb);                    -- [out]

  msm2_interface_I2C_1 : msm2_interface_I2C
    port map (
      rst          => rst,                  -- [in]
      clk          => clk,                  -- [in]
      exec         => exec,                 -- [in]
      rd_nwr       => rnw,                  -- [in]
      ld_regs      => '0',                  -- [in]
      fec_add      => fec_add,              -- [in]
      bc_reg_add   => reg_add,              -- [in]
      B1_din       => B_in(7 downto 0),     -- [in]
      B2_din       => B_in(15 downto 8),    -- [in]
      rst_rslt     => '0',                  -- [in]
      sv_rslt      => open,                 -- [out]
      sda_in_B1    => result(7 downto 0),   -- [out]
      sda_in_B2    => result(15 downto 8),  -- [out]
      end_rd       => open,                 -- [out]
      I2C_BSY      => seq_active,           -- [out]
      err_fec_ack  => err_reg(0),           -- [out]
      err_bc_add   => err_reg(1),           -- [out]
      err_din1_ack => err_reg(2),           -- [out]
      err_din2_ack => err_reg(3),           -- [out]
      fsm_state    => open,                 -- [out]
      bcast        => bcast,                -- [in]
      sda_in       => sda_trcud,            -- [in]
      sda_out      => sda_frcu,             -- [out]
      s_clk        => scl);                 -- [out]


  slave_gen : for i in 0 to 1 generate
    slave_1 : slave
      port map (
        clk      => clkd,               -- [in]
        rstb     => rstb,               -- [in]
        scl      => scld,               -- [in]
        sda_in   => sda_frcud,          -- [in]
        hadd     => int2slv(i, 5),      -- [in]
        data_tx  => data_tx(i),         -- [in]
        bc_rst   => '0',
        csr1_clr => '0',                -- [in]
        sda_out  => sda_fbc(i),         -- [out]
        reg_add  => open,               -- [out]
        data_rx  => open,               -- [out]
        slctr    => slctr(i),           -- [out]
        we       => open,               -- [out]
        ierr_sc  => open,               -- [out]
        state    => open);              -- [out]
  end generate slave_gen;

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stim : process
  begin  -- process stim
    report "Doing a reset" severity note;
    wait until rstb = '1';
    report "After reset" severity note;

    spacer(rclk => clk); report "After spacer" severity note;

    for i in 0 to 1 loop
      for j in 0 to 2 loop
        report "Reading from FEC " & integer'image(i) &
          " register " & integer'image(j) severity note;
        read_one(clk     => clk,
                 busy    => seq_active,
                 fec     => i,
                 reg     => j,
                 exec    => exec,
                 rnw     => rnw,
                 bcast   => bcast,
                 branch  => branch,
                 fec_add => fec_add,
                 reg_add => reg_add);

        wait for 1 us;
        result <= (others => 'Z');
      end loop;  -- j
    end loop;  -- i

  end process stim;

end test;

-------------------------------------------------------------------------------
library work;
use work.fec_address_pack.all;
use work.sel_signals_pack.all;

configuration i2c2_tb_minimal of i2c2_tb is
  for test
    for slave_gen
      for all : slave
        use entity work.slave(rtl);
        for rtl
          for all : fec_address
            use entity work.fec_address(rtl_minimal);
          end for;
          for all : sel_signals
            use entity work.sel_signals(rtl_async);
          end for;
        end for;
      end for;
    end for;
  end for;

end i2c2_tb_minimal;
-------------------------------------------------------------------------------
--
-- EOF
-------------------------------------------------------------------------------
