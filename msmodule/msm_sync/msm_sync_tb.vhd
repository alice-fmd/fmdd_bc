-------------------- Test bench of RCU MS syncronisation ----------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_sync_pack.all;

entity msm_sync_tb is
end msm_sync_tb;

architecture test of msm_sync_tb is

  -- component ports
  signal clk    : std_logic := '0';     -- [in]  Clock
  signal rstb   : std_logic := '0';     -- [in]  Async reset
  signal input  : std_logic := '0';     -- [in]  Input
  signal output : std_logic;            -- [out] Output

  -- Clock period
    constant PERIOD : time := 25 ns;

begin  -- test

  -- component instantiation
  dut: msm_sync
    port map (
      clk    => clk,                    -- [in]  Clock
      rstb   => rstb,                   -- [in]  Async reset
      input  => input,                  -- [in]  Input
      output => output);                -- [out] Output

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';

    wait for 4 * PERIOD;
    input <= '1';
    wait for PERIOD;
    input <= '0';

    wait for 4 * PERIOD;
    input <= '1';
    wait for PERIOD;
    input <= '0';

    wait for 4 * PERIOD;
    input <= '1';
    wait for 2 * PERIOD;
    input <= '0';

    wait for 4 * PERIOD;
    input <= '1';
    wait for 3 * PERIOD;
    input <= '0';

    wait for 4 * PERIOD;
    input <= '1';
    wait for 4 * PERIOD;
    input <= '0';
    
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration sync_vhdl of msm_sync_tb is

  for test
    for dut : msm_sync
      use entity msmodule_lib.msm_sync(rtl);
    end for;
  end for;

end sync_vhdl;


-------------------------------------------------------------------------------
--
-- EOF
--
