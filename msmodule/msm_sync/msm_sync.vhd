-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- Sync 12: msm_slow - fast
library ieee;
use ieee.std_logic_1164.all;

entity msm_sync is
  
  port (
    clk    : in  std_logic;             -- Clock
    rstb   : in  std_logic;             -- Async reset
    input  : in  std_logic;             -- Input
    output : out std_logic);            -- Output

end msm_sync;

architecture rtl of msm_sync is

  signal r1 : std_logic;
  signal r2 : std_logic;
  
begin  -- rtl
  output <= '1' when not (r1 = '0' or r2 = '1') else '0'; 
  
  -- purpose: msm_Syncronize
  -- type   : msm_sequential
  -- inputs : msm_clk, rst, input
  -- outputs: msm_r1, r2
  syncronize: process (clk, rstb)
  begin  -- process syncronize
    if rstb = '0' then                   -- asynchronous reset (active low)
      r1 <= '0';
      r2 <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      r1 <= input;
      r2 <= r1;
    end if;
  end process syncronize;
  

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_sync_pack is

  component msm_sync
    port (
      clk    : in  std_logic;           -- Clock
      rstb   : in  std_logic;           -- Async reset
      input  : in  std_logic;           -- Input
      output : out std_logic);          -- Output
  end component;
  
end msm_sync_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
