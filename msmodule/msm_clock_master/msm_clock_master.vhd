-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- This is the clock (frequency = 20 MHz) for the master state machine which
-- provides the scl and sda signals. Every 4 states of that fsm generates a scl
-- cycle (5 MHz) 
library ieee;
use ieee.std_logic_1164.all;

entity msm_clock_master is
  
  port (
    clk40        : in  std_logic;         -- Input clock
    reset       : in  std_logic;         -- Aync reset
    clk_master : out std_logic);        -- Output 'clock' @ 20MHz

end msm_clock_master;

architecture rtl of msm_clock_master is
  signal clk_master_i : std_logic;
begin  -- rtl
  clk_master <= clk_master_i;

  -- purpose: msm_Generate clock
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_clk_master
  fsm : process (clk40, reset)
  begin  -- process fsm
    if reset = '0' then                     -- asynchronous reset (active low)
      clk_master_i <= '0';
    elsif clk40'event and clk40 = '1' then  -- rising clock edge
      clk_master_i <= not clk_master_i;
    end if;
  end process fsm;

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_clock_master_pack is

  component msm_clock_master
    port (
      clk40      : in  std_logic;       -- Input clock
      reset      : in  std_logic;       -- Aync reset
      clk_master : out std_logic);      -- Output 'clock' @ 20MHz
  end component;
  
end msm_clock_master_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
