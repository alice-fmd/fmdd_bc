-------------------- Test bench of RCU MS master clock generator --------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_clock_master_pack.all;

entity msm_clock_master_tb is

end msm_clock_master_tb;

architecture test of msm_clock_master_tb is
  constant PERIOD : time := 25 ns;
  
  -- component ports
  signal clk        : std_logic := '0';  -- [in]  Input clock
  signal rstb       : std_logic := '0';  -- [in]  Aync reset
  signal clk_master : std_logic;         -- [out] Output 'clock' @ 20MHz

begin  -- tes

  -- component instantiation
  dut : msm_clock_master
    port map (
      clk40      => clk,                -- [in]  Input clock
      reset      => rstb,               -- [in]  Aync reset
      clk_master => clk_master);        -- [out] Output 'clock' @ 20MHz

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 100 ns;
    rstb <= '1';
    
    wait; 
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_clock_master_vhdl of msm_clock_master_tb is

  for test
    for dut : msm_clock_master
      use entity msmodule_lib.msm_clock_master(rtl);
    end for;
  end for;

end msm_clock_master_vhdl;


-------------------------------------------------------------------------------
--
-- EOF
--
