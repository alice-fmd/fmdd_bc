-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- D FF with enable
library ieee;
use ieee.std_logic_1164.all;

entity msm_ffd_en is
  
  port (
    clk    : in  std_logic;             -- Clock
    arst   : in  std_logic;             -- Async reset
    srst   : in  std_logic;             -- Sync. reset
    enable : in  std_logic;             -- Enable
    d      : in  std_logic;             -- Input
    q      : out std_logic);            -- Output

end msm_ffd_en;

architecture rtl of msm_ffd_en is

begin  -- rtl

  -- purpose: msm_Register d
  -- type   : msm_sequential
  -- inputs : msm_clk, arst, srst, d
  -- outputs: msm_q
  register_it: process (clk, arst)
  begin  -- process register
    if arst = '0' then                  -- asynchronous reset (active low)
      q <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if srst = '1' then
        q <= '0';
      elsif enable = '1' then
        q <= d;
      end if;
    end if;
  end process register_it;

end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_ffd_en_pack is
  
  component msm_ffd_en
    port (
      clk    : in  std_logic;           -- Clock
      arst   : in  std_logic;           -- Async reset
      srst   : in  std_logic;           -- Sync. reset
      enable : in  std_logic;           -- Enable
      d      : in  std_logic;           -- Input
      q      : out std_logic);          -- Output
  end component;

end msm_ffd_en_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
