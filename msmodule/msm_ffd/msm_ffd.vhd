-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- D Flip-flop
library ieee;
use ieee.std_logic_1164.all;

entity msm_ffd is
  
  port (
    clk    : in  std_logic;             -- Clock
    arst   : in  std_logic;             -- Async reset
    d      : in  std_logic;             -- Input
    q      : out std_logic);            -- Output

end msm_ffd;

architecture rtl of msm_ffd is

begin  -- rtl

  -- purpose: msm_Register d
  -- type   : msm_sequential
  -- inputs : msm_clk, arst, d
  -- outputs: msm_q
  do_register: process (clk, arst)
  begin  -- process register
    if arst = '0' then                  -- asynchronous reset (active low)
      q <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      q <= d;
    end if;
  end process do_register;

end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_ffd_pack is

  component msm_ffd
    port (
      clk  : in  std_logic;             -- Clock
      arst : in  std_logic;             -- Async reset
      d    : in  std_logic;             -- Input
      q    : out std_logic);            -- Output
  end component;

end msm_ffd_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
