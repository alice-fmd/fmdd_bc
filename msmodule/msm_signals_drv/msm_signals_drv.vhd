-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- Driver of I/O signals
library ieee;
use ieee.std_logic_1164.all;

entity msm_signals_drv is
  
  port (
    clk                : in  std_logic;   -- Clock
    rstb               : in  std_logic;   -- Async reset
    add_siu            : in  std_logic_vector(15 downto 0);  -- SIU: msm_Address
    we_siu             : in  std_logic;   -- SIU: msm_Write enable
    datain_siu         : in  std_logic_vector(31 downto 0);  -- SIU : msm_write data
    add_dcs            : in  std_logic_vector(15 downto 0);  -- DCS: msm_Address
    we_dcs             : in  std_logic;   -- DCS: msm_Write enable
    datain_dcs         : in  std_logic_vector(31 downto 0);  -- DCS : msm_write data
    dcsnsiu            : in  std_logic;   -- BUS: msm_DCS (not SIU)
    addsm_fsc          : in  std_logic_vector(4 downto 0);  -- INT: msm_SM address
    datasm_fsc         : in  std_logic_vector(15 downto 0);  -- INT: msm_SM data
    wesm_fsc           : in  std_logic;   -- INT: msm_SM write enable
    dataresult_fsc     : in  std_logic_vector(20 downto 0);  -- INT: msm_result data
    weresult_fsc       : in  std_logic;   -- INT: msm_Write enab. res.
    fec_al_fsc         : in  std_logic_vector(31 downto 0);  -- INT: msm_Act. FECs 
    we_fec_al_fsc      : in  std_logic;   -- INT: msm_Act. FECs write enab.
    rdol_fsc           : in  std_logic_vector(31 downto 0);  -- INT: msm_RO FECs 
    we_rdol_fsc        : in  std_logic;   -- INT: msm_RO FECs write enab.
    result_lsc         : in  std_logic_vector(20 downto 0);  -- INT: msm_from here
    rdol               : in  std_logic_vector(31 downto 0);  -- CORE: msm_RO list
    dosm               : in  std_logic_vector(15 downto 0);  -- INT: msm_data output
    errreg             : in  std_logic_vector(1 downto 0);  -- REG: msm_errors
    inta_not_en        : in  std_logic;   -- REG: interrupts A disabled
    intb_not_en        : in  std_logic;   -- REG: interrupts B disabled
    dataout_rcuversion : in  std_logic_vector(31 downto 0);  -- CORE: msm_FW version
    addsm              : out std_logic_vector(4 downto 0);  -- INT: msm_SM address
    datasm             : out std_logic_vector(15 downto 0);  -- INT: msm_SM data
    wesm               : out std_logic;   -- INT: msm_SM write enab.
    dataresult         : out std_logic_vector(20 downto 0);  -- INT: msm_Result data
    weresult           : out std_logic;   -- INT: msm_Result write enab.
    data_fec_al        : out std_logic_vector(31 downto 0);  -- CORE: msm_Valid FECs
    we_fec_al          : out std_logic;   -- CORE: msm_FEC write enab.
    data_rdol          : out std_logic_vector(31 downto 0);  -- CORE: msm_Valid FECs
    we_rdol            : out std_logic;   -- CORE: msm_FEC write enab.
    inta_enable        : out std_logic;   -- INT: msm_Interrupts on A enable
    intb_enable        : out std_logic;   -- INT: msm_Interrupts on B enable
    rst_errreg         : out std_logic;   -- INT: msm_reset errors
    rst_resultreg      : out std_logic;   -- INT: msm_reset result
    rst_sclksync       : out std_logic;   -- INT: msm_reset sync of I2C
    dataout_msm        : out std_logic_vector(31 downto 0);  -- CORE: out data
    scadd              : out std_logic_vector(15 downto 0);  -- CORE: msm_I2C addr
    scdata             : out std_logic_vector(15 downto 0);  -- CORE: msm_I2C data
    scexec             : out std_logic);  -- CORE : msm_I2C execute

end msm_signals_drv;

architecture rtl of msm_signals_drv is

  signal add_siu_i : std_logic_vector(15 downto 0);

  signal sel_siu_sm_i           : boolean;
  signal sel_siu_result_i       : boolean;
  signal sel_siu_errreg_i       : boolean;
  signal sel_siu_intmode_i      : boolean;
  signal sel_siu_scadd_i        : boolean;
  signal sel_siu_scdata_i       : boolean;
  signal sel_siu_rcu_version_i  : boolean;
  signal sel_siu_scexec_i       : boolean;
  signal sel_siu_rst_errreg_i   : boolean;
  signal sel_siu_rst_result_i   : boolean;
  signal sel_siu_rst_sclksync_i : boolean;
  signal sel_siu_sccommand_i    : boolean;
  signal sel_siu_bus_i          : boolean;

  signal sel_fec_al_i       : boolean;
  signal sel_rdol_i         : boolean;
  signal sel_sm_i           : boolean;
  signal sel_result_i       : boolean;
  signal sel_errreg_i       : boolean;
  signal sel_intmode_i      : boolean;
  signal sel_scadd_i        : boolean;
  signal sel_scdata_i       : boolean;
  signal sel_rcu_version_i  : boolean;
  signal sel_scexec_i       : boolean;
  signal sel_rst_errreg_i   : boolean;
  signal sel_rst_result_i   : boolean;
  signal sel_rst_sclksync_i : boolean;
  signal sel_sccommand_i    : boolean;

  signal add_msm_i  : std_logic_vector(15 downto 0);
  signal we_msm_i   : boolean;
  signal data_msm_i : std_logic_vector(31 downto 0);
  signal intmode_i  : std_logic_vector(1 downto 0);

  signal scexec_i : std_logic;
  signal scadd_i  : std_logic_vector(15 downto 0);
  signal scdata_i : std_logic_vector(15 downto 0);
  
begin  -- rtl

  -- purpose: msm_Collect combintatorics
  combinatorics : block
  begin  -- blockk combinatorics
    -- Selection of address/command from SIU
    sel_siu_sm_i           <= add_siu(15 downto 8) = X"81";
    -- sel_siu_fec_al_i        <= add_siu_i = X"8000";
    -- sel_siu_rdol_i          <= add_siu_i = X"8001";
    sel_siu_result_i       <= add_siu = X"8002";
    sel_siu_errreg_i       <= add_siu = X"8003";
    sel_siu_intmode_i      <= add_siu = X"8004";
    sel_siu_scadd_i        <= add_siu = X"8005";
    sel_siu_scdata_i       <= add_siu = X"8006";
    sel_siu_rcu_version_i  <= add_siu = X"8008";
    sel_siu_scexec_i       <= add_siu = X"8010";
    sel_siu_rst_errreg_i   <= add_siu = X"8011";
    sel_siu_rst_result_i   <= add_siu = X"8012";
    sel_siu_rst_sclksync_i <= add_siu = X"8020";
    sel_siu_sccommand_i    <= add_siu(15 downto 12) = X"C";

    -- SIU bus request
    sel_siu_bus_i <= (sel_siu_sm_i or
                      sel_siu_result_i or
                      sel_siu_errreg_i or
                      sel_siu_intmode_i or
                      sel_siu_scadd_i or
                      sel_siu_scdata_i or
                      sel_siu_rcu_version_i or
                      sel_siu_scexec_i or
                      sel_siu_rst_errreg_i or
                      sel_siu_rst_result_i or
                      sel_siu_rst_sclksync_i or
                      sel_siu_sccommand_i);

    -- Address selection (DCS has priority, SIU overrides if external flag
    -- dcsnsiu isn't set, and the SIU selects a register here) 
    add_msm_i <= add_siu when (dcsnsiu = '0' and sel_siu_bus_i) else
                 add_dcs;
    data_msm_i <= datain_siu when (dcsnsiu = '0' and sel_siu_bus_i) else
                  datain_dcs;
    we_msm_i <= we_siu = '1' when (dcsnsiu = '0' and sel_siu_bus_i) else
                we_dcs = '1';

    -- Selection of address/command - both SIU and DCS
    sel_sm_i           <= add_msm_i(15 downto 8) = X"81";
    -- sel_fec_al_i        <= add_msm_i = X"8000";
    -- sel_rdol_i          <= add_msm_i = X"8001";
    sel_fec_al_i       <= false;
    sel_rdol_i         <= false;
    sel_result_i       <= add_msm_i = X"8002";
    sel_errreg_i       <= add_msm_i = X"8003";
    sel_intmode_i      <= add_msm_i = X"8004";
    sel_scadd_i        <= add_msm_i = X"8005";
    sel_scdata_i       <= add_msm_i = X"8006";
    sel_rcu_version_i  <= add_msm_i = X"8008";
    sel_scexec_i       <= add_msm_i = X"8010";
    sel_rst_errreg_i   <= add_msm_i = X"8011";
    sel_rst_result_i   <= add_msm_i = X"8012";
    sel_rst_sclksync_i <= add_msm_i = X"8020";
    sel_sccommand_i    <= add_msm_i(15 downto 12) = X"C";

    -- Address, data, and write enable for status memory
    addsm <= addsm_fsc when wesm_fsc = '1' else
             add_msm_i(4 downto 0);
    datasm <= datasm_fsc when wesm_fsc = '1' else
              data_msm_i(15 downto 0);
    wesm <= '1' when wesm_fsc = '1' or (we_msm_i and sel_sm_i) else
            '0';

    -- Data for result register and write enable
    dataresult <= dataresult_fsc when weresult_fsc = '1' else
                  data_msm_i(20 downto 0);
    weresult <= '1' when (weresult_fsc = '1' or
                          (we_msm_i and sel_result_i)) else '0';

    -- Data and write enable for FEC active list
    data_fec_al <= fec_al_fsc when we_fec_al_fsc = '1' else data_msm_i;
    we_fec_al <= '1' when (we_fec_al_fsc = '1' or
                           (we_msm_i and sel_fec_al_i)) else '0';

    -- Data and write enable for FEC read-out list
    data_rdol <= rdol_fsc when we_rdol_fsc = '1' else data_msm_i;
    we_rdol <= '1' when (we_rdol_fsc = '1' or
                         (we_msm_i and sel_rdol_i)) else '0';

    -- Interrupt enables
    inta_enable <= intmode_i(0);
    intb_enable <= intmode_i(1);

    -- Reset of various registers and sync of I2C clk
    rst_errreg    <= '1' when (we_msm_i and sel_rst_errreg_i)   else '0';
    rst_resultreg <= '1' when (we_msm_i and sel_rst_result_i)   else '0';
    rst_sclksync  <= '1' when (we_msm_i and sel_rst_sclksync_i) else '0';

    -- Slow control execute 
    scexec_i <= '1' when ((we_msm_i and sel_scexec_i) or
                          (we_msm_i and sel_sccommand_i)) else '0';

    -- Output slow control address and data
    scadd  <= scadd_i;
    scdata <= scdata_i;
    
  end block combinatorics;

  -- purpose: msm_Register interrupt mode
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, inta_not_en, intb_not_en, we_msm_i, sel_intmode_i
  -- outputs: intmode_i
  reg_insmod : process (clk, rstb)
  begin  -- process reg_insmod
    if rstb = '0' then                  -- asynchronous reset (active low)
      intmode_i <= "00";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if (inta_not_en = '1') then
        intmode_i(0) <= '0';
      elsif (intb_not_en = '1') then
        intmode_i(1) <= '0';
      elsif we_msm_i and sel_intmode_i then
        intmode_i <= data_msm_i(1 downto 0);
      end if;
    end if;
  end process reg_insmod;

  -- purpose: msm_Register slow control commands, data, etc.
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, sel_scadd, sel_scdata, sel_sccommand, data_msm_i
  -- outputs: msm_scadd, scdata, 
  process (clk, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      scadd_i  <= (others => '0');
      scdata_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if (we_msm_i and sel_scadd_i) then
        scadd_i <= data_msm_i(15 downto 0);
      elsif (we_msm_i and sel_scdata_i) then
        scdata_i <= data_msm_i(15 downto 0);
      elsif (we_msm_i and sel_sccommand_i) then
        scadd_i(15)          <= '0';
        scadd_i(14 downto 8) <= add_msm_i(11 downto 5);
        scadd_i(7 downto 5)  <= "000";
        scadd_i(4 downto 0)  <= add_msm_i(4 downto 0);
        scdata_i             <= data_msm_i(15 downto 0);
      end if;
    end if;
  end process;

  -- purpose: msm_Register execution flag
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, scexec_i
  -- outputs: msm_scexec
  sc_exec_out : process (clk, rstb)
  begin  -- process sc_exec_out
    if rstb = '0' then                  -- asynchronous reset (active low)
      scexec <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      scexec <= scexec_i;
    end if;
  end process sc_exec_out;

  -- purpose: msm_Register output
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_
  output : process (clk, rstb)
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      dataout_msm <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if sel_result_i then
        dataout_msm <= "000" & X"00" & result_lsc;
      elsif sel_errreg_i then
        dataout_msm <= "00" & X"0000000" & errreg;
      elsif sel_intmode_i then
        dataout_msm <= "00" & X"0000000" & intmode_i;
      elsif sel_scadd_i then
        dataout_msm <= X"0000" & scadd_i;
      elsif sel_scdata_i then
        dataout_msm <= X"0000" & scdata_i;
      elsif sel_rcu_version_i then
        dataout_msm <= dataout_rcuversion;
      elsif sel_sm_i then
        dataout_msm <= X"0000" & dosm;
      end if;
    end if;
  end process output;
end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_signals_drv_pack is

  component msm_signals_drv
    port (
      clk                : in  std_logic;   -- Clock
      rstb               : in  std_logic;   -- Async reset
      add_siu            : in  std_logic_vector(15 downto 0);  -- SIU: msm_Address
      we_siu             : in  std_logic;   -- SIU: msm_Write enable
      datain_siu         : in  std_logic_vector(31 downto 0);  -- SIU: msm_write dat
      add_dcs            : in  std_logic_vector(15 downto 0);  -- DCS: msm_Address
      we_dcs             : in  std_logic;   -- DCS: msm_Write enable
      datain_dcs         : in  std_logic_vector(31 downto 0);  -- DCS: msm_write dat
      dcsnsiu            : in  std_logic;   -- BUS: msm_DCS (not SIU)
      addsm_fsc          : in  std_logic_vector(4 downto 0);  -- INT: msm_SM address
      datasm_fsc         : in  std_logic_vector(15 downto 0);  -- INT: msm_SM data
      wesm_fsc           : in  std_logic;   -- INT: msm_SM write enable
      dataresult_fsc     : in  std_logic_vector(20 downto 0);  -- INT: msm_result da
      weresult_fsc       : in  std_logic;   -- INT: msm_Write enab. res.
      fec_al_fsc         : in  std_logic_vector(31 downto 0);  -- INT: msm_Act. FECs
      we_fec_al_fsc      : in  std_logic;   -- INT: msm_Act. FECs write enab.
      rdol_fsc           : in  std_logic_vector(31 downto 0);  -- INT: msm_RO FECs
      we_rdol_fsc        : in  std_logic;   -- INT: msm_RO FECs write enab.
      result_lsc         : in  std_logic_vector(20 downto 0);  -- INT: msm_from here
      rdol               : in  std_logic_vector(31 downto 0);  -- CORE: msm_RO list
      dosm               : in  std_logic_vector(15 downto 0);  -- INT: msm_data out
      errreg             : in  std_logic_vector(1 downto 0);  -- REG: msm_errors
      inta_not_en        : in  std_logic;   -- REG: interrupts A disabled
      intb_not_en        : in  std_logic;   -- REG: interrupts B disabled
      dataout_rcuversion : in  std_logic_vector(31 downto 0);  -- CORE: msm_FW vers
      addsm              : out std_logic_vector(4 downto 0);  -- INT: msm_SM address
      datasm             : out std_logic_vector(15 downto 0);  -- INT: msm_SM data
      wesm               : out std_logic;   -- INT: msm_SM write enab.
      dataresult         : out std_logic_vector(20 downto 0);  -- INT: msm_Result da
      weresult           : out std_logic;   -- INT: msm_Result write enab.
      data_fec_al        : out std_logic_vector(31 downto 0);  -- CORE: msm_Valid FE
      we_fec_al          : out std_logic;   -- CORE: msm_FEC write enab.
      data_rdol          : out std_logic_vector(31 downto 0);  -- CORE: msm_Valid FE
      we_rdol            : out std_logic;   -- CORE: msm_FEC write enab.
      inta_enable        : out std_logic;   -- INT: msm_Interrupts on A enable
      intb_enable        : out std_logic;   -- INT: msm_Interrupts on B enable
      rst_errreg         : out std_logic;   -- INT: msm_reset errors
      rst_resultreg      : out std_logic;   -- INT: msm_reset result
      rst_sclksync       : out std_logic;   -- INT: msm_reset sync of I2C
      dataout_msm        : out std_logic_vector(31 downto 0);  -- CORE: out data
      scadd              : out std_logic_vector(15 downto 0);  -- CORE: msm_I2C addr
      scdata             : out std_logic_vector(15 downto 0);  -- CORE: msm_I2C data
      scexec             : out std_logic);  -- CORE : msm_I2C execute
  end component;

end msm_signals_drv_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
