-------------------- Test bench of RCU MS signal driver -----------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library msmodule_lib;
use msmodule_lib.msm_signals_drv_pack.all;

entity msm_signals_drv_tb is

end msm_signals_drv_tb;

architecture test of msm_signals_drv_tb is

  signal clk                : std_logic                     := '0';
  signal rstb               : std_logic                     := '0';
  signal add_siu            : std_logic_vector(15 downto 0) := (others => 'Z');
  signal we_siu             : std_logic                     := '0';
  signal datain_siu         : std_logic_vector(31 downto 0) := (others => 'Z');
  signal add_dcs            : std_logic_vector(15 downto 0) := (others => 'Z');
  signal we_dcs             : std_logic                     := '0';
  signal datain_dcs         : std_logic_vector(31 downto 0) := (others => 'Z');
  signal dcsnsiu            : std_logic                     := '0';
  signal addsm_fsc          : std_logic_vector(4 downto 0)  := (others => 'Z');
  signal datasm_fsc         : std_logic_vector(15 downto 0) := (others => 'Z');
  signal wesm_fsc           : std_logic                     := '0';
  signal dataresult_fsc     : std_logic_vector(20 downto 0) := '1' & X"deadb";
  signal weresult_fsc       : std_logic                     := '0';
  signal fec_al_fsc         : std_logic_vector(31 downto 0) := X"deadbeef";
  signal we_fec_al_fsc      : std_logic                     := '0';
  signal rdol_fsc           : std_logic_vector(31 downto 0) := X"beafdead";
  signal we_rdol_fsc        : std_logic                     := '0';
  signal result_lsc         : std_logic_vector(20 downto 0) := '0' & X"bdead";
  signal rdol               : std_logic_vector(31 downto 0) := X"deadbeef";
  signal dosm               : std_logic_vector(15 downto 0) := X"beef";
  signal errreg             : std_logic_vector(1 downto 0)  := "11";
  signal inta_not_en        : std_logic                     := '1';
  signal intb_not_en        : std_logic                     := '0';
  signal dataout_rcuversion : std_logic_vector(31 downto 0) := X"00776688";
  signal addsm              : std_logic_vector(4 downto 0);
  signal datasm             : std_logic_vector(15 downto 0);
  signal wesm               : std_logic;
  signal dataresult         : std_logic_vector(20 downto 0);
  signal weresult           : std_logic;
  signal data_fec_al        : std_logic_vector(31 downto 0);
  signal we_fec_al          : std_logic;
  signal data_rdol          : std_logic_vector(31 downto 0);
  signal we_rdol            : std_logic;
  signal inta_enable        : std_logic;
  signal intb_enable        : std_logic;
  signal rst_errreg         : std_logic;
  signal rst_resultreg      : std_logic;
  signal rst_sclksync       : std_logic;
  signal dataout_msm        : std_logic_vector(31 downto 0);
  signal scadd              : std_logic_vector(15 downto 0);
  signal scdata             : std_logic_vector(15 downto 0);
  signal scexec             : std_logic;

  -- Clock period
  constant PERIOD : time := 25 ns;

  type add_array_t is array (0 to 9) of integer;
  constant adds : add_array_t := (16#02#,                 -- result
                                  16#03#,                 -- errreg
                                  16#04#,                 -- intmode
                                  16#05#,                 -- scadd
                                  16#06#,                 -- scdata
                                  16#08#,                 -- rcu_version
                                  16#10#,                 -- scexec
                                  16#11#,                 -- rst_errreg
                                  16#12#,                 -- rst_result
                                  16#20#);                -- rst_sclksync
  procedure write_one(
    constant where : in  integer;
    constant value : in  integer;
    signal   add   : out std_logic_vector(15 downto 0);  -- Address
    signal   data  : out std_logic_vector(31 downto 0);
    signal   we    : out std_logic) is                   -- Write enable
    variable buf : std_logic_vector(7 downto 0);
  begin  -- write_one
      buf                := std_logic_vector(to_unsigned(value, 8));
      add(15 downto 12)   <= X"8";
      add(11 downto 0)    <= std_logic_vector(to_unsigned(where, 12));
      data(7 downto 0)   <= buf;
      data(15 downto 8)  <= buf;
      data(23 downto 16) <= buf;
      data(31 downto 24) <= buf;
      wait for PERIOD;
      we                 <= '1';
      wait for PERIOD;
      we                 <= '0';
      add                <= (others => 'Z');
      data               <= (others => 'Z');
      wait for PERIOD;
  end write_one;
    
  -- purpose: msm_Write to all registers
  procedure write_all (
    signal add  : out std_logic_vector(15 downto 0);  -- Address
    signal data : out std_logic_vector(31 downto 0);
    signal we   : out std_logic) is                   -- Write enable
    variable i : integer;
  begin  -- write_all
    wait for 2 * PERIOD;
    
    for i in adds'range loop
      write_one(where => adds(i),
                value => i+1,
                add   => add,
                data  => data,
                we    => we);
    end loop;  -- i
  end write_all;

  procedure write_mem (
    signal add  : out std_logic_vector(15 downto 0);
    signal data : out std_logic_vector(31 downto 0);
    signal we   : out std_logic) is
    variable i   : integer;
  begin  -- write_mem
    wait for PERIOD;

    for i in 0 to 25 loop
      write_one(where => 16#100# + i,
                value => i + 1,
                add   => add,
                data  => data,
                we    => we);
    end loop;  -- i
  end write_mem;
  
begin  -- test

  -- component instantiation
  dut : msm_signals_drv
    port map (
      clk                => clk,        -- [in]  Clock
      rstb               => rstb,       -- [in]  Async reset
      add_siu            => add_siu,    -- [in]  SIU: msm_Address
      we_siu             => we_siu,     -- [in]  SIU: msm_Write enable
      datain_siu         => datain_siu,   -- [in]  SIU : msm_write data
      add_dcs            => add_dcs,    -- [in]  DCS: msm_Address
      we_dcs             => we_dcs,     -- [in]  DCS: msm_Write enable
      datain_dcs         => datain_dcs,   -- [in]  DCS : msm_write data
      dcsnsiu            => dcsnsiu,    -- [in]  BUS: msm_DCS (not SIU)
      addsm_fsc          => addsm_fsc,  -- [in]  INT: msm_SM address
      datasm_fsc         => datasm_fsc,   -- [in]  INT: msm_SM data
      wesm_fsc           => wesm_fsc,   -- [in]  INT: msm_SM write enable
      dataresult_fsc     => dataresult_fsc,      -- [in]  INT: msm_result data
      weresult_fsc       => weresult_fsc,   -- [in]  INT: msm_Write enab. res.
      fec_al_fsc         => fec_al_fsc,   -- [in]  INT: msm_Act. FECs
      we_fec_al_fsc      => we_fec_al_fsc,  -- [in]  INT: msm_Act. FECs write enab.
      rdol_fsc           => rdol_fsc,   -- [in]  INT: msm_RO FECs
      we_rdol_fsc        => we_rdol_fsc,  -- [in]  INT: msm_RO FECs write enab.
      result_lsc         => result_lsc,   -- [in]  INT: msm_from here
      rdol               => rdol,       -- [in]  CORE: msm_RO list
      dosm               => dosm,       -- [in]  INT: msm_data output
      errreg             => errreg,     -- [in]  REG: msm_errors
      inta_not_en        => inta_not_en,  -- [in]  REG: interrupts A disabled
      intb_not_en        => intb_not_en,  -- [in]  REG: interrupts B disabled
      dataout_rcuversion => dataout_rcuversion,  -- [in]  CORE: msm_FW version
      addsm              => addsm,      -- [out] INT: msm_SM address
      datasm             => datasm,     -- [out] INT: msm_SM data
      wesm               => wesm,       -- [out] INT: msm_SM write enab.
      dataresult         => dataresult,   -- [out] INT: msm_Result data
      weresult           => weresult,   -- [out] INT: msm_Result write enab.
      data_fec_al        => data_fec_al,  -- [out] CORE: msm_Valid FECs
      we_fec_al          => we_fec_al,  -- [out] CORE: msm_FEC write enab.
      data_rdol          => data_rdol,  -- [out] CORE: msm_Valid FECs
      we_rdol            => we_rdol,    -- [out] CORE: msm_FEC write enab.
      inta_enable        => inta_enable,  -- [out] INT: msm_Interrupts on A enable
      intb_enable        => intb_enable,  -- [out] INT: msm_Interrupts on B enable
      rst_errreg         => rst_errreg,   -- [out] INT: msm_reset errors
      rst_resultreg      => rst_resultreg,  -- [out] INT: msm_reset result
      rst_sclksync       => rst_sclksync,   -- [out] INT: msm_reset sync of I2C
      dataout_msm        => dataout_msm,  -- [out] CORE: out data
      scadd              => scadd,      -- [out] CORE: msm_I2C addr
      scdata             => scdata,     -- [out] CORE: msm_I2C data
      scexec             => scexec);    -- [out] CORE : msm_I2C execute

  -- clock generation
  clk <= not clk after PERIOD / 2;

  fsc_res : process (clk, rstb)
  begin  -- process fsc_res
    if rstb = '0' then                  -- asynchronous reset (active low)
      dataresult_fsc <= (others => '0');
      weresult_fsc   <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      dataresult_fsc <= '1' & X"11111";
      if add_dcs = X"8002" then
        weresult_fsc <= '1';
      else
        weresult_fsc <= '0';
      end if;
    end if;
  end process fsc_res;
  
  -- waveform generation
  stim : process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';
    wait for 4 * PERIOD;

    write_all(add  => add_dcs,
              data => datain_dcs,
              we   => we_dcs);

    wait for 4 * PERIOD;
    write_mem(add => add_dcs,
              data => datain_dcs,
              we   => we_dcs);

    wait for 4 * PERIOD;
    write_all(add  => add_siu,
              data => datain_siu,
              we   => we_siu);
    
    wait;                               -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration signals_drv_vhdl of msm_signals_drv_tb is

  for test
    for dut : msm_signals_drv
      use entity msmodule_lib.msm_signals_drv(rtl);
    end for;
  end for;

end signals_drv_vhdl;


-------------------------------------------------------------------------------
--
-- EOF
--
