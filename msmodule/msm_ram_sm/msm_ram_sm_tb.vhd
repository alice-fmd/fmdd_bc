-------------------- Test bench of RCU MS RAM for status memory  --------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library msmodule_lib;
use msmodule_lib.msm_ram_sm_pack.all;

entity msm_ram_sm_tb is

end msm_ram_sm_tb;

architecture test of msm_ram_sm_tb is
  -- component ports
  signal clk     : std_logic                     := '0';  -- [in]  Clock
  signal address : std_logic_vector(4 downto 0)  := (others => '0');  --
  signal data    : std_logic_vector(15 downto 0) := (others => '0');  --
  signal we      : std_logic                     := '0';  -- [in]  Write enable
  signal q       : std_logic_vector(15 downto 0);         -- [out] output

  -- Clock period
  constant PERIOD : time := 25 ns;

  -- purpose: msm_Write data to ram
  procedure write_data (
    constant input   : in  std_logic_vector(15 downto 0);
    constant where   : in  integer;     -- Address
    signal   data    : out std_logic_vector(15 downto 0);
    signal   address : out std_logic_vector(4 downto 0);
    signal   we      : out std_logic) is
  begin  -- write_data
    data <= input;
    address <= std_logic_vector(to_unsigned(where, 5));
    wait for PERIOD;
    we <= '1';
    wait for PERIOD;
    we <= '0';
    wait for PERIOD;
  end write_data;

  
begin  -- test

  -- component instantiation
  dut: msm_ram_sm
    port map (
      clk     => clk,                   -- [in]  Clock
      address => address,               -- [in]  Address
      data    => data,                  -- [in]  Input
      we      => we,                    -- [in]  Write enable
      q       => q);                    -- [out] output

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim : process
    variable i     : integer;
    variable input : std_logic_vector(15 downto 0);
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;

    for i in 0 to 25 loop
      input(7 downto 0)   := "000" & std_logic_vector(to_unsigned(i+1, 5));
      input(15 downto 8)   := input(7 downto 0);
      write_data(input   =>  input,
                 where   =>  i,
                 data    =>  data,
                 address =>  address,
                 we      =>  we);
      wait for 1 * PERIOD;
    end loop;  -- i
    wait;                               -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration ram_sm_vhdl of msm_ram_sm_tb is

  for test
    for dut : msm_ram_sm
      use entity msmodule_lib.msm_ram_sm(rtl);
    end for;
  end for;

end ram_sm_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
-------------------------------------------------------------------------------
