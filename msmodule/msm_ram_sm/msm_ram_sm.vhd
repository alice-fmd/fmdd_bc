-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- Memory to store data from Interrupt Handling block
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity msm_ram_sm is
  
  port (
    clk     : in  std_logic;                       -- Clock
    address : in  std_logic_vector(4 downto 0);    -- Address
    data    : in  std_logic_vector(15 downto 0);   -- Input
    we      : in  std_logic;                       -- Write enable
    q       : out std_logic_vector(15 downto 0));  -- output

end msm_ram_sm;

architecture rtl of msm_ram_sm is
  type mem_t is array (25 downto 0) of std_logic_vector(15 downto 0);
  signal mem_i : mem_t := ((others => X"0000"));

  constant rst_i  : std_logic := '1';
begin  -- rtl

  -- purpose: msm_Process
  -- type   : msm_sequential
  -- inputs : msm_clk, <reset>
  -- outputs: msm_
  store : process (clk)
    variable i : integer;
  begin  -- process store 
    if clk'event and clk = '1' then  -- rising clock edge
      i := to_integer(unsigned(address));
      if we = '1' then
        mem_i(i) <= data;
      end if;
      q <= mem_i(i);
    end if;
  end process store ;
  

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_ram_sm_pack is

  component msm_ram_sm
    port (
      clk     : in  std_logic;                       -- Clock
      address : in  std_logic_vector(4 downto 0);    -- Address
      data    : in  std_logic_vector(15 downto 0);   -- Input
      we      : in  std_logic;                       -- Write enable
      q       : out std_logic_vector(15 downto 0));  -- output
  end component;

end msm_ram_sm_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
