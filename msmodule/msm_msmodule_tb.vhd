-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library msmodule_lib;
use msmodule_lib.msm_msmodule_pack.all;
use msmodule_lib.msm_slave_pack.all;

entity msm_msmodule_tb is
end msm_msmodule_tb;

-------------------------------------------------------------------------------
architecture test of msm_msmodule_tb is

  -- component ports
  signal clk                : std_logic                     := '0';  -- [in]
  signal rstb               : std_logic                     := '0';  -- [in]
  signal rstb_global        : std_logic                     := '0';  -- [in]
  signal add_siu            : std_logic_vector(15 downto 0) := (others => '0');
  signal we_siu             : std_logic                     := '0';  -- [in]
  signal data_siu           : std_logic_vector(31 downto 0) := (others => '0');
  signal add_dcs            : std_logic_vector(15 downto 0) := (others => '0');
  signal we_dcs             : std_logic                     := '0';  -- [in]
  signal data_dcs           : std_logic_vector(31 downto 0) := (others => '0');
  signal dcsnsiu            : std_logic                     := '0';  -- [in]
  signal interrupta_in      : std_logic                     := '1';  -- [in]
  signal interruptb_in      : std_logic                     := '1';  -- [in]
  signal sda_outa           : std_logic                     := '0';  -- [in]
  signal sda_outb           : std_logic                     := '0';  -- [in]
  signal dataout_rcuversion : std_logic_vector(31 downto 0) := (others => '0');
  signal dataout_bpversion  : std_logic                     := '0';  -- [in]
  signal sda_ina            : std_logic;                             -- [out]
  signal sda_inb            : std_logic;                             -- [out]
  signal scla               : std_logic;                             -- [out]
  signal sclb               : std_logic;                             -- [out]
  signal warningtodcs       : std_logic;                             -- [out]
  signal we_fec_al_fsc      : std_logic;                             -- [out]
  signal fec_al_fsc         : std_logic_vector(31 downto 0);         -- [out]
  signal dataout_msm        : std_logic_vector(31 downto 0);         -- [out]
  signal rst_sclksync       : std_logic;                             -- [out]
  signal fec_al             : std_logic_vector(31 downto 0) := (others => '0');

  -- clock period
  constant PERIOD : time := 25 ns;

  signal dataA    : std_logic_vector(7 downto 0);
  signal dataB    : std_logic_vector(7 downto 0);
  signal sdaA_out : std_logic;
  signal sdaB_out : std_logic;
  signal sdaA_in  : std_logic;
  signal sdaB_in  : std_logic;

  signal start_a : boolean := false;
  signal start_b : boolean := false;
  
  type words_t is array (0 to 1) of std_logic_vector(15 downto 0);
  constant wordA : words_t := (X"dead", X"2001");
  constant wordB : words_t := (X"beef", X"2000");
  
begin  -- test

  -- component instantiation
  DUT : msm_msmodule
    port map (
      clk                => clk,                 -- [in]
      rstb               => rstb,                -- [in]
      rstb_global        => rstb_global,         -- [in]
      add_siu            => add_siu,             -- [in]
      we_siu             => we_siu,              -- [in]
      datain_siu         => data_siu,            -- [in]
      add_dcs            => add_dcs,             -- [in]
      we_dcs             => we_dcs,              -- [in]
      datain_dcs         => data_dcs,            -- [in]
      dcsnsiu            => dcsnsiu,             -- [in]
      interrupta_in      => interrupta_in,       -- [in]
      interruptb_in      => interruptb_in,       -- [in]
      sda_outa           => sda_outa,            -- [in]
      sda_outb           => sda_outb,            -- [in]
      dataout_rcuversion => dataout_rcuversion,  -- [in]
      dataout_bpversion  => dataout_bpversion,   -- [in]
      sda_ina            => sda_ina,             -- [out]
      sda_inb            => sda_inb,             -- [out]
      scla               => scla,                -- [out]
      sclb               => sclb,                -- [out]
      warningtodcs       => warningtodcs,        -- [out]
      we_fec_al_fsc      => we_fec_al_fsc,       -- [out]
      fec_al_fsc         => fec_al_fsc,          -- [out]
      dataout_msm        => dataout_msm,         -- [out]
      rst_sclksync       => rst_sclksync,        -- [out]
      fec_al             => fec_al);             -- [in]

  -- clock generation
  clk <= not clk after PERIOD;

  -- Simulate GTL response to 'Z'
  sdaA_in <= '1' when sda_ina = '1' else '0';
  sdaB_in <= '1' when sda_inb = '1' else '0';
  sda_outa <= '0' when sdaa_out = '0' else '1';
  sda_outb <= '0' when sdab_out = '0' else '1';
    
  
  slaveA_gen : for i in 0 to 1 generate
    slave_A : entity msmodule_lib.msm_slave(test2)
      generic map (
        ME     => std_logic_vector(to_unsigned(i, 4)),
        PERIOD => 200 ns,
        DELAY  => 8)
      port map (
        scl     => sclA,                -- [in]  Serial clock
        sda_in  => sdaA_in,             -- [in]  Serial data in
        sda_out => sdaA_out,            -- [out] Serial data out
        word    => wordA(i),            -- [in]  2 output bytes
        data    => open);               -- [out] Output byte
  end generate slaveA_gen;

  slaveB_gen : for i in 0 to 1 generate
    slave_B : entity msmodule_lib.msm_slave(test2)
      generic map (
        ME     => std_logic_vector(to_unsigned(i, 4)),
        PERIOD => 200 ns,
        DELAY  => 8)
      port map (
        scl     => sclB,                -- [in]  Serial clock
        sda_in  => sdaB_in,             -- [in]  Serial data in
        sda_out => sdaB_out,            -- [out] Serial data out
        word    => wordB(i),            -- [in]  2 output bytes
        data    => open);               -- [out] Output byte
  end generate slaveB_gen;

  -- purpose: FECs
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  fecs : process (clk, rstb)
  begin  -- process fecs
    if rstb = '0' then                  -- asynchronous reset (active low)
      fec_al <= X"00030003";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_fec_al_fsc = '1' then
        fec_al <= fec_al_fsc;
        if (fec_al & X"0000FFFF" /= X"00000000") then
          interrupta_in <= '1';
        elsif (fec_al & X"FFFF0000" /= X"00000000") then
          interruptb_in <= '1';
        end if;
      end if;
      if start_a then
        report "Will make an interrupt on branch A" severity note;
        interrupta_in <= '0';           -- Active low
      end if;
      if start_b then
        report "Will make an interrupt on branch B" severity note;
        interruptb_in <= '0';           -- Active low
      end if;
    end if;
  end process fecs;

  -- waveform generation
  stim : process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb        <= '1';
    rstb_global <= '1';

    wait for 4 * PERIOD;

    add_dcs <=  X"8004";
    data_dcs <= X"00000003";
    we_dcs <= '1'; 
    wait for PERIOD;
    we_dcs <= '0';
    
    wait for 4 * PERIOD;

    start_a <= true;
    wait for 2 * PERIOD;
    start_a <= false;
    
    wait; -- forever
  end process stim;
 

end test;

-------------------------------------------------------------------------------
configuration msmodule_vhdl of msm_msmodule_tb is

  use msmodule_lib.msm_msmodule_pack.all;
  use msmodule_lib.msm_lsc_core_pack.all;
  use msmodule_lib.msm_sequencer_rcu_pack.all;
  use msmodule_lib.msm_instr_builder_pack.all;
  use msmodule_lib.msm_master_pack.all;
  use msmodule_lib.msm_master_sm_pack.all;
  use msmodule_lib.msm_sync_pack.all;
  use msmodule_lib.msm_serializer_rcu_pack.all;
  use msmodule_lib.msm_interrupt_driver_pack.all;
  use msmodule_lib.msm_interrupt_handler_pack.all;
  use msmodule_lib.msm_cards_status_pack.all;
  use msmodule_lib.msm_branch_selector_pack.all;
  use msmodule_lib.msm_mux_signals_pack.all;
  use msmodule_lib.msm_sc_signals_pack.all;
  use msmodule_lib.msm_clock_master_pack.all;
  use msmodule_lib.msm_ffd_pack.all;
  use msmodule_lib.msm_error_module_pack.all;
  use msmodule_lib.msm_result_pack.all;
  use msmodule_lib.msm_ram_sm_pack.all;
  use msmodule_lib.msm_comm_selection_pack.all;
  use msmodule_lib.msm_signals_drv_pack.all;
  
  for test
    for dut : msm_msmodule
      use entity msmodule_lib.msm_msmodule(rtl); 
      for rtl
        for all : msm_lsc_core
          use entity msmodule_lib.msm_lsc_core(rtl);
          for rtl
            for all : msm_sequencer_rcu
              use entity msmodule_lib.msm_sequencer_rcu(rtl);
            end for;
            for all : msm_instr_builder
              use entity msmodule_lib.msm_instr_builder(rtl);
            end for;
            for all : msm_master
              use entity msmodule_lib.msm_master(rtl);
              for rtl
                for all : msm_master_sm
                  use entity msmodule_lib.msm_master_sm(rtl);
                end for;
                for all : msm_sync
                  use entity msmodule_lib.msm_sync(rtl);
                end for;
                for all : msm_serializer_rcu
                  use entity msmodule_lib.msm_serializer_rcu(rtl);
                end for;
              end for;
            end for;
            for all : msm_interrupt_driver
              use entity msmodule_lib.msm_interrupt_driver(rtl);
              for rtl
                for all : msm_interrupt_handler
                  use entity msmodule_lib.msm_interrupt_handler(rtl);
                end for;
                for all : msm_cards_status
                  use entity msmodule_lib.msm_cards_status(rtl);
                  for rtl
                    for all : msm_branch_selector
                      use entity msmodule_lib.msm_branch_selector(rtl);
                    end for;
                  end for;
                end for;
              end for;
            end for;
            for all : msm_mux_signals
              use entity msmodule_lib.msm_mux_signals(rtl);
            end for;
            for all : msm_sc_signals
              use entity msmodule_lib.msm_sc_signals(rtl);
            end for;
          end for;
        end for;
        for all : msm_ffd
          use entity msmodule_lib.msm_ffd(rtl);
        end for;
        for clock_master_1 : msm_clock_master
          use entity msmodule_lib.msm_clock_master(rtl);
        end for;
        for all : msm_sync
          use entity msmodule_lib.msm_sync(rtl);
        end for;
        for all : msm_error_module
          use entity msmodule_lib.msm_error_module(rtl);
        end for;
        for all : msm_result
          use entity msmodule_lib.msm_result(rtl);
        end for;
        for all : msm_ram_sm
          use entity msmodule_lib.msm_ram_sm(rtl);
        end for;
        for all : msm_comm_selection
          use entity msmodule_lib.msm_comm_selection(rtl);
        end for;
        for all : msm_signals_drv
          use entity msmodule_lib.msm_signals_drv(rtl);
        end for;
      end for;
    end for;
  end for;

end msmodule_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
