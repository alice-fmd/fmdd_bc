-------------------- Test bench of RCU MS error module ------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_error_module_pack.all;

entity msm_error_module_tb is

end msm_error_module_tb;

architecture test of msm_error_module_tb is

  -- component ports
  signal clk         : std_logic                     := '0';  -- [in]  Clock
  signal rstb        : std_logic                     := '0';  -- [in]  Async
  signal nack        : std_logic                     := '0';  -- [in]  No ack
  signal seq_active  : std_logic                     := '0';  -- [in]  Seq act
  signal exec_in     : std_logic                     := '0';  -- [in]  Interrupt
  signal bcast_dcs   : std_logic                     := '0';  -- [in]  Broadcast
  signal branch_dcs  : std_logic                     := '0';  -- [in]  Branch
  signal fec_add_dcs : std_logic_vector(3 downto 0)  := (others => '0');  --
  signal fec_al      : std_logic_vector(31 downto 0) := X"00030003";      --
  signal rst_errreg  : std_logic                     := '0';  -- [in]  Reset
  signal errreg      : std_logic_vector(1 downto 0);  -- [out] Error register
  signal exec_dcs    : std_logic;       -- [out] Execute DCS

  -- Clock period
  constant PERIOD : time := 25 ns;

  procedure reset_it (
    signal clk : in  std_logic;
    signal rst : out std_logic) is
  begin
    wait until rising_edge(clk);
    rst <= '1';
    wait for PERIOD / 2;
    wait until rising_edge(clk);
    rst <= '0';
    wait for PERIOD;
  end reset_it;

  procedure execute (
    signal   clk    : in  std_logic;
    signal   exec   : out std_logic;
    signal   bcast  : out std_logic;
    signal   add    : out std_logic_vector(3 downto 0);
    signal   branch : out std_logic;
    constant fec    : in  std_logic_vector(4 downto 0);
    constant do_all : in  std_logic) is
  begin
    wait until rising_edge(clk);
    bcast  <= do_all;
    add    <= fec(3 downto 0);
    branch <= fec(4);
    wait for PERIOD / 2;
    wait until rising_edge(clk);
    exec   <= '1';
    wait for PERIOD / 2;
    wait until rising_edge(clk);
    exec   <= '0';
  end execute;

  
begin  -- test

  -- component instantiation
  dut : msm_error_module
    port map (
      clk         => clk,               -- [in]  Clock
      rstb        => rstb,              -- [in]  Async reset
      nack        => nack,              -- [in]  No acknowledge from FEC
      seq_active  => seq_active,        -- [in]  Sequencer is active
      exec_in     => exec_in,           -- [in]  Interrupt handler execute
      bcast_dcs   => bcast_dcs,         -- [in]  Broadcast from DCS
      branch_dcs  => branch_dcs,        -- [in]  Branch from DCS
      fec_add_dcs => fec_add_dcs,       -- [in]  FEC address from DCS
      fec_al      => fec_al,            -- [in]  Active FECs
      rst_errreg  => rst_errreg,        -- [in]  Reset errors
      errreg      => errreg,            -- [out] Error register
      exec_dcs    => exec_dcs);         -- [out] Execute DCS

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim : process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';

    wait for 4 * PERIOD;
    execute(clk    => clk,
            exec   => exec_in,
            bcast  => bcast_dcs,
            branch => branch_dcs,
            add    => fec_add_dcs,
            fec    => '0' & X"4",
            do_all => '0');
    wait for PERIOD;

    reset_it(clk => clk,
             rst => rst_errreg);

    wait for 4 * PERIOD;
    nack <= '1';
    wait for PERIOD;
    nack <= '0';
    wait for PERIOD;

    execute(clk    => clk,
            exec   => exec_in,
            bcast  => bcast_dcs,
            branch => branch_dcs,
            add    => fec_add_dcs,
            fec    => '1' & X"F",
            do_all => '1');
    wait for PERIOD;
    
    reset_it(clk => clk,
             rst => rst_errreg);


    wait;                               -- forever
  end process stim;

  

end test;
-------------------------------------------------------------------------------
configuration error_module_vhdl of msm_error_module_tb is

  for test
    for dut : msm_error_module
      use entity msmodule_lib.msm_error_module(rtl);
    end for;
  end for;

end error_module_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
