-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- This module takes care o the possibles error and writes them in the Error
-- Register. The bit 0 is asserted if the DCS send a request to a FEC that is
-- OFF. In that case, the execution (exec) is not sent.  The bit 1 is asserted
-- if the card is ON but it does not answer 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity msm_error_module is
  
  port (
    clk         : in  std_logic;        -- Clock
    rstb        : in  std_logic;        -- Async reset
    nack        : in  std_logic;        -- No acknowledge from FEC
    seq_active  : in  std_logic;        -- Sequencer is active
    exec_in     : in  std_logic;        -- Interrupt handler execute
    bcast_dcs   : in  std_logic;        -- Broadcast from DCS
    branch_dcs  : in  std_logic;        -- Branch from DCS
    fec_add_dcs : in  std_logic_vector(3 downto 0);   -- FEC address from DCS
    fec_al      : in  std_logic_vector(31 downto 0);  -- Active FECs
    rst_errreg  : in  std_logic;        -- Reset errors
    errreg      : out std_logic_vector(1 downto 0);   -- Error register
    exec_dcs    : out std_logic);       -- Execute DCS

end msm_error_module;

architecture rtl of msm_error_module is
  signal fec_add_i   : std_logic_vector(4 downto 0);
  signal err0_i      : std_logic;
  signal err1_i      : std_logic;
  signal exec_reg1_i : std_logic;
  signal exec_reg2_i : std_logic;
  signal exec_i      : std_logic;
begin  -- rtl
  fec_add_i(4)          <= branch_dcs;
  fec_add_i(3 downto 0) <= fec_add_dcs;
  errreg(0)             <= err0_i;
  errreg(1)             <= err1_i;


  -- purpose: msm_First bit
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, nack, rst_errreg, exec_in
  -- outputs: msm_err1
  bit_1 : process (clk, rstb)
  begin  -- process bit_1
    if rstb = '0' then                  -- asynchronous reset (active low)
      err1_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if rst_errreg = '1' then
        err1_i <= '0';
      elsif exec_in = '1' then
        err1_i <= '0';
      elsif nack = '1' then
        err1_i <= '1';
      end if;
    end if;
  end process bit_1;

  -- purpose: msm_0th bit
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, rst_errreg, exec_in
  -- outputs: msm_err0
  bit_0 : process (clk, rstb)
  begin  -- process bit_0
    if rstb = '0' then                  -- asynchronous reset (active low)
      err0_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if rst_errreg = '1' then
        err0_i <= '0';
      elsif exec_in = '1' then
        err0_i <= not (fec_al(to_integer(unsigned(fec_add_i))) or bcast_dcs);
      end if;
    end if;
  end process bit_0;


  -- purpose: msm_Glitch on execute
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_
  execute : process (clk, rstb)
  begin  -- process execute
    if rstb = '0' then                  -- asynchronous reset (active low)
      exec_reg1_i <= '0';
      exec_reg2_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      exec_reg1_i <= exec_in;
      exec_reg2_i <= exec_reg1_i;
    end if;
  end process execute;

  -- Exec is not sent if the FEC is OFF
  -- If it is sent, it is asserted for 2 40MHz clock cycles
  exec_i <= '1' when ((exec_reg1_i = '1' or exec_reg2_i = '1')
                      and (err0_i = '0') and (seq_active = '0')) else '0';

  -- purpose: msm_Send execution bit
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_
  exec_out : process (clk, rstb)
  begin  -- process exec_out
    if rstb = '0' then                  -- asynchronous reset (active low)
      exec_dcs <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      exec_dcs <= exec_i;
    end if;
  end process exec_out;
end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


package msm_error_module_pack is
  component msm_error_module
    port (
      clk         : in  std_logic;      -- Clock
      rstb        : in  std_logic;      -- Async reset
      nack        : in  std_logic;      -- No acknowledge from FEC
      seq_active  : in  std_logic;      -- Sequencer is active
      exec_in     : in  std_logic;      -- Interrupt handler execute
      bcast_dcs   : in  std_logic;      -- Broadcast from DCS
      branch_dcs  : in  std_logic;      -- Branch from DCS
      fec_add_dcs : in  std_logic_vector(3 downto 0);   -- FEC address from DCS
      fec_al      : in  std_logic_vector(31 downto 0);  -- Active FECs
      rst_errreg  : in  std_logic;      -- Reset errors
      errreg      : out std_logic_vector(1 downto 0);   -- Error register
      exec_dcs    : out std_logic);     -- Execute DCS
  end component;
end package msm_error_module_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
