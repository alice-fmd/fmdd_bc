-------------------- Test bench of RCU MS Instruction builder -----------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_instr_builder_pack.all;

entity msm_instr_builder_tb is
end msm_instr_builder_tb;

architecture test of msm_instr_builder_tb is

  -- component ports
  signal clk     : std_logic                     := '0';  -- [in]  Clock
  signal rstb    : std_logic                     := '0';  -- [in]  Async reset
  signal add     : std_logic_vector(2 downto 0)  := (others => '0');  -- [in] 
  signal rnw     : std_logic                     := '0';  -- [in]  Read
  signal bcast   : std_logic                     := '0';  -- [in]  Broadcast
  signal fec_add : std_logic_vector(3 downto 0)  := (others => '0');  -- [in]
  signal reg_add : std_logic_vector(7 downto 0)  := (others => '0');  -- [in]
  signal data    : std_logic_vector(15 downto 0) := (others => '0');  -- [in]
  signal word    : std_logic_vector(9 downto 0);          -- [out] Output word

  -- Clock period
  constant PERIOD : time := 25 ns;    

  constant start_cond : std_logic_vector(2 downto 0) := "000";
  constant bc_addr    : std_logic_vector(2 downto 0) := "001";
  constant reg_addr   : std_logic_vector(2 downto 0) := "010";
  constant reg_data1  : std_logic_vector(2 downto 0) := "011";
  constant reg_data2  : std_logic_vector(2 downto 0) := "100";
  constant stop_cond  : std_logic_vector(2 downto 0) := "101";

  -- purpose: msm_Go throug cycle
  procedure cycle (
    signal add : out std_logic_vector(2 downto 0)) is
  begin  -- cycle
    wait for 2 * PERIOD;
    add <= start_cond;

    wait for 2 * PERIOD;
    add <= bc_addr;

    wait for 2 * PERIOD;
    add <= reg_addr;

    wait for 2 * PERIOD;
    add <= reg_data1;

    wait for 2 * PERIOD;
    add <= reg_data2;

    wait for 2 * PERIOD;
    add <= stop_cond;    
  end cycle;
  
begin  -- test

  -- component instantiation
  dut: msm_instr_builder
    port map (
      clk     => clk,                   -- [in]  Clock
      rstb    => rstb,                  -- [in]  Async reset
      add     => add,                   -- [in]  Address
      rnw     => rnw,                   -- [in]  Read (not write)
      bcast   => bcast,                 -- [in]  Broadcast
      fec_add => fec_add,               -- [in]  FEC address
      reg_add => reg_add,               -- [in]  Register address
      data    => data,                  -- [in]  Write data
      word    => word);                 -- [out] Output word

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';

    wait for 4 * PERIOD;
    fec_add <= X"1";
    bcast   <= '0';
    rnw     <= '0';
    reg_add <= X"0a";
    data    <= X"dead";
    cycle(add => add);

    wait for 4 * PERIOD;
    rnw <= '1';
    cycle(add => add);

    wait for 4 * PERIOD;
    rnw <= '0';
    bcast <= '1';
    cycle(add => add);

    wait for 4 * PERIOD;
    rnw <= '1';
    bcast <= '1';
    cycle(add => add);
    

    
    
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_instr_builder_vhdl of msm_instr_builder_tb is

  for test
    for dut : msm_instr_builder
      use entity msmodule_lib.msm_instr_builder(rtl);
    end for;
  end for;

end msm_instr_builder_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
