-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- This module builds the sequence of instruction for the Board Controller in
-- function of the information sent by the DCS using the address and data buses
-- or by the Interrupt handler in case of Interrupt.
library ieee;
use ieee.std_logic_1164.all;

entity msm_instr_builder is
  
  port (
    clk     : in  std_logic;                      -- Clock
    rstb    : in  std_logic;                      -- Async reset
    add     : in  std_logic_vector(2 downto 0);   -- Address
    rnw     : in  std_logic;                      -- Read (not write)
    bcast   : in  std_logic;                      -- Broadcast
    fec_add : in  std_logic_vector(3 downto 0);   -- FEC address
    reg_add : in  std_logic_vector(7 downto 0);   -- Register address
    data    : in  std_logic_vector(15 downto 0);  -- Write data
    word    : out std_logic_vector(9 downto 0));  -- Output word

end msm_instr_builder;

architecture rtl of msm_instr_builder is

  constant start : std_logic_vector(1 downto 0) := "10";  -- Start condition
  constant write : std_logic_vector(1 downto 0) := "11";  -- Write flag
  constant read  : std_logic_vector(1 downto 0) := "01";  -- Read flag
  constant stop  : std_logic_vector(1 downto 0) := "00";  -- Stop condition
  
begin  -- rtl

  -- purpose: msm_Output signals
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, rnw, bcast, fec_add, reg_add, data
  -- outputs: msm_word
  output: process (clk, rstb)
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      word <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge

      if rnw = '0' then                 -- Write
        case add is
          when "000"  => word <= start & "00000000"; -- (others => '0');
          when "001"  => word <= write & '0' & bcast & '0' & fec_add & '0';
          when "010"  => word <= write & reg_add;
          when "011"  => word <= write & data(15 downto 8);
          when "100"  => word <= write & data(7 downto 0);
          when "101"  => word <= stop  & "00000000"; -- (others => '0');
          when others => word <= (others => '0');
        end case;
      else                              -- Read a register
        case add is
          when "000"  => word <= start & "00000000";
          when "001"  => word <= write & "000" & fec_add & '1';
          when "010"  => word <= write & reg_add;
          when "011"  => word <= read  & "00000000"; -- (others => '0');
          when "100"  => word <= read  & "00000000"; -- (others => '0');
          when "101"  => word <= stop  & "00000000"; -- (others => '0');
          when others => word <= (others => '0');
        end case;
      end if;
    end if;
  end process output;
end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_instr_builder_pack is

  component msm_instr_builder
    port (
      clk     : in  std_logic;                      -- Clock
      rstb    : in  std_logic;                      -- Async reset
      add     : in  std_logic_vector(2 downto 0);   -- Address
      rnw     : in  std_logic;                      -- Read (not write)
      bcast   : in  std_logic;                      -- Broadcast
      fec_add : in  std_logic_vector(3 downto 0);   -- FEC address
      reg_add : in  std_logic_vector(7 downto 0);   -- Register address
      data    : in  std_logic_vector(15 downto 0);  -- Write data
      word    : out std_logic_vector(9 downto 0));  -- Output word
  end component;

end msm_instr_builder_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
