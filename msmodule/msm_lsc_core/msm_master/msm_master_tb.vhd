-------------------- Test bench of RCU MS I2C master --------------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_master_pack.all;
use msmodule_lib.msm_slave_pack.all;
use msmodule_lib.msm_ffd_pack.all;
use msmodule_lib.msm_clock_master_pack.all;

entity msm_master_tb is

end msm_master_tb;

-------------------------------------------------------------------------------

architecture test of msm_master_tb is

  -- component ports
  signal clk40      : std_logic                    := '0';  -- 40 MHz base clock
  signal clk        : std_logic                    := '0';  -- [in]  Clock
  signal rstb       : std_logic                    := '0';  -- [in]  Async reset
  signal do_im      : std_logic_vector(7 downto 0) := (others => '0');
  signal new_data   : std_logic                    := '0';  -- [in]  Got more
  signal read       : std_logic                    := '0';  -- [in]  Read flag
  signal write      : std_logic                    := '0';  -- [in]  Write flag
  signal start      : std_logic                    := '0';  -- [in]  Start cond
  signal stop       : std_logic                    := '0';  -- [in]  Stop cond
  signal sda_out    : std_logic                    := '0';  -- [in]  Serial dat
  signal sda_ack    : std_logic                    := '0';  -- [in]  Acknowledge
  signal ready_seq  : std_logic;        -- [out] Sequencer ready
  signal data_valid : std_logic;        -- [out] Data is valid
  signal error      : std_logic;        -- [out] Error on I2C bus;
  signal scl        : std_logic;        -- [out] I2C Clock
  signal sda_in     : std_logic;        -- [out] Serial output data
  signal master_end : std_logic;        -- [out] Master is done
  signal cnt_rx     : std_logic;        -- [out] Number of bytes recieved
  signal data_fbc   : std_logic_vector(7 downto 0);  -- [out] Data from BC

  signal data       : std_logic_vector(7 downto 0); 

  -- Clock period
  constant PERIOD : time := 25 ns;    

  -- purpose: Write to slave
  procedure write_data (
    constant fec   : in  std_logic_vector(3 downto 0);   -- FEC to address
    constant reg   : in  std_logic_vector(7 downto 0);   -- Register to address
    constant val   : in  std_logic_vector(15 downto 0);  -- Data
    signal   start : out std_logic;                      -- Start it
    signal   stop  : out std_logic;                      -- Stop processing
    signal   write : out std_logic;                      -- Write flag
    signal   byte  : out std_logic_vector(7 downto 0);   -- Byte out
    signal   more  : out std_logic;                      -- More data to come
    signal   done  : in  std_logic;                      -- Master is done
    signal   ready : in  std_logic) is                   -- Next data
  begin  -- write_data
    byte  <= "000" & fec & '0';
    write <= '1';
    stop  <= '0';
    start <= '1';
    more  <= '1';
    wait for 2 * PERIOD;
    more  <= '0';
    start <= '0';

    wait until ready = '1';
    wait for 6 * PERIOD;
    more  <= '1';
    byte <= reg;
    wait for 2.5 * PERIOD;
    more  <= '0';

    wait until ready = '1';
    wait for 6 * PERIOD;
    more  <= '1';
    byte <= val(15 downto 8);
    wait for 2.5 * PERIOD;
    more  <= '0';

    wait until ready = '1';
    wait for 6 * PERIOD;
    more  <= '1';
    byte <= val(7 downto 0);
    wait for 2.5 * PERIOD;
    more  <= '0';

    wait until ready = '1';
    stop <= '1';
    wait for 2 * PERIOD;

    wait until done = '1';
    more  <= '0';
    stop  <= '0';
    write <= '0';
    wait for 2 * PERIOD;

  end write_data;

  -- purpose: Read to slave
  procedure read_data (
    constant fec   : in  std_logic_vector(3 downto 0);  -- FEC to address
    constant reg   : in  std_logic_vector(7 downto 0);  -- Register to address
    signal   start : out std_logic;                     -- Start it
    signal   stop  : out std_logic;                     -- Stop processing
    signal   read  : out std_logic;                     -- Read flag
    signal   write : out std_logic;
    signal   byte  : out std_logic_vector(7 downto 0);  -- Byte out
    signal   more  : out std_logic;                     -- More data to come
    signal   done  : in  std_logic;
    signal   ready : in  std_logic) is                  -- Next data
  begin  -- read_data
    byte  <= "000" & fec & '1';
    read  <= '0';
    write <= '1';
    stop  <= '0';
    start <= '1';
    more  <= '1';                       -- First write word
    wait for 2 * PERIOD;
    more  <= '0';
    start <= '0';

    wait until ready = '1';             -- Got first write
    wait for 6 * PERIOD;
    more  <= '1';                       -- Second write word
    byte <= reg;
    wait for 2.5 * PERIOD;
    more  <= '0';

    wait until ready = '1';             -- Got second write
    wait for 6 * PERIOD;
    write <= '0';
    read  <= '1';
    more  <= '1';                       -- First read
    wait for 2.5 * PERIOD;
    more  <= '0';
    
    wait until ready = '1';             -- Got first read
    wait for 6 * PERIOD;
    more  <= '1';                       -- Second read
    wait for 2.5 * PERIOD;
    more <= '0';
    
    wait until ready = '1';             -- Got second read
    wait for 6 * PERIOD;
    more <= '1';                        -- Stop word
    stop <= '1';
    wait for 2.5 * PERIOD;
    more <= '0';


    wait until done = '1';
    more <= '0';
    stop <= '0';
    read <= '0';
    wait for 2 * PERIOD;

  end read_data;
  
begin  -- test
  clock_master_1 : msm_clock_master
    port map (
      clk40      => clk40,              -- [in]
      reset      => rstb,               -- [in]
      clk_master => clk);               -- [out]
  
  -- component instantiation
  dut: msm_master
    port map (
      clk        => clk,                -- [in]  Clock
      rstb       => rstb,               -- [in]  Async reset
      do_im      => do_im,              -- [in]  data input
      new_data   => new_data,           -- [in]  Got more
      read       => read,               -- [in]  Read flag
      write      => write,              -- [in]  Write flag
      start      => start,              -- [in]  Start condition
      stop       => stop,               -- [in]  Stop condition
      sda_out    => sda_out,            -- [in]  Serial data from BC
      sda_ack    => sda_ack,            -- [in]  Acknowledge on I2C
      ready_seq  => ready_seq,          -- [out] Sequencer ready
      data_valid => data_valid,         -- [out] Data is valid
      error      => error,              -- [out] Error on I2C bus;
      scl        => scl,                -- [out] I2C Clock
      sda_in     => sda_in,             -- [out] Serial output data
      master_end => master_end,         -- [out] Master is done
      cnt_rx     => cnt_rx,             -- [out] Number of bytes recieved
      data_fbc   => data_fbc);          -- [out] Data from BC

  slave_1 : entity msmodule_lib.msm_slave(test2)
    generic map (
      ME     => X"1",
      PERIOD => 200 ns)
    port map (
      scl     => scl,                   -- [in]  Serial clock
      sda_in  => sda_in,                -- [in]  Serial data in
      sda_out => sda_out,               -- [out] Serial data out
      word    => X"dead",               -- [in]  2 output bytes
      data    => data);                 -- [out] Output byte
  
  -- clock generation
  clk40 <= not clk40 after PERIOD / 2;
  -- sda_ack <= sda_out;
  -- sda_out <= sda_in;

  ffd_1 : msm_ffd
    port map (
      clk  => clk40,                    -- [in]  Clock
      arst => rstb,                     -- [in]  Async reset
      d    => sda_out,                  -- [in]  Input
      q    => sda_ack);                 -- [out] Output
  
  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';

    wait for 4 * PERIOD;
    write_data(fec   => X"1",
               reg   => X"0a",
               val   => X"dead",
               start => start,
               stop  => stop,
               write => write,
               more  => new_data,
               byte  => do_im,
               done  => master_end,
               ready => ready_seq);

    wait for 4 * PERIOD;
    read_data(fec   => X"1",
              reg   => X"a0",
              start => start,
              stop  => stop,
              read  => read,
              write => write,
              more  => new_data,
              byte  => do_im,
              done  => master_end,
              ready => ready_seq);

    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
library msmodule_lib;
use msmodule_lib.msm_master_sm_pack.all;
use msmodule_lib.msm_sync_pack.all;
use msmodule_lib.msm_serializer_rcu_pack.all;
use msmodule_lib.msm_clock_master_pack.all;

configuration msm_master_vhdl of msm_master_tb is

  for test
    for dut : msm_master
      use entity msmodule_lib.msm_master(rtl);
      for rtl
        for master_sm_1 : msm_master_sm
          use entity msmodule_lib.msm_master_sm(rtl);
        end for;
        for sync_1 : msm_sync
          use entity msmodule_lib.msm_sync(rtl);
        end for;
        for serializer_rcu_1 : msm_serializer_rcu
          use entity msmodule_lib.msm_serializer_rcu(rtl);
        end for;
      end for;
    end for;
    for clock_master_1 : msm_clock_master
      use entity msmodule_lib.msm_clock_master(rtl);
    end for;
    for ffd_1 : ffd
      use entity msmodule_lib.msm_ffd(rtl);
    end for;
  end for;

end msm_master_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
