-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity msm_serializer_rcu is
  
  port (
    clk       : in  std_logic;                      -- Clock
    rstb      : in  std_logic;                      -- Async reset
    do_im     : in  std_logic_vector(7 downto 0);   -- Data input;
    sda_frcu  : in  std_logic;                      -- Serial data from RCU
    load      : in  std_logic;                      -- Load next byte
    enable    : in  std_logic;                      -- Shift to next
    clear     : in  std_logic;                      -- Clear registers
    cnt_8     : out std_logic;                      -- Got 8 bits
    sda_trcu  : out std_logic;                      -- Serial data to RCU
    data_frcu : out std_logic_vector(7 downto 0));  -- Byte to RCU

end msm_serializer_rcu;

architecture rtl of msm_serializer_rcu is
  signal shiftreg_i : std_logic_vector(7 downto 0);  -- shift register
  signal counter_i  : unsigned(3 downto 0);          -- Counter
begin  -- rtl
  cnt_8     <= '1' when counter_i = "1000" else '0';
  sda_trcu  <= shiftreg_i(7);
  data_frcu <= shiftreg_i;
  
  -- purpose: msm_Count
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, clear, enable
  -- outputs: msm_counter_i
  counter_proc : process (clk, rstb)
  begin  -- process counter_proc
    if rstb = '0' then                  -- asynchronous reset (active low)
      counter_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clear = '1' then
        counter_i <= (others => '0');
      elsif enable = '1' then
        counter_i <= counter_i + "0001";
      end if;
    end if;
  end process counter_proc;
  

  -- purpose: msm_Shift register
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, load, enable, shiftreg_i, sda_frcu
  -- outputs: msm_shiftreg_i
  shift_register: process (clk, rstb)
  begin  -- process shift_register
    if rstb = '0' then                  -- asynchronous reset (active low)
      shiftreg_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if (load = '1') then
        shiftreg_i <= do_im;
      elsif enable = '1' then
        shiftreg_i <= shiftreg_i(6 downto 0) & sda_frcu;
      else
        shiftreg_i <=  shiftreg_i;
      end if;
    end if;
  end process shift_register;
end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_serializer_rcu_pack is

  component msm_serializer_rcu
    port (
      clk       : in  std_logic;                      -- Clock
      rstb      : in  std_logic;                      -- Async reset
      do_im     : in  std_logic_vector(7 downto 0);   -- Data input
      sda_frcu  : in  std_logic;                      -- Serial data from RCU
      load      : in  std_logic;                      -- Load next byte
      enable    : in  std_logic;                      -- Shift to next
      clear     : in  std_logic;                      -- Clear registers
      cnt_8     : out std_logic;                      -- Got 8 bits
      sda_trcu  : out std_logic;                      -- Serial data to RCU
      data_frcu : out std_logic_vector(7 downto 0));  -- Byte to RCU
  end component;

end msm_serializer_rcu_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
