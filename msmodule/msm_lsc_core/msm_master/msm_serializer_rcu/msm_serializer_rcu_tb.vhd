-------------------- Test bench of RCU MS I2C serializer ----------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_serializer_rcu_pack.all;

entity msm_serializer_rcu_tb is

end msm_serializer_rcu_tb;

architecture test of msm_serializer_rcu_tb is

  -- component ports
  signal clk       : std_logic                    := '0';  -- [in]  Clock
  signal rstb      : std_logic                    := '0';  -- [in]  Async reset
  signal do_im     : std_logic_vector(7 downto 0) := (others => '0');  -- [in]
  signal sda_frcu  : std_logic                    := '0';  -- [in]  Serial data
  signal load      : std_logic                    := '0';  -- [in]  Load next
  signal enable    : std_logic                    := '0';  -- [in]  Shift to
  signal clear     : std_logic                    := '0';  -- [in]  Clear
  signal cnt_8     : std_logic;         -- [out] Got 8 bits
  signal sda_trcu  : std_logic;         -- [out] Serial data to RCU
  signal data_frcu : std_logic_vector(7 downto 0);         -- [out] Byte to RCU

  -- Clock period
  constant PERIOD : time := 25 ns;

  -- purpose: msm_Do one cycle
  procedure one_cycle (
    constant input  : in  std_logic_vector(7 downto 0);
    constant serial : in  std_logic_vector(7 downto 0);
    signal   load   : out std_logic;
    signal   enable : out std_logic;    -- clear
    signal   clear  : out std_logic;
    signal   sda_in : out std_logic;
    signal   do_im  : out std_logic_vector(7 downto 0)) is
    variable i : integer;
  begin  -- one_cycle
    do_im <= input;
    wait for 1 * PERIOD;
    load  <= '1';
    clear <= '1';
    wait for 1 * PERIOD;
    load  <= '0';
    clear <= '0';

    for i in 7 downto 0 loop
      wait for 1 * PERIOD;
      sda_in <= serial(i);
      enable <= '1';
      wait for 1 * PERIOD;
      enable <= '0';
      wait for 1 * PERIOD;
    end loop;  -- i
    wait for 1 * PERIOD;
    
  end one_cycle;

begin  -- test

  -- component instantiation
  dut: msm_serializer_rcu
    port map (
      clk       => clk,                 -- [in]  Clock
      rstb      => rstb,                -- [in]  Async reset
      do_im     => do_im,               -- [in]  Data input;
      sda_frcu  => sda_frcu,            -- [in]  Serial data from RCU
      load      => load,                -- [in]  Load next byte
      enable    => enable,              -- [in]  Shift to next
      clear     => clear,               -- [in]  Clear registers
      cnt_8     => cnt_8,               -- [out] Got 8 bits
      sda_trcu  => sda_trcu,            -- [out] Serial data to RCU
      data_frcu => data_frcu);          -- [out] Byte to RCU

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';
    
    wait for 4 * PERIOD;
    one_cycle(input => X"de",
              serial => X"ad",
              load => load,
              enable => enable,
              clear => clear,
              sda_in => sda_frcu,
              do_im => do_im);
    
    wait for 4 * PERIOD;
    one_cycle(input => X"aa",
              serial => X"bb",
              load => load,
              enable => enable,
              clear => clear,
              sda_in => sda_frcu,
              do_im => do_im);
            
            
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_serializer_rcu_vhdl of msm_serializer_rcu_tb is

  for test
    for dut : msm_serializer_rcu
      use entity msmodule_lib. msm_serializer_rcu(rtl);
    end for;
  end for;

end msm_serializer_rcu_vhdl;
-------------------------------------------------------------------------------
--
-- EOF
--
