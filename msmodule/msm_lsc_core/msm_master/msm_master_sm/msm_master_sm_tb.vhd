-------------------- Test bench of RCU MS I2C master state machine ------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library msmodule_lib;
use msmodule_lib.msm_master_sm_pack.all;
use msmodule_lib.msm_slave_pack.all;

entity msm_master_sm_tb is
end msm_master_sm_tb;

architecture test of msm_master_sm_tb is

  -- component ports
  signal clk         : std_logic := '0';  -- [in]  Clock
  signal rstb        : std_logic := '0';  -- [in]  Async reset
  signal new_data    : std_logic := '0';  -- [in]  Got more data
  signal read        : std_logic := '0';  -- [in]  Read flag
  signal write       : std_logic := '0';  -- [in]  Write flag
  signal start       : std_logic := '0';  -- [in]  Send start condition
  signal stop        : std_logic := '0';  -- [in]  Send stop condition
  signal sda_tbc     : std_logic := '0';  -- [in]  Data to send to BC(s)
  signal sda_out     : std_logic := '0';  -- [in]  Data from BC
  signal cnt_8       : std_logic := '0';  -- [in]  Got 8 bits
  signal load        : std_logic;         -- [out] Load next in serializer
  signal enable      : std_logic;         -- [out] Enable serializer
  signal clear       : std_logic;         -- [out] Clear serializer
  signal data_valid  : std_logic;         -- [out] Bit ok
  signal error       : std_logic;         -- [out] Error on bus
  signal ready       : std_logic;         -- [out] Ready for more
  signal scl         : std_logic;         -- [out] Output clock
  signal sda_in      : std_logic;         -- [out] Output serial data
  signal sel_sda_out : std_logic;         -- [out] Select input
  signal cnt_rx      : std_logic;         -- [out] Enable RX counter
  signal master_end  : std_logic;         -- [out] At end of sequence

  signal counter_i  : unsigned(3 downto 0)          := X"0";
  signal shiftreg_i : std_logic_vector(7 downto 0)  := X"FF";
  signal data       : std_logic_vector(7 downto 0);
  signal byte       : std_logic_vector(7 downto 0)  := X"00";
  signal enable_tmp : std_logic;
  signal ret        : std_logic_vector(15 downto 0) := (others => '0');
  
  -- Clock period
  constant PERIOD : time := 50 ns;      -- NB: msm_Slower clock

  -- purpose: msm_Write to slave
  procedure write_data (
    constant fec   : in  std_logic_vector(3 downto 0);   -- FEC to address
    constant reg   : in  std_logic_vector(7 downto 0);   -- Register to address
    constant val   : in  std_logic_vector(15 downto 0);  -- Data
    signal   start : out std_logic;                      -- Start it
    signal   stop  : out std_logic;                      -- Stop processing
    signal   write : out std_logic;                      -- Write flag
    signal   byte  : out std_logic_vector(7 downto 0);   -- Byte out
    signal   more  : out std_logic;                      -- More data to come
    signal   done  : in  std_logic;                      -- Master is done
    signal   ready : in  std_logic) is                   -- Next data
  begin  -- write_data
    byte  <= "000" & fec & '0';
    write <= '1';
    stop  <= '0';
    start <= '1';
    more  <= '1';
    wait for 2 * PERIOD;
    more  <= '0';
    start <= '0';

    wait until ready = '1';
    wait for 6 * PERIOD;
    more  <= '1';
    byte <= reg;
    wait for 2.5 * PERIOD;
    more  <= '0';

    wait until ready = '1';
    wait for 6 * PERIOD;
    more  <= '1';
    byte <= val(15 downto 8);
    wait for 2.5 * PERIOD;
    more  <= '0';

    wait until ready = '1';
    wait for 6 * PERIOD;
    more  <= '1';
    byte <= val(7 downto 0);
    wait for 2.5 * PERIOD;
    more  <= '0';

    wait until ready = '1';
    stop <= '1';
    wait for 2 * PERIOD;

    wait until done = '1';
    more  <= '0';
    stop  <= '0';
    write <= '0';
    wait for 2 * PERIOD;

  end write_data;

  -- purpose: msm_Read to slave
  procedure read_data (
    constant fec   : in  std_logic_vector(3 downto 0);  -- FEC to address
    constant reg   : in  std_logic_vector(7 downto 0);  -- Register to address
    signal   start : out std_logic;                     -- Start it
    signal   stop  : out std_logic;                     -- Stop processing
    signal   read  : out std_logic;                     -- Read flag
    signal   write : out std_logic;
    signal   byte  : out std_logic_vector(7 downto 0);  -- Byte out
    signal   more  : out std_logic;                     -- More data to come
    signal   done  : in  std_logic;
    signal   ready : in  std_logic) is                  -- Next data
  begin  -- read_data
    byte  <= "000" & fec & '1';
    read  <= '0';
    write <= '1';
    stop  <= '0';
    start <= '1';
    more  <= '1';                       -- First write word
    wait for 2 * PERIOD;
    more  <= '0';
    start <= '0';

    wait until ready = '1';             -- Got first write
    wait for 6 * PERIOD;
    more  <= '1';                       -- Second write word
    byte <= reg;
    wait for 2.5 * PERIOD;
    more  <= '0';

    wait until ready = '1';             -- Got second write
    wait for 6 * PERIOD;
    write <= '0';
    read  <= '1';
    more  <= '1';                       -- First read
    wait for 2.5 * PERIOD;
    more  <= '0';
    
    wait until ready = '1';             -- Got first read
    wait for 6 * PERIOD;
    more  <= '1';                       -- Second read
    wait for 2.5 * PERIOD;
    more <= '0';
    
    wait until ready = '1';             -- Got second read
    wait for 6 * PERIOD;
    more <= '1';                        -- Stop word
    stop <= '1';
    wait for 2.5 * PERIOD;
    more <= '0';


    wait until done = '1';
    more <= '0';
    stop <= '0';
    read <= '0';
    wait for 2 * PERIOD;

  end read_data;

  
begin  -- test
  slave_1 : entity msmodule_lib.msm_slave(test2)
    generic map (
      ME => X"1",
      PERIOD => 8 * PERIOD)
    port map (
      scl     => scl,                   -- [in]  Serial clock
      sda_in  => sda_in,                -- [in]  Serial data in
      sda_out => sda_out,               -- [out] Serial data out
      word    => X"dead",               -- [in]  2 output bytes
      data    => data);                 -- [out] Output byte

  -- component instantiation
  dut : msm_master_sm
    port map (
      clk         => clk,               -- [in]  Clock
      rstb        => rstb,              -- [in]  Async reset
      new_data    => new_data,          -- [in]  Got more data
      read        => read,              -- [in]  Read flag
      write       => write,             -- [in]  Write flag
      start       => start,             -- [in]  Send start condition
      stop        => stop,              -- [in]  Send stop condition
      sda_tbc     => sda_tbc,           -- [in]  Data to send to BC(s)
      sda_out     => sda_out,           -- [in]  Data from BC
      cnt_8       => cnt_8,             -- [in]  Got 8 bits
      load        => load,              -- [out] Load next in serializer
      enable      => enable,            -- [out] Enable serializer
      clear       => clear,             -- [out] Clear serializer
      data_valid  => data_valid,        -- [out] Bit ok
      error       => error,             -- [out] Error on bus
      ready       => ready,             -- [out] Ready for more
      scl         => scl,               -- [out] Output clock
      sda_in      => sda_in,            -- [out] Output serial data
      sel_sda_out => sel_sda_out,       -- [out] Select input
      cnt_rx      => cnt_rx,            -- [out] Enable RX counter
      master_end  => master_end);       -- [out] At end of sequence

  cnt_8     <= '1' when counter_i = "1000" else '0';
  sda_tbc   <= shiftreg_i(7);

  -- purpose: msm_Count
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, clear, enable
  -- outputs: msm_counter_i
  counter_proc : process (clk, rstb)
  begin  -- process counter_proc
    if rstb = '0' then                  -- asynchronous reset (active low)
      counter_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clear = '1' then
        counter_i <= (others => '0');
      elsif enable = '1' then
        counter_i <= counter_i + "0001";
      end if;
    end if;
  end process counter_proc;


  -- purpose: msm_Shift register
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, load, enable, shiftreg_i, sda_frcu
  -- outputs: msm_shiftreg_i
  shift_register : process (clk, rstb)
  begin  -- process shift_register
    if rstb = '0' then                  -- asynchronous reset (active low)
      shiftreg_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if (load = '1') then
        shiftreg_i <= byte;
      elsif clear = '1' then
        shiftreg_i <= (others => '0');
      elsif enable = '1' then
        shiftreg_i <= shiftreg_i(6 downto 0) & sda_out;
      else
        shiftreg_i <= shiftreg_i;
      end if;
    end if;
  end process shift_register;

  -- purpose: msm_Store return value
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, ready, cnt_rx
  -- outputs: msm_ret
  make_return: process (clk, rstb)
  begin  -- process make_return
    if rstb = '0' then                  -- asynchronous reset (active low)
      ret <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if data_valid = '1' then
        if cnt_rx = '0' then
          ret(15 downto 8) <= shiftreg_i;
        else
          ret(7 downto 0) <= shiftreg_i;
        end if;
      end if;
    end if;
  end process make_return;
  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim : process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';

    wait for 4 * PERIOD;
    write_data(fec   => X"1",
               reg   => X"0a",
               val   => X"dead",
               start => start,
               stop  => stop,
               write => write,
               more  => new_data,
               byte  => byte,
               done  => master_end,
               ready => ready);

    wait for 4 * PERIOD;
    read_data(fec   => X"1",
              reg   => X"a0",
              start => start,
              stop  => stop,
              read  => read,
              write => write,
              more  => new_data,
              byte  => byte,
              done  => master_end,
              ready => ready);

    wait;                               -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_master_sm_vhdl of msm_master_sm_tb is

  for test
    for dut : msm_master_sm
      use entity msmodule_lib.msm_master_sm(rtl);
    end for;
  end for;

end msm_master_sm_vhdl;


-------------------------------------------------------------------------------
--
-- EOF
--
