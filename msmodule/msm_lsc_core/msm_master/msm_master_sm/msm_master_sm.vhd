-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-------------------------------------------------------------------------------
-- RCU master state machine: msm_tx and rx
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity msm_master_sm is
  
  port (
    clk         : in  std_logic;        -- Clock
    rstb        : in  std_logic;        -- Async reset
    new_data    : in  std_logic;        -- Got more data
    read        : in  std_logic;        -- Read flag
    write       : in  std_logic;        -- Write flag
    start       : in  std_logic;        -- Send start condition
    stop        : in  std_logic;        -- Send stop condition
    sda_tbc     : in  std_logic;        -- Data to send to BC(s)
    sda_out     : in  std_logic;        -- Data from BC
    cnt_8       : in  std_logic;        -- Got 8 bits
    load        : out std_logic;        -- Load next in serializer
    enable      : out std_logic;        -- Enable serializer
    clear       : out std_logic;        -- Clear serializer
    data_valid  : out std_logic;        -- Bit ok
    error       : out std_logic;        -- Error on bus
    ready       : out std_logic;        -- Ready for more
    scl         : out std_logic;        -- Output clock
    sda_in      : out std_logic;        -- Output serial data
    sel_sda_out : out std_logic;        -- Select input
    cnt_rx      : out std_logic;        -- Enable RX counter
    master_end  : out std_logic);       -- At end of sequence

end msm_master_sm;

architecture rtl of msm_master_sm is

  type st_t is (idle,
                start_1,                -- Start conditions
                start_2,                -- Start conditions
                latch_1,                -- Put data to slave
                latch_2,                -- Put data to slave
                latch_3,                -- Put data to slave
                latch_4,                -- Put data to slave 
                ack_1,                  -- Get slave acknowledge
                ack_2,                  -- Get slave acknowledge
                ack_3,                  -- Get slave acknowledge 
                stop_1,                 -- Send stop condition
                stop_2,                 -- Send stop condition
                stop_3,                 -- Send stop condition
                latch_data_1,           -- Get slave data
                latch_data_2,           -- Get slave data
                latch_data_3,           -- Get slave data
                latch_data_4,           -- Get slave data
                ack_m_1,                -- master acknowledge
                ack_m_2,                -- master acknowledge
                ack_m_3,                -- master acknowledge
                ack_m_4,                -- master acknowledge
                n_ack_m_2,              -- master no-acknowledge
                n_ack_m_3,              -- master no-acknowledge
                n_ack_m_4,              -- master no-acknowledge
                nack_1,                 -- No acknowledge from slave
                nack_2,                 -- No acknowledge from slave
                nack_3,                 -- No acknowledge from slave
                wait_new_data);         -- State type

  signal st_i    : st_t := idle;        -- State
  signal nx_st_i : st_t := idle;        -- Next state

  signal en_cnt_rx_i  : std_logic;
  signal cnt_rx_i     : unsigned(0 downto 0);
  signal en_cnt_tx_i  : std_logic;
  signal cnt_tx_i     : unsigned(1 downto 0);
  signal error_i      : std_logic;
  signal sel_sda_i    : std_logic_vector(1 downto 0);
  signal sda_master_i : std_logic;
  signal scl_i        : std_logic;

  constant sel_master : std_logic_vector(1 downto 0) := "00";
  constant sel_ser    : std_logic_vector(1 downto 0) := "01";
  constant sel_slave  : std_logic_vector(1 downto 0) := "10";
  
begin  -- rtl
  master_end <= '1' when (st_i = idle) else '0';
  error      <= error_i;
  cnt_rx     <= cnt_rx_i(0);

  -- purpose: Next state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st_i
  -- outputs: st_i
  next_state : process (clk, rstb)
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;

  -- purpose: state machine.
  -- type   : combinational
  -- inputs : st_i, start, stop, cnt_8, read, write, new_data, cnt_tx_i, error
  -- outputs: clear, data_valid, en_cnt_tx_i, en_cnt_rx_i, enable, load, ready,
  --          scl_i, sda_master_i, sel_sda_i, nx_st_i
  fsm : process (st_i,
                 start,
                 stop,
                 cnt_8,
                 read,
                 write,
                 new_data,
                 cnt_tx_i,
                 error_i)
  begin  -- process fsm
    case st_i is
      when idle =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';            -- Clock up
        sda_master_i <= '1';            -- Data up
        sel_sda_i    <= sel_master;
        if start = '1' then
          nx_st_i <= start_1;
        else
          nx_st_i <= idle;
        end if;

      when start_1 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';            -- Clock up
        sda_master_i <= '0';            -- Data down
        sel_sda_i    <= sel_master;
        nx_st_i      <= start_2;
        
      when start_2 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        ready        <= '0';
        scl_i        <= '0';            -- Clock down (1)
        sda_master_i <= '0';            
        sel_sda_i    <= sel_master;     -- Select master
        if start = '0' then
          enable  <= '1';               -- Enable serial out
          load    <= '1';               -- Load next data word
          nx_st_i <= latch_1;
        else
          enable  <= '0';
          load    <= '0';
          nx_st_i <= start_2;
        end if;

      when latch_1 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '0';            -- Clock down (2)
        sda_master_i <= '1';            
        sel_sda_i    <= sel_ser;        -- Select data 
        if error_i = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= latch_2;
        end if;
        
      when latch_2 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';            -- Clock up (1)
        sda_master_i <= '1';            -- 
        sel_sda_i    <= sel_ser;        -- Select data 
        nx_st_i      <= latch_3;

      when latch_3 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';            -- Clock up (2)
        sda_master_i <= '1';            --
        sel_sda_i    <= sel_ser;        -- Select data
        nx_st_i      <= latch_4;

      when latch_4 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '1';
        load         <= '0';
        scl_i        <= '0';            -- Clock down (1)
        sda_master_i <= '1';            -- Us 
        if cnt_8 = '1' then             -- Got 1 byte
          ready     <= '1';             -- Ready for more
          sel_sda_i <= sel_slave;       -- Select input from slave
          if cnt_tx_i = "11" then       -- Send a total of 3 bytes
            nx_st_i <= nack_1;          -- Send no-acknowledge
          else
            nx_st_i <= ack_1;           -- Wait for an acknowledge
          end if;
        else
          ready     <= '0';
          sel_sda_i <= sel_ser;         -- Select data 
          nx_st_i   <= latch_1;         -- More bits
        end if;

      when ack_1 =>
        clear        <= '1';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '0';            -- Clock down (2)
        sda_master_i <= '1';
        sel_sda_i    <= sel_slave;      -- Select slave 
        nx_st_i      <= ack_2;

      when ack_2 =>
        clear        <= '1';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';            -- Clock up (1)
        sda_master_i <= '1';            -- 
        sel_sda_i    <= sel_slave;      -- Select slave 
        nx_st_i      <= ack_3;

      when ack_3 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';            -- Clock up (2)
        sda_master_i <= '1';
        sel_sda_i    <= sel_slave;      -- Select slave
        nx_st_i      <= stop_1;

      when nack_1 =>
        clear        <= '1';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '0';
        sda_master_i <= '0';
        sel_sda_i    <= sel_slave;
        nx_st_i      <= nack_2;

      when nack_2 =>
        clear        <= '1';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '0';
        sel_sda_i    <= sel_slave;
        nx_st_i      <= nack_3;

      when nack_3 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '0';
        sel_sda_i    <= sel_slave;
        nx_st_i      <= stop_1;

      when stop_1 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_rx_i  <= '0';
        ready        <= '0';
        sda_master_i <= '1';
        if error_i = '1' then
          en_cnt_tx_i <= '0';
          enable      <= '0';
          load        <= '0';
          sel_sda_i   <= sel_master;
          scl_i       <= '1';
          nx_st_i     <= idle;
        elsif stop = '1' then           -- Send stop condition
          en_cnt_tx_i <= '0';           -- No TX counter
          enable      <= '0';           -- Do not enable serial shift
          load        <= '0';           -- Keep current serial
          sel_sda_i   <= sel_master;    -- Select us 
          scl_i       <= '1';           -- Clock high (3)
          nx_st_i     <= stop_2;        -- Send stop 
        elsif write = '1' and new_data = '1' then
          en_cnt_tx_i <= '1';           -- Enable TX counter
          enable      <= '1';           -- Enable serial shift 
          load        <= '1';           -- Load next word 
          sel_sda_i   <= sel_ser;       -- Select daa
          scl_i       <= '0';           -- Clock down (1)
          nx_st_i     <= latch_1;
        elsif read = '1' and new_data = '1' then
          en_cnt_tx_i <= '0';           -- No TX counter
          enable      <= '0';           -- No enable serial shift
          load        <= '0';           -- Keep word 
          sel_sda_i   <= sel_slave;     -- Select slave data 
          scl_i       <= '0';           -- Clock down (1)
          nx_st_i     <= latch_data_1;  -- Read from slave
        else
          en_cnt_tx_i <= '0';
          enable      <= '0';
          load        <= '0';
          sel_sda_i   <= sel_slave;
          scl_i       <= '0';           -- Clock down (1)
          nx_st_i     <= stop_1;
        end if;

      when stop_2 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '0';            -- Clock low (1)
        sda_master_i <= '0';
        sel_sda_i    <= sel_master;
        if error_i = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= stop_3;
        end if;

      when stop_3 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';            -- Clock high 
        sda_master_i <= '0';
        sel_sda_i    <= sel_master;
        nx_st_i      <= idle;           -- And go back 
        
      when latch_data_1 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '1';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '0';
        sda_master_i <= '1';
        sel_sda_i    <= sel_slave;
        if error_i = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= latch_data_2;
        end if;

      when latch_data_2 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '1';
        sel_sda_i    <= sel_slave;
        nx_st_i      <= latch_data_3;
        
      when latch_data_3 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '1';
        sel_sda_i    <= sel_slave;
        nx_st_i      <= latch_data_4;

      when latch_data_4 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        scl_i        <= '0';
        sda_master_i <= '1';
        sel_sda_i    <= sel_slave;
        if cnt_8 = '0' then
          ready   <= '0';
          nx_st_i <= latch_data_1;
        else
          ready   <= '1';
          nx_st_i <= ack_m_1;           -- Master acknowledge
        end if;

      when ack_m_1 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '1';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '0';
        sda_master_i <= '1';
        sel_sda_i    <= sel_slave;
        nx_st_i      <= wait_new_data;

      when ack_m_2 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '0';
        sel_sda_i    <= sel_master;
        nx_st_i      <= ack_m_3;
        
      when ack_m_3 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '0';
        sel_sda_i    <= sel_master;
        nx_st_i      <= ack_m_4;

      when ack_m_4 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '0';
        sda_master_i <= '0';
        sel_sda_i    <= sel_master;
        nx_st_i      <= latch_data_1;
        
      when n_ack_m_2 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '1';
        sel_sda_i    <= sel_master;
        nx_st_i      <= n_ack_m_3;
        
      when n_ack_m_3 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '1';
        sel_sda_i    <= sel_master;
        nx_st_i      <= n_ack_m_4;

      when n_ack_m_4 =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '0';
        sda_master_i <= '1';
        sel_sda_i    <= sel_master;
        nx_st_i      <= stop_2;
        
      when wait_new_data =>
        clear       <= '1';
        data_valid  <= '1';
        en_cnt_tx_i <= '0';
        enable      <= '0';
        load        <= '0';
        ready       <= '0';
        scl_i       <= '0';
        sel_sda_i   <= sel_master;
        if stop = '1' and new_data = '1' then
          en_cnt_rx_i  <= '0';
          sda_master_i <= '1';
          nx_st_i      <= n_ack_m_2;    -- No acknowledge from master
        elsif stop = '0' and new_data = '1' then
          en_cnt_rx_i  <= '1';
          sda_master_i <= '0';
          nx_st_i      <= ack_m_2;      -- Acknowledge from master
        else
          en_cnt_rx_i  <= '0';
          sda_master_i <= '1';
          nx_st_i      <= wait_new_data;
        end if;

      when others =>
        clear        <= '0';
        data_valid   <= '0';
        en_cnt_tx_i  <= '0';
        en_cnt_rx_i  <= '0';
        enable       <= '0';
        load         <= '0';
        ready        <= '0';
        scl_i        <= '1';
        sda_master_i <= '1';
        sel_sda_i    <= sel_master;
        nx_st_i      <= idle;
        
    end case;
  end process fsm;


  -- purpose: Error handling
  -- type   : sequential
  -- inputs : clk, rstb, nx_st, sda_out
  -- outputs: error
  errors : process (clk, rstb)
  begin  -- process errors
    if rstb = '0' then                  -- asynchronous reset (active low)
      error_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if (nx_st_i = stop_1) and sda_out /= '0' then
        error_i <= '1';
      else
        error_i <= '0';
      end if;
    end if;
  end process errors;

  -- purpose: This counter controls if the recieved data correspond to first
  --          or send byte
  -- type   : sequential
  -- inputs : clk, rstb, start
  -- outputs: cnt_rx_i
  recieve_counter : process (clk, rstb, start)
  begin  -- process recieve_counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_rx_i <= "0";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if start = '1' then
        cnt_rx_i <= "0";
      elsif en_cnt_rx_i = '1' then
        cnt_rx_i <= cnt_rx_i + "1";
      end if;
    end if;
  end process recieve_counter;

  -- purpose: This counter controls the number of words to transfer
  -- type   : sequential
  -- inputs : clk, rstb, start
  -- outputs: cnt_tx_i
  transmit_counter : process (clk, rstb)
  begin  -- process transmit_counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_tx_i <= "00";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if start = '1' then
        cnt_tx_i <= "00";
      elsif en_cnt_tx_i = '1' then
        cnt_tx_i <= cnt_tx_i + "01";
      end if;
    end if;
  end process transmit_counter;

  -- purpose: msm_Output signals
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, sda_master_i, sel_sda_i, scl_i
  -- outputs: msm_sel_sda_out, sda_in, scl
  output : process (clk, rstb)
  begin  -- process output
    if rstb = '0' then                  -- asynchronous reset (active low)
      sel_sda_out <= '0';
      sda_in      <= '1';
      scl         <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge

      if sel_sda_i = sel_master then
        sel_sda_out <= '0';
        sda_in      <= sda_master_i;
        scl         <= scl_i;
      elsif sel_sda_i = sel_ser then
        sel_sda_out <= '0';
        sda_in      <= sda_tbc;
        scl         <= scl_i;
      elsif sel_sda_i = sel_slave then
        sel_sda_out <= '1';
        sda_in      <= sda_master_i;
        scl         <= scl_i;
        
      end if;
    end if;
  end process output;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_master_sm_pack is
  component msm_master_sm
    port (
      clk         : in  std_logic;      -- Clock
      rstb        : in  std_logic;      -- Async reset
      new_data    : in  std_logic;      -- Got more data
      read        : in  std_logic;      -- Read flag
      write       : in  std_logic;      -- Write flag
      start       : in  std_logic;      -- Send start condition
      stop        : in  std_logic;      -- Send stop condition
      sda_tbc     : in  std_logic;      -- Data to send to BC(s)
      sda_out     : in  std_logic;      -- Data from BC
      cnt_8       : in  std_logic;      -- Got 8 bits
      load        : out std_logic;      -- Load next in serializer
      enable      : out std_logic;      -- Enable serializer
      clear       : out std_logic;      -- Clear serializer
      data_valid  : out std_logic;      -- Bit ok
      error       : out std_logic;      -- Error on bus
      ready       : out std_logic;      -- Ready for more
      scl         : out std_logic;      -- Output clock
      sda_in      : out std_logic;      -- Output serial data
      sel_sda_out : out std_logic;      -- Select input
      cnt_rx      : out std_logic;      -- Enable RX counter
      master_end  : out std_logic);     -- At end of sequence
  end component;
end msm_master_sm_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
