-------------------------------------------------------------------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_serializer_rcu_pack.all;
use msmodule_lib.msm_master_sm_pack.all;
use msmodule_lib.msm_sync_pack.all;

entity msm_master is
  
  port (
    clk        : in  std_logic;         -- Clock
    rstb       : in  std_logic;         -- Async reset
    do_im      : in  std_logic_vector(7 downto 0);   -- data input
    new_data   : in  std_logic;         -- Got more
    read       : in  std_logic;         -- Read flag
    write      : in  std_logic;         -- Write flag
    start      : in  std_logic;         -- Start condition
    stop       : in  std_logic;         -- Stop condition
    sda_out    : in  std_logic;         -- Serial data from BC
    sda_ack    : in  std_logic;         -- Acknowledge on I2C
    ready_seq  : out std_logic;         -- Sequencer ready
    data_valid : out std_logic;         -- Data is valid
    error      : out std_logic;         -- Error on I2C bus;
    scl        : out std_logic;         -- I2C Clock
    sda_in     : out std_logic;         -- Serial output data
    master_end : out std_logic;         -- Master is done
    cnt_rx     : out std_logic;         -- Number of bytes recieved
    data_fbc   : out std_logic_vector(7 downto 0));  -- Data from BC

end msm_master;

architecture rtl of msm_master is
  signal sda_fbc_i      : std_logic;    -- Serial data from BC
  signal sda_tbc_i      : std_logic;    -- Serial data to BC
  signal load_i         : std_logic;
  signal enable_i       : std_logic;
  signal clear_i        : std_logic;
  signal cnt_8_i        : std_logic;
  signal ready_master_i : std_logic;
  signal sel_sda_out_i  : std_logic;
  -- signal sda_tbc_i : std_logic;
  -- signal sda_fbc_i : std_logic;

begin  -- rtl

  serializer_rcu_1 : msm_serializer_rcu
    port map (
      clk       => clk,                 -- [in]  Clock
      rstb      => rstb,                -- [in]  Async reset
      do_im     => do_im,               -- [in]  Data input
      sda_frcu  => sda_fbc_i,           -- [in]  Serial data from RCU
      load      => load_i,              -- [in]  Load next byte
      enable    => enable_i,            -- [in]  Shift to next
      clear     => clear_i,             -- [in]  Clear registers
      cnt_8     => cnt_8_i,             -- [out] Got 8 bits
      sda_trcu  => sda_tbc_i,           -- [out] Serial data to RCU
      data_frcu => data_fbc);           -- [out] Byte to RCU

  master_sm_1 : msm_master_sm
    port map (
      clk         => clk,               -- [in]  Clock
      rstb        => rstb,              -- [in]  Async reset
      new_data    => new_data,          -- [in]  Got more data
      read        => read,              --       Read flag
      write       => write,             -- [in]  Write flag
      start       => start,             -- [in]  Send start condition
      stop        => stop,              -- [in]  Send stop condition
      sda_tbc     => sda_tbc_i,         -- [in]  Data to send to BC(s)
      sda_out     => sda_ack,           -- [in]  Data from BC
      cnt_8       => cnt_8_i,           -- [in]  Got 8 bits
      load        => load_i,            -- [out] Load next in serializer
      enable      => enable_i,          -- [out] Enable serializer
      clear       => clear_i,           -- [out] Clear serializer
      data_valid  => data_valid,        -- [out] Bit ok
      error       => error,             -- [out] Error on bus
      ready       => ready_master_i,    -- [out] Ready for more
      scl         => scl,               -- [out] Output clock
      sda_in      => sda_in,            -- [out] Output serial data
      sel_sda_out => sel_sda_out_i,     -- [out] Select input
      cnt_rx      => cnt_rx,            -- [out] Enable RX counter
      master_end  => master_end);       -- [out] At end of sequence

  sync_1 : msm_sync
    port map (
      clk    => clk,                    -- [in]  Clock
      rstb   => rstb,                   -- [in]  Async reset
      input  => ready_master_i,         -- [in]  Input
      output => ready_seq);             -- [out] Output

  sda_fbc_i <= sda_out when sel_sda_out_i = '1' else 'Z';
  -- bufif1 (sda_fbc_i, sda_out, sel_sda_out_i)

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_master_pack is

  component msm_master
    port (
      clk        : in  std_logic;       -- Clock
      rstb       : in  std_logic;       -- Async reset
      do_im      : in  std_logic_vector(7 downto 0);   -- data input
      new_data   : in  std_logic;       -- Got more
      read       : in  std_logic;       -- Read flag
      write      : in  std_logic;       -- Write flag
      start      : in  std_logic;       -- Start condition
      stop       : in  std_logic;       -- Stop condition
      sda_out    : in  std_logic;       -- Serial data from BC
      sda_ack    : in  std_logic;       -- Acknowledge on I2C
      ready_seq  : out std_logic;       -- Sequencer ready
      data_valid : out std_logic;       -- Data is valid
      error      : out std_logic;       -- Error on I2C bus;
      scl        : out std_logic;       -- I2C Clock
      sda_in     : out std_logic;       -- Serial output data
      master_end : out std_logic;       -- Master is done
      cnt_rx     : out std_logic;       -- Number of bytes recieved
      data_fbc   : out std_logic_vector(7 downto 0));  -- Data from BC
  end component;

end msm_master_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
