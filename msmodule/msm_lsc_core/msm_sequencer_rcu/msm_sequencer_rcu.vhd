-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- State machine of sequencer
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity msm_sequencer_rcu is
  
  port (
    clk         : in  std_logic;        -- Clock
    rstb        : in  std_logic;        -- Async reset
    data_valid  : in  std_logic;        -- Input is valid
    icode       : in  std_logic_vector(1 downto 0);  -- From instruction bulder
    error       : in  std_logic;        -- Error flag
    ready       : in  std_logic;        -- Ready for more
    exec        : in  std_logic;        -- Execute flag
    cnt_rx      : in  std_logic;        -- Counter of recieved words
    data_fbc    : in  std_logic_vector(7 downto 0);  -- Byte from BC
    ih_busy     : in  std_logic;        -- Interrupt handler busy
    new_data    : out std_logic;        -- Got new data
    read        : out std_logic;        -- Read flag
    write       : out std_logic;        -- Write flag
    start       : out std_logic;        -- Make start condition
    stop        : out std_logic;        -- Make stop condition
    seq_active  : out std_logic;        -- Sequencer is active
    add_ib      : out std_logic_vector(2 downto 0);  -- Address to Instr. build.
    we_result   : out std_logic;        -- Write enable for result reg.
    data_result : out std_logic_vector(15 downto 0));  -- Data for result reg.

end msm_sequencer_rcu;

architecture rtl of msm_sequencer_rcu is
  type st_t is (idle,
                read_im,
                load_data,
                wait_ready,
                inc_cnt_ib,
                wait_instr,
                end_instr);             -- State type

  signal st_i         : st_t;                  -- State
  signal nx_st_i      : st_t;                  -- Next state
  signal start_i      : std_logic;
  signal stop_i       : std_logic;
  signal cnt_en_ib_i  : std_logic;
  signal reset_add_i  : std_logic;
  signal seq_active_i : std_logic;
  signal add_ib_i     : unsigned(2 downto 0);  -- Address to Instr. build.

  constant seq_start : std_logic_vector(1 downto 0) := "10";
  constant seq_stop  : std_logic_vector(1 downto 0) := "00";
  constant seq_write : std_logic_vector(1 downto 0) := "11";
  constant seq_read  : std_logic_vector(1 downto 0) := "01";
  
begin  -- rtl
  start_i      <= '1' when icode = seq_start and seq_active_i = '1' else '0';
  stop_i       <= '1' when icode = seq_stop  else '0';
  read         <= '1' when icode = seq_read  else '0';
  write        <= '1' when icode = seq_write else '0';
  seq_active_i <= '1' when not (st_i = idle) else '0';
  start        <= start_i;
  stop         <= stop_i;
  seq_active   <= seq_active_i;
  add_ib       <= std_logic_vector(add_ib_i);
  
  -- purpose: msm_Update next state
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, nx_st_i
  -- outputs: msm_st_i
  next_state : process (clk, rstb)
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;

  -- purpose: msm_State machine
  -- type   : msm_combinational
  -- inputs : msm_st_i, exec, ready, data_valid, error, start_i, stop_i
  -- outputs: msm_cnt_en_ib, reset_add, new_data, nx_st_i
  fsm : process (st_i, exec, ready, data_valid, error, start_i, stop_i)
  begin  -- process fsm

    case st_i is
      when idle =>
        cnt_en_ib_i <= '0';
        reset_add_i <= '0';
        new_data    <= '0';
        if exec = '1' then
          nx_st_i <= read_im;
        else
          nx_st_i <= idle;
        end if;
        
      when read_im =>
        cnt_en_ib_i <= '0';
        reset_add_i <= '1';
        new_data    <= '0';
        if error = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= load_data;
        end if;
        
      when load_data =>
        cnt_en_ib_i <= '0';
        reset_add_i <= '0';
        new_data    <= '1';
        if error = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= wait_ready;
        end if;
        
      when wait_ready =>
        cnt_en_ib_i <= '0';
        reset_add_i <= '0';
        new_data    <= '0';
        if error = '1' then
          nx_st_i <= idle;
        elsif start_i = '1' then
          nx_st_i <= inc_cnt_ib;
        elsif ready = '1' then          -- ready comes from master if it has
                                        -- finished a cycle
          nx_st_i <= inc_cnt_ib;
        else
          nx_st_i <= wait_ready;
        end if;
        
      when inc_cnt_ib =>
        cnt_en_ib_i <= '1';
        reset_add_i <= '0';
        new_data    <= '0';
        if error = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= wait_instr;
        end if;
        
      when wait_instr =>
        cnt_en_ib_i <= '0';
        reset_add_i <= '0';
        new_data    <= '0';
        if error = '1' then
          nx_st_i <= idle;
        else
          nx_st_i <= end_instr;
        end if;
        
      when end_instr =>
        cnt_en_ib_i <= '0';
        reset_add_i <= '0';
        if error = '1' then
          new_data <= '0';
          nx_st_i  <= idle;
        elsif stop_i = '0' then
          new_data <= '0';
          nx_st_i  <= load_data;
        else
          new_data <= '1';
          nx_st_i  <= idle;
        end if;
        
      when others =>
        cnt_en_ib_i <= '0';
        reset_add_i <= '0';
        new_data    <= '0';
        nx_st_i     <= idle;
    end case;
  end process fsm;

  -- purpose: msm_Counter for address to instruction builder
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, seq_active_i, reset_add_i, cnt_en_ib_i
  -- outputs: msm_add_ib_i
  counter_for_ib : process (clk, rstb)
  begin  -- process counter_for_ib
    if rstb = '0' then                  -- asynchronous reset (active low)
      add_ib_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
        if seq_active_i = '0' then
          add_ib_i <= (others => '0');
        elsif reset_add_i = '1' then
          add_ib_i <= (others => '0');
        elsif cnt_en_ib_i = '1' then
          add_ib_i <= add_ib_i + "001";
        end if;
      end if;
  end process counter_for_ib;


  -- purpose: msm_Data and write enable for result register
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, data_valid, cnt_rx, data_fbc
  -- outputs: msm_data_result, we_result
  result_output : process (clk, rstb)
  begin  -- process result_output
    if rstb = '0' then                  -- asynchronous reset (active low)
      we_result   <= '0';
      data_result <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if (data_valid = '1' and cnt_rx = '0') then
        we_result                <= '0';
        data_result(15 downto 8) <= data_fbc;
      elsif (data_valid = '1' and cnt_rx = '1') then
        data_result(7 downto 0) <= data_fbc;
        if ih_busy = '1' then
          we_result <= '0';
        else
          we_result <= '1';
        end if;
      else
        we_result <= '0';
      end if;
    end if;
  end process result_output;
end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_sequencer_rcu_pack is

  component msm_sequencer_rcu
    port (
      clk         : in  std_logic;      -- Clock
      rstb        : in  std_logic;      -- Async reset
      data_valid  : in  std_logic;      -- Input is valid
      icode       : in  std_logic_vector(1 downto 0);  -- From instr builder
      error       : in  std_logic;      -- Error flag
      ready       : in  std_logic;      -- Ready for more
      exec        : in  std_logic;      -- Execute flag
      cnt_rx      : in  std_logic;      -- Counter of recieved words
      data_fbc    : in  std_logic_vector(7 downto 0);  -- Byte from BC
      ih_busy     : in  std_logic;      -- Interrupt handler busy
      new_data    : out std_logic;      -- Got new data
      read        : out std_logic;      -- Read flag
      write       : out std_logic;      -- Write flag
      start       : out std_logic;      -- Make start condition
      stop        : out std_logic;      -- Make stop condition
      seq_active  : out std_logic;      -- Sequencer is active
      add_ib      : out std_logic_vector(2 downto 0);  -- Address to Instr build
      we_result   : out std_logic;      -- Write enable for result reg.
      data_result : out std_logic_vector(15 downto 0));  -- Data for result reg.
  end component;

end msm_sequencer_rcu_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
