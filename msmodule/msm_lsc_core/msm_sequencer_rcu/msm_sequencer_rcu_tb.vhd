-------------------- Test bench of RCU MS I2C sequencer -----------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library msmodule_lib;
use msmodule_lib.msm_sequencer_rcu_pack.all;


entity msm_sequencer_rcu_tb is

end msm_sequencer_rcu_tb;

architecture test of msm_sequencer_rcu_tb is


  -- component ports
  signal clk         : std_logic                    := '0';  -- [in]  Clock
  signal rstb        : std_logic                    := '0';  -- [in]  Async
  signal data_valid  : std_logic                    := '0';  -- [in]  Input is 
  signal icode       : std_logic_vector(1 downto 0) := (others => '0');  -- 
  signal error       : std_logic                    := '0';  -- [in]  Error flag
  signal ready       : std_logic                    := '0';  -- [in]  Ready for
  signal exec        : std_logic                    := '0';  -- [in]  Execute 
  signal cnt_rx      : std_logic                    := '0';  -- [in]  Counter 
  signal data_fbc    : std_logic_vector(7 downto 0) := (others => '0');  -- [in]
  signal ih_busy     : std_logic                    := '0';  -- [in]  Interrupt
  signal new_data    : std_logic;       -- [out] Got new data
  signal read        : std_logic;       -- [out] Read flag
  signal write       : std_logic;       -- [out] Write flag
  signal start       : std_logic;       -- [out] Make start condition
  signal stop        : std_logic;       -- [out] Make stop condition
  signal seq_active  : std_logic;       -- [out] Sequencer is active
  signal add_ib      : std_logic_vector(2 downto 0);  -- [out] Address to 
  signal we_result   : std_logic;       -- [out] Write enable for result reg.
  signal data_result : std_logic_vector(15 downto 0);  -- [out] Data for result

  -- Clock period
  constant PERIOD : time := 25 ns;    

  constant seq_start : std_logic_vector(1 downto 0) := "10";
  constant seq_stop  : std_logic_vector(1 downto 0) := "00";
  constant seq_write : std_logic_vector(1 downto 0) := "11";
  constant seq_read  : std_logic_vector(1 downto 0) := "01";

  signal do_read : boolean := false;
  
begin  -- test

  -- component instantiation
  dut: msm_sequencer_rcu
    port map (
      clk         => clk,               -- [in]  Clock
      rstb        => rstb,              -- [in]  Async reset
      data_valid  => data_valid,        -- [in]  Input is valid
      icode       => icode,             -- [in]  From instruction bulder
      error       => error,             -- [in]  Error flag
      ready       => ready,             -- [in]  Ready for more
      exec        => exec,              -- [in]  Execute flag
      cnt_rx      => cnt_rx,            -- [in]  Counter of recieved words
      data_fbc    => data_fbc,          -- [in]  Byte from BC
      ih_busy     => ih_busy,           -- [in]  Interrupt handler busy
      new_data    => new_data,          -- [out] Got new data
      read        => read,              -- [out] Read flag
      write       => write,             -- [out] Write flag
      start       => start,             -- [out] Make start condition
      stop        => stop,              -- [out] Make stop condition
      seq_active  => seq_active,        -- [out] Sequencer is active
      add_ib      => add_ib,            -- [out] Address to Instr. build.
      we_result   => we_result,         -- [out] Write enable for result reg.
      data_result => data_result);      -- [out] Data for result reg.

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- purpose: msm_generate instruction code (simulate instr_builder)
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_
  select_icode: process (clk, rstb)
  begin  -- process select_icode
    if rstb = '0' then                  -- asynchronous reset (active low)
      icode <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      case to_integer(unsigned(add_ib)) is
        when 0 => icode <= seq_start; cnt_rx <= '0';
        when 1 => icode <= seq_write; cnt_rx <= '0';
        when 2 => icode <= seq_write; cnt_rx <= '0';
        when 3 =>
          if do_read then
            icode <= seq_read;
          else 
            icode <= seq_write;
          end if;
           cnt_rx <= '0';
        when 4 =>
          if do_read then
            icode <= seq_read;
            cnt_rx <= '1';
          else 
            icode <= seq_write;
            cnt_rx <= '0';
          end if;
        when 5 => icode <= seq_stop;  cnt_rx <= '0';
        when others => icode <= (others => '0');
      end case;
    end if;
  end process select_icode;

  -- purpose: msm_Generate data
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_
  gen_data: process (clk, rstb)
  begin  -- process gen_data
    if rstb = '0' then                  -- asynchronous reset (active low)
      data_fbc <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      case icode is
        when seq_read =>
          data_valid <= new_data; -- '1';
          if cnt_rx = '0' then
            data_fbc <= X"DE";
          else
            data_fbc <= X"AD";
          end if;
        when others =>
          data_fbc <= (others => '0');
          data_valid <= '0';
      end case;
    end if;
  end process gen_data;

  -- purpose: msm_Generate ready signal
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_
  gen_ready: process (clk, rstb)
  begin  -- process gen_ready
    if rstb = '0' then                  -- asynchronous reset (active low)
      ready <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      ready <= new_data;
    end if;
  end process gen_ready;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';

    wait for 4 * PERIOD;
    exec <= '1';
    wait for 1 * PERIOD;
    exec <= '0';

    if seq_active = '0' then
      wait until seq_active = '1';
    end if;
    wait until seq_active = '0';
    
    wait for 4 * PERIOD;
    do_read <= true;
    exec    <= '1';
    wait for 1 * PERIOD;
    exec    <= '0';

    if seq_active = '0' then
      wait until seq_active = '1';
    end if;
    wait until seq_active = '0';
    
    
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_sequencer_rcu_vhdl of msm_sequencer_rcu_tb is

  for test
    for dut : msm_sequencer_rcu
      use entity msmodule_lib.msm_sequencer_rcu(rtl);
    end for;
  end for;

end msm_sequencer_rcu_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
