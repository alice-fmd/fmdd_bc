-------------------- Test bench of RCU MS Local slow control ------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library msmodule_lib;
use msmodule_lib.msm_lsc_core_pack.all;
use msmodule_lib.msm_slave_pack.all;
use msmodule_lib.msm_ffd_pack.all;
use msmodule_lib.msm_clock_master_pack.all;

entity msm_lsc_core_tb is
end msm_lsc_core_tb;

-------------------------------------------------------------------------------

architecture test of msm_lsc_core_tb is

  -- component ports
  signal clk40          : std_logic                     := '0';
  signal clk            : std_logic                     := '0';
  signal rstb           : std_logic                     := '0';
  signal sda_out        : std_logic                     := '0';
  signal sda_ack        : std_logic                     := '0';
  signal fec_al         : std_logic_vector(31 downto 0) := (others => '0');
  signal rdol           : std_logic_vector(31 downto 0) := (others => '0');
  signal exec_dcs       : std_logic                     := '0';
  signal bcast_dcs      : std_logic                     := '0';
  signal rnw_dcs        : std_logic                     := '0';
  signal fec_add_dcs    : std_logic_vector(3 downto 0)  := (others => '0');
  signal branch_dcs     : std_logic                     := '0';
  signal bcreg_add_dcs  : std_logic_vector(7 downto 0)  := (others => '0');
  signal bcdata_dcs     : std_logic_vector(15 downto 0) := (others => '0');
  signal interruptA     : std_logic                     := '0';
  signal interruptB     : std_logic                     := '0';
  signal dataresult_fsc : std_logic_vector(20 downto 0);
  signal weresultmaster : std_logic;    -- [out] I2C: result write enab.
  signal fec_al_fsc     : std_logic_vector(31 downto 0);  -- [out] Valid FECs
  signal we_fec_al_fsc  : std_logic;    -- [out] Write enable for FECs
  signal rdol_fsc       : std_logic_vector(31 downto 0);  -- [out] Readout FECs
  signal we_rdol_fsc    : std_logic;    -- [out] Write enab. for RO FECs
  signal addsm          : std_logic_vector(4 downto 0);   -- [out] Add STATUS
  signal sm             : std_logic_vector(15 downto 0);  -- [out] Data STATUS 
  signal wesm           : std_logic;    -- [out] Write enable for STATUS MEM
  signal seq_active     : std_logic;    -- [out] Sequencer active
  signal sclA           : std_logic;    -- [out] I2C: Clock
  signal sda_inA        : std_logic;    -- [out] I2C: data out
  signal sclB           : std_logic;    -- [out] I2C: Clock
  signal sda_inB        : std_logic;    -- [out] I2C: data out
  signal error          : std_logic;    -- [out] Error in I2C
  signal warningtodcs   : std_logic;    -- [out] Warning
  signal inta_noten     : std_logic;    -- [out] Interrupt branch A masked
  signal intb_noten     : std_logic;    -- [out] Interrupt branch B masked
  signal result         : std_logic_vector(20 downto 0);

  -- Clock period
  constant PERIOD : time := 25 ns;      -- NB: Slower clock

  signal dataA    : std_logic_vector(7 downto 0);
  signal dataB    : std_logic_vector(7 downto 0);
  signal sdaA_out : std_logic;
  signal sdaB_out : std_logic;
  signal sdaA_in  : std_logic;
  signal sdaB_in  : std_logic;

  type words_t is array (0 to 1) of std_logic_vector(15 downto 0);
  constant wordA : words_t := (X"dead", X"2001");
  constant wordB : words_t := (X"beef", X"2000");

  signal start_a : boolean := false;
  signal start_b : boolean := false;
  
begin  -- test
  -- Simulate GTL response to 'Z'
  sdaA_in <= '1' when sda_ina = '1' else '0';
  sdaB_in <= '1' when sda_inb = '1' else '0';

  clock_master_1 : msm_clock_master
    port map (
      clk40      => clk40,              -- [in]
      reset      => rstb,               -- [in]
      clk_master => clk);               -- [out]
  
  -- component instantiation
  dut : msm_lsc_core
    port map (
      clk            => clk,             -- [in]  Clock
      rstb           => rstb,            -- [in]  Async reset
      sda_out        => sda_out,         -- [in]  I2C: data in
      sda_ack        => sda_ack,         -- [in]  I2C: data in
      fec_al         => fec_al,          -- [in]  Active FECs
      rdol           => rdol,            -- [in]  readout fecs
      exec_dcs       => exec_dcs,        -- [in]  Execute I2C
      bcast_dcs      => bcast_dcs,       -- [in]  Broadcast
      rnw_dcs        => rnw_dcs,         -- [in]  Read (not write)
      branch_dcs     => branch_dcs,      -- [in]  Branch select
      fec_add_dcs    => fec_add_dcs,     -- [in]  FEC address
      bcreg_add_dcs  => bcreg_add_dcs,   -- [in]  BC register address
      bcdata_dcs     => bcdata_dcs,      -- [in]  Data for BC
      interruptA     => interruptA,      -- [in]  Interrupt on
      interruptB     => interruptB,      -- [in]  Interrupt on B
      dataresult_fsc => dataresult_fsc,  -- [out] I2C: result
      weresultmaster => weresultmaster,  -- [out] I2C: result write enab.
      fec_al_fsc     => fec_al_fsc,      -- [out] Valid FECs
      we_fec_al_fsc  => we_fec_al_fsc,   -- [out] Write enable for FECs
      rdol_fsc       => rdol_fsc,        -- [out] Readout FECs
      we_rdol_fsc    => we_rdol_fsc,     -- [out] Write enab. for RO FECs
      addsm          => addsm,           -- [out] Address for STATUS
      sm             => sm,              -- [out] Data for STATUS mem
      wesm           => wesm,            -- [out] Write enable for STATUS MEM
      seq_active     => seq_active,      -- [out] Sequencer active
      sclA           => sclA,            -- [out] I2C: Clock
      sda_inA        => sda_inA,         -- [out] I2C: data out
      sclB           => sclB,            -- [out] I2C: Clock
      sda_inB        => sda_inB,         -- [out] I2C: data out
      error          => error,           -- [out] Error in I2C
      warningtodcs   => warningtodcs,    -- [out] Warning
      inta_noten     => inta_noten,      -- [out] Interrupt branch A masked
      intb_noten     => intb_noten);     -- [out] Interrupt branch B masked

  slaveA_gen : for i in 0 to 1 generate
    slave_A : entity msmodule_lib.msm_slave(test2)
      generic map (
        ME => std_logic_vector(to_unsigned(i, 4)),
        PERIOD => 200 ns)
      port map (
        scl     => sclA,                -- [in]  Serial clock
        sda_in  => sdaA_in,             -- [in]  Serial data in
        sda_out => sdaA_out,            -- [out] Serial data out
        word    => wordA(i),            -- [in]  2 output bytes
        data    => open);               -- [out] Output byte
  end generate slaveA_gen;

  slaveB_gen : for i in 0 to 1 generate
    slave_B : entity msmodule_lib.msm_slave(test2)
      generic map (
        ME => std_logic_vector(to_unsigned(i, 4)),
        PERIOD => 200 ns)
      port map (
        scl     => sclB,                -- [in]  Serial clock
        sda_in  => sdaB_in,             -- [in]  Serial data in
        sda_out => sdaB_out,            -- [out] Serial data out
        word    => wordB(i),            -- [in]  2 output bytes
        data    => open);               -- [out] Output byte
  end generate slaveB_gen;

  sda_out <= '1' when (sdaA_out /= '0' and sdaB_out /= '0') else '0';

  ffd_1: msm_ffd
    port map (
      clk  => clk40,                      -- [in]  Clock
      arst => rstb,                     -- [in]  Async reset
      d    => sda_out,                        -- [in]  Input
      q    => sda_ack);                       -- [out] Output
  
  -- purpose: FECs
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  fecs : process (clk, rstb)
  begin  -- process fecs
    if rstb = '0' then                  -- asynchronous reset (active low)
      fec_al <= X"00030003";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_fec_al_fsc = '1' then
        fec_al <= fec_al_fsc;
        if (fec_al & X"0000FFFF" /= X"00000000") then
          interrupta <= '0';
        elsif (fec_al & X"FFFF0000" /= X"00000000") then
          interruptb <= '0';
        end if;
      end if;
      if start_a then
        interrupta <= '1';
      end if;
      if start_b then
        interruptb <= '1';
      end if;
    end if;
  end process fecs;

  -- purpose: FECs
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  rdols : process (clk, rstb)
  begin  -- process fecs
    if rstb = '0' then                  -- asynchronous reset (active low)
      rdol <= X"00030003";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_rdol_fsc = '1' then
        rdol <= rdol_fsc;
      end if;
    end if;
  end process rdols;

  -- purpose: Store result
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  res: process (clk, rstb)
  begin  -- process result
    if rstb = '0' then                  -- asynchronous reset (active low)
      result <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if weresultmaster = '1' then
        result <= dataresult_fsc;
      end if;
    end if;
  end process res;
  -- clock generation
  clk40 <= not clk40 after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';
    wait for 4 * PERIOD;

    start_a <= true;
    wait for PERIOD;
    start_a <= false;
    
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration lsc_core_vhdl of msm_lsc_core_tb is

  use msmodule_lib.msm_lsc_core_pack.all;
  use msmodule_lib.msm_sequencer_rcu_pack.all;
  use msmodule_lib.msm_instr_builder_pack.all;
  use msmodule_lib.msm_master_pack.all;
  use msmodule_lib.msm_master_sm_pack.all;
  use msmodule_lib.msm_sync_pack.all;
  use msmodule_lib.msm_serializer_rcu_pack.all;
  use msmodule_lib.msm_interrupt_driver_pack.all;
  use msmodule_lib.msm_interrupt_handler_pack.all;
  use msmodule_lib.msm_cards_status_pack.all;
  use msmodule_lib.msm_branch_selector_pack.all;
  use msmodule_lib.msm_mux_signals_pack.all;
  use msmodule_lib.msm_sc_signals_pack.all;
  use msmodule_lib.msm_clock_master_pack.all;
  
  for test
    for dut : msm_lsc_core
      use entity msmodule_lib.msm_lsc_core(rtl);
      for rtl
        for sequencer_rcu_1 : msm_sequencer_rcu
          use entity msmodule_lib.msm_sequencer_rcu(rtl);
        end for;
        for instr_builder_1 : msm_instr_builder
          use entity msmodule_lib.msm_instr_builder(rtl);
        end for;
        for master_1 : msm_master
          use entity msmodule_lib.msm_master(rtl);
          for rtl
            for master_sm_1 : msm_master_sm
              use entity msmodule_lib.msm_master_sm(rtl);
            end for;
            for sync_1 : msm_sync
              use entity msmodule_lib.msm_sync(rtl);
            end for;
            for serializer_rcu_1 : msm_serializer_rcu
              use entity msmodule_lib.msm_serializer_rcu(rtl);
            end for;
          end for;
        end for;
        for interrupt_driver_1 : msm_interrupt_driver
          use entity msmodule_lib.msm_interrupt_driver(rtl);
          for rtl
            for interrupt_handler_1 : msm_interrupt_handler
              use entity msmodule_lib.msm_interrupt_handler(rtl);
            end for;
            for cards_status_1 : msm_cards_status
              use entity msmodule_lib.msm_cards_status(rtl);
              for rtl
                for branch_selector_1 : msm_branch_selector
                  use entity msmodule_lib.msm_branch_selector(rtl);
                end for;
              end for;
            end for;
          end for;
        end for;
        for mux_signals_1 : msm_mux_signals
          use entity msmodule_lib.msm_mux_signals(rtl);
        end for;
        for sc_signals_1 : msm_sc_signals
          use entity msmodule_lib.msm_sc_signals(rtl);
        end for;
      end for;
    end for;
    for ffd_1 : msm_ffd
      use entity msmodule_lib.msm_ffd(rtl);
    end for;
    for clock_master_1 : msm_clock_master
      use entity msmodule_lib.msm_clock_master(rtl);
    end for;
  end for;
end lsc_core_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
-------------------------------------------------------------------------------
