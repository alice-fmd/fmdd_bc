-------------------------------------------------------------------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-------------------------------------------------------------------------------
-- Local slow control core
--
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_sequencer_rcu_pack.all;
use msmodule_lib.msm_instr_builder_pack.all;
use msmodule_lib.msm_master_pack.all;
use msmodule_lib.msm_interrupt_driver_pack.all;
use msmodule_lib.msm_mux_signals_pack.all;
use msmodule_lib.msm_sc_signals_pack.all;

entity msm_lsc_core is
  
  port (
    clk            : in  std_logic;     -- Clock
    rstb           : in  std_logic;     -- Async reset
    sda_out        : in  std_logic;     -- I2C: data in
    sda_ack        : in  std_logic;     -- I2C: data in
    fec_al         : in  std_logic_vector(31 downto 0);  -- Active FECs
    rdol           : in  std_logic_vector(31 downto 0);  -- readout fecs
    exec_dcs       : in  std_logic;     -- Execute I2C
    bcast_dcs      : in  std_logic;     -- Broadcast
    rnw_dcs        : in  std_logic;     -- Read (not write)
    branch_dcs     : in  std_logic;     -- Branch select
    fec_add_dcs    : in  std_logic_vector(3 downto 0);   -- FEC address
    bcreg_add_dcs  : in  std_logic_vector(7 downto 0);   -- BC register address
    bcdata_dcs     : in  std_logic_vector(15 downto 0);  -- Data for BC
    interruptA     : in  std_logic;     -- Interrupt on
    interruptB     : in  std_logic;     -- Interrupt on B
    dataresult_fsc : out std_logic_vector(20 downto 0);  -- I2C: result
    weresultmaster : out std_logic;     -- I2C: result write enab.
    fec_al_fsc     : out std_logic_vector(31 downto 0);  -- Valid FECs
    we_fec_al_fsc  : out std_logic;     -- Write enable for FECs
    rdol_fsc       : out std_logic_vector(31 downto 0);  -- Readout FECs
    we_rdol_fsc    : out std_logic;     -- Write enab. for RO FECs
    addsm          : out std_logic_vector(4 downto 0);   -- Address for STATUS
    sm             : out std_logic_vector(15 downto 0);  -- Data for STATUS mem
    wesm           : out std_logic;     -- Write enable for STATUS MEM
    seq_active     : out std_logic;     -- Sequencer active
    sclA           : out std_logic;     -- I2C: Clock
    sda_inA        : out std_logic;     -- I2C: data out
    sclB           : out std_logic;     -- I2C: Clock
    sda_inB        : out std_logic;     -- I2C: data out
    error          : out std_logic;     -- Error in I2C
    warningtodcs   : out std_logic;     -- Warning
    inta_noten     : out std_logic;     -- Interrupt branch A masked
    intb_noten     : out std_logic);    -- Interrupt branch B masked

end msm_lsc_core;

architecture rtl of msm_lsc_core is
  signal data_valid_i     : std_logic;  -- Valid byte
  signal icode_i          : std_logic_vector(1 downto 0);  -- Instruction
  signal byte_i           : std_logic_vector(7 downto 0);  -- 1 byte of data
  signal ready_i          : std_logic;  -- Master is ready
  signal new_data_i       : std_logic;  -- Got new byte
  signal read_i           : std_logic;  -- Read flat
  signal write_i          : std_logic;  -- Write flag
  signal start_i          : std_logic;  -- Start condition
  signal stop_i           : std_logic;  -- Stop condition
  signal error_i          : std_logic;  -- Error flag
  signal word_i           : std_logic_vector(9 downto 0);  -- A word for I2C bus
  signal addib_i          : std_logic_vector(2 downto 0);  -- Address instr.
  signal data_result_i    : std_logic_vector(15 downto 0);  -- Data for result
  signal dataresult_fsc_i : std_logic_vector(20 downto 0);  -- Result
  signal ih_busy_i        : std_logic;  -- Interrupt handler busy
  signal rnw_i            : std_logic;  -- IH: Read (not write)
  signal exec_id_i        : std_logic;  -- IH: Exec
  signal rnw_id_i         : std_logic;  -- IH: Read (not write)
  signal branch_id_i      : std_logic;  -- IH: Branch select 
  signal fec_add_id_i     : std_logic_vector(3 downto 0);  -- IH: FEC address
  signal bcreg_add_id_i   : std_logic_vector(7 downto 0);  -- IH: BC reg. add
  signal bcdata_id_i      : std_logic_vector(15 downto 0);  -- EXT: BC data
  signal exec_i           : std_logic;  -- EXT: execute flag
  signal rwn_i            : std_logic;  -- EXT: read (not write) flag
  signal branch_i         : std_logic;  -- EXT: branch select
  signal bcast_i          : std_logic;  -- EXT: broadcast flag
  signal fec_add_i        : std_logic_vector(3 downto 0);  -- EXT: FEC address
  signal bcreg_add_i      : std_logic_vector(7 downto 0);  -- EXT: BC reg. add
  signal bcdata_i         : std_logic_vector(15 downto 0);  -- EXT: BC data
  signal csr1_i           : std_logic_vector(13 downto 0);  -- BC CSR1 content
  signal scl_i            : std_logic;  -- I2C: clock
  signal sda_in_i         : std_logic;  -- I2C: data out
  signal master_end_i     : std_logic;  -- I2C: msm_master is done
  signal data_fbc_i       : std_logic_vector(7 downto 0);  -- I2C: byte of input
  signal cnt_rx_i         : std_logic;  -- I2C: count input bytes
  signal seq_active_i     : std_logic;  -- Sequencer active

  
begin  -- rtl
  combinatorics : block
  begin  -- block combinatorics
    error            <= error_i;
    seq_active       <= seq_active_i;
    icode_i          <= word_i(9 downto 8);
    byte_i           <= word_i(7 downto 0);
    csr1_i           <= dataresult_fsc_i(13 downto 0);
    dataresult_fsc_i <= branch_i & fec_add_i & data_result_i;
    dataresult_fsc   <= dataresult_fsc_i;
  end block combinatorics;
  
  sequencer_rcu_1 : msm_sequencer_rcu
    port map (
      clk         => clk,               -- [in]  Clock
      rstb        => rstb,              -- [in]  Async reset
      data_valid  => data_valid_i,      -- [in]  Input is valid
      icode       => icode_i,           -- [in]  From instruction bulder
      error       => error_i,           -- [in]  Error flag
      ready       => ready_i,           -- [in]  Ready for more
      exec        => exec_i,            -- [in]  Execute flag
      cnt_rx      => cnt_rx_i,          -- [in]  Counter of recieved words
      data_fbc    => data_fbc_i,        -- [in]  Byte from BC
      ih_busy     => ih_busy_i,         -- [in]  Interrupt handler busy
      new_data    => new_data_i,        -- [out] Got new data
      read        => read_i,            -- [out] Read flag
      write       => write_i,           -- [out] Write flag
      start       => start_i,           -- [out] Make start condition
      stop        => stop_i,            -- [out] Make stop condition
      seq_active  => seq_active_i,      -- [out] Sequencer is active
      add_ib      => addib_i,           -- [out] Address to Instr. build.
      we_result   => weresultmaster,    -- [out] Write enable for result reg.
      data_result => data_result_i);    -- [out] Data for result reg.

  instr_builder_1 : msm_instr_builder
    port map (
      clk     => clk,                   -- [in]  Clock
      rstb    => rstb,                  -- [in]  Async reset
      add     => addib_i,               -- [in]  Address
      rnw     => rnw_i,                 -- [in]  Read (not write)
      bcast   => bcast_i,               -- [in]  Broadcast
      fec_add => fec_add_i,             -- [in]  FEC address
      reg_add => bcreg_add_i,           -- [in]  Register address
      data    => bcdata_i,              -- [in]  Write data
      word    => word_i);               -- [out] Output word
  

  master_1 : msm_master
    port map (
      clk        => clk,                -- [in]  Clock
      rstb       => rstb,               -- [in]  Async reset
      do_im      => byte_i,             -- [in]  data input
      new_data   => new_data_i,         -- [in]  Got more
      read       => read_i,             -- [in]  Read flag
      write      => write_i,            -- [in]  Write flag
      start      => start_i,            -- [in]  Start condition
      stop       => stop_i,             -- [in]  Stop condition
      sda_out    => sda_out,            -- [in]  Serial data from BC
      sda_ack    => sda_ack,            -- [in]  Acknowledge on I2C
      ready_seq  => ready_i,            -- [out] Sequencer ready
      data_valid => data_valid_i,       -- [out] Data is valid
      error      => error_i,            -- [out] Error on I2C bus;
      scl        => scl_i,              -- [out] I2C Clock
      sda_in     => sda_in_i,           -- [out] Serial output data
      master_end => master_end_i,       -- [out] Master is done
      cnt_rx     => cnt_rx_i,           -- [out] Number of bytes recieved
      data_fbc   => data_fbc_i);        -- [out] Data from BC


  interrupt_driver_1 : msm_interrupt_driver
    port map (
      clk            => clk,            -- [in]  Clock
      rstb           => rstb,           -- [in]  Async reset
      seq_active     => seq_active_i,   -- [in]  Sequencer active
      fec_al         => fec_al,         -- [in]  Active FECs
      rdol           => rdol,           -- [in]  Read-out enab. FECs
      stop           => stop_i,         -- [in]  Stop signal
      interruptA     => interruptA,     -- [in]  Interrupt from branch A
      interruptB     => interruptB,     -- [in]  Interrupt from branch B
      csr1           => csr1_i,         -- [in]  Status from FEC BC
      master_end     => master_end_i,   -- [in]  End of master SM
      error          => error_i,        -- [in]  Error from I2C bus
      fec_al_fsc     => fec_al_fsc,     -- [out] Output list of FECs
      we_fec_al_fsc  => we_fec_al_fsc,  -- [out] Write enable for FEC list
      rdol_fsc       => rdol_fsc,       -- [out] Output readout FECs
      we_rdol_fsc    => we_rdol_fsc,    -- [out] Write enable for RDO list
      ih_busy        => ih_busy_i,      -- [out] Interrupt handler busy
      exec           => exec_id_i,      -- [out] Execution flag
      branch         => branch_id_i,    -- [out] Branch select
      rnw            => rnw_id_i,        -- [out] Read (not write) flag
      fec_add        => fec_add_id_i,   -- [out] FEC address
      bcreg_add      => bcreg_add_id_i,  -- [out] BC register address
      data           => bcdata_id_i,    -- [out] Output data
      add_sr         => addsm,          -- [out] Status memory address
      sr             => sm,             -- [out] Data for status mem
      we_sr          => wesm,           -- [out] Write enable for status memory
      warning_to_dcs => warningtodcs,   -- [out] Warning to DCS flag
      inta_not_en    => inta_noten,   -- [out] Interrupts on branch A disabled
      intb_not_en    => intb_noten);  -- [out] Interrupts on branch B disabled


  mux_signals_1 : msm_mux_signals
    port map (
      ih_busy       => ih_busy_i,       -- [in]  Interrupt handler busy
      exec_dcs      => exec_dcs,        -- [in]  DCS: Execute
      rnw_dcs       => rnw_dcs,         -- [in]  DCS: Read (not write)
      branch_dcs    => branch_dcs,      -- [in]  DCS: branch select
      bcast_dcs     => bcast_dcs,       -- [in]  DCS: Broadcast
      fec_add_dcs   => fec_add_dcs,     -- [in]  DCS: FEC address
      bcreg_add_dcs => bcreg_add_dcs,   -- [in]  DCS: BC reg. addr.
      bcdata_dcs    => bcdata_dcs,      -- [in]  DCS: Data for BC
      exec_id       => exec_id_i,       -- [in]  ID: Execute
      rnw_id        => rnw_id_i,        -- [in]  ID: Read (not write)
      branch_id     => branch_id_i,     -- [in]  ID: branch select
      fec_add_id    => fec_add_id_i,    -- [in]  ID: FEC address
      bcreg_add_id  => bcreg_add_id_i,  -- [in]  ID: BC reg. addr.
      bcdata_id     => bcdata_id_i,     -- [in]  ID: Data for BC
      exec          => exec_i,          -- [out] Execute
      rnw           => rnw_i,           -- [out] Read (not write)
      branch        => branch_i,        -- [out] Branch select
      bcast         => bcast_i,         -- [out] Broadcast
      fec_add       => fec_add_i,       -- [out] FEC address
      bcreg_add     => bcreg_add_i,     -- [out] BC reg. add.
      bcdata        => bcdata_i);       -- [out] Data for BC

  sc_signals_1 : msm_sc_signals
    port map (
      sda_in   => sda_in_i,             -- [in]  Data input
      scl      => scl_i,                -- [in]  Clock
      branch   => branch_i,             -- [in]  Branch select
      bcast    => bcast_i,              -- [in]  Broadcast
      sda_in_a => sda_ina,              -- [out] Data input for branch A
      sda_in_b => sda_inb,              -- [out] Data input for branch B
      scl_a    => scla,                 -- [out] Clock for branch A
      scl_b    => sclb);                -- [out] Clock for branch B

  
end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_lsc_core_pack is

  
  component msm_lsc_core
    port (
      clk            : in  std_logic;   -- Clock
      rstb           : in  std_logic;   -- Async reset
      sda_out        : in  std_logic;   -- I2C: data in
      sda_ack        : in  std_logic;   -- I2C: data in
      fec_al         : in  std_logic_vector(31 downto 0);  -- Active FECs
      rdol           : in  std_logic_vector(31 downto 0);  -- readout fecs
      exec_dcs       : in  std_logic;   -- Execute I2C
      bcast_dcs      : in  std_logic;   -- Broadcast
      rnw_dcs        : in  std_logic;   -- Read (not write)
      fec_add_dcs    : in  std_logic_vector(3 downto 0);  -- FEC address
      branch_dcs     : in  std_logic;   -- Branch select
      bcreg_add_dcs  : in  std_logic_vector(7 downto 0);  -- BC register address
      bcdata_dcs     : in  std_logic_vector(15 downto 0);  -- Data for BC
      interruptA     : in  std_logic;   -- Interrupt on
      interruptB     : in  std_logic;   -- Interrupt on B
      dataresult_fsc : out std_logic_vector(20 downto 0);  -- I2C: result
      weresultmaster : out std_logic;   -- I2C: result write enab.
      fec_al_fsc     : out std_logic_vector(31 downto 0);  -- Valid FECs
      we_fec_al_fsc  : out std_logic;   -- Write enable for FECs
      rdol_fsc       : out std_logic_vector(31 downto 0);  -- Readout FECs
      we_rdol_fsc    : out std_logic;   -- Write enab. for RO FECs
      addsm          : out std_logic_vector(4 downto 0);  -- Address for STATUS
      sm             : out std_logic_vector(15 downto 0);  -- Data for STATUS
      wesm           : out std_logic;   -- Write enable for STATUS MEM
      seq_active     : out std_logic;   -- Sequencer active
      sclA           : out std_logic;   -- I2C: Clock
      sda_inA        : out std_logic;   -- I2C: data out
      sclB           : out std_logic;   -- I2C: Clock
      sda_inB        : out std_logic;   -- I2C: data out
      error          : out std_logic;   -- Error in I2C
      warningtodcs   : out std_logic;   -- Warning
      inta_noten     : out std_logic;   -- Interrupt branch A masked
      intb_noten     : out std_logic);  -- Interrupt branch B masked
  end component;
  
end msm_lsc_core_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
