-------------------- Test bench of RCU MS signal multiplexer ------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_mux_signals_pack.all;

entity msm_mux_signals_tb is

end msm_mux_signals_tb;

architecture test of msm_mux_signals_tb is

  signal clk : std_logic;
  
  -- component ports
  signal ih_busy       : std_logic                     := '1';  
  signal exec_dcs      : std_logic                     := '1';  
  signal rnw_dcs       : std_logic                     := '1';  
  signal branch_dcs    : std_logic                     := '1';  
  signal bcast_dcs     : std_logic                     := '1';  
  signal fec_add_dcs   : std_logic_vector(3 downto 0)  := (others => '1');  
  signal bcreg_add_dcs : std_logic_vector(7 downto 0)  := (others => '1');  
  signal bcdata_dcs    : std_logic_vector(15 downto 0) := (others => '1');  
  signal exec_id       : std_logic                     := '0';  
  signal rnw_id        : std_logic                     := '0';  
  signal branch_id     : std_logic                     := '0';  
  signal fec_add_id    : std_logic_vector(3 downto 0)  := (others => '0');  
  signal bcreg_add_id  : std_logic_vector(7 downto 0)  := (others => '0');  
  signal bcdata_id     : std_logic_vector(15 downto 0) := (others => '0');  
  signal exec          : std_logic;     -- [out] Execute
  signal rnw           : std_logic;     -- [out] Read (not write)
  signal branch        : std_logic;     -- [out] Branch select
  signal bcast         : std_logic;     -- [out] Broadcast
  signal fec_add       : std_logic_vector(3 downto 0);   -- [out] FEC address
  signal bcreg_add     : std_logic_vector(7 downto 0);   -- [out] BC reg. add.
  signal bcdata        : std_logic_vector(15 downto 0);  -- [out] Data for BC

  -- Clock period
  constant PERIOD : time := 25 ns;

begin  -- test

  -- component instantiation
  dut: msm_mux_signals
    port map (
      ih_busy       => ih_busy,         -- [in]  Interrupt handler busy
      exec_dcs      => exec_dcs,        -- [in]  DCS: msm_Execute
      rnw_dcs       => rnw_dcs,         -- [in]  DCS: msm_Read (not write)
      branch_dcs    => branch_dcs,      -- [in]  DCS: msm_branch select
      bcast_dcs     => bcast_dcs,       -- [in]  DCS: msm_Broadcast
      fec_add_dcs   => fec_add_dcs,     -- [in]  DCS: msm_FEC address
      bcreg_add_dcs => bcreg_add_dcs,   -- [in]  DCS: msm_BC reg. addr.
      bcdata_dcs    => bcdata_dcs,      -- [in]  DCS: msm_Data for BC
      exec_id       => exec_id,         -- [in]  ID: msm_Execute
      rnw_id        => rnw_id,          -- [in]  ID: msm_Read (not write)
      branch_id     => branch_id,       -- [in]  ID: msm_branch select
      fec_add_id    => fec_add_id,      -- [in]  ID: msm_FEC address
      bcreg_add_id  => bcreg_add_id,    -- [in]  ID: msm_BC reg. addr.
      bcdata_id     => bcdata_id,       -- [in]  ID: msm_Data for BC
      exec          => exec,            -- [out] Execute
      rnw           => rnw,             -- [out] Read (not write)
      branch        => branch,          -- [out] Branch select
      bcast         => bcast,           -- [out] Broadcast
      fec_add       => fec_add,         -- [out] FEC address
      bcreg_add     => bcreg_add,       -- [out] BC reg. add.
      bcdata        => bcdata);         -- [out] Data for BC

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here

    ih_busy <= '0';
    wait for 1 * PERIOD;

    assert exec      = exec_dcs      report "Exec mismatch"    severity warning;
    assert rnw       = rnw_dcs       report "Rnw mismatch"     severity warning;
    assert branch    = branch_dcs    report "Branch mismatch"  severity warning;
    assert bcast     = bcast_dcs     report "Bcast mismatch"   severity warning;
    assert fec_add   = fec_add_dcs   report "Fec_Add mismatch" severity warning;
    assert bcreg_add = bcreg_add_dcs report "Bcreg mismatch"   severity warning;
    assert bcdata    = bcdata_dcs    report "Bcdata mismatch"  severity warning;

    wait for 4 * PERIOD;

    ih_busy <= '1';
    wait for 1 * PERIOD;
    
    assert exec      = exec_id      report "Exec mismatch"    severity warning;
    assert rnw       = rnw_id       report "Rnw mismatch"     severity warning;
    assert branch    = branch_id    report "Branch mismatch"  severity warning;
    assert bcast     = '0'          report "Bcast mismatch"   severity warning;
    assert fec_add   = fec_add_id   report "Fec_Add mismatch" severity warning;
    assert bcreg_add = bcreg_add_id report "Bcreg mismatch"   severity warning;
    assert bcdata    = bcdata_id    report "Bcdata mismatch"  severity warning;
    
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_mux_signals_vhdl of msm_mux_signals_tb is

  for test
    for dut : msm_mux_signals
      use entity msmodule_lib.msm_mux_signals(rtl);
    end for;
  end for;

end msm_mux_signals_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
