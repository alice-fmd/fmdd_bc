-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- Mux of signals
library ieee;
use ieee.std_logic_1164.all;

entity msm_mux_signals is
  
  port (
    ih_busy       : in  std_logic;      -- Interrupt handler busy
    exec_dcs      : in  std_logic;      -- DCS: msm_Execute 
    rnw_dcs       : in  std_logic;      -- DCS: msm_Read (not write)
    branch_dcs    : in  std_logic;      -- DCS: msm_branch select
    bcast_dcs     : in  std_logic;      -- DCS: msm_Broadcast
    fec_add_dcs   : in  std_logic_vector(3 downto 0);    -- DCS: msm_FEC address
    bcreg_add_dcs : in  std_logic_vector(7 downto 0);    -- DCS: msm_BC reg. addr.
    bcdata_dcs    : in  std_logic_vector(15 downto 0);   -- DCS: msm_Data for BC
    exec_id       : in  std_logic;      -- ID: msm_Execute 
    rnw_id        : in  std_logic;      -- ID: msm_Read (not write)
    branch_id     : in  std_logic;      -- ID: msm_branch select
    fec_add_id    : in  std_logic_vector(3 downto 0);    -- ID: msm_FEC address
    bcreg_add_id  : in  std_logic_vector(7 downto 0);    -- ID: msm_BC reg. addr.
    bcdata_id     : in  std_logic_vector(15 downto 0);   -- ID: msm_Data for BC
    exec          : out std_logic;      -- Execute
    rnw           : out std_logic;      -- Read (not write)
    branch        : out std_logic;      -- Branch select
    bcast         : out std_logic;      -- Broadcast
    fec_add       : out std_logic_vector(3 downto 0);    -- FEC address
    bcreg_add     : out std_logic_vector(7 downto 0);    -- BC reg. add.
    bcdata        : out std_logic_vector(15 downto 0));  -- Data for BC

end msm_mux_signals;

architecture rtl of msm_mux_signals is

begin  -- rtl

  exec      <= exec_id      when ih_busy = '1' else exec_dcs;
  rnw       <= rnw_id       when ih_busy = '1' else rnw_dcs;
  branch    <= branch_id    when ih_busy = '1' else branch_dcs;
  bcast     <= '0'          when ih_busy = '1' else bcast_dcs;
  fec_add   <= fec_add_id   when ih_busy = '1' else fec_add_dcs;
  bcreg_add <= bcreg_add_id when ih_busy = '1' else bcreg_add_dcs;
  bcdata    <= bcdata_id    when ih_busy = '1' else bcdata_dcs;
  

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_mux_signals_pack is

  component msm_mux_signals
    port (
      ih_busy       : in  std_logic;    -- Interrupt handler busy
      exec_dcs      : in  std_logic;    -- DCS: msm_Execute
      rnw_dcs       : in  std_logic;    -- DCS: msm_Read (not write)
      branch_dcs    : in  std_logic;    -- DCS: msm_branch select
      bcast_dcs     : in  std_logic;    -- DCS: msm_Broadcast
      fec_add_dcs   : in  std_logic_vector(3 downto 0);  -- DCS: msm_FEC address
      bcreg_add_dcs : in  std_logic_vector(7 downto 0);  -- DCS: msm_BC reg. addr.
      bcdata_dcs    : in  std_logic_vector(15 downto 0);   -- DCS: msm_Data for BC
      exec_id       : in  std_logic;    -- ID: msm_Execute
      rnw_id        : in  std_logic;    -- ID: msm_Read (not write)
      branch_id     : in  std_logic;    -- ID: msm_branch select
      fec_add_id    : in  std_logic_vector(3 downto 0);  -- ID: msm_FEC address
      bcreg_add_id  : in  std_logic_vector(7 downto 0);  -- ID: msm_BC reg. addr.
      bcdata_id     : in  std_logic_vector(15 downto 0);   -- ID: msm_Data for BC
      exec          : out std_logic;    -- Execute
      rnw           : out std_logic;    -- Read (not write)
      branch        : out std_logic;    -- Branch select
      bcast         : out std_logic;    -- Broadcast
      fec_add       : out std_logic_vector(3 downto 0);  -- FEC address
      bcreg_add     : out std_logic_vector(7 downto 0);  -- BC reg. add.
      bcdata        : out std_logic_vector(15 downto 0));  -- Data for BC
  end component;

end msm_mux_signals_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
