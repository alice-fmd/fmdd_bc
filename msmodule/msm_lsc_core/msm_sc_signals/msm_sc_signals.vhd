-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- selection signal SC with the branch
library ieee;
use ieee.std_logic_1164.all;

entity msm_sc_signals is
  
  port (
    sda_in   : in  std_logic;           -- Data input
    scl      : in  std_logic;           -- Clock
    branch   : in  std_logic;           -- Branch select
    bcast    : in  std_logic;           -- Broadcast
    sda_in_a : out std_logic;           -- Data input for branch A
    sda_in_b : out std_logic;           -- Data input for branch B
    scl_a    : out std_logic;           -- Clock for branch A
    scl_b    : out std_logic);          -- Clock for branch B

end msm_sc_signals;

architecture rtl of msm_sc_signals is

begin  -- rtl

  sda_in_a <= sda_in when (branch = '0' or bcast = '1') else 'H';
  sda_in_b <= sda_in when (branch = '1' or bcast = '1') else 'H';
  scl_a    <= scl    when (branch = '0' or bcast = '1') else 'H';
  scl_b    <= scl    when (branch = '1' or bcast = '1') else 'H';
  

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_sc_signals_pack is

  component msm_sc_signals
    port (
      sda_in   : in  std_logic;         -- Data input
      scl      : in  std_logic;         -- Clock
      branch   : in  std_logic;         -- Branch select
      bcast    : in  std_logic;         -- Broadcast
      sda_in_a : out std_logic;         -- Data input for branch A
      sda_in_b : out std_logic;         -- Data input for branch B
      scl_a    : out std_logic;         -- Clock for branch A
      scl_b    : out std_logic);        -- Clock for branch B
  end component;

end msm_sc_signals_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
