-------------------- Test bench of RCU MS slow control signals driver ---------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_sc_signals_pack.all;

entity msm_sc_signals_tb is

end msm_sc_signals_tb;

architecture test of msm_sc_signals_tb is

  -- component ports
  signal sda_in   : std_logic := '0';   -- [in]  Data input
  signal scl      : std_logic := '0';   -- [in]  Clock
  signal branch   : std_logic := '0';   -- [in]  Branch select
  signal bcast    : std_logic := '0';   -- [in]  Broadcast
  signal sda_in_a : std_logic;          -- [out] Data input for branch A
  signal sda_in_b : std_logic;          -- [out] Data input for branch B
  signal scl_a    : std_logic;          -- [out] Clock for branch A
  signal scl_b    : std_logic;          -- [out] Clock for branch B

  -- Clock period
  constant PERIOD : time := 200 ns;

begin  -- test

  -- component instantiation
  dut: msm_sc_signals
    port map (
      sda_in   => sda_in,               -- [in]  Data input
      scl      => scl,                  -- [in]  Clock
      branch   => branch,               -- [in]  Branch select
      bcast    => bcast,                -- [in]  Broadcast
      sda_in_a => sda_in_a,             -- [out] Data input for branch A
      sda_in_b => sda_in_b,             -- [out] Data input for branch B
      scl_a    => scl_a,                -- [out] Clock for branch A
      scl_b    => scl_b);               -- [out] Clock for branch B

  -- clock generation
  scl <= not scl after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    branch <= '0';
    wait for 1 * PERIOD;

    assert scl_a    = scl    report "SCL mismatch" severity warning;
    assert sda_in_a = sda_in report "SDA mismatch" severity warning;

    wait for 4 * PERIOD;
    branch <= '1';
    wait for 1 * PERIOD;

    assert scl_b    = scl    report "SCL mismatch" severity warning;
    assert sda_in_b = sda_in report "SDA mismatch" severity warning;

    wait for 4 * PERIOD;
    branch <= '0';
    bcast <= '1';
    wait for 1 * PERIOD;

    assert scl_a    = scl    report "SCL mismatch" severity warning;
    assert sda_in_a = sda_in report "SDA mismatch" severity warning;
    assert scl_b    = scl    report "SCL mismatch" severity warning;
    assert sda_in_b = sda_in report "SDA mismatch" severity warning;

    wait for 4 * PERIOD;
    
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_sc_signals_vhdl of msm_sc_signals_tb is

  for test
    for dut : msm_sc_signals
      use entity msmodule_lib.msm_sc_signals(rtl);
    end for;
  end for;

end msm_sc_signals_vhdl;
-------------------------------------------------------------------------------
--
-- EOF
--
-------------------------------------------------------------------------------
