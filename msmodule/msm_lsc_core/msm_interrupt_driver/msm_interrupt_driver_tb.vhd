-------------------- Test bench of RCU MS Interrupt driver --------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library msmodule_lib;
use msmodule_lib.msm_interrupt_driver_pack.all;

entity msm_interrupt_driver_tb is
end msm_interrupt_driver_tb;

architecture test of msm_interrupt_driver_tb is


  -- component ports
  signal clk            : std_logic                     := '0';
  signal rstb           : std_logic                     := '0';
  signal seq_active     : std_logic                     := '0';
  signal fec_al         : std_logic_vector(31 downto 0) := X"00030003";
  signal rdol           : std_logic_vector(31 downto 0) := X"00030003";
  signal stop           : std_logic                     := '0';
  signal interruptA     : std_logic                     := '0';
  signal interruptB     : std_logic                     := '0';
  signal csr1           : std_logic_vector(13 downto 0) := (others => '0');
  signal master_end     : std_logic                     := '0';
  signal error          : std_logic                     := '0';
  signal fec_al_fsc     : std_logic_vector(31 downto 0);
  signal we_fec_al_fsc  : std_logic;
  signal rdol_fsc       : std_logic_vector(31 downto 0);
  signal we_rdol_fsc    : std_logic;
  signal ih_busy        : std_logic;
  signal exec           : std_logic;
  signal branch         : std_logic;
  signal rnw            : std_logic;
  signal fec_add        : std_logic_vector(3 downto 0);
  signal bcreg_add      : std_logic_vector(7 downto 0);
  signal data           : std_logic_vector(15 downto 0);
  signal add_sr         : std_logic_vector(4 downto 0);
  signal sr             : std_logic_vector(15 downto 0);
  signal we_sr          : std_logic;
  signal warning_to_dcs : std_logic;
  signal inta_not_en    : std_logic;
  signal intb_not_en    : std_logic;

  -- Clock period
  constant PERIOD : time := 25 ns;

  signal hard    : std_logic                    := '0';
  signal our_fec : std_logic_vector(4 downto 0) := "00000";
  signal start   : std_logic                    := '0';
  
  -- purpose: Produce interrupt and set fec address
  procedure doit (
    signal   busy    : in  std_logic;
    signal   start   : out std_logic;
    signal   our_fec : out std_logic_vector(4 downto 0);
    constant fec     : in  std_logic_vector(4 downto 0)) is
  begin  -- doit
    start   <= '1';
    our_fec <= fec;
    wait for 2 * PERIOD;
    start   <= '0';
    wait until busy = '1';
    wait until busy = '0';
  end doit;
  
begin  -- test

  -- component instantiation
  dut: msm_interrupt_driver
    port map (
      clk            => clk,            -- [in]  Clock
      rstb           => rstb,           -- [in]  Async reset
      seq_active     => seq_active,     -- [in]  Sequencer active
      fec_al         => fec_al,         -- [in]  Active FECs
      rdol           => rdol,           -- [in]  Read-out enab. FECs
      stop           => stop,           -- [in]  Stop signal
      interruptA     => interruptA,     -- [in]  Interrupt from branch A
      interruptB     => interruptB,     -- [in]  Interrupt from branch B
      csr1           => csr1,           -- [in]  Status from FEC BC
      master_end     => master_end,     -- [in]  End of master SM
      error          => error,          -- [in]  Error from I2C bus
      fec_al_fsc     => fec_al_fsc,     -- [out] Output list of FECs
      we_fec_al_fsc  => we_fec_al_fsc,  -- [out] Write enable for FEC list
      rdol_fsc       => rdol_fsc,       -- [out] Output readout FECs
      we_rdol_fsc    => we_rdol_fsc,    -- [out] Write enable for RDO list
      ih_busy        => ih_busy,        -- [out] Interrupt handler busy
      exec           => exec,           -- [out] Execution flag
      branch         => branch,         -- [out] Branch select
      rnw            => rnw,            -- [out] Read (not write) flag
      fec_add        => fec_add,        -- [out] FEC address
      bcreg_add      => bcreg_add,      -- [out] BC register address
      data           => data,           -- [out] Output data
      add_sr         => add_sr,         -- [out] Status memory address
      sr             => sr,             -- [out] Data for status mem
      we_sr          => we_sr,          -- [out] Write enable for status memory
      warning_to_dcs => warning_to_dcs,  -- [out] Warning to DCS flag
      inta_not_en    => inta_not_en,   -- [out] Interrupts on branch A disabled
      intb_not_en    => intb_not_en);  -- [out] Interrupts on branch B disabled

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- purpose: Sim stop response
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  stop_it : process (clk, rstb)
  begin  -- process stop_it
    if rstb = '0' then                  -- asynchronous reset (active low)
      stop <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if exec = '1' then
        if branch = our_fec(4) and fec_add = our_fec(3 downto 0) then
          csr1(13)          <= '1';
          csr1(12 downto 2) <= (others => '0');
          csr1(0)           <= hard;
          csr1(1)           <= not hard;
        else
          csr1 <= (others => '0');
        end if;
        stop <= '1';
      else
        stop <= '0';
      end if;
    end if;
  end process stop_it;

  -- purpose: Make interrupts
  -- type   : sequential
  -- inputs : clk, rstb, our_fec, start
  -- outputs: interrupta, interruptb
  make_them : process (clk, rstb)
  begin  -- process make_them
    if rstb = '0' then                  -- asynchronous reset (active low)
      interrupta <= '0';
      interruptb <= '0';
      master_end <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if start = '1' then
        if our_fec(4) = '0' then
          interrupta <= '1';
          interruptb <= '0';
        else
          interrupta <= '0';
          interruptb <= '1';
        end if;
        master_end <= '0';
      elsif fec_al(to_integer(unsigned(our_fec))) = '0' then
        -- Remove interrupt when card is off
        interrupta <= '0';
        interruptb <= '0';
        master_end <= '1';
      elsif (hard = '0' and
             fec_add = our_fec(3 downto 0) and
             branch = our_fec(4) and
             bcreg_add = X"11" and
             rnw = '0') then
        -- Remove interrupt when disabled
        interrupta <= '0';
        interruptb <= '0';
        master_end <= '1';
      end if;
    end if;
  end process make_them;
  
  -- purpose: FECs
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  fecs : process (clk, rstb)
  begin  -- process fecs
    if rstb = '0' then                  -- asynchronous reset (active low)
      fec_al <= X"00030003";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_fec_al_fsc = '1' then
        fec_al <= fec_al_fsc;
      end if;
    end if;
  end process fecs;

  -- purpose: FECs
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  rdols : process (clk, rstb)
  begin  -- process fecs
    if rstb = '0' then                  -- asynchronous reset (active low)
      rdol <= X"00030003";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_rdol_fsc = '1' then
        rdol <= rdol_fsc;
      end if;
    end if;
  end process rdols;

  -- waveform generation
  stim : process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';


    wait for 4 * PERIOD;
    doit(start   => start,
         busy    => ih_busy,
         our_fec => our_fec,
         fec     => '1' & X"1");


    wait for 4 * PERIOD;
    hard <= '1';
    doit(start   => start,
         busy    => ih_busy,
         our_fec => our_fec,
         fec     => '1' & X"0");
    

  
    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_interrupt_driver_vhdl of msm_interrupt_driver_tb is

  for test
    for dut : msm_interrupt_driver
      use entity msmodule_lib.msm_interrupt_driver(rtl);
    end for;
  end for;

end msm_interrupt_driver_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
