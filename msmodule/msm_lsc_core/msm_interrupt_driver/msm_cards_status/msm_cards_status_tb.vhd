-------------------- Test bench of RCU MS card status -------------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_cards_status_pack.all;

entity msm_cards_status_tb is
  
end msm_cards_status_tb;

architecture test of msm_cards_status_tb is
  constant PERIOD : time      := 25 ns;
  signal   clk    : std_logic := '0';
  signal   rstb   : std_logic := '0';

  signal Interrupta     : std_logic                     := '0';
  signal Interruptb     : std_logic                     := '0';
  signal read_al        : std_logic                     := '0';
  signal fec_al         : std_logic_vector(31 downto 0) := X"00030003";
  signal stop           : std_logic                     := '0';
  signal error          : std_logic                     := '0';
  signal check_int_wait : std_logic                     := '0';

  signal add_ready  : std_logic;
  signal add_card   : std_logic_vector(4 downto 0);
  signal last_card  : std_logic;
  signal inta_not_en : std_logic;
  signal intb_not_en : std_logic;
  
  
begin  -- test

  stop_it: process (clk, rstb)
  begin  -- process stop_it
    if rstb = '0' then                  -- asynchronous reset (active low)
      stop <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      stop <= add_ready;
    end if;
  end process stop_it;

  readit: process (clk,rstb)
  begin  -- process readit
    if rstb = '0' then                  -- asynchronous reset (active low)
      read_al <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if interrupta = '1' and not (add_card = "00010") then
        read_al <= '1';
      elsif interruptb = '1' and not (add_card = "10010") then
        read_al <= '1';
      else
        read_al <= '0';
      end if;
    end if;
  end process readit;

  dut : msm_cards_status
    port map (
      clk            => clk,            -- [in]  clock
      rstb           => rstb,           -- [in]  async reset
      interruptA     => interruptA,     -- [in]  Interrupt on branch A
      interruptB     => interruptB,     -- [in]  Interrupt on branch B
      read_al        => read_al,        -- [in]  Read from BC's
      fec_al         => fec_al,         -- [in]  Active list
      stop           => stop,           -- [in]  Stop
      error          => error,          -- [in]  Error from I2C
      check_int_wait => check_int_wait,  -- [in]
      add_ready      => add_ready,      -- [out] Validates address
      add_card       => add_card,       -- [out] FEC address
      last_card      => last_card,      -- [out] At last card
      inta_not_en    => inta_not_en,    -- [out] Disable interrupts on branch A
      intb_not_en    => intb_not_en);   -- [out] Disable interrupts on branch B
  
  -- purpose: Generate
  -- type   : combinational
  -- inputs : 
  -- outputs: clk
  clock : process
  begin  -- process clock
    clk <= not clk;
    wait for PERIOD / 2;
  end process clock;

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stmuli : process
  begin  -- process stmuli
    report "Starting" severity note;
    wait for 100 ns;
    rstb <= '1';

    interrupta <= '1';
    check_int_wait <= '1';

    wait for 100 ns;
    wait until read_al = '0';
    interrupta <= '0';
    
    wait for 300 ns;
    
    interruptb <= '1';
    check_int_wait <= '1';

    wait for 100 ns;
    wait until read_al = '0';
    interruptb <= '0';
    

    -- read_al <= '1';
              
    
    wait;                               -- for ever
  end process stmuli;
  

end test;
-------------------------------------------------------------------------------
configuration msm_cards_status_vhdl of msm_cards_status_tb is

  for test
    for dut : msm_cards_status
      use entity msmodule_lib.msm_cards_status(rtl);
    end for;
  end for;

end msm_cards_status_vhdl;



-------------------------------------------------------------------------------
--
-- EOF
--
