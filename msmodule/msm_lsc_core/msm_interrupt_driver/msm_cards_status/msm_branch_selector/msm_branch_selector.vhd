-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- This state machine makes the choice of the first Interrupt (from branch A or
-- B) to be handle. In the case of any BC gives information about the
-- Interrupt, it will be disabled.
--
library ieee;
use ieee.std_logic_1164.all;

entity msm_branch_selector is
  
  port (
    clk            : in  std_logic;     -- Clock
    rstb           : in  std_logic;     -- Async reset
    interruptA     : in  std_logic;     -- Interrupt on branch A
    interruptB     : in  std_logic;     -- Interrupt on branch B
    cnt            : in  std_logic_vector(3 downto 0);  -- Counter
    check_int_wait : in  std_logic;     -- Check interrupt waiting
    branch         : out std_logic;     -- Branch
    last_card      : out std_logic;     -- At last card
    inta_not_en    : out std_logic;     -- Interrupt on branch A disabled
    intb_not_en    : out std_logic;     -- Interrupt on branch B disabled
    reset_card     : out std_logic);    -- Reset FEC that caused interrupt

end msm_branch_selector;

architecture rtl of msm_branch_selector is

  type   st_t is (idle, branch_a, branch_b);  -- State type
  signal st_i        : st_t := idle;          -- State
  signal nx_st_i     : st_t := idle;          -- Next state
  signal last_card_i : std_logic;
begin  -- rtl
  reset_card  <= '1' when st_i = idle else '0';
  last_card_i <= '1' when cnt = X"F" else '0';
  last_card   <= last_card_i;

  -- purpose: msm_Update state
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, nx_st_i
  -- outputs: msm_st_i
  next_state : process (clk, rstb)
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;


  -- purpose: msm_The state machine
  -- type   : msm_combinational
  -- inputs : msm_st_i, interruptA, interruptB, last_card, check_int_wait
  -- outputs: msm_nx_st_i, inta_not_en, intb_not_en
  fsm : process (st_i, interruptA, interruptB, last_card_i, check_int_wait)
  begin  -- process fsm
    case st_i is
      when idle =>
        branch      <= '0';
        inta_not_en <= '0';
        intb_not_en <= '0';
        if interruptA = '1' then
          nx_st_i <= branch_a;
        elsif interruptB = '1' then
          nx_st_i <= branch_b;
        else
          nx_st_i <= idle;
        end if;

      when branch_a =>
        branch      <= '0';
        intb_not_en <= '0';
        if interruptA = '0' and check_int_wait = '1' then
          inta_not_en <= '0';
          nx_st_i     <= idle;
        elsif last_card_i = '1' then
          inta_not_en <= '1';
          nx_st_i     <= idle;
        else
          inta_not_en <= '0';
          nx_st_i     <= branch_a;
        end if;

      when branch_b =>
        branch      <= '1';
        inta_not_en <= '0';
        if interruptB = '0' and check_int_wait = '1' then
          intb_not_en <= '0';
          nx_st_i     <= idle;
        elsif last_card_i = '1' then
          intb_not_en <= '1';
          nx_st_i     <= idle;
        else
          intb_not_en <= '0';
          nx_st_i     <= branch_b;
        end if;
        
      when others =>
        branch  <= '0';
        nx_st_i <= idle;
    end case;
    
  end process fsm;

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_branch_selector_pack is

  component msm_branch_selector
    port (
      clk            : in  std_logic;   -- Clock
      rstb           : in  std_logic;   -- Async reset
      interruptA     : in  std_logic;   -- Interrupt on branch A
      interruptB     : in  std_logic;   -- Interrupt on branch B
      cnt            : in  std_logic_vector(3 downto 0);  -- Counter
      check_int_wait : in  std_logic;   -- Check interrupt waiting
      branch         : out std_logic;   -- Branch
      last_card      : out std_logic;   -- At last card
      inta_not_en    : out std_logic;   -- Interrupt on branch A disabled
      intb_not_en    : out std_logic;   -- Interrupt on branch B disabled
      reset_card     : out std_logic);  -- Reset FEC that caused interrupt
  end component;

end msm_branch_selector_pack;

-------------------------------------------------------------------------------
--
-- EOF
--

