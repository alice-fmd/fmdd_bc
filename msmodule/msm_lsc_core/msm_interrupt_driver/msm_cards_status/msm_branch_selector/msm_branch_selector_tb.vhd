-------------------- Test bench of RCU MS branch selector ---------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_branch_selector_pack.all;

entity msm_branch_selector_tb is

end msm_branch_selector_tb;

architecture test of msm_branch_selector_tb is
  constant PERIOD : time      := 25 ns;
  signal   clk    : std_logic := '0';
  signal   rstb   : std_logic := '0';

  signal Interrupta : std_logic := '0';
  signal Interruptb : std_logic := '0';

  signal cnt : std_logic_vector(3 downto 0) := X"0";

  signal check_int_wait : std_logic := '0';

  signal branch      : std_logic;
  signal last_card   : std_logic;
  signal inta_not_en : std_logic;
  signal intb_not_en : std_logic;
  signal reset_card  : std_logic;

  procedure gen_interrupt (
    signal interrupt : out std_logic) is
  begin
    wait for 100 ns;
    interrupt <= '1';
    wait for 100 ns;
    interrupt <= '0';
  end gen_interrupt;

begin  -- test

  dut : msm_branch_selector
    port map (
      clk            => clk,            -- [in]  Clock
      rstb           => rstb,           -- [in]  Async reset
      interruptA     => interruptA,     -- [in]  Interrupt on branch A
      interruptB     => interruptB,     -- [in]  Interrupt on branch B
      cnt            => cnt,            -- [in]  Counter
      check_int_wait => check_int_wait,  -- [in]  Check interrupt waiting
      branch         => branch,         -- [out] Branch
      last_card      => last_card,      -- [out] At last card
      inta_not_en    => inta_not_en,    -- [out] Interrupt on branch A disabled
      intb_not_en    => intb_not_en,    -- [out] Interrupt on branch B disabled
      reset_card     => reset_card);  -- [out] Reset FEC that caused interrupt

  -- purpose: msm_Generate
  -- type   : msm_combinational
  -- inputs : msm_
  -- outputs: msm_clk
  clock : process
  begin  -- process clock
    clk <= not clk;
    wait for PERIOD / 2;
  end process clock;

  -- purpose: msm_Provide stimuli
  -- type   : msm_combinational
  -- inputs : msm_
  -- outputs: msm_
  stmuli : process
  begin  -- process stmuli
    report "Starting" severity note;
    wait for 100 ns;
    rstb <= '1';

    report "No check, cnt=0" severity note;
    gen_interrupt(interrupt => interruptA);
    wait for 100 ns;

    gen_interrupt(interrupt => interruptB);
    wait for 100 ns;

    report "Check, cnt=0" severity note;
    check_int_wait <= '1';
    wait for 100 ns;

    gen_interrupt(interrupt => interruptA);
    wait for 100 ns;

    gen_interrupt(interrupt => interruptB);
    wait for 100 ns;

    report "Check, cnt=15" severity note;
    cnt <= X"F";
    gen_interrupt(interrupt => interruptA);
    wait for 100 ns;

    gen_interrupt(interrupt => interruptB);
    wait for 100 ns;

    check_int_wait <= '0';
    wait for 100 ns;

    report "No check, cnt=15" severity note;
    gen_interrupt(interrupt => interruptA);
    wait for 100 ns;

    gen_interrupt(interrupt => interruptB);
    wait for 100 ns;

    wait;                               -- for ever
  end process stmuli;
end test;

-------------------------------------------------------------------------------
configuration msm_branch_selector_vhdl of msm_branch_selector_tb is

  for test
    for dut : msm_branch_selector
      use entity msmodule_lib.msm_branch_selector(rtl);
    end for;
  end for;

end msm_branch_selector_vhdl;


-------------------------------------------------------------------------------
--
-- EOF
--
