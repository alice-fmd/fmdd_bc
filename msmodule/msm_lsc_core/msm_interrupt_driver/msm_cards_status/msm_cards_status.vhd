-------------------------------------------------------------------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- This block gives the address of the Active FECs to start polling the CSR of
-- every BC in case of Interrupt.  Depending of the origin of the Interrupt
-- (branch A or B) the Card Status Block checks the Active FECs in the higest
-- or lowest bit of the FEC_AL.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library msmodule_lib;
use msmodule_lib.msm_branch_selector_pack.all;

entity msm_cards_status is
  
  port (
    clk            : in  std_logic;     -- clock
    rstb           : in  std_logic;     -- async reset
    interruptA     : in  std_logic;     -- Interrupt on branch A
    interruptB     : in  std_logic;     -- Interrupt on branch B
    read_al        : in  std_logic;     -- Read from BC's
    fec_al         : in  std_logic_vector(31 downto 0);  -- Active list
    stop           : in  std_logic;     -- Stop
    error          : in  std_logic;     -- Error from I2C
    check_int_wait : in  std_logic;
    add_ready      : out std_logic;     -- Validates address
    add_card       : out std_logic_vector(4 downto 0);   -- FEC address
    last_card      : out std_logic;     -- At last card
    inta_not_en    : out std_logic;     -- Disable interrupts on branch A
    intb_not_en    : out std_logic);    -- Disable interrupts on branch B

end msm_cards_status;

architecture rtl of msm_cards_status is

  type st_t is (s0, s1, s2, s3, s4);    -- State type

  signal st_i          : st_t;              -- State
  signal nx_st_i       : st_t;              -- Next state
  signal last_card_i   : std_logic;
  signal enable_cnt_i  : std_logic := '0';  -- Enable counter
  signal reset_card_i  : std_logic;
  signal branch_i      : std_logic;
  signal al_i          : std_logic_vector(12 downto 0);
  signal cnt_i         : unsigned(3 downto 0) := X"0";
  signal cnt_ii        : std_logic_vector(3 downto 0);
  signal active_card_i : std_logic;
  signal idx_i         : integer   := 0;
begin  -- rtl
  idx_i                <= 0                    when cnt_i < 1 else
                          to_integer(cnt_i - X"1");
  last_card            <= last_card_i;
  add_card(4)          <= branch_i;
  add_card(3 downto 0) <= cnt_ii;
  al_i                 <= fec_al(28 downto 16) when branch_i = '1'
                          else fec_al(12 downto 0);
  active_card_i <= '1' when cnt_i > 0 and cnt_i < X"C" and al_i(idx_i) = '1'
                   else '0';
  cnt_ii        <= std_logic_vector(to_unsigned(idx_i, 4));
  
  branch_selector_1 : msm_branch_selector
    port map (
      clk            => clk,            -- [in]  Clock
      rstb           => rstb,           -- [in]  Async reset
      interruptA     => interruptA,     -- [in]  Interrupt on branch A
      interruptB     => interruptB,     -- [in]  Interrupt on branch B
      cnt            => cnt_ii,         -- [in]  Counter
      check_int_wait => check_int_wait,  -- [in]  Check interrupt waiting
      branch         => branch_i,       -- [out] Branch
      last_card      => last_card_i,    -- [out] At last card
      inta_not_en    => inta_not_en,    -- [out] Interrupt on branch A disabled
      intb_not_en    => intb_not_en,    -- [out] Interrupt on branch B disabled
      reset_card     => reset_card_i);  -- [out] Reset FEC that caused interrupt

  -- purpose: Update state
  -- type   : sequential
  -- inputs : clk, rstb, nx_st_i
  -- outputs: st_i
  next_state : process (clk, rstb)
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= s0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;

  -- purpose: State machine
  fsm : process (st_i, read_al, active_card_i, stop, error, last_card_i)
  begin  -- process fsm
    case st_i is
      when s0 =>
        enable_cnt_i <= '0';
        add_ready    <= '0';
        if read_al = '1' then
          nx_st_i <= s1;
        else
          nx_st_i <= s0;
        end if;

      when s1 =>
        enable_cnt_i <= '1';
        add_ready    <= '0';
        nx_st_i      <= s2;

      when s2 =>
        enable_cnt_i <= '0';
        add_ready    <= '0';
        if last_card_i = '1' then
          nx_st_i <= s0;
        elsif active_card_i = '1' then
          nx_st_i <= s3;
        else
          nx_st_i <= s1;
        end if;

      when s3 =>
        enable_cnt_i <= '0';
        add_ready    <= '1';
        if error = '1' then
          nx_st_i <= s0;
        elsif stop = '1' then
          nx_st_i <= s0;
        else
          nx_st_i <= s4;
        end if;

      when s4 =>
        enable_cnt_i <= '0';
        add_ready    <= '0';
        if error = '1' then
          nx_st_i <= s0;
        elsif stop = '1' then
          nx_st_i <= s0;
        else
          nx_st_i <= s4;
        end if;
        
      when others =>
        enable_cnt_i <= '0';
        add_ready    <= '0';
        nx_st_i      <= s0;
    end case;
  end process fsm;

  -- purpose: Counter of the FEC list
  -- type   : sequential
  -- inputs : clk, rstb, reset_card, last_card, enable_cnt_i
  -- outputs: cnt_i
  counter : process (clk, rstb)
  begin  -- process counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i <= X"0";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if reset_card_i = '1' then
        cnt_i <= X"0"; 
      elsif last_card_i = '1' then
        cnt_i <= X"0";
      elsif enable_cnt_i = '1' then
        cnt_i <= cnt_i + X"1";
      end if;
    end if;
  end process counter;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_cards_status_pack is

  component msm_cards_status
    port (
      clk            : in  std_logic;   -- clock
      rstb           : in  std_logic;   -- async reset
      interruptA     : in  std_logic;   -- Interrupt on branch A
      interruptB     : in  std_logic;   -- Interrupt on branch B
      read_al        : in  std_logic;   -- Read from BC's
      fec_al         : in  std_logic_vector(31 downto 0);  -- Active list
      stop           : in  std_logic;   -- Stop
      error          : in  std_logic;   -- Error from I2C
      check_int_wait : in  std_logic;
      add_ready      : out std_logic;   -- Validates address
      add_card       : out std_logic_vector(4 downto 0);   -- FEC address
      last_card      : out std_logic;   -- At last card
      inta_not_en    : out std_logic;   -- Disable interrupts on branch A
      intb_not_en    : out std_logic);  -- Disable interrupts on branch B
  end component;

end msm_cards_status_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
