-------------------- Test bench of RCU MS Interrupt handler -------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_interrupt_handler_pack.all;

entity msm_interrupt_handler_tb is

end msm_interrupt_handler_tb;

-------------------------------------------------------------------------------

architecture test of msm_interrupt_handler_tb is
  -- component ports
  signal clk            : std_logic                     := '0';
  signal rstb           : std_logic                     := '0';
  signal interrupt      : std_logic                     := '0';
  signal csr1           : std_logic_vector(13 downto 0) := (others => '0');
  signal add_ready      : std_logic                     := '0';
  signal add_card       : std_logic_vector(4 downto 0)  := (others => '0');
  signal stop           : std_logic                     := '0';
  signal fec_al         : std_logic_vector(31 downto 0) := X"00030003";
  signal rdol           : std_logic_vector(31 downto 0) := X"00030003";
  signal seq_active     : std_logic                     := '0';
  signal master_end     : std_logic                     := '0';
  signal error          : std_logic                     := '0';
  signal last_card      : std_logic                     := '0';
  signal read_al        : std_logic;
  signal fec_al_fsc     : std_logic_vector(31 downto 0);
  signal we_fec_al_fsc  : std_logic;
  signal rdol_fsc       : std_logic_vector(31 downto 0);
  signal we_rdol_fsc    : std_logic;
  signal warning_to_dcs : std_logic;
  signal ih_busy        : std_logic;
  signal add_sm         : std_logic_vector(4 downto 0);
  signal sm             : std_logic_vector(15 downto 0);
  signal we_sm          : std_logic;
  signal exec           : std_logic;
  signal branch         : std_logic;
  signal fec_add        : std_logic_vector(3 downto 0);
  signal rnw            : std_logic;
  signal bcreg_add      : std_logic_vector(7 downto 0);
  signal bcdata         : std_logic_vector(15 downto 0);
  signal check_int_wait : std_logic;

  signal stop_i : std_logic;
  
  -- Clock period
  constant PERIOD : time := 25 ns;

  -- purpose: msm_Make sequence
  procedure sequence (
    signal   interrupt  : out std_logic;
    signal   add_card   : out std_logic_vector(4 downto 0);
    signal   csr1       : out std_logic_vector(13 downto 0);
    signal   master_end : out std_logic;
    constant hard       : in  std_logic;
    constant card       : in  std_logic_vector(4 downto 0)) is
  begin  -- sequence
    wait for 4 * PERIOD;
    interrupt  <= '1';
    wait for 2 * PERIOD;
    add_card   <= card;
    wait for 2 * PERIOD;
    csr1(13)   <= '1';
    csr1(0)    <= hard;
    csr1(1)    <= not hard;
    wait for 2 * PERIOD;
    interrupt  <= '0';
    wait for 4 * PERIOD;
    csr1       <= (others => '0');
    master_end <= '1';
    add_card   <= (others => '0');
  end sequence;
  
begin  -- test

  -- component instantiation
  dut : msm_interrupt_handler
    port map (
      clk            => clk,            -- [in]  Clock
      rstb           => rstb,           -- [in]  Async reset
      interrupt      => interrupt,      -- [in]  Interrupt
      csr1           => csr1,           -- [in]  BC status
      add_ready      => add_ready,      -- [in]  Address is ready
      add_card       => add_card,       -- [in]  Address of card
      stop           => stop,           -- [in]  Stop processing
      fec_al         => fec_al,         -- [in]  Active FECs
      rdol           => rdol,           -- [in]  Readout FECs
      seq_active     => seq_active,     -- [in]  Sequencer is active
      master_end     => master_end,     -- [in]  End from I2C
      error          => error,          -- [in]  Error from I2C bus
      last_card      => last_card,      -- [in]  At last possible card
      read_al        => read_al,        -- [out] Read from BC
      fec_al_fsc     => fec_al_fsc,     -- [out] Validated FECs
      we_fec_al_fsc  => we_fec_al_fsc,  -- [out] Write enable on active FECs
      rdol_fsc       => rdol_fsc,       -- [out] Validated RO FECs
      we_rdol_fsc    => we_rdol_fsc,    -- [out] Write enable on read-out FECs
      warning_to_dcs => warning_to_dcs,   -- [out] Warning to DCS
      ih_busy        => ih_busy,        -- [out] Interrupt handler busy
      add_sm         => add_sm,         -- [out] Address status mem.
      sm             => sm,             -- [out] Data for status mem
      we_sm          => we_sm,          -- [out] Write enable for status mem
      exec           => exec,           -- [out] Execute I2C
      branch         => branch,         -- [out] Branch select
      fec_add        => fec_add,        -- [out] FEC address
      rnw            => rnw,            -- [out] Read (not write) flag
      bcreg_add      => bcreg_add,      -- [out] BC register address
      bcdata         => bcdata,         -- [out] Data for BC
      check_int_wait => check_int_wait);  -- [out] Wait for interrupt

  -- clock generation
  clk <= not clk after PERIOD / 2;

  stopit : process (clk, rstb)
  begin  -- process stopit
    if rstb = '0' then                  -- asynchronous reset (active low)
      stop      <= '0';
      add_ready <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      stop      <= exec;
      add_ready <= read_al;
    end if;
  end process stopit;

  -- stop <= stop_i after 2 * PERIOD;
  
  -- purpose: msm_FECs
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_
  fecs : process (clk, rstb)
  begin  -- process fecs
    if rstb = '0' then                  -- asynchronous reset (active low)
      fec_al <= X"00030003";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_fec_al_fsc = '1' then
        fec_al <= fec_al_fsc;
      end if;
    end if;
  end process fecs;

  -- purpose: msm_FECs
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb
  -- outputs: msm_
  rdols : process (clk, rstb)
  begin  -- process fecs
    if rstb = '0' then                  -- asynchronous reset (active low)
      rdol <= X"00030003";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we_rdol_fsc = '1' then
        rdol <= rdol_fsc;
      end if;
    end if;
  end process rdols;

  -- waveform generation
  stim : process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';

    sequence(interrupt  => interrupt,
             add_card   => add_card,
             csr1       => csr1,
             master_end => master_end,
             hard       => '1',
             card       => '1' & X"1");
    wait until check_int_wait = '1';
    wait until check_int_wait = '0';
    
    sequence(interrupt  => interrupt,
             add_card   => add_card,
             csr1       => csr1,
             master_end => master_end,
             hard       => '0',
             card       => '1' & X"0");
    wait until check_int_wait = '1';
    wait until check_int_wait = '0';

    wait;                               -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration msm_interrupt_handler_vhdl of msm_interrupt_handler_tb is

  for test
    for dut : msm_interrupt_handler
      use entity msmodule_lib.msm_interrupt_handler(rtl);
    end for;
  end for;

end msm_interrupt_handler_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
