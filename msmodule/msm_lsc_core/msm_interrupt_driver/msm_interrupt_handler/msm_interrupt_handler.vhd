-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
--  Once this block receives an interrupt (from branch A or B) starts polling
--  the Active FEC to know which error generated the interrupt. The error is
--  classified in soft or hard and in function of that, the Interrupt Handler
--  mask the error or switch off the card.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity msm_interrupt_handler is
  
  port (
    clk            : in  std_logic;     -- Clock
    rstb           : in  std_logic;     -- Async reset
    interrupt      : in  std_logic;     -- Interrupt
    csr1           : in  std_logic_vector(13 downto 0);  -- BC status
    add_ready      : in  std_logic;     -- Address is ready
    add_card       : in  std_logic_vector(4 downto 0);   -- Address of card
    stop           : in  std_logic;     -- Stop processing
    fec_al         : in  std_logic_vector(31 downto 0);  -- Active FECs
    rdol           : in  std_logic_vector(31 downto 0);  -- Readout FECs
    seq_active     : in  std_logic;     -- Sequencer is active
    master_end     : in  std_logic;     -- End from I2C
    error          : in  std_logic;     -- Error from I2C bus
    last_card      : in  std_logic;     -- At last possible card
    read_al        : out std_logic;     -- Read from BC
    fec_al_fsc     : out std_logic_vector(31 downto 0);  -- Validated FECs
    we_fec_al_fsc  : out std_logic;     -- Write enable on active FECs
    rdol_fsc       : out std_logic_vector(31 downto 0);  -- Validated RO FECs
    we_rdol_fsc    : out std_logic;     -- Write enable on read-out FECs
    warning_to_dcs : out std_logic;     -- Warning to DCS
    ih_busy        : out std_logic;     -- Interrupt handler busy
    add_sm         : out std_logic_vector(4 downto 0);   -- Address status mem.
    sm             : out std_logic_vector(15 downto 0);  -- Data for status mem
    we_sm          : out std_logic;     -- Write enable for status mem
    exec           : out std_logic;     -- Execute I2C
    branch         : out std_logic;     -- Branch select
    fec_add        : out std_logic_vector(3 downto 0);   -- FEC address
    rnw            : out std_logic;     -- Read (not write) flag
    bcreg_add      : out std_logic_vector(7 downto 0);   -- BC register address
    bcdata         : out std_logic_vector(15 downto 0);  -- Data for BC
    check_int_wait : out std_logic);    -- Wait for interrupt

end msm_interrupt_handler;

architecture rtl of msm_interrupt_handler is

  type st_t is (idle,
                check_sequencer,
                rd_reg, rd_error,
                int_handling,
                mask_error,
                check_stop,
                check_int,
                wait_1cycle,
                wait_2cycle,
                wait_cnt);              -- State type

  signal branch_i  : std_logic;
  signal fec_add_i : std_logic_vector(3 downto 0);

  signal st_i         : st_t := idle;   -- State
  signal nx_st_i      : st_t := idle;   -- Next state
  signal hard_error_i : std_logic;      -- Hard error - turn of card
  signal soft_error_i : std_logic;      -- Soft error - 
  signal csr1_ready_i : std_logic;      -- Got CSR1 from I2C 
  signal merror_i     : std_logic;      -- Mask error
  signal wr_nwords_i  : std_logic;      -- Write number of words

  signal cnt_wait_clr_i : std_logic;             -- Clear wait counter
  signal cnt_wait_en_i  : std_logic;             -- Enable wait counter
  signal cnt_wait_i     : unsigned(4 downto 0);  -- wait counter

  signal cnt_word_clr_i : std_logic;             -- Clear wait counter
  signal cnt_word_en_i  : std_logic;             -- Enable counter of words
  signal cnt_word_i     : unsigned(4 downto 0);  -- Counter of words
  
begin  -- rtl
  -- Interrupt handling is not busy untill the normal mode has finished.
  ih_busy <= '0' when ((st_i = idle) or (st_i = check_sequencer)) else '1';
  -- Hard errors (will switch off card)
  -- True if
  --   Temperature over threshold
  --   Analogue current (bit 2) over threshold
  --   Digital current (bit 4) over threshold
  --   PASA power supply (bit 5) error
  --   ALTRO power supply (bit 6) error
  hard_error_i <= '1' when (csr1(0) = '1' or
                            csr1(2) = '1' or
                            csr1(4) = '1' or
                            csr1(5) = '1' or
                            csr1(6) = '1') else '0';
  -- Soft errors (ignored)
  -- True if
  --   Analogue voltage (bit 1) under threshold
  --   Digital voltage (bit 3) under threshold
  --   Missed sample clock cycles when doing sparse read-out
  soft_error_i <= '1' when (csr1(1) = '1' or
                            csr1(3) = '1' or
                            csr1(7) = '1') else '0';
  -- Reset for the counter of the FECs in cards_status
  warning_to_dcs <= wr_nwords_i;
  -- Number of FECs that send te interrupt at the same time
  cnt_word_clr_i <= '1' when (st_i = idle) else '0';
  --
  check_int_wait <= cnt_wait_clr_i;
  --
  branch         <= branch_i;
  fec_add        <= fec_add_i;

  -- purpose: msm_Update state
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, nx_st_i
  -- outputs: msm_st_i
  next_state : process (clk, rstb)
  begin  -- process next_state
    if rstb = '0' then                  -- asynchronous reset (active low)
      st_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process next_state;

  -- purpose: msm_Get the next state
  -- type   : msm_combinational
  -- inputs : msm_st_i, interrupt, seq_active, stop, csr1, hard_error_i,
  --          master_end, error, last_card, cnt_wait
  -- outputs: msm_read_al, csr1_ready_i, merror_i, cnt_word_en_i, wr_nwords_i,
  --          cnt_wait_clr_i, cnt_wait_en_i, cnt_word_en_i, nx_st_i
  fsm : process (st_i,
                 interrupt,
                 seq_active,
                 stop,
                 csr1,
                 hard_error_i,
                 master_end,
                 error,
                 last_card,
                 cnt_wait_i)
  begin  -- process fsm

    case st_i is
      when idle =>
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        if interrupt = '1' then
          nx_st_i <= check_sequencer;
        else
          nx_st_i <= idle;
        end if;
        
      when check_sequencer =>           -- Wait for sequencer to end
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        if seq_active = '1' then        -- Seq. still active in normal mode
          cnt_word_en_i <= '0';
          nx_st_i       <= check_sequencer;
        else
          cnt_word_en_i <= '1';
          nx_st_i       <= rd_reg;
        end if;

      when rd_reg =>                    -- Read register
        read_al        <= '1';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        if error = '1' then             -- No answer from BC, hardware
                                        -- interrupt: msm_error in the related
                                        -- power regulators
          nx_st_i <= wait_1cycle;
        elsif last_card = '1' then      -- Hardware interrupt, but BC gives no
                                        -- information: msm_error in the power
                                        -- regulator of monitor ADCs and EPROM
          nx_st_i <= wait_1cycle;
        elsif stop = '1' then           -- At end
          nx_st_i <= rd_error;
        else
          nx_st_i <= rd_reg;
        end if;

      when rd_error =>                  -- Check for error or interrupt
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        if csr1(13) = '1' then
          nx_st_i <= int_handling;
        else
          nx_st_i <= rd_reg;
        end if;

      when int_handling =>              -- Handle interrupt
        read_al        <= '0';
        csr1_ready_i   <= '1';
        merror_i       <= '0';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        if hard_error_i = '1' then
          nx_st_i <= wait_cnt;
          -- For when we had soft errors
          -- elsif soft_error_i = '1' then 
          -- nx_st_i <= mask_error;
        else
          nx_st_i <= mask_error;
          -- For when we had soft errors
          -- nx_st_i <= wait_cnt;
        end if;
        
      when wait_cnt =>                  -- Wait for counter
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '1';
        if cnt_wait_i = "10100" then    -- count to 20 (800ns)
          nx_st_i <= check_int;
        else
          nx_st_i <= wait_cnt;
        end if;

      when mask_error =>                -- Ignore this error
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '1';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        nx_st_i        <= check_stop;

      when check_int =>                 -- Check the interrupt
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        cnt_wait_clr_i <= '1';
        cnt_wait_en_i  <= '0';
        if interrupt = '1' then
          cnt_word_en_i <= '1';
          wr_nwords_i   <= '0';
          nx_st_i       <= rd_reg;
        elsif master_end = '1' then
          cnt_word_en_i <= '0';
          wr_nwords_i   <= '1';
          nx_st_i       <= idle;
        else
          cnt_word_en_i <= '0';
          wr_nwords_i   <= '0';
          nx_st_i       <= check_int;
        end if;
        
      when check_stop =>
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '1';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        if stop = '1' then              -- wait until error is masked in BC
          nx_st_i <= wait_cnt;
        else
          nx_st_i <= check_stop;
        end if;
        
      when wait_1cycle =>
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        nx_st_i        <= wait_2cycle;

      when wait_2cycle =>
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        nx_st_i        <= check_int;

      when others =>
        read_al        <= '0';
        csr1_ready_i   <= '0';
        merror_i       <= '0';
        cnt_word_en_i  <= '0';
        wr_nwords_i    <= '0';
        cnt_wait_clr_i <= '0';
        cnt_wait_en_i  <= '0';
        nx_st_i        <= idle;

    end case;
  end process fsm;

  -- purpose: msm_Output of I2C addresses and data
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, st_i
  -- outputs: msm_exec, branch_i, fec_add_i, rnw, bcreg_add, bcdata
  output_addresses : process (clk, rstb)
  begin  -- process output_addresses
    if rstb = '0' then                  -- asynchronous reset (active low)
      exec      <= '0';
      branch_i  <= '0';
      fec_add_i <= (others => '0');
      rnw       <= '1';
      bcreg_add <= (others => '0');
      bcdata    <= (others => '1');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if add_ready = '1' then
        exec      <= '1';
        branch_i  <= add_card(4);
        fec_add_i <= add_card(3 downto 0);
        rnw       <= '1';
        bcreg_add <= X"12";
      elsif merror_i = '1' then
        exec      <= '1';
        branch_i  <= add_card(4);
        fec_add_i <= add_card(3 downto 0);
        rnw       <= '0';
        bcreg_add <= X"11";
        if csr1(0) = '1' then
          bcdata(0) <= '0';
        elsif csr1(1) = '1' then
          bcdata(1) <= '0';
        elsif csr1(3) = '1' then
          bcdata(3) <= '0';
        elsif csr1(7) = '1' then
          bcdata(7) <= '0';
        end if;
      else
        exec <= '0';
      end if;
    end if;
  end process output_addresses;

  -- purpose: msm_Count number of words
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, cnt_word_clr_i, cnt_word_en_i
  -- outputs: msm_cnt_word_i
  word_counter : process (clk, rstb)
  begin  -- process word_counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_word_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if cnt_word_clr_i = '1' then
        cnt_word_i <= (others => '0');
      elsif cnt_word_en_i = '1' then
        cnt_word_i <= cnt_word_i + "00001";
      end if;
    end if;
  end process word_counter;

  -- purpose: msm_Output validated FECs for enabled and readout
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, csr1_ready_i, hard_error_i, soft_error_i, fec_al, rdol
  -- outputs: msm_fec_al_fsc, rdol_fsc, we_fec_al_fsc, we_rdol_fsc
  validated_fecs : process (clk, rstb)
  begin  -- process validated_fecs
    if rstb = '0' then                  -- asynchronous reset (active low)
      fec_al_fsc    <= (others => '0');
      rdol_fsc      <= (others => '0');
      we_fec_al_fsc <= '0';
      we_rdol_fsc   <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if csr1_ready_i = '1' then
        if hard_error_i = '1' then
          fec_al_fsc(to_integer(unsigned(add_card))) <= '0';
          we_fec_al_fsc                              <= '1';
        else
          -- For when we had soft errors
          -- elsif soft_error_i = '1' then
          rdol_fsc(to_integer(unsigned(add_card))) <= '0';
          we_rdol_fsc                              <= '1';
          -- For when we had soft errors
          -- else
          --   fec_al_fsc(add_card) <= '0';
          --   we_fec_al_fsc <= '1';
        end if;
      else
        fec_al_fsc    <= fec_al;
        rdol_fsc      <= rdol;
        we_fec_al_fsc <= '0';
        we_rdol_fsc   <= '0';
      end if;
    end if;
  end process validated_fecs;


  -- purpose: msm_Addres and write to status memory
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, wr_nwords_i, last_card, error
  -- outputs: msm_add_sm, sm, wr_sm.
  address_status : process (clk, rstb)
  begin  -- process address_status
    if rstb = '0' then                  -- asynchronous reset (active low)
      add_sm <= (others => '0');
      sm     <= (others => '0');
      we_sm  <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge

      if (wr_nwords_i = '1') then
        add_sm          <= (others => '0');
        sm(15 downto 5) <= (others => '0');
        sm(4 downto 0)  <= std_logic_vector(cnt_word_i);
        we_sm           <= '1';
      elsif last_card = '1' then
        add_sm          <= std_logic_vector(cnt_word_i);
        sm(15)          <= '1';              -- Not identified
        sm(14)          <= add_card(4);      -- Branch
        sm(13 downto 0) <= (others => '0');  -- No information
        we_sm           <= '1';
      elsif error = '1' then
        add_sm           <= std_logic_vector(cnt_word_i);
        sm(15)           <= '0';             -- Valid information
        sm(14 downto 10) <= branch_i & fec_add_i;
        sm(9)            <= '0';             -- FEC is off
        sm(8)            <= '1';             -- Hardware interrupt
        sm(7 downto 0)   <= (others => '0');
        we_sm            <= '1';
      elsif csr1_ready_i = '1' then
        add_sm           <= std_logic_vector(cnt_word_i);
        sm(15)           <= '0';             -- Valid information
        sm(14 downto 10) <= branch_i & fec_add_i;
        sm(8)            <= '0';             -- 
        sm(7 downto 0)   <= csr1(7 downto 0);
        we_sm            <= '1';
        if (hard_error_i = '1') then
          sm(9) <= '0';                      -- Card is off
        else
          -- When we had soft-errors
          -- elsif soft_error_i = '1' then
          sm(9) <= '1';                      -- Card is on
          -- else
          --   sm(9) <= '0';                 -- Cards is off
        end if;
      else
        we_sm <= '0';
      end if;
    end if;
  end process address_status;

  -- purpose: msm_Wait counter - counts to 20 (or 800ns)
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, cnt_wait_clr_i, cnt_wait_en_i
  -- outputs: msm_cnt_wait_i
  wait_counter : process (clk, rstb)
  begin  -- process wait_counter
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_wait_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if cnt_wait_clr_i = '1' then
        cnt_wait_i <= (others => '0');
      elsif cnt_wait_en_i = '1' then
        cnt_wait_i <= cnt_wait_i + "00001";
      end if;
    end if;
  end process wait_counter;
end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_interrupt_handler_pack is

  
  component msm_interrupt_handler
    port (
      clk            : in  std_logic;   -- Clock
      rstb           : in  std_logic;   -- Async reset
      interrupt      : in  std_logic;   -- Interrupt
      csr1           : in  std_logic_vector(13 downto 0);  -- BC status
      add_ready      : in  std_logic;   -- Address is ready
      add_card       : in  std_logic_vector(4 downto 0);  -- Address of card
      stop           : in  std_logic;   -- Stop processing
      fec_al         : in  std_logic_vector(31 downto 0);  -- Active FECs
      rdol           : in  std_logic_vector(31 downto 0);  -- Readout FECs
      seq_active     : in  std_logic;   -- Sequencer is active
      master_end     : in  std_logic;   -- End from I2C
      error          : in  std_logic;   -- Error from I2C bus
      last_card      : in  std_logic;   -- At last possible card
      read_al        : out std_logic;   -- Read from BC
      fec_al_fsc     : out std_logic_vector(31 downto 0);  -- Validated FECs
      we_fec_al_fsc  : out std_logic;   -- Write enable on active FECs
      rdol_fsc       : out std_logic_vector(31 downto 0);  -- Validated RO FECs
      we_rdol_fsc    : out std_logic;   -- Write enable on read-out FECs
      warning_to_dcs : out std_logic;   -- Warning to DCS
      ih_busy        : out std_logic;   -- Interrupt handler busy
      add_sm         : out std_logic_vector(4 downto 0);  -- Address status mem.
      sm             : out std_logic_vector(15 downto 0);  -- Data for status.
      we_sm          : out std_logic;   -- Write enable for status mem
      exec           : out std_logic;   -- Execute I2C
      branch         : out std_logic;   -- Branch select
      fec_add        : out std_logic_vector(3 downto 0);  -- FEC address
      rnw            : out std_logic;   -- Read (not write) flag
      bcreg_add      : out std_logic_vector(7 downto 0);  -- BC register address
      bcdata         : out std_logic_vector(15 downto 0);  -- Data for BC
      check_int_wait : out std_logic);  -- Wait for interrupt
  end component;
  
end msm_interrupt_handler_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
