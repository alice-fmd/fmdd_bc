-------------------------------------------------------------------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- Interrupt driver
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_cards_status_pack.all;
use msmodule_lib.msm_interrupt_handler_pack.all;

entity msm_interrupt_driver is
  
  port (
    clk            : in  std_logic;     -- Clock
    rstb           : in  std_logic;     -- Async reset
    seq_active     : in  std_logic;     -- Sequencer active
    fec_al         : in  std_logic_vector(31 downto 0);  -- Active FECs
    rdol           : in  std_logic_vector(31 downto 0);  -- Read-out enab. FECs
    stop           : in  std_logic;     -- Stop signal
    interruptA     : in  std_logic;     -- Interrupt from branch A
    interruptB     : in  std_logic;     -- Interrupt from branch B
    csr1           : in  std_logic_vector(13 downto 0);  -- Status from FEC BC
    master_end     : in  std_logic;     -- End of master SM
    error          : in  std_logic;     -- Error from I2C bus
    fec_al_fsc     : out std_logic_vector(31 downto 0);  -- Output list of FECs
    we_fec_al_fsc  : out std_logic;     -- Write enable for FEC list
    rdol_fsc       : out std_logic_vector(31 downto 0);  -- Output readout FECs
    we_rdol_fsc    : out std_logic;     -- Write enable for RDO list
    ih_busy        : out std_logic;     -- Interrupt handler busy
    exec           : out std_logic;     -- Execution flag
    branch         : out std_logic;     -- Branch select
    rnw            : out std_logic;     -- Read (not write) flag
    fec_add        : out std_logic_vector(3 downto 0);  -- FEC address
    bcreg_add      : out std_logic_vector(7 downto 0);  -- BC register address
    data           : out std_logic_vector(15 downto 0);  -- Output data
    add_sr         : out std_logic_vector(4 downto 0);  -- Status memory address
    sr             : out std_logic_vector(15 downto 0);  -- Data for status mem
    we_sr          : out std_logic;     -- Write enable for status memory
    warning_to_dcs : out std_logic;     -- Warning to DCS flag
    inta_not_en    : out std_logic;     -- Interrupts on branch A disabled
    intb_not_en    : out std_logic);    -- Interrupts on branch B disabled

end msm_interrupt_driver;

architecture rtl of msm_interrupt_driver is

  signal interrupt_i      : std_logic;
  signal add_ready_i      : std_logic;
  signal add_card_i       : std_logic_vector(4 downto 0);
  signal check_int_wait_i : std_logic;
  signal last_card_i      : std_logic;
  signal read_al_i        : std_logic;
  
begin  -- rtl
  interrupt_i <= interruptA or interruptB;


  cards_status_1 : msm_cards_status
    port map (
      clk            => clk,            -- [in]  clock
      rstb           => rstb,           -- [in]  async reset
      interruptA     => interruptA,     -- [in]  Interrupt on branch A
      interruptB     => interruptB,     -- [in]  Interrupt on branch B
      read_al        => read_al_i,      -- [in]  Read from BC's
      fec_al         => fec_al,         -- [in]  Active list
      stop           => stop,           -- [in]  Stop
      error          => error,          -- [in]  Error from I2C
      check_int_wait => check_int_wait_i,  -- [in]
      add_ready      => add_ready_i,    -- [out] Validates address
      add_card       => add_card_i,     -- [out] FEC address
      last_card      => last_card_i,    -- [out] At last card
      inta_not_en    => inta_not_en,    -- [out] Disable interrupts on branch A
      intb_not_en    => intb_not_en);   -- [out] Disable interrupts on branch B

  interrupt_handler_1 : msm_interrupt_handler
    port map (
      clk            => clk,            -- [in]  Clock
      rstb           => rstb,           -- [in]  Async reset
      interrupt      => interrupt_i,    -- [in]  Interrupt
      csr1           => csr1,           -- [in]  BC status
      add_ready      => add_ready_i,    -- [in]  Address is ready
      add_card       => add_card_i,     -- [in]  Address of card
      stop           => stop,           -- [in]  Stop processing
      fec_al         => fec_al,         -- [in]  Active FECs
      rdol           => rdol,           -- [in]  Readout FECs
      seq_active     => seq_active,     -- [in]  Sequencer is active
      master_end     => master_end,     -- [in]  End from I2C
      error          => error,          -- [in]  Error from I2C bus
      last_card      => last_card_i,    -- [in]  At last possible card
      read_al        => read_al_i,      -- [out] Read from BC
      fec_al_fsc     => fec_al_fsc,     -- [out] Validated FECs
      we_fec_al_fsc  => we_fec_al_fsc,  -- [out] Write enable on active FECs
      rdol_fsc       => rdol_fsc,       -- [out] Validated RO FECs
      we_rdol_fsc    => we_rdol_fsc,    -- [out] Write enable on read-out FECs
      warning_to_dcs => warning_to_dcs,     -- [out] Warning to DCS
      ih_busy        => ih_busy,        -- [out] Interrupt handler busy
      add_sm         => add_sr,         -- [out] Address status mem.
      sm             => sr,             -- [out] Data for status mem
      we_sm          => we_sr,          -- [out] Write enable for status mem
      exec           => exec,           -- [out] Execute I2C
      branch         => branch,         -- [out] Branch select
      fec_add        => fec_add,        -- [out] FEC address
      rnw            => rnw,            -- [out] Read (not write) flag
      bcreg_add      => bcreg_add,      -- [out] BC register address
      bcdata         => data,           -- [out] Data for BC
      check_int_wait => check_int_wait_i);  -- [out] Wait for interrupt

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_interrupt_driver_pack is

  component msm_interrupt_driver
    port (
      clk            : in  std_logic;   -- Clock
      rstb           : in  std_logic;   -- Async reset
      seq_active     : in  std_logic;   -- Sequencer active
      fec_al         : in  std_logic_vector(31 downto 0);  -- Active FECs
      rdol           : in  std_logic_vector(31 downto 0);  -- Read-out FECs
      stop           : in  std_logic;   -- Stop signal
      interruptA     : in  std_logic;   -- Interrupt from branch A
      interruptB     : in  std_logic;   -- Interrupt from branch B
      csr1           : in  std_logic_vector(13 downto 0);  -- Status from FEC
      master_end     : in  std_logic;   -- End of master SM
      error          : in  std_logic;   -- Error from I2C bus
      fec_al_fsc     : out std_logic_vector(31 downto 0);  -- Output of FECs
      we_fec_al_fsc  : out std_logic;   -- Write enable for FEC list
      rdol_fsc       : out std_logic_vector(31 downto 0);  -- Output ro FECs
      we_rdol_fsc    : out std_logic;   -- Write enable for RDO list
      ih_busy        : out std_logic;   -- Interrupt handler busy
      exec           : out std_logic;   -- Execution flag
      branch         : out std_logic;   -- Branch select
      rnw            : out std_logic;   -- Read (not write) flag
      fec_add        : out std_logic_vector(3 downto 0);  -- FEC address
      bcreg_add      : out std_logic_vector(7 downto 0);  -- BC register address
      data           : out std_logic_vector(15 downto 0);  -- Output data
      add_sr         : out std_logic_vector(4 downto 0);  -- Status memory add
      sr             : out std_logic_vector(15 downto 0);  -- Data for status
      we_sr          : out std_logic;   -- Write enable for status memory
      warning_to_dcs : out std_logic;   -- Warning to DCS flag
      inta_not_en    : out std_logic;   -- Interrupts on branch A disabled
      intb_not_en    : out std_logic);  -- Interrupts on branch B disabled
  end component;

end msm_interrupt_driver_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
