-------------------------------------------------------------------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-------------------------------------------------------------------------------
-- Monitoring and safety module top level
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_clock_master_pack.all;
use msmodule_lib.msm_comm_selection_pack.all;
use msmodule_lib.msm_ffd_pack.all;
use msmodule_lib.msm_ram_sm_pack.all;
use msmodule_lib.msm_result_pack.all;
use msmodule_lib.msm_sync_pack.all;
use msmodule_lib.msm_lsc_core_pack.all;
use msmodule_lib.msm_error_module_pack.all;
use msmodule_lib.msm_signals_drv_pack.all;


entity msm_msmodule is
  
  port (
    clk                : in  std_logic;  -- Clock
    rstb               : in  std_logic;  -- Async reset
    rstb_global        : in  std_logic;  -- Async global reset
    add_siu            : in  std_logic_vector(15 downto 0);  -- SIU: Address
    we_siu             : in  std_logic;  -- SIU: write enable
    datain_siu         : in  std_logic_vector(31 downto 0);  -- SIU: input data
    add_dcs            : in  std_logic_vector(15 downto 0);  -- DCS: Address
    we_dcs             : in  std_logic;  -- DCS: write enable
    datain_dcs         : in  std_logic_vector(31 downto 0);  -- DCS: input data
    dcsnsiu            : in  std_logic;  -- CORE: DCS (not SIU)
    interrupta_in      : in  std_logic;  -- BUS: interrupt on A
    interruptb_in      : in  std_logic;  -- BUS: interrupt on B
    sda_outa           : in  std_logic;  -- I2C: data in from A
    sda_outb           : in  std_logic;  -- I2C: data in from B
    dataout_rcuversion : in  std_logic_vector(31 downto 0);  -- CORE: FW vers
    dataout_bpversion  : in  std_logic;  -- CORE: TPC/FMD or PHOS bus
    sda_ina            : out std_logic;  -- I2C: data out for A
    sda_inb            : out std_logic;  -- I2C: data out for B
    scla               : out std_logic;  -- I2C: clock for A
    sclb               : out std_logic;  -- I2C: clock for B
    warningtodcs       : out std_logic;  -- CORE: warning
    we_fec_al_fsc      : out std_logic;  -- CORE: Write enable for AFL
    fec_al_fsc         : out std_logic_vector(31 downto 0);  -- CORE: Valid FECs
    dataout_msm        : out std_logic_vector(31 downto 0);  -- CORE: data out
    rst_sclksync       : out std_logic;  -- reset sclk sync.
    fec_al             : in  std_logic_vector(31 downto 0));  -- CORE: AFL 
end msm_msmodule;

architecture rtl of msm_msmodule is
  signal clk_master_i : std_logic;

  signal scadd_i  : std_logic_vector(15 downto 0);  -- SC address
  signal scdata_i : std_logic_vector(15 downto 0);  -- SC data
  signal scexec_i : std_logic;                      -- SC exec

  signal execin_i        : std_logic;   -- Internal exec
  signal exec_dcs_i      : std_logic;   -- DCS: Execute
  signal branch_dcs_i    : std_logic;   -- DCS: branch select
  signal bcast_dcs_i     : std_logic;   -- DCS: Broadcast
  signal rnw_dcs_i       : std_logic;   -- DCS: Read (not write)
  signal fec_add_dcs_i   : std_logic_vector(3 downto 0);   -- DCS: FEC
  signal bcreg_add_dcs_i : std_logic_vector(7 downto 0);   -- DCS: BC reg. add.
  signal bcdata_dcs_i    : std_logic_vector(15 downto 0);  -- DCS: BC data

  signal wesmmaster_i     : std_logic;
  signal weresultmaster_i : std_logic;

  signal wesm_fsc_i     : std_logic;
  signal weresult_fsc_i : std_logic;

  signal seq_active_i : std_logic;
  signal nack_i       : std_logic;
  signal rst_errreg_i : std_logic;
  signal errreg_i     : std_logic_vector(1 downto 0);

  signal rst_resultreg_i  : std_logic;
  signal weresult_i       : std_logic;
  signal dataresult_i     : std_logic_vector(20 downto 0);
  signal dataresult_fsc_i : std_logic_vector(20 downto 0);
  signal result_lsc_i     : std_logic_vector(20 downto 0);
  signal addsm_fsc_i      : std_logic_vector(4 downto 0);
  signal datasm_fsc_i     : std_logic_vector(15 downto 0);

  signal addsm_i  : std_logic_vector(4 downto 0);
  signal datasm_i : std_logic_vector(15 downto 0);
  signal dosm_i   : std_logic_vector(15 downto 0);
  signal wesm_i   : std_logic;

  signal exec_in_i : std_logic;

  signal interrupta_i  : std_logic;
  signal interruptb_i  : std_logic;
  signal inta_enable_i : std_logic;
  signal intb_enable_i : std_logic;
  signal inta_noten_i  : std_logic;
  signal intb_noten_i  : std_logic;

  signal sda_out_i  : std_logic;
  signal sda_outa_i : std_logic;
  signal sda_outb_i : std_logic;
  signal sda_ack_i  : std_logic;

  signal rdol_i        : std_logic_vector(31 downto 0) := X"FFFFFFFF";
  signal rdol_fsc_i    : std_logic_vector(31 downto 0);
  signal we_rdol_fsc_i : std_logic;

  signal fec_al_fsc_i    : std_logic_vector(31 downto 0);
  signal we_fec_al_fsc_i : std_logic;
  
  
begin  -- rtl
  -- purpose: Collect combinatorics
  combinatorics : block
  begin  -- block combinatorics
    interrupta_i <= not interrupta_in when inta_enable_i = '1' else '0';
    interruptb_i <= not interruptb_in when intb_enable_i = '1' else '0';

    sda_outa_i <= sda_outa;
    sda_outb_i <= sda_outb;
    sda_out_i  <= '1' when (sda_outa_i = '1' and sda_outb_i = '1') else '0';

    we_fec_al_fsc <= we_fec_al_fsc_i;
    fec_al_fsc    <= fec_al_fsc_i;
    
  end block combinatorics;

  clock_master_1 : msm_clock_master
    port map (
      clk40      => clk,                -- [in]  Input clock
      reset      => rstb,               -- [in]  Aync reset
      clk_master => clk_master_i);      -- [out] Output 'clock' @ 20MHz

  
  sync_1 : msm_sync
    port map (
      clk    => clk,                    -- [in]  Clock
      rstb   => rstb,                   -- [in]  Async reset
      input  => weresultmaster_i,       -- [in]  Input
      output => weresult_fsc_i);        -- [out] Output

  sync_2 : msm_sync
    port map (
      clk    => clk,                    -- [in]  Clock
      rstb   => rstb,                   -- [in]  Async reset
      input  => wesmmaster_i,           -- [in]  Input
      output => wesm_fsc_i);            -- [out] Output

  ffd_1 : msm_ffd
    port map (
      clk  => clk,                      -- [in]  Clock
      arst => rstb,                     -- [in]  Async reset
      d    => sda_out_i,                -- [in]  Input
      q    => sda_ack_i);               -- [out] Output

  error_module_1 : msm_error_module
    port map (
      clk         => clk,               -- [in]  Clock
      rstb        => rstb,              -- [in]  Async reset
      nack        => nack_i,            -- [in]  No acknowledge from FEC
      seq_active  => seq_active_i,      -- [in]  Sequencer is active
      exec_in     => exec_in_i,         -- [in]  Interrupt handler execute
      bcast_dcs   => bcast_dcs_i,       -- [in]  Broadcast from DCS
      branch_dcs  => branch_dcs_i,      -- [in]  Branch from DCS
      fec_add_dcs => fec_add_dcs_i,     -- [in]  FEC address from DCS
      fec_al      => fec_al,            -- [in]  Active FECs
      rst_errreg  => rst_errreg_i,      -- [in]  Reset errors
      errreg      => errreg_i,          -- [out] Error register
      exec_dcs    => exec_dcs_i);       -- [out] Execute DCS

  result_1 : msm_result
    port map (
      clk           => clk,              -- [in]  Clock
      rstb          => rstb,             -- [in]  Async reset
      rst_resultreg => rst_resultreg_i,  -- [in]  Syncreset
      we_rm         => weresult_i,       -- [in]  Write enable
      data_rm       => dataresult_i,     -- [in]  Data for register
      result_lsc    => result_lsc_i);    -- [out] Output of reg.

  ram_sm_1 : msm_ram_sm
    port map (
      clk     => clk,                   -- [in]  Clock
      address => addsm_i,               -- [in]  Address
      data    => datasm_i,              -- [in]  Input
      we      => wesm_i,                -- [in]  Write enable
      q       => dosm_i);               -- [out] output

  comm_selection_1 : msm_comm_selection
    port map (
      sc_add    => scadd_i,             -- [in]  SC address
      sc_data   => scdata_i,            -- [in]  SC data
      sc_exec   => scexec_i,            -- [in]  Execute
      exec_in   => exec_in_i,           -- [out] Execute interrupt handler
      branch    => branch_dcs_i,        -- [out] Branch select
      bcast     => bcast_dcs_i,         -- [out] Broadcast select
      rnw       => rnw_dcs_i,           -- [out] Read (not write)
      fec_add   => fec_add_dcs_i,       -- [out] FEC address
      bcreg_add => bcreg_add_dcs_i,     -- [out] BC register address
      bc_data   => bcdata_dcs_i);       -- [out] Data


  lsc_core_1 : msm_lsc_core
    port map (
      clk            => clk_master_i,      -- [in]  Clock
      rstb           => rstb,              -- [in]  Async reset
      sda_out        => sda_out_i,         -- [in]  I2C: data in
      sda_ack        => sda_ack_i,         -- [in]  I2C: data in
      fec_al         => fec_al,            -- [in]  Active FECs
      rdol           => rdol_i,            -- [in]  readout fecs
      exec_dcs       => exec_dcs_i,        -- [in]  Execute I2C
      bcast_dcs      => bcast_dcs_i,       -- [in]  Broadcast
      rnw_dcs        => rnw_dcs_i,         -- [in]  Read (not write)
      branch_dcs     => branch_dcs_i,      -- [in]  Branch from DCS
      fec_add_dcs    => fec_add_dcs_i,     -- [in]  FEC address
      bcreg_add_dcs  => bcreg_add_dcs_i,   -- [in]  BC register address
      bcdata_dcs     => bcdata_dcs_i,      -- [in]  Data for BC
      interruptA     => interruptA_i,      -- [in]  Interrupt on
      interruptB     => interruptB_i,      -- [in]  Interrupt on B
      dataresult_fsc => dataresult_fsc_i,  -- [out] I2C: msm_result
      weresultmaster => weresultmaster_i,  -- [out] I2C: msm_result write enab.
      fec_al_fsc     => fec_al_fsc_i,      -- [out] Valid FECs
      we_fec_al_fsc  => we_fec_al_fsc_i,   -- [out] Write enable for FECs
      rdol_fsc       => rdol_fsc_i,        -- [out] Readout FECs
      we_rdol_fsc    => we_rdol_fsc_i,     -- [out] Write enab. for RO FECs
      addsm          => addsm_fsc_i,       -- [out] Address for STATUS
      sm             => datasm_fsc_i,      -- [out] Data for STATUS mem
      wesm           => wesmmaster_i,      -- [out] Write enable for STATUS MEM
      seq_active     => seq_active_i,      -- [out] Sequencer active
      sclA           => sclA,              -- [out] I2C: Clock
      sda_inA        => sda_inA,           -- [out] I2C: data out
      sclB           => sclB,              -- [out] I2C: Clock
      sda_inB        => sda_inB,           -- [out] I2C: data out
      error          => nack_i,            -- [out] Error in I2C
      warningtodcs   => warningtodcs,      -- [out] Warning
      inta_noten     => inta_noten_i,      -- [out] Interrupt branch A masked
      intb_noten     => intb_noten_i);     -- [out] Interrupt branch B masked

  signals_drv_1 : msm_signals_drv
    port map (
      clk                => clk,        -- [in]  Clock
      rstb               => rstb,       -- [in]  Async reset
      add_siu            => add_siu,    -- [in]  SIU: Address
      we_siu             => we_siu,     -- [in]  SIU: Write enable
      datain_siu         => datain_siu,     -- [in]  SIU : write data
      add_dcs            => add_dcs,    -- [in]  DCS: Address
      we_dcs             => we_dcs,     -- [in]  DCS: Write enable
      datain_dcs         => datain_dcs,     -- [in]  DCS : write data
      dcsnsiu            => dcsnsiu,    -- [in]  BUS: DCS (not SIU)
      addsm_fsc          => addsm_fsc_i,    -- [in]  INT: SM address
      datasm_fsc         => datasm_fsc_i,   -- [in]  INT: SM data
      wesm_fsc           => wesm_fsc_i,     -- [in]  INT: SM write enable
      dataresult_fsc     => dataresult_fsc_i,    -- [in]  INT: msm_result data
      weresult_fsc       => weresult_fsc_i,   -- [in]  INT: Write enab. res.
      fec_al_fsc         => fec_al_fsc_i,   -- [in]  INT: Act. FECs
      we_fec_al_fsc      => we_fec_al_fsc_i,  -- [in]  INT: Act. FECs write enab
      rdol_fsc           => rdol_fsc_i,     -- [in]  INT: RO FECs
      we_rdol_fsc        => we_rdol_fsc_i,  -- [in]  INT: RO FECs write enab.
      result_lsc         => result_lsc_i,   -- [in]  INT: from here
      rdol               => rdol_i,     -- [in]  CORE: RO list
      dosm               => dosm_i,     -- [in]  INT: data output
      errreg             => errreg_i,   -- [in]  REG: errors
      inta_not_en        => inta_noten_i,   -- [in]  REG: interrupts A disabled
      intb_not_en        => intb_noten_i,   -- [in]  REG: interrupts B disabled
      dataout_rcuversion => dataout_rcuversion,  -- [in]  CORE: FW version
      addsm              => addsm_i,    -- [out] INT: SM address
      datasm             => datasm_i,   -- [out] INT: SM data
      wesm               => wesm_i,     -- [out] INT: SM write enab.
      dataresult         => dataresult_i,   -- [out] INT: Result data
      weresult           => weresult_i,     -- [out] INT: Result write enab.
      data_fec_al        => open,       -- [out] CORE: Valid FECs
      we_fec_al          => open,       -- [out] CORE: FEC write enab.
      data_rdol          => open,       -- [out] CORE: Valid FECs
      we_rdol            => open,       -- [out] CORE: FEC write enab.
      inta_enable        => inta_enable_i,  -- [out] INT: Interrupts on A enable
      intb_enable        => intb_enable_i,  -- [out] INT: Interrupts on B enable
      rst_errreg         => rst_errreg_i,   -- [out] INT: reset errors
      rst_resultreg      => rst_resultreg_i,  -- [out] INT: reset result
      rst_sclksync       => rst_sclksync,   -- [out] INT: reset sync of I2C
      dataout_msm        => dataout_msm,    -- [out] CORE: out data
      scadd              => scadd_i,    -- [out] CORE: I2C addr
      scdata             => scdata_i,   -- [out] CORE: I2C data
      scexec             => scexec_i);  -- [out] CORE : I2C execute

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_msmodule_pack is

  
  component msm_msmodule
    port (
      clk                : in  std_logic;  -- Clock
      rstb               : in  std_logic;  -- Async reset
      rstb_global        : in  std_logic;  -- Async global reset
      add_siu            : in  std_logic_vector(15 downto 0);  -- SIU: Address
      we_siu             : in  std_logic;  -- SIU: write enable
      datain_siu         : in  std_logic_vector(31 downto 0);  -- SIU: input dat
      add_dcs            : in  std_logic_vector(15 downto 0);  -- DCS: Address
      we_dcs             : in  std_logic;  -- DCS: write enable
      datain_dcs         : in  std_logic_vector(31 downto 0);  -- DCS: input dat
      dcsnsiu            : in  std_logic;  -- CORE: DCS (not SIU)
      interrupta_in      : in  std_logic;  -- BUS: interrupt on A
      interruptb_in      : in  std_logic;  -- BUS: interrupt on B
      sda_outa           : in  std_logic;  -- I2C: data in from A
      sda_outb           : in  std_logic;  -- I2C: data in from B
      dataout_rcuversion : in  std_logic_vector(31 downto 0);  -- CORE: FW vers
      dataout_bpversion  : in  std_logic;  -- CORE: TPC/FMD or PHOS bus
      sda_ina            : out std_logic;  -- I2C: data out for A
      sda_inb            : out std_logic;  -- I2C: data out for B
      scla               : out std_logic;  -- I2C: clock for A
      sclb               : out std_logic;  -- I2C: clock for B
      warningtodcs       : out std_logic;  -- CORE: warning
      we_fec_al_fsc      : out std_logic;  -- CORE: Write enable for AFL
      fec_al_fsc         : out std_logic_vector(31 downto 0);  -- CORE: Valid
      dataout_msm        : out std_logic_vector(31 downto 0);  -- CORE: data out
      rst_sclksync       : out std_logic;  -- reset sclk sync.
      fec_al             : in  std_logic_vector(31 downto 0));  -- CORE : AFL
  end component;
  
end msm_msmodule_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
