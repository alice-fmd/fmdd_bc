-------------------- Test bench of RCU MS communications selector -------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_comm_selection_pack.all;

entity msm_comm_selection_tb is
end msm_comm_selection_tb;

architecture test of msm_comm_selection_tb is

  -- component ports
  signal sc_add    : std_logic_vector(15 downto 0) := (others => '0');
  signal sc_data   : std_logic_vector(15 downto 0) := (others => '0');
  signal sc_exec   : std_logic                     := '0';  -- [in]  Execute
  signal exec_in   : std_logic;         -- [out] Execute interrupt handler
  signal branch    : std_logic;         -- [out] Branch select
  signal bcast     : std_logic;         -- [out] Broadcast select
  signal rnw       : std_logic;         -- [out] Read (not write)
  signal fec_add   : std_logic_vector(3 downto 0);  -- [out] FEC address
  signal bcreg_add : std_logic_vector(7 downto 0);  -- [out] BC register address
  signal bc_data   : std_logic_vector(15 downto 0);         -- [out] Data

  -- Clock period
  constant PERIOD : time := 25 ns;

  signal clk : std_logic := '0';

  procedure set (
    constant exec       : in  std_logic;
    constant branch     : in  std_logic;
    constant rnw        : in  std_logic;
    constant bcast      : in  std_logic;
    constant fec_add    : in  std_logic_vector(3 downto 0);
    constant reg_add    : in  std_logic_vector(7 downto 0);
    constant data       : in  std_logic_vector(15 downto 0);
    signal   sc_add     : out std_logic_vector(15 downto 0);
    signal   sc_data    : out std_logic_vector(15 downto 0);
    signal   sc_exec    : out std_logic;
    signal   in_exec    : in  std_logic;
    signal   in_branch  : in  std_logic;
    signal   in_rnw     : in  std_logic;
    signal   in_bcast   : in  std_logic;
    signal   in_fec_add : in  std_logic_vector(3 downto 0);
    signal   in_reg_add : in  std_logic_vector(7 downto 0);
    signal   in_data    : in  std_logic_vector(15 downto 0)) is
    variable tmp : std_logic_vector(15 downto 0) := X"0000";
  begin
    tmp(14)          := rnw;
    tmp(13)          := bcast;
    tmp(12)          := branch;
    tmp(11 downto 8) := fec_add;
    tmp(7 downto 0)  := reg_add;
    sc_add           <= tmp;
    sc_data          <= data;
    sc_exec          <= exec;

    wait for PERIOD;
    assert exec = in_exec report "Bad return: msm_exec" severity warning;
    assert rnw = in_rnw report "Bad return: msm_rnw" severity warning;
    assert bcast = in_bcast report "Bad return: msm_bcast" severity warning;
    assert branch = in_branch report "Bad return: msm_branch" severity warning;
    assert fec_add = in_fec_add report "Bad return: msm_fec_add" severity warning;
    assert reg_add = in_reg_add report "Bad return: msm_reg_add" severity warning;
    assert data = in_data report "Bad return: msm_data" severity warning;
  end set;

begin  -- test

  -- component instantiation
  dut : msm_comm_selection
    port map (
      sc_add    => sc_add,              -- [in]  SC address
      sc_data   => sc_data,             -- [in]  SC data
      sc_exec   => sc_exec,             -- [in]  Execute
      exec_in   => exec_in,             -- [out] Execute interrupt handler
      branch    => branch,              -- [out] Branch select
      bcast     => bcast,               -- [out] Broadcast select
      rnw       => rnw,                 -- [out] Read (not write)
      fec_add   => fec_add,             -- [out] FEC address
      bcreg_add => bcreg_add,           -- [out] BC register address
      bc_data   => bc_data);            -- [out] Data

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim : process
  begin
    -- insert signal assignments here

    wait for 100 ns;
    set(exec       => '0',
        branch     => '0',
        rnw        => '0',
        bcast      => '1',
        fec_add    => X"1",
        reg_add    => X"0a",
        data       => X"dead",
        sc_add     => sc_add,
        sc_data    => sc_data,
        sc_exec    => sc_exec,
        in_exec    => exec_in,
        in_branch  => branch,
        in_rnw     => rnw,
        in_bcast   => bcast,
        in_fec_add => fec_add,
        in_reg_add => bcreg_add,
        in_data    => bc_data);

    wait for 100 ns;
    set(exec       => '1',
        branch     => '0',
        rnw        => '0',
        bcast      => '1',
        fec_add    => X"1",
        reg_add    => X"0a",
        data       => X"dead",
        sc_add     => sc_add,
        sc_data    => sc_data,
        sc_exec    => sc_exec,
        in_exec    => exec_in,
        in_branch  => branch,
        in_rnw     => rnw,
        in_bcast   => bcast,
        in_fec_add => fec_add,
        in_reg_add => bcreg_add,
        in_data    => bc_data);

    wait for 100 ns;
    set(exec       => '0',
        branch     => '1',
        rnw        => '0',
        bcast      => '0',
        fec_add    => X"1",
        reg_add    => X"0a",
        data       => X"dead",
        sc_add     => sc_add,
        sc_data    => sc_data,
        sc_exec    => sc_exec,
        in_exec    => exec_in,
        in_branch  => branch,
        in_rnw     => rnw,
        in_bcast   => bcast,
        in_fec_add => fec_add,
        in_reg_add => bcreg_add,
        in_data    => bc_data);

    wait;                               -- forever
  end process stim;

  

end test;
-------------------------------------------------------------------------------
configuration msm_comm_selection_vhdl of msm_comm_selection_tb is

  for test
    for dut : msm_comm_selection
      use entity msmodule_lib.msm_comm_selection(rtl);
    end for;
  end for;

end msm_comm_selection_vhdl;

-------------------------------------------------------------------------------
--
-- EOF
--
-------------------------------------------------------------------------------
