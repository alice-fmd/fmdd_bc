-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- The bits of the address and data bus are decoded and sent to the instruction
-- builder. The same information can be sent from the interrupt_handler (whitin
-- interrupt_driver) in case of Interrupt. 
-- This second case has the priority and the mux_signal block (within lsc_core)
-- takes care of the choice.
library ieee;
use ieee.std_logic_1164.all;

entity msm_comm_selection is
  
  port (
    sc_add    : in  std_logic_vector(15 downto 0);   -- SC address
    sc_data   : in  std_logic_vector(15 downto 0);   -- SC data
    sc_exec   : in  std_logic;          -- Execute
    exec_in   : out std_logic;          -- Execute interrupt handler
    branch    : out std_logic;          -- Branch select
    bcast     : out std_logic;          -- Broadcast select
    rnw       : out std_logic;          -- Read (not write)
    fec_add   : out std_logic_vector(3 downto 0);    -- FEC address
    bcreg_add : out std_logic_vector(7 downto 0);    -- BC register address
    bc_data   : out std_logic_vector(15 downto 0));  -- Data

end msm_comm_selection;

architecture rtl of msm_comm_selection is

begin  -- rtl

  rnw       <= sc_add(14);
  bcast     <= sc_add(13);
  branch    <= sc_add(12);
  fec_add   <= sc_add(11 downto 8);
  bcreg_add <= sc_add(7 downto 0);
  bc_data   <= sc_data;
  exec_in   <= sc_exec;

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_comm_selection_pack is

  component msm_comm_selection
    port (
      sc_add    : in  std_logic_vector(15 downto 0);   -- SC address
      sc_data   : in  std_logic_vector(15 downto 0);   -- SC data
      sc_exec   : in  std_logic;        -- Execute
      exec_in   : out std_logic;        -- Execute interrupt handler
      branch    : out std_logic;        -- Branch select
      bcast     : out std_logic;        -- Broadcast select
      rnw       : out std_logic;        -- Read (not write)
      fec_add   : out std_logic_vector(3 downto 0);    -- FEC address
      bcreg_add : out std_logic_vector(7 downto 0);    -- BC register address
      bc_data   : out std_logic_vector(15 downto 0));  -- Data
  end component;

end msm_comm_selection_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
