-------------------- Test bench component of an I2C slave ---------------------
-- Author     : Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: A simple I2C slave component for use in test benches.
--              It runs on the I2C clock, and takes separate inputs and outputs
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity msm_slave is
  generic (
    ME         : std_logic_vector(3 downto 0) := X"F";
    PERIOD     : time                         := 200 ns;
    TIMEOUT    : integer                      := 8;
    DELAY      : integer                      := 4);
  port (
    scl     : in  std_logic;                      -- Serial clock
    sda_in  : in  std_logic;                      -- Serial data in
    sda_out : out std_logic;                      -- Serial data out
    word    : in  std_logic_vector(15 downto 0);  -- 2 output bytes
    data    : out std_logic_vector(7 downto 0));  -- Output byte
end msm_slave;

-------------------------------------------------------------------------------
architecture test2 of msm_slave is

  signal start_i  : boolean := false;              -- Detected start condition
  signal stop_i   : boolean := false;              -- Detected start condition
  signal cnt_i    : integer := 0;                  -- Counter
  signal buffer_i : std_logic_vector(7 downto 0);  -- Buffer
  signal rw_i     : boolean;                       -- Read flag
  signal sub_i    : integer;
  signal no_ack_i : boolean;                       -- No ack from master
  signal words_i  : integer := 0;                  -- Counter
  signal first_i  : boolean;
  signal sda_in_i : std_logic;

  signal fec_i     : std_logic_vector(3 downto 0);  -- Seen FEC address
  signal bcast_i   : boolean;
  signal reg_add_i : std_logic_vector(7 downto 0);
  signal data_i    : std_logic_vector(7 downto 0);
  
  type   st_t is (idle, who, addressed, ack, write_data, read_data, ack_master);
  signal st_i     : st_t;
  signal nx_st_i  : st_t;

  --! @brief Convert std_logic_vector to a string
  --! @param v The vector to convert
  --! @return String representation of @a v
  function slv2str (
    constant v   : std_logic_vector)    --! Vector
    return string
  is
    variable ret : string(v'length downto 1);
    variable i   : integer;
    variable j   : integer;
  begin  -- function slv2str

    ret := (others => 'X');
    j   := v'length;

    for i in v'high downto v'low loop
      case v(i) is
        when 'U'    => ret(j) := 'U';
        when 'X'    => ret(j) := 'X';
        when '0'    => ret(j) := '0';
        when '1'    => ret(j) := '1';
        when 'Z'    => ret(j) := 'Z';
        when 'W'    => ret(j) := 'W';
        when 'L'    => ret(j) := 'L';
        when 'H'    => ret(j) := 'H';
        when '-'    => ret(j) := '-';
        when others => ret(j) := 'X';
      end case;
      j := j - 1;
    end loop;  -- i
    return ret;
  end function slv2str;

  -- purpose: Output the serial data
  procedure one_out (
    signal sda_out : out std_logic;                         -- Serial data
    signal cnt     : in  integer;                           -- Clock
    signal sub     : in  integer;                           -- Byte counter
    signal data    : in  std_logic_vector(15 downto 0)) is  -- Output data
    variable idx_i : integer := 0;
  begin  -- one_out
    if cnt = 0 or sub > 1 then
      -- report "one_out called when cnt is " & integer'image(cnt)
      --   & " and sub is " & integer'image(sub)
      --   severity warning;
      sda_out <= 'H' after DELAY * PERIOD / 4;
    else
      idx_i   := sub * 8 + cnt - 1;
      -- report
      --  "sub_i=" & integer'image(sub_i) &
      --  " cnt_i=" & integer'image(cnt_i) &
      --  " idx_i=" & integer'image(idx_i) severity note;
      sda_out <= data(idx_i) after DELAY * PERIOD / 4;
    end if;
  end one_out;
  
begin  -- test2
  -- purpose: Collect combinatorics
  combinatorics : block
  begin  -- block combinatorics
    sda_in_i <= '1' when sda_in = '1' else '0';
    data     <= data_i;
  end block combinatorics;

  -- purpose: Store received data
  -- type   : sequential
  -- inputs : scl, start_i
  -- outputs: 
  inputs : process (scl, start_i)
  begin  -- process inputs
    if start_i then                     -- asynchronous reset (active low)
      fec_i     <= (others => '0');
      bcast_i   <= false;
      reg_add_i <= (others => '0');
    elsif scl'event and scl = '1' then  -- rising clock edge
      if words_i = 0 and cnt_i = 0 then
        fec_i   <= data_i(4 downto 1);
        bcast_i <= data_i(6) = '1';
      elsif words_i = 1 and cnt_i = 0 then
        reg_add_i <= data_i;
      end if;
    end if;
  end process inputs;
  -- purpose: Count number of recieved words
  -- type   : sequential
  -- inputs : scl, start_i
  -- outputs: 
  nwords : process (scl, start_i)
  begin  -- process nwords
    if start_i or stop_i then           -- asynchronous reset (active low)
      words_i <= 0;
    elsif scl'event and scl = '1' then  -- rising clock edge
      if cnt_i = 8 then
        words_i <= words_i + 1;
      end if;
    end if;
  end process nwords;
  
  -- purpose: Detect start condition
  -- type   : combinational
  -- inputs : sda_in, scl
  -- outputs: start_i
  start_condition : process (sda_in, scl)
  begin  -- process start_condition
    if falling_edge(sda_in) and scl /= '0' then
      start_i <= true, false after TIMEOUT*PERIOD;
    elsif st_i /= idle then
      start_i <= false;
    end if;
  end process start_condition;

  -- purpose: Detect stop condition
  -- type   : combinational
  -- inputs : sda_in, scl_in
  -- outputs: stop_i
  stop_condition : process (sda_in, scl)
  begin  -- process stop_condition
    if rising_edge(sda_in) and scl /= '0' then
      stop_i <= true, false after PERIOD;
    end if;
  end process stop_condition;

  -- purpose: Count clock edges
  -- type   : combinational
  -- inputs : scl, start_i, stop_i
  -- outputs: cnt_i
  count_scl : process (scl, start_i, stop_i)
  begin  -- process count_scl
    if (start_i'event and start_i) or (stop_i'event and stop_i) then
    -- if start_i or stop_i then
      cnt_i  <= 8;
    elsif scl'event and scl /= '0' then
      cnt_i  <= (cnt_i - 1) mod 9;      --  after PERIOD / 4;
    end if;
  end process count_scl;

  -- purpose: Buffer serial data
  -- type   : combinational
  -- inputs : start_i, stop_i, cnt_i
  -- outputs: buffer_i
  buffer_data : process (start_i, stop_i, scl) -- cnt_i)
  begin  -- process buffer_data
    if (start_i'event and start_i) or
      (stop_i'event and stop_i) then
      buffer_i <= (others => '0');
    elsif scl'event and scl /= '0'  then
      buffer_i <= buffer_i(6 downto 0) & sda_in_i; --  after PERIOD / 4;
    end if;
  end process buffer_data;


  -- purpose: Write output data
  -- type   : combinational
  -- inputs : st_i
  -- outputs: data
  out_data : process (cnt_i)
  begin  -- process out_data
    if cnt_i = 0 then
      data_i <= buffer_i;
    elsif cnt_i = 8 then
      data_i <= (others => '0');
    end if;
  end process out_data;

  -- purpose: Update to next state
  -- type   : sequential
  -- inputs : scl, stop_i, nx_st
  -- outputs: st
  update_state : process (scl, stop_i)
  begin  -- process update_state
    if stop_i then                      -- asynchronous reset (active low)
      st_i <= idle;
    elsif scl'event and scl = '1' then  -- rising clock edge
      st_i <= nx_st_i;
    end if;
  end process update_state;

  -- purpose: Get the next state
  -- type   : combinational
  -- inputs : start_i, stop_i, st_i, cnt_i
  -- outputs: 
  fsm : process (start_i, stop_i, st_i, cnt_i)
    variable idx_i : integer   := 0;
    variable nxt_i : std_logic := 'H';
  begin  -- process fsm
    if stop_i then
      nx_st_i <= idle;
    else
      -- nx_st_i <= st_i;

      case st_i is
        when idle =>
          rw_i    <= false;
          sub_i   <= 1;
          sda_out <= 'H';
          first_i <= true;
          if start_i then
            nx_st_i <= who;
          end if;

        when who =>
          sda_out <= 'H';
          if cnt_i = 1 then
            -- Check address (4 lower bits) or bcast bit
            -- report "I2C FEC: " & slv2str(fec_i)
            --   & " Buffer: " & slv2str(buffer_i(4 downto 1))
            --   & " ME: " & slv2str(ME)
            --   severity note;
            if buffer_i(3 downto 0) = ME or buffer_i(5) = '1' then
              nx_st_i <= addressed;
            else
              nx_st_i <= idle;
            end if;
          end if;

        when addressed =>
          sda_out <= '0' after DELAY * PERIOD / 4;               -- One early
          rw_i    <= buffer_i(0) /= '0';
          nx_st_i <= ack;

        when ack =>
          nxt_i := 'H';
          if bcast_i and rw_i then
            report "Read when in broadcast not allowed" severity warning;
            nx_st_i <= idle;
          elsif first_i and rw_i then
            nx_st_i <= write_data;
          elsif not rw_i then
            nx_st_i <= write_data;
          else
            idx_i   := sub_i * 8 + cnt_i - 1;
            nxt_i   := word(idx_i);
            nx_st_i <= read_data;
          end if;
          sda_out <= '0', nxt_i after DELAY * PERIOD / 4;

        when write_data =>
          first_i <= false;
          if cnt_i = 0 then
            sda_out <= '0' after DELAY * PERIOD / 4;
            nx_st_i <= ack;
          else
            sda_out <= 'H';             --
            -- nx_st_i <= write_data;
          end if;

        when read_data =>
          if cnt_i = 0 then
            nx_st_i <= ack_master;
          end if;
          one_out(sda_out => sda_out,
                  sub     => sub_i,
                  cnt     => cnt_i,
                  data    => word);

        when ack_master =>
          sub_i    <= (sub_i - 1) mod 2;
          no_ack_i <= sda_in /= '0' after PERIOD / 2;
          if no_ack_i then
            nx_st_i <= idle;
            sda_out <= 'H';
          else
            sda_out <= 'L';
            nx_st_i <= read_data;
            one_out(sda_out => sda_out,
                    sub     => sub_i,
                    cnt     => cnt_i,
                    data    => word);
          end if;
          
        when others => null;
      end case;
    end if;
  end process fsm;
end test2;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_slave_pack is
  component msm_slave
    generic (
      ME      : std_logic_vector(3 downto 0) := X"F";
      PERIOD  : time                         := 200 ns;
      TIMEOUT : integer                      := 8;
      DELAY   : integer                      := 4);
    port (
      scl     : in  std_logic;                      -- Serial clock
      sda_in  : in  std_logic;                      -- Serial data in
      sda_out : out std_logic;                      -- Serial data out
      word    : in  std_logic_vector(15 downto 0);  -- 2 output bytes
      data    : out std_logic_vector(7 downto 0));  -- Output byte
  end component;
end msm_slave_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
