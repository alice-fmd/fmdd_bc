-------------------------------------------------------------------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
--
-- Based on verilog code by Carmen González (CERN-PH/ED).
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
-- 
-- Result register
library ieee;
use ieee.std_logic_1164.all;

entity msm_result is
  
  port (
    clk           : in  std_logic;                       -- Clock
    rstb          : in  std_logic;                       -- Async reset
    rst_resultreg : in  std_logic;                       -- Syncreset
    we_rm         : in  std_logic;                       -- Write enable
    data_rm       : in  std_logic_vector(20 downto 0);   -- Data for register
    result_lsc    : out std_logic_vector(20 downto 0));  -- Output of reg.

end msm_result;

architecture rtl of msm_result is

  signal result_reg_i : std_logic_vector(20 downto 0);
  
begin  -- rtl
  result_lsc <= result_reg_i;
  
  -- purpose: msm_Register result data
  -- type   : msm_sequential
  -- inputs : msm_clk, rstb, rst_resultreg, we_rm, data_rm
  -- outputs: msm_result_reg_i
  process (clk, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      result_reg_i <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if rst_resultreg = '1' then
        result_reg_i <= (others => '0');
      elsif we_rm = '1' then
        result_reg_i <= data_rm;
      end if;
    end if;
  end process;

end rtl;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm_result_pack is

  component msm_result
    port (
      clk           : in  std_logic;                       -- Clock
      rstb          : in  std_logic;                       -- Async reset
      rst_resultreg : in  std_logic;                       -- Syncreset
      we_rm         : in  std_logic;                       -- Write enable
      data_rm       : in  std_logic_vector(20 downto 0);   -- Data for register
      result_lsc    : out std_logic_vector(20 downto 0));  -- Output of reg.
  end component;
end msm_result_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
