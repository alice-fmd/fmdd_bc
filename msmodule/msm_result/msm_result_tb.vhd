-------------------- Test bench of RCU MS result register ---------------------
-- Author     : msm_Christian Holm Christensen  <cholm@cholm.priv.nbi.dk>
-------------------------------------------------------------------------------
-- Description: msm_
-------------------------------------------------------------------------------
-- Copyright (c) 2009 Christian Holm Christensen
--
-- Licensed under the Lesser GNU Public License version 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule_lib;
use msmodule_lib.msm_result_pack.all;

entity msm_result_tb is

end msm_result_tb;

architecture test of msm_result_tb is

  -- component ports
  signal clk           : std_logic                     := '0'; 
  signal rstb          : std_logic                     := '0'; 
  signal rst_resultreg : std_logic                     := '0'; 
  signal we_rm         : std_logic                     := '0'; 
  signal data_rm       : std_logic_vector(20 downto 0) := (others => '0');
  signal result_lsc    : std_logic_vector(20 downto 0);

  -- Clock period
  constant PERIOD : time := 25 ns;
  constant X : std_logic_vector(20 downto 0) := '1' & X"deadb";
  constant O : std_logic_vector(20 downto 0) := (others => '0');
  

begin  -- test

  -- component instantiation
  dut: msm_result
    port map (
      clk           => clk,             -- [in]  Clock
      rstb          => rstb,            -- [in]  Async reset
      rst_resultreg => rst_resultreg,   -- [in]  Syncreset
      we_rm         => we_rm,           -- [in]  Write enable
      data_rm       => data_rm,         -- [in]  Data for register
      result_lsc    => result_lsc);     -- [out] Output of reg.

  -- clock generation
  clk <= not clk after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 4 * PERIOD;
    rstb <= '1';
    wait for 4 * PERIOD;

    data_rm <= X;
    wait for 1 * PERIOD;
    we_rm <= '1';
    wait for 1 * PERIOD;
    we_rm <= '0';

    assert result_lsc = X report "mismatch" severity warning;
    
    wait for 4 * PERIOD;
    rst_resultreg <= '1';
    wait for 1 * PERIOD;
    rst_resultreg <= '0';
    
    assert result_lsc = O report "mismatch" severity warning;
    

    wait; -- forever
  end process stim;

  

end test;

-------------------------------------------------------------------------------
configuration result_vhdl of msm_result_tb is

  for test
    for dut : msm_result
      use entity msmodule_lib.msm_result(rtl);
    end for;
  end for;

end result_vhdl;


-------------------------------------------------------------------------------
--
-- EOF
--
-------------------------------------------------------------------------------
