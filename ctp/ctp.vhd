-------------------------------------------------------------------------------
--!
--! Emulation of CTP
--!
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ctp is
  port (clk      : in  std_logic;       -- Master clock (must be fast)
        rstb     : in  std_logic;       -- Async. reset (-)
        l0       : out std_logic;       -- L0 output (+)
        l1       : out std_logic;       -- L1 output (-)
        l2       : out std_logic;       -- L2 output (-)
        busy     : in  std_logic;       -- Busy input
        scale    : in  std_logic_vector(31 downto 0);   -- Down scaler
        l1_delay : in  std_logic_vector(31 downto 0);   -- L1 delay from L0
        l2_delay : in  std_logic_vector(31 downto 0));  -- L2 delay from L1
end ctp;

architecture behaviour of ctp is
  signal l1_cnt_i   : integer;           -- L1 delay counter
  signal l2_cnt_i   : integer;           -- L2 delay counter; 
  signal scaler_i   : integer;           -- Downscaler
  signal start_i    : boolean := false;  -- Start sequence;
  signal l1_delay_i : integer;
  signal l2_delay_i : integer;

  type gen_state_t is (idle, 
                       wait_l1, 
                       wait_l2,  
                       protect);
  signal gen_state_i : gen_state_t;
  
  -- purpose: Convert standard logic vector to integer
  function slv2int(constant v : std_logic_vector)
    return integer is
  begin  -- slv2int
    return to_integer(unsigned(v));
  end slv2int;
begin  -- behaviour
  l1_delay_i <= slv2int(l1_delay);
  l2_delay_i <= slv2int(l2_delay);


  -- purpose: Generate triggers as often as requested
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: 
  gen: process (clk, rstb)
  begin  -- process gen
    if rstb = '0' then                  -- asynchronous reset (active low)
      l0 <= '0';
      l1 <= '1';
      l2 <= '1';
      gen_state_i <= idle;
    elsif clk'event and clk = '1' then  -- rising clock edge
      l0 <= '0';
      l1 <= '1';
      l2 <= '1';

      case gen_state_i is
        when idle =>
          l1_cnt_i <= l1_delay_i;
          l2_cnt_i <= l2_delay_i;
          if start_i then
            l0 <= '1';
            gen_state_i <= wait_l1;
          end if;

        when wait_l1 =>
          l1_cnt_i <= l1_cnt_i - 1;
          l2_cnt_i <= l2_cnt_i - 1;
          if l1_cnt_i = 1 then
            l1          <= '0';
            gen_state_i <= wait_l2;
          end if;

        when wait_l2 =>
          l2_cnt_i <= l2_cnt_i - 1;
          if l2_cnt_i = 1 then
            l2          <= '0';
            gen_state_i <= protect;
          end if;
          
        when protect =>
          gen_state_i <= idle;
          
        when others => null;
      end case;
    end if;
  end process gen;
  

  -- purpose: Generate gate
  -- type   : sequential
  -- inputs : clk, rstb, busy
  -- outputs: start_i
  doit : process (clk, rstb)
  begin  -- process doit
    if rstb = '0' then                  -- asynchronous reset (active low)
      start_i  <= false;
      scaler_i <= 0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      start_i <= false;
      
      -- Edge detect
      if scaler_i <= 1 then
        scaler_i <= to_integer(unsigned(scale));
        if busy = '0' then
          start_i <= true;
        end if;
      else
        scaler_i <= scaler_i - 1;
        start_i <= false;
      end if;
    end if;
  end process doit;
end behaviour;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package ctp_pack is
  component ctp
    port (clk      : in  std_logic;     -- Master clock (must be fast)
          rstb     : in  std_logic;     -- Async. reset (-)
          l0       : out std_logic;     -- L0 output (+)
          l1       : out std_logic;     -- L1 output (-)
          l2       : out std_logic;     -- L2 output (-)
          busy     : in  std_logic;     -- Busy input
          scale    : in  std_logic_vector(31 downto 0);   -- Down scaler
          l1_delay : in  std_logic_vector(31 downto 0);   -- L1 delay from L0
          l2_delay : in  std_logic_vector(31 downto 0));  -- L2 delay from L1
  end component;
end ctp_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
