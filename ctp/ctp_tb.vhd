-------------------------------------------------------------------------------
-- Title      : Testbench for design "ctp"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : ctp_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Created    : 2008-08-06
-- Last update: 2008-08-06
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2008 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2008-08-06  1.0      cholm	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ctp_pack.all;

-------------------------------------------------------------------------------
entity ctp_tb is
end ctp_tb;

-------------------------------------------------------------------------------
architecture test of ctp_tb is

  -- component generics
  constant PERIOD     : time      := 25 ns;

  -- component ports
  signal clk_i      : std_logic := '1';  -- [in]  Master clock (must be fast)
  signal rstb_i     : std_logic := '0';  -- [in]  Async. reset (-)
  signal l0_i       : std_logic;        -- [out] L0 output (+)
  signal l1_i       : std_logic;        -- [out] L1 output (-)
  signal l2_i       : std_logic;        -- [out] L2 output (-)
  signal busy_i     : std_logic := '0';  -- [in]  Busy input
  signal scale_i    : std_logic_vector(31 downto 0);  -- [in]  Down scaler
  signal l1_delay_i : std_logic_vector(31 downto 0);  -- [in]  L1 delay from L0
  signal l2_delay_i : std_logic_vector(31 downto 0);  -- [in] L2 delay from L1

  signal scale_ii    : integer := 1;
  signal l1_delay_ii : integer := 20;
  signal l2_delay_ii : integer := 300;
  
begin  -- test
  scale_i    <= std_logic_vector(to_unsigned(scale_ii,    32));
  l1_delay_i <= std_logic_vector(to_unsigned(l1_delay_ii, 32));
  l2_delay_i <= std_logic_vector(to_unsigned(l2_delay_ii, 32));
  
  -- component instantiation
  DUT : ctp
    port map (clk      => clk_i,        -- [in]  Master clock (must be fast)
              rstb     => rstb_i,       -- [in]  Async. reset (-)
              l0       => l0_i,         -- [out] L0 output (+)
              l1       => l1_i,         -- [out] L1 output (-)
              l2       => l2_i,         -- [out] L2 output (-)
              busy     => busy_i,       -- [in]  Busy input
              scale    => scale_i,      -- [in]  Down scaler
              l1_delay => l1_delay_i,   -- [in]  L1 delay from L0
              l2_delay => l2_delay_i);  -- [in] L2 delay from L1

  -- clock generation
  clk_i <= not clk_i after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    -- insert signal assignments here
    wait for 100 ns;
    rstb_i <= '1';
    
    wait;
  end process stim;
end test;

-------------------------------------------------------------------------------

configuration ctp_tb_test_cfg of ctp_tb is
  for test
  end for;
end ctp_tb_test_cfg;

-------------------------------------------------------------------------------
