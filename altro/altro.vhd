-------------------------------------------------------------------------------
-- Title      : altro simulation top level
-- Project    : 
-------------------------------------------------------------------------------
-- File       : altro.vhd
-- Author     : cholm
-- Company    : 
-- Created    : 2005-04-24
-- Last update: 2008-08-01
-- Platform   : 
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description:  altro simulation top level
--
-------------------------------------------------------------------------------
-- Copyright (c) 2005 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2005-04-24  1.0      cholm      Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
--! @defgroup altro_lib Model of ALTRO chip
--! Code in this module models the ALTRO chip

-------------------------------------------------------------------------------
--! ALTRO model library
library altro_model;
use altro_model.interface_pack.all;
use altro_model.data_types_pack.all;


-------------------------------------------------------------------------------
--! Model of ALTRO.  This is the top-level entity of the model
--! @ingroup altro_lib
entity altro is
  generic (
    HADD : std_logic_vector (7 downto 0) := "00000001");
  port (
    clk      : in    std_logic;         -- !clock
    clk2     : in    std_logic;         -- !clock 2
    rstb     : in    std_logic;         --! Reset
    trg      : in    std_logic;         --! trigger
    l2y      : in    std_logic;         --! L2 accept
    cstb     : in    std_logic;         --! Control strobe 
    writeb   : in    std_logic;         --! write input
    ackb     : out   std_logic;         --! acknowledge;
    ackb_en  : out   std_logic;         --! acknowledge enable 
    dolob_en : out   std_logic;         --! Data output enable 
    trsfb    : out   std_logic;         --! Transfer gate
    trsfb_en : out   std_logic;         --! Transfer enable 
    dstb     : out   std_logic;         --! Data strobe 
    errorb   : out   std_logic;         --! Error 
    tstoutb  : out   std_logic;         --! Test output 
    bd       : inout std_logic_vector(39 downto 0);  --! Bus
    data     : in    data_vector);      --! data
end altro;

-------------------------------------------------------------------------------
architecture rtl of altro is
  constant TWX           : std_logic := '0';
  signal   ch_red_i      : std_logic;
  signal   din_i         : std_logic_vector (39 downto 0);  --! Data input
  signal   write_i       : std_logic;   --! write
  signal   h_abt_i       : std_logic;   --! Hamming encoding
  signal   h_err_i       : std_logic;   --! Hamming encoding error
  signal   chrdo_i       : std_logic;   --! channel read-out
  signal   ack_i         : std_logic;   --! Acknowledge
  signal   ack_en_i      : std_logic;   --! Enable acknowledge
  signal   dolo_en_i     : std_logic;   --! Data output enable
  signal   trsf_i        : std_logic;   --! Transfer gate
  signal   trsf_en_i     : std_logic;   --! Enable transfer
  signal   trg_overlap_i : std_logic;   --! Trigger overlap
  signal   csr_wr_i      : std_logic;   --! Config/status write.
  signal   pop_i         : std_logic;   --! Pop an event
  signal   push_i        : std_logic;   --! Push an event
  signal   trg_o_i       : std_logic;   --! Trigger
  signal   trc_clr_i     : std_logic;   --! Clear trigger
  signal   err_clr_i     : std_logic;   --! Error clear
  signal   wr_bsl_i      : std_logic;   --! Write baseline
  signal   rd_bsl_i      : std_logic;   --! Read baseline
  signal   instr_err_i   : std_logic;   --! Instruction error
  signal   bcast_i       : std_logic;   --! Broadcast 
  signal   chadd_i       : std_logic_vector (3 downto 0);  --! Channel address
  signal   csr_sel_i     : std_logic_vector (4 downto 0);  --! CSR select
  signal   hadd_o_i      : std_logic_vector (7 downto 0);  --! HW address
  signal   csr_do_i      : std_logic_vector (19 downto 0);  --! CSR do
  signal   par_err_i     : std_logic;   --! Parity error 
  signal   tstout_i      : std_logic;   --! Test out
  signal   mixerror_i    : std_logic;   --! Mixed error
  signal   error_i       : std_logic;   --! Error
  signal   i             : integer;     --! Counter
  signal   cst_i         : std_logic;   --! Control strobe
  type     state_t is (idle, start, count1, count2, finish);  --! state type
  signal   state         : state_t   := start;  --! state

  type   bd_out_t is array (18 downto 0) of std_logic_vector (39 downto 0);
  signal bd_out_i       : bd_out_t;     --! Data out
  signal bd_out_ptr_i   : integer := 0;  --! Pointer into data out
  signal data_out_ptr_i : integer := -1;  --! Data out pointer

  --! Convert integer to std_logic_vector
  --! @param x Integer to convert
  --! @param n Size (in bits) of integer
  function conv_std_logic_vector (
    constant x : in integer;
    constant n : in natural)            -- Input
    return std_logic_vector is
  begin  -- function conv_std_logic_vector
    return std_logic_vector(to_unsigned(x, n));
  end function conv_std_logic_vector;

begin  -- rtl
  cst_i <= not cstb;

  --! Bus interface decoder 
  i_interface : interface
    port map (
      clk         => clk,
      clk2        => clk2,
      rstb        => rstb,
      ch_red      => ch_red_i,
      hadd        => HADD,
      bd          => din_i,
      cstb        => cst_i,
      wrt         => write_i,
      trgi        => trg,
      twx         => TWX,
      h_abt       => h_abt_i,
      h_err       => h_err_i,
      chrdo       => chrdo_i,
      ack         => ack_i,
      ack_en      => ack_en_i,
      dolo_en     => dolo_en_i,
      trsf_en     => trsf_en_i,
      trg_overlap => trg_overlap_i,
      csr_wr      => csr_wr_i,
      push        => push_i,
      pop         => pop_i,
      trg         => trg_o_i,
      trc_clr     => trc_clr_i,
      err_clr     => err_clr_i,
      wr_bsl      => wr_bsl_i,
      rd_bsl      => rd_bsl_i,
      instr_err   => instr_err_i,
      bcast       => bcast_i,
      chadd       => chadd_i,
      csr_sel     => csr_sel_i,
      hadd_o      => hadd_o_i,
      csr_do      => csr_do_i,
      par_err     => par_err_i,
      l2y         => l2y,
      tstout      => tstout_i);

  --! State machine to control delayed response
  --! @param chrdo_i Channel readout
  --! @param clk2 Slow clock
  --! @param rstb Asyncronous reset
  process (chrdo_i, clk2, rstb)
  begin  -- process
    if rstb = '0' then
      ch_red_i       <= '1';
      trsfb          <= 'Z';
      state          <= idle;
      data_out_ptr_i <= -1;
    elsif clk2'event and clk2 = '1' then
        case state is
          when idle =>
            trsfb          <= 'Z';
            state          <= idle;
            data_out_ptr_i <= -1;
            if chrdo_i = '1' then
              state <= start;
            end if;
          when start =>
            -- Wait for 2 cycles ~ 48 ns / 24
            trsfb    <= '1';
            i        <= 2;
            state    <= count1;
            ch_red_i <= '0';
          when count1 =>
            if i <= 0 then
              state          <= count2;
              trsfb          <= '0';
              -- Wait for 41 cycles ~ 985 ns / 24
              -- i        <= 41;
              i              <= 4;
              data_out_ptr_i <= 4 - i;
            else
              i <= i - 1;
            end if;
          when count2 =>
            if i <= 0 then
              state    <= finish;
              ch_red_i <= '1';
            else
              i              <= i - 1;
              data_out_ptr_i <= 4 - i;
            end if;
          when finish =>
            data_out_ptr_i <= -1;
            trsfb          <= '1';
            state          <= idle;
          when others =>
            state <= idle;
        end case;
      end if;

    end if;
  end process;

  --! Error outout
  --! @param mixerror_i Mixed error
  --! @param err_clr_i  Clear errors
  --! @param rstb       Async reset
  process (mixerror_i, err_clr_i, rstb)
  begin  -- process
    if err_clr_i = '1' or rstb = '0' then
      error_i <= '0';
    elsif mixerror_i'event and mixerror_i = '1' then
      error_i <= '1';
    end if;
  end process;

  bd_out_i(0) <= conv_std_logic_vector(data(0), 10) &
                 conv_std_logic_vector(data(1), 10) &
                 conv_std_logic_vector(data(2), 10) &
                 conv_std_logic_vector(data(3), 10);
  bd_out_i(1) <= conv_std_logic_vector(data(4), 10) &
                 conv_std_logic_vector(data(5), 10) &
                 conv_std_logic_vector(data(6), 10) &
                 conv_std_logic_vector(data(7), 10);
  bd_out_i(2) <= conv_std_logic_vector(data(8), 10) &
                 conv_std_logic_vector(data(9), 10) &
                 conv_std_logic_vector(data(10), 10) &
                 conv_std_logic_vector(data(11), 10);
  bd_out_i(3) <= conv_std_logic_vector(data(12), 10) &
                 conv_std_logic_vector(data(13), 10) &
                 conv_std_logic_vector(data(14), 10) &
                 conv_std_logic_vector(data(15), 10);
  bd_out_i(4) <= std_logic_vector(to_unsigned(16#5a5a5a#, 24)) &
                 std_logic_vector(to_unsigned(16#5a5a#, 16));
  bd_out_i(5) <= "ZZZZZZZZZZZZZZZZZZZZ" &
                 std_logic_vector(to_unsigned(16#aaaaa#, 20));
  bd_out_i(6) <= (others => 'Z');

  mixerror_i   <= instr_err_i or par_err_i;
  dstb         <= not clk2        when trsf_i = '1'    else 'Z';
  din_i        <= (others => '0') when trsf_en_i = '1' else bd;
  bd_out_ptr_i <= data_out_ptr_i  when data_out_ptr_i >= 0
                  else 4 when trsf_en_i = '1' and dolo_en_i = '1'
                  else 5 when trsf_en_i = '0' and dolo_en_i = '1'
                  else 6;
  bd <= bd_out_i(bd_out_ptr_i);

  -- inverting inputs
  ackb     <= '0' when ack_i = '1' else 'Z';
  -- ackb     <= not ack_i;
  ackb_en  <= not ack_en_i;
--  trsfb    <= not trsf_i;
  trsfb_en <= not trsf_en_i;
  tstoutb  <= not tstout_i;
  dolob_en <= not dolo_en_i;
  errorb   <= not error_i;
  -- invert outputs
  write_i  <= not writeb;

end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.data_types_pack.all;

--! Package of ALTRO model entity 
package altro_pack is
  component altro
    generic (
      hadd : std_logic_vector (7 downto 0) := "00000001");
    port (
      clk      : in    std_logic;
      clk2     : in    std_logic;
      rstb     : in    std_logic;
      trg      : in    std_logic;
      l2y      : in    std_logic;
      cstb     : in    std_logic;
      writeb   : in    std_logic;
      ackb     : out   std_logic;
      ackb_en  : out   std_logic;
      dolob_en : out   std_logic;
      trsfb    : out   std_logic;
      trsfb_en : out   std_logic;
      dstb     : out   std_logic;
      errorb   : out   std_logic;
      tstoutb  : out   std_logic;
      bd       : inout std_logic_vector(39 downto 0);
      data     : in    data_vector);
  end component;
end altro_pack;
-------------------------------------------------------------------------------
--
-- EOF
--









