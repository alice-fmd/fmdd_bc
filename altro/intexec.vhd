-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library altro_model;
use altro_model.sync21_pack.all;

-------------------------------------------------------------------------------
--! Execute instructions
--! @ingroup altro_lib
entity intexec is
  port (
    l2y_r       : in  std_logic;        --! L2 accept
    exec        : in  std_logic;        --! exec state 
    en1         : in  std_logic;        --! en1 state
    ackn        : in  std_logic;        --! acknowledge state 
    en2         : in  std_logic;        --! en2 state 
    waitst      : in  std_logic;        --! Wait state 
    clk2        : in  std_logic;        --! Clock 2
    rstb        : in  std_logic;        --! reset
    clk         : in  std_logic;        --! clock 
    valid       : in  std_logic;        --! valid data
    chrdo0      : in  std_logic;        --! channel r/o 
    rg_rd0      : in  std_logic;        --! read register
    rg_wr0      : in  std_logic;        --! write register  
    push0       : in  std_logic;        --! Push it 
    pop0        : in  std_logic;        --! Pop it 
    swtrg       : in  std_logic;        --! software trigger 
    trc_clr0    : in  std_logic;        --! trigger counter clear
    err_clr0    : in  std_logic;        --! error clear
    wr_bsl0     : in  std_logic;        --! write 
    rd_bsl0     : in  std_logic;        --! read  
    bcast       : in  std_logic;        --! broadcast flag 
    hwtrg       : in  std_logic;        --! hardware trigger
    twx         : in  std_logic;        --! Test mode
    done        : out std_logic;        --! Done 
    ack         : out std_logic;        --! Acknowledge 
    ack_en      : out std_logic;        --! Enable acknowledge
    dolo_en     : out std_logic;        --! Enable output
    trsf_en     : out std_logic;        --! Enable transfer
    trg_overlap : out std_logic;        --! Trigger overlap 
    rg_wr       : out std_logic;        --! Register write 
    push        : out std_logic;        --! Push it 
    pop         : out std_logic;        --! Pop it 
    trg         : out std_logic;        --! Trigger 
    trc_clr     : out std_logic;        --! Clear trigger counter 
    err_clr     : out std_logic;        --! Error clear 
    wr_bsl      : out std_logic;        --! Write base line 
    rd_bsl      : out std_logic;        --! Read base line 
    tstout      : out std_logic);       --! Test mode out
end intexec;

-------------------------------------------------------------------------------
--! Execute instructions
--! @ingroup altro_lib
architecture rtl of intexec is
  signal x1_i    : std_logic;
  signal busyb_i : std_logic;
  signal w0_i    : std_logic;
  signal r1_i    : std_logic;
  signal r2_i    : std_logic;
  signal ack_i   : std_logic;
begin  -- rtl
  ack         <= ack_i;
  ack_i       <= valid and ackn;
  w0_i        <= en1 or ackn or en2;
  dolo_en     <= valid and ((rg_rd0 and w0_i) or (waitst and chrdo0));
  ack_en      <= valid and not (bcast or not w0_i);
  trsf_en     <= valid and (waitst and chrdo0);
  done        <= not (not r1_i or r2_i);
  tstout      <= ack_i or twx or x1_i;
  rg_wr       <= rg_wr0 and x1_i;
  push        <= (push0 and x1_i) or l2y_r;
  pop         <= pop0 and x1_i;
  trc_clr     <= trc_clr0 and x1_i;
  err_clr     <= err_clr0 and x1_i;
  wr_bsl      <= wr_bsl0 and x1_i;
  rd_bsl      <= rd_bsl0 and x1_i;
  trg         <= not (twx or not((swtrg and x1_i) or hwtrg));
  trg_overlap <= twx and ((swtrg and x1_i) or hwtrg);


  --! Syncronise @a exec to @a clk, and @a clk2
  i_sync : sync21
    port map (x2    => exec,
              clk   => clk,
              clk2  => clk2,
              rstb  => rstb,
              x1    => x1_i,
              busyb => busyb_i);

  --! Execute
  --! @param clk2 40MHz clock
  --! @param rstb Async. reset
  p_exec : process (clk2, rstb)
  begin  -- process
    if rstb = '0' then                    -- asynchronous reset (active low)
      r1_i <= '0';
      r2_i <= '0';
    elsif clk2'event and clk2 = '1' then  -- rising clock edge
      r1_i <= busyb_i;
      r2_i <= r1_i;
    end if;
  end process;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Execute instructions
--! @ingroup altro_lib
package intexec_pack is
  component intexec
    port (
      l2y_r       : in  std_logic;
      exec        : in  std_logic;
      en1         : in  std_logic;
      ackn        : in  std_logic;
      en2         : in  std_logic;
      waitst      : in  std_logic;
      clk2        : in  std_logic;
      rstb        : in  std_logic;
      clk         : in  std_logic;
      valid       : in  std_logic;
      chrdo0      : in  std_logic;
      rg_rd0      : in  std_logic;
      rg_wr0      : in  std_logic;
      push0       : in  std_logic;
      pop0        : in  std_logic;
      swtrg       : in  std_logic;
      trc_clr0    : in  std_logic;
      err_clr0    : in  std_logic;
      wr_bsl0     : in  std_logic;
      rd_bsl0     : in  std_logic;
      bcast       : in  std_logic;
      hwtrg       : in  std_logic;
      twx         : in  std_logic;
      done        : out std_logic;
      ack         : out std_logic;
      ack_en      : out std_logic;
      dolo_en     : out std_logic;
      trsf_en     : out std_logic;
      trg_overlap : out std_logic;      --  
      rg_wr       : out std_logic;
      push        : out std_logic;
      pop         : out std_logic;
      trg         : out std_logic;
      trc_clr     : out std_logic;
      err_clr     : out std_logic;
      wr_bsl      : out std_logic;
      rd_bsl      : out std_logic;
      tstout      : out std_logic);
  end component;
end intexec_pack;
-------------------------------------------------------------------------------
--
-- EOF
--


