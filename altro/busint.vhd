-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library altro_model;
use altro_model.glitchf_pack.all;
use altro_model.sync221_pack.all;

-------------------------------------------------------------------------------
--! Bus interface
--! @ingroup altro_lib
entity busint is
  port (
    l2y_i   : in  std_logic;                      --! L2 accept
    load    : in  std_logic;                      --! Load data
    hadd    : in  std_logic_vector(7 downto 0);   --! hardware address
    bd      : in  std_logic_vector(39 downto 0);  --! bidirectional bus
    wrt     : in  std_logic;                      --! Write enable
    trg     : in  std_logic;                      --! trigger
    clk     : in  std_logic;                      --! clk2
    clk2    : in  std_logic;                      --! clock 2
    rstb    : in  std_logic;                      --! reset
    hadd_r  : out std_logic_vector(7 downto 0);   --! hardware address
    add_r   : out std_logic_vector(19 downto 0);  --! return of address
    data_r  : out std_logic_vector(19 downto 0);  --! data output
    write_r : out std_logic;                      --! write return
    trg_r   : out std_logic;                      --! trigger return
    l2y_r   : out std_logic;                      --! l2 accept return
    cs      : out std_logic;                      --! Card select
    loadcs  : in  std_logic);                     --! Check address
end busint;

-------------------------------------------------------------------------------
--! Architecture of bus interface
--! @ingroup altro_lib 
architecture rtl of busint is
  signal eq_i     : std_logic;          --! Equal
  signal w1_i     : std_logic;          --! Wire 1
  signal w0_i     : std_logic;          --! Wire 0
  signal w2_i     : std_logic_vector (7 downto 0);  --! wire 2
  signal hadd_r_i : std_logic_vector(7 downto 0);  --! hardware address
begin  -- rtl
  --! Trigger glitch filter 
  i_glitch : glitchf
    port map (
      x2   => trg,
      clk  => clk,
      rstb => rstb,
      x1   => trg_r);

  --! Sync l2 to both clocks 
  i_sync1 : sync221
    port map (
      x2   => l2y_i,
      clk  => clk,
      clk2 => clk2,
      rstb => rstb,
      x1   => l2y_r);

  hadd_r <= hadd_r_i;
  eq_i   <= '1' when hadd_r_i = w2_i else '0';
  cs     <= not w0_i and (eq_i or w1_i);

  --! Decode address
  --! @param clk2 Clock
  --! @param rstb Async reset
  p_busFFF : process (clk2, rstb)
  begin  -- process
    if rstb = '0' then                    -- asynchronous reset (active low)
      write_r   <= '0';
      add_r     <= (others => '0');
      data_r    <= (others => '0');
      hadd_r_i  <= (others => '0');
      w0_i      <= '0';
      w1_i      <= '0';
      w2_i      <= (others => '0');
    elsif clk2'event and clk2 = '1' then  -- rising clock edge
      if load = '1' then
        write_r <= wrt;
        data_r  <= bd(19 downto 0);
        add_r   <= bd(39 downto 20);
      end if;
      hadd_r_i  <= hadd;
      if loadcs = '1' then
        w0_i      <= bd(37);            -- Board controller switch
        w1_i      <= bd(38);            -- Broadcast 
        w2_i      <= bd(36 downto 29);  -- Hardware address (8 bits)
      end if;
    end if;
  end process;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Package of ALTRO Bus interface
--! @ingroup altro_lib
package busint_pack is
  component busint
    port (
      l2y_i   : in  std_logic;
      load    : in  std_logic;
      hadd    : in  std_logic_vector(7 downto 0);
      bd      : in  std_logic_vector(39 downto 0);
      wrt     : in  std_logic;
      trg     : in  std_logic;
      clk     : in  std_logic;
      clk2    : in  std_logic;
      rstb    : in  std_logic;
      hadd_r  : out std_logic_vector(7 downto 0);
      add_r   : out std_logic_vector(19 downto 0);
      data_r  : out std_logic_vector(19 downto 0);
      write_r : out std_logic;
      trg_r   : out std_logic;
      l2y_r   : out std_logic;
      cs      : out std_logic;
      loadcs  : in  std_logic);
  end component;
end busint_pack;
-------------------------------------------------------------------------------
--
-- EOF
--



