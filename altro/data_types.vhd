-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Package to define data types
--! @ingroup altro_lib
package data_types_pack is

  type data_vector is array (15 downto 0) of integer range 0 to 1023;

end data_types_pack;
-------------------------------------------------------------------------------
--
-- EOF
--

