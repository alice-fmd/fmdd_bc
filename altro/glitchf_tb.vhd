------------------------------------------------------------------------------
-- Title      : Test of glitch filter in ALTRO code
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : glitchf_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/09/27
-- Platform   : 
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/27  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library altro_model;
use altro_model.glitchf_pack.all;

------------------------------------------------------------------------------
entity glitchf_tb is
end entity glitchf_tb;

------------------------------------------------------------------------------
architecture test of glitchf_tb is
  signal PERIOD : time      := 25 ns;
  signal x2_i   : std_logic := '0';
  signal clk_i  : std_logic := '0';     -- Clock
  signal rstb_i : std_logic := '0';     -- reset
  signal x1_i   : std_logic;

  procedure try (
    signal   x2    : out std_logic;
    signal   clk   : in  std_logic;
    constant delay : in  time) is
  begin  -- procedure try
    for i in 0 to 5 loop
      wait for 100 ns;
      wait until rising_edge(clk);
      wait for delay;
      x2 <= '1';
      for j in 0 to i loop
        wait until rising_edge(clk);
      end loop;  -- j
      wait for delay;
      x2 <= '0';
    end loop;  -- i
    
  end procedure try;

begin  -- architecture test
  dut: entity altro_model.glitchf
    port map (
      x2   => x2_i,                   -- in  
      clk  => clk_i,                  -- in  Clock
      rstb => rstb_i,                 -- in  reset
      x1   => x1_i);                  -- out

  -- purpose: Provide stimuli
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  stimuli: process is
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '1';

    try(x2 => x2_i, clk => clk_i, delay => 0 ns);
    try(x2 => x2_i, clk => clk_i, delay => 5 ns);
    try(x2 => x2_i, clk => clk_i, delay => 10 ns);
    try(x2 => x2_i, clk => clk_i, delay => 20 ns);

    wait; -- forever
  end process stimuli;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: 
  clock_gen: process is
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;
end architecture test;
------------------------------------------------------------------------------
-- 
-- EOF
--
