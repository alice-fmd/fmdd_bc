-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library altro_model;
use altro_model.intrech_pack.all;

-------------------------------------------------------------------------------
entity intrech_tb is
end intrech_tb;

-------------------------------------------------------------------------------
architecture test of intrech_tb is
  signal clk2_i     : std_logic := '0';  -- Clock
  signal rstb_i     : std_logic := '1';  -- Reset
  signal cstb_i     : std_logic := '0';  -- Clear
  signal cs_i       : std_logic := '0';  -- ??
  signal done_i     : std_logic := '0';  -- Done state from other FSM
  signal chrdo_i    : std_logic := '0';  -- Channel Read out from other 
  signal valid_i    : std_logic := '0';  -- Valid from other FSM
  signal rdo_done_i : std_logic := '0';  -- done from other FSM
  signal idle_i     : std_logic;         -- Idle state
  signal decode_i   : std_logic;         -- Decode state
  signal exec_i     : std_logic;         -- Execute state
  signal en1_i      : std_logic;         -- Enable 1 state 
  signal ack_i      : std_logic;         -- Acknowledge state
  signal en2_i      : std_logic;         -- Enable 2 state 
  signal exrdo_i    : std_logic;         -- Execute Read out state
  signal h_err_i    : std_logic;         -- Error state
  signal h_abt_i    : std_logic;         -- Abort state 
begin  -- test

  dut : intrech
    port map (
      clk2     => clk2_i,
      rstb     => rstb_i,
      cstb     => cstb_i,
      cs       => cs_i,
      done     => done_i,
      chrdo    => chrdo_i,
      valid    => valid_i,
      rdo_done => rdo_done_i,
      idle     => idle_i,
      decode   => decode_i,
      exec     => exec_i,
      en1      => en1_i,
      ack      => ack_i,
      en2      => en2_i,
      exrdo    => exrdo_i,
      h_err    => h_err_i,
      h_abt    => h_abt_i);

  clock_gen : process
  begin  -- process clock_gen
    clk2_i <= not clk2_i;
    wait for 10 ns;
  end process clock_gen;

  stimuli : process
  begin  -- process stimuli
    -- Reset
    rstb_i <= '0';
    wait for 20 ns;
    rstb_i <= '1';
    wait for 40 ns;

    -- Signals
    cstb_i     <= '1';
    cs_i       <= '1';
    valid_i    <= '1';
    done_i     <= '1';
    wait for 100 ns;
    chrdo_i    <= '1';
    cstb_i     <= '0';
    wait for 20 ns;
    rdo_done_i <= '1';
    wait;
  end process stimuli;

  end_sim : process (clk2_i)
  begin
    assert NOW < 500 ns report "End of simulation" severity failure;
  end process end_sim;
end test;
-------------------------------------------------------------------------------
--
-- EOF
--

