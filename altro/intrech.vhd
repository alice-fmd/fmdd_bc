-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
--! Decode exection of bus instructions
--! @ingroup altro_lib
entity intrech is
  port (
    clk2     : in  std_logic;           -- clock
    rstb     : in  std_logic;           -- Reset (active low)
    cstb     : in  std_logic;           -- Clear status (active low)
    cs       : in  std_logic;           -- ??? 
    done     : in  std_logic;           -- Done from other FSM
    chrdo    : in  std_logic;           -- Channel read out from other FSM 
    valid    : in  std_logic;           -- Valid from other FSM 
    rdo_done : in  std_logic;           -- Read out done from other FSM 
    idle     : out std_logic;           -- Idle state  
    decode   : out std_logic;           -- Decode state 
    exec     : out std_logic;           -- Execute state  
    en1      : out std_logic;           -- Enable state 1
    ack      : out std_logic;           -- Acknowledge  
    en2      : out std_logic;           -- Enable state 2 
    exrdo    : out std_logic;           -- Execute RDO 
    h_err    : out std_logic;           -- Error state 
    h_abt    : out std_logic);          -- Abort state  
end intrech;

-------------------------------------------------------------------------------
--! Decode exection of bus instructions
--! @ingroup altro_lib
architecture rtl of intrech is
  constant s_idle    : std_logic_vector(5 downto 0) := "000000";
  constant s_decode1 : std_logic_vector(5 downto 0) := "000111";
  constant s_decode2 : std_logic_vector(5 downto 0) := "011001";
  constant s_exec    : std_logic_vector(5 downto 0) := "011110";
  constant s_en1     : std_logic_vector(5 downto 0) := "101010";
  constant s_ack     : std_logic_vector(5 downto 0) := "101101";
  constant s_en2     : std_logic_vector(5 downto 0) := "110011";
  constant s_rdo     : std_logic_vector(5 downto 0) := "110100";
  signal   next_st_i : std_logic_vector(5 downto 0);
  signal   st_i      : std_logic_vector(5 downto 0);
begin  -- rtl
  p_sync : process (clk2, rstb)
  begin  -- process
    if rstb = '0' then                    -- asynchronous reset (active low)
      st_i <= s_idle;
    elsif clk2'event and clk2 = '1' then  -- rising clock edge
      st_i <= next_st_i;
    end if;
  end process;


  --! Hamming protected state machine.
  --! @param st_i     State
  --! @param cstb     Control strobe
  --! @param cs       Card/channel selected
  --! @param done     Done decoding (?)
  --! @param chrdo    Channel read-out
  --! @param valid    Valid instruction
  --! @param rdo_done Read-out done
  p_fsm : process (st_i, cstb, cs, done, chrdo, valid, rdo_done)
  begin  -- process
    next_st_i <= st_i;
    h_err     <= '0';
    h_abt     <= '0';
    idle      <= '0';
    decode    <= '0';
    exec      <= '0';
    en1       <= '0';
    ack       <= '0';
    en2       <= '0';
    exrdo     <= '0';

    -- Good
    case st_i is
      when s_idle    =>
        idle          <= '1';
        if cstb = '1' then
          next_st_i   <= s_decode1;
        end if;
      when s_decode1 =>
        decode        <= '1';
        if cstb = '1' and cs = '1' then
          next_st_i   <= s_decode2;
        else
          next_st_i   <= s_idle;
        end if;
      when s_decode2 =>
        if valid = '1' then
          if chrdo = '1' then
            next_st_i <= s_en1;
          else
            next_st_i <= s_exec;
          end if;
        else
          next_st_i   <= s_idle;
        end if;
      when s_exec    =>
        exec          <= '1';
        if done = '1' then
          next_st_i   <= s_en1;
        else
          next_st_i   <= s_exec;
        end if;
      when s_en1     =>
        en1           <= '1';
        next_st_i     <= s_ack;
      when s_ack     =>
        ack           <= '1';
        if not cstb = '1' then
          next_st_i   <= s_en2;
        else
          next_st_i   <= s_ack;
        end if;
      when s_en2     =>
        en2           <= '1';
        if not chrdo = '1' then
          next_st_i   <= s_idle;
        else
          next_st_i   <= s_rdo;
        end if;
      when s_rdo     =>
        exrdo         <= '1';
        if rdo_done = '1' then
          next_st_i   <= s_idle;
        else
          next_st_i   <= s_rdo;
        end if;
        -- Recoverable
      when "000001" |
        "000010" |
        "000100" |
        "001000" |
        "010000" |
        "100000"     =>                 -- From s_idle
        h_err         <= '1';
        idle          <= '1';
        if cstb = '1' then
          next_st_i   <= s_decode1;
        end if;
      when "000110" |
        "000101" |
        "000011" |
        "001111" |
        "010111" |
        "100111"     =>                 -- From s_decode1
        h_err         <= '1';
        decode        <= '1';
        if cstb = '1' and cs = '1' then
          next_st_i   <= s_decode2;
        else
          next_st_i   <= s_idle;
        end if;
      when "011000" |
        "011011" |
        "011101" |
        "010001" |
        "001001" |
        "111001"     =>                 -- From s_decode2
        h_err         <= '1';
        if chrdo = '1' then
          next_st_i   <= s_en1;
        else
          next_st_i   <= s_exec;
        end if;
      when "011111" |
        "011100" |
        "011010" |
        "010110" |
        "001110" |
        "111110"     =>                 -- From s_exec
        h_err         <= '1';
        exec          <= '1';
        if done = '1' then
          next_st_i   <= s_en1;
        else
          next_st_i   <= s_exec;
        end if;
      when "101011" |
        "101000" |
        "101110" |
        "100010" |
        "111010" |
        "001010"     =>                 -- From s_en1
        h_err         <= '1';
        en1           <= '1';
        next_st_i     <= s_ack;
      when "101100" |
        "101111" |
        "101001" |
        "100101" |
        "111101" |
        "001101"     =>                 -- From s_ack
        h_err         <= '1';
        ack           <= '1';
        if not cstb = '1' then
          next_st_i   <= s_en2;
        else
          next_st_i   <= s_ack;
        end if;
      when "110010" |
        "110001" |
        "110111" |
        "111011" |
        "100011" |
        "010011"     =>                 -- From s_en2
        h_err         <= '1';
        en2           <= '1';
        if not chrdo = '1' then
          next_st_i   <= s_idle;
        else
          next_st_i   <= s_rdo;
        end if;
      when "110101" |
        "110110" |
        "110000" |
        "111100" |
        "100100" |
        "010100"     =>                 -- From s_rdo
        h_err         <= '1';
        exrdo         <= '1';
        if rdo_done = '1' then
          next_st_i   <= s_idle;
        else
          next_st_i   <= s_rdo;
        end if;
      when others    =>
        h_abt         <= '1';
        next_st_i     <= s_idle;
    end case;
  end process;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Decode exection of bus instructions
--! @ingroup altro_lib
package intrech_pack is
  component intrech
    port (
      clk2     : in  std_logic;
      rstb     : in  std_logic;
      cstb     : in  std_logic;
      cs       : in  std_logic;
      done     : in  std_logic;
      chrdo    : in  std_logic;
      valid    : in  std_logic;
      rdo_done : in  std_logic;
      idle     : out std_logic;
      decode   : out std_logic;
      exec     : out std_logic;
      en1      : out std_logic;
      ack      : out std_logic;
      en2      : out std_logic;
      exrdo    : out std_logic;
      h_err    : out std_logic;
      h_abt    : out std_logic);
  end component;
end intrech_pack;
-------------------------------------------------------------------------------
--
-- EOF
--

