-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
--! Cadence alias entity
--! @ingroup altro_lib
entity cds_alias is
  generic (
    WIDTH                          :  integer := 1);    
  port (
    cds_alias_sig1          : in std_logic_vector(WIDTH downto 1);   
    cds_alias_sig2          : in std_logic_vector(WIDTH downto 1));   
end cds_alias;

-------------------------------------------------------------------------------
--! Empty architecture
--! @ingroup altro_lib
architecture rtl of cds_alias is
begin
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Package of Cadence alias
--! @ingroup altro_lib
package cds_alias_pack is
  component cds_alias
    generic (
      WIDTH : integer);
    port (
      cds_alias_sig1 : in std_logic_vector(WIDTH downto 1);
      cds_alias_sig2 : in std_logic_vector(WIDTH downto 1));
  end component;
end cds_alias_pack;
-------------------------------------------------------------------------------
--
-- EOF
--

