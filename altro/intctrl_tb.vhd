-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library altro_model;
use altro_model.intctrl_pack.all;

-------------------------------------------------------------------------------
entity intctrl_tb is
end intctrl_tb;

-------------------------------------------------------------------------------
architecture test of intctrl_tb is
  signal clk2_i   : std_logic := '0';   -- Clock
  signal rstb_i   : std_logic := '1';   -- Reset (active low)
  signal done_i   : std_logic := '0';   -- Done flag
  signal ch_red_i : std_logic := '0';   -- Channel ready
  signal cstb_i   : std_logic := '0';   -- Clear
  signal cs_i     : std_logic := '0';   -- ???
  signal chrdo_i  : std_logic := '0';   -- Channel read out
  signal valid_i  : std_logic := '0';   -- Valid data 
  signal load_i   : std_logic;
  signal exec_i   : std_logic;
  signal en1_i    : std_logic;
  signal ack_i    : std_logic;
  signal en2_i    : std_logic;          -- 
  signal rdo_i    : std_logic;
  signal waitst_i : std_logic;
  signal h_err_i  : std_logic;
  signal h_abt_i  : std_logic;
  signal loadcs_i : std_logic;
begin  -- test
  dut             : intctrl
    port map (
      clk2   => clk2_i,
      rstb   => rstb_i,
      done   => done_i,
      ch_red => ch_red_i,
      cstb   => cstb_i,
      cs     => cs_i,
      chrdo  => chrdo_i,
      valid  => valid_i,
      load   => load_i,
      exec   => exec_i,
      en1    => en1_i,
      ack    => ack_i,
      en2    => en2_i,
      rdo    => rdo_i,
      waitst => waitst_i,
      h_err  => h_err_i,
      h_abt  => h_abt_i,
      loadcs => loadcs_i);

  clock_gen : process
  begin  -- process clock_gen
    clk2_i <= not clk2_i;
    wait for 10 ns;
  end process clock_gen;

  stimuli : process
  begin  -- process stimuli
    -- Reset
    rstb_i <= '0';
    wait for 20 ns;
    rstb_i <= '1';
    wait for 40 ns;

    -- Signals
    ch_red_i <= '1';
    cstb_i   <= '1';
    cs_i     <= '1';
    valid_i  <= '1';
    done_i   <= '1';
    wait for 100 ns;
    chrdo_i  <= '1';
    cstb_i   <= '0';
    wait for 100 ns;
    ch_red_i <= '0';
    done_i   <= '1';
    wait;
  end process stimuli;

  end_sim : process (clk2_i)
  begin
    assert NOW < 500 ns report "End of simulation" severity failure;
  end process end_sim;


end test;
