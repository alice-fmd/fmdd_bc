-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library altro_model;
use altro_model.intdec_pack.all;
use altro_model.intexec_pack.all;
use altro_model.intctrl_pack.all;
use altro_model.busint_pack.all;
use altro_model.sync21_pack.all;

-------------------------------------------------------------------------------
entity interface is
  port (
    clk         : in  std_logic;        -- clock
    clk2        : in  std_logic;        -- clock 2
    rstb        : in  std_logic;        --  reset
    ch_red      : in  std_logic;        --  Channel read-out
    hadd        : in  std_logic_vector(7 downto 0);  -- hardware address
    bd          : in  std_logic_vector(39 downto 0);  -- bidirectional bus
    cstb        : in  std_logic;        -- Control stribe
    wrt         : in  std_logic;        -- Write flag
    trgi        : in  std_logic;        -- trigger
    twx         : in  std_logic;        -- Test mode bit 
    h_abt       : out std_logic;        -- Hamming state-machine abort
    h_err       : out std_logic;        -- Hamming state-machine error
    chrdo       : out std_logic;        -- Channel read-out
    ack         : out std_logic;        -- acknowledge
    ack_en      : out std_logic;        -- Enable acknowledge 
    dolo_en     : out std_logic;        -- Enable output 
    trsf_en     : out std_logic;        -- Transfer pulse 
    trg_overlap : out std_logic;        -- Trigger overlap flag
    csr_wr      : out std_logic;        -- Configuration status write 
    push        : out std_logic;        -- push data 
    pop         : out std_logic;        -- Pop data
    trg         : out std_logic;        -- trigger
    trc_clr     : out std_logic;        -- Trigger counter clear 
    err_clr     : out std_logic;        -- error clear
    wr_bsl      : out std_logic;        -- Write base-line
    rd_bsl      : out std_logic;        -- Read base-line
    instr_err   : out std_logic;        -- Instruction error 
    bcast       : out std_logic;        -- broadcast flag
    chadd       : out std_logic_vector(3 downto 0);  -- channel address
    csr_sel     : out std_logic_vector(4 downto 0);  -- Config/status select 
    hadd_o      : out std_logic_vector(7 downto 0);  -- output hardware address
    csr_do      : out std_logic_vector(19 downto 0);  --  Config/Status exec
    par_err     : out std_logic;        -- Parity error
    l2y         : in  std_logic;        -- L2 accept
    tstout      : out std_logic);       -- Test mode out. 
end interface;

-------------------------------------------------------------------------------
architecture rtl of interface is
  signal done_i      : std_logic;
  signal cs_i        : std_logic;
  signal chrdo_i     : std_logic;
  signal valid_i     : std_logic;
  signal load_i      : std_logic;
  signal exec_i      : std_logic;
  signal loadcs_i    : std_logic;
  signal en1_i       : std_logic;
  signal ackn_i      : std_logic;
  signal en2_i       : std_logic;
  signal waitst_i    : std_logic;
  signal h_abt_i     : std_logic;
  signal h_err_i     : std_logic;
  --
  signal add_r_i     : std_logic_vector(19 downto 0);
  signal write_r_i   : std_logic;
  signal rg_rd_i     : std_logic;
  signal rg_wr_i     : std_logic;
  signal push_i      : std_logic;
  signal pop_i       : std_logic;
  signal swtrg_i     : std_logic;
  signal trc_clr_i   : std_logic;
  signal err_clr_i   : std_logic;
  signal wr_bsl_i    : std_logic;
  signal rd_bsl_i    : std_logic;
  signal bcast_i     : std_logic;
  signal instr_err_i : std_logic;
  signal par_err_i   : std_logic;
  --
  signal trg_r_i     : std_logic;
  signal l2y_r_i     : std_logic;
  signal hwtrg_i     : std_logic;
  --
  signal csr_sel_i   : std_logic_vector(4 downto 0);
  --
  signal by0_i       : std_logic;
  signal by1_i       : std_logic;
  signal by2_i       : std_logic;
  signal by3_i       : std_logic;
  signal by4_i       : std_logic;
  signal by5_i       : std_logic;
  signal x0_i        : std_logic;
  signal x1_i        : std_logic;
  signal x2_i        : std_logic;
  signal x3_i        : std_logic;
  signal x4_i        : std_logic;
  signal x5_i        : std_logic;

begin  -- rtl
  csr_sel <= csr_sel_i;

  -- State machines
  i_ctrl : intctrl
    port map (
        clk2   => clk2,                 -- in  Clock 2
        rstb   => rstb,                 -- in  reset
        done   => done_i,               -- in  Done 
        ch_red => ch_red,               -- in  Channel read 
        cstb   => cstb,                 -- in  Control strobe
        cs     => cs_i,                 -- in  Card select 
        chrdo  => chrdo_i,              -- in  Channel readout 
        valid  => valid_i,              -- in  Valid instruction 
        load   => load_i,               -- out Load data  
        exec   => exec_i,               -- out Execute 
        en1    => en1_i,                -- out Enable  
        ack    => ack_i,                -- out Acknowledge
        en2    => en2_i,                -- out Enable 
        rdo    => chrdo,                -- out Read-out 
        waitst => waitst_i,             -- out Wait state  
        h_err  => h_err_i,              -- out Hamming state-machine error 
        h_abt  => h_abt_i,              -- out Hamming state machine abort 
        loadcs => loadcs_i);            -- out Load card switch


  -- Instruction decoder
  i_dec : intdec
    port map (
        add       => add_r_i,           -- in  address
        wrt       => wrt_r_i,           -- in  write flag
        cs        => cs_i,              -- in  Card select 
        valid     => valid_i,           -- out Valid instr. 
        chrdo     => chrdo_i,           -- out channel readout
        rg_wr     => rg_wr_i,           -- out write pointer
        rg_rd     => rg_rd_i,           -- out read pointer
        push      => push_i,            -- out Push it 
        pop       => pop_i,             -- out Pop it 
        swtrg     => swtrg_i,           -- out software trigger
        trc_clr   => trc_clr_i,         -- out trigger counter clr 
        err_clr   => err_clr_i,         -- out Error clear;
        wr_bsl    => wr_bsl_i,          -- out Write base-line 
        rd_bsl    => rd_bsl_i,          -- out Read base-line 
        bcast     => bcast_i,           -- out broadcast flag
        chadd     => chadd,             -- out channel address
        csr_sel   => csr_sel_i,         -- out Selected config 
        instr_err => instr_err_i,       -- out Instruction error 
        par_err   => par_err_i);        -- out Parity error

  -- bus interface
  i_bus : busint
    port map (
        l2y_i     => l2y_i,             -- in  L2 accept
        load      => load_i,            -- in  Load it 
        hadd      => hadd,              -- in  hardware address
        bd        => bd,                -- in  bidirectional bus
        wrt       => wrt,               -- in  Write enable
        trg       => trgi,              -- in  trigger
        clk       => clk,               -- in  clk2
        clk2      => clk2,              -- in  clock 2
        rstb      => rstb,              -- in  reset
        hadd_r    => hadd_o,            -- out hardware address
        add_r     => add_r_i,           -- out return of address
        data_r    => csr_do,            -- out data output
        write_r   => write_r_i,         -- out write return
        trg_r     => trg_r_i,           -- out trigger return
        l2y_r     => l2y_r_i,           -- out l2 accept return
        cs        => cs_i,              -- out Card select
        loadcs    => loadcs_i);         -- in Load card select

  -- instrucion executor
  i_exec : intexec
    port map (
      l2y_r       => l2y_r_i,
      exec        => exec_i,
      en1         => en1_i,
      ackn        => ackn_i,
      en2         => en2_i,
      waitst      => waitst_i,
      clk2        => clk2,
      rstb        => rstb,
      clk         => clk,
      valid       => valid_i,
      chrdo0      => chrdo_i,
      rg_rd0      => rg_rd_i,
      rg_wr0      => rg_wr_i,
      push0       => push_i,
      pop0        => pop_i,
      swtrg       => swtrg_i,
      trc_clr0    => trc_clr_i,
      err_clr0    => err_clr_i,
      wr_bsl0     => wr_bsl_i,
      rd_bsl0     => rd_bsl_i,
      bcast       => bcast_i,
      hwtrg       => hwtrg_i,
      twx         => twx,
      done        => done_i,
      ack         => ack,
      ack_en      => ack_en,
      dolo_en     => dolo_en,
      trsf_en     => trsf_en,
      trg_overlap => trg_overlap,
      rg_wr       => csr_wr,
      push        => push,
      pop         => pop,
      trg         => trg,
      trc_clr     => trc_clr,
      err_clr     => err_clr,
      wr_bsl      => wr_bsl,
      rd_bsl      => rd_bsl,
      tstout      => tstout);

  hwtrg_i <= trg_r_i;
  bcast   <= (not csr_sel_i(3) and not csr_sel_i(4)) and bcast_i;

  -- synchronizers
  i_sync0 : sync21
    port map (
      x2    => par_err_i,
      clk   => clk,
      clk2  => clk2,
      rstb  => rstb,
      x1    => x0_i,
      busyb => by0_i);

  i_sync1 : sync21
    port map (
      x2    => par_err_i,
      clk   => clk,
      clk2  => clk2,
      rstb  => rstb,
      x1    => x1_i,
      busyb => by1_i);

  i_sync2 : sync21
    port map (
      x2    => par_err_i,
      clk   => clk,
      clk2  => clk2,
      rstb  => rstb,
      x1    => x2_i,
      busyb => by2_i);

  par_err_i <= (x0_i and x1_i) or (x2_i and x1_i) or (x0_i and x2_i);

  i_sync3 : sync21
    port map (
      x2    => instr_err_i,
      clk   => clk,
      clk2  => clk2,
      rstb  => rstb,
      x1    => x3_i,
      busyb => by3_i);

  i_sync4 : sync21
    port map (
      x2    => instr_err_i,
      clk   => clk,
      clk2  => clk2,
      rstb  => rstb,
      x1    => x4_i,
      busyb => by4_i);

  i_sync5 : sync21
    port map (
      x2    => instr_err_i,
      clk   => clk,
      clk2  => clk2,
      rstb  => rstb,
      x1    => x5_i,
      busyb => by5_i);

  instr_err_i <= (x3_i and x4_i) or (x5_i and x3_i) or (x5_i and x4_i);

  p_errors : process (clk2, rstb)
  begin  -- process
    if rstb = '0' then                    -- asynchronous reset (active low)
      h_err <= h_err_i;
      h_abt <= h_abt_i;
    elsif clk2'event and clk2 = '1' then  -- rising clock edge
      h_err <= h_err_i;
      h_abt <= h_abt_i;
    end if;
  end process;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package interface_pack is
  component interface
    port (
      clk         : in  std_logic;
      clk2        : in  std_logic;
      rstb        : in  std_logic;
      ch_red      : in  std_logic;
      hadd        : in  std_logic_vector(7 downto 0);
      bd          : in  std_logic_vector(39 downto 0);
      cstb        : in  std_logic;
      wrt         : in  std_logic;
      trgi        : in  std_logic;
      twx         : in  std_logic;
      h_abt       : out std_logic;
      h_err       : out std_logic;
      chrdo       : out std_logic;
      ack         : out std_logic;
      ack_en      : out std_logic;
      dolo_en     : out std_logic;
      trsf_en     : out std_logic;
      trg_overlap : out std_logic;
      csr_wr      : out std_logic;
      push        : out std_logic;
      pop         : out std_logic;
      trg         : out std_logic;
      trc_clr     : out std_logic;
      err_clr     : out std_logic;
      wr_bsl      : out std_logic;
      rd_bsl      : out std_logic;
      instr_err   : out std_logic;
      bcast       : out std_logic;
      chadd       : out std_logic_vector(3 downto 0);
      csr_sel     : out std_logic_vector(4 downto 0);
      hadd_o      : out std_logic_vector(7 downto 0);
      csr_do      : out std_logic_vector(19 downto 0);
      par_err     : out std_logic;
      l2y         : in  std_logic;
      tstout      : out std_logic);
  end component;
end interface_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
