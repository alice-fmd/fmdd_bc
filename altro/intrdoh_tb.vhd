-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library altro_model;
use altro_model.intrdoh_pack.all;

-------------------------------------------------------------------------------
entity intrdoh_tb is

end intrdoh_tb;

-------------------------------------------------------------------------------
architecture test of intrdoh_tb is
  signal clk2_i   : std_logic := '0';
  signal rstb_i   : std_logic;
  signal exrdo_i  : std_logic;
  signal ch_red_i : std_logic;
  signal rdo_i    : std_logic;
  signal waitst_i : std_logic;
  signal done_i   : std_logic;
  signal h_err_i  : std_logic;
  signal h_abt_i  : std_logic;
begin  -- test

  dut : intrdoh
    port map (
      clk2   => clk2_i,
      rstb   => rstb_i,
      exrdo  => exrdo_i,
      ch_red => ch_red_i,
      rdo    => rdo_i,
      waitst => waitst_i,
      done   => done_i,
      h_err  => h_err_i,
      h_abt  => h_abt_i);

  clock_gen : process
  begin  -- process clock_gen
    clk2_i <= not clk2_i;
    wait for 10 ns;
  end process clock_gen;

  stimuli : process
  begin  -- process stimuli
    -- Reset
    ch_red_i <= '1';
    exrdo_i  <= '0';
    rstb_i   <= '0';
    wait for 20 ns;
    rstb_i   <= '1';
    wait for 40 ns;

    -- Signals
    exrdo_i  <= '1';
    wait for 60 ns;
    exrdo_i  <= '0';
    ch_red_i <= '0';
    wait for 20 ns;
    ch_red_i <= '1';
    wait;
  end process stimuli;

  end_sim : process (clk2_i)
  begin
    assert NOW < 500 ns report "End of simulation" severity failure;
  end process end_sim;
end test;
-------------------------------------------------------------------------------
--
-- EOF
--

