-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
--! Read-out module
--! @ingroup altro_lib
entity intrdoh is
  port (
    clk2   : in  std_logic;             --! Clock
    rstb   : in  std_logic;             --! Reset
    exrdo  : in  std_logic;             --! Execute read-out 
    ch_red : in  std_logic;             --! Channel ready (active low)
    rdo    : out std_logic;             --! In read-out state
    waitst : out std_logic;             --! In wait state
    done   : out std_logic;             --! In done state 
    h_err  : out std_logic;             --! Error state
    h_abt  : out std_logic);            --! Abort state
end intrdoh;

-------------------------------------------------------------------------------
--! Read-out module
--! @ingroup altro_lib
architecture rtl of intrdoh is
  constant s_idle    : std_logic_vector(4 downto 0) := "00000";
  constant s_rdo     : std_logic_vector(4 downto 0) := "00111";
  constant s_wait    : std_logic_vector(4 downto 0) := "11001";
  constant s_done    : std_logic_vector(4 downto 0) := "11110";
  signal   next_st_i : std_logic_vector(4 downto 0);
  signal   st_i      : std_logic_vector(4 downto 0);
begin  -- rstb
  -- Set the current state
  -- @param clk2 40Mhz clock
  -- @param rstb Async. reset
  p_sync : process (clk2, rstb)
  begin  -- process
    if rstb = '0' then                    -- asynchronous reset (active low)
      st_i <= s_idle;
    elsif clk2'event and clk2 = '1' then  -- rising clock edge
      st_i <= next_st_i;
    end if;
  end process;

  --! Hamming protected state machine
  --! @param st_i   State
  --! @param exrdo  Execute read-out
  --! @param ch_red Channel read-out
  p_fsm : process (st_i, exrdo, ch_red)
  begin  -- process
    next_st_i <= st_i;
    h_err     <= '0';
    h_abt     <= '0';
    done      <= '0';
    rdo       <= '0';
    waitst    <= '0';

    case st_i is
      -- Good
      when s_idle =>
        if exrdo = '1' then
          next_st_i <= s_rdo;
        else
          next_st_i <= s_idle;
        end if;
      when s_rdo  =>
        rdo         <= '1';
        next_st_i   <= s_wait;
      when s_wait =>
        waitst      <= '1';
        if ch_red = '1' then
          next_st_i <= s_done;
        else
          next_st_i <= s_wait;
        end if;
      when s_done =>
        done        <= '1';
        next_st_i   <= s_idle;
        -- recovery
      when "00001" |
        "00010" |
        "00100" |
        "01000" |
        "10000"   =>                    -- derived from s_idle
        h_err       <= '1';
        if exrdo = '1' then
          next_st_i <= s_rdo;
        else
          next_st_i <= s_idle;
        end if;
      when "00110" |
        "00101" |
        "00011" |
        "01111" |
        "10111"   =>                    -- derived from s_rdo
        h_err       <= '1';
        rdo         <= '1';
        next_st_i   <= s_wait;
      when "11000" |
        "11011" |
        "11101" |
        "10001" |
        "01001"   =>                    -- derived from s_wait
        h_err       <= '1';
        waitst      <= '1';
        if ch_red = '1' then
          next_st_i <= s_done;
        else
          next_st_i <= s_wait;
        end if;
      when "11111" |
        "11100" |
        "11010" |
        "10110" |
        "01110"   =>                    -- derived from s_done
        h_err       <= '1';
        done        <= '1';
        next_st_i   <= s_idle;
        -- bad
      when others =>
        h_abt       <= '1';
        next_st_i   <= s_idle;
    end case;
  end process;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Read-out module
--! @ingroup altro_lib
package intrdoh_pack is
  component intrdoh
    port (
      clk2   : in  std_logic;
      rstb   : in  std_logic;
      exrdo  : in  std_logic;
      ch_red : in  std_logic;
      rdo    : out std_logic;
      waitst : out std_logic;
      done   : out std_logic;
      h_err  : out std_logic;
      h_abt  : out std_logic);
  end component;
end intrdoh_pack;

-------------------------------------------------------------------------------
--
-- EOF
--

