-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
--! A glitch filter
--! @ingroup altro_lib
--! This filer insist on the input @a x2 to be stable for at least 3 clock
--! cycles, and then changes the output @a x1
entity glitchf is
  port (
    x2   : in  std_logic;               -- Input
    clk  : in  std_logic;               -- Clock
    rstb : in  std_logic;               -- reset
    x1   : out std_logic);              -- Output
end glitchf;

-------------------------------------------------------------------------------
--! A glitch filter
--! @ingroup altro_lib
architecture rtl of glitchf is
  signal w0_i : std_logic;              -- Wire 0
  signal r1_i : std_logic;              -- Register 1
  signal r2_i : std_logic;              -- Register 2
  signal r3_i : std_logic;              -- Register 3
  signal x1_i : std_logic;              -- Output
begin  -- rtl
  x1   <= x1_i;
  w0_i <= r1_i and r2_i;
  x1_i <= not(r3_i or not w0_i);

  --! The filter
  --! @param clk Clock
  --! @param rstb Async. reset
  p_filter : process (clk, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      r1_i <= '0';
      r2_i <= '0';
      r3_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      r1_i <= x2;
      r2_i <= r1_i;
      r3_i <= r2_i;
    end if;
  end process;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! A glitch filter
--! @ingroup altro_lib
package glitchf_pack is
  component glitchf
    port (
      x2   : in  std_logic;
      clk  : in  std_logic;
      rstb : in  std_logic;
      x1   : out std_logic);
  end component;
end glitchf_pack;
-------------------------------------------------------------------------------
--
-- EOF
--


