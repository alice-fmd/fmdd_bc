-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
--! Decode the bus signals
--! @ingroup altro_lib
entity intdec is
  port (
    add       : in  std_logic_vector(19 downto 0);  --! address
    wrt       : in  std_logic;                      --! write flag
    cs        : in  std_logic;                      --! Card select 
    valid     : out std_logic;                      --! Valid instr. 
    chrdo     : out std_logic;                      --! channel readout
    rg_wr     : out std_logic;                      --! write pointer
    rg_rd     : out std_logic;                      --! read pointer
    push      : out std_logic;                      --! Push it 
    pop       : out std_logic;                      --! Pop it 
    swtrg     : out std_logic;                      --! software trigger
    trc_clr   : out std_logic;                      --! trigger counter clr 
    err_clr   : out std_logic;                      --! Error clear;
    wr_bsl    : out std_logic;                      --! Write base-line 
    rd_bsl    : out std_logic;                      --! Read base-line 
    bcast     : out std_logic;                      --! broadcast flag
    chadd     : out std_logic_vector(3 downto 0);   --! channel address
    csr_sel   : out std_logic_vector(4 downto 0);   --! Selected config 
    instr_err : out std_logic;                      --! Instruction error 
    par_err   : out std_logic);                     --! Parity error 
end intdec;

-------------------------------------------------------------------------------
--! Decode the bus signals
--! @ingroup altro_lib
architecture rtl of intdec is
  signal w1_i           : std_logic;
  signal w2_i           : std_logic;
  signal w3_i           : std_logic;
  signal w4_i           : std_logic;
  signal bcast_rd_err_i : std_logic;
  signal bcast_err_i    : std_logic;
  signal bcast_err2_i   : std_logic;
  signal bcast_err3_i   : std_logic;
  signal reg_err_i      : std_logic;
  signal bank1_err_i    : std_logic;
  signal bank2_err_i    : std_logic;
  signal bank3_err_i    : std_logic;
  signal bank0_sel_i    : std_logic;
  signal bank1_sel_i    : std_logic;
  signal bank2_sel_i    : std_logic;
  signal bank3_sel_i    : std_logic;
  signal read_i         : std_logic;
  signal reg_err_new_i  : std_logic;
  signal instr_err_i    : std_logic;
  signal par_err_i      : std_logic;
  signal csr_sel_i      : std_logic_vector(4 downto 0);  --
  signal bcast_i        : std_logic;
  signal valid_i        : std_logic;
begin  -- rtl
  -- Channel register select
  csr_sel        <= csr_sel_i;
  csr_sel_i      <= add(4 downto 0);
  -- channel address
  chadd          <= add(8 downto 5);
  -- broadcast
  bcast          <= bcast_i;
  bcast_i        <= add(18);
  -- paraity error
  par_err        <= par_err_i;
  par_err_i      <= add(0) xor add(1) xor add(2) xor add(3) xor
                    add(4) xor add(5) xor add(6) xor add(7) xor
                    add(8) xor add(9) xor add(10) xor add(11) xor
                    add(12) xor add(13) xor add(14) xor add(15) xor
                    add(16) xor add(17) xor add(18) xor add(19);
  -- read signal
  read_i         <= not wrt;
  -- valid if not instruction error
  valid          <= valid_i;
  valid_i        <= not instr_err_i and not par_err_i;
  -- assignment of bank registers
  bank0_sel_i    <= '1'
                    when csr_sel_i(4 downto 3) = "00"
                    else '0';
  bank1_sel_i    <= '1'
                    when csr_sel_i(4 downto 3) = "01"
                    else '0';
  bank2_sel_i    <= '1'
                    when csr_sel_i(4 downto 3) = "10"
                    else '0';
  bank3_sel_i    <= '1'
                    when csr_sel_i(4 downto 3) = "11"
                    else '0';
  -- assignment error
  -- instruction error = broadcast error or register address error
  instr_err      <= instr_err_i;
  instr_err_i    <= bcast_err_i or reg_err_new_i;
  bcast_err_i    <= bcast_rd_err_i or bcast_err2_i or bcast_err3_i;
  -- gen error signals
  bcast_rd_err_i <= read_i and bcast_i;
  bcast_err2_i   <= bank2_sel_i and bcast_i;
  bcast_err3_i   <= '1'
                    when (wrt and bank3_sel_i and bcast_i
                          and csr_sel_i(1)) = '1'
                    else '0';
  -- registe error
  reg_err_new_i  <= reg_err_i and cs;
  reg_err_i      <= bank1_err_i or bank2_err_i or bank3_err_i;
  -- Def bank err
  bank1_err_i    <= '1'
                    when (bank1_sel_i = '1' and
                          ((csr_sel_i(2 downto 0) = "110") or
                           (csr_sel_i(2 downto 0) = "111")))
                    else '0';
  bank2_err_i    <= '1'
                    when (bank2_sel_i = '1' and
                          (wrt = '1' or
                           ((csr_sel_i(2 downto 0) = "001") or
                            (csr_sel_i(2 downto 0) = "011"))))
                    else '0';
  bank3_err_i    <= '1'
                    when (bank3_sel_i = '1' and
                          (read_i = '1' or
                           ((csr_sel_i(2 downto 0) = "110") or
                            (csr_sel_i(2 downto 0) = "111"))))
                    else '0';
  w2_i           <= '1'
                    when (valid_i and wrt and bank3_sel_i and cs) = '1'
                    else '0';
  -- Common conditions, chip selected
  push           <= '1' when csr_sel_i(2 downto 0) = "000" and w2_i = '1'
                    else '0';
  pop            <= '1' when csr_sel_i(2 downto 0) = "001" and w2_i = '1'
                    else '0';
  chrdo          <= '1' when csr_sel_i(2 downto 0) = "010" and w2_i = '1'
                    else '0';
  swtrg          <= '1' when csr_sel_i(2 downto 0) = "011" and w2_i = '1'
                    else '0';
  trc_clr        <= '1' when csr_sel_i(2 downto 0) = "100" and w2_i = '1'
                    else '0';
  err_clr        <= '1' when csr_sel_i(2 downto 0) = "101" and w2_i = '1'
                    else '0';
  -- Read/Write baseline
  w3_i           <= '1' when (cs and bank0_sel_i and valid_i) = '1' else '0';
  wr_bsl         <= '1' when csr_sel_i(2 downto 0) = "111" and
                    wrt = '1' and w3_i = '1'                          else '0';
  rd_bsl         <= '1' when csr_sel_i(2 downto 0) = "111" and
                    read_i = '1' and w3_i = '1'                       else '0';
  -- Read/Write register 
  w4_i           <= '1'
                    when cs = '1' and (bank0_sel_i = '1' or bank1_sel_i = '1'
                                       or bank2_sel_i = '1')
                    and valid_i = '1' else '0';
  rg_wr          <= wrt and w4_i;
  rg_rd          <= read_i and w4_i;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Decode the bus signals
--! @ingroup altro_lib
package intdec_pack is
  component intdec
    port (
      add       : in    std_logic_vector(19 downto 0);
      wrt       : in    std_logic;
      cs        : in    std_logic;
      valid     : out   std_logic;
      chrdo     : out   std_logic;
      rg_wr     : out   std_logic;
      rg_rd     : out   std_logic;
      push      : out   std_logic;
      pop       : out   std_logic;
      swtrg     : out   std_logic;
      trc_clr   : out   std_logic;
      err_clr   : out   std_logic;
      wr_bsl    : out   std_logic;
      rd_bsl    : out   std_logic;
      bcast     : out   std_logic;
      chadd     : out   std_logic_vector(3 downto 0);
      csr_sel   : out   std_logic_vector(4 downto 0);
      instr_err : out   std_logic;
      par_err   : out   std_logic);
  end component;
end intdec_pack;
-------------------------------------------------------------------------------
--
-- EOF
--




