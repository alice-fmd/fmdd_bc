-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library altro_model;
use altro_model.intrech_pack.all;
use altro_model.intrdoh_pack.all;

-------------------------------------------------------------------------------
--! Controller
--! @ingroup altro_lib
entity intctrl is
  port (
    clk2   : in  std_logic;             --! Clock 2
    rstb   : in  std_logic;             --! reset
    done   : in  std_logic;             --! Done 
    ch_red : in  std_logic;             --! Channel read 
    cstb   : in  std_logic;             --! Control strobe
    cs     : in  std_logic;             --! Card select 
    chrdo  : in  std_logic;             --! Channel readout 
    valid  : in  std_logic;             --! Valid instruction 
    load   : out std_logic;             --! Load data  
    exec   : out std_logic;             --! Execute 
    en1    : out std_logic;             --! Enable  
    ack    : out std_logic;             --! Acknowledge
    en2    : out std_logic;             --! Enable 
    rdo    : out std_logic;             --! Read-out 
    waitst : out std_logic;             --! Wait state  
    h_err  : out std_logic;             --! Hamming state-machine error 
    h_abt  : out std_logic;             --! Hamming state machine abort 
    loadcs : out std_logic);            --! Load card switch 
end intctrl;

-------------------------------------------------------------------------------
--! Controller
--! @ingroup altro_lib
architecture rtl of intctrl is
  signal decode_i   : std_logic;        --! Decoded bus
  signal exec_i     : std_logic;        --! Execute 
  signal exrdo_i    : std_logic;        --! Execute read-out
  signal h_err0_i   : std_logic;        --! Hamming error 0
  signal h_abt0_i   : std_logic;        --! Hamming abort 0
  signal h_err1_i   : std_logic;        --! Hamming error 1
  signal h_abt1_i   : std_logic;        --! Hamminb abort 1
  signal idle_i     : std_logic;        --! In idle state
  signal rdo_done_i : std_logic;        --! Read-out done
begin  -- rstb
  --! Read-out interface
  i_rdo : intrdoh
    port map (
      clk2   => clk2,
      rstb   => rstb,
      exrdo  => exrdo_i,
      ch_red => ch_red,
      rdo    => rdo,
      waitst => waitst,
      done   => rdo_done_i,
      h_err  => h_err0_i,
      h_abt  => h_abt0_i);

  i_rec : intrech
    port map (
      clk2     => clk2,
      rstb     => rstb,
      cstb     => cstb,
      cs       => cs,
      done     => done,
      chrdo    => chrdo,
      valid    => valid,
      rdo_done => rdo_done_i,
      idle     => idle_i,
      decode   => decode_i,
      exec     => exec,
      en1      => en1,
      ack      => ack,
      en2      => en2,
      exrdo    => exrdo_i,
      h_err    => h_err1_i,
      h_abt    => h_abt1_i);

  h_err  <= h_err0_i or h_err1_i;       -- Hamming error
  h_abt  <= h_abt0_i or h_abt1_i;       -- Hamming abor
  load   <= cs and decode_i;            -- Load it
  loadcs <= idle_i;                     -- Whether we're idle 
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


--! Controller
--! @ingroup altro_lib
package intctrl_pack is
  component intctrl
    port (
      clk2   : in  std_logic;
      rstb   : in  std_logic;
      done   : in  std_logic;
      ch_red : in  std_logic;
      cstb   : in  std_logic;
      cs     : in  std_logic;
      chrdo  : in  std_logic;
      valid  : in  std_logic;
      load   : out std_logic;
      exec   : out std_logic;
      en1    : out std_logic;
      ack    : out std_logic;
      en2    : out std_logic;             -- 
      rdo    : out std_logic;
      waitst : out std_logic;
      h_err  : out std_logic;
      h_abt  : out std_logic;
      loadcs : out std_logic);
  end component;
end intctrl_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
