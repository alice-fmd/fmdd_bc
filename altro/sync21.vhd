-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
--! Syncronise @a x2 to clocks @a clk and @a clk2
--! @ingroup altro_lib
entity sync21 is
  port (
    x2    : in  std_logic;              --! Input  
    clk   : in  std_logic;              --! clock
    clk2  : in  std_logic;              --! clock
    rstb  : in  std_logic;              --! reset
    x1    : out std_logic;              --! output  
    busyb : out std_logic);             --! Busy  
end sync21;

-------------------------------------------------------------------------------
--! Syncronise @a x2 to clocks @a clk and @a clk2
--! @ingroup altro_lib
architecture rtl of sync21 is
  signal w0_i    : std_logic;           --! Wire 0
  signal w1_i    : std_logic;           --! Wire 1
  signal r1_i    : std_logic;           --! Register 1
  signal r2_i    : std_logic;           --! Register 2
  signal r3_i    : std_logic;           --! Register 3
  signal x1_i    : std_logic;           --! Output
  signal busyb_i : std_logic;           --! Busy signal
begin  -- rtl
  x1      <= x1_i;
  busyb   <= busyb_i;
  w0_i    <= (r3_i or not(r2_i or not r1_i)) and not x1_i;
  w1_i    <= r3_i and not x1_i;
  busyb_i <= not x1_i and not r3_i;

  --! Syncronise to @a clk
  --! @param clk  Clock to syncronise to
  --! @param rstb Async. reset
  --! @return x1_i
  p_sync1 : process (clk, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      x1_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      x1_i <= w1_i;
    end if;
  end process;

  --! Syncronise to @a clk2
  --! Input signal @a x2 is cascaded through 2 registers (@a r1_i, @a r2_i) 
  --! @param clk2  Clock to syncronise to
  --! @param rstb Async. reset
  --! @return r1_i, r2_i, r3_i
  p_sync2 : process (clk2, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      r1_i <= '0';
      r2_i <= '0';
      r3_i <= '0';
    elsif clk2'event and clk2 = '1' then  -- rising clock edge
      r1_i <= x2;
      r2_i <= r1_i;
      r3_i <= w0_i;
    end if;
  end process;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Syncronise @a x2 to clocks @a clk and @a clk2
--! @ingroup altro_lib
package sync21_pack is
  component sync21
    port (
      x2    : in  std_logic;
      clk   : in  std_logic;
      clk2  : in  std_logic;
      rstb  : in  std_logic;
      x1    : out std_logic;
      busyb : out std_logic);
  end component;
end sync21_pack;
-------------------------------------------------------------------------------
--
-- EOF
--


