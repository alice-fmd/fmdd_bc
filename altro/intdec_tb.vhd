-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library altro_model;
use altro_model.intdec_pack.all;

-------------------------------------------------------------------------------
entity intdec_tb is
end intdec_tb;

-------------------------------------------------------------------------------

architecture test of intdec_tb is
  signal clk_i       : std_logic := '0';
  signal add_i       : std_logic_vector(19 downto 0);  -- Address
  signal wrt_i       : std_logic;                      -- Write flag
  signal cs_i        : std_logic := '0';                      -- ???
  signal valid_i     : std_logic;                      -- Valid
  signal chrdo_i     : std_logic;                      -- Channel readout
  signal rg_wr_i     : std_logic;                      -- Write pointer
  signal rg_rd_i     : std_logic;                      -- Read pointer
  signal push_i      : std_logic;                      -- Push 
  signal pop_i       : std_logic;                      -- Pop 
  signal swtrg_i     : std_logic;                      -- Software trigger
  signal trc_clr_i   : std_logic;                      -- Trigger counter clear
  signal err_clr_i   : std_logic;                      -- Error status clear 
  signal wr_bsl_i    : std_logic;                      -- Write baseline 
  signal rd_bsl_i    : std_logic;                      -- Read base line 
  signal bcast_i     : std_logic;                      -- Broadcast 
  signal chadd_i     : std_logic_vector(3 downto 0);   -- Channel address
  signal csr_sel_i   : std_logic_vector(4 downto 0);   -- ???
  signal instr_err_i : std_logic;                      -- Instruction error
  signal par_err_i   : std_logic;                      -- Parity error  

  constant BANK0   : std_logic_vector(1 downto 0)  := "00";
  constant BANK1   : std_logic_vector(1 downto 0)  := "01";
  constant BANK2   : std_logic_vector(1 downto 0)  := "10";
  constant BANK3   : std_logic_vector(1 downto 0)  := "11";
  constant PUSH    : std_logic_vector (2 downto 0) := "000";  -- BANK3
  constant POP     : std_logic_vector (2 downto 0) := "001";  -- BANK3
  constant CHRDO   : std_logic_vector (2 downto 0) := "010";  -- BANK3
  constant SWTRG   : std_logic_vector (2 downto 0) := "011";  -- BANK3
  constant TRC_CLR : std_logic_vector (2 downto 0) := "100";  -- BANK3
  constant ERR_CLR : std_logic_vector (2 downto 0) := "101";  -- BANK3
  constant BSL     : std_logic_vector (2 downto 0) := "111";  -- BANK0


begin  -- test
  dut : intdec
    port map (
      add       => add_i,
      wrt       => wrt_i,
      cs        => cs_i,
      valid     => valid_i,
      chrdo     => chrdo_i,
      rg_wr     => rg_wr_i,
      rg_rd     => rg_rd_i,
      push      => push_i,
      pop       => pop_i,
      swtrg     => swtrg_i,
      trc_clr   => trc_clr_i,
      err_clr   => err_clr_i,
      wr_bsl    => wr_bsl_i,
      rd_bsl    => rd_bsl_i,
      bcast     => bcast_i,
      chadd     => chadd_i,
      csr_sel   => csr_sel_i,
      instr_err => instr_err_i,
      par_err   => par_err_i);


  stimuli : process
  begin  -- process stimuli
    cs_i  <= '0';
    wrt_i <= '1';
    add_i <= (others => '0');
    wait for 10 ns;

    -- 0-4    channel register
    -- 5-8    channel address
    -- 18     broadcast
    -- Bank 0 selected
    cs_i  <= '1';
    add_i <= "10000000000000111000";
    wait for 10 ns;

    cs_i  <= '0';
    wrt_i <= '1';
    add_i <= (others => '0');
    wait for 10 ns;

    wait;
  end process stimuli;

  clock_gen : process
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for 10 ns;
  end process clock_gen;

  end_sim : process (clk_i)
  begin
    assert NOW < 500 ns report "End of simulation" severity failure;
  end process end_sim;


end test;

-------------------------------------------------------------------------------

