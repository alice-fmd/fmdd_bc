-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
--! Syncronise @a x2 to clocks @a clk and @a clk2
--! @ingroup altro_lib
entity sync221 is
  port (
    x2   : in  std_logic;               --! input
    clk  : in  std_logic;               --! clock
    clk2 : in  std_logic;               --! clock 2
    rstb : in  std_logic;               --! reset
    x1   : out std_logic);              --! Output  
end sync221;

-------------------------------------------------------------------------------
--! Syncronise @a x2 to clocks @a clk and @a clk2
--! @ingroup altro_lib
architecture rtl of sync221 is
  signal w0_i : std_logic;              --
  signal w1_i : std_logic;              --
  signal w2_i : std_logic;              --
  signal w3_i : std_logic;              --
  signal w4_i : std_logic;              --
  signal r1_i : std_logic;
  signal r2_i : std_logic;
  signal r3_i : std_logic;
  signal r4_i : std_logic;
  signal x1_i : std_logic;
begin  -- rtl
  x1   <= x1_i;
  w0_i <= r1_i and r2_i;
  w1_i <= not (r3_i or not w0_i);
  w2_i <= not x1_i and (w1_i or w4_i);
  w3_i <= not x1_i and r4_i;

  --! Syncronise to @a clk
  --! @param clk  Clock to syncronise to
  --! @param rstb Async. reset
  --! @return x1_i
  p_sync1 : process (clk, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      x1_i <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      x1_i <= w3_i;
    end if;
  end process;

  --! Syncronise to @a clk2
  --! Input signal @a x2 is cascaded through 2 registers (@a r1_i, @a r2_i) 
  --! @param clk2  Clock to syncronise to
  --! @param rstb Async. reset
  --! @return r1_i, r2_i, r3_i
  p_sync2 : process (clk2, rstb)
  begin  -- process
    if rstb = '0' then                    -- asynchronous reset (active low)
      r1_i <= '0';
      r2_i <= '0';
      r3_i <= '0';
      r4_i <= '0';
    elsif clk2'event and clk2 = '1' then  -- rising clock edge
      r1_i <= x2;
      r2_i <= r1_i;
      r3_i <= r2_i;
      r4_i <= w2_i;
    end if;
  end process;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

--! Syncronise @a x2 to clocks @a clk and @a clk2
--! @ingroup altro_lib
package sync221_pack is
  component sync221
    port (
      x2   : in  std_logic;
      clk  : in  std_logic;
      clk2 : in  std_logic;
      rstb : in  std_logic;
      x1   : out std_logic);
  end component;
end sync221_pack;
-------------------------------------------------------------------------------
--
-- EOF
--



