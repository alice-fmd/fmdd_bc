-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;

library altro_model;
use altro_model.altro_pack.all;
use altro_model.data_types_pack.all;

-------------------------------------------------------------------------------
entity altro_tb is

end altro_tb;

-------------------------------------------------------------------------------
architecture test of altro_tb is
  constant TIMESCALE : time    := 1 ns;
  constant T1        : time    := 100 ns;  -- Period of ADC clock
  constant T2        : time    := 24 ns;   -- Period of bus clock
  constant DCHIP     : time    := 2 ns;    -- output delay of ALTRO
  constant DRCU      : time    := 4 ns;    -- output delay of RCU
  constant ACK_TO    : integer := 20;      -- ack timeout in clk2 cycles

  signal clk_i      : std_logic                     := '0';
  signal clk2_i     : std_logic                     := '0';
  signal rstb_i     : std_logic                     := '0';
  signal trg_i      : std_logic                     := '0';
  signal l2y_i      : std_logic                     := '0';
  signal cstb_i     : std_logic                     := '1';
  signal writeb_i   : std_logic;
  signal ackb_i     : std_logic;
  signal ackb_en_i  : std_logic;
  signal dolob_en_i : std_logic;
  signal trsfb_i    : std_logic;
  signal trsfb_en_i : std_logic;
  signal dstb_i     : std_logic;
  signal errorb_i   : std_logic;
  signal tstoutb_i  : std_logic;
  signal bd_i       : std_logic_vector(39 downto 0) := (others => '0');
  signal dummy      : std_logic_vector(19 downto 0);
  signal data_i     : data_vector;

-- input async
begin  -- sim
  -----------------------------------------------------------------------------
  altro_i : altro
    port map (
      clk      => clk_i,
      clk2     => clk2_i,
      rstb     => rstb_i,
      trg      => trg_i,
      l2y      => l2y_i,
      cstb     => cstb_i,
      writeb   => writeb_i,
      ackb     => ackb_i,
      ackb_en  => ackb_en_i,
      dolob_en => dolob_en_i,
      trsfb    => trsfb_i,
      trsfb_en => trsfb_en_i,
      dstb     => dstb_i,
      errorb   => errorb_i,
      tstoutb  => tstoutb_i,
      bd       => bd_i,
      data     => data_i);

  -----------------------------------------------------------------------------
  sclock_gen : process
  begin  -- process
    clk_i <= not clk_i;                 -- generate sample clock
    wait for 0.5 * T1;
  end process;

  -----------------------------------------------------------------------------
  bclock_gen : process
  begin  -- process
    clk2_i <= not clk2_i;               -- generate bus clock
    wait for 0.5 * T2;
  end process;

  -----------------------------------------------------------------------------
  -- purpose: Read/Write regisrers and send all possible commands
  -- including non-existenting nstructions  
  stimuli        : process
    variable ret : std_logic_vector(19 downto 0);
    variable j   : integer;
    variable i   : integer;
    ---------------------------------------------------------------------------
    procedure sync_ck is
    begin  -- sync_ck
      if clk_i = '1' then
        wait until not clk_i = '1';
      end if;
      wait until clk_i = '1';
    end sync_ck;

    ---------------------------------------------------------------------------
    procedure sync_ck2 is
    begin
      if clk2_i = '1' then
        wait until not clk2_i = '1';
      end if;
      wait until clk2_i = '1';
    end sync_ck2;

    ---------------------------------------------------------------------------
    procedure wait_ack is
      variable i : integer;
    begin
      if ackb_i = '0' then
        wait until not (ackb_i = '0');
      end if;
      i   := ACK_TO;
      while i > 0 and not (ackb_i = '0') loop
        sync_ck2;
        i := i - 1;
      end loop;
      assert i > 0 report "Acknowledge timeout" severity warning;
    end wait_ack;

    ---------------------------------------------------------------------------
    procedure parity (
      bd_ii : inout std_logic_vector(39 downto 0)) is
    begin  -- parity (even)
      bd_ii(39) := bd_ii(38) xor bd_ii(37) xor bd_ii(36) xor
                   bd_ii(35) xor bd_ii(34) xor bd_ii(33) xor
                   bd_ii(32) xor bd_ii(31) xor bd_ii(30) xor
                   bd_ii(29) xor bd_ii(28) xor bd_ii(27) xor
                   bd_ii(26) xor bd_ii(25) xor bd_ii(24) xor
                   bd_ii(23) xor bd_ii(22) xor bd_ii(21) xor
                   bd_ii(20);
    end parity;

    ---------------------------------------------------------------------------
    procedure io_csr (
      constant wrt   : in    std_logic;
      constant csr   : in    std_logic_vector(4 downto 0);
      constant chadd : in    std_logic_vector(3 downto 0);
      variable data  : inout std_logic_vector(19 downto 0)) is
      variable bd_ii :       std_logic_vector(39 downto 0);
    begin
      -- Wait for acknowledge to not be asserted 
      if ackb_i = '0' then
        wait until not (ackb_i = '0');
      end if;
      -- Wait for transfer to not be asserted 
      if trsfb_i = '0' then
        wait until not trsfb_i = '0';
      end if;

      sync_ck2;
      wait for DRCU;

      -- Build up bus signal 
      bd_ii(38 downto 37) := "00";
      bd_ii(36 downto 29) := "00000001";  -- chip address
      bd_ii(28 downto 25) := "0000";      -- no channel address
      bd_ii(24 downto 20) := csr;         -- register address
      bd_ii(19 downto  0) := data;         -- register address
      parity(bd_ii);
      bd_i <= bd_ii;

      -- Set write and control strobe 
      writeb_i <= not wrt;
      cstb_i   <= '0';

      assert not (errorb_i = '0')
        report "Error from ALTRO on instruction" severity warning;
      -- Wait for acknowledgement 
      wait_ack;

      -- Sync to clock edge, and simulate RCU delay 
      sync_ck2;
      wait for DRCU;

      -- Get the data from the bus, take up the command strobe, and set the bus
      -- to high impedance
      data     := bd_i(19 downto 0);
      cstb_i   <= '1';
      bd_i     <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      writeb_i <= wrt;
    end io_csr;


    --------------------------------------------------------------------------
    procedure read_csr (
      csr   : in  std_logic_vector (4 downto 0);
      chadd : in  std_logic_vector (3 downto 0);
      data  : inout std_logic_vector (19 downto 0)) is
    begin
      io_csr('0', csr, chadd, data);
    end read_csr;

    --------------------------------------------------------------------------
    procedure write_csr (
      csr   : in  std_logic_vector (4 downto 0);
      chadd : in  std_logic_vector (3 downto 0);
      data  : inout std_logic_vector (19 downto 0)) is
    begin
      io_csr('1', csr, chadd, data);
    end write_csr;

    ---------------------------------------------------------------------------
    procedure wait_transfer is
      variable trs_r_ii : integer;
    begin  -- wait_transfer
      if trsfb_i = '0' then
        wait until trsfb_i = '1';
      end if;
      trs_r_ii   := ACK_TO;
      while trs_r_ii > 0 and trsfb_i = '1' loop
        sync_ck2;
        trs_r_ii := trs_r_ii - 1;
      end loop;
      assert trs_r_ii > 0 report "Transfer timeout" severity warning;
      wait until not trsfb_i = '0';
    end wait_transfer;

    ---------------------------------------------------------------------------
  begin

    wait for T2;
    wait for DRCU;

    rstb_i <= '0';
    wait for 50 * TIMESCALE;
    rstb_i <= '1';
    wait for 1000 * TIMESCALE;

    for j in 0 to 25 loop
      ret   := conv_std_logic_vector(5 * j, 20);
      write_csr(conv_std_logic_vector(j, 5), "0000", ret);
    end loop;
    
    ret := "00000000000000000001";
    for i in 0 to 15 loop
      data_i(i) <= i + 1;
    end loop;
    write_csr("11010", "0000", ret);
    wait_transfer;

    for j in 27 to 31 loop
      ret   := conv_std_logic_vector(5 * j, 20);
      write_csr(conv_std_logic_vector(j, 5), "0000", ret);
    end loop;
    wait for 1000 * TIMESCALE;

    for j in 0 to 31 loop
      read_csr(conv_std_logic_vector(j, 5), "0000", ret);
      dummy <= ret;
    end loop;
    wait;

  end process;

  end_sim : process (clk2_i)
  begin
    assert NOW < 40 us report "End of simulation" severity failure;
  end process end_sim;
end test;
-------------------------------------------------------------------------------
--
-- EOF
--













