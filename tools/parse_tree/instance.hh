#ifndef PARSE_TREE_INSTANCE_HH
#define PARSE_TREE_INSTANCE_HH
#ifndef __STRING__
# include <string>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

namespace parse_tree
{
  struct entity;
  struct master;
  
  struct instance 
  {
    std::string _name;
    entity*     _entity;
    instance(const std::string& n) : _name(n), _entity(0) {}
    bool  parse(master& m);
    void  print(size_t& i) const;
    float write(std::ostream& out, const std::string& sheet,
		size_t id, float  s, float  x,  float  y) const;
  };
}

#endif
//____________________________________________________________________    
//
// EOF
//
