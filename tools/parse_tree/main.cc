#include "parse_tree/master.hh"
#include <iostream>
#include <fstream>
#include <string>


//====================================================================
int
main(int argc, char** argv) 
{
  if (argc <= 1) {
    std::cerr << "Usage: " << argv[0] << " INPUT" << std::endl;
    return 1;
  }
  std::ifstream in(argv[1]);
  try {
    parse_tree::master m(in);
    if (!m.read()) return 1;
    m.write("BC");
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}
//____________________________________________________________________
//
// EOF
//
