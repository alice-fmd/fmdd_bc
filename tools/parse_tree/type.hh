#ifndef PARSE_TREE_TYPE_HH
#define PARSE_TREE_TYPE_HH

namespace parse_tree 
{
  //____________________________________________________________________    
  enum type 
    {
      unk,
      ent,
      arc, 
      blk, 
      prc,
      sig,
      ins,
      gen,
      inp,
      oup,
      iop,
      pkg
    };
}
#endif
//
// EOF
//
