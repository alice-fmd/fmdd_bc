#ifndef PARSE_TREE_LINE_HH
#define PARSE_TREE_LINE_HH
#ifndef PARSE_TREE_TYPE_HH
# include <parse_tree/type.hh>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace parse_tree
{
  //____________________________________________________________________    
  struct line
  {
    type        _type;
    std::string _name;
    size_t      _lvl;
    size_t      _line;
    void print() const;
  };
}
#endif
//____________________________________________________________________
//
// EOF
//
