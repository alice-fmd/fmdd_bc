#ifndef PARSE_TREE_PORT_HH
#define PARSE_TREE_PORT_HH
#ifndef PARSE_TREE_TYPE_HH
# include <parse_tree/type.hh>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace parse_tree
{
  struct port
  {
    enum {
      in, 
      out, 
      inout
    } _type;
    std::string _name;
    port(const std::string& n, type t);
    void print(size_t i) const;
  };
}
#endif
//____________________________________________________________________    
//
// EOF
//
