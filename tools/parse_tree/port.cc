#include "parse_tree/port.hh"
#include <iostream>


//====================================================================
parse_tree::port::port(const std::string& n, type t) 
  : _name(n) 
{
  switch (t) {
  case inp: _type = in;    break;
  case oup: _type = out;   break;
  case iop: _type = inout; break;
  default: break;
  }
}

//____________________________________________________________________
void 
parse_tree::port::print(size_t i) const 
{
  for (size_t j = 0; j < i; j++) std::cout << ' ';
  switch (_type) {
  case in:    std::cout << "in   "; break;
  case out:   std::cout << "out  "; break;
  case inout: std::cout << "inout"; break;
  }
  std::cout << " : " << _name << std::endl;
}

//____________________________________________________________________
//
// EOF
//
