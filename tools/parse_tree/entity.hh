#ifndef PARSE_TREE_ENTITY_HH
#define PARSE_TREE_ENTITY_HH
#ifndef PARSE_TREE_INSTANCE_HH
# include <parse_tree/instance.hh>
#endif
#ifndef PARSE_TREE_PORT_HH
# include <parse_tree/port.hh>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef __LIST__
# include <list>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

namespace parse_tree 
{
  struct master;
  
  struct entity 
  {
    typedef std::list<instance> instance_list;
    typedef std::list<port>     port_list;
    std::string         _name;
    std::string         _arch;
    instance_list       _instances;
    port_list           _ports;
    bool                _done;
    mutable size_t      _max;
    mutable float       _width;
    mutable float       _height;
    static const float  _dy;
    static const float  _margin;
    static const float  _padding;
  
    entity(const std::string& n) 
      : _name(n),_done(false), _max(0),_width(-1),_height(-1)
    {}
    bool   parse(master& m);
    void   print(size_t& i) const;
    void   write(const std::string& sheet) const;
    void   write_shape(const std::string& sheet) const;
    void   write_diagram(const std::string& sheet) const;
    void   write_icon(const std::string& sheet) const;
    float  width() const;
    float  height() const;
    size_t max() const;
  };
}
#endif
//____________________________________________________________________    
//
// EOF
//
