#ifndef PARSE_TREE_MASTER_HH
#define PARSE_TREE_MASTER_HH
#ifndef PARSE_TREE_LINE_HH
# include <parse_tree/line.hh>
#endif
#ifndef PARSE_TREE_INSTANCE_HH
# include <parse_tree/instance.hh>
#endif
#ifndef PARSE_TREE_ENTITY_HH
# include <parse_tree/entity.hh>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef __MAP__
# include <map>
#endif

namespace parse_tree
{
  struct master 
  {
    typedef std::map<std::string,entity> entity_map;
    entity_map     _entities;
    std::istream&  _in;
    line           _line;
    instance       _top;

    master(std::istream& in);
    entity& add(const std::string& n);
    bool    read();
    bool    parse();
    void    print();
    bool    error(const char* format, ...);
    void    write(const std::string& sheet) const;
    void    write_port(const std::string& sheet, 
		       const std::string& name) const;
  };
}
#endif
//____________________________________________________________________
//
// EOF
//
