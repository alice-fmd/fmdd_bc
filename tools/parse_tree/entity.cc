#include "parse_tree/entity.hh"
#include "parse_tree/master.hh"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cassert>

//====================================================================
size_t 
parse_tree::entity::max() const
{
  if (_max != 0) return _max;
  size_t nleft  = 0;
  size_t nright = 0;
  for (port_list::const_iterator p = _ports.begin(); p != _ports.end(); ++p) {
    if (p->_type == port::in) nleft++;
    else                      nright++;
  }
  _max = std::max(nleft, nright);
  return _max;
}

//____________________________________________________________________
float 
parse_tree::entity::width() const
{
  return _width = 5;
}

//____________________________________________________________________
float 
parse_tree::entity::height() const
{
  if (_height > 0) return _height;
  return _height = max() * _dy + 2 * _margin + 2 * _padding;
}
//____________________________________________________________________
const float  parse_tree::entity::_dy      = .5;
const float  parse_tree::entity::_margin  = .5;
const float  parse_tree::entity::_padding = .5;

//____________________________________________________________________
bool 
parse_tree::entity::parse(master& m) 
{
  line&  l    = m._line;
  size_t last = l._lvl;
  while (true) {
    if (m._in.eof()) break;
    // Read the name 
    line&  l    = m._line;
    if      (l._lvl < last) { 
      _done = true;
      break;
    }
    else if (l._type == ins && !_done) {
      _instances.push_back(instance(l._name));
      instance& i = (_instances.back());
      if (!m.parse()) return false;
      if (!i.parse(m)) return false;
    }
    else if (!_done && (l._type == inp || l._type == oup || l._type == iop)) {
      _ports.push_back(port(l._name, l._type));
      if (!m.parse()) return false;
    }
    else if (!m.parse()) return false;
  }
  return true;
}

//____________________________________________________________________    
void 
parse_tree::entity::print(size_t& i) const
{
  std::cout << _name << std::endl;
  i++;
  for (port_list::const_iterator p = _ports.begin(); p != _ports.end(); ++p) 
    p->print(i);
  for (instance_list::const_iterator d = _instances.begin(); 
       d != _instances.end(); ++d) 
    d->print(i);
  i--;
}

//____________________________________________________________________    
void 
parse_tree::entity::write(const std::string& sheet) const
{
  write_shape(sheet);
  write_diagram(sheet);
  write_icon(sheet);
}

//____________________________________________________________________    
void 
parse_tree::entity::write_shape(const std::string& sheet) const
{
  std::string fname(_name);
  fname.append(".shape");
  std::ofstream out(fname.c_str());
  if (!out) {
    std::cerr << "Failed to open file \"" << fname << "\"" << std::endl;
    return;
  }
  // Write out header 
  out << "<?xml version=\"1.0\"?>\n" 
      << "<shape xmlns=\"http://www.daa.com.au/~james/dia-shape-ns\"\n" 
      << "  xmlns:svg=\"http://www.w3.org/2000/svg\">\n"
      << "  <name>" << sheet << " - " << _name << "</name>\n"
      << "  <icon>" << _name <<  ".png</icon>\n"
      << "  <connections>" << std::endl;

  float  h       = height();
  float  w       = width();
  float  yleft   = _margin + _padding;
  float  yright  = _margin + _padding;
  for (port_list::const_iterator p = _ports.begin(); p != _ports.end(); ++p) {
    const port& pp = (*p);
    out << "    <point ";
    if (pp._type == port::in) {
      out << "x=\"0\" y=\"" << yleft << "\"/>";
      yleft += _dy;
    }
    else {
      out << "x=\"" << w << "\" y=\"" << yright << "\"/>";
      yright += _dy;
    }
    out << "<!-- " << pp._name << " -->" << std::endl;
  }
  out << "  </connections>\n"
      << "  <!-- Instance text -->\n"
      << "  <textbox x1=\"" << _margin << "\" y1=\"" << _margin / 4 
      << "\" x2=\"" << w - 2 * _margin << "\" y2=\"" << 3 * _margin / 4
      << "\" resize=\"no\" align=\"left\"/>\n"
      << "  <svg:svg width=\"" << w << "\" height=\"" << h << "\">\n"
      << "    <svg:g style=\"stroke-width:0.05; stroke:#000000; fill: none; "
      << "text-anchor:start; font-size: 0.8; font-family: monospace; "
      << "font-style: normal; font-weight: normal\">\n"
      << "      <!-- Bounding box -->\n"
      << "      <svg:rect style=\"stroke-pattern:dashed; "
      << "stroke-dashlength:0.5;\" x=\"0\" y=\"0\" width=\"" << w 
      << "\" height=\"" << h << "\"/>\n" 
      << "      <!-- Inner box -->\n" 
      << "      <svg:rect style=\"fill:#ffffff\" x=\"" << _margin 
      << "\" y=\"" << _margin << "\" width=\"" << w-2*_margin 
      << "\" height=\"" << h-2*_margin << "\"/>\n"
      << "      <!-- Entity label -->\n"
      << "      <svg:text x=\"" << _margin << "\" y=\"" << h - _margin /4 
      << "\">" << _name << "</svg:text>\n"
      << "      <!-- Ports -->" << std::endl;
  yleft       = _margin + _padding;
  yright      = _margin + _padding;
  for (port_list::const_iterator p = _ports.begin(); p != _ports.end(); ++p) {
    const port& pp = (*p);
    float x1, x2, xt, y1;
    std::string anchor;
    
    if (pp._type == port::in) {
      x1 = 0;
      x2 = _margin * 1.5;
      xt = x2;
      y1 = yleft;
      yleft += _dy;
    }
    else {
      x1 = w - _margin * 1.5;
      x2 = w;
      xt = x1;
      y1 = yright;
      anchor="style=\"text-anchor:end\" ";
      yright += _dy;
    }
    out << "      <svg:line x1=\"" << x1 << "\" x2=\"" << x2 << "\" y1=\"" 
	<< y1 << "\" y2=\"" << y1 << "\"/>\n" 
	<< "      <svg:text " << anchor << "x=\"" << xt << "\" y=\"" 
	<< y1 + _dy / 4 << "\">" << pp._name << "</svg:text>" << std::endl;
  }
  out << "    </svg:g>\n"
      << "  </svg:svg>\n"
      << "</shape>"
      << std::endl;
  out.close();
}

//____________________________________________________________________
void 
parse_tree::entity::write_icon(const std::string& sheet) const
{
  std::string fname(_name);
  fname.append("_icon.dia");
  std::ofstream out(fname.c_str());
  if (!out) {
    std::cerr << "Failed to open file \"" << fname << "\"" << std::endl;
    return;
  }
  out << "<?xml version=\"1.0\"?>\n" 
      << "<dia:diagram xmlns:dia=\"http://www.lysator.liu.se/~alla/dia/\">\n" 
      << "  <dia:layer name=\"Background\" visible=\"true\">"
      << std::endl;
  // Make a temporary instance, and output that
  instance i("&lt;&lt;instance&gt;&gt;");
  i._entity = const_cast<entity*>(this);
  i.write(out, sheet, 0, 2., 0., 0.);
  out << "  </dia:layer>\n"
      << "</dia:diagram>" << std::endl;
  out.close();
}

//____________________________________________________________________
void 
parse_tree::entity::write_diagram(const std::string& sheet) const
{
  std::string fname(_name);
  fname.append(".dia");
  std::ofstream out(fname.c_str());
  if (!out) {
    std::cerr << "Failed to open file \"" << fname << "\"" << std::endl;
    return;
  }
  // Write out header 
  out << "<?xml version=\"1.0\"?>\n" 
      << "<dia:diagram xmlns:dia=\"http://www.lysator.liu.se/~alla/dia/\">\n" 
      << "  <dia:layer name=\"Background\" visible=\"true\">"
      << std::endl;
  
  float s = 2;
  float h = s * _margin;
  float w = 0;
  for (instance_list::const_iterator d = _instances.begin(); 
       d != _instances.end(); ++d) {
    assert(d->_entity);
    h += s * d->_entity->height() + 2 * _margin;
    w =  std::max(s * d->_entity->width(), w);
  }
  size_t id     = 0;
  float  yleft  = s * _margin + s * 3 * _dy / 2;
  float  yright = s * _margin + s * 3 * _dy / 2;
  float  wport  = s * 8 * _dy;
  w += 2 * s * _margin + 2 * wport;
  // Add ports
  for (port_list::const_iterator p = _ports.begin(); p != _ports.end(); ++p) {
    const port& pp = (*p);
    float x1, y1;
    size_t a;
    std::string t;
    if (pp._type == port::in) {
      y1    = yleft;
      x1    =  0;
      y1    =  yleft;
      yleft += s * _dy;
      t     =  "in";
      a     = 0;
    }
    else {
      y1     = yright;
      x1     =  w - wport;
      yright += s * _dy;
      t      =  (pp._type == port::inout) ? "inout" : "out";
      a      = 2;
    }
    out << "    <dia:object type=\"" << sheet << " - " << t << "\" id=\"" 
	<< std::setfill('0') << std::setw(2) << id << std::setfill(' ') 
	<< "\">\n"
	<< "      <dia:attribute name=\"obj_pos\">\n" 
	<< "        <dia:point val=\"" << x1 << "," << y1 << "\"/>\n"
	<< "      </dia:attribute>\n"
	<< "      <dia:attribute name=\"elem_corner\">\n" 
	<< "        <dia:point val=\"" << x1 << "," << y1 << "\"/>\n"
	<< "      </dia:attribute>\n"
	<< "      <dia:attribute name=\"elem_width\">\n" 
	<< "        <dia:real val=\"" << wport << "\"/>\n"
	<< "      </dia:attribute>\n"
	<< "      <dia:attribute name=\"elem_height\">\n" 
	<< "        <dia:real val=\"" << s * _dy << "\"/>\n"
	<< "      </dia:attribute>\n"
	<< "      <dia:attribute name=\"text\">\n" 
	<< "        <dia:composite type=\"text\">\n" 
	<< "          <dia:attribute name=\"string\">\n" 
	<< "            <dia:string>#" << pp._name << "#</dia:string>\n"
	<< "          </dia:attribute>\n"
	<< "          <dia:attribute name=\"font\">"
        << "            <dia:font family=\"monospace\"/>\n"
	<< "          </dia:attribute>\n"
	<< "          <dia:attribute name=\"alignment\">"
        << "            <dia:enum val=\"" << a << "\"/>\n"
	<< "          </dia:attribute>\n"
	<< "          <dia:attribute name=\"height\">"
        << "            <dia:real val=\"" << s * _dy * .8 << "\"/>\n"
	<< "          </dia:attribute>\n"
	<< "        </dia:composite>\n"
	<< "      </dia:attribute>\n"
	<< "    </dia:object>" << std::endl;
    id++;
  }
  float x = wport + s * _margin;
  float y = s * _margin;
  for (instance_list::const_iterator d = _instances.begin(); 
       d != _instances.end(); ++d) {
    const instance& dd = (*d);
    y += dd.write(out, sheet, id, s, x, y);
    y += 2 * _margin;
    id++;
  }
  out << "  </dia:layer>\n"
      << "</dia:diagram>" << std::endl;
  out.close();
}


//____________________________________________________________________
//
// EOF
//
