#include "parse_tree/instance.hh"
#include "parse_tree/master.hh"
#include <iostream>
#include <iomanip>
#include <cassert>

//====================================================================
bool 
parse_tree::instance::parse(master& m) 
{
  size_t last = m._line._lvl;
  while (true) {
    if (m._in.eof()) break;
    line&  l    = m._line;
    if      (l._lvl < last) break;
    else if (l._type == ent) {
      if (!_entity) _entity = &(m.add(l._name));
      assert(_entity != 0);
      if (!m.parse()) return false;
      if (!_entity->parse(m)) return false;
    }
    else if (!m.parse()) return false;    
  }
  return true;
}

//____________________________________________________________________    
void 
parse_tree::instance::print(size_t& i) const
{
  for (size_t j = 0; j < i; j++) std::cout << ' ';
  std::cout << _name << " : " << std::flush;
  i++;
  if (!_entity) 
    std::cerr << "No entity!" << std::endl;
  else 
    _entity->print(i);
  i--;
}

//____________________________________________________________________    
float
parse_tree::instance::write(std::ostream& out, 
			    const std::string& sheet,
			    size_t id,
			    float  s,
			    float  x, 
			    float  y) const
{
  const entity&   ee = *_entity;
  out << "    <!-- Instance " << _name << " -->\n"
      << "    <dia:object type=\"" << sheet << " - " << ee._name 
      << "\" id=\"" << std::setfill('0') << std::setw(2) << id 
      << std::setfill(' ') << "\">\n"
      << "      <dia:attribute name=\"obj_pos\">\n" 
      << "        <dia:point val=\"" << x << "," << y << "\"/>\n"
      << "      </dia:attribute>\n"
      << "      <dia:attribute name=\"elem_corner\">\n" 
      << "        <dia:point val=\"" << x << "," << y << "\"/>\n"
      << "      </dia:attribute>\n"
      << "      <dia:attribute name=\"elem_width\">\n" 
      << "        <dia:real val=\"" << s * ee.width() << "\"/>\n"
      << "      </dia:attribute>\n"
      << "      <dia:attribute name=\"elem_height\">\n" 
      << "        <dia:real val=\"" << s * ee.height() << "\"/>\n"
      << "      </dia:attribute>\n"
      << "      <dia:attribute name=\"text\">\n" 
      << "        <dia:composite type=\"text\">\n" 
      << "          <dia:attribute name=\"string\">\n" 
      << "            <dia:string>#" << _name << "#</dia:string>\n"
      << "          </dia:attribute>\n"
      << "          <dia:attribute name=\"font\">"
      << "            <dia:font family=\"sans\"/>\n"
      << "          </dia:attribute>\n"
      << "        </dia:composite>\n"
      << "      </dia:attribute>\n"
      << "    </dia:object>" << std::endl;
  return s * ee.height();
}

  

//____________________________________________________________________    
//
// EOF
//
