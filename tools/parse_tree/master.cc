#include "parse_tree/master.hh"
#include "parse_tree/entity.hh"
#include "parse_tree/instance.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdarg>

//====================================================================
parse_tree::master::master(std::istream& in) 
  : _in(in), 
    _top("top")
{ 
  _line._type = unk; 
  _line._line = 0; 
}  

//____________________________________________________________________
parse_tree::entity& 
parse_tree::master::add(const std::string& n) 
{
  entity_map::iterator i = _entities.find(n);
  if (i != _entities.end()) return i->second;
  entity_map::iterator j = _entities.insert(i,std::make_pair(n,entity(n)));
  return j->second;
}

//____________________________________________________________________
bool 
parse_tree::master::error(const char* format, ...) 
{
  va_list ap;
  va_start(ap, format);
  static char buf[1024];
  vsnprintf(buf, 1024, format, ap);
  va_end(ap);
  std::cerr << buf << std::endl;
  return false;
}

//____________________________________________________________________
bool 
parse_tree::master::read()
{
  if (!parse()) return error("Very first parse failed!");
  if (!_top.parse(*this)) return error("top instance parse failed");
  return true;
}

//____________________________________________________________________
void
parse_tree::master::print()
{
  size_t lvl = 0;
  _top.print(lvl);
}

//____________________________________________________________________
bool 
parse_tree::master::parse()
{
  // Read indention
  size_t h = 0;
  while (true) {
    char p = _in.peek();
    if (p != ' ' && p != '\t' && p != '|') break;
    h++;
    _in.get();
  }
  _line._lvl = h;
  // Read leader
  char c = '\0', m;
  if (_line._lvl != 0) {
    _in >> c >> m;
    if (_in.bad()) 
      return error("Bad read on line %d while reading '[`+]-'",_line._line);
    if (c != '`' && c != '+') 
      return error("Expected '`' or '+' but got '%c' on line %d",c,
		     _line._line);
    if (m != '-') 
      return error("Expected '-' but got '%c' on line %d",m,_line._line);
  }
    
  // Read name 
  _in >> _line._name;
  if (_in.bad()) 
    return error("Bad read on line %d while reading name",_line._line);
  if (_in.eof()) return true;
  char l;
  
  // Read type 
  std::string t;
  _in >> l >> t;
  if (_in.bad()) 
    return error("Bad read on line %d while reading type", _line._line);
  if (l != '[')
    return error("Expected a '[' bug got '%c' on line %d",l, _line._line);
  if (t[t.size()-1] == ']') t.erase(t.size()-1);
  
  if       (t == "entity")        _line._type = ent;
  else if  (t == "arch")          _line._type = arc;
  else if  (t == "signal")        _line._type = sig;
  else if  (t == "process")       _line._type = prc;
  else if  (t == "block")         _line._type = blk;
  else if  (t == "instance")      _line._type = ins;
  else if  (t == "for-generate")  _line._type = gen;
  else if  (t == "package")       _line._type = pkg;
  else if  (t == "port") {
    std::string dir;
    _in >> dir;
    if (_in.bad()) 
      return error("Bad read on line %d while reading direction",_line._line);
    if (dir[dir.size()-1] == ']') dir.erase(dir.size()-1);
    if      (dir == "in")         _line._type = inp;
    else if (dir == "out")        _line._type = oup;
    else if (dir == "inout")      _line._type = iop;
    else 
      return error("Invalid direction '%s' for port '%s' on line %d",
		   dir.c_str(), _line._name.c_str(), _line._line);
  }
  else 
    return error("Unknown type '%s' on line %d", t.c_str(), _line._line);
  
    
  // Get the rest of the line 
  std::string remain;
  std::getline(_in, remain);
  _line._line++;
  return true;
}

//____________________________________________________________________
void 
parse_tree::master::write_port(const std::string& sheet,
			       const std::string& name) const
{
  std::string fname(name);
  fname.append(".shape");
  std::ofstream out(fname.c_str());
  if (!out) {
    std::cerr << "Failed to open file \"" << fname << "\"" << std::endl;
    return;
  }
  float       h  = entity::_dy;
  float       w  = 8 * h;
  float       dx = w / 20;
  float       xc, xm, xt1, xt2;
  std::string a;
  std::stringstream sp;
  if (name == "in") {
    xc  = w;
    xm  = 0;
    xt1 = dx;
    xt2 = w / 2 - dx;
    a   = "left";
    sp << "0,0 " << w/2-dx << ",0 " << w/2 << "," << h/2 << " " 
       << w/2-dx << "," << h << " 0," << h;
  }
  else {
    xc = 0;
    xm = w;
    xt1 = w / 2 + dx;
    xt2 = w - dx;
    a  = "right";
    if (name == "out")
      sp << w/2 << "," <<  h/2 << " " << xt1 << ",0 " << w << ",0 " 
	 << w << "," << h << " " << xt1 << "," << h;
    else
      sp << w/2 << "," << h/2 << " " << xt1 << ",0 " << xt2 << ",0 " 
	 << w << "," << h/2 << " " << xt2 << "," << h << " " 
	 << xt1 << "," << h;
  }
  std::string p(sp.str());
  
  // Write out header 
  out << "<?xml version=\"1.0\"?>\n" 
      << "<shape xmlns=\"http://www.daa.com.au/~james/dia-shape-ns\"\n" 
      << "  xmlns:svg=\"http://www.w3.org/2000/svg\">\n"
      << "  <name>" << sheet << " - " << name << "</name>\n"
      << "  <connections>\n" 
      << "    <point x=\"" << xc << "\" y=\"" << h / 2 << "\"/>\n"
      << "  </connections>\n"
      << "  <aspectratio type=\"fixed\"/>\n"
      << "  <textbox x1=\"" << xt1 << "\" x2=\"" << xt2 
      << "\" y1=\"" << h / 8 << "\" y2=\"" << 7 * h / 8 << "\" align=\""
      << a << "\" resize=\"no\"/>\n" 
      << "  <svg:svg width=\"" << w << "\" height=\"" << h << "\">\n"
      << "    <svg:g style=\"stroke-width:0.05; stroke:#000000; "
      << "fill:#ffffff; text-anchor:end; font-size:0.7 font-family:monospace;"
      << "font-weight:normal\">\n"
      << "      <svg:line x1=\"" << xc << "\" x2=\"" << w/2 << "\" y1=\"" 
      << h / 2 << "\" y2=\"" << h / 2 << "\"/>\n"
      << "      <svg:polygon points=\"" << p << "\"/>\n"
      << "    </svg:g>\n"
      << "  </svg:svg>\n"
      << "</shape>" << std::endl;
  out.close();
}

//____________________________________________________________________
void 
parse_tree::master::write(const std::string& sheet) const
{
  std::string fname(sheet);
  fname.append(".sheet");
  std::ofstream out(fname.c_str());
  if (!out) {
    std::cerr << "Failed to open file \"" << fname << "\"" << std::endl;
    return;
  }
  // Write out header 
  out << "<?xml version=\"1.0\"?>\n" 
      << "<sheet xmlns=\"http://www.lysator.liu.se/~alla/dia/dia-sheet-ns\">\n"
      << "  <name>" << sheet << "</name>\n"
      << "  <description>Entities in " << sheet << "</description>\n"
      << "  <contents>" << std::endl;
  std::string pn[] = { "in", "out", "inout" };
  for (size_t ip = 0; ip < 3; ip++) {
    out << "    <object name=\"" << sheet << " - " << pn[ip] << "\">\n"
	<< "      <description>" << pn[ip] << " port</description>\n"
	<< "    </object>" << std::endl;
    write_port(sheet, pn[ip]);
  }
  for (entity_map::const_iterator e = _entities.begin(); 
       e != _entities.end();++e) {
    const entity& ee = (e->second);
    out << "    <object name=\"" << sheet << " - " << ee._name << "\">\n"
	<< "      <description>Entity " << ee._name << "</description>\n"
	<< "    </object>" << std::endl;
    ee.write(sheet);
  }
  out << "  </contents>\n"
      << "</sheet>" << std::endl;
  
  out.close();
}

//____________________________________________________________________
//
// EOF
//
