#include "parse_tree/line.hh"
#include <iostream>
#include <iomanip>

//____________________________________________________________________
void 
parse_tree::line::print() const 
{
  std::cout << "Line # " << std::setw(3) << _line << " @ "
	    << std::setw(2) << _lvl  << ": ";
  for (size_t i = 0; i < _lvl; i++) std::cout << ' ';
  switch (_type) {
  case ent: std::cout << "ent"; break;
  case arc: std::cout << "arc"; break; 
  case blk: std::cout << "blk"; break; 
  case prc: std::cout << "prc"; break;
  case sig: std::cout << "sig"; break;
  case ins: std::cout << "ins"; break;
  case gen: std::cout << "gen"; break;
  case inp: std::cout << "inp"; break;
  case oup: std::cout << "oup"; break;
  case iop: std::cout << "iop"; break;
  case pkg: std::cout << "pkg"; break;
  default:  std::cout << "???"; break;
  }
  std::cout << " " << _name << std::endl;
}

//____________________________________________________________________
//
// EOF
//
