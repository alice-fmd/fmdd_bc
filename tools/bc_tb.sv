# SimVision Command Script (Sat Sep 30 04:07:41 CEST 2006)

#
# groups
#

if {[group find -match exact -name DAC] == {}} {
    group new -name DAC -overlay 0
} else {
    group using DAC
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::dac_data_i

if {[group find -match exact -name DACs] == {}} {
    group new -name DACs -overlay 0
} else {
    group using DACs
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::dac_data_i \
    simulator:::dac_addr_i

if {[group find -match exact -name VA1] == {}} {
    group new -name VA1 -overlay 0
} else {
    group using VA1
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::test_on_i \
    simulator:::shift_in_i \
    simulator:::shift_clk_i \
    simulator:::sample_clk_i \
    simulator:::pulser_enable_i \
    simulator:::hold_i \
    simulator:::digital_reset_i

if {[group find -match exact -name Bus] == {}} {
    group new -name Bus -overlay 0
} else {
    group using Bus
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::writeb_i \
    simulator:::trsfb_i \
    simulator:::rstb_i \
    simulator:::sclk_i \
    simulator:::errorb_i \
    simulator:::dstbb_i \
    simulator:::cstbb_i \
    simulator:::clk_i \
    simulator:::bd_i \
    simulator:::acknb_i

if {[group find -match exact -name {Test mode}] == {}} {
    group new -name {Test mode} -overlay 0
} else {
    group using {Test mode}
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::test_c_i \
    simulator:::test_b_i \
    simulator:::test_a_i \
    simulator:::rdoclk_en_i \
    simulator:::adcclk_en_i \
    simulator:::adc_add1_i \
    simulator:::adc_add0_i

if {[group find -match exact -name Triggers] == {}} {
    group new -name Triggers -overlay 0
} else {
    group using Triggers
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::l2b_i \
    simulator:::l1b_i \
    simulator:::l0_i \
    simulator:::busy_i \
    simulator:::altro_l2_i \
    simulator:::altro_l1_i

if {[group find -match exact -name I2C] == {}} {
    group new -name I2C -overlay 0
} else {
    group using I2C
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::sda_out_i \
    simulator:::sda_in_i \
    simulator:::scl_i

if {[group find -match exact -name ALTRO] == {}} {
    group new -name ALTRO -overlay 0
} else {
    group using ALTRO
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::pasa_sw_i \
    simulator:::paps_errorb_i \
    {simulator:::bcout_ad_i[4]} \
    {simulator:::bcout_ad_i[3]} \
    {simulator:::bcout_ad_i[2]} \
    {simulator:::bcout_ad_i[1]} \
    {simulator:::bcout_ad_i[0]} \
    simulator:::altro_sw_i \
    simulator:::alps_errorb_i \
    simulator:::al_trsf_enb_i \
    simulator:::al_dolo_enb_i

if {[group find -match exact -name GTL] == {}} {
    group new -name GTL -overlay 0
} else {
    group using GTL
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::oeba_l_i \
    simulator:::oeba_h_i \
    simulator:::oeab_l_i \
    simulator:::oeab_h_i \
    simulator:::ctr_out_i \
    simulator:::ctr_in_i

if {[group find -match exact -name Monitor] == {}} {
    group new -name Monitor -overlay 0
} else {
    group using Monitor
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::msda_i \
    simulator:::mscl_i \
    simulator:::mcst_i

if {[group find -match exact -name Misc] == {}} {
    group new -name Misc -overlay 0
} else {
    group using Misc
    group set -overlay 0
    group clear 0 end
}
group insert \
    simulator:::rst_fbc_i \
    simulator:::hadd_i \
    simulator:::HADD \
    simulator:::debug_i \
    simulator:::bc_int_i

#
# preferences
#
preferences set ams-show-flow 1
preferences set ams-show-potential 1
preferences set analog-height 5
preferences set color-verilog-by-value 1
preferences set create-cursor-for-new-window 0
preferences set cv-num-lines 25
preferences set cv-show-only 1
preferences set db-scope-gen-compnames 0
preferences set db-scope-gen-icons 1
preferences set db-scope-gen-sort name
preferences set db-scope-gen-tracksb 0
preferences set db-scope-systemc-processes 1
preferences set db-scope-verilog-cells 1
preferences set db-scope-verilog-functions 1
preferences set db-scope-verilog-namedbegins 1
preferences set db-scope-verilog-namedforks 1
preferences set db-scope-verilog-tasks 1
preferences set db-scope-vhdl-assertions 1
preferences set db-scope-vhdl-assignments 1
preferences set db-scope-vhdl-blocks 1
preferences set db-scope-vhdl-breakstatements 1
preferences set db-scope-vhdl-calls 1
preferences set db-scope-vhdl-generates 1
preferences set db-scope-vhdl-processstatements 1
preferences set db-scope-vhdl-unnamedprocesses 1
preferences set db-show-editbuf 0
preferences set db-show-modnames 0
preferences set db-show-values simulator
preferences set db-signal-filter-constants 1
preferences set db-signal-filter-generics 1
preferences set db-signal-filter-other 1
preferences set db-signal-filter-quantities 1
preferences set db-signal-filter-signals 1
preferences set db-signal-filter-terminals 1
preferences set db-signal-filter-variables 1
preferences set db-signal-gen-radix default
preferences set db-signal-gen-showdetail 0
preferences set db-signal-gen-showstrength 0
preferences set db-signal-gen-sort name
preferences set db-signal-show-assertions 1
preferences set db-signal-show-errorsignals 1
preferences set db-signal-show-fibers 1
preferences set db-signal-show-inouts 1
preferences set db-signal-show-inputs 1
preferences set db-signal-show-internal 1
preferences set db-signal-show-live 1
preferences set db-signal-show-mutexes 1
preferences set db-signal-show-outputs 1
preferences set db-signal-show-semaphores 1
preferences set db-signal-vlogfilter-branches 1
preferences set db-signal-vlogfilter-memories 1
preferences set db-signal-vlogfilter-parameters 1
preferences set db-signal-vlogfilter-registers 1
preferences set db-signal-vlogfilter-variables 1
preferences set db-signal-vlogfilter-wires 1
preferences set default-ams-formatting potential
preferences set default-time-units ar
preferences set delete-unused-cursors-on-exit 1
preferences set delete-unused-groups-on-exit 1
preferences set enable-toolnet 0
preferences set initial-zoom-out-full 0
preferences set key-bindings {
	Edit>Undo "Ctrl+Z"
	Edit>Redo "Ctrl+Y"
	Edit>Copy "Ctrl+C"
	Edit>Cut "Ctrl+X"
	Edit>Paste "Ctrl+V"
	Edit>Delete "Del"
        Select>All "Ctrl+A"
        Edit>Select>All "Ctrl+A"
        Edit>SelectAll "Ctrl+A"
      	openDB "Ctrl+O"
        Simulation>Run "F2"
        Simulation>Next "F6"
        Simulation>Step "F5"
        #Schematic window
        View>Zoom>Fit "Alt+="
        View>Zoom>In "Alt+I"
        View>Zoom>Out "Alt+O"
        #Waveform Window
	View>Zoom>InX "Alt+I"
	View>Zoom>OutX "Alt+O"
	View>Zoom>FullX "Alt+="
	View>Zoom>InX_widget "I"
	View>Zoom>OutX_widget "O"
	View>Zoom>FullX_widget "="
	View>Zoom>FullY_widget "Y"
	View>Zoom>Cursor-Baseline "Alt+Z"
	View>Center "Alt+C"
	View>ExpandSequenceTime>AtCursor "Alt+X"
	View>CollapseSequenceTime>AtCursor "Alt+S"
	Edit>Create>Group "Ctrl+G"
	Edit>Ungroup "Ctrl+Shift+G"
	Edit>Create>Marker "Ctrl+M"
	Edit>Create>Condition "Ctrl+E"
	Edit>Create>Bus "Ctrl+W"
	Explore>NextEdge "Ctrl+]"
	Explore>PreviousEdge "Ctrl+["
	ScrollRight "Right arrow"
	ScrollLeft "Left arrow"
	ScrollUp "Up arrow"
	ScrollDown "Down arrow"
	PageUp "PageUp"
	PageDown "PageDown"
	TopOfPage "Home"
	BottomOfPage "End"
}
preferences set marching-waveform 1
preferences set prompt-exit 1
preferences set prompt-on-reinvoke 1
preferences set register-print-colors {Black on white}
preferences set register-print-fittopage 0
preferences set register-print-orientation Portrait
preferences set register-print-paper {Letter (8.5 x 11)}
preferences set respond-to-simvision-command 1
preferences set restore-state-on-startup 0
preferences set save-state-on-startup 0
preferences set sb-double-click-command @goto-definition
preferences set sb-editor-command {xterm -e vi +%L %F}
preferences set sb-history-size 10
preferences set sb-radix default
preferences set sb-show-strength 1
preferences set sb-syntax-highlight 1
preferences set sb-syntax-types {
    {-name "VHDL/VHDL-AMS" -cleanname "vhdl" -extensions {.vhd .vhdl}}
    {-name "Verilog/Verilog-AMS" -cleanname "verilog" -extensions {.v .vams .vms .va}}
    {-name "C" -cleanname "c" -extensions {.c}}
    {-name "C++" -cleanname "c++" -extensions {.h .hpp .cc .cpp .CC}}
    {-name "SystemC" -cleanname "systemc" -extensions {.h .hpp .cc .cpp .CC}}
}
preferences set sb-tab-size 8
preferences set schematic-show-values simulator
preferences set search-toolbar 1
preferences set seq-time-width 30
preferences set sfb-colors {
    register #beded1
    variable #beded1
    assignStmt gray85
    force #faa385
}
preferences set sfb-default-tree 0
preferences set sfb-max-cell-width 40
preferences set sfb-print-colors {Black on white}
preferences set sfb-print-fittopage false
preferences set sfb-print-margin-x 1i
preferences set sfb-print-margin-y 1i
preferences set sfb-print-orientation Portrait
preferences set sfb-print-paper {Letter (8.5 x 11)}
preferences set show-database-names 0
preferences set show-full-signal-names 0
preferences set show-strength 0
preferences set show-times-on-cursors 1
preferences set show-times-on-markers 1
preferences set signal-type-colors {
	group #0000FF
	overlay #0000FF
	input #FFFF00
	output #FFA500
	inout #00FFFF
	internal #00FF00
	fiber #FF99FF
	errorsignal #FF0000
	assertion #FF0000
	unknown #FFFFFF
}
preferences set snap-to-edge 1
preferences set toolbars-style icon
preferences set transaction-height 3
preferences set txe-locate-add-fibers yes
preferences set txe-locate-create-waveform sometimes
preferences set txe-locate-pop-waveform yes
preferences set txe-locate-scroll-x yes
preferences set txe-locate-scroll-y yes
preferences set txe-man-doubleclick-search edit
preferences set txe-navigate-search-locate no
preferences set txe-navigate-waveform-locate yes
preferences set txe-navigate-waveform-next-child no
preferences set txe-search-default-form built_in.basic
preferences set txe-search-result-limit 200
preferences set txe-search-reuse-window never
preferences set txe-search-show-linenumbers yes
preferences set txe-search-style form
preferences set txe-view-hold off
preferences set undo-add-signal 0
preferences set use-signal-type-colors 0
preferences set use-signal-type-icons 1
preferences set verilog-colors {
	HiZ #ff9900
	StrX #ff0000
	Sm #00ff99
	Me #0000ff
	We #00ffff
	La #ff00ff
	Pu #9900ff
	St ""
	Su #ff0099
	0 ""
	1 ""
	X #ff0000
	Z #ff9900
	other #ffff00
}
preferences set vhdl-colors {
	U #9900ff 
	X #ff0000 
	0 ""
	1 ""
	Z #ff9900 
	W #ff0000
	L #00ffff 
	H #00ffff
	- ""
}
preferences set waveform-banding 1
preferences set waveform-height 10
preferences set waveform-print-colors {Black on white}
preferences set waveform-print-orientation Landscape
preferences set waveform-print-paper {Letter (8.5 x 11)}
preferences set waveform-print-time visible
preferences set waveform-print-variables visible
preferences set waveform-space 2
