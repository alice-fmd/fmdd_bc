#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>

struct intel_hex 
{
  struct record 
  {
    typedef std::vector<unsigned char> byte_list;
    size_t _start_address;
    byte_list _bytes;
    void print(std::ostream& o) const
    {
      o << std::setw(8) << _start_address << ": "
	<< std::setfill('0') << std::hex;
      for (byte_list::const_iterator i = _bytes.begin();
	   i != _bytes.end(); ++i) 
	o << " 0x" << std::setw(2)<< unsigned(*i);
      unsigned v = value();
      o << " -> 0x" << std::setw(4) << v 
	<< std::dec << std::setfill(' ') << " = " 
	<< std::setw(10) << v << std::endl;
    }
    unsigned value() const 
    {
      unsigned v = 0;
      unsigned j = 0;
      for (byte_list::const_iterator i = _bytes.begin();
	   i != _bytes.end(); ++i, ++j) 
	v = (v << j * 8) + unsigned(*i);
      return v;
    }
  };
  typedef std::vector<record> record_list;

  
  intel_hex(std::istream& in)
    : _in(in), 
      _eof(false),
      _offset(0)
  {}
  
  bool read_file()
  {
    _offset = 0;
    _eof    = false;
    _line   = 0;
    
    while (true) {
      std::string s;
      std::getline(_in, s);
      if (s[s.size()-1] == '\r') s.erase(s.size()-1);
      if (s.empty()) break;      
      if (!decode_record(s)) return false;
      if (_eof) break;
      _line++;
    }
    return true;
  }
  void error(std::ostream& s) 
  {
    std::stringstream* e = dynamic_cast<std::stringstream*>(&s);
    if (!e) throw std::runtime_error("Bad ostream");
    std::stringstream st;
    st << "Error (Line # " << std::setw(4) << _line << "): " << e->str();
    throw std::runtime_error(st.str().c_str());
  }
  void warning(std::ostream& s) 
  {
    std::stringstream st;
    std::cerr << "Warning (Line # " << std::setw(4) << _line << "): " 
	      << s << std::endl;
  }
  bool decode_record(const std::string& s) 
  {
    std::stringstream e;
    size_t l = s.size();
    if (l == 0) return true;
    if (s[0] != ':') return true;    
    if (l < 11) error((e << "\" length (" << l << ") is too short"));

    unsigned rlen = to_unsigned(s.substr(1,2));
    unsigned addr = to_unsigned(s.substr(3,4));    
    unsigned type = to_unsigned(s.substr(7,2));

    if (l != (11 + rlen * 2)) 
      error(e << "length (" << l << ") is bad (expected " << 11+rlen*2 << ")");

    unsigned char crc  = 0;
    for (size_t i = 0; i <= rlen + 4; i++) {
      unsigned t = to_unsigned(s.substr(2 * i + 1, 2)); 
      crc += t;
    }
    if (crc != 0) error(e << "check sum (" << crc << ") is invalid");

    switch (type) {
    case 0: // Data record
      if (_records.size() == 0 || 
	  _records.back()._start_address + _records.back()._bytes.size() 
	  != _offset + addr) {
	_records.push_back(record());
	_records.back()._start_address = _offset + addr;
      }
      {
	unsigned char t = 0;
	for (size_t i = 0; i < rlen; ++i) {
	  unsigned t = to_unsigned(s.substr(9 + i * 2,2));
	  _records.back()._bytes.push_back(t);
	}
      }
      break;
    case 1: // End-of-file record
      if (rlen != 0) 
	error(e << "End-of-file length (" << rlen << ") is non-zero");
      if (addr != 0) 
	error(e << "End-of-file address (" << addr << ") is non-zero");
      _eof = true;
      break;
    case 2: // Extended segment address record 
      if (rlen != 2) 
	error(e << "Extended seqment address length ("<< rlen << ") is not 2");
      if (addr != 0) 
	error(e << "Extended seqment address offset ("<<addr<<") is non-zero");
      addr = to_unsigned(s.substr(9, 4));
      _offset = addr << 4;
      break;
    case 3: // Start segment address record 
      if (rlen != 4) 
	warning(e << "Start seqment address length ("<<rlen<<") is not 4");
      if (addr != 0)
	warning(e << "Start seqment address offset ("<<addr<<") is non-zero");
      if (rlen == 4) {
	unsigned ssa = to_unsigned(s.substr(9, 8));
	std::cerr << "Start segment address (CS/IP): 0x" 
		  << std::hex << ssa  << std::dec << std::endl;
      }
      break;
    case 4: // Extended linear address record 
      if (rlen != 2) 
	error(e << "Extended linear address length (" <<rlen<<") is not 2");
      if (addr != 0) 
	error(e << "Extended linear address offset (" <<addr<<") is non-zero");
      addr = to_unsigned(s.substr(9, 4));
      _offset = ((unsigned long)(addr)) << 16;
      break;
    case 5: // Start linear address record 
      if (rlen != 4) 
	warning(e << "Start seqment address length (" <<rlen<<") is not 4");
      if (addr != 0) 
	warning(e << "Start seqment address offset (" <<addr<<") is non-zero");
      if (rlen == 4) {
	unsigned lsa = to_unsigned(s.substr(9, 8));
	std::cerr << "Linear start address (CS/IP): 0x" 
		  << std::hex << lsa  << std::dec << std::endl;
      }
      break;
    default:
      error(e << "Unknown type of record (" << type << ") found");
    }
    return true;
  }
  unsigned to_unsigned(const std::string& s)
  {
    unsigned ret = 0;
    std::stringstream str(s);
    str >> std::hex  >> ret;
    return ret;
  }
  void print(std::ostream& o) const 
  {
    o << "Got " << _records.size() << " records: " << std::endl;
    for (record_list::const_iterator i = _records.begin(); 
	 i != _records.end(); ++i) 
      i->print(o);
  }
  void as_vector(std::vector<unsigned>& v) 
  {
    v.resize(_records.size());
    for (record_list::const_iterator i = _records.begin(); 
	 i != _records.end(); ++i) 
      v[i->_start_address] = i->value();
  }
  std::vector<record> _records;
  size_t _line;
  bool   _eof;
  size_t _offset;
  std::istream& _in;
};


std::ostream&
as_bits(std::ostream& o, unsigned v, size_t i, size_t n)
{
  for (size_t j = i + n; j > i; j--) 
    o << ((v & (1 << (j - 1))) != 0 ? '1' : '0');
  return o;
}

std::ostream&
printit(std::ostream& o, unsigned i, unsigned v, char t) 
{
  unsigned mode    = ((v >> 8) & 0x7);
  unsigned data    = ((v >> 0) & 0xff);
  unsigned vendor  = ((v >> 4) & 0xf);
  unsigned channel = ((v >> 5) & 0x7);
  o << "  " << std::setw(3) << i << " => (";
  switch (mode) {
  case 6: // 110 - start  & don't-care
    o << "\"110\" & \"00000000\")" << t << "          -- Start";
    break;
  case 1: // 001 - stop & don't-care
    o << "\"001\" & \"00000000\")" << t << "          -- Stop";
    break;
  case 2: // 010 - write & vendor & adc & write flag
    o << "\"010\" & \"";
    if ((data >> 3) == 0) { // address pointer
      as_bits(o, data, 3, 5);
      o << "\" & \"";
      as_bits(o, data, 0, 3);
      o << "\")" << t << "     -- write -> ";
      switch (data & 0x7) {
      case 0: o << "T";      break;
      case 1: o << "cfg1";   break;
      case 2: o << "T_hyst"; break;
      case 3: o << "T_oti";  break;
      case 4: o << "ADC";    break;
      case 5: o << "cfg2";   break;
      default: o << "n/a";   break;
      }
    }
    else if (vendor == 5) {
      as_bits(o, v, 4, 4); // Vendor
      o << "\" & \""; 
      as_bits(o, v, 1, 3); // ADC
      o << "\" & \""; 
      as_bits(o, v, 0, 1); // Write flag
      o << ")" << t << " -- Write & vendor & adc & we";
    }
    else {
      as_bits(o, v, 5, 3); // ADC channel
      o << "\" & \"";
      as_bits(o, v, 0, 5); // Flags 
      o << "\")" << t << "     -- Cfg ";
      switch (data >> 5) {
      case 0: o << "T"; break;
      default: o << "ADC" << (data >> 5); break;
      }
    }
    break;
  case 3: // 011 - read & don't-care 
    o << "\"011\" & \"00000000\")" << t << "          -- Read";
    break;
  case 5: // 101 - end of block 
    o << "\"101\" & \"00000000\")" << t << "          -- End-of-block";
    break;
  default: 
    o << "\"";
    as_bits(o, v, 0, 11);
    o << "\") " << t << " -- Unknown";
    break;
  }
  return o;
}

int main(int argc, char** argv)
{
  try {
    intel_hex i(std::cin);
    i.read_file();
    // i.print(std::cout);
    std::vector<unsigned> v;
    i.as_vector(v);
    std::cout << "subtype data_t is std_logic_vector(10 downto 0);\n"
	      << "constant data_i : data_t(0 up " 
	      << v.size() - 1 << ") := (" << std::endl; 
    for (size_t j = 0; j < v.size(); j++) {
      printit(std::cout, j, v[j], ',');
      std::cout <<  std::endl;
    }
    std::cout << "  others => \"00000000000\");" << std::endl;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}
