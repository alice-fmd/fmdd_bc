------------------------------------------------------------------------------
-- Title      : Package declaration of bc_pack - for Quartus simulation only
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : bc_pack.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2006/10/11
-- Platform   : ACEX1K - EP1K100QC208-1
------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
--              This code takes care of
--              * monitoring voltages, currents, and temperatures.
--              * handle L0 triggers, and sends the appropriate control
--                signals to the VA1 pre-amplifier chips on the FMD hybrid
--                cards.
--              * controls the GTL bus transceivers for the ALTRO's on the
--                card, and of course for it self.
--
--              The code is based on the TPC FEC BC code. The original 
--              authors were
--              * Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> 
--              * Roberto Campagnolo <Roberto.Campagnolo@cern.ch>.
--
--              Some initial development, and a lot of guidance was given by 
--              * Daniel Kirschner <daniel.kirschner@nbi.dk>
--              * Tiago Perez <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/30  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package bc_pack is
  component bc
    port (
      clk           : in    std_logic;  -- Clock
      rstb          : in    std_logic;  -- Async reset
      sclk          : in    std_logic;  -- Sample clock (10 MHz)
      scl           : in    std_logic;  -- I2C: Serial clock (<= 5MHz)
      sda_in        : in    std_logic;  -- I2C: data input
      sda_out       : out   std_logic;  -- I2C: data out
      l0            : in    std_logic;  -- Level 0 trigger 
      l1b           : in    std_logic;  -- Level 1 trigger
      l2b           : in    std_logic;  -- Level 2 trigger
      mscl          : out   std_logic;  -- ADC: clock
      msda          : inout std_logic;  -- ADC: data in/out
      pasa_sw       : out   std_logic;  -- PASA switch
      paps_errorb   : in    std_logic;  -- PASA power supply error
      rdoclk_en     : out   std_logic;  -- TSM: Readout clock enable
      adcclk_en     : out   std_logic;  -- TSM: ADC clock enable
      adc_add0      : out   std_logic;  -- ADC address bit 0
      adc_add1      : out   std_logic;  -- ADC address bit 1
      test_a        : out   std_logic;  -- TSM: mask a
      test_b        : out   std_logic;  -- TSM: mask b
      test_c        : out   std_logic;  -- TSM: mask c
      rst_fbc       : out   std_logic;  -- Reset front-end card
      hadd          : in    std_logic_vector(4 downto 0);  -- Card address
      bcout_ad      : out   std_logic_vector(4 downto 0);  -- Out board address
      oeba_l        : out   std_logic;  -- GTL: enable in low bits
      oeab_l        : out   std_logic;  -- GTL: enable out low bits
      oeba_h        : out   std_logic;  -- GTL: enable in high bits
      oeab_h        : out   std_logic;  -- GTL: enable out high bits
      ctr_in        : out   std_logic;  -- GTL: enable ctrl bus in
      ctr_out       : out   std_logic;  -- GTL: enable ctrl bus out
      bd            : inout std_logic_vector(39 downto 0);  -- Bus: 40 bits
      acknb         : inout std_logic;  -- CTL: Acknowledge
      errorb        : inout std_logic;  -- CTL: Error
      trsfb         : inout std_logic;  -- CTL: Transfer
      dstbb         : inout std_logic;  -- CTL: Data strobe
      writeb        : inout std_logic;  -- CTL: Write 
      cstbb         : inout std_logic;  -- Control strobe
      bc_int        : out   std_logic;  -- CTL: Interrupt
      otib          : in    std_logic;  -- Not used
      mcst          : out   std_logic;  -- ADC : Montor start disabled='0';
      clk10         : in    std_logic;  -- Not used
      debug_on      : in    std_logic;  -- Not used (should be high)
      altro_sw      : out   std_logic;  -- ALTRO switch
      sample_clk    : out   std_logic_vector(2 downto 0);  -- Sample clock 
      altro_l1      : out   std_logic;  -- L1 for ALTROs (active low)
      altro_l2      : out   std_logic;  -- L2 for ALTROs (active low)
      alps_errorb   : in    std_logic;  -- ALTRO power supply error
      al_dolo_enb   : in    std_logic;  -- ALTRO data out enable
      al_trsf_enb   : in    std_logic;  -- ALTRO transfer enable
      al_ackn_enb   : in    std_logic;  -- ALTRO ackn enable (Not used)
      digital_reset : out   std_logic;  -- Reset VA1's
      test_on       : out   std_logic;  -- Calibration mode on.
      pulser_enable : out   std_logic;  -- Enable step on pulser
      shift_in      : out   std_logic_vector(2 downto 0);  -- shift reset
      shift_clk     : out   std_logic_vector(2 downto 0);  -- shift register
      hold          : out   std_logic_vector(2 downto 0);  -- Hold charge
      dac_addr      : out   std_logic_vector (11 downto 0);  -- DAC address
      dac_data      : out   std_logic_vector (7 downto 0);  -- DAC data
      busy          : out   std_logic;  -- busy (LED on test)
      debug         : out   std_logic_vector(14 downto 0));  -- Debug pins
  end component bc;
end package bc_pack;
------------------------------------------------------------------------------
-- 
-- EOF
--
