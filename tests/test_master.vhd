-- ===========================================================================
library ieee;
use ieee.std_logic_1164.all;

entity clock_gen is
  generic (
    DIV : natural);                     -- Scale
  port (
    clk  : in  std_logic;               -- Master
    rstb : in  std_logic;               -- reset
    oclk : out std_logic);              -- Output clock
end clock_gen;

-------------------------------------------------------------------------------
architecture behaviour of clock_gen is
  signal cnt_i : integer := 0;          -- Counter
begin  -- behaviour
  -- purpose: Generate clock
  -- type   : sequential
  -- inputs : clk, rstb
  -- outputs: oclk
  gen : process (clk, rstb)
  begin  -- process gen
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i <= DIV;
      oclk  <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      cnt_i <= cnt_i - 1;
      if cnt_i > DIV/2 then
        oclk <= '1';
      else
        oclk <= '0';
      end if;
      if cnt_i = 1 then
        cnt_i <= DIV;
      end if;
    end if;
  end process gen;
end behaviour;

-- ============================================================================
library ieee;
use ieee.std_logic_1164.all;

entity shift_reg is
  generic (
    WIDTH : natural := 8);
  port (
    clk     : in  std_logic;                           -- Clock
    rstb    : in  std_logic;                           -- Reset
    enable  : in  std_logic;                           -- Shift one bit
    clear   : in  std_logic;                           -- Clear content
    load    : in  std_logic;                           -- Load shift from data
    data    : in  std_logic_vector(WIDTH-1 downto 0);  -- Input data
    bit_in  : in  std_logic;                           -- Serial bit in
    bit_out : out std_logic;                           -- Serial bit out
    result  : out std_logic_vector(WIDTH-1 downto 0);  -- Output data
    full    : out std_logic);                          -- Got all bits
end shift_reg;
-------------------------------------------------------------------------------
architecture behaviour of shift_reg is
  signal reg_i    : std_logic_vector(WIDTH-1 downto 0) := (others => 'Z');
  signal cnt_i    : integer;                           -- Counter
  signal old_en_i : std_logic;
begin  -- behaviour
  full    <= '1' when cnt_i = WIDTH else '0';
  bit_out <= reg_i(WIDTH-1);
  result  <= reg_i;

-- purpose: Edge detect on enable
-- type   : sequential
-- inputs : clk, rstb, enable
-- outputs: enable_i
  en_edge : process (clk, rstb)
  begin  -- process en_edge
    if rstb = '0' then                  -- asynchronous reset (active low)
      cnt_i    <= 0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      old_en_i <= enable;
      if enable = '1' and old_en_i = '0' then
        if load = '1' then
          reg_i <= data;
          cnt_i <= 1;
        else
          reg_i(0)                <= bit_in;
          reg_i(WIDTH-1 downto 1) <= reg_i(WIDTH-2 downto 0);
          cnt_i                   <= cnt_i + 1;
        end if;
      elsif clear = '1' then
        cnt_i <= 0;
      end if;
    end if;
  end process en_edge;
end behaviour;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity shift_reg_tb is
end shift_reg_tb;

architecture test of shift_reg_tb is
  -----------------------------------------------------------------------------
  -- purpose: Convert std_logic_vector bus to string
  function slv2str (input : std_logic_vector) return string
  is
    variable retval       : string(input'length downto 1);
  begin  -- slv2str
    for i in input'range loop
      case input(i) is
        when '1' | 'H' => retval(i+1) := '1';
        when '0' | 'L' => retval(i+1) := '0';
        when 'U'       => retval(i+1) := 'U';
        when 'X'       => retval(i+1) := 'X';
        when 'Z' | 'W' => retval(i+1) := 'Z';
        when '-'       => retval(i+1) := '-';
        when others    => retval(i+1) := '?';
      end case;
    end loop;  -- i
    return retval;
  end slv2str;

  component shift_reg
    generic (
      WIDTH : natural);
    port (
      clk     : in  std_logic;
      rstb    : in  std_logic;
      enable  : in  std_logic;
      clear   : in  std_logic;
      load    : in  std_logic;
      data    : in  std_logic_vector(WIDTH-1 downto 0);
      bit_in  : in  std_logic;
      bit_out : out std_logic;
      result  : out std_logic_vector(WIDTH-1 downto 0);
      full    : out std_logic);
  end component;

  constant WIDTH     : natural                       := 8;
  constant PERIOD    : time                          := 25 ns;
  constant STUFF     : std_logic_vector(15 downto 0) := X"ffff";
  signal   clk_i     : std_logic                     := '0';
  signal   rstb_i    : std_logic                     := '0';
  signal   enable_i  : std_logic                     := '0';
  signal   clear_i   : std_logic                     := '0';
  signal   load_i    : std_logic                     := '0';
  signal   data_i    : std_logic_vector(7 downto 0)  := X"de";
  signal   bit_in_i  : std_logic                     := '0';
  signal   bit_out_i : std_logic;
  signal   result_i  : std_logic_vector(7 downto 0);
  signal   full_i    : std_logic;
  signal   i         : integer                       := 8;  -- Counter
  signal   j         : integer;                             -- Counter
begin  -- test
  dut : shift_reg
    generic map (
      WIDTH => WIDTH)
    port map (
      clk     => clk_i,
      rstb    => rstb_i,
      enable  => enable_i,                                  -- [in]
      clear   => clear_i,                                   -- [in]
      load    => load_i,                                    -- [in]
      data    => data_i,                                    -- [in]
      bit_in  => bit_in_i,                                  -- [in]
      bit_out => bit_out_i,                                 -- [out]
      result  => result_i,                                  -- [out]
      full    => full_i);                                   -- [out]
  
  clocker: process
  begin  -- process clocker
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clocker;

  bit_in_i <= STUFF(i);
  ext : process (enable_i, clear_i)
  begin  -- process ext
    if clear_i = '1' then
      i <= 0;
    elsif enable_i = '1' then
      if load_i = '0' then
        i <= i + 1;
      else
        i <= 0;
      end if;
    end if;
  end process ext;

  stim: process
  begin  -- process stim
    wait for 4 * PERIOD;
    rstb_i <= '1';
    wait for 4 * PERIOD;
    
    load_i   <= '1';
    wait for PERIOD;
    clear_i  <= '1';
    enable_i <= '1';
    wait for PERIOD;
    clear_i  <= '0';
    enable_i <= '0';
    wait for PERIOD;
    load_i   <= '0';
    wait for 2 * PERIOD;
    
    while full_i = '0' loop
      enable_i <= '1';
      wait for PERIOD;
      enable_i <= '0';
      wait for 2 * PERIOD;
    end loop;
    report "Result is " & slv2str(result_i) severity note;

    wait;                               -- forever
      
  end process stim;
end test;
-- ============================================================================
library ieee;
use ieee.std_logic_1164.all;

entity master_fsm is
  port (clk     : in  std_logic;
        rstb    : in  std_logic;
        scl     : in  std_logic;
        exec    : in  std_logic;
        rnw     : in  std_logic;
        enable  : out std_logic;
        clear   : out std_logic;
        load    : out std_logic;
        sda_in  : in  std_logic;
        sda_out : out std_logic;
        scl_en  : out std_logic;
        full    : in  std_logic;
        which   : out std_logic_vector(2 downto 0);
        busy    : out std_logic);
end master_fsm;

-------------------------------------------------------------------------------
architecture behaviour of master_fsm is

  constant ID_NONE : std_logic_vector(2 downto 0) := "000";
  constant ID_FSM  : std_logic_vector(2 downto 0) := "001";
  constant ID_FEC  : std_logic_vector(2 downto 0) := "010";
  constant ID_REG  : std_logic_vector(2 downto 0) := "011";
  constant ID_B1   : std_logic_vector(2 downto 0) := "100";
  constant ID_B2   : std_logic_vector(2 downto 0) := "101";

  type st_t is (idle,
                start_1,
                start_2,
                fec_wait,
                fec_wait_ack,
                reg_wait,
                reg_wait_ack,
                b1_wait,
                b1_wait_ack,
                b2_wait,
                b2_wait_ack,
                stop_1,
                stop_2);

  signal old_scl_i : std_logic;
  signal st_i      : st_t;              -- -- State
begin  -- behaviour

  fsm : process (clk, rstb)
  begin  -- process fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      sda_out   <= '1';
      which     <= ID_NONE;
      scl_en    <= '0';
      load      <= '0';
      enable    <= '0';
      clear     <= '0';
      st_i      <= idle;
      old_scl_i <= '0';
      busy      <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      old_scl_i <= scl;
      load      <= '0';
      enable    <= '0';
      clear     <= '0';
      
      case st_i is
        when idle =>
          which  <= ID_NONE;
          scl_en <= '0';
          busy   <= '0';
          if exec = '1' then
            st_i    <= start_1;
            which   <= ID_FSM;
            sda_out <= '0';
            busy    <= '1';
          end if;

        when start_1 =>
          if scl = '1' and old_scl_i <= '0' then
            scl_en <= '1';
            st_i   <= start_2;
          end if;

        when start_2 =>
          if scl = '0' and old_scl_i = '1' then
            which  <= ID_FEC;
            load   <= '1';
            enable <= '1';
            clear  <= '1';
            st_i   <= fec_wait;
          end if;

        when fec_wait =>
          if scl = '0' and old_scl_i = '1' then
            if full = '1' then
              clear <= '1';
              st_i  <= fec_wait_ack;
            else
              enable <= '1';
            end if;
          end if;

        when fec_wait_ack =>
          if scl = '0' and old_scl_i = '1' then
            if sda_in /= '0' then
              st_i <= idle;
            else
              which  <= ID_REG;
              load   <= '1';
              enable <= '1';
              clear  <= '1';
              st_i   <= reg_wait;
            end if;
          end if;

        when reg_wait =>
          if scl = '0' and old_scl_i = '1' then
            if full = '1' then
              clear <= '1';
              st_i <= reg_wait_ack;
            else
              enable <= '1';              
            end if;
          end if;

        when reg_wait_ack =>
          if scl = '0' and old_scl_i = '1' then
            if sda_in /= '0' then
              st_i <= idle;
            else
              which  <= ID_B1;
              load   <= not rnw;
              enable <= not rnw;
              clear  <= '1';
              st_i   <= b1_wait;
            end if;
          end if;

        when b1_wait =>
          if scl = '0' and old_scl_i = '1' then
            if full = '1' then
              st_i <= b1_wait_ack;
            elsif rnw = '0' then
              enable <= '1';
            end if;
          end if;
          if scl = '1' and old_scl_i = '0' and rnw = '1' then
            enable <= '1';
          end if;

        when b1_wait_ack =>
          if rnw = '1' then
            which <= ID_FSM;
            sda_out <= '0';
          end if;
          if scl = '0' and old_scl_i = '1' then
            if rnw = '0' and sda_in /= '0' then
              st_i <= idle;
            else
              which  <= ID_B2;
              load   <= not rnw;
              enable <= not rnw;
              clear  <= '1';
              st_i   <= b2_wait;
            end if;
          end if;

        when b2_wait =>
          if scl = '0' and old_scl_i = '1' then
            if full = '1' then
              st_i <= b2_wait_ack;
            elsif rnw = '0' then
              enable <= '1';
            end if;
          end if;
          if scl = '1' and old_scl_i = '0' and rnw = '1' then
            enable <= '1';
          end if;

        when b2_wait_ack =>
          if rnw = '1' then
            which   <= ID_FSM;
            sda_out <= '1';
          end if;
          if scl = '0' and old_scl_i = '1' then
            if rnw = '0' and sda_in /= '0' then
              st_i <= idle;
            else
              which   <= ID_FSM;
              sda_out <= '0';
              st_i    <= stop_1;
            end if;
          end if;

        when stop_1 =>
          if scl = '1' and old_scl_i = '0' then
            sda_out <= '1';
            st_i <= stop_2;
          end if;

        when stop_2 =>
          st_i <= idle;
          
        when others => 
          st_i <= idle;
      end case;
    end if;
  end process fsm;
  

end behaviour;
-- ============================================================================
library ieee;
use ieee.std_logic_1164.all;

entity test_master is
  generic (
    SCL_DIV : natural := 8);                      -- Scale of SCL
  port (
    clk     : in  std_logic;                      -- Clock
    rstb    : in  std_logic;                      -- Reset
    exec    : in  std_logic;                      -- Execute
    data    : in  std_logic_vector(15 downto 0);  -- Input data
    bcast   : in  std_logic;                      -- Broadcast flag
    branch  : in  std_logic;                      -- Branch select
    fec     : in  std_logic_vector(3 downto 0);   -- Where
    rnw     : in  std_logic;                      -- Read (not write) flag
    reg     : in  std_logic_vector(7 downto 0);   -- Register address
    result  : out std_logic_vector(15 downto 0);  -- Result of read
    scl     : out std_logic;                      -- I2C clock
    sda_out : out std_logic;                      -- Serial data out
    sda_in  : in  std_logic;                      -- Serial data input
    busy    : out std_logic);
end test_master;

-------------------------------------------------------------------------------
architecture behaviour of test_master is
  component clock_gen
    generic (
      DIV : natural);
    port (
      clk  : in  std_logic;
      rstb : in  std_logic;
      oclk : out std_logic);
  end component;

  component shift_reg
    generic (
      WIDTH : natural);
    port (
      clk     : in  std_logic;
      rstb    : in  std_logic;
      enable  : in  std_logic;
      clear   : in  std_logic;
      load    : in  std_logic;
      data    : in  std_logic_vector(WIDTH-1 downto 0);
      bit_in  : in  std_logic;
      bit_out : out std_logic;
      result  : out std_logic_vector(WIDTH-1 downto 0);
      full    : out std_logic);
  end component;

  component master_fsm
    port (
      clk     : in  std_logic;
      rstb    : in  std_logic;
      scl     : in  std_logic;
      exec    : in  std_logic;
      rnw     : in  std_logic;
      enable  : out std_logic;
      clear   : out std_logic;
      load    : out std_logic;
      sda_in  : in  std_logic;
      sda_out : out std_logic;
      scl_en  : out std_logic;
      full    : in  std_logic;
      which   : out std_logic_vector(2 downto 0);
      busy    : out std_logic);
  end component;
  
  signal scl_i    : std_logic;
  signal scl_en_i : std_logic;
  
  signal fsm_outputs_i : std_logic_vector(1 downto 0);
  
  signal fec_inputs_i  : std_logic_vector(2 downto 0);
  signal fec_outputs_i : std_logic_vector(1 downto 0);
  signal fec_add_i     : std_logic_vector(7 downto 0);

  signal reg_inputs_i  : std_logic_vector(2 downto 0);
  signal reg_outputs_i : std_logic_vector(1 downto 0);

  signal b1_inputs_i  : std_logic_vector(2 downto 0);
  signal b1_outputs_i : std_logic_vector(1 downto 0);
  signal b1_res_i  : std_logic_vector(7 downto 0);
  
  signal b2_inputs_i  : std_logic_vector(2 downto 0);
  signal b2_outputs_i : std_logic_vector(1 downto 0);
  signal b2_res_i     : std_logic_vector(7 downto 0);

  signal inputs_i : std_logic_vector(2 downto 0);
  signal outputs_i : std_logic_vector(1 downto 0);
  
  constant ID_FSM : std_logic_vector(2 downto 0) := "001";
  constant ID_FEC : std_logic_vector(2 downto 0) := "010";
  constant ID_REG : std_logic_vector(2 downto 0) := "011";
  constant ID_B1  : std_logic_vector(2 downto 0) := "100";
  constant ID_B2  : std_logic_vector(2 downto 0) := "101";
  
  signal which_i  : std_logic_vector(2 downto 0);
  signal which_ii : std_logic_vector(2 downto 0);
  
begin  -- behaviour
  combinatorics : block
  begin  -- block combinatorics
    scl       <= scl_i when scl_en_i = '1' else '1';
    result    <= b1_res_i & b2_res_i;
    fec_add_i <= bcast & '0' & branch & fec & rnw;
  end block combinatorics;

  process (clk, rstb)
  begin  -- process
    if rstb = '0' then                  -- asynchronous reset (active low)
      which_ii <= "000";
    elsif clk'event and clk = '1' then  -- rising clock edge
      which_ii <= which_i;
    end if;
  end process;
  
  fsm_outputs_i(1) <= '0';
  sda_out          <= outputs_i(0);
  fec_inputs_i     <= inputs_i      when which_i = ID_FEC  else "000";
  reg_inputs_i     <= inputs_i      when which_i = ID_REG  else "000";
  b1_inputs_i      <= inputs_i      when which_i = ID_B1   else "000";
  b2_inputs_i      <= inputs_i      when which_i = ID_B2   else "000";
  outputs_i        <= fsm_outputs_i when which_ii = ID_FSM else
                      fec_outputs_i when which_ii = ID_FEC else
                      reg_outputs_i when which_ii = ID_REG else
                      b1_outputs_i  when which_ii = ID_B1  else
                      b2_outputs_i  when which_ii = ID_B2  else "01";
  
  -- Clock generator 
  clock_gen_1 : clock_gen
    generic map (DIV => 8)
    port map (clk  => clk,              -- [in]
              rstb => rstb,             -- [in]
              oclk => scl_i);           -- [out]
  
  fec_reg : shift_reg
    generic map (
      WIDTH => 8)
    port map (
      clk     => clk,                   -- [in]
      rstb    => rstb,                  -- [in]
      enable  => fec_inputs_i(0),       -- [in]
      clear   => fec_inputs_i(1),       -- [in]
      load    => fec_inputs_i(2),       -- [in]
      data    => fec_add_i,             -- [in]
      bit_in  => '0',                   -- [in]
      bit_out => fec_outputs_i(0),      -- [out]
      result  => open,                  -- [out]
      full    => fec_outputs_i(1));     -- [out]

  reg_reg : shift_reg
    generic map (
      WIDTH => 8)
    port map (
      clk     => clk,                   -- [in]
      rstb    => rstb,                  -- [in]
      enable  => reg_inputs_i(0),       -- [in]
      clear   => reg_inputs_i(1),       -- [in]
      load    => reg_inputs_i(2),       -- [in]
      data    => reg,                   -- [in]
      bit_in  => '0',                   -- [in]
      bit_out => reg_outputs_i(0),      -- [out]
      result  => open,                  -- [out]
      full    => reg_outputs_i(1));     -- [out]

  b1_reg : shift_reg
    generic map (
      WIDTH => 8)
    port map (
      clk     => clk,                   -- [in]
      rstb    => rstb,                  -- [in]
      enable  => b1_inputs_i(0),        -- [in]
      clear   => b1_inputs_i(1),        -- [in]
      load    => b1_inputs_i(2),        -- [in]
      data    => data(15 downto 8),     -- [in]
      bit_in  => sda_in,                -- [in]
      bit_out => b1_outputs_i(0),       -- [out]
      result  => b1_res_i,              -- [out]
      full    => b1_outputs_i(1));      -- [out]

  b2_reg : shift_reg
    generic map (
      WIDTH => 8)
    port map (
      clk     => clk,                   -- [in]
      rstb    => rstb,                  -- [in]
      enable  => b2_inputs_i(0),        -- [in]
      clear   => b2_inputs_i(1),        -- [in]
      load    => b2_inputs_i(2),        -- [in]
      data    => data(7 downto 0),      -- [in]
      bit_in  => sda_in,                -- [in]
      bit_out => b2_outputs_i(0),       -- [out]
      result  => b2_res_i,              -- [out]
      full    => b2_outputs_i(1));      -- [out]

  master_fsm_1 : master_fsm
    port map (
      clk     => clk,                   -- [in]
      rstb    => rstb,                  -- [in]
      scl     => scl_i,                 -- [in]
      exec    => exec,                  -- [in]
      rnw     => rnw,                   -- [in]
      enable  => inputs_i(0),           -- [out]
      clear   => inputs_i(1),           -- [out]
      load    => inputs_i(2),           -- [out]
      sda_in  => sda_in,                -- [in]
      sda_out => fsm_outputs_i(0),      -- [out]
      scl_en  => scl_en_i,              -- [out]
      full    => outputs_i(1),          -- [in]
      which   => which_i,               -- [out]
      busy    => busy);              
  
end behaviour;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_master_tb is
  
end test_master_tb;

-------------------------------------------------------------------------------
architecture test of test_master_tb is
  component shift_reg
    generic (
      WIDTH : natural); 
    port (
      clk     : in  std_logic;
      rstb    : in  std_logic;
      enable  : in  std_logic;
      clear   : in  std_logic;
      load    : in  std_logic;
      data    : in  std_logic_vector(WIDTH-1 downto 0);
      bit_in  : in  std_logic;
      bit_out : out std_logic;
      result  : out std_logic_vector(WIDTH-1 downto 0);
      full    : out std_logic); 
  end component;
  component test_master
    generic (
      SCL_DIV : natural);
    port (
      clk     : in  std_logic;
      rstb    : in  std_logic;
      exec    : in  std_logic;
      data    : in  std_logic_vector(15 downto 0);
      bcast   : in  std_logic;
      branch  : in  std_logic;
      fec     : in  std_logic_vector(3 downto 0);
      rnw     : in  std_logic;
      reg     : in  std_logic_vector(7 downto 0);
      result  : out std_logic_vector(15 downto 0);
      scl     : out std_logic;
      sda_out : out std_logic;
      sda_in  : in  std_logic;
      busy    : out std_logic);
  end component;

  signal clk_i     : std_logic                     := '1';
  signal rstb_i    : std_logic                     := '0';
  signal exec_i    : std_logic                     := '0';
  signal data_i    : std_logic_vector(15 downto 0) := X"dead";
  signal bcast_i   : std_logic                     := '0';
  signal branch_i  : std_logic                     := '1';
  signal fec_i     : std_logic_vector(3 downto 0)  := X"a";
  signal rnw_i     : std_logic                     := '1';
  signal reg_i     : std_logic_vector(7 downto 0)  := X"0a";
  signal result_i  : std_logic_vector(15 downto 0);
  signal scl_i     : std_logic;
  signal sda_out_i : std_logic;
  signal sda_in_i  : std_logic;
  signal busy_i    : std_logic;

  type st_t is (idle,
                fec, fec_ack,
                reg, reg_ack,
                tx, tx_ack,
                rx, rx_ack,
                stop);

  signal start_i : std_logic;
  signal stop_i  : std_logic;
  
  signal sda_out_ii     : std_logic;
  signal sda_in_ii      : std_logic := '1';
  signal sda_shift_i    : std_logic;
  signal sda_fsm_i      : std_logic;
  signal scl_ii         : std_logic;
  signal cnt_i          : integer   := 0;
  signal cnt_ii         : integer   := 0;
  signal st_i           : st_t;
  signal rnw_ii         : std_logic;
  signal enable_i       : std_logic;
  signal clear_i        : std_logic;
  signal full_i         : std_logic;
  signal load_i         : std_logic;
  signal en_shift_out_i : std_logic;
  
  constant PERIOD     : time      := 25 ns;
begin  -- test

  sda_in_ii <= sda_shift_i when en_shift_out_i = '1' else sda_fsm_i;
  sda_in_i  <= '0'         when sda_in_ii = '0'      else 'H';
  
  clock_gen: process
  begin  -- process clock_gen
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clock_gen;
  
  dut : test_master
    generic map (
      SCL_DIV => 8)
    port map (
      clk     => clk_i,                 -- [in]
      rstb    => rstb_i,                -- [in]
      exec    => exec_i,                -- [in]
      data    => data_i,                -- [in]
      bcast   => bcast_i,               -- [in]
      branch  => branch_i,              -- [in]
      fec     => fec_i,                 -- [in]
      rnw     => rnw_i,                 -- [in]
      reg     => reg_i,                 -- [in]
      result  => result_i,              -- [out]
      scl     => scl_i,                 -- [out]
      sda_out => sda_out_i,             -- [out]
      sda_in  => sda_in_i,              -- [in]
      busy    => busy_i);

  stim: process
  begin  -- process stim
    wait for 4 * PERIOD;
    rstb_i <= '1';

    wait for 4 * PERIOD;
    exec_i <= '1';
    wait for 2 * PERIOD;
    exec_i <= '0';

    wait for 2 * PERIOD;

    report "Check if master is busy" severity note;
    wait until busy_i = '0';
    report "Master no longer busy" severity note;
    wait for 8 * PERIOD;

    rnw_i <= '0';
    wait for 2 * PERIOD;
    exec_i <= '1';
    wait for 2 * PERIOD;
    exec_i <= '0';
    
    wait;                               -- forever 
  end process stim;

  slave : process (clk_i, rstb_i)
  begin  -- process slave
    if rstb_i = '0' then                    -- asynchronous reset (active low)
      st_i           <= idle;
      sda_fsm_i      <= '1';
      sda_out_ii     <= '0';
      scl_ii         <= '0';
      enable_i       <= '0';
      clear_i        <= '0';
      load_i         <= '0';
      en_shift_out_i <= '0';
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      sda_out_ii <= sda_out_i;
      scl_ii     <= scl_i;
      enable_i   <= '0';
      clear_i    <= '0';
      load_i     <= '0';
      
      case st_i is
        when idle =>
          sda_fsm_i     <= '1';
          cnt_ii        <= 0;
          en_shift_out_i <= '0';
          if start_i = '1' then
            clear_i <= '1';
            st_i    <= fec;
          end if;

        when fec =>
          if stop_i = '1' then
            st_i <= idle;
          end if;
          if scl_i = '1' and scl_ii = '0' then
            enable_i <= '1';
          end if;
          if scl_i = '0' and scl_ii = '1' then
            if full_i = '1' then
              st_i <= fec_ack;
            end if;
          end if;

        when fec_ack =>
          sda_fsm_i <= '0';
          if scl_i = '0' and scl_ii = '1' then
            st_i <= reg;
            clear_i <= '1';            
          end if;
          if stop_i = '1' then
            st_i <= idle;
          end if;

        when reg =>
          sda_fsm_i <= '1';
          if scl_i = '1' and scl_ii = '0' then
            enable_i <= '1';
          end if;
          if scl_i = '0' and scl_ii = '1' then
            if full_i = '1' then
              st_i <= reg_ack;
            end if;
          end if;
          if stop_i = '1' then
            st_i <= idle;
          end if;

        when reg_ack =>
          sda_fsm_i <= '0';
          clear_i   <= '1';
          if scl_i = '0' and scl_ii = '1' then
            if rnw_i = '1' then
              enable_i       <= '1';
              load_i         <= '1';
              en_shift_out_i <= '1';
              st_i           <= tx;
            else
              st_i <= rx;
            end if;
          end if;
          if stop_i = '1' then
            st_i <= idle;
          end if;

        when tx =>
          -- if scl_i = '1' and scl_ii = '0' then
          -- end if;
          if scl_i = '0' and scl_ii = '1' then
            -- sda_in_ii <= to_unsigned(cnt_i,3)(cnt_ii);
            enable_i <= '1';
            if full_i = '1' then
              cnt_ii <= cnt_ii + 1;
              en_shift_out_i <= '0';
              st_i <= tx_ack;
            end if;
          end if;
          if stop_i = '1' then
            st_i <= idle;
          end if;

        when tx_ack =>
          sda_fsm_i <= '0';
          clear_i   <= '1';
          if scl_i = '1' and scl_ii = '0' then
            if sda_out_i /= '0' then
              st_i <= stop;
            end if;
          end if;
          if scl_i = '0' and scl_ii = '1' then
            en_shift_out_i <= '1';
            enable_i       <= '1';
            load_i         <= '1';
            st_i           <= tx;
          end if;
          if stop_i = '1' then
            st_i <= idle;
          end if;

        when rx =>
          sda_fsm_i <= '1';
          if scl_i = '1' and scl_ii = '0' then
            enable_i <= '1';
          end if;
          if scl_i = '0' and scl_ii = '1' then
            if full_i = '1' then
              cnt_ii <= cnt_ii + 1;
              st_i   <= rx_ack;
            end if;
          end if;
          if stop_i = '1' then
            st_i <= idle;
          end if;

        when rx_ack =>
          clear_i   <= '1';
          sda_fsm_i <= '0';             -- acknowledge
          if scl_i = '0' and scl_ii = '1' then
              st_i <= rx;
          end if;
          if stop_i = '1' then
            st_i <= idle;
          end if;

        when stop =>
          if stop_i = '1' then
            st_i <= idle;
          end if;
        when others => null;
      end case;
    end if;
  end process slave;

  shift_reg_1 : shift_reg
    generic map (
      WIDTH => 8)
    port map (
      clk     => clk_i,                 -- [in]
      rstb    => rstb_i,                -- [in]
      enable  => enable_i,              -- [in]
      clear   => clear_i,               -- [in]
      load    => load_i,                -- [in]
      data    => X"aa",                 -- [in]
      bit_in  => sda_out_i,             -- [in]
      bit_out => sda_shift_i,           -- [out]
      result  => open,                  -- [out]
      full    => full_i);               -- [out]
  
  stop_cond : process(scl_i, sda_out_i)
  begin
    if scl_i = '1' and scl_ii = '1' then
      if sda_out_i = '1' and sda_out_ii = '0' then
        stop_i <= '1';
      else
        stop_i <= '0';
      end if;
      if sda_out_i = '0' and sda_out_ii = '1' then
        start_i <= '1';
      else
        start_i <= '0';  
      end if;
    else
      start_i <= '0';
      stop_i <= '0';
    end if;
  end process stop_cond;
  
end test;
-- ============================================================================
--
-- EOF
--
