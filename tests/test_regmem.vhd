library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_regmem is
  port (clk     : in  std_logic;                       --! Clock
        rstb    : in  std_logic;                       --! Async. reset
        clr     : in  std_logic;                       --! Sync. reset
        din     : in  std_logic_vector(15 downto 0);   --! Data in
        we      : in  std_logic;                       --! Write enable
        scsel   : in  std_logic;                       --! I2C select 
        al_add  : in  std_logic_vector(6 downto 0);    --! Altro bus address
        sc_add  : in  std_logic_vector(6 downto 0);    --! I2C bus address
        al_dout : out std_logic_vector(15 downto 0);   --! ALTRO bus data out
        sc_dout : out std_logic_vector(15 downto 0));  --! I2C bus data out
end test_regmem;

-------------------------------------------------------------------------------
architecture rtl of test_regmem is
  constant NUM_REG : integer := 10;     --! Number of registers
  
  subtype regval_t is std_logic_vector(15 downto 0);  --! Register value type
  type    regarray_t is array (NUM_REG-1 downto 0) of regval_t;  --! Register array
  type    reg_t is
    record                              --! Record describing registers
      bcast : boolean;                  --! Allow broadcast
      sc    : boolean;                  --! Allow slow control
      w     : boolean;                  --! Allow write
      mask  : std_logic_vector(15 downto 0);          --! Bit mask
      def   : std_logic_vector(15 downto 0);          --! Default value
    end record reg_t;
  type regs_t is array (NUM_REG-1 downto 0) of reg_t;

  signal cache_i : regarray_t;                       --! Cached register values
  signal value_i : regarray_t;                       --! Register values
  constant regs_i : regs_t := (                      --! Register description
    0      => (true, true, true, X"0001", X"001f"),  --
    1      => (true, true, true, X"0003", X"003f"),  --
    2      => (true, true, true, X"0007", X"007f"),  --
    3      => (true, true, true, X"000f", X"00ff"),  --
    4      => (true, true, true, X"001f", X"01ff"),  --
    5      => (true, true, true, X"003f", X"03ff"),  --
    6      => (true, true, true, X"007f", X"07ff"),  --
    7      => (true, true, true, X"00ff", X"0fff"),  --
    8      => (true, true, true, X"01ff", X"1fff"),  --
    9      => (true, true, true, X"03ff", X"3fff"),  --
    others => (false, false, false, X"0000", X"0000"));
begin  -- rtl

  -- purpose: Output values to ALTRO bus
  -- type   : sequential
  -- inputs : clk, rstb, al_add
  -- outputs: al_dout
  read_out : process (clk)
    variable al_add_i : integer := 0;
    variable sc_add_i : integer := 0;
  begin  -- process al_read
    if rstb = '0' then                  -- asynchronous reset (active low)
      al_add_i   := 0;
      sc_add_i   := 0;
    else
      al_add_i := to_integer(unsigned(al_add));
      sc_add_i := to_integer(unsigned(sc_add));
    end if;

    al_dout <= (others => '0');
    sc_dout <= (others => '0');
    
    if (al_add_i >= 0) and (al_add_i < NUM_REG) then
      al_dout <= value_i(al_add_i) and regs_i(al_add_i).mask;
    end if;

    if (sc_add_i >= 0) and (sc_add_i < NUM_REG) and (regs_i(sc_add_i).sc) then
      sc_dout <= value_i(sc_add_i) and regs_i(sc_add_i).mask;
    end if;
  end process read_out;
  

  -- purpose: Store values from input
  -- type   : sequential
  -- inputs : clk, rstb, al_add, sc_add, we, scsel
  -- outputs: 
  write_in : process (clk, rstb)
    variable i      : integer := 0;
    variable add_i  : integer := 0;
    variable data_i : std_logic_vector(15 downto 0);
  begin  -- process write_in
    if rstb = '0' then                  -- asynchronous reset (active low)
      -- cache_i <= regs_i(NUM_REG-1 downto 0).def;
      for i in NUM_REG-1 downto 0 loop
        cache_i(i) <= regs_i(i).def;
      end loop;  -- i
    elsif clk'event and clk = '1' then  -- rising clock edge
      if clr = '1' then
        -- cache_i <= regs_i.def;
        for i in NUM_REG-1 downto 0 loop
          cache_i(i) <= regs_i(i).def;
        end loop;  -- i
      elsif we = '1' then
        if scsel = '1' then
          add_i  := to_integer(unsigned(sc_add));
        else
          add_i  := to_integer(unsigned(al_add));          
        end if;
        data_i := din(15 downto 0);

        if (add_i >= 0) and (add_i < NUM_REG) then
          cache_i(add_i) <= data_i and regs_i(add_i).mask;
        end if;
      end if;
    end if;
  end process write_in;

  -- purpose: Collect all combinatorics
  combinatorics: block
  begin  -- block combinatorics
    value_i <= cache_i;
  end block combinatorics;
end rtl;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package test_regmem_pack is
  component test_regmem
    port (clk     : in  std_logic;      -- ! Clock
          rstb    : in  std_logic;      -- ! Async. reset
          clr     : in  std_logic;      -- ! Sync. reset
          din     : in  std_logic_vector(15 downto 0);  -- ! Data in
          we      : in  std_logic;      -- ! Write enable
          scsel   : in  std_logic;      -- ! I2C select
          al_add  : in  std_logic_vector(6 downto 0);   -- ! Altro bus address
          sc_add  : in  std_logic_vector(6 downto 0);   -- ! I2C bus address
          al_dout : out std_logic_vector(15 downto 0);  -- ! ALTRO bus data out
          sc_dout : out std_logic_vector(15 downto 0));  -- ! I2C bus data out
  end component;
end test_regmem_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
