-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.test_regmem_pack.all;

entity test_regmem_tb is
end test_regmem_tb;

-------------------------------------------------------------------------------
architecture test of test_regmem_tb is
  constant PERIOD : time := 25 ns;
  
  -- component ports
  signal clk_i     : std_logic                     := '0';
  signal rstb_i    : std_logic                     := '0';
  signal clr_i     : std_logic                     := '0';
  signal din_i     : std_logic_vector(15 downto 0) := (others => '0');
  signal we_i      : std_logic                     := '0';
  signal scsel_i   : std_logic                     := '0';
  signal al_add_i  : std_logic_vector(6 downto 0)  := (others => '0');
  signal sc_add_i  : std_logic_vector(6 downto 0)  := (others => '0');
  signal al_dout_i : std_logic_vector(15 downto 0);
  signal sc_dout_i : std_logic_vector(15 downto 0);
  signal res_i     : integer;
  
  -- purpose: Write value
  procedure write_reg (constant which : in  integer;
                       constant val   : in  integer;
                       signal   clk   : in  std_logic;  -- Clock
                       signal   we    : out std_logic;  -- Write enable
                       signal   add   : out std_logic_vector(6 downto 0);
                       signal   din   : out std_logic_vector(15 downto 0)) is
  begin  -- write_reg
    wait until rising_edge(clk_i);
    din <= std_logic_vector(to_unsigned(val, 16));
    add <= std_logic_vector(to_unsigned(which, 7));
    we  <= '1';
    wait until falling_edge(clk_i);
    wait until rising_edge(clk_i);
    we  <= '0';
    wait until rising_edge(clk_i);
  end write_reg;

  -- purpose: Read value
  procedure read_reg (constant which : in  integer;
                      signal val     : out integer;
                      signal clk     : in  std_logic;  -- Clock
                      signal we      : out std_logic;  -- Write enable
                      signal add     : out std_logic_vector(6 downto 0);
                      signal dout    : in  std_logic_vector(15 downto 0)) is
  begin  -- write_reg
    wait until rising_edge(clk_i);
    val <= 0;
    add <= std_logic_vector(to_unsigned(which, 7));
    we  <= '0';
    wait until falling_edge(clk_i);
    wait until rising_edge(clk_i);
    val <= to_integer(unsigned(dout));
    wait until rising_edge(clk_i);
  end read_reg;

  -- purpose: Write and read value
  procedure read_write_reg (constant which : in  integer;
                            constant val   : in  integer;
                            signal   res   : out integer;
                            signal   clk   : in  std_logic;
                            signal   we    : out std_logic;
                            signal   add   : out std_logic_vector(6 downto 0);
                            signal   din   : out std_logic_vector(15 downto 0);
                            signal   dout  : in  std_logic_vector(15 downto 0))
  is
  begin
    write_reg(which => which,
              val   => val,
              clk   => clk,
              we    => we,
              add   => add,
              din   => din);
    wait until rising_edge(clk_i);
    wait until rising_edge(clk_i);
    read_reg(which => which,
             val   => res,
             clk   => clk,
             we    => we,
             add   => add,
             dout  => dout);
    wait until rising_edge(clk_i);
    
  end read_write_reg;

begin  -- test

  -- component instantiation
  DUT: test_regmem
    port map (clk     => clk_i,           -- [in]  ! Clock
              rstb    => rstb_i,          -- [in]  ! Async. reset
              clr     => clr_i,           -- [in]  ! Sync. reset
              din     => din_i,           -- [in]  ! Data in
              we      => we_i,            -- [in]  ! Write enable
              scsel   => scsel_i,         -- [in]  ! I2C select
              al_add  => al_add_i,        -- [in]  ! Altro bus address
              sc_add  => sc_add_i,        -- [in]  ! I2C bus address
              al_dout => al_dout_i,       -- [out] ! ALTRO bus data out
              sc_dout => sc_dout_i);      -- [out] ! I2C bus data out

  -- clock generation
  clk_i <= not clk_i after PERIOD / 2;

  -- waveform generation
  stim : process
    variable i     : integer := 0;
    variable val_i : integer := 0;
  begin
    -- insert signal assignments here
    wait for 100 ns;
    rstb_i <= '1';
    
    wait for 100 ns;

    for i in 0 to 9 loop
      val_i := 2 * i;
      read_write_reg(which => i,
                     val   => val_i,
                     res   => res_i,
                     clk   => clk_i,
                     we    => we_i,
                     add   => al_add_i,
                     din   => din_i,
                     dout  => al_dout_i);
      assert val_i = res_i
        report "Mismatch in written and read value: "
        & integer'image(val_i) & " != "
        & integer'image(res_i) severity warning;
      wait for 2 * PERIOD;
    end loop;  -- i

    wait;
  end process stim;
end test;

-------------------------------------------------------------------------------
--
-- EOF
--
