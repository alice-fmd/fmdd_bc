-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity test_scope is
end test_scope;

-------------------------------------------------------------------------------
architecture test of test_scope is
  constant PERIOD : time := 25 ns;
  
  signal r1 : std_logic_vector(3 downto 0)  := X"0";
  signal r2 : std_logic_vector(7 downto 0)  := X"00";
  signal r3 : std_logic_vector(11 downto 0) := X"000";
  signal c1 : std_logic_vector(3 downto 0)  := X"0";
  signal c2 : std_logic_vector(7 downto 0)  := X"00";
  signal c3 : std_logic_vector(11 downto 0) := X"000";

  -- purpose: Get register value
  procedure get_reg (constant add :     integer;
                     signal val   : out std_logic_vector) is
  begin  -- get_reg
    -- val <= (others => '0');
    case add is
      when 0      => val(r1'length-1 downto 0)  <= r1;
      when 1      => val(r2'length-1 downto 0)  <= r2;
      when 2      => val(r3'length-1 downto 0) <= r3;
      when others => null;
    end case;
  end get_reg;

  signal add  : integer                       := 0;
  signal din  : std_logic_vector(15 downto 0) := X"0000";
  signal dout : std_logic_vector(15 downto 0) := X"0000";
  signal we   : std_logic                     := '0';
  signal clk  : std_logic                     := '0';
  signal rstb : std_logic                     := '0';

  -- purpose: Convert std_logic_vector to a string
  function slv2str (
    constant v   : std_logic_vector)
    return string
  is
    variable ret : string(v'length downto 1);
    variable i   : integer;
  begin  -- function slv2str
    ret := (others => 'X');

    for i in v'range loop
      case v(i) is
        when 'U'    => ret(i+1) := 'U';
        when 'X'    => ret(i+1) := 'X';
        when '0'    => ret(i+1) := '0';
        when '1'    => ret(i+1) := '1';
        when 'Z'    => ret(i+1) := 'Z';
        when 'W'    => ret(i+1) := 'W';
        when 'L'    => ret(i+1) := 'L';
        when 'H'    => ret(i+1) := 'H';
        when '-'    => ret(i+1) := '-';
        when others => ret(i+1) := 'X';
      end case;
    end loop;  -- i
    return ret;
  end function slv2str;

  procedure check (constant which : in integer;
                   constant val : in std_logic_vector(15 downto 0);
                   signal clk : in std_logic;
                   signal add : out integer;
                   signal we : out std_logic;
                   signal din : out std_logic_vector(15 downto 0);
                   signal dout : in std_logic_vector(15 downto 0)) is
  begin
    din <= val;
    add <= which;
    wait until rising_edge(clk);
    we <= '1';
    wait until falling_edge(clk);
    wait until rising_edge(clk);
    we <= '0';
    wait for 2 * PERIOD;
    assert dout = val report "Returned value does not match input: " &
      slv2str(dout) & " != " & slv2str(val)
      severity note;
  end check;
  
begin  -- test

  handle_read: process (clk)
  begin  -- process handle_read
    if clk'event and clk = '1' then  -- rising clock edge
      dout <= (others => '0');
      get_reg(add, dout);
    end if;
  end process handle_read;

  handle_write : process (clk, rstb)
  begin  -- process handle_write
    if rstb = '0' then                  -- asynchronous reset (active low)
      c1 <= X"0";
      c2 <= X"00";
      c3 <= X"000";
    elsif clk'event and clk = '1' then  -- rising clock edge
      if we = '1' then
        case add is
          when 0      => c1 <= din(3 downto 0);
          when 1      => c2 <= din(7 downto 0);
          when 2      => c3 <= din(11 downto 0);
          when others => null;
        end case;
      end if;
    end if;
  end process handle_write;
  
  stim: process
  begin  -- process stim
    wait for 4 * PERIOD;
    rstb <= '1';

    check(which => 0,
          val   => X"0001",
          clk   => clk,
          we    => we,
          add   => add,
          din   => din,
          dout  => dout);
    wait for 2 * PERIOD;

    check(which => 1,
          val   => X"0011",
          clk   => clk,
          we    => we,
          add   => add,
          din   => din,
          dout  => dout);
    wait for 2 * PERIOD;
    
    check(which => 2,
          val   => X"0111",
          clk   => clk,
          we    => we,
          add   => add,
          din   => din,
          dout  => dout);
    wait for 2 * PERIOD;
    
    wait;
  end process stim;

  -- Clock gen
  clk <= not clk after PERIOD / 2;

  -- Values
  r1 <= c1;
  r2 <= c2;
  r3 <= c3;
  
end test;
