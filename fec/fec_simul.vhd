-------------------------------------------------------------------------------
package fec_misc_pack is
  -- purpose: convert integer to std_logic_vector
  function int2slv (constant x   : integer;             -- Value
                    constant l   : natural)             -- Size
    return std_logic_vector;

  -- purpose: Convert std_logic_vector to a string
  function slv2str (constant v   : std_logic_vector) return string;

  -- purpose: Convert std_logic_vector to integer
  function slv2int (constant v : std_logic_vector) return integer;
end fec_misc_pack;

-------------------------------------------------------------------------------
package body fec_misc_pack is
  -- purpose: convert integer to std_logic_vector
  function int2slv (
    constant x   : integer;             -- Value
    constant l   : natural)             -- Size
    return std_logic_vector is
    variable ret : std_logic_vector(l-1 downto 0);
  begin  -- function int2slv
    ret := std_logic_vector(to_unsigned(x, l));
    return ret;
  end function int2slv;

  -- purpose: Convert std_logic_vector to a string
  function slv2str (
    constant v   : std_logic_vector)
    return string
  is
    variable ret : string(v'length downto 1);
    variable i   : integer;
  begin  -- function slv2str
    ret := (others => 'X');

    for i in v'range loop
      case v(i) is
        when 'U'    => ret(i+1) := 'U';
        when 'X'    => ret(i+1) := 'X';
        when '0'    => ret(i+1) := '0';
        when '1'    => ret(i+1) := '1';
        when 'Z'    => ret(i+1) := 'Z';
        when 'W'    => ret(i+1) := 'W';
        when 'L'    => ret(i+1) := 'L';
        when 'H'    => ret(i+1) := 'H';
        when '-'    => ret(i+1) := '-';
        when others => ret(i+1) := 'X';
      end case;
    end loop;  -- i
    return ret;
  end function slv2str;

  -- purpose: Convert std_logic_vector to integer
  function slv2int (constant v : std_logic_vector) return integer is
  begin
    return to_integer(unsigned(v));
  end function slv2int;
end fec_misc_pack;
-------------------------------------------------------------------------------
--
-- EOF
--

