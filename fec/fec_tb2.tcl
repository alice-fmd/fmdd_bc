
# NC-Sim Command File
# TOOL:	ncsim	06.10-s003
#
#
# You can restore this configuration with:
#
#     ncsim -gui -cdslib /home/hehi/cholm/alice/fee/fmdd_bc/tools/cds.lib -logfile ncsim.log -errormax 15 -status work.fec_tb:test -input /home/hehi/cholm/alice/fee/fmdd_bc/fec/fec_tb2.tcl
#

set tcl_prompt1 {puts -nonewline "ncsim> "}
set tcl_prompt2 {puts -nonewline "> "}
set vlog_format %h
set vhdl_format %v
set real_precision 6
set display_unit auto
set time_unit module
set heap_garbage_size -200
set heap_garbage_time 0
set assert_report_level note
set assert_stop_level error
set autoscope yes
set assert_1164_warnings yes
set pack_assert_off {}
set severity_pack_assert_off {note warning}
set assert_output_stop_level failed
set tcl_debug_level 0
set relax_path_name 0
set vhdl_vcdmap XX01ZX01X
set intovf_severity_level ERROR
set probe_screen_format 0
set rangecnst_severity_level ERROR
set textio_severity_level ERROR
set vital_timing_checks_on 1
set vlog_code_show_force 0
set assert_count_attempts 0
alias . run
alias quit exit
database -open -shm -into waves.shm waves -default
probe -create -database waves : -ports -depth all -waveform
probe -create -database waves : -ports -depth all -waveform
probe -create -database waves :ackn_i :add :adds :bd_i :buf_i :cstb_i :data_rx_i :data_tx_i :dstb_i :eror_i :expect :grst_i :i :inter_i :lvl0_i :lvl1_i :lvl2_i :name_i :rclk_i :reg_add_i :res_i :scl_en_i :scl_i :scl_ii :sclk_i :sin_i :sout_i :trsf_i :writ_i

simvision -input /home/hehi/cholm/alice/fee/fmdd_bc/fec/fec_tb2.tcl.svcf
