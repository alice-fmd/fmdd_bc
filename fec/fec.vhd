
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library ad7417_model;
use ad7417_model.ad7417_pack.all;

use work.bc_pack.all;
use work.fec_misc_pack.all;

-------------------------------------------------------------------------------
entity fec is
  generic (
    HADD : std_logic_vector(4 downto 0) := "00000");  -- card address
  port (
    bd    : inout std_logic_vector(39 downto 0);      -- Bus
    writ  : in    std_logic;                          -- Write (-)
    cstb  : in    std_logic;                          -- Control strobe (-)
    ackn  : out   std_logic;                          -- Acknowledge (-)
    eror  : out   std_logic;                          -- Error (-)
    trsf  : out   std_logic;                          -- Transfer
    dstb  : out   std_logic;                          -- Data strobe
    inter : out   std_logic;                          -- Interrupt
    lvl0  : in    std_logic;                          -- Trigger level 0 (+)
    lvl1  : in    std_logic;                          -- Trigger level 1 (-)
    lvl2  : in    std_logic;                          -- Trigger level 2 (-)
    grst  : in    std_logic;                          -- Global reset (-)
    sclk  : in    std_logic;                          -- Slow clock
    rclk  : in    std_logic;                          -- Readout clock
    scl   : in    std_logic;                          -- I2C clock
    sin   : in    std_logic;                          -- I2C data in
    sout  : out   std_logic);                         -- I2C data out
end fec;

-------------------------------------------------------------------------------
architecture behaviour of fec is
  signal rclk_i          : std_logic;   -- Clock
  signal rstb_i          : std_logic;   -- Reset
  signal sclk_i          : std_logic;   -- Sample clock
  signal scl_i           : std_logic;   -- I2C clock
  signal sda_in_i        : std_logic;   -- I2C data in
  signal sda_out_i       : std_logic;   -- I2C data out
  signal l0_i            : std_logic;   -- L0 trigger
  signal l1b_i           : std_logic;   -- L0 trigger
  signal l2b_i           : std_logic;   -- L0 trigger
  signal mscl_i          : std_logic;   -- Monitor I2C clock
  signal msda_i          : std_logic;   -- Monitor I2C data
  signal pasa_sw_i       : std_logic;   -- Pasa switch
  signal paps_errorb_i   : std_logic := '1';                -- Pasa error (-)
  signal rdoclk_en_i     : std_logic;   -- Readout clk enable
  signal adcclk_en_i     : std_logic;   -- ADC clock enable
  signal adc_add0_i      : std_logic;   -- ADC address
  signal adc_add1_i      : std_logic;   -- ADC address
  signal test_a_i        : std_logic;   -- Test mode
  signal test_b_i        : std_logic;   -- Test mode
  signal test_c_i        : std_logic;   -- Test mode
  signal rst_fbc_i       : std_logic;   -- Reset front-end bc
  signal bcout_ad_i      : std_logic_vector(4 downto 0);    -- out address
  signal oeba_l_i        : std_logic;   -- Enable rcu->card
  signal oeab_l_i        : std_logic;   -- Enable card->rcu
  signal oeba_h_i        : std_logic;   -- Enable rcu->card
  signal oeab_h_i        : std_logic;   -- Enable card->rcu
  signal ctr_in_i        : std_logic;   -- CTRL in enable
  signal ctr_out_i       : std_logic;   -- CTRL out enable
  signal bd_i            : std_logic_vector(39 downto 0);   -- Bus lines (int)
  signal cbus_i          : std_logic_vector(14 downto 0);   -- Control bus
  signal acknb_i         : std_logic;   -- Acknowledge
  signal errorb_i        : std_logic;   -- Error
  signal trsfb_i         : std_logic;   -- Transfer
  signal dstbb_i         : std_logic;   -- Data strobe
  signal writeb_i        : std_logic;   -- Write
  signal cstbb_i         : std_logic;   -- Control strobe
  signal otib_i          : std_logic;   -- Not used
  signal mcst_i          : std_logic;   -- 
  signal clk10_i         : std_logic;   -- ALTRO clock
  signal debug_on_i      : std_logic;   -- Debug on
  signal altro_sw_i      : std_logic;   -- ALTRO switch
  signal sample_clk_i    : std_logic_vector(2 downto 0);    -- Sample clock
  signal altro_l1_i      : std_logic;   -- ALTRO l1 trigger
  signal altro_l2_i      : std_logic;   -- ALTRO l2 trigger
  signal alps_errorb_i   : std_logic := '1';                -- ALTRO ps error
  signal al_dolo_enb_i   : std_logic;   -- ALTRO data out enable
  signal al_trsf_enb_i   : std_logic;   -- ALTRO transfer enable
  signal al_ackn_enb_i   : std_logic;   -- ALTRO acknowledge enable 
  signal digital_reset_i : std_logic;   -- Digital reset for VA1_3
  signal test_on_i       : std_logic;   -- Test on for VA1_3
  signal pulser_enable_i : std_logic;   -- Pulser enable for VA1_3
  signal shift_in_i      : std_logic_vector(2 downto 0);    -- VA1_3 Shift in 
  signal shift_clk_i     : std_logic_vector(2 downto 0);    -- VA1_3 shift 
  signal hold_i          : std_logic_vector(2 downto 0);    -- VA1_3 hold
  signal dac_addr_i      : std_logic_vector (11 downto 0);  -- DAC addres
  signal dac_data_i      : std_logic_vector (7 downto 0);   -- DAC data
  signal busy_i          : std_logic;   -- Trigger busy
  signal debug_i         : std_logic_vector(14 downto 0);   -- Debug

begin  -- behaviour
  -- purpose: Pull up serial lines
  pull_up : block
  begin  -- block pull_up
    -- scl_i         <= 'H';
    -- sda_in_i      <= 'H';
    mscl_i        <= 'H';
    msda_i        <= 'H';
    errorb_i      <= 'H';
    al_dolo_enb_i <= 'H';               -- ALTRO data out enable
    al_trsf_enb_i <= 'H';
    ctr_in_i      <= 'L';
    ctr_out_i     <= 'H';
  end block pull_up;

  -----------------------------------------------------------------------------
  -- High bits of bus
  gtl_h1 : gtl_transciever
    generic map (WIDTH => 10)
    port map (oeab => oeab_h_i, oeba => oeba_h_i,
              a    => bd_i(39 downto 30), b => bd(39 downto 30));
  -- High bits of busy
  gtl_h2 : gtl_transciever
    generic map (WIDTH => 10)
    port map (oeab => oeab_h_i, oeba => oeba_h_i,
              a    => bd_i(29 downto 20), b => bd(29 downto 20));
  -- Low bits of bus
  gtl_l1 : gtl_transciever
    generic map (WIDTH => 10)
    port map (oeab => oeab_l_i, oeba => oeba_l_i,
              a    => bd_i(19 downto 10), b => bd(19 downto 10));
  -- Low bits of bus
  gtl_l2 : gtl_transciever
    generic map (WIDTH => 10)
    port map (oeab => oeab_l_i, oeba => oeba_l_i,
              a    => bd_i(9 downto 0), b => bd(9 downto 0));
  -----------------------------------------------------------------------------
  -- Control bus
  scl_i    <= scl;
  sda_in_i <= sin;
  rclk_i   <= rclk;
  sclk_i   <= sclk;
  rstb_i   <= grst;
  writeb_i <= writ;
  cstbb_i  <= cstb;
  l2b_i    <= lvl2;
  l1b_i    <= lvl1;
  sout     <= sda_out_i when ctr_out_i = '0' else 'H';
  trsf     <= trsfb_i   when ctr_out_i = '0' else 'H';
  dstb     <= dstbb_i   when ctr_out_i = '0' else 'H';
  eror     <= errorb_i  when ctr_out_i = '0' else 'H';
  ackn     <= acknb_i   when ctr_out_i = '0' else 'H';

  ----------------------------------------------------------------------------
  -- BC
  bc_1 : bc
    port map (
      clk           => rclk_i,           -- in  Clock
      rstb          => rstb_i,           -- in  Async reset
      sclk          => sclk_i,           -- in  Sample clock (10 MHz)
      -- Serial interface
      scl           => scl_i,            -- in  I2C: Serial clock (<= 5MHz)
      sda_in        => sda_in_i,         -- in  I2C: data input
      sda_out       => sda_out_i,        -- out I2C: data out
      -- Triggers
      l0            => l0_i,             -- in  Level 0 trigger
      l1b           => l1b_i,            -- in  Level 1 trigger
      l2b           => l2b_i,            -- in  Level 2 trigger
      -- Monitor interface
      mscl          => mscl_i,           -- out ADC: clock
      msda          => msda_i,           -- inout ADC: data in/out
      -- Pasa
      pasa_sw       => pasa_sw_i,        -- out PASA switch
      paps_errorb   => paps_errorb_i,    -- in  PASA power supply error
      -- Test mode
      rdoclk_en     => rdoclk_en_i,      -- out TSM: Readout clock enable
      adcclk_en     => adcclk_en_i,      -- out TSM: ADC clock enable
      adc_add0      => adc_add0_i,       -- out ADC address bit 0
      adc_add1      => adc_add1_i,       -- out ADC address bit 1
      test_a        => test_a_i,         -- out TSM: mask a
      test_b        => test_b_i,         -- out TSM: mask b
      test_c        => test_c_i,         -- out TSM: mask c
      -- Hardware stuff
      rst_fbc       => rst_fbc_i,        -- out Reset front-end card
      hadd          => HADD,             -- in  Card address
      bcout_ad      => bcout_ad_i,       -- out Out board address
      -- GTL enable on/off
      oeba_l        => oeba_l_i,         -- out GTL: enable in low bits
      oeab_l        => oeab_l_i,         -- out GTL: enable out low bits
      oeba_h        => oeba_h_i,         -- out GTL: enable in high bits
      oeab_h        => oeab_h_i,         -- out GTL: enable out high bits
      ctr_in        => ctr_in_i,         -- out GTL: enable ctrl bus in
      ctr_out       => ctr_out_i,        -- out GTL: enable ctrl bus out
      -- Bus
      bd            => bd_i,             -- inout Bus: 40 bits
      -- Control bus
      acknb         => acknb_i,          -- inout CTL: Acknowledge
      errorb        => errorb_i,         -- inout CTL: Error
      trsfb         => trsfb_i,          -- inout CTL: Transfer
      dstbb         => dstbb_i,          -- inout CTL: Data strobe
      writeb        => writeb_i,         -- inout CTL: Write
      cstbb         => cstbb_i,          -- inout Control strobe
      bc_int        => inter,            -- out CTL: Interrupt
      -- Misc
      otib          => otib_i,           -- Not used
      mcst          => mcst_i,           -- out ADC : start disabled='0';
      clk10         => clk10_i,          -- Not used
      debug_on      => debug_on_i,       -- Not used
      -- ALTRO
      altro_sw      => altro_sw_i,       -- out ALTRO switch
      sample_clk    => sample_clk_i,     -- out Sample clock
      altro_l1      => altro_l1_i,       -- out L1 for ALTROs (active low)
      altro_l2      => altro_l2_i,       -- out L2 for ALTROs (active low)
      alps_errorb   => alps_errorb_i,    -- in  ALTRO power supply error
      al_dolo_enb   => al_dolo_enb_i,    -- in  ALTRO data out enable
      al_trsf_enb   => al_trsf_enb_i,    -- in  ALTRO transfer enable
      al_ackn_enb   => al_ackn_enb_i,    -- Not used
      -- VA1
      digital_reset => digital_reset_i,  -- out Reset VA1's
      test_on       => test_on_i,        -- out Calibration mode on.
      pulser_enable => pulser_enable_i,  -- out Enable step on pulser
      shift_in      => shift_in_i,       -- out shift reset
      shift_clk     => shift_clk_i,      -- out shift register
      hold          => hold_i,           -- out Hold charge
      -- DAC interface
      dac_addr      => dac_addr_i,       -- out DAC address
      dac_data      => dac_data_i,       -- out DAC data
      -- L0 interface
      busy          => busy_i,           -- out busy (LED on test)
      -- Debug
      debug         => debug_i);         -- out Debug pins

  -----------------------------------------------------------------------------
  -- Monitors
  mons : for i in 0 to 3 generate
  begin  -- generate mons
    adc : entity ad7417_model.ad7417
      generic map (
        VENDOR => "0101",                  -- Vendor ID
        ADD    => int2slv(i, 3),           -- Address
        PERIOD => 6.5 us,                  -- Period of clk
        T      => to_unsigned(5*i+1, 16),  -- T value
        ADC1   => to_unsigned(5*i+2, 16),  -- ADC1 value
        ADC2   => to_unsigned(5*i+3, 16),  -- ADC2 value
        ADC3   => to_unsigned(5*i+4, 16),  -- ADC3 value
        ADC4   => to_unsigned(5*i+5, 16))  -- ADC4 value
      port map (
        scl => mscl_i,                     -- inout I2C Clock
        sda => msda_i);                    -- inout I2C data
  end generate mons;

end behaviour;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package fec_pack is
  component fec
    generic (
      HADD : std_logic_vector(4 downto 0));
    port (
      bd    : inout std_logic_vector(39 downto 0);
      writ  : in    std_logic;
      cstb  : in    std_logic;
      ackn  : out   std_logic;
      eror  : out   std_logic;
      trsf  : out   std_logic;
      dstb  : out   std_logic;
      inter : out   std_logic;
      lvl0  : in    std_logic;
      lvl1  : in    std_logic;
      lvl2  : in    std_logic;
      grst  : in    std_logic;
      sclk  : in    std_logic;
      rclk  : in    std_logic;
      scl   : in    std_logic;
      sin   : in    std_logic;
      sout  : out   std_logic);
  end component;
end fec_pack;
------------------------------------------------------------------------------
--
-- EOF
--
