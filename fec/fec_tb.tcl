
# NC-Sim Command File
# TOOL:	ncsim	06.10-s003
#
#
# You can restore this configuration with:
#
#     ncsim fec_tb -cdslib tools/cds.lib -hdlvar tools/hdl.var -logfile tools/nc.log -append_log -nocopyright -gui -input fec_tb.tcl -input /home/hehi/cholm/alice/fee/fmdd_bc/fec/fec_tb.tcl
#

set tcl_prompt1 {puts -nonewline "ncsim> "}
set tcl_prompt2 {puts -nonewline "> "}
set vlog_format %h
set vhdl_format %v
set real_precision 6
set display_unit auto
set time_unit module
set heap_garbage_size -200
set heap_garbage_time 0
set assert_report_level note
set assert_stop_level error
set autoscope yes
set assert_1164_warnings yes
set pack_assert_off {}
set severity_pack_assert_off {note warning}
set assert_output_stop_level failed
set tcl_debug_level 0
set relax_path_name 0
set vhdl_vcdmap XX01ZX01X
set intovf_severity_level ERROR
set probe_screen_format 0
set rangecnst_severity_level ERROR
set textio_severity_level ERROR
set vital_timing_checks_on 1
set vlog_code_show_force 0
set assert_count_attempts 0
alias . run
alias quit exit
stop -create  -time 60 US -relative -modulo 60 US
database -open -shm -into waves.shm waves -default
probe -create -database waves :sout_i :sin_i :scl_ii :scl_i :scl_en_i :res_i :reg_add_i :name_i :inter_i :grst_i :data_tx_i :data_rx_i :buf_i :add :fecs(0):DUT:scl :fecs(0):DUT:scl_i :fecs(0):DUT:sda_in_i :fecs(0):DUT:sda_out_i :fecs(0):DUT:sin :fecs(0):DUT:sout :fecs(0):DUT:bc_1:core:slctr :fecs(0):DUT:inter :fecs(1):DUT:scl :fecs(1):DUT:scl_i :fecs(1):DUT:sda_in_i :fecs(1):DUT:sda_out_i :fecs(1):DUT:sin :fecs(1):DUT:sout :fecs(1):DUT:bc_1:core:slctr :fecs(1):DUT:inter
probe -create -database waves : -all -variables -memories -depth all -waveform

simvision -input /home/hehi/cholm/alice/fee/fmdd_bc/fec/fec_tb.tcl.svcf
