-------------------------------------------------------------------------------
-- Title      : Testbench for design "fec"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : fec_tb.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Created    : 2008-03-07
-- Last update: 2008-08-14
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2008 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2008-03-07  1.0      cholm	Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.fec_pack.all;
-- use work.fec_misc_pack.all;
use work.i2c_simul_pack.all;
use work.register_config.all;

library rcu_model;
use rcu_model.rcu_pack.all;
use rcu_model.rcu_misc_pack.all;

-------------------------------------------------------------------------------
entity fec_tb is
end fec_tb;

-------------------------------------------------------------------------------
architecture test of fec_tb is
  -- component ports
  signal bd_i      : std_logic_vector(39 downto 0) := (others => 'H');
  signal writ_i    : std_logic                     := 'H';
  signal cstb_i    : std_logic                     := 'H';
  signal ackn_i    : std_logic;
  signal eror_i    : std_logic;
  signal trsf_i    : std_logic;
  signal dstb_i    : std_logic;
  signal inter_i   : std_logic;
  signal lvl0_i    : std_logic                     := '0';
  signal lvl1_i    : std_logic                     := '1';
  signal lvl2_i    : std_logic                     := '1';
  signal grst_i    : std_logic                     := '1';
  signal sclk_i    : std_logic                     := '1';
  signal rclk_i    : std_logic;

  signal scl_i     : std_logic;
  signal sin_i     : std_logic := 'H';
  signal sout_i    : std_logic;
  signal scl_inh_i : std_logic := '0';
  signal scl_en_i  : std_logic := '0';
  signal scl_ii    : std_logic;
  signal scl_iii   : std_logic;
  
  signal data_tx_i : std_logic_vector(15 downto 0);  -- Data to output
  signal data_rx_i : std_logic_vector(15 downto 0);  -- Data read from bus
  signal reg_add_i : std_logic_vector(6 downto 0);   -- Register address
  signal i         : integer;                        -- counter
  signal add       : integer;                        -- counter
  signal buf_i     : unsigned(15 downto 0);
  signal res_i     : unsigned(15 downto 0);

  signal i2c_state_i : i2c_state_t;
  
  signal name_i : string(7 downto 1);

  type addrs is array (0 to 2) of add_t;
  type defs  is array (0 to 2) of std_logic_vector(15 downto 0);
  signal adds   : addrs := (add_csr0,
                            add_csr1,
                            add_csr2);
  signal expect : defs  := (regs(add_csr0).def,
                            regs(add_csr1).def,
                            regs(add_csr2).def);
  
begin  -- architecture stimulus
  -- Reset
  reset_it : entity rcu_model.reseter port map (rstb => grst_i);

  -- 40 Mhz
  rclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 40000000)
    port map (clk       => rclk_i);

  -- 10 MHz  
  sclk_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 10000000)
    port map (clk       => sclk_i);

  -- 5 MHz  
  scl_clocker : entity rcu_model.clocker
    generic map (PERIOD => 1 sec / 5000000)
    port map (clk       => scl_i);
  scl_iii <= scl_i when scl_en_i = '1' else 'H';
  scl_ii <= '0' when scl_inh_i = '1' else scl_iii;

  -- component instantiation
  fecs : for i in 0 to 1 generate
  begin  -- generate mons
    DUT: fec
      generic map (
        HADD => int2slv(i, 5))
      port map (
        bd    => bd_i,
        writ  => writ_i,
        cstb  => cstb_i,
        ackn  => ackn_i,
        eror  => eror_i,
        trsf  => trsf_i,
        dstb  => dstb_i,
        inter => inter_i,
        lvl0  => lvl0_i,
        lvl1  => lvl1_i,
        lvl2  => lvl2_i,
        grst  => grst_i,
        sclk  => sclk_i,
        rclk  => rclk_i,
        scl   => scl_ii,
        sin   => sin_i,
        sout  => sout_i);
  end generate fecs;


  -- waveform generation
  stim : process
  begin
    bd_i   <= (others => '0');
    writ_i <= 'H';
    cstb_i <= 'H';
    lvl0_i <= '0';
    lvl1_i <= '1';
    lvl2_i <= '1';
    wait until grst_i /= '0';

    wait for 200 ns;
    
    read_register(
      hadd => "00000",
      what =>  "CSR1",
      bd => bd_i,
      cstb => cstb_i,
      writ => writ_i,
      ackn => ackn_i,
      trsf => trsf_i,
      rclk => rclk_i,
      name => name_i);

    wait for 100 ns;

    for add in 0 to 1 loop
      for i in 0 to 2 loop
        wait until rising_edge(rclk_i);
        buf_i <= unsigned(expect(i));
        send(hadd    => int2slv(add,5),
             addr    => adds(i),
             bcast   => false,
             rw      => true,
             idat    => buf_i,
             odat    => res_i,
             scl     => scl_i,
             scl_en  => scl_en_i,
             scl_inh => scl_inh_i,
             sda_in  => sout_i,
             sda_out => sin_i,
             state   => i2c_state_i);
        wait until rising_edge(rclk_i);
        sin_i <= 'H';
        report "Read " & integer'image(to_integer(res_i)) & " from "
          & integer'image(adds(i)) severity note; 
        assert buf_i = res_i report
          integer'image(to_integer(res_i)) & " and expected mis-match " &
          integer'image(to_integer(unsigned(expect(i)))) severity warning;
        wait until rising_edge(rclk_i);
        wait for 1 us; 
      end loop;  -- i
    end loop;  -- add
    
    wait; -- forever 
  end process stim;
end test;

-------------------------------------------------------------------------------

configuration fec_tb_test_cfg of fec_tb is
  for test
  end for;
end fec_tb_test_cfg;

-------------------------------------------------------------------------------
