
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;

entity test_counter is
end test_counter;
------------------------------------------------------------------------------
architecture test of test_counter is
  constant PERIOD : time    := 25 ns;   -- Clock cycle
  constant WIDTH  : integer := 4;       -- Width of counter
  constant MAX    : integer := 2 ** WIDTH;

  signal clk_i     : std_logic                          := '0';  -- Clock
  signal rstb_i    : std_logic                          := '1';  -- Async reset
  signal load_i    : std_logic                          := '0';  -- Load start
  signal enable_i  : std_logic                          := '0';  -- Enable
  signal start_i   : std_logic_vector(WIDTH-1 downto 0) := "0000";
  signal output_i  : std_logic_vector(WIDTH-1 downto 0);  -- Output
  signal output_ii : std_logic_vector(WIDTH-1 downto 0);  -- Output
  signal i         : integer;           -- Counter
  signal rst_i     : std_logic;

begin  -- test
  rst_i <= not rstb_i;
  
  counter: LPM_COUNTER
    generic map (
      LPM_WIDTH     => WIDTH,
      LPM_DIRECTION => "UP")
    port map (
      data   => start_i,
      clock  => clk_i,
      cnt_en => enable_i,
      sload  => load_i,
      aclr   => rst_i,
      q      => output_i);  

  -- purpose: Counter w/load and enable
  -- type   : sequential
  -- inputs : clk, rstb, enable_i, load_i, start_i
  -- outputs: output_ii
  my_counter       : process (clk_i, rstb_i)
    variable cnt_i : unsigned(WIDTH-1 downto 0);
  begin  -- process my_counter
    if rstb_i = '0' then                    -- asynchronous reset (active low)
      output_ii <= (others => '0');
      cnt_i     := (others => '0');
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if load_i = '1' then
        cnt_i   := unsigned(start_i);
      elsif enable_i = '1' then
        if cnt_i = "1111" then
          cnt_i := (others => '0');
        else
          cnt_i := cnt_i + 1;
        end if;
      end if;
      output_ii <= std_logic_vector(cnt_i);
    end if;
  end process my_counter;

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    rstb_i  <= '0';
    wait for 100 ns;
    rstb_i  <= '1';
    wait for 4 * PERIOD;
    start_i <= "0010";
    wait until rising_edge(clk_i);
    load_i <= '1';
    wait until rising_edge(clk_i);
    load_i <= '0';
    
    for i in 0 to MAX loop
      wait until rising_edge(clk_i);
      enable_i <= '1';
      wait until rising_edge(clk_i);
      enable_i <= '0';
    end loop;  -- i

    wait until rising_edge(clk_i);
    rstb_i <= '0';
    wait until rising_edge(clk_i);
    wait until rising_edge(clk_i);
    rstb_i <= '1';
    
    wait;                               -- forever
  end process stimuli;
  
  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
  
end test;
