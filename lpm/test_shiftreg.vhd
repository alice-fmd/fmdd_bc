------------------------------------------------------------------------------
-- Title      : Test of LPM shift register
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : test_shiftreg.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2006/09/08
-- Platform   : 
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test of LPM shift register
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/08  1.0      cholm   Created
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;
use work.clock_scl_pack.all;

entity test_shiftreg is
end test_shiftreg;
------------------------------------------------------------------------------
architecture test of test_shiftreg is
  constant PERIOD : time    := 25 ns;   -- Clock cycle
  constant MAX    : integer := 2 ** 6 - 1;
  constant DIV    : integer := 32;

  signal clk_i        : std_logic                    := '0';  -- Clock
  signal rstb_i       : std_logic                    := '1';  -- Async reset
  signal data_i       : std_logic_vector(7 downto 0) := (others => '0');
  signal enable_i     : std_logic                    := '0';
  signal shiftin_i    : std_logic                    := '0';
  signal load_i       : std_logic                    := '0';
  signal out_i        : std_logic_vector(7 downto 0);
  signal myout_i      : std_logic_vector(7 downto 0);
  signal out2_i       : std_logic_vector(7 downto 0);
  signal shiftout_i   : std_logic;
  signal myshiftout_i : std_logic;
  signal shiftout2_i  : std_logic;
  signal i            : integer;                              -- Counter
  signal and_enable_i : std_logic;
  signal sclk_i       : std_logic                    := '1';
  signal sclk_en_i    : std_logic;
  signal nrstb_i      : std_logic;
  signal delay_i      : integer range 0 to 2 * DIV + 1;
begin  -- test
  nrstb_i <= not rstb_i;

  scale_clk : clock_scl
    generic map (
      DIV     => DIV)
    port map (
      clk     => clk_i,
      rstb    => nrstb_i,
      clk_scl => sclk_i,
      clk_en  => sclk_en_i);

  -- Enable pushes the data up one bit.
  -- Shiftin is the value pushed in at the end of the data
  -- Shiftout is the MSB of the data
  dut : LPM_SHIFTREG
    generic map (
      LPM_WIDTH     => 8,
      LPM_DIRECTION => "LEFT")
    port map (
      clock         => sclk_i,
      aclr          => rstb_i,
      data          => data_i,
      enable        => enable_i,
      shiftin       => shiftin_i,
      load          => load_i,
      q             => out_i,
      shiftout      => shiftout_i);

  dut2 : LPM_SHIFTREG
    generic map (
      LPM_WIDTH     => 8,
      LPM_DIRECTION => "LEFT")
    port map (
      clock         => clk_i,
      aclr          => rstb_i,
      data          => data_i,
      enable        => and_enable_i,
      shiftin       => shiftin_i,
      load          => load_i,
      q             => out2_i,
      shiftout      => shiftout2_i);

  delay_it : process (clk_i, rstb_i)
  begin  -- process delay_it
    if rstb_i = '1' then                    -- asynchronous reset (active low)
      delay_i   <= 0;
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if sclk_en_i = '1' and enable_i = '1' then
        report "delay set to 1" severity note;
        delay_i <= 1;
      elsif delay_i = 2 * DIV + 1 then
        report "delay set to zero" severity note;
        delay_i <= 0;
      elsif delay_i > 0 then
        report "delay incremented" severity note;
        delay_i <= delay_i + 1;
      end if;
    end if;
  end process delay_it;
  and_enable_i  <= '1' when (delay_i = 2 * DIV +1) else '0';

  -- purpose: Shift register
  -- type   : sequential
  -- inputs : clk_i, rstb_i, enable_i, shiftin_i
  -- outputs: myshiftout_i, myout_i

  shift_it         : process (clk_i, rstb_i)
    variable tmp_i : std_logic_vector(7 downto 0);
  begin  -- process shift_it
    if rstb_i = '1' then                -- asynchronous reset (active low)
      myout_i      <= (others => '0');
      myshiftout_i <= '0';
      tmp_i := (others        => '0');

    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if and_enable_i = '1' then
        if load_i = '1' then
          tmp_i             := data_i;
        else
          tmp_i(7 downto 1) := tmp_i(6 downto 0);
          tmp_i(0)          := shiftin_i;
        end if;
        myout_i      <= tmp_i;
        myshiftout_i <= tmp_i(7);
      end if;
    end if;
  end process shift_it;

  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 100 ns;
    rstb_i <= '0';
    wait for 100 ns;

    data_i   <= "10101010";
    wait until rising_edge(sclk_i);
    wait until rising_edge(sclk_i);
    enable_i <= '1';                    -- Enable must be asserted when loading
    load_i   <= '1';
    wait until rising_edge(sclk_i);
    enable_i <= '0';
    load_i   <= '0';

    wait until rising_edge(sclk_i);
    wait until rising_edge(sclk_i);
    for i in 0 to 8 loop
      wait until rising_edge(sclk_i);
      shiftin_i <= '1';
      enable_i  <= '1';
      wait until rising_edge(sclk_i);
      shiftin_i <= '0';
      enable_i  <= '0';
    end loop;  -- i


    for i in 0 to 8 loop
      wait until rising_edge(sclk_i);
      enable_i <= '1';
      wait until rising_edge(sclk_i);
      enable_i <= '0';
    end loop;  -- i

    wait;                               -- forever
  end process stimuli;

  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;

end test;

------------------------------------------------------------------------------
--
-- EOF
--
