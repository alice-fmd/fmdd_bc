------------------------------------------------------------------------------
-- Title      : Test of LPM bustri buffers
-- Project    : ALICE FMD Digitizer Board Controller
------------------------------------------------------------------------------
-- File       : test_bustri.vhd
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : 
-- Last update: 2006/09/10
-- Platform   : 
-------------------------------------------------------------------------------
-- ALICE FMD Digitiser Board Controller firmware.
--
-- This code takes care of
-- * monitoring voltages, currents, and temperatures.
-- * handle L0 triggers, and sends the appropriate contro signals to the
--   VA1 pre-amplifier chips on the FMD hybrid cards.
-- * controls the GTL bus transceivers for the ALTRO's on the card, and
--   of course for it self.
--
-- The code is based on the TPC FEC BC code. The original authors were
-- Carmen Gonzalez <Carmen.Gonzalez.Gutierrez@cern.ch> and Roberto Campagnolo
-- <Roberto.Campagnolo@cern.ch>.
--
-- Some initial development, and a lot of guidance was given by Daniel
-- Kirschner <daniel.kirschner@nbi.dk> and Tiago Perez
-- <Tiago.Perez@exp2.physik.uni-giessen.de>
------------------------------------------------------------------------------
-- Description: Test how a tri-state buffer works.
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2006/09/10  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library lpm;
use lpm.lpm_components.all;

entity test_bustri is  
end test_bustri;

------------------------------------------------------------------------------
architecture test of test_bustri is
  constant gnd_i : std_logic_vector(0 downto 0) := "0";

  signal en_data1_i : std_logic := '0';
  signal tri1_i     : std_logic_vector(0 downto 0) := "1";

  signal en_data2_i : std_logic := '0';
  signal en_tri2_i  : std_logic := '1';
  signal tri2_i     : std_logic_vector(0 downto 0)  := "1";
  signal res2_i     : std_logic_vector(0 downto 0);

  signal mytri1_i  : std_logic := '1';
  signal mytri2_i : std_logic := '1';
  signal myres2_i : std_logic;

begin  -- test

  bustri1: LPM_BUSTRI
    generic map (
      LPM_WIDTH => 1)
    port map (
      data     => gnd_i,
      enabledt => en_data1_i,
      tridata  => tri1_i);
  

  bustri2 : LPM_BUSTRI
    generic map (
      LPM_WIDTH => 1)
    port map (
      data      => gnd_i,
      enabledt  => en_data2_i,
      enabletr  => en_tri2_i,
      result    => res2_i,
      tridata   => tri2_i);


  -- purpose: Implement a tri-state buffer - uni-directional (open collector)
  mytri1: block
  begin  -- block mytri1
    mytri1_i <= '0' when en_data1_i = '1' else 'Z';
  end block mytri1;


  -- purpose: Implement a tri-state buffer - bi-directional
  mytri2 : block
  begin  -- block mytri2
    mytri2_i <= '0'       when en_data2_i = '1' else 'Z';
    myres2_i <= tri2_i(0) when en_tri2_i = '1'  else 'Z';
  end block mytri2;

  -- purpose: Provide stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 100 ns;
    mytri1_i   <= '1';
    mytri2_i   <= '1';
    tri1_i     <= "1";
    tri2_i     <= "1";
    wait for 100 ns;
    en_tri2_i  <= '0';
    wait for 10 ns;
    mytri1_i   <= 'Z';
    mytri2_i   <= 'Z';
    tri1_i     <= "Z";
    tri2_i     <= "Z";
    wait for 10 ns;
    en_data1_i <= '1';
    en_data2_i <= '1';
    wait for 100 ns;
    en_data1_i <= '0';
    en_data2_i <= '0';
    wait for 10 ns;
    en_tri2_i  <= '1';
    wait for 10 ns;
    mytri1_i   <= '1';
    mytri2_i   <= '1';
    tri1_i     <= "1";
    tri2_i     <= "1";


    wait for 100 ns;
    wait;                               -- forever
  end process stimuli;
end test;
------------------------------------------------------------------------------
--
-- EOF
--
