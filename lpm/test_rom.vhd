
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library lpm;
use lpm.lpm_components.all;

entity test_rom is
end test_rom;
------------------------------------------------------------------------------
architecture test of test_rom is
  constant PERIOD : time    := 25 ns;   -- Clock cycle
  constant MAX    : integer := 2 ** 6 - 1;

  signal clk_i  : std_logic := '0';     -- Clock
  signal rstb_i : std_logic := '1';     -- Async reset
  signal add_i  : std_logic_vector(6 downto 0) := (others => '0');
  signal data_i : std_logic_vector(10 downto 0);
  signal i      : integer;              -- Counter

begin  -- test

  rom : LPM_ROM
    generic map (
        LPM_WIDTH              => 11,
        LPM_WIDTHAD            => 7,
        LPM_NUMWORDS           => 256,
        LPM_ADDRESS_CONTROL    => "REGISTERED",
        LPM_OUTDATA            => "REGISTERED",
        LPM_FILE               => "rom.hex",
        INTENDED_DEVICE_FAMILY => "ACEX1K")
    port map (
        address                => add_i,
        inclock                => clk_i,
        outclock               => clk_i,
        q                      => data_i);


  -- purpose: Provdides stimuli
  -- type   : combinational
  stimuli : process
  begin  -- process stimuli
    wait for 100 ns;

    for i in 0 to MAX loop
      wait until rising_edge(clk_i);
      add_i <= std_logic_vector(to_unsigned(i, 7));
      wait until rising_edge(clk_i);
    end loop;  -- i

    wait;                               -- forever
  end process stimuli;
  
  -- purpose: Clock generator
  -- type   : combinational
  -- outputs: clk_i
  clock_gen: process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
  
end test;
