
		ALICE FMD Digitizer Board Controller
		====================================

This is a port of the ALICE TPC FEC Board Controller firmware code
from schematic and Verilog code to VHDL code.   The code has also been
adapted to suit the needs of the ALICE FMD Digitizer Board Controller
requirements. 

Differences to the TPC BC code:
-------------------------------

- More registers. 
- More Monitor ADC's.


Structure of the code:
---------------------- 

Below is the hierarchy of the code illustrated as a tree: 


        bc_core
        |-- altro_sw_mask_out
        |-- altrobusinterface
        |   |-- alprotocol_if
        |   `-- interfacebus
        |-- evl_man
        |   |-- alevl_address
        |   |-- ch_counter
        |   |-- evlreg_trsf
        |   `-- read_evl
        |-- interface_adc
        |   |-- master
        |   |   |-- clock_scl
        |   |   |-- decoder
        |   |   |-- exec
        |   |   |-- master_sm
        |   |   |-- serializer
        |   |   |   `-- cnt_8
        |   |   `-- sync
        |   |-- rom
        |   `-- sequencer
        |-- interfacedec
        |-- registers
        |   |-- counters
        |   |   |-- dstb_counter
        |   |   |-- sclk_counter
        |   |   `-- triggercounter
        |   `-- registers_block
        |-- slave
        |   |-- fec_address
        |   |-- sel_signals
        |   |-- serializer_bc
        |   |-- slave_rx
        |   `-- slave_tx
        |-- trsf_mux
        |   `-- mux_rdo
        `-- tsm_man
            |-- mem_rdo
            `-- mem_wr
        
There's a one-to-one correspondence to the names given above, and the
VHDL source files
