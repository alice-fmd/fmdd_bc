---------------------------------------------------------------------
-- DESIGN RCU FIRM WARE V.2.0
-- MODULE NAME   : MSM_DECODER.VHD
--
----------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;

library msmodule2_lib;
use msmodule2_lib.msm2_interface_i2c_pack.all;
use msmodule2_lib.msm2_decoder_pack.all;

-- library WORK;
-- use work.rcu_addr_set.all;

entity msm2_msmodule is
  
  port(

    rst : in std_logic;
    clk : in std_logic;                 -- RCU CLOCK.

    --------- DCS Signals----------------------

    wen_dcs     : in  std_logic;
    addr_dcs    : in  std_logic_vector(15 downto 0);
    data_dcs    : in  std_logic_vector(31 downto 0);
    dat_out_dcs : out std_logic_vector(31 downto 0);

    ----------active front end card list------------

    fec_act_list : in std_logic_vector(31 downto 0);
    --  fec_rdo_list    : in std_logic_vector(31 downto 0);             
    rcu_version  : in std_logic_vector(31 downto 0);  -- RCU version
    err_reg_a    : in std_logic_vector(31 downto 0);
    err_reg_b    : in std_logic_vector(31 downto 0);

    --------- Baord Controller (Branch A) ---------

    sda_in_A      : in  std_logic;      -- FROM BOARD CONTROLLER 
    sda_out_A     : out std_logic;      -- TO BOARD CONTROLLER
    s_clk_A       : out std_logic;      -- I2C CLOCK TO BOARD CONTROLLER
    interruptA_in : in  std_logic;
    ------------------------------------

--              sm_act_list     : out std_logic_vector(31 downto 0);
--              sm_rdo_list     : out std_logic_vector(31 downto 0);
    warningTOdcs : out std_logic;

    -------------debugg signal simulator------------------



    --------- Baord Controller (Branch B) ---------
    interruptB_in : in  std_logic;
    sda_in_B      : in  std_logic;      -- FROM BOARD CONTROLLER 
    sda_out_B     : out std_logic;      -- TO BOARD CONTROLLER
    s_clk_B       : out std_logic       -- I2C CLOCK TO BOARD CONTROLLER

    );
end msm2_msmodule;

architecture ARCH_MSM of msm2_msmodule is
  
  signal set_act       : std_logic;
  signal set_rdo       : std_logic;
  signal exec          : std_logic;
  signal exec_A        : std_logic;
  signal exec_B        : std_logic;
  signal s_exec_A      : std_logic;
  signal s_exec_B      : std_logic;
  signal RDnRW         : std_logic;
  signal ld_regs       : std_logic;
  signal Branch        : std_logic;
  signal bcast         : std_logic;
  signal SELA_ADD_IH   : std_logic;
  signal SELB_ADD_IH   : std_logic;
  signal LD_REGS_A     : std_logic;
  signal LD_REGS_B     : std_logic;
  signal bc_din_B1     : std_logic_vector(7 downto 0);
  signal sda_inB_B1    : std_logic_vector(7 downto 0);
  signal sda_inB_B2    : std_logic_vector(7 downto 0);
  signal sda_inA_B1    : std_logic_vector(7 downto 0);
  signal sda_inA_B2    : std_logic_vector(7 downto 0);
  signal bc_din_B2     : std_logic_vector(7 downto 0);
  signal bc_reg_add    : std_logic_vector(7 downto 0);
  signal sda_in_B1     : std_logic_vector(7 downto 0);
  signal sda_in_B2     : std_logic_vector(7 downto 0);
  signal fec_add       : std_logic_vector(3 downto 0);
  signal FEC_ADD_A     : std_logic_vector(3 downto 0);
  signal FEC_ADD_B     : std_logic_vector(3 downto 0);
  signal FEC_ADDA_IH   : std_logic_vector(3 downto 0);
  signal FEC_ADDB_IH   : std_logic_vector(3 downto 0);
  signal FSM_STIH_A    : std_logic_vector(3 downto 0);
  signal FSM_STIH_B    : std_logic_vector(3 downto 0);
  signal errA_fec_ack  : std_logic;
  signal errA_ack_bcR  : std_logic;
  signal errA_din1_ack : std_logic;
  signal errA_din2_ack : std_logic;
  signal errB_fec_ack  : std_logic;
  signal errB_ack_bcR  : std_logic;
  signal errB_din1_ack : std_logic;
  signal errB_din2_ack : std_logic;
  signal SEL_DOUT      : std_logic;
  signal BSYA_TO_DCS   : std_logic;
  signal BSYB_TO_DCS   : std_logic;

  signal fsmA_state   : std_logic_vector(7 downto 0)   := (others => '0');
  signal fsmB_state   : std_logic_vector(7 downto 0)   := (others => '0');
  signal BCA_ADD_IH   : std_logic_vector(7 downto 0)   := (others => '0');
  signal BCB_ADD_IH   : std_logic_vector(7 downto 0)   := (others => '0');
  signal bcA_reg_add  : std_logic_vector(7 downto 0)   := (others => '0');
  signal bcB_reg_add  : std_logic_vector (7 downto 0)  := (others => '0');
  signal fsm_state    : std_logic_vector(15 downto 0)  := (others => '0');
  signal rslt_a       : std_logic_vector(15 downto 0)  := (others => '0');
  signal rslt_b       : std_logic_vector(15 downto 0)  := (others => '0');
  signal DOUT_DCS_A   : std_logic_vector(15 downto 0)  := (others => '0');
  signal DOUT_DCS_B   : std_logic_vector(15 downto 0)  := (others => '0');
  signal WARN_DCS_A   : std_logic                      := '0';
  signal LDA_ADD_IH   : std_logic                      := '0';
  signal LDB_ADD_IH   : std_logic                      := '0';
  signal WARN_DCS_B   : std_logic                      := '0';
  signal END_RD_B     : std_logic                      := '0';
  signal END_RD_A     : std_logic                      := '0';
  signal EXEC_IH_A    : std_logic                      := '0';
  signal EXEC_IH_B    : std_logic                      := '0';
  signal I2C_BSY_A    : std_logic                      := '0';
  signal I2C_BSY_B    : std_logic                      := '0';
  signal IHA_ACT_LIST : std_logic_vector(15 downto 0)  := (others => '0');
  signal IHB_ACT_LIST : std_logic_vector(15 downto 0)  := (others => '0');
  signal IHA_RDO_LIST : std_logic_vector(15 downto 0)  := (others => '0');
  signal IHB_RDO_LIST : std_logic_vector (15 downto 0) := (others => '0');
  signal bcast_dec    : std_logic                      := '0';
  signal RDnRW_dec    : std_logic                      := '0';
  signal sel_ih_ctr   : std_logic                      := '0';
  signal sv_rslt_A    : std_logic                      := '0';
  signal sv_rslt_B    : std_logic                      := '0';
  signal en_mem_b     : std_logic                      := '0';
  signal en_mem_a     : std_logic                      := '0';
  signal EN_REG       : std_logic                      := '0';
  signal s_exec       : std_logic                      := '0';
  signal m_interA_in  : std_logic                      := '0';
  signal m_interB_in  : std_logic                      := '0';
  signal s_en_int_a   : std_logic                      := '0';
  signal s_en_int_b   : std_logic                      := '0';
  signal dat_out_reg  : std_logic_vector (31 downto 0);
  signal dat_out_mem  : std_logic_vector (31 downto 0);
  signal confg_err    : std_logic_vector(7 downto 0)   := (others => '0');
  signal sm_wrd_BA    : std_logic_vector(9 downto 0)   := (others => '0');
  signal sm_wrds_A    : std_logic_vector(3 downto 0)   := (others => '0');
  signal sm_wrds_B    : std_logic_vector(3 downto 0)   := (others => '0');
  signal ex_ih_a      : std_logic                      := '0';
  signal ex_ih_b      : std_logic                      := '0';
  signal msk_dcs_accs : std_logic                      := '0';
  signal mem_b_rdy    : std_logic                      := '0';
  signal mem_a_rdy    : std_logic                      := '0';
  signal mem_rdy_a    : std_logic                      := '0';
  signal mem_rdy_b    : std_logic                      := '0';
  signal rst_rslt     : std_logic                      := '0';
  signal rst_mem      : std_logic                      := '0';
  signal fsm_st_msm   : std_logic_vector (4 downto 0);

begin

  --fsm_state    <= fsmB_state & fsmA_state;
  
  fsm_state <= fsmB_state & FSM_STIH_B & FSM_STIH_A;
  sm_wrd_BA <= mem_b_rdy & sm_wrds_B & mem_a_rdy & sm_wrds_A;

  m_interA_in <= s_en_int_a and interruptA_in;
  m_interB_in <= s_en_int_b and interruptB_in;

  -- <= WARN_DCS_A or WARN_DCS_B;

--      sm_act_list <= IHB_ACT_LIST & IHA_ACT_LIST;
--      sm_rdo_list <= IHB_RDO_LIST & IHA_RDO_LIST;

--      exec_A          <= s_exec_A ;  or EXEC_IH_A;
--      exec_B          <= s_exec_B ;  or  EXEC_IH_B;

  dat_out_dcs <= dat_out_reg          when en_reg = '1'   else
                 dat_out_mem;
  dat_out_mem <= X"0000" & DOUT_DCS_A when en_mem_a = '1' else
                 X"0000" & DOUT_DCS_B;

  ld_regs_A  <= LDA_ADD_IH or ld_regs;
  ld_regs_B  <= LDB_ADD_IH or ld_regs;
  sel_ih_ctr <= SELB_ADD_IH or SELA_ADD_IH;

--      fec_add_A   <= FEC_ADDA_IH when SELA_ADD_IH = '1' else fec_add;
--      fec_add_B   <= FEC_ADDB_IH when SELB_ADD_IH = '1' else fec_add;
--      bcA_reg_add <= BCA_ADD_IH when SELA_ADD_IH = '1' else bc_reg_add;
--      bcB_reg_add <= BCB_ADD_IH when SELB_ADD_IH = '1' else bc_reg_add;
--      RDnRW           <= '1' when sel_ih_ctr = '1' else RDnRW_dec ;
--      bcast           <= '0' when sel_ih_ctr = '1' else bcast_dec ;


  fec_add_A   <= fec_add;
  fec_add_B   <= fec_add;
  bcA_reg_add <= bc_reg_add;
  bcB_reg_add <= bc_reg_add;
  RDnRW       <= RDnRW_dec;
  bcast       <= bcast_dec;

  sv_rslt : process(rst, clk, sv_rslt_a, sv_rslt_b)
  begin
    if rst = '1' then
      rslt_a <= (others => '0');
      rslt_b <= (others => '0');
    elsif clk'event and clk = '1' then
      if sv_rslt_a = '1' then
        rslt_a <= sda_inA_B2 & sda_inA_B1;
      end if;
      if sv_rslt_b = '1' then
        rslt_b <= sda_inB_B2 & sda_inB_B1;
      end if;
    end if;
  end process;


  process (exec, Branch)
  begin
    case Branch is
      when '0' =>
        exec_A <= exec;
        exec_B <= '0';
      when '1' =>
        exec_A <= '0';
        exec_B <= exec;
      when others =>
        exec_A <= '0';
        exec_B <= '0';
    end case;
  end process;

  sda_in_B1 <= sda_inB_B1 when Branch = '1' else sda_inA_B1;
  sda_in_B2 <= sda_inB_B2 when Branch = '1' else sda_inA_B2;

--      
--MSM_STATE_MACHIN : entity work.FSM_MSM PORT MAP (
--
--                      rst                             => rst ,
--                      clk                             => clk, 
--                      int_in_A                        => m_interA_in,
--                      int_in_B                        => m_interB_in,
--                      bsy_ih_A                        => BSYA_TO_DCS,
--                      bsy_ih_B                        => BSYB_TO_DCS,
--                      ti_out_wt       => '0', -- to be replaced with counter
--              en_wt_cnt               => open,
--                      mem_a_bsy               => mem_a_rdy,
--                      mem_b_bsy               => mem_b_rdy,
--                      bsy_to_dcs              => msk_dcs_accs,
--                      warn_to_dcs             => warningTOdcs,
--                      exe_ih_a                        => ex_ih_a,
--                      exe_ih_b                        => ex_ih_b,
--                      mem_rdy_a               => mem_rdy_a,
--                      mem_rdy_b               => mem_rdy_b,
--                      fsm_st          => fsm_st_msm,
--                      err_msm                 => open
--                      
--                      );
  
  decoder : msm2_decoder
    port map(

    rst           => rst ,
    clk           => clk,
    wen_dcs       => wen_dcs,
    addr_dcs      => addr_dcs,
    data_dcs      => data_dcs,
    dat_out_dcs   => dat_out_reg,
    bc_din_B1     => sda_in_B1,
    bc_din_B2     => sda_in_B2,
    exec          => exec,
    rst_mem       => rst_mem,
    set_act       => set_act,
    set_rdo       => set_rdo,
    RDnRW         => RDnRW_dec,
    Branch        => Branch,
    rst_rslt      => rst_rslt,
    confg_err     => confg_err,
    fec_act_list  => fec_act_list, 
    err_reg_a     => err_reg_a,
    err_reg_b     => err_reg_b,
    interruptA_in => interruptA_in,
    interruptB_in => interruptB_in,
    RCU_version   => RCU_version,
    ld_regs       => ld_regs,
    bcast         => bcast_dec ,
    fec_add       => fec_add,
    en_mem_a      => en_mem_a,
    en_mem_b      => en_mem_b,
    en_int_a      => s_en_int_a,
    en_int_b      => s_en_int_b,
    en_reg        => en_reg,
    bc_reg_add    => bc_reg_add,
    B1_dout       => bc_din_B1,
    errA_din1_ack => errA_din1_ack,
    errA_din2_ack => errA_din2_ack,
    errA_fec_ack  => errA_fec_ack,
    errA_ack_bcR  => errA_ack_bcR,
    errB_din1_ack => errB_din1_ack,
    errB_din2_ack => errB_din2_ack,
    errB_fec_ack  => errB_fec_ack,
    errB_ack_bcR  => errB_ack_bcR,
    fsm_state     => fsm_state,
    fsm_st_msm    => fsm_st_msm,
    sm_wrd_BA     => sm_wrd_BA,
    B2_dout       => bc_din_B2
    );

  BC_interface_A : msm2_interface_I2C
    port map(

    rst          => rst,
    rst_rslt     => rst_rslt,
    clk          => clk,
    exec         => exec_A,
    rd_nwr       => RDnRW,
    ld_regs      => ld_regs_A,
    fec_add      => fec_add_A,
    bc_reg_add   => bcA_reg_add,
    B1_din       => bc_din_B1,
    B2_din       => bc_din_B2,
    sda_in_B1    => sda_inA_B1,
    sda_in_B2    => sda_inA_B2,
    sda_in       => sda_in_A,
    END_RD       => END_RD_A,
    sv_rslt      => sv_rslt_A,
    I2C_BSY      => I2C_BSY_A,
    bcast        => bcast_dec ,
    err_fec_ack  => errA_fec_ack,
    err_bc_add   => errA_ack_bcR,
    err_din1_ack => errA_din1_ack,
    err_din2_ack => errA_din2_ack,
    fsm_state    => fsmA_state,
    sda_out      => sda_out_A ,
    s_clk        => s_clk_A
    );


  BC_interface_B : msm2_interface_I2C
    port map(
      rst          => rst,
      rst_rslt     => rst_rslt,
      clk          => clk,
      exec         => exec_B,
      rd_nwr       => RDnRW,
      ld_regs      => ld_regs_B,
      fec_add      => fec_add_B,
      bc_reg_add   => bcB_reg_add,
      B1_din       => bc_din_B1,
      B2_din       => bc_din_B2,
      sda_in_B1    => sda_inB_B1,
      sda_in_B2    => sda_inB_B2,
      sda_in       => sda_in_B,
      bcast        => bcast_dec ,
      END_RD       => END_RD_B,
      sv_rslt      => sv_rslt_B,
      I2C_BSY      => I2C_BSY_B,
      err_fec_ack  => errB_fec_ack,
      err_bc_add   => errB_ack_bcR,
      err_din1_ack => errB_din1_ack,
      err_din2_ack => errB_din2_ack,
      fsm_state    => fsmB_state,
      sda_out      => sda_out_B ,
      s_clk        => s_clk_B
      );

--Inter_hnd_A : msm2_int_hanlder 
--              port map (                 
--                         CLK          => CLK,
--               RST          => RST,
--               INTR_IN      => ex_ih_a,
--               I2C_BSY      => I2C_BSY_A,
--               END_RD       => END_RD_A,
--                         en_mem       => en_mem_a,
--                         set_act      => set_act,
--                         set_rdo       => set_rdo,
--                         sv_fec_add   => sv_rslt_A,
--               ADDR_DCS     => ADDR_DCS,
--               RSLT_REG     => rslt_a,
--                         confg_err     => confg_err,
--               DOUT_DCS     => DOUT_DCS_A,
--               WARN_DCS     => WARN_DCS_A,
--               BSY_TO_DCS   => BSYA_TO_DCS,
--               SEL_ADD_IH   => SELA_ADD_IH,
--               EXEC_IH      => EXEC_IH_A,
--               LD_ADD_IH    => LDA_ADD_IH,
--               FEC_ADD_IH   => FEC_ADDA_IH,
--                         fec_rdo_list => fec_rdo_list(15 downto 0),
--               FSM_ST       => FSM_STIH_A,
--                         sm_wrds      => sm_wrds_A,
--                              fec_act_list    => fec_act_list(15 downto 0),
--                              ih_act_list     => ihA_act_list,
--                              ih_rdo_list  => ihA_rdo_list,
--               BC_ADD_IH    => BCA_ADD_IH);
--                         
--Inter_hnd_B : msm2_int_hanlder 
--              port map (
--                         
--                         CLK          => CLK,
--               RST          => RST,
--               INTR_IN      => ex_ih_b,
--               I2C_BSY      => I2C_BSY_B,
--               END_RD       => END_RD_B,
--                         en_mem       => en_mem_b,
 --                         set_act      => set_act,
--                         set_rdo       => set_rdo,
--                         sv_fec_add   => sv_rslt_B,
--               ADDR_DCS     => ADDR_DCS,
--               RSLT_REG     => rslt_b,
--                         confg_err     => confg_err,
--               DOUT_DCS     => DOUT_DCS_B,
--               WARN_DCS     => WARN_DCS_B,
--               BSY_TO_DCS   => BSYB_TO_DCS,
--               SEL_ADD_IH   => SELB_ADD_IH, 
--               EXEC_IH      => EXEC_IH_B,
--               LD_ADD_IH    => LDB_ADD_IH,
--               FEC_ADD_IH   => FEC_ADDB_IH,
--                         fec_rdo_list => fec_rdo_list(31 downto 16),
--               FSM_ST       => FSM_STIH_B,
--                         fec_act_list => fec_act_list(31 downto 16),
--                         sm_wrds      => sm_wrds_B,
--                         ih_act_list  => ihB_act_list,
--                         ih_rdo_list  => ihB_rdo_list,
--               BC_ADD_IH    => BCB_ADD_IH);

  
end ARCH_MSM;

-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm2_msmodule_pack is

  component msm2_msmodule
    port (
      rst           : in  std_logic;
      clk           : in  std_logic;
      wen_dcs       : in  std_logic;
      addr_dcs      : in  std_logic_vector(15 downto 0);
      data_dcs      : in  std_logic_vector(31 downto 0);
      dat_out_dcs   : out std_logic_vector(31 downto 0);
      fec_act_list  : in  std_logic_vector(31 downto 0);
      rcu_version   : in  std_logic_vector(31 downto 0);
      err_reg_a     : in  std_logic_vector(31 downto 0);
      err_reg_b     : in  std_logic_vector(31 downto 0);
      sda_in_A      : in  std_logic;
      sda_out_A     : out std_logic;
      s_clk_A       : out std_logic;
      interruptA_in : in  std_logic;
      warningTOdcs  : out std_logic;
      interruptB_in : in  std_logic;
      sda_in_B      : in  std_logic;
      sda_out_B     : out std_logic;
      s_clk_B       : out std_logic);
  end component;

end msm2_msmodule_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
