

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;
--use work.all;



entity msm2_fsm is
  
  port (
    rst         : in  std_logic;        -- RESET 
    clk         : in  std_logic;        -- CLOCK
    int_in_A    : in  std_logic;        -- INTERRUPT FROM BC A
    int_in_B    : in  std_logic;        -- INTERRUPT FROM BC B
    bsy_ih_A    : in  std_logic;        -- Interrupt handler A busy
    bsy_ih_B    : in  std_logic;        -- Interrupt handler B busy 
    mem_a_bsy   : in  std_logic;  -- Memory A is ready but not read but DCS
    mem_b_bsy   : in  std_logic;  -- Memory B is ready but not read but DCS
    ti_out_wt   : in  std_logic;        -- Maximum time for IH A & B is over
    en_wt_cnt   : out std_logic;        -- Enable wait counter for IH A & B
    bsy_to_dcs  : out std_logic;  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
    warn_to_dcs : out std_logic;        -- DCS WILL BE WARNED FRO INTERRUPT
    exe_ih_a    : out std_logic;        -- EXECUTE THE IH PROCESS ON Branch A
    exe_ih_b    : out std_logic;        -- EXECUTE THE IH PROCESS ON Branch B
    err_msm     : out std_logic;
    mem_rdy_a   : out std_logic;
    mem_rdy_b   : out std_logic;

    fsm_st : out std_logic_vector(4 downto 0)  -- FSM STATE

    );  

end msm2_fsm;

architecture AFSM_MSM of MSM2_fsm is
--      
--      type state is (idle,st_ih_a,st_ih_b,st_ih_ab,wt_ih_a,wt_ih_b,wt_ih_ab,wt_mem_a,wt_mem_b,wt_mem_ab,end_a,end_b,dcs_warn,set_err, finish);
--      signal pr_st,nxt_st: state;

  constant idle          : std_logic_vector (4 downto 0) := "00000";  --x 00; 
  constant st_ih_a       : std_logic_vector (4 downto 0) := "00001";  --x 01; 
  constant st_ih_b       : std_logic_vector (4 downto 0) := "00010";  --x 03; 
  constant st_ih_ab      : std_logic_vector (4 downto 0) := "00011";  --x 02; 
  constant wt_ih_a       : std_logic_vector (4 downto 0) := "00100";  --x 04; 
  constant wt_ih_b       : std_logic_vector (4 downto 0) := "00101";  --x 05; 
  constant wt_ih_ab      : std_logic_vector (4 downto 0) := "00110";  --x 07; 
  constant wt_mem_a      : std_logic_vector (4 downto 0) := "00111";  --x 06; 
  constant wt_mem_b      : std_logic_vector (4 downto 0) := "01000";  --x 0E; 
  constant wt_mem_ab     : std_logic_vector (4 downto 0) := "01001";  --x 0F; 
  constant end_a         : std_logic_vector (4 downto 0) := "01100";  --x 0B; 
  constant end_b         : std_logic_vector (4 downto 0) := "01101";  --x 0A; 
  constant dcs_warn      : std_logic_vector (4 downto 0) := "01110";  --x 00; 
  constant set_err       : std_logic_vector (4 downto 0) := "01111";  --x 00; 
  constant finish        : std_logic_vector (4 downto 0) := "10000";  --x 00; 
--
  signal   pr_st, nxt_st : std_logic_vector(4 downto 0);
  
begin

  fsm_st <= pr_st;
--fsM_st <= "00000";

  process(clk, rst)
  begin
    if(rst = '1') then
      pr_st <= idle;
    elsif(clk' event and clk = '1') then
      pr_st <= nxt_st;
    end if;
  end process;

  next_state : process (rst, pr_st, int_in_A, int_in_B, bsy_ih_A, bsy_ih_B, ti_out_wt, mem_a_bsy, mem_b_bsy)
  begin
    if rst = '1' then
      nxt_st <= idle;
    else
      case pr_st is
        
        when idle =>                    --default state 
          if int_in_A = '1' and int_in_B = '0' then
            
            nxt_st <= wt_mem_a;
            
          elsif int_in_A = '0' and int_in_B = '1' then
            
            nxt_st <= wt_mem_b;
            
          elsif int_in_A = '1' and int_in_B = '1' then
            
            nxt_st <= wt_mem_ab;
          else
            nxt_st <= idle;
          end if;
          
        when wt_mem_a =>
          if mem_a_bsy = '0' then
            nxt_st <= st_ih_a;
          else
            nxt_st <= wt_mem_a;
          end if;
          
        when wt_mem_b =>
          if mem_b_bsy = '0' then
            nxt_st <= st_ih_b;
          else
            nxt_st <= wt_mem_b;
          end if;
          
        when wt_mem_ab =>
          if mem_a_bsy = '0' and mem_b_bsy = '0' then
            nxt_st <= st_ih_ab;
          else
            nxt_st <= wt_mem_ab;
          end if;
          
        when st_ih_a =>
          
          nxt_st <= wt_ih_a;
          
        when st_ih_b =>
          
          nxt_st <= wt_ih_b;
          
        when st_ih_ab =>
          
          nxt_st <= wt_ih_ab;
          
        when wt_ih_a =>
          
          if bsy_ih_A = '1' then
            
            nxt_st <= wt_ih_a;
            
          else
            nxt_st <= end_a;
            
          end if;
          
        when end_a =>
          nxt_st <= dcs_warn;
        when end_b =>
          nxt_st <= dcs_warn;
        when wt_ih_b =>
          
          if bsy_ih_B = '1' then        -- and int_in_A ='1' then
            
            nxt_st <= wt_ih_b;

                                        -- elsif bsy_ih_B = '0' and int_in_A ='1' then

                                        --      nxt_st <= st_ih_a2;
            
          else
            nxt_st <= end_b;
            
          end if;
          
        when wt_ih_ab =>
          
          if ti_out_wt = '0' then
            if bsy_ih_A = '0' and int_in_B = '0' then
              nxt_st <= dcs_warn;
            else
              nxt_st <= wt_ih_ab;
            end if;
          else
            nxt_st <= set_err;
          end if;
          
        when set_err =>
          
          nxt_st <= dcs_warn;
          
        when dcs_warn =>
          
          nxt_st <= finish;
          
        when finish =>
          
          nxt_st <= idle;
          
        when others =>
          nxt_st <= idle;
      end case;
    end if;
  end process;



  --------------------------------------------
  output_stage : process (rst, clk)
  begin
    if rst = '1' then
      
      en_wt_cnt   <= '0';
      bsy_to_dcs  <= '0';
      warn_to_dcs <= '0';
      exe_ih_a    <= '0';
      exe_ih_b    <= '0';
      err_msm     <= '0';
      mem_rdy_a   <= '0';
      mem_rdy_b   <= '0';
      
      
    elsif(clk' event and clk = '1') then
      
      case nxt_st is
        
        when idle =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '0';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when st_ih_a =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '1';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when st_ih_b =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '1';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when st_ih_ab =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '1';
          exe_ih_b    <= '1';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when wt_ih_a =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when wt_ih_b =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when end_a =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '1';
          mem_rdy_b   <= '0';
          
        when end_b =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '1';
          
        when wt_ih_ab =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when dcs_warn =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '0';
          warn_to_dcs <= '1';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when set_err =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
        when wt_mem_a =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '0';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
        when wt_mem_b =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '0';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
        when wt_mem_ab =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '1';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
        when finish =>
          
          en_wt_cnt   <= '0';
          bsy_to_dcs  <= '0';
          warn_to_dcs <= '0';
          exe_ih_a    <= '0';
          exe_ih_b    <= '0';
          err_msm     <= '0';
          mem_rdy_a   <= '0';
          mem_rdy_b   <= '0';
          
          
        when others =>
      end case;
    end if;
  end process;
  
end;

-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_fsm_pack is

  component msm2_fsm
    port (
      rst         : in  std_logic;
      clk         : in  std_logic;
      int_in_A    : in  std_logic;
      int_in_B    : in  std_logic;
      bsy_ih_A    : in  std_logic;
      bsy_ih_B    : in  std_logic;
      mem_a_bsy   : in  std_logic;
      mem_b_bsy   : in  std_logic;
      ti_out_wt   : in  std_logic;
      en_wt_cnt   : out std_logic;
      bsy_to_dcs  : out std_logic;
      warn_to_dcs : out std_logic;
      exe_ih_a    : out std_logic;
      exe_ih_b    : out std_logic;
      err_msm     : out std_logic;
      mem_rdy_a   : out std_logic;
      mem_rdy_b   : out std_logic;
      fsm_st      : out std_logic_vector(4 downto 0));
  end component;

end msm2_fsm_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
