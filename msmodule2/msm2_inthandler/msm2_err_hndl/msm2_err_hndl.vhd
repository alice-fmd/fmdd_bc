
-- Rmoved rdo err0r so that soft error wont modify rdo-fec list.25/08/09 dafter discussion with luciano.


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;

entity msm2_err_hndl is
  port (

    clk         : in  std_logic;
    rst         : in  std_logic;
    safty_en    : in  std_logic;
    set_act     : in  std_logic;
    set_rdo     : in  std_logic;
    reslt_reg   : in  std_logic_vector (15 downto 0);
    fec_add     : in  std_logic_vector (3 downto 0);
    confg_err   : in  std_logic_vector (7 downto 0);
    mem_din     : out std_logic_vector (15 downto 0);
    ih_act_list : out std_logic_vector (15 downto 0);
    ih_rdo_list : out std_logic_vector (15 downto 0)

    );

end msm2_err_hndl;

architecture arch_err_hndlr of msm2_err_hndl is
  
  signal soft_err     : std_logic                      := '0';
  signal hard_err     : std_logic                      := '0';
  signal fect_act     : std_logic                      := '0';
  signal reg_rdo_list : std_logic_vector(15 downto 0)  := X"FFFF";
  signal reg_act_list : std_logic_vector (15 downto 0) := X"FFFF";
  signal sft_err0     : std_logic                      := '0';
  signal sft_err1     : std_logic                      := '0';
  signal sft_err2     : std_logic                      := '0';
  signal sft_err3     : std_logic                      := '0';
  signal sft_err4     : std_logic                      := '0';
  signal sft_err5     : std_logic                      := '0';
  signal sft_err6     : std_logic                      := '0';
  signal sft_err7     : std_logic                      := '0';
  signal hrd_err0     : std_logic                      := '0';
  signal hrd_err1     : std_logic                      := '0';
  signal hrd_err2     : std_logic                      := '0';
  signal hrd_err3     : std_logic                      := '0';
  signal hrd_err4     : std_logic                      := '0';
  signal hrd_err5     : std_logic                      := '0';
  signal hrd_err6     : std_logic                      := '0';
  signal hrd_err7     : std_logic                      := '0';
  signal tmp_ov_th    : std_logic                      := '0';
  signal av_un_th     : std_logic                      := '0';
  signal ac_ov_th     : std_logic                      := '0';
  signal dv_un_th     : std_logic                      := '0';
  signal dc_ov_th     : std_logic                      := '0';
  signal paps_err     : std_logic                      := '0';
  signal alps_err     : std_logic                      := '0';
  signal misd_sclk    : std_logic                      := '0';


  -- purpose: convert std_logic_Vector to interger
  function conv_integer (
    constant input : std_logic_vector)
    return integer is
  begin  -- conv_integer
    return to_integer(unsigned(input));
  end conv_integer;

begin
  
  tmp_ov_th <= reslt_reg(0);
  av_un_th  <= reslt_reg(1);
  ac_ov_th  <= reslt_reg(2);
  dv_un_th  <= reslt_reg(3);
  dc_ov_th  <= reslt_reg(4);
  paps_err  <= reslt_reg(5);
  alps_err  <= reslt_reg(6);
  misd_sclk <= reslt_reg(7);

  sft_err0 <= tmp_ov_th and not (confg_err(0));
  sft_err1 <= av_un_th and not (confg_err(1));
  sft_err2 <= ac_ov_th and not (confg_err(2));
  sft_err3 <= dv_un_th and not (confg_err(3));
  sft_err4 <= dc_ov_th and not (confg_err(4));
  sft_err5 <= paps_err and not (confg_err(5));
  sft_err6 <= alps_err and not (confg_err(6));
  sft_err7 <= misd_sclk and not (confg_err(7));

  hrd_err0 <= tmp_ov_th and confg_err(0);
  hrd_err1 <= av_un_th and confg_err(1);
  hrd_err2 <= ac_ov_th and confg_err(2);
  hrd_err3 <= dv_un_th and confg_err(3);
  hrd_err4 <= dc_ov_th and confg_err(4);
  hrd_err5 <= paps_err and confg_err(5);
  hrd_err6 <= alps_err and confg_err(6);
  hrd_err7 <= misd_sclk and confg_err(7);

  soft_err <= sft_err0 or sft_err1 or sft_err2 or sft_err3 or sft_err4 or sft_err5 or sft_err6 or sft_err7;
  hard_err <= hrd_err0 or hrd_err1 or hrd_err2 or hrd_err3 or hrd_err4 or hrd_err5 or hrd_err6 or hrd_err7;

  mem_din     <= fec_add & reslt_reg(11 downto 0);
  ih_act_list <= reg_act_list;
  ih_rdo_list <= reg_rdo_list;

  process (set_act, clk, hard_err, fec_add, safty_en, reslt_reg)
  begin
    if set_act = '1' then
      reg_act_list <= X"FFFF";
    elsif clk'event and clk = '1' then
      if hard_err = '1' and safty_en = '1' then
        reg_act_list(conv_integer(fec_add(3 downto 0))) <= '0';
      end if;
    end if;
  end process;

  process (clk, soft_err, hard_err, fec_add, safty_en, reslt_reg)
  begin
    if set_rdo = '1' then
      reg_rdo_list <= X"FFFF";
    elsif clk'event and clk = '1' then
      if hard_err = '1' and safty_en = '1' then
        reg_rdo_list(conv_integer(fec_add(3 downto 0))) <= '0';
      end if;
    end if;
    
  end process;
  
end arch_err_hndlr;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_err_hndl_pack is

  component msm2_err_hndl
    port (
      clk         : in  std_logic;
      rst         : in  std_logic;
      safty_en    : in  std_logic;
      set_act     : in  std_logic;
      set_rdo     : in  std_logic;
      reslt_reg   : in  std_logic_vector (15 downto 0);
      fec_add     : in  std_logic_vector (3 downto 0);
      confg_err   : in  std_logic_vector (7 downto 0);
      mem_din     : out std_logic_vector (15 downto 0);
      ih_act_list : out std_logic_vector (15 downto 0);
      ih_rdo_list : out std_logic_vector (15 downto 0));
  end component;

end msm2_err_hndl_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
