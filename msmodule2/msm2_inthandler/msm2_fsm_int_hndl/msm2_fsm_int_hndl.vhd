

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;
--use work.all;



entity msm2_FSM_INT_HNDL is
  
  port (
    rst         : in  std_logic;        -- RESET 
    clk         : in  std_logic;        -- CLOCK
    int_in      : in  std_logic;        -- INTERRUPT FROM BC
    lst_fec     : in  std_logic;        -- LAST FEC WAS READ
    fec_act     : in  std_logic;  -- FRONT END CARD IS ACTIVE , IT MUST BE CHECKED BEFORE INTERRUPT HANDLER TRY TO READ
    i2c_bsy     : in  std_logic;        -- I2C Module is Busy for read/write 
    end_rd      : in  std_logic;        -- END OF THE ADDRESSES FEC
    en_cnt      : out std_logic;  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
    clr_fec_cnt : out std_logic;
    wen_st_mem  : out std_logic;        -- WRITE ENABLE FOR STATUS MEMORY
    bsy_to_dcs  : out std_logic;  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
    warn_to_dcs : out std_logic;        -- DCS WILL BE WARNED FRO INTERRUPT
    exe_rd      : out std_logic;        -- EXECUTE THE READ PROCESS ON FEC
    sel_add_ih  : out std_logic;  -- SELECT THE INTERRUPT HANDLER ADDRESS FOR FEC
    ld_add_ih   : out std_logic;  -- SELECT THE INTERRUPT HANDLER ADDRESS FOR FEC
    en_mem_cnt  : out std_logic;
    sv_mem_cnt  : out std_logic;
    fsm_st      : out std_logic_vector(3 downto 0)  -- FSM STATE

    );

end msm2_FSM_INT_HNDL;

architecture AFSM_INT_HNDL of msm2_FSM_INT_HNDL is
  
  constant idle          : std_logic_vector(3 downto 0) := "0000";
  constant ld_add        : std_logic_vector(3 downto 0) := "0001";
  constant ex_rd         : std_logic_vector(3 downto 0) := "0011";
  constant ex_rd_2       : std_logic_vector(3 downto 0) := "0100";
  constant ex_rd_3       : std_logic_vector(3 downto 0) := "0101";
  constant sv_dat1       : std_logic_vector(3 downto 0) := "0110";
  constant sv_dat2       : std_logic_vector(3 downto 0) := "0111";
  constant chk_lst_fec   : std_logic_vector(3 downto 0) := "1000";
  constant nxt_fec       : std_logic_vector(3 downto 0) := "1001";
  constant wrn_dcs1      : std_logic_vector(3 downto 0) := "1010";
  constant wrn_dcs       : std_logic_vector(3 downto 0) := "1011";
  constant finish        : std_logic_vector(3 downto 0) := "1100";
  constant chk_bsy       : std_logic_vector(3 downto 0) := "1101";
  signal   pr_st, nxt_st : std_logic_vector(3 downto 0);

--      type state is (idle,ld_add,ex_rd,ex_rd_2,ex_rd_3,sv_dat1,sv_dat2,chk_lst_fec,nxt_fec,wrn_dcs,wrn_dcs1,finish,chk_bsy);
--      signal pr_st,nxt_st: state;     
  
begin

  fsm_st <= pr_st;

  process(clk, rst)
  begin
    if(rst = '1') then
      pr_st <= idle;
    elsif(clk' event and clk = '1') then
      pr_st <= nxt_st;
    end if;
  end process;

  next_state : process (rst, pr_st, int_in, lst_fec, end_rd, fec_act, i2c_bsy)
  begin
    if rst = '1' then
      nxt_st <= idle;
    else
      case pr_st is
        
        when idle =>                    --default state 
          if int_in = '1' then
            nxt_st <= chk_bsy;
          else
            nxt_st <= idle;
          end if;
          
        when chk_bsy =>
          
          if i2c_bsy = '0' then
            nxt_st <= ld_add;
          else
            nxt_st <= chk_bsy;
          end if;
          
        when ld_add =>
          
          if fec_act = '1' then
            nxt_st <= ex_rd;
          else
            nxt_st <= chk_lst_fec;
          end if;
          
        when ex_rd =>
          nxt_st <= ex_rd_2;
          
        when ex_rd_2 =>
          nxt_st <= ex_rd_3;
          
        when ex_rd_3 =>
          
          if end_rd = '0' then
            nxt_st <= ex_rd_3;
          else
            nxt_st <= sv_dat1;
          end if;
          
        when sv_dat1 =>
          
          nxt_st <= sv_dat2;
          
        when sv_dat2 =>
          
          nxt_st <= chk_lst_fec;
          
        when chk_lst_fec =>
          
          if lst_fec = '0' then
            
            nxt_st <= nxt_fec;
            
          else
            
            nxt_st <= wrn_dcs;
            
          end if;
          
        when nxt_fec =>
          
          nxt_st <= ld_add;
          
        when wrn_dcs =>
          
          nxt_st <= wrn_dcs1;
          
        when wrn_dcs1 =>
          
          nxt_st <= finish;
          
        when finish =>
          
          nxt_st <= idle;
          
        when others =>
          nxt_st <= idle;
      end case;
    end if;
  end process;



  --------------------------------------------
  output_stage : process (rst, clk)
  begin
    if rst = '1' then
      
      en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
      wen_st_mem  <= '0';               -- WRITE ENABLE FOR STATUS MEMORY
      bsy_to_dcs  <= '0';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
      warn_to_dcs <= '0';               -- DCS WILL BE WARNED FRO INTERRUPT
      exe_rd      <= '0';               -- EXECUTE THE READ PROCESS ON FEC
      sel_add_ih  <= '0';
      ld_add_ih   <= '0';
      en_mem_cnt  <= '0';
      sv_mem_cnt  <= '0';
      clr_fec_cnt <= '0';
      
    elsif(clk' event and clk = '1') then
      
      case nxt_st is
        
        when idle =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '0';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                              
          sel_add_ih  <= '0';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
        when chk_bsy =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '0';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
          
        when ld_add =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  --                                                      
          sel_add_ih  <= '1';
          ld_add_ih   <= '1';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
        when nxt_fec =>

          en_cnt      <= '1';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '1';
          ld_add_ih   <= '1';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
          
        when ex_rd =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '1';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '1';
          ld_add_ih   <= '1';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
        when ex_rd_2 =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '1';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '1';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
        when ex_rd_3 =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '1';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
        when sv_dat1 =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '1';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '0';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
        when sv_dat2 =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '0';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '1';
          sv_mem_cnt  <= '1';
          clr_fec_cnt <= '0';
          
        when chk_lst_fec =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '1';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '1';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
        when wrn_dcs =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '0';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '1';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '0';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '1';
          
        when wrn_dcs1 =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '0';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '1';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '0';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '0';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '1';
          
        when finish =>

          en_cnt      <= '0';  -- ENABLE FEC ADD COUNTER TO STORE DATA FROM BC
          wen_st_mem  <= '0';           -- WRITE ENABLE FOR STATUS MEMORY
          bsy_to_dcs  <= '0';  -- DCS WILL NOT BE ABLE TO READ/WRITE WHILE INTERRUPT HANDLER IS ACTIVE
          warn_to_dcs <= '0';           -- DCS WILL BE WARNED FRO INTERRUPT
          exe_rd      <= '0';  -- EXECUTE THE READ PROCESS ON FEC                                                                      
          sel_add_ih  <= '0';
          ld_add_ih   <= '0';
          en_mem_cnt  <= '1';
          sv_mem_cnt  <= '0';
          clr_fec_cnt <= '0';
          
        when others =>
      end case;
    end if;
  end process;
  
end;

-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_FSM_INT_HNDL_pack is

  component msm2_FSM_INT_HNDL
    port (
      rst         : in  std_logic;
      clk         : in  std_logic;
      int_in      : in  std_logic;
      lst_fec     : in  std_logic;
      fec_act     : in  std_logic;
      i2c_bsy     : in  std_logic;
      end_rd      : in  std_logic;
      en_cnt      : out std_logic;
      clr_fec_cnt : out std_logic;
      wen_st_mem  : out std_logic;
      bsy_to_dcs  : out std_logic;
      warn_to_dcs : out std_logic;
      exe_rd      : out std_logic;
      sel_add_ih  : out std_logic;
      ld_add_ih   : out std_logic;
      en_mem_cnt  : out std_logic;
      sv_mem_cnt  : out std_logic;
      fsm_st      : out std_logic_vector(3 downto 0));
  end component;

end msm2_FSM_INT_HNDL_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
