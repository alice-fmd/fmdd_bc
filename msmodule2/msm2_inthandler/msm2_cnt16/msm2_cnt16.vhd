library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;


entity msm2_CNT16 is
  port (
    clk : in std_logic;
    rst : in std_logic;
    en  : in std_logic;
    clr : in std_logic;

    cnt_out : out std_logic_vector (3 downto 0)
    );
end msm2_CNT16;

architecture A_CNT16 of msm2_CNT16 is
  subtype cnt_t is integer range 0 to 15;
  signal cnt : cnt_t := 0;
  -- signal cnt : std_logic_vector (3 downto 0);
  
begin
  cnt_out <= std_logic_vector(to_unsigned(cnt,4));

  process(rst, clr, clk)
  begin
    if rst = '1' or clr = '1' then
      cnt <= 0; -- (others => '0');
    elsif (clk' event and clk = '1') then
      if (en = '1') then
        cnt <= cnt+1;
      end if;
    end if;
  end process;
end;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_CNT16_pack is

  component msm2_CNT16
    port (
      clk     : in  std_logic;
      rst     : in  std_logic;
      en      : in  std_logic;
      clr     : in  std_logic;
      cnt_out : out std_logic_vector (3 downto 0));
  end component;

end msm2_CNT16_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
