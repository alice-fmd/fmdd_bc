---------------------------------------------------------------------------------------------------
-- DESIGN RCU FIRM WARE V.2.0
-- MODULE NAME   : MSM INTERRUPT HANDLER.VHD
--
-------------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;

-- library WORK;
-- use work.rcu_addr_set.all;
library msmodule2_lib;
use msmodule2_lib.msm2_cnt16_pack.all;
use msmodule2_lib.msm2_cnt_fec_pack.all;
use msmodule2_lib.msm2_err_hndl_pack.all;
use msmodule2_lib.msm2_fsm_int_hndl_pack.all;


entity msm2_inthandler is
  
  port (

    clk          : in  std_logic;
    rst          : in  std_logic;
    intr_in      : in  std_logic;
    i2c_bsy      : in  std_logic;
    end_rd       : in  std_logic;
    en_mem       : in  std_logic;
    set_act      : in  std_logic;
    set_rdo      : in  std_logic;
    sv_fec_add   : in  std_logic;
    confg_err    : in  std_logic_vector (7 downto 0);
    addr_dcs     : in  std_logic_vector (15 downto 0);
    rslt_reg     : in  std_logic_vector (15 downto 0);
    dout_dcs     : out std_logic_vector(15 downto 0);
    fec_act_list : in  std_logic_vector (15 downto 0);
    fec_rdo_list : in  std_logic_vector (15 downto 0);
    ih_act_list  : out std_logic_vector(15 downto 0);
    ih_rdo_list  : out std_logic_vector(15 downto 0);
    warn_dcs     : out std_logic;
    bsy_to_dcs   : out std_logic;
    sel_add_ih   : out std_logic;
    exec_ih      : out std_logic;
    ld_add_ih    : out std_logic;
    fec_add_ih   : out std_logic_vector (3 downto 0);
    sm_wrds      : out std_logic_vector (3 downto 0);
    fsm_st       : out std_logic_vector(3 downto 0);  -- FSM STATE
    bc_add_ih    : out std_logic_vector (7 downto 0)

    );
end msm2_inthandler;

architecture ARCH_int_hanlder of msm2_inthandler is

  signal lst_fec , en_fec_cnt : std_logic := '0';
  signal fec_act              : std_logic := '0';
  signal en_mem_cnt           : std_logic := '0';
  signal wen_st_mem           : std_logic := '0';
  signal clr_fec_cnt          : std_logic := '0';
  signal clr_mem_cnt          : std_logic := '0';
  signal s_bsy_to_dcs         : std_logic := '0';
  signal branch               : std_logic := '0';
  signal en_stmem             : std_logic := '0';
  signal sv_mem_cnt           : std_logic := '0';
  signal mem_cnt_out          : std_logic_vector(3 downto 0);
  signal mem_add              : std_logic_vector (3 downto 0);
  signal s_add_ih             : std_logic_vector (3 downto 0);
  signal fec_add_err          : std_logic_vector (3 downto 0);
  signal st_mem_din           : std_logic_vector (15 downto 0);
  signal ihm_act_list         : std_logic_vector(15 downto 0);
  signal ih_rdo               : std_logic_vector (15 downto 0);

    -- purpose: convert std_logic_Vector to interger
  function conv_integer (
    constant input : std_logic_vector)
    return integer is
  begin  -- conv_integer
    return to_integer(unsigned(input));
  end conv_integer;

begin

  fec_add_ih <= s_add_ih(3 downto 0);
  bc_add_ih  <= X"12";
  lst_fec    <= '1' when s_add_ih = X"F" else '0';
  bsy_to_dcs <= s_bsy_to_dcs;

  en_stmem <= en_mem or wen_st_mem;
  mem_add  <= addr_dcs(3 downto 0) when en_mem = '1' else mem_cnt_out;

  ih_act_list <= ihm_act_list and fec_act_list;

  ih_rdo_list <= ih_rdo and fec_rdo_list;

  fec_act <= fec_act_list(conv_integer(s_add_ih(3 downto 0)));

  FEC_ADD_REG : process (rst, clk, sv_fec_add, s_add_ih)
  begin
    if rst = '1' then
      fec_add_err <= (others => '0');
    elsif clk'event and clk = '1' then
      if sv_fec_add = '1' then
        fec_add_err <= s_add_ih;
      end if;
    end if;
  end process;


  NO_WRDS_SM : process (rst, clk, sv_mem_cnt)
  begin
    if rst = '1' then
      sm_wrds <= (others => '0');
    elsif clk'event and clk = '1' then
      if sv_mem_cnt = '1' then
        sm_wrds <= mem_cnt_out;
      end if;
    end if;
  end process;

  FSM_INERTPT : msm2_fsm_int_hndl
    
    port map(
      rst         => rst,
      clk         => clk,
      int_in      => intr_in,
      lst_fec     => lst_fec,
      fec_act     => fec_act,
      i2c_bsy     => i2c_bsy,
      end_rd      => end_rd,
      en_cnt      => en_fec_cnt,
      clr_fec_cnt => clr_fec_cnt,
      wen_st_mem  => wen_st_mem,
      bsy_to_dcs  => s_bsy_to_dcs,
      warn_to_dcs => warn_dcs,
      exe_rd      => exec_ih,
      sel_add_ih  => sel_add_ih,
      ld_add_ih   => ld_add_ih,
      en_mem_cnt  => en_mem_cnt,
      sv_mem_cnt  => sv_mem_cnt,
      fsm_st      => fsm_st
      );

  fec_add_cnt : msm2_cnt_fec
    port map (
      clk     => clk,
      rst     => rst,
      en      => en_fec_cnt,
      clr     => clr_fec_cnt,
      cnt_out => s_add_ih
      );


--STATUS_MEMORY : msm2_st_mem 
--      port map (
--      addr  => mem_add,
--      clk       => clk,
--      en        => en_stmem,  
--      din       => st_mem_din,
--      dout  => dout_dcs,
--      we        => wen_st_mem
--);

  Error_hndler : msm2_err_hndl
    port map (
      reslt_reg   => rslt_reg,
      fec_add     => fec_add_err,
      confg_err   => confg_err,
      mem_din     => st_mem_din,
      set_act     => set_act,
      set_rdo     => set_rdo,
      ih_act_list => ihm_act_list,
      ih_rdo_list => ih_rdo,
      safty_en    => s_bsy_to_dcs,
      clk         => clk,
      rst         => rst
      );


  mem_add_cnt : msm2_cnt16
    port map(
      clk     => clk,
      rst     => rst,
      en      => en_mem_cnt,            --en_fec_cnt,
      clr     => clr_mem_cnt,
      cnt_out => mem_cnt_out
      );


end ARCH_int_hanlder;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_inthandler_pack is

  component msm2_inthandler
    port (
      clk          : in  std_logic;
      rst          : in  std_logic;
      intr_in      : in  std_logic;
      i2c_bsy      : in  std_logic;
      end_rd       : in  std_logic;
      en_mem       : in  std_logic;
      set_act      : in  std_logic;
      set_rdo      : in  std_logic;
      sv_fec_add   : in  std_logic;
      confg_err    : in  std_logic_vector (7 downto 0);
      addr_dcs     : in  std_logic_vector (15 downto 0);
      rslt_reg     : in  std_logic_vector (15 downto 0);
      dout_dcs     : out std_logic_vector(15 downto 0);
      fec_act_list : in  std_logic_vector (15 downto 0);
      fec_rdo_list : in  std_logic_vector (15 downto 0);
      ih_act_list  : out std_logic_vector(15 downto 0);
      ih_rdo_list  : out std_logic_vector(15 downto 0);
      warn_dcs     : out std_logic;
      bsy_to_dcs   : out std_logic;
      sel_add_ih   : out std_logic;
      exec_ih      : out std_logic;
      ld_add_ih    : out std_logic;
      fec_add_ih   : out std_logic_vector (3 downto 0);
      sm_wrds      : out std_logic_vector (3 downto 0);
      fsm_st       : out std_logic_vector(3 downto 0);
      bc_add_ih    : out std_logic_vector (7 downto 0));
  end component;

end msm2_inthandler_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
