
-- DESIGN RCU FIRM WARE V.2.0
-- MODULE NAME   : MSM_DECODER.VHD
--
-------------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;

library msmodule2_lib;
use msmodule2_lib.msm2_regs_pack.all;
use msmodule2_lib.msm2_cmd_dec_pack.all;
use msmodule2_lib.msm2_exec_stretch_pack.all;

-- library WORK;
-- use work.rcu_addr_set.all;

entity MSM2_DECODER is
  
  port(
    rst : in std_logic;
    clk : in std_logic;                 -- RCU CLOCK.

    --------- DCS Signals----------------------
    wen_dcs     : in  std_logic;
    addr_dcs    : in  std_logic_vector(15 downto 0);
    data_dcs    : in  std_logic_vector(31 downto 0);
    dat_out_dcs : out std_logic_vector(31 downto 0);

    --------- Baord Controller Data in---------

    bc_din_B1 : in std_logic_vector(7 downto 0);  -- first  byte FROM I2C interface Block
    bc_din_B2 : in std_logic_vector(7 downto 0);  -- second byte FROM I2C interface Block

    ----------status of interrupt line every clock cycle--------

    interruptA_in : in std_logic;
    interruptB_in : in std_logic;

    ----------active front end card list------------

    fec_act_list  : in  std_logic_vector(31 downto 0);
    rcu_version   : in  std_logic_vector(31 downto 0);
    confg_err     : out std_logic_vector(7 downto 0);
    -------------------------------------------------------
    err_reg_a     : in  std_logic_vector(31 downto 0);
    err_reg_b     : in  std_logic_vector(31 downto 0);
    ---------------------------------------------------------
    sm_wrd_BA     : in  std_logic_vector (9 downto 0);
    fsm_st_msm    : in  std_logic_vector (4 downto 0);
    en_mem_a      : out std_logic;
    en_mem_b      : out std_logic;
    en_reg        : out std_logic;
    en_int_a      : out std_logic;
    en_int_b      : out std_logic;
    --- DEBUG SIGNAL TO REGISTER-----
    errA_fec_ack  : in  std_logic;
    errA_ack_bcR  : in  std_logic;
    errA_din1_ack : in  std_logic;
    errA_din2_ack : in  std_logic;
    errB_fec_ack  : in  std_logic;
    errB_ack_bcR  : in  std_logic;
    errB_din1_ack : in  std_logic;
    errB_din2_ack : in  std_logic;
    fsm_state     : in  std_logic_vector(15 downto 0);
    bcast         : out std_logic;
    exec          : out std_logic;      -- START EXECUTE I2C Block
    rst_rslt      : out std_logic;
    set_act       : out std_logic;
    set_rdo       : out std_logic;
    rst_mem       : out std_logic;
    RDnRW         : out std_logic;      -- READ OR WRITE TRANSACTION 
    Branch        : out std_logic;      -- BRANCH BIT IN THE ADDRESS FROM DCS
    ld_regs       : out std_logic;      -- SAVE ADD/DATA FOR TRANSACTION 
    fec_add       : out std_logic_vector(3 downto 0);
    bc_reg_add    : out std_logic_vector(7 downto 0);
    B1_dout       : out std_logic_vector(7 downto 0);
    B2_dout       : out std_logic_vector(7 downto 0)

    );
end MSM2_DECODER;

architecture A_MSM_DECODER of MSM2_DECODER is
  
  signal en_cmd     : std_logic := '0';
  signal s_en_reg   : std_logic := '0';
  signal ld_reg     : std_logic := '0';
  signal s_exec     : std_logic := '0';
  signal rst_err    : std_logic := '0';
  signal cmd_vector : std_logic_vector (3 downto 0);

  constant MSM_REG : std_logic_vector(11 downto 0) := X"800";
  constant MSM_CMD : std_logic_vector(11 downto 0) := X"801";
  constant MSM_MEM_A : std_logic_vector(11 downto 0) := X"810";
  constant MSM_MEM_B : std_logic_vector(11 downto 0) := X"811";
begin
  exec     <= s_exec;
  s_en_reg <= cmd_vector(0);
  en_cmd   <= cmd_vector(1);
  en_reg   <= s_en_reg;
  en_mem_a <= cmd_vector(2);
  en_mem_b <= cmd_vector(3);
  ld_reg   <= s_en_reg and wen_dcs;

  COMMAND_DEC :  MSM2_CMD_DEC port map(
    rst      => rst,
    clk      => clk,
    wen_dcs  => wen_dcs,
    addr_dcs => addr_dcs(3 downto 0),
    data_dcs => data_dcs,
    en_cmd   => en_cmd,
    exec     => s_exec,
    rst_mem  => rst_mem,
    set_act  => set_act,
    set_rdo  => set_rdo,
    rst_err  => rst_err,
    rst_rslt => rst_rslt
    );

  REGISTRS : MSM2_REGS port map(
    rst           => rst,
    clk           => clk,
    en_reg        => s_en_reg,
    clr_err       => rst_err,
    --------- DCS Signals-------------------------------
    wen_dcs       => wen_dcs,
    addr_dcs      => addr_dcs(3 downto 0),
    data_dcs      => data_dcs,
    dat_out_dcs   => dat_out_dcs,
    en_int_a      => en_int_a,
    en_int_b      => en_int_b,
    ----------------------------------------------------
    fec_act_list  => fec_act_list,
    RCU_version   => RCU_version, 
    err_reg_a     => err_reg_a,
    err_reg_b     => err_reg_b,
    fsm_state     => fsm_state,
    fsm_st_msm    => fsm_st_msm,
    errA_din1_ack => errA_din1_ack,
    errA_din2_ack => errA_din2_ack,
    errA_fec_ack  => errA_fec_ack,
    errA_ack_bcR  => errA_ack_bcR,
    errB_din1_ack => errB_din1_ack,
    errB_din2_ack => errB_din2_ack,
    errB_fec_ack  => errB_fec_ack,
    errB_ack_bcR  => errB_ack_bcR,
    bcast         => bcast ,
    rdnwr         => RDnRW,
    Branch        => Branch,
    fec_add       => fec_add,
    bc_reg_add    => bc_reg_add,
    B1_dout       => B1_dout,
    B2_dout       => B2_dout,
    confg_err     => confg_err,
    sm_wrd_BA     => sm_wrd_BA,
    ------- From Baord Controller-----------------------
    bc_din_B1     => bc_din_B1,         -- first  byte from I2C interface Block
    bc_din_B2     => bc_din_B2,         -- second byte from I2C interface Block
    interruptA_in => interruptA_in,
    interruptB_in => interruptB_in
    ----------------------------------------------------
    );

  Stretch_EXEC : msm2_exec_stretch
    generic map (
      delay => 1)
    port map(
      clk    => clk,
      rst    => rst,
      s_exec => ld_reg,
      exec   => ld_regs
      );                        
  process(rst, clk)
  begin
    if rst = '1' then
      cmd_vector <= (others => '0');
    elsif clk' event and clk = '1' then
      case addr_dcs(15 downto 4) is
        when MSM_REG =>
          cmd_vector <= "0001";
        when MSM_CMD =>
          cmd_vector <= "0010";
        when MSM_MEM_A =>
          cmd_vector <= "0100";
        when MSM_MEM_B =>
          cmd_vector <= "1000";
        when others =>
          cmd_vector <= "0000";
      end case;
    end if;
  end process;
end;

-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_decoder_pack is

  component MSM2_DECODER
    port (
      rst           : in  std_logic;
      clk           : in  std_logic;
      wen_dcs       : in  std_logic;
      addr_dcs      : in  std_logic_vector(15 downto 0);
      data_dcs      : in  std_logic_vector(31 downto 0);
      dat_out_dcs   : out std_logic_vector(31 downto 0);
      bc_din_B1     : in  std_logic_vector(7 downto 0);
      bc_din_B2     : in  std_logic_vector(7 downto 0);
      interruptA_in : in  std_logic;
      interruptB_in : in  std_logic;
      fec_act_list  : in  std_logic_vector(31 downto 0);
      rcu_version   : in  std_logic_vector(31 downto 0);
      confg_err     : out std_logic_vector(7 downto 0);
      err_reg_a     : in  std_logic_vector(31 downto 0);
      err_reg_b     : in  std_logic_vector(31 downto 0);
      sm_wrd_BA     : in  std_logic_vector (9 downto 0);
      fsm_st_msm    : in  std_logic_vector (4 downto 0);
      en_mem_a      : out std_logic;
      en_mem_b      : out std_logic;
      en_reg        : out std_logic;
      en_int_a      : out std_logic;
      en_int_b      : out std_logic;
      errA_fec_ack  : in  std_logic;
      errA_ack_bcR  : in  std_logic;
      errA_din1_ack : in  std_logic;
      errA_din2_ack : in  std_logic;
      errB_fec_ack  : in  std_logic;
      errB_ack_bcR  : in  std_logic;
      errB_din1_ack : in  std_logic;
      errB_din2_ack : in  std_logic;
      fsm_state     : in  std_logic_vector(15 downto 0);
      bcast         : out std_logic;
      exec          : out std_logic;
      rst_rslt      : out std_logic;
      set_act       : out std_logic;
      set_rdo       : out std_logic;
      rst_mem       : out std_logic;
      RDnRW         : out std_logic;
      Branch        : out std_logic;
      ld_regs       : out std_logic;
      fec_add       : out std_logic_vector(3 downto 0);
      bc_reg_add    : out std_logic_vector(7 downto 0);
      B1_dout       : out std_logic_vector(7 downto 0);
      B2_dout       : out std_logic_vector(7 downto 0));
  end component;

end msm2_decoder_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
