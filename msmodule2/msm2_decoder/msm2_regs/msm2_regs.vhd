---------------------------------------------------------------------------------------------------
-- DESIGN RCU FIRM WARE V.2.0
-- MODULE NAME   : MSM_DECODER.VHD
--
-------------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;

-- library WORK;
-- use work.rcu_addr_set.all;

entity MSM2_REGS is
  
  port(
    rst         : in  std_logic;
    clk         : in  std_logic;        -- RCU CLOCK.
    en_reg      : in  std_logic;
    --------- DCS Signals-------------------------------
    wen_dcs     : in  std_logic;
    addr_dcs    : in  std_logic_vector (3 downto 0);
    data_dcs    : in  std_logic_vector (31 downto 0);
    dat_out_dcs : out std_logic_vector(31 downto 0);
    ----------------------------------------------------

    rdnwr      : out std_logic;
    Branch     : out std_logic;         -- BRANCH BIT IN THE ADDRESS FROM DCS
    bcast      : out std_logic;
    fec_add    : out std_logic_vector(3 downto 0);
    bc_reg_add : out std_logic_vector(7 downto 0);
    B1_dout    : out std_logic_vector(7 downto 0);
    B2_dout    : out std_logic_vector(7 downto 0);

    fec_act_list : in std_logic_vector(31 downto 0);
    rcu_version  : in std_logic_vector(31 downto 0);

    err_reg_a : in std_logic_vector(31 downto 0);
    err_reg_b : in std_logic_vector(31 downto 0);

    sm_wrd_BA : in  std_logic_vector (9 downto 0);
    en_int_a  : out std_logic;
    en_int_b  : out std_logic;

    ----------status of interrupt line every clock cycle--------

    interruptA_in : in std_logic;
    interruptB_in : in std_logic;
    ------------DEBUG INPUTSTO BE REGISTERED-----------------

    errA_fec_ack  : in  std_logic;
    errA_ack_bcR  : in  std_logic;
    errA_din1_ack : in  std_logic;
    errA_din2_ack : in  std_logic;
    errB_fec_ack  : in  std_logic;
    errB_ack_bcR  : in  std_logic;
    errB_din1_ack : in  std_logic;
    errB_din2_ack : in  std_logic;
    fsm_state     : in  std_logic_vector(15 downto 0);
    fsm_st_msm    : in  std_logic_vector (4 downto 0);
    -----Set soft or Hard Errors configuration--------------
    confg_err     : out std_logic_vector (7 downto 0);
    -------- inputs to clear rsult and status registers--
    clr_err       : in  std_logic;

    ------- From Baord Controller-----------------------
    bc_din_B1 : in std_logic_vector(7 downto 0);  -- first  byte from I2C interface Block
    bc_din_B2 : in std_logic_vector(7 downto 0)  -- second byte from I2C interface Block
    ----------------------------------------------------
    );

end MSM2_REGS;

architecture ARCH_MSM_REGS of MSM2_REGS is

  signal BC_DAT_REG    : std_logic_vector(15 downto 0)  := (others => '0');
  signal EN_INT_REG    : std_logic_vector(1 downto 0)   := (others => '0');
  signal BC_ADD_REG    : std_logic_vector(15 downto 0)  := (others => '0');
  signal RESULT_REG    : std_logic_vector(20 downto 0)  := (others => '0');
  signal DBG_REG1      : std_logic_vector (31 downto 0) := (others => '0');
  signal fec_ack_lch   : std_logic                      := '0';
  signal bcreg_ack_lch : std_logic                      := '0';
  signal B1_ack_lch    : std_logic                      := '0';
  signal B2_ack_lch    : std_logic                      := '0';
  signal rs_fec_add    : std_logic_vector(4 downto 0)   := (others => '0');
  signal st_err_st     : std_logic_vector(7 downto 0)   := x"74";

  constant old_act_fec : std_logic_vector(3 downto 0) := X"0";
  constant bc_rslt     : std_logic_vector(3 downto 0) := X"2";
  constant sc_st_reg   : std_logic_vector(3 downto 0) := X"3";
  constant int_en      : std_logic_vector(3 downto 0) := X"4";
  constant BC_ADDR     : std_logic_vector(3 downto 0) := X"5";
  constant BC_DATA     : std_logic_vector(3 downto 0) := X"6";
  constant old_fw_ver  : std_logic_vector(3 downto 0) := X"8";
  constant msk_err     : std_logic_vector(3 downto 0) := X"9";
  constant no_st_wrds  : std_logic_vector(3 downto 0) := X"A";
  constant err_br_a    : std_logic_vector(3 downto 0) := X"B";
  constant err_br_b    : std_logic_vector(3 downto 0) := X"C";
  
begin
  
  RESULT_REG <= rs_fec_add & bc_din_B2 & bc_din_B1;

  rdnwr      <= BC_ADD_REG(14);
  Branch     <= BC_ADD_REG(12);
  bcast      <= BC_ADD_REG(13);
  fec_add    <= BC_ADD_REG(11 downto 8);
  bc_reg_add <= BC_ADD_REG(7 downto 0);
  B1_dout    <= BC_DAT_REG(7 downto 0);
  B2_dout    <= BC_DAT_REG(15 downto 8);
  rs_fec_add <= BC_ADD_REG(12 downto 8);

  en_int_a  <= EN_INT_REG(0);
  en_int_b  <= EN_INT_REG(1);
  confg_err <= st_err_st;

  process (clk, clr_err, errA_ack_bcR, errA_fec_ack, errA_din1_ack, errA_din2_ack, errB_ack_bcR, errB_fec_ack, errB_din1_ack, errB_din2_ack)
  begin
    if clr_err = '1' then
      DBG_REG1 <= (others => '0');
    elsif clk'event and clk = '1' then
      DBG_REG1 (23 downto 8)  <= fsm_state;
      DBG_REG1 (28 downto 24) <= fsm_st_msm;
      DBG_REG1(29)            <= interruptA_in;
      DBG_REG1(30)            <= interruptB_in;
      if errA_fec_ack = '1' then
        DBG_REG1(0) <= '1';
      end if;
      if errA_ack_bcR = '1' then
        DBG_REG1(1) <= '1';
      end if;
      if errA_din1_ack = '1' then
        DBG_REG1(2) <= '1';
      end if;
      if errA_din2_ack = '1' then
        DBG_REG1(3) <= '1';
      end if;
      if errB_fec_ack = '1' then
        DBG_REG1(4) <= '1';
      end if;
      if errB_ack_bcR = '1' then
        DBG_REG1(5) <= '1';
      end if;
      if errB_din1_ack = '1' then
        DBG_REG1(6) <= '1';
      end if;
      if errB_din2_ack = '1' then
        DBG_REG1(7) <= '1';
      end if;
    end if;
  end process;


--- WRITE PROCESS FOR THE DATA AND ADDRESS ROM DCS              
  process(rst, clk, en_reg, wen_dcs, addr_dcs)
  begin
    if clk' event and clk = '1' then
      if en_reg = '1' and wen_dcs = '1' then
        case addr_dcs is
          when BC_ADDR =>
            BC_ADD_REG <= data_dcs(15 downto 0);
          when BC_DATA =>
            BC_DAT_REG <= data_dcs(15 downto 0);
          when INT_EN =>
            EN_INT_REG <= data_dcs(1 downto 0);
          when MSK_ERR =>
            st_err_st <= data_dcs(7 downto 0);
          when others =>
        end case;
      end if;
    end if;
  end process;

-- DEBUG REGISTERS ARE ADDED HERE ------


--- READ PROCESS FOR THE RESULT REGISTER FROM DCS

  process(clk, rst, en_reg, wen_dcs)
  begin
    if rst = '1' then
      dat_out_dcs <= (others => '0');
    elsif clk' event and clk = '1' then
      if en_reg = '1' and wen_dcs = '0' then
        case addr_dcs is
          
          when BC_RSLT =>               ---           
            dat_out_dcs <= X"00" & "000" & RESULT_REG;
          when BC_ADDR =>
            dat_out_dcs <= X"0000" & BC_ADD_REG;
          when BC_DATA =>
            dat_out_dcs <= X"0000" & BC_DAT_REG;
          when sc_st_reg =>
            dat_out_dcs <= DBG_REG1;
          when old_act_fec =>
            dat_out_dcs <= fec_act_list;
          when old_fw_ver =>
            dat_out_dcs <= rcu_version;
          when INT_EN =>
            dat_out_dcs <= X"0000000"&"00"& EN_INT_REG;
          when MSK_ERR =>
            dat_out_dcs <= X"000000" & st_err_st;
          when no_St_wrds =>
            dat_out_dcs <= X"00000" & "00" & sm_wrd_BA;
          when err_br_a =>
            dat_out_dcs <= err_reg_a;
          when err_br_b =>
            dat_out_dcs <= err_reg_b;
            
          when others =>
            dat_out_dcs <= (others => '0');
        end case;
      end if;
    end if;
  end process;
  
end ARCH_MSM_REGS;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_regs_pack is

  component MSM2_REGS
    port (
      rst           : in  std_logic;
      clk           : in  std_logic;
      en_reg        : in  std_logic;
      wen_dcs       : in  std_logic;
      addr_dcs      : in  std_logic_vector (3 downto 0);
      data_dcs      : in  std_logic_vector (31 downto 0);
      dat_out_dcs   : out std_logic_vector(31 downto 0);
      rdnwr         : out std_logic;
      Branch        : out std_logic;
      bcast         : out std_logic;
      fec_add       : out std_logic_vector(3 downto 0);
      bc_reg_add    : out std_logic_vector(7 downto 0);
      B1_dout       : out std_logic_vector(7 downto 0);
      B2_dout       : out std_logic_vector(7 downto 0);
      fec_act_list  : in  std_logic_vector(31 downto 0);
      rcu_version   : in  std_logic_vector(31 downto 0);
      err_reg_a     : in  std_logic_vector(31 downto 0);
      err_reg_b     : in  std_logic_vector(31 downto 0);
      sm_wrd_BA     : in  std_logic_vector (9 downto 0);
      en_int_a      : out std_logic;
      en_int_b      : out std_logic;
      interruptA_in : in  std_logic;
      interruptB_in : in  std_logic;
      errA_fec_ack  : in  std_logic;
      errA_ack_bcR  : in  std_logic;
      errA_din1_ack : in  std_logic;
      errA_din2_ack : in  std_logic;
      errB_fec_ack  : in  std_logic;
      errB_ack_bcR  : in  std_logic;
      errB_din1_ack : in  std_logic;
      errB_din2_ack : in  std_logic;
      fsm_state     : in  std_logic_vector(15 downto 0);
      fsm_st_msm    : in  std_logic_vector (4 downto 0);
      confg_err     : out std_logic_vector (7 downto 0);
      clr_err       : in  std_logic;
      bc_din_B1     : in  std_logic_vector(7 downto 0);
      bc_din_B2     : in  std_logic_vector(7 downto 0));
  end component;
end msm2_regs_pack;
-------------------------------------------------------------------------------
--
-- EOF
-------------------------------------------------------------------------------

