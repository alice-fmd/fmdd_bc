library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;

library msmodule2_lib;
use msmodule2_lib.msm2_exec_stretch_pack.all;

-- library WORK;
-- use work.rcu_addr_set.all;

entity MSM2_CMD_DEC is
  
  port(
    rst : in std_logic;
    clk : in std_logic;                 -- RCU CLOCK.

    --------- DCS Signals---------------------------
    wen_dcs  : in std_logic;
    addr_dcs : in std_logic_vector(3 downto 0);
    data_dcs : in std_logic_vector(31 downto 0);
    -----------------------------------------------
    en_cmd   : in std_logic;

    -----------------------------------------------
    set_act  : out std_logic;
    rst_rslt : out std_logic;
    rst_err  : out std_logic; 
    rst_mem  : out std_logic;
     set_rdo  : out std_logic;
    exec     : out std_logic            -- START EXECUTE I2C Block


    );
end MSM2_CMD_DEC;

architecture A_MSM_CMD_DEC of MSM2_CMD_DEC is

  signal s_exec : std_logic;

  constant BC_EXEC      : std_logic_vector(3 downto 0) := X"0";
  constant RST_ERR_REG  : std_logic_vector(3 downto 0) := X"1";
  constant RST_RSLT_REG : std_logic_vector(3 downto 0) := X"2";
  constant RESTOR_ACT  : std_logic_vector(3 downto 0) := X"3";
  constant RESTOR_RdO  : std_logic_vector(3 downto 0) := X"4";
  constant RST_MEM_REG  : std_logic_vector(3 downto 0) := X"5";
begin
  process(rst, clk, wen_dcs, addr_dcs)
  begin
    if clk' event and clk = '1' then
      if en_cmd = '1' and wen_dcs = '1' then
        case addr_dcs is
          when BC_EXEC =>
            
            s_exec   <= '1';
            set_act  <= '0';
            set_rdo  <= '0';
            rst_rslt <= '0';
            rst_err  <= '0'; 
            rst_mem  <= '0';
            
          when RESTOR_ACT =>
            
            s_exec   <= '0';
            set_rdo  <= '0';
            set_act  <= '1';
            rst_rslt <= '0';
            rst_err  <= '0'; 
            rst_mem  <= '0';
            
          when RESTOR_RDO =>
            
            s_exec   <= '0';
            set_rdo  <= '1';
            set_act  <= '0';
            rst_rslt <= '0';
            rst_err  <= '0'; 
            rst_mem  <= '0';
            
          when RST_ERR_REG =>
            
            s_exec   <= '0';
            set_rdo  <= '0';
            set_act  <= '0';
            rst_rslt <= '0';
            rst_err  <= '1'; 
            rst_mem  <= '0';
            
          when RST_RSLT_REG =>
            
            s_exec   <= '0';
            set_rdo  <= '0';
            set_act  <= '0';
            rst_rslt <= '1';
            rst_err  <= '0';
            rst_mem  <= '0';
            
          when RST_MEM_REG =>
            
            s_exec   <= '0';
            set_rdo  <= '0';
            set_act  <= '0';
            rst_rslt <= '1';
            rst_err  <= '0';
            rst_mem  <= '1';
            
          when others =>
            
            s_exec   <= '0';
            set_act  <= '0';
            set_rdo  <= '0';
            rst_rslt <= '0';
            rst_err  <= '0';
            rst_mem  <= '0';
            
        end case;
      else
        s_exec   <= '0';
        set_act  <= '0';
        set_rdo  <= '0';
        rst_rslt <= '0';
        rst_err  <= '0';
        rst_mem  <= '0';
      end if;
    end if;
  end process;


  Stretch_EXEC : msm2_exec_stretch
    generic map (
      delay => 1)
    port map(
      clk    => clk,
      rst    => rst,
      s_exec => s_exec,
      exec   => exec

      );        


end A_MSM_CMD_DEC;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_cmd_dec_pack is

  component MSM2_CMD_DEC
    port (
      rst      : in  std_logic;
      clk      : in  std_logic;
      wen_dcs  : in  std_logic;
      addr_dcs : in  std_logic_vector(3 downto 0);
      data_dcs : in  std_logic_vector(31 downto 0);
      en_cmd   : in  std_logic;
      set_act  : out std_logic;
      rst_rslt : out std_logic;
      rst_err  : out std_logic;
      rst_mem  : out std_logic;
      set_rdo  : out std_logic;
      exec     : out std_logic);
  end component;

end msm2_cmd_dec_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
