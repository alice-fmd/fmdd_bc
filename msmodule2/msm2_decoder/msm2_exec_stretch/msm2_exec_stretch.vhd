
library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;


entity msm2_exec_stretch is
  generic (delay : integer := 1);
  port (
    clk    : in  std_logic;
    s_exec : in  std_logic;
    rst    : in  std_logic;
    exec   : out std_logic

    );

end msm2_exec_stretch;

architecture arch_delay of msm2_exec_stretch is
  subtype cnt_t is integer range 0 to 15;
  --  signal cnt       : std_logic_vector (3 downto 0) := "0000";
  signal cnt       : cnt_t := 0; -- "0000";
  signal s_fec_rst : std_logic                     := '0';
  signal en_del    : std_logic                     := '0';
  signal time_out  : std_logic                     := '0';
  signal clr_cnt   : std_logic                     := '0';
-- defining state mahcine attribute.    

  type   sm is(idle, sen_exe);
  attribute ENUM_ENCODING       : string;
  attribute ENUM_ENCODING of sm : type is "0 1";
  signal pr_st, nxt_st          : sm;
  
begin

  process(clk)
  begin
    
    if rst = '1' then
      
      pr_st <= idle;
      
    elsif(clk' event and clk = '1') then
      pr_st <= nxt_st;
    end if;
  end process;

  process (rst, pr_st, s_exec, time_out)
  begin

    if rst = '1' then
      
      nxt_st <= idle;
      
    else
      case pr_st is

        when idle =>

          if s_exec = '1' then
            nxt_st <= sen_exe;

          end if;

        when sen_exe =>

          if time_out = '1' then
            nxt_st <= idle;
          else
            nxt_st <= sen_exe;

          end if;

        when others =>
          nxt_st <= idle;


      end case;
      
    end if;

  end process;

  --------------------------------OUT PUT STAGE---------------------------
  process(rst, clk)
  begin
    if rst = '1' then
      
      en_del  <= '0';
      clr_cnt <= '1';
      exec    <= '0';
      
    elsif clk'event and clk = '1' then
      case nxt_st is

        when idle =>

          en_del  <= '0';
          clr_cnt <= '1';
          exec    <= '0';

        when sen_exe =>

          clr_cnt <= '0';
          en_del  <= '1';
          exec    <= '1';

        when others =>
          clr_cnt <= '0';
          exec    <= '0';
          en_del  <= '0';
      end case;
    end if;

  end process;

  -----------------------------------counter-------------------------

  process(clk, en_del, clr_cnt)
  begin
    if clr_cnt = '1' then
      cnt <= 0; -- (others => '0');
    elsif (clk' event and clk = '1') then
      if en_del = '1' then
        if cnt < delay then
          cnt <= cnt + 1;
        end if;
      end if;
    end if;
  end process;

  process(en_del, cnt, clk)
  begin
    if en_del = '1' then
      if cnt < delay then
        time_out <= '0';
      else
        time_out <= '1';
      end if;
    else
      time_out <= '0';
    end if;
  end process;

end arch_delay;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_exec_stretch_pack is

  component msm2_exec_stretch
    generic (
      delay : integer);
    port (
      clk    : in  std_logic;
      s_exec : in  std_logic;
      rst    : in  std_logic;
      exec   : out std_logic);
  end component;

end msm2_exec_stretch_pack;

-------------------------------------------------------------------------------
--
-- EOF
--
