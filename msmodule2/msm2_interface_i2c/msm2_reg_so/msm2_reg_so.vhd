
library IEEE;
use IEEE.std_logic_1164.all;
-- use IEEE.std_logic_arith.all;


entity msm2_reg_so is
  generic(reg_width : integer := 8);
  port(
    DIN : in  std_logic_vector(7 downto 0);
    CLK : in  std_logic;
    LD  : in  std_logic;
    rst : in  std_logic;
    SE  : in  std_logic;
    SO  : out std_logic
    );
end msm2_reg_so;

architecture rtl of msm2_reg_so is
  signal pre_Q : std_logic_vector((reg_width-1) downto 0) := (others => '0');

  attribute preserve          : boolean;
  attribute preserve of pre_Q : signal is true;
begin
  SHIFT_REGISTER : process(CLK, rst, SE, DIN, LD)
  begin
    if rst = '1' then
      pre_Q <= (others => '0');
      SO    <= '0';
    elsif (LD = '1') then
      pre_Q <= DIN;
    elsif CLK'event and CLK = '1' then
      -- IF (LD = '1') THEN
      -- pre_Q <= DIN;
      if SE = '1' then
        pre_Q((reg_width-1) downto 1) <= pre_Q((reg_width-2) downto 0);
        pre_Q(0)                      <= '0';
        
      end if;
      SO <= pre_Q(reg_width-1);
    end if;
  end process SHIFT_REGISTER;
  
end rtl;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

package msm2_reg_so_pack is

  component msm2_reg_so
    generic (
      reg_width : integer);
    port (
      DIN : in  std_logic_vector(7 downto 0);
      CLK : in  std_logic;
      LD  : in  std_logic;
      rst : in  std_logic;
      SE  : in  std_logic;
      SO  : out std_logic);
  end component;

end msm2_reg_so_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
