library ieee;
use ieee.std_logic_1164.all;
-- use ieee.std_logic_unsigned.all;

entity msm2_clk_div is
  port(rcu_clk    : in  std_logic;
         reset    : in  std_logic;
         enable   : in  std_logic;
         sclk     : out std_logic;
         sclk_90  : out std_logic;
         sclk_270 : out std_logic
         );
end msm2_clk_div;

-- tmp_clk  divide by 2
-- tmp_clk2 divide by 4
-- tmp_clk3 divide by 8

architecture ach_clk_div_msm of msm2_clk_div is
  signal count     : std_logic_vector(2 downto 0) := (others => '1');
  signal count2    : std_logic_vector(2 downto 0) := (others => '0');
  signal inv_clk90 : std_logic;
  signal tmp_clk   : std_logic;
  signal tmp_clk2  : std_logic                    := '1';
  signal tmp_clk3  : std_logic                    := '1';
  signal tmp_clk4  : std_logic                    := '1';
begin
  sclk     <= tmp_clk;
  sclk_90  <= tmp_clk2;
  sclk_270 <= tmp_clk4;
  
  -- process (enable, tmp_clk2,tmp_clk3)
  -- begin
  -- if enable = '0' then
  -- sclk <= '1' ;
  -- elsif ( tmp_clk2'event and tmp_clk2='1' ) then
  -- sclk <= tmp_clk3 ;
  -- end if;
  -- end process;

  process(rcu_clk, reset)
  begin
    if reset = '1' then
      tmp_clk <= '1';
    elsif (rcu_clk'event and rcu_clk = '1') then
      tmp_clk <= not tmp_clk;
    end if;
  end process;

  process(tmp_clk, reset)
  begin
    if reset = '1' then
      tmp_clk2 <= '1';
    elsif ( tmp_clk'event and tmp_clk='1' ) then
      tmp_clk2 <= not tmp_clk2;
    end if;                               
  end process; 

  -- process(tmp_clk2, reset,tmp_clk)
  -- begin
  -- if reset = '1' then
  -- tmp_clk3 <= '1';
  -- elsif ( tmp_clk2'event and tmp_clk2='1' ) then
  -- tmp_clk3 <= not tmp_clk3;
  -- END IF;                               
  -- if tmp_clk = '1' then
  -- tmp_clk4 <= not tmp_clk3;
  -- end if;
  -- end process; 

  
  
end ach_clk_div_msm;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package msm2_clk_div_pack is

  component msm2_clk_div
    port (
      rcu_clk  : in  std_logic;
      reset    : in  std_logic;
      enable   : in  std_logic;
      sclk     : out std_logic;
      sclk_90  : out std_logic;
      sclk_270 : out std_logic);
  end component;

end msm2_clk_div_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
