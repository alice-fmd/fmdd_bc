-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library msmodule2_lib;
use msmodule2_lib.msm2_clk_div_pack.all;

-------------------------------------------------------------------------------
entity msm2_clk_div_tb is
end msm2_clk_div_tb;

-------------------------------------------------------------------------------
architecture test of msm2_clk_div_tb is

  -- component ports
  signal rcu_clk  : std_logic := '0';   -- [in]
  signal reset    : std_logic := '1';   -- [in]
  signal enable   : std_logic := '0';   -- [in]
  signal sclk     : std_logic;          -- [out]
  signal sclk_90  : std_logic;          -- [out]
  signal sclk_270 : std_logic;          -- [out]

  -- clock period
  constant PERIOD : time := 25 ns;

begin  -- test

  -- component instantiation
  DUT: msm2_clk_div
    port map (
      rcu_clk  => rcu_clk,              -- [in]
      reset    => reset,                -- [in]
      enable   => enable,               -- [in]
      sclk     => sclk,                 -- [out]
      sclk_90  => sclk_90,              -- [out]
      sclk_270 => sclk_270);            -- [out]

  -- clock generation
  rcu_clk <= not rcu_clk after PERIOD / 2;

  -- waveform generation
  stim: process
  begin
    wait for 4 * PERIOD;
    reset <= '0';
    -- insert signal assignments here

    wait; -- forever
  end process stim;
end test;

-------------------------------------------------------------------------------
