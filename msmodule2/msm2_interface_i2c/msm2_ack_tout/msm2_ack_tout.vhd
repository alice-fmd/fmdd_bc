------------------------------------------------------------------------------------------------------
-- BLOCK: STATUS BIT COUNTER
-- POLARITIES: All control Signal Active High, ASYNCHRONOUS RESET AND CLEAR
-- CLOCK DOMAIN:10 MHZ clock
-- LAST UPDATE:25.10.07
-------------------------------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;


entity msm2_ack_tout is
  generic (bit_width : integer := 3);
  port (
    --input----------------
    rst     : in  std_logic;
    clr_cnt : in  std_logic;
    clk     : in  std_logic;
    en_cnt  : in  std_logic;
    --output----------------
    t_out   : out std_logic);           
end msm2_ack_tout;

architecture arch_time_out of msm2_ack_tout is
  signal count2 : std_logic_vector (11 downto 0) := (others => '0');
begin
  process(clk, clr_cnt, rst)
  begin
    if (rst = '1' or clr_cnt = '1') then
      count2 <= (others => '0');
      t_out  <= '0';
    elsif (clk' event and clk = '1') then
      if en_cnt = '1' then
        if count2 < bit_width then
          count2 <= count2 + 1;
          t_out  <= '0';
        else
          t_out <= '1';
        end if;
      end if;
    end if;
  end process;
end;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_ack_tout_pack is

  component msm2_ack_tout
    generic (
      bit_width : integer);
    port (
      rst     : in  std_logic;
      clr_cnt : in  std_logic;
      clk     : in  std_logic;
      en_cnt  : in  std_logic;
      t_out   : out std_logic);
  end component;

end msm2_ack_tout_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
