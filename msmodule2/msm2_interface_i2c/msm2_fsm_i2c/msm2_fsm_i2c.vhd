

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;
--use work.all;



entity msm2_fsm_i2c is
  
  port (
    rst          : in  std_logic;       -- RESET 
    clk          : in  std_logic;       -- CLOCK
    exec         : in  std_logic;       -- START FROM COMMAND DECODER
    rd_nwr       : in  std_logic;       -- READ OT WRITE OPERATION
    sda_in       : in  std_logic;  -- FROM BOARD CONTROLLER SERIAL INPUT FOR ACK
    bit_cnt_ovr  : in  std_logic;       -- bit counting is of one byte (8-bits)
    sclK         : out std_logic;       -- ENABLE OR DISABLE THE I2C CLOCK OUT
    sel_sda_out  : out std_logic_vector(2 downto 0);  -- SELECT THE FEC/REG ADDRESS TO SEND ON BUS "sda_out"
    sda_out_fsm  : out std_logic;       -- START PULSE ON SDA_OUT / STOP
    sv_data_b1   : out std_logic;  -- ENABLE THE REGISTER BYTE ONE SERIAL IN PARALLEL OUT
    sv_data_b2   : out std_logic;  -- ENABLE THE REGISTER BYTE TWO SERIAL IN PARALLEL OUT
    en_fec_reg   : out std_logic;
    en_bc_reg    : out std_logic;
    en_B1_dout   : out std_logic;
    en_B2_dout   : out std_logic;
    en_cnt_in    : out std_logic;
    cnt_in_ovr   : in  std_logic;
    -- tout_ack         : in std_logic;
    --- DEBUG SIGNAL TO REGISTER-----
    err_fec_ack  : out std_logic;
    err_bc_add   : out std_logic;
    err_din1_ack : out std_logic;
    err_din2_ack : out std_logic;
    sv_rslt      : out std_logic;
    I2C_BSY      : out std_logic;
    end_rd       : out std_logic;
    fsm_state    : out std_logic_vector(7 downto 0);
    en_bit_cnt   : out std_logic;
    clr_bit_cnt  : out std_logic
    );

end msm2_fsm_i2c;

architecture ARCH_fsm_i2c of msm2_fsm_i2c is


  -- constant    idle                                   : std_logic_vector(7 downto 0) := X"00"; --x 00; 
  -- constant  start1                   : std_logic_vector(7 downto 0) := X"01"; --x 00;                
  -- constant  start2                      : std_logic_vector(7 downto 0) := X"03"; --x 00; 
  -- constant  fec1                             : std_logic_vector(7 downto 0) := X"02"; --x 00; 
  -- constant  fec2                             : std_logic_vector(7 downto 0) := X"06"; --x 00; 
  -- constant  fec3                             : std_logic_vector(7 downto 0) := X"07"; --x 00; 
  -- constant  fec4                             : std_logic_vector(7 downto 0) := X"05"; --x 00; 
  -- constant  chk1_ack_fec             : std_logic_vector(7 downto 0) := X"04"; --x 00; 
  -- constant  chk2_ack_fec             : std_logic_vector(7 downto 0) := X"0C"; --x 00; 
  -- constant  chk3_ack_fec             : std_logic_vector(7 downto 0) := X"0D"; --x 00; 
  -- constant  chk4_ack_fec             : std_logic_vector(7 downto 0) := X"0F"; --x 00; 
  -- constant  fec_ack_err              : std_logic_vector(7 downto 0) := X"0E"; --x 00; 
  -- constant  REG_ACK_ERR              : std_logic_vector(7 downto 0) := X"0A"; --x 00; 
  -- constant  DIN1_ACK_ERR             : std_logic_vector(7 downto 0) := X"0B"; --x 00; 
  -- constant  reg_add1                 : std_logic_vector(7 downto 0) := X"09"; --x 00;                
  -- constant  reg_add2                 : std_logic_vector(7 downto 0) := X"08"; --x 00; 
  -- constant  reg_add3                 : std_logic_vector(7 downto 0)   := X"18"; --x 00; 
  -- constant  reg_add4                 : std_logic_vector(7 downto 0) := X"19"; --x 00; 
  -- constant  chk_ack1_reg     : std_logic_vector(7 downto 0) := X"1B"; --x 00; 
  -- constant  chk_ack2_reg             : std_logic_vector(7 downto 0) := X"1A"; --x 00; 
  -- constant  chk_ack3_reg     : std_logic_vector(7 downto 0) := X"1E"; --x 00; 
  -- constant  chk_ack4_reg             : std_logic_vector(7 downto 0) := X"1F"; --x 00; 
  -- constant  sv1_dat_b1               : std_logic_vector(7 downto 0) := X"1D"; --x 00; 
  -- constant  sv2_dat_b1               : std_logic_vector(7 downto 0)   := X"1C"; --x 00; 
  -- constant  sv3_dat_b1               : std_logic_vector(7 downto 0)   := X"14"; --x 00; 
  -- constant  sv4_dat_b1               : std_logic_vector(7 downto 0)   := X"15"; --x 00;              
  -- constant  ack1_din_b1              : std_logic_vector(7 downto 0) := X"17"; --x 00; 
  -- constant  ack2_din_b1              : std_logic_vector(7 downto 0) := X"16"; --x 00; 
  -- constant  ack3_din_b1              : std_logic_vector(7 downto 0) := X"12"; --x 00; 
  -- constant  ack4_din_b1              : std_logic_vector(7 downto 0) := X"13"; --x 00; 
  -- constant  sv1_dat_b2               : std_logic_vector(7 downto 0) := X"11"; --x 00; 
  -- constant  sv2_dat_b2               : std_logic_vector(7 downto 0)   := X"10"; --x 00; 
  -- constant  sv3_dat_b2               : std_logic_vector(7 downto 0)   := X"30"; --x 00; 
  -- constant  sv4_dat_b2               : std_logic_vector(7 downto 0)   := X"31"; --x 00; 
  -- constant  ack1_din_b2              : std_logic_vector(7 downto 0) := X"33"; --x 00; 
  -- constant  ack2_din_b2              : std_logic_vector(7 downto 0) := X"32"; --x 00; 
  -- constant  ack3_din_b2              : std_logic_vector(7 downto 0) := X"36"; --x 00; 
  -- constant  ack4_din_b2              : std_logic_vector(7 downto 0) := X"37"; --x 00; 
  -- constant  DIN2_ACK_ERR             : std_logic_vector(7 downto 0) := X"35"; --x 00; 
  -- constant  stop1                    : std_logic_vector(7 downto 0)   := X"34"; --x 00; 
  -- constant  stop2                    : std_logic_vector(7 downto 0)   := X"3C"; --x 00; 
  -- constant  stop3                    : std_logic_vector(7 downto 0)   := X"3D"; --x 00;      
  -- constant  stop4                            : std_logic_vector(7 downto 0) := X"3F"; --x 00; 
  -- constant  stop5                            : std_logic_vector(7 downto 0) := X"3E"; --x 00; 
  -- constant  finish                   : std_logic_vector(7 downto 0)          := X"3A"; --x 00; 


  -- signal pr_st,nxt_st                : std_logic_vector(7 downto 0);
  
  
  
  type state is (idle
                 , start1
                 , start2
                 , start3
                 , fec1
                 , fec2
                 , fec3
                 , fec4
                 , chk1_ack_fec
                 , chk2_ack_fec
                 , chk3_ack_fec
                 , chk4_ack_fec
                 , fec_ack_err
                 , REG_ACK_ERR
                 , DIN1_ACK_ERR
                 , reg_add1
                 , reg_add2
                 , reg_add3
                 , reg_add4
                 , chk_ack1_reg
                 , chk_ack2_reg
                 , chk_ack3_reg
                 , chk_ack4_reg , chk_ack5_reg  --- Branch for Wrtie TX  -- dut = data out :((
                 , sv1_dat_b1 , wr1_dat_b2
                 , sv2_dat_b1 , wr2_dat_b2
                 , sv3_dat_b1 , wr3_dat_b2
                 , sv4_dat_b1 , wr4_dat_b2
                 , ack1_din_b1 , ack1_dut_b2
                 , ack2_din_b1 , ack2_dut_b2
                 , ack3_din_b1 , ack3_dut_b2
                 , ack4_din_b1 , ack4_dut_b2
                 , sv1_dat_b2 , wr1_dat_b1
                 , sv2_dat_b2 , wr2_dat_b1
                 , sv3_dat_b2 , wr3_dat_b1
                 , sv4_dat_b2 , wr4_dat_b1
                 , ack1_din_b2 , ack1_dut_b1
                 , ack2_din_b2 , ack2_dut_b1
                 , ack3_din_b2 , ack3_dut_b1
                 , ack4_din_b2 , ack4_dut_b1
                 , DIN2_ACK_ERR , DUT1_ACK_ERR
                 , stop1 , DUT2_ACK_ERR
                 , stop2 , stop_wr_1
                 , stop3 , stop_wr_2
                 , stop4 , stop_wr_3
                 , stop5 , stop_wr_4
                 , finish
                 );
  signal pr_st  : state;
  signal nxt_st : state;

  attribute preserve           : boolean;
  attribute preserve of pr_st  : signal is true;
  attribute preserve of nxt_st : signal is true;
  
begin

--      fsm_state <= pr_st;                     
  
  process(clk, rst)
  begin
    if(rst = '1') then
      pr_st <= idle;
    elsif(clk' event and clk = '1') then
      pr_st <= nxt_st;
    end if;
  end process;

  --next_state : process (rst,pr_st,exec, rd_nwr,sda_in,bit_cnt_ovr )   

  next_state : process (rst, pr_st, exec, rd_nwr, bit_cnt_ovr, cnt_in_ovr, sda_in)

  begin
    if rst = '1' then
      nxt_st <= idle;
    else
      case pr_st is
        
        when idle =>
          
          if exec = '1' then
            nxt_st <= start1;
          else
            nxt_st <= idle;
          end if;
          
        when start1 =>
          
          nxt_st <= start2;
          
        when start2 =>
          
          nxt_st <= start3;
          
        when start3 =>
          
          nxt_st <= fec1;
          
        when fec1 =>
          
          nxt_st <= fec2;
          
        when fec2 =>
          
          nxt_st <= fec3;
          
        when fec3 =>
          
          nxt_st <= fec4;
          
        when fec4 =>
          
          if bit_cnt_ovr = '1' then
            
            nxt_st <= chk1_ack_fec;
            
          else
            nxt_st <= fec1;
            
          end if;
          
        when chk1_ack_fec =>
          
          
          nxt_st <= chk2_ack_fec;
          
        when chk2_ack_fec =>            -- adding for debug
          
          
          if sda_in = '0' then
            
            nxt_st <= chk3_ack_fec;
          else
            
            nxt_st <= fec_ack_err; 

                      
          end if;
          
          
        when chk3_ack_fec =>
          
          nxt_st <= chk4_ack_fec;
          
        when chk4_ack_fec =>
          
          nxt_st <= reg_add1;
          
        when REG_ACK_ERR =>
          
          nxt_st <= idle;
          
        when fec_ack_err =>
          
          nxt_st <= idle;


        when reg_add1 =>
          
          nxt_st <= reg_add2;
          
        when reg_add2 =>
          
          nxt_st <= reg_add3;
          
        when reg_add3 =>
          
          nxt_st <= reg_add4;
          
        when reg_add4 =>
          
          if bit_cnt_ovr = '1' then
            
            nxt_st <= chk_ack1_reg;
            
          else
            nxt_st <= reg_add1;
            
          end if;
          
        when chk_ack1_reg =>
          
          nxt_st <= chk_ack2_reg;
          
          
        when chk_ack2_reg =>
          
          if sda_in = '0' then
            
            nxt_st <= chk_ack3_reg;
            
          else
            
            nxt_st <= reg_ack_err;
            
          end if;
          
        when chk_ack3_reg =>
          
          if rd_nwr = '0' then
            nxt_st <= chk_ack5_reg;
          else
            nxt_st <= chk_ack4_reg;
          end if;
          
        when chk_ack4_reg =>
          
          nxt_st <= sv1_dat_b2;

        when sv1_dat_b2 =>
          
          nxt_st <= sv2_dat_b2;
          
        when sv2_dat_b2 =>
          
          nxt_st <= sv3_dat_b2;
          
        when sv3_dat_b2 =>
          
          nxt_st <= sv4_dat_b2;
          
        when sv4_dat_b2 =>
          
          if cnt_in_ovr = '1' then
            
            nxt_st <= ack1_din_b2;
            
          else
            
            nxt_st <= sv1_dat_b2;
            
          end if;
          
        when ack1_din_b2 =>

          --    if sda_in ='1' then
          
          nxt_st <= ack2_din_b2;

          --    else

          --    nxt_st <= din2_ack_err;

          --    end if;
          
        when din2_ack_err =>
          
          nxt_st <= idle;
          
        when ack2_din_b2 =>
          
          nxt_st <= ack3_din_b2;
          
        when ack3_din_b2 =>
          
          nxt_st <= ack4_din_b2;
          
        when ack4_din_b2 =>
          
          nxt_st <= sv1_dat_b1;
          
        when sv1_dat_b1 =>
          
          nxt_st <= sv2_dat_b1;
          
        when sv2_dat_b1 =>
          
          nxt_st <= sv3_dat_b1;
          
        when sv3_dat_b1 =>
          
          nxt_st <= sv4_dat_b1;
          
        when sv4_dat_b1 =>
          
          if cnt_in_ovr = '1' then
            
            nxt_st <= ack1_din_b1;
            
          else
            
            nxt_st <= sv1_dat_b1;
            
          end if;
          
        when ack1_din_b1 =>

          --    if sda_in ='0' then
          
          nxt_st <= ack2_din_b1;

          --    else

          --    nxt_st <= din1_ack_err;

          -- end if;    
          
        when din1_ack_err =>
          
          nxt_st <= idle;
          
        when ack2_din_b1 =>
          
          nxt_st <= ack3_din_b1;
          
        when ack3_din_b1 =>
          
          nxt_st <= ack4_din_b1;
          
        when ack4_din_b1 =>
          
          nxt_st <= stop1;
          
        when chk_ack5_reg =>
          
          nxt_st <= wr1_dat_b2;
          
        when wr1_dat_b2 =>
          
          nxt_st <= wr2_dat_b2;
          
        when wr2_dat_b2 =>
          
          nxt_st <= wr3_dat_b2;
          
        when wr3_dat_b2 =>
          
          nxt_st <= wr4_dat_b2;
          
        when wr4_dat_b2 =>
          
          if bit_cnt_ovr = '1' then
            
            nxt_st <= ack1_dut_b2;
            
          else
            
            nxt_st <= wr1_dat_b2;
            
          end if;
          
        when ack1_dut_b2 =>
          
          nxt_st <= ack2_dut_b2;
          
        when ack2_dut_b2 =>
          
          nxt_st <= ack3_dut_b2;
          
        when ack3_dut_b2 =>
          
          nxt_st <= ack4_dut_b2;
          
        when ack4_dut_b2 =>
          
          nxt_st <= wr1_dat_b1;
          
        when wr1_dat_b1 =>
          
          nxt_st <= wr2_dat_b1;
          
        when wr2_dat_b1 =>
          
          nxt_st <= wr3_dat_b1;
          
        when wr3_dat_b1 =>
          
          nxt_st <= wr4_dat_b1;
          
        when wr4_dat_b1 =>
          
          if bit_cnt_ovr = '1' then
            
            nxt_st <= ack1_dut_b1;
            
          else
            
            nxt_st <= wr1_dat_b1;
            
          end if;
          
        when ack1_dut_b1 =>
          
          nxt_st <= ack2_dut_b1;
          
        when ack2_dut_b1 =>
          
          nxt_st <= ack3_dut_b1;
          
        when ack3_dut_b1 =>
          
          nxt_st <= ack4_dut_b1;
          
        when ack4_dut_b1 =>
          
          nxt_st <= stop1;
          
        when stop1 =>
          
          nxt_st <= stop2;
          
        when stop2 =>
          
          nxt_st <= stop3;
          
        when stop3 =>
          
          nxt_st <= stop4;
          
        when stop4 =>
          
          nxt_st <= stop5;
        when stop5 =>
          
          nxt_st <= finish;
          
        when finish =>

          nxt_st <= idle;
          
        when others =>
          nxt_st <= idle;
      end case;
    end if;
  end process;

  --------------------------------------------
  output_stage : process (rst, clk)
  begin
    if rst = '1' then
      
      sclK         <= '1';
      sel_sda_out  <= "000";
      sda_out_fsm  <= '1';
      sv_data_b1   <= '0';
      sv_data_b2   <= '0';
      en_bit_cnt   <= '0';
      clr_bit_cnt  <= '1';
      en_B2_dout   <= '0';
      en_cnt_in    <= '0';
      err_fec_ack  <= '0';
      err_bc_add   <= '0';
      en_B1_dout   <= '0';
      en_bc_reg    <= '0';
      err_din1_ack <= '0';
      err_din2_ack <= '0';
      I2C_BSY      <= '0';
      sv_rslt      <= '0';
      end_rd       <= '0';
      en_fec_reg   <= '0';
      
    elsif(clk' event and clk = '1') then
      
      case nxt_st is
        
        when idle =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '0';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when start1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
          
        when start2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when start3 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '1';
          
        when fec1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "001";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '1';
          
        when fec2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "001";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '1';
          
        when fec3 =>
          
          sclK         <= '1';
          sel_sda_out  <= "001";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '1';
          
        when fec4 =>
          
          sclK         <= '0';
          sel_sda_out  <= "001";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '1';
          
        when chk1_ack_fec =>

          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when chk2_ack_fec =>

          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when chk3_ack_fec =>

          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when chk4_ack_fec =>

          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '1';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when reg_add1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "010";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '1';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when reg_add2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "010";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '1';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when reg_add3 =>
          
          
          sclK         <= '1';
          sel_sda_out  <= "010";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '1';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when reg_add4 =>
          
          
          sclK         <= '0';
          sel_sda_out  <= "010";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '1';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
          
        when chk_ack1_reg =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when chk_ack2_reg =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when chk_ack3_reg =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when chk_ack4_reg =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when chk_ack5_reg =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';          --- 24/08/09
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '1';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when sv1_dat_b1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '1';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '1';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
          
        when sv2_dat_b1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '1';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '1';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when sv3_dat_b1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          -- sv_data_b1   <= '1'; -- CHOlM: Wait one cycle before latching input
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '1';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when sv4_dat_b1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '1';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '1';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack1_din_b1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack2_din_b1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack3_din_b1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack4_din_b1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when sv1_dat_b2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '1';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '1';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when wr1_dat_b2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "011";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '1';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when wr2_dat_b2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "011";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '1';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when wr3_dat_b2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "011";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '1';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when wr4_dat_b2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "011";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '1';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when sv2_dat_b2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '1';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '1';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when sv3_dat_b2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          -- sv_data_b2   <= '1'; -- CHOLM: Wait one cycle before latching input
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '1';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when sv4_dat_b2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '1';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '1';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack1_din_b2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack2_din_b2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack3_din_b2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack4_din_b2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when stop1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '1';          ----modified 24/08
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when stop2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '1';          ----modified 24/08
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when stop3 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when stop4 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '1';
          en_fec_reg   <= '0';
          
        when stop5 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '1';
          en_fec_reg   <= '0';
          
        when fec_ack_err =>

          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '1';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
          
        when REG_ACK_ERR =>

          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '1';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when DIN1_ACK_ERR =>

          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '1';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when DIN2_ACK_ERR =>

          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '1';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when finish =>

          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '1';
          en_fec_reg   <= '0';
          
        when ack1_dut_b2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack2_dut_b2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '1';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack3_dut_b2 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack4_dut_b2 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';          -- 24/08/09
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '1';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when wr1_dat_b1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "100";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '1';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when wr2_dat_b1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "100";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '1';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when wr3_dat_b1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "100";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '1';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when wr4_dat_b1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "100";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '1';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '1';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack1_dut_b1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack2_dut_b1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack3_dut_b1 =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when ack4_dut_b1 =>
          
          sclK         <= '0';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '0';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
        when others =>
          
          sclK         <= '1';
          sel_sda_out  <= "000";
          sda_out_fsm  <= '1';
          sv_data_b1   <= '0';
          sv_data_b2   <= '0';
          en_bit_cnt   <= '0';
          clr_bit_cnt  <= '0';
          en_B2_dout   <= '0';
          en_cnt_in    <= '0';
          err_fec_ack  <= '0';
          err_bc_add   <= '0';
          err_din1_ack <= '0';
          err_din2_ack <= '0';
          en_bc_reg    <= '0';
          en_B1_dout   <= '0';
          I2C_BSY      <= '1';
          sv_rslt      <= '0';
          end_rd       <= '0';
          en_fec_reg   <= '0';
          
      end case;
    end if;
  end process;
  
end;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_fsm_i2c_pack is

  component msm2_fsm_i2c
    port (
      rst          : in  std_logic;
      clk          : in  std_logic;
      exec         : in  std_logic;
      rd_nwr       : in  std_logic;
      sda_in       : in  std_logic;
      bit_cnt_ovr  : in  std_logic;
      sclK         : out std_logic;
      sel_sda_out  : out std_logic_vector(2 downto 0);
      sda_out_fsm  : out std_logic;
      sv_data_b1   : out std_logic;
      sv_data_b2   : out std_logic;
      en_fec_reg   : out std_logic;
      en_bc_reg    : out std_logic;
      en_B1_dout   : out std_logic;
      en_B2_dout   : out std_logic;
      en_cnt_in    : out std_logic;
      cnt_in_ovr   : in  std_logic;
      err_fec_ack  : out std_logic;
      err_bc_add   : out std_logic;
      err_din1_ack : out std_logic;
      err_din2_ack : out std_logic;
      sv_rslt      : out std_logic;
      I2C_BSY      : out std_logic;
      end_rd       : out std_logic;
      fsm_state    : out std_logic_vector(7 downto 0);
      en_bit_cnt   : out std_logic;
      clr_bit_cnt  : out std_logic);
  end component;

end msm2_fsm_i2c_pack;
-------------------------------------------------------------------------------
--
-- EOF
--





