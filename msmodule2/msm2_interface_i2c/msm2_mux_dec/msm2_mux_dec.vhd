---------------------------------------------------------------------------------------------------
-- DESIGN RCU FIRM WARE V.2.0
-- MODULE: MONITERING & SAFETY MODULE
-- DESCRIPTION : All control Signal Active High, Data RD/WR on rising edge,ASYNCHRONOUS RESET AND CLEAR
-- CLOCK DOMAIN: RCU CLOCK / 5MHZ
-- PURPOSE     : TO SELECT THE SAD_OUT
-------------------------------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;

entity msm2_mux_dec is
  port (

    sel_sda_out : in  std_logic_vector (2 downto 0);
    sda_out_fsm : in  std_logic;
    sda_fec_add : in  std_logic;
    sda_reg_add : in  std_logic;
    sda_dat_B1  : in  std_logic;
    sda_dat_B2  : in  std_logic;
    sel_fec_add : out std_logic;
    sel_reg_add : out std_logic;
    sel_dat_B1  : out std_logic;
    sel_dat_B2  : out std_logic;
    sda_out     : out std_logic

    );                          
end msm2_mux_dec;

architecture ARCH_mux_dec of msm2_mux_dec is

begin
  
  process(sel_sda_out,
          sda_out_fsm ,
          sda_fec_add,
          sda_reg_add,
          sda_dat_B1 ,
          sda_dat_B2)

  begin
    case sel_sda_out is

      when "000" =>
        
        sel_fec_add <= '0';
        sel_reg_add <= '0';
        sel_dat_B1  <= '0';
        sel_dat_B2  <= '0';
        sda_out     <= sda_out_fsm;
        
      when "001" =>
        
        sel_fec_add <= '1';
        sel_reg_add <= '0';
        sel_dat_B1  <= '0';
        sel_dat_B2  <= '0';
        sda_out     <= sda_fec_add;
        
      when "010" =>
        
        sel_fec_add <= '0';
        sel_reg_add <= '1';
        sel_dat_B1  <= '0';
        sel_dat_B2  <= '0';
        sda_out     <= sda_reg_add;
        
      when "011" =>
        
        sel_fec_add <= '0';
        sel_reg_add <= '0';
        sel_dat_B1  <= '0';
        sel_dat_B2  <= '1';
        sda_out     <= sda_dat_B2;

      when "100" =>
        
        sel_fec_add <= '0';
        sel_reg_add <= '0';
        sel_dat_B1  <= '1';
        sel_dat_B2  <= '0';
        sda_out     <= sda_dat_B1;
        
      when others =>
        
        sel_fec_add <= '0';
        sel_reg_add <= '0';
        sel_dat_B1  <= '0';
        sel_dat_B2  <= '0';
        sda_out     <= '0';
        
    end case;
  end process;
  
  
end;

-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_mux_dec_pack is

  component msm2_mux_dec
    port (
      sel_sda_out : in  std_logic_vector (2 downto 0);
      sda_out_fsm : in  std_logic;
      sda_fec_add : in  std_logic;
      sda_reg_add : in  std_logic;
      sda_dat_B1  : in  std_logic;
      sda_dat_B2  : in  std_logic;
      sel_fec_add : out std_logic;
      sel_reg_add : out std_logic;
      sel_dat_B1  : out std_logic;
      sel_dat_B2  : out std_logic;
      sda_out     : out std_logic);
  end component;

end msm2_mux_dec_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
