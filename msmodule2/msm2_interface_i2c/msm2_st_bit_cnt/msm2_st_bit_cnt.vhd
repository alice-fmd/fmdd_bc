------------------------------------------------------------------------------------------------------
-- BLOCK: STATUS BIT COUNTER
-- POLARITIES: All control Signal Active High, ASYNCHRONOUS RESET AND CLEAR
-- CLOCK DOMAIN:10 MHZ clock
-- LAST UPDATE:25.10.07
-------------------------------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
-- use IEEE.STD_LOGIC_ARITH.all;
-- use IEEE.STD_LOGIC_UNSIGNED.all;


entity msm2_st_bit_cnt is
  generic (bit_width : integer := 8);
  port (
    --input----------------
    rst         : in  std_logic;
    clr_cnt     : in  std_logic;
    clk         : in  std_logic;
    en_cnt      : in  std_logic;
    --output----------------
    bit_cnt_ovr : out std_logic
    );           
end msm2_st_bit_cnt;

architecture count of msm2_st_bit_cnt is
  subtype cnt_t is integer range 0 to 2**12-1;
  signal count2 : cnt_t := 0;
  -- signal count2 : std_logic_vector (11 downto 0) := (others => '0');
begin
  process(clk, clr_cnt, rst, en_cnt)
  begin
    if (rst = '1' or clr_cnt = '1') then
      count2 <= 0; -- (others => '0');
    elsif (clk' event and clk = '1') then
      if en_cnt = '1' then
        if count2 <= bit_width then
          count2 <= count2 + 1;
        end if;
      end if;
    end if;
  end process;

  process(en_cnt, count2)
  begin
    if en_cnt = '1' then
      if count2 < bit_width then
        bit_cnt_ovr <= '0';
      else
        bit_cnt_ovr <= '1';
      end if;
    else
      bit_cnt_ovr <= '0';
    end if;
  end process;  
end;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_st_bit_cnt_pack is

  component msm2_st_bit_cnt
    generic (
      bit_width : integer);
    port (
      rst         : in  std_logic;
      clr_cnt     : in  std_logic;
      clk         : in  std_logic;
      en_cnt      : in  std_logic;
      bit_cnt_ovr : out std_logic);
  end component;

end msm2_st_bit_cnt_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
