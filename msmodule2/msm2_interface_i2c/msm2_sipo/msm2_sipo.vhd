

library IEEE;
use IEEE.std_logic_1164.all;
-- use IEEE.std_logic_arith.all;


entity msm2_sipo is
  
  generic(reg_wid : integer := 8);
  port(

    CLK  : in  std_logic;
    rst  : in  std_logic;
    SE   : in  std_logic;
    SI   : in  std_logic;
    SO   : out std_logic;
    DOUT : out std_logic_vector((reg_wid-1) downto 0)
    );
end msm2_sipo;

architecture arch_sipo of msm2_sipo is
  
  signal ser_val : std_logic_vector((reg_wid-1) downto 0) := (others => '0');
  
begin
  SHIFT_REGISTER_Process : process(CLK, rst)
  begin
    if (rst = '1') then
      ser_val <= (others => '0');
    elsif (CLK'event and CLK = '1') then
      if (SE = '1') then
        ser_val((reg_wid-1) downto 1) <= ser_val((reg_wid-2) downto 0);
        ser_val(0)                    <= SI;
      end if;
    end if;
  end process SHIFT_REGISTER_Process;



  DOUT <= ser_val;
  SO   <= ser_val(0);
  
end arch_sipo;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

package msm2_sipo_pack is

  component msm2_sipo
    generic (
      reg_wid : integer);
    port (
      CLK  : in  std_logic;
      rst  : in  std_logic;
      SE   : in  std_logic;
      SI   : in  std_logic;
      SO   : out std_logic;
      DOUT : out std_logic_vector((reg_wid-1) downto 0));
  end component;

end msm2_sipo_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
