


library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_ARITH.ALL;
-- use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- use work.all;

library msmodule2_lib;
use msmodule2_lib.msm2_clk_div_pack.all;
use msmodule2_lib.msm2_fsm_i2c_pack.all;
use msmodule2_lib.msm2_mux_dec_pack.all;
use msmodule2_lib.msm2_reg_so_pack.all;
use msmodule2_lib.msm2_sipo_pack.all;
use msmodule2_lib.msm2_st_bit_cnt_pack.all;

entity msm2_interface_I2C is
  
  port (
    rst          : in  std_logic;       -- RESET 
    clk          : in  std_logic;       -- CLOCK
    exec         : in  std_logic;  -- START FROM COMMAND DECODER -- START EXECUTE I2C Block
    rd_nwr       : in  std_logic;  -- READ OT WRITE OPERATION FROM CMD DECODER
    ld_regs      : in  std_logic;       -- SAVE ADD/DATA FOR TRANSACTION 
    fec_add      : in  std_logic_vector(3 downto 0);
    bc_reg_add   : in  std_logic_vector(7 downto 0);
    B1_din       : in  std_logic_vector(7 downto 0);
    B2_din       : in  std_logic_vector(7 downto 0);
    rst_rslt     : in  std_logic;
    sv_rslt      : out std_logic;
    sda_in_B1    : out std_logic_vector(7 downto 0);
    sda_in_B2    : out std_logic_vector(7 downto 0);
    end_rd       : out std_logic;
    I2C_BSY      : out std_logic;
    err_fec_ack  : out std_logic;
    err_bc_add   : out std_logic;
    err_din1_ack : out std_logic;
    err_din2_ack : out std_logic;
    fsm_state    : out std_logic_vector(7 downto 0);
    bcast        : in  std_logic;
    sda_in       : in  std_logic;       -- FROM BOARD CONTROLLER 
    sda_out      : out std_logic;       -- TO BOARD CONTROLLER
    s_clk        : out std_logic        -- I2C CLOCK TO BOARD CONTROLLER

    );

end msm2_interface_I2C;

architecture ARCH_interface_I2C of msm2_interface_I2C is

  signal en_sclK      : std_logic                     := '0';
  signal clk_sda_in   : std_logic                     := '0';
  signal sclk_90      : std_logic                     := '0';
  signal sclk_270     : std_logic                     := '0';
  signal clk_din      : std_logic                     := '0';
  signal en_bc_reg    : std_logic                     := '0';
  signal en_se_bc     : std_logic                     := '0';
  signal ld_reg       : std_logic                     := '0';
  signal clk_dout     : std_logic                     := '0';
  signal clk_dout_i   : std_logic                     := '0';
  signal bit_cnt_ovr  : std_logic                     := '0';
  signal sda_out_fsm  : std_logic                     := '0';
  signal sv_data_b1   : std_logic                     := '0';
  signal sv_data_b2   : std_logic                     := '0';
  signal en_bit_cnt   : std_logic                     := '0';
  signal clr_bit_cnt  : std_logic                     := '0';
  signal en_B1_dout   : std_logic                     := '0';
  signal en_fec_reg   : std_logic                     := '0';
  signal sel_sda_out  : std_logic_vector (2 downto 0) := "000";
  signal DIN_FEC_ADD  : std_logic_vector (7 downto 0) := X"00";
  signal sel_fec_add  : std_logic                     := '0';
  signal sda_fec_add  : std_logic                     := '0';
  signal sel_reg_add  : std_logic                     := '0';
  signal sda_reg_add  : std_logic                     := '0';
  signal sel_dat_B1   : std_logic                     := '0';
  signal sda_dat_B1   : std_logic                     := '0';
  signal sel_dat_B2   : std_logic                     := '0';
  signal sda_dat_B2   : std_logic                     := '0';
  signal clr_tout_cnt : std_logic                     := '0';
  signal en_tout_cnt  : std_logic                     := '0';
  signal tout_ack     : std_logic                     := '0';
  signal sclK         : std_logic                     := '0';
  signal en_B2_dout   : std_logic                     := '0';
  signal rst_rsl_reg  : std_logic                     := '0';
  signal en_cnt_in    : std_logic                     := '0';
  signal cnt_in_ovr   : std_logic                     := '0';
begin

  DIN_FEC_ADD <= '0' & bcast & '0' & fec_add & rd_nwr;
  clk_sda_in  <= not clk_dout_i;        -- CHOLM
  s_clk       <= sclK;
  ld_reg      <= ld_regs or exec;
--en_se_bc <= en_bc_reg or sel_reg_add;

  rst_rsl_reg <= rst or rst_rslt;

  CLOCK_DIVIDER : msm2_clk_div
    
    port map(
      rcu_clk  => clk,
      reset    => rst,
      enable   => en_sclK,
      sclk     => clk_din, -- open, -- Was clk_din,
      sclk_90  => sclk_90,
      sclk_270 => sclk_270
      );
  -- Use a 2.5MHz clock instead - 5MHz clock for FSM
  -- clk_din <= sclk_90;
  
  ST_MACHINE_I2C : msm2_fsm_i2c
    
    port map(
      rst          => rst,
      clk          => clk_din,
      exec         => exec,
      rd_nwr       => rd_nwr,
      sda_in       => sda_in,
      bit_cnt_ovr  => bit_cnt_ovr,
      sclK         => sclK,
      sel_sda_out  => sel_sda_out,
      sda_out_fsm  => sda_out_fsm,
      sv_data_b1   => sv_data_b1,
      sv_data_b2   => sv_data_b2,
      en_cnt_in    => en_cnt_in,
      cnt_in_ovr   => cnt_in_ovr,
      en_B2_dout   => en_B2_dout,
      en_bc_reg    => en_bc_reg, 
      en_fec_reg   => en_fec_reg,
      end_rd       => end_rd,
      sv_rslt      => sv_rslt,
      I2C_BSY      => I2C_BSY,
      err_fec_ack  => err_fec_ack,
      err_din1_ack => err_din1_ack,
      err_din2_ack => err_din2_ack,
      err_bc_add   => err_bc_add,
      fsm_state    => fsm_state,
      en_B1_dout   => en_B1_dout,
      en_bit_cnt   => en_bit_cnt,
      clr_bit_cnt  => clr_bit_cnt
      );

  SDA_BIT_CNT : msm2_st_bit_cnt
    generic map (
      bit_width => 8)        
    port map(
      rst         => rst,
      clr_cnt     => clr_bit_cnt,
      clk         => clk_dout,
      en_cnt      => en_bit_cnt,
      bit_cnt_ovr => bit_cnt_ovr
      );

  SDAIN_BIT_CNT : msm2_st_bit_cnt
    generic map (
      bit_width => 8)    
    port map(
      rst         => rst,
      clr_cnt     => clr_bit_cnt,
      clk         => clk_sda_in,
      en_cnt      => en_cnt_in,
      bit_cnt_ovr => cnt_in_ovr
      );

  DAT_INREG_B1 : msm2_sipo
    generic map (
      reg_wid => 8)
    port map(CLK  => clk_sda_in,
             rst  => rst_rsl_reg,
             SE   => sv_data_b1,
             SI   => sda_in,
             SO   => open,
             DOUT => sda_in_B1);

  DAT_INREG_B2 : msm2_sipo
    generic map (
      reg_wid => 8)
    port map(CLK  => clk_sda_in,
             rst  => rst_rsl_reg,
             SE   => sv_data_b2,
             SI   => sda_in,
             SO   => open,
             DOUT => sda_in_B2);

  FEC_ADD_REG : msm2_reg_so
    generic map (
      reg_width => 8)
    port map(
      DIN => DIN_FEC_ADD,
      CLK => clk_dout,
      LD  => ld_reg,
      rst => rst,
      SE  => en_fec_reg,
      SO  => sda_fec_add
      );

  BCREG_ADD_REG : msm2_reg_so
    generic map (
      reg_width => 8)
    port map(
      DIN => bc_reg_add,
      CLK => clk_dout,
      LD  => ld_reg,
      rst => rst,
      SE  => en_bc_reg,
      SO  => sda_reg_add
      );

  DAT_B1_REG : msm2_reg_so
    generic map (
      reg_width => 8)
    port map(
      DIN => B1_din,
      CLK => clk_dout,
      LD  => ld_reg,
      rst => rst,
      SE  => en_B1_dout,
      SO  => sda_dat_B1
      );

  DAT_B2_REG : msm2_reg_so
    generic map (
      reg_width => 8)
    port map(
      DIN => B2_din,
      CLK => clk_dout,
      LD  => ld_reg,
      rst => rst,
      SE  => en_B2_dout,
      SO  => sda_dat_B2
      );

  OUTPUT_MUX : msm2_mux_dec
    
    port map(

      sel_sda_out => sel_sda_out,
      sda_out_fsm => sda_out_fsm,
      sda_fec_add => sda_fec_add,
      sda_reg_add => sda_reg_add,
      sda_dat_B1  => sda_dat_B1,
      sda_dat_B2  => sda_dat_B2,
      sel_fec_add => sel_fec_add,
      sel_reg_add => sel_reg_add,
      sel_dat_B1  => sel_dat_B1,
      sel_dat_B2  => sel_dat_B2,
      sda_out     => sda_out

      );

  -- Original stuff
  process(clk_din, rst, sclk)
  begin
    if rst = '1' then
      clk_dout <= '0';
      clk_dout_i <= '0';                -- CHOLM
    elsif (clk_din'event and clk_din = '1') then
      clk_dout <= not sclk;
      clk_dout_i <= clk_dout;
    end if;
  end process;

  
  -- This will work for even long delays 
  --process(clk_din, rst, sclk)
  --begin
  --  if rst = '1' then
  --    clk_dout <= '0';
  --  elsif (clk_din'event and clk_din = '1') then
  --    clk_dout_i <= not sclk;
  --    clk_dout <= clk_dout_i; -- not sclk;
  --  end if;
  --end process;
  
end ARCH_interface_I2C;
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package msm2_interface_i2c_pack is

  component msm2_interface_I2C
    port (
      rst          : in  std_logic;
      clk          : in  std_logic;
      exec         : in  std_logic;
      rd_nwr       : in  std_logic;
      ld_regs      : in  std_logic;
      fec_add      : in  std_logic_vector(3 downto 0);
      bc_reg_add   : in  std_logic_vector(7 downto 0);
      B1_din       : in  std_logic_vector(7 downto 0);
      B2_din       : in  std_logic_vector(7 downto 0);
      rst_rslt     : in  std_logic;
      sv_rslt      : out std_logic;
      sda_in_B1    : out std_logic_vector(7 downto 0);
      sda_in_B2    : out std_logic_vector(7 downto 0);
      end_rd       : out std_logic;
      I2C_BSY      : out std_logic;
      err_fec_ack  : out std_logic;
      err_bc_add   : out std_logic;
      err_din1_ack : out std_logic;
      err_din2_ack : out std_logic;
      fsm_state    : out std_logic_vector(7 downto 0);
      bcast        : in  std_logic;
      sda_in       : in  std_logic;
      sda_out      : out std_logic;
      s_clk        : out std_logic);
  end component;

end msm2_interface_i2c_pack;
-------------------------------------------------------------------------------
--
-- EOF
--
