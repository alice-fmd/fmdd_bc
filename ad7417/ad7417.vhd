--!
--! @defgroup ad7417_lib Behavoural Model of AD7417 5-channel 10bit ADC
--! @ingroup other
--! Code in this group makes a model of the AD7417 5-channel 10bit ADC.
--! Communication with the IC goes via an @f$ I^2C@f$ bus.
--! The data sheet on the device can be found 
--! <a href="http://www.analog.com/en/analog-to-digital-converters/temperature-to-digital-converters/AD7417/products/product.html">here</a>
--!
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @ingroup ad7417_lib
--! @brief Top entity of AD7417 model
entity ad7417 is
  generic (
    VENDOR :       std_logic_vector(3 downto 0) := "0101";   --! Vendor ID
    ADD    :       std_logic_vector(2 downto 0) := "000";    --! Address
    PERIOD :       time                         := 6.6 us;   --! Period of clk
    T      :       unsigned(15 downto 0)        := X"dead";  --! T value
    ADC1   :       unsigned(15 downto 0)        := X"beaf";  --! ADC1 value
    ADC2   :       unsigned(15 downto 0)        := X"1111";  --! ADC2 value
    ADC3   :       unsigned(15 downto 0)        := X"0000";  --! ADC3 value
    ADC4   :       unsigned(15 downto 0)        := X"aaaa"); --! ADC4 value
  port (
    scl    : inout std_logic;                                --! I2C Clock
    sda    : inout std_logic);                               --! I2C data
end ad7417;

------------------------------------------------------------------------------
--! @brief Architecture of AD7417 model
--! @ingroup ad7417_lib
architecture behaviour of ad7417 is
  signal scl_sel_in_i  : boolean;             --! Tri-state select
  signal scl_sel_out_i : boolean   := false;  --! Tri-state select
  signal scl_in_i      : std_logic;           --! Clock input
  signal scl_out_i     : std_logic := '1';    --! Clock output

  signal sda_sel_in_i  : boolean;             --! Tri-state select
  signal sda_sel_out_i : boolean   := false;  --! Tri-state select
  signal sda_in_i      : std_logic;           --! Data input
  signal sda_out_i     : std_logic := '1';    --! Data output

  signal add_i  : std_logic_vector(6 downto 0) := (others => '0');
  signal data_i : std_logic_vector(7 downto 0) := (others => '0');

  signal rw_i     : boolean   := false;  --! Read-not-write
  signal start_i  : std_logic := '0';   --! Start condition
  signal stop_i   : std_logic := '0';   --! Stop condition
  signal cnt_i    : integer   := 8;     --! Counter
  signal buffer_i : std_logic_vector(7 downto 0);  --! buffer 
  signal ptr_i    : integer;            --! Address pointer

  signal no_ack_i  : boolean := false;  --! No acknowledge from master

  type channel_t is array (0 to 4) of unsigned(15 downto 0);  --! channel
  constant result_i : channel_t := (0 => T, 
                                    1 => ADC1, 
                                    2 => ADC2, 
                                    3 => ADC3, 
                                    4 => ADC4);  --! channels
  constant ME : unsigned(6 downto 0) := unsigned(VENDOR & ADD);  --! our addr.
  
  type state is (idle,
                 start,
                 who,
                 addressed,
                 ack,
                 read_data,
                 write_data,
                 ack_master);           --! State types
  signal nx_st : state;                 --! Next state
  signal st_i  : state;                 --! This state

  --! @brief Convert std_logic_vector to a string
  --! @param v The vector to convert
  --! @return String representation of @a v
  function slv2str (
    constant v   : std_logic_vector)    --! Vector
    return string
  is
    variable ret : string(v'length downto 1);
    variable i   : integer;
    variable j   : integer;
  begin  -- function slv2str

    ret := (others => 'X');
    j   := v'length;

    for i in v'high downto v'low loop
      case v(i) is
        when 'U'    => ret(j) := 'U';
        when 'X'    => ret(j) := 'X';
        when '0'    => ret(j) := '0';
        when '1'    => ret(j) := '1';
        when 'Z'    => ret(j) := 'Z';
        when 'W'    => ret(j) := 'W';
        when 'L'    => ret(j) := 'L';
        when 'H'    => ret(j) := 'H';
        when '-'    => ret(j) := '-';
        when others => ret(j) := 'X';
      end case;
      j := j - 1;
    end loop;  -- i
    return ret;
  end function slv2str;
begin  -- behaviour
  -- bla <= 'H'; -- pullup bla <= blubber when a1='1' else 'Z';  --
  -- driver1 bla <= blabber when a2='1' else 'Z';  -- driver2 purpose:
  --! @brief Tristate buffers
  combi : block
  begin  -- block combi
    scl_sel_in_i <= not scl_sel_out_i;
    sda_sel_in_i <= not sda_sel_out_i;
    scl_in_i     <= scl       when scl_sel_in_i  else scl_out_i;
    scl          <= scl_out_i when scl_sel_out_i else 'Z';
    sda_in_i     <= sda       when sda_sel_in_i  else 'Z';
    sda          <= sda_out_i when sda_sel_out_i else 'Z';
  end block combi;

  --! @brief Detect start condtion - that is, that the data line transition from
  --!        low to high, while the clock line isn't low.
  --! @param sda_in_i Serial data input
  --! @param scl_in_i Serial clock
  start_condition: process (sda_in_i, scl_in_i)
  begin  -- process start_condition
    if falling_edge(sda_in_i) and scl_in_i /= '0' then
      -- report "Got start condition" severity note;
      start_i <= '1', '0' after PERIOD;
    end if;
  end process start_condition;

  --! @brief Detect stop condition - that is, that the data line goes high
  --!        while the clock line isn't low.
  --! @param sda_in_i Serial data input
  --! @param scl_in_i Serial clock
  stop_condition: process (sda_in_i, scl_in_i)
  begin  -- process stop_condition
    if rising_edge(sda_in_i) and scl_in_i /= '0' then
      -- report " Got stop condition" severity note;
      stop_i <= '1', '0' after PERIOD;
    end if;
  end process stop_condition;
  
  --! @brief Count clock edges
  --! @param scl_in_i Serial clock
  --! @param start_i  Start condition
  --! @param stop_i   Stop condition.
  --! Increments counter on each rising clock edge
  count_clks: process (scl_in_i, start_i, stop_i)
  begin  -- process count_clks
    if start_i = '1' or stop_i = '1' then
      cnt_i <= 0;
    elsif scl_in_i'event and scl_in_i /= '0' and scl_sel_in_i then
      -- report "cnt=" & integer'image(cnt_i) severity note;
      cnt_i <= (cnt_i + 1) mod 9;
    end if;
  end process count_clks;

  --! @brief Buffer data
  --! @param start_i  Start condition
  --! @param stop_i   Stop condition.
  --! @param cnt_i    Counter
  buffer_it: process (start_i, stop_i, cnt_i)
  begin  -- process buffer_it
    if start_i = '1' or stop_i = '1' then
      buffer_i <= (others => '0');
    elsif cnt_i = 8 then
      null; -- buffer_i <= (others => '0');
    else
      buffer_i(7 - cnt_i) <= sda_in_i  after PERIOD / 4;
    end if;
  end process buffer_it;

  --! @brief Update the state
  --! @param scl_in_i Serial clock
  --! @param stop_i   Stop condition.
  --! @return the state
  update_state: process (scl_in_i, stop_i)
  begin  -- process update_state
    if stop_i = '1' then                -- asynchronous reset (active low)
      st_i <= idle;
    elsif scl_in_i'event and scl_in_i /= '0' then  -- rising clock edge
      --       report 
      --         " scl_in=" & std_logic'image(scl_in_i) &
      --         " nx_st=" & state'image(nx_st) severity note;
      st_i <= nx_st;
    end if;
  end process update_state;

  --! @brief Get the next state
  --! @param start_i  Start condition
  --! @param stop_i   Stop condition.
  --! @param cnt_i    Counter
  --! @param st_i     The state
  --! @return the next state 
  fsm                  : process (start_i, stop_i, st_i, cnt_i)
    variable have_rw_i : boolean := false;
    variable rw_ii     : boolean := false;
    variable sub_i     : integer := 0;
  begin  -- process fsm
    if stop_i = '1' then
      nx_st         <= idle;
    else
      nx_st         <= st_i;
      sda_sel_out_i <= false;
      scl_sel_out_i <= false;
      -- report
      --   " start=" & std_logic'image(start_i) &
      --   " stop=" & std_logic'image(stop_i) &
      --   " st=" & state'image(st_i) &
      --   " cnt=" & integer'image(cnt_i) severity note;
      case st_i is
        when idle =>
          rw_i      <= false;
          rw_ii                  := false;
          have_rw_i              := false;
          sub_i                  := 2;
          if start_i = '1' then
            nx_st   <= who;
          end if;

        when start =>
          nx_st <= who;

        when who =>
          if cnt_i = 7 then
            report "Buffer=" & slv2str(buffer_i(7 downto 1))
              & " we are=" & slv2str(std_logic_vector(ME)) severity note;
            if unsigned(buffer_i(7 downto 1)) = ME then
              nx_st <= addressed;
            else
              nx_st <= idle;
            end if;
          end if;

        when addressed =>
          -- scl_sel_out_i <= true after PERIOD / 2;
          -- rw_i  <= not (sda_in_i = '0') after PERIOD / 2;
          rw_i  <= not (buffer_i(0) = '0');
          nx_st <= ack;

        when ack =>
          sda_sel_out_i <= true;
          scl_sel_out_i <= true;
          sda_out_i     <= '0';
          if not have_rw_i then
            have_rw_i := true;
            rw_ii     := (buffer_i(0) /= '0');
          end if;
          if not rw_ii then
            rw_i        <= false;
            data_i      <= buffer_i;
            nx_st       <= write_data;
          else
            rw_i        <= true;
            nx_st       <= read_data;
            sub_i     := 1;
            no_ack_i    <= false;
          end if;
          scl_out_i     <= '0', '1' after PERIOD;

          
        when write_data =>
          if cnt_i = 7 then
            nx_st  <= ack;
          -- else data_i(8 - cnt_i) <= sda_in_i;
          end if;

        when read_data =>
          sda_sel_out_i <= true;
          if cnt_i = 7 then
            nx_st <= ack_master;
          -- elsif cnt_i = 0 then
          -- null;
          end if;
          if cnt_i /= 8 then
            -- report 
            --   "return(" &
            --    integer'image(ptr_i) & ")((" & 
            --    integer'image(sub_i) & " + 1) * 8 - " &
            --    integer'image(cnt_i) & "-1) => return(" &
            --    integer'image(ptr_i) & ")((" & 
            --    integer'image((sub_i+1)*8-cnt_i-1) & ")" 
            --     severity note;
            sda_out_i     <= result_i(ptr_i)((sub_i + 1) * 8 - cnt_i-1);
          end if;

        when ack_master =>
          sub_i     := (sub_i - 1) mod 2;
          no_ack_i <= sda_in_i /= '0' after PERIOD / 4;
          if no_ack_i then        -- no-ack by master
            nx_st <= idle;
          else
            nx_st <= read_data;              
          end if;
          sda_sel_out_i <= false after PERIOD / 4;
        when others => null;
      end case;
    end if;
  end process fsm;

  --! @brief Decode the instructions
  --! @param data_i  Buffer of data
  --! @param start_i  Start condition
  --! @return return_i
  decode                : process (data_i, start_i)
    variable have_cfg_i : boolean := false;  --! If we saw CFG1 address
    variable have_ptr_i : boolean := false;  --! If we saw a PTR address
    variable count_i    : integer := 0;  --! Counter
    variable base_i     : integer := 0;  --! Base
  begin  -- process decode

    if start_i = '1' then
      count_i    := 0;
    elsif data_i'event then

      -- report "data=" & integer'image(to_integer(unsigned(data_i)))
      --  severity note; 
      if count_i = 0 then
        -- report "First data=" & integer'image(to_integer(unsigned(data_i)))
        --  severity note;
        count_i := count_i + 1;

      elsif count_i = 1 then
        have_cfg_i := false;
        -- report "Second data=" & integer'image(to_integer(unsigned(data_i)))
        --  severity note;
        if unsigned(data_i(2 downto 0)) = "001" then  -- CFG1
          -- report "Saw CFG1" severity note;
          have_cfg_i := true;
          base_i     := 0;
        elsif have_ptr_i and (unsigned(data_i(2 downto 0)) = "100" or
                              unsigned(data_i(2 downto 0)) = "000") then
          -- report "Saw PTR=" & integer'image(base_i) severity note;
          ptr_i <= base_i;
        end if;
        count_i      := count_i + 1;

      elsif count_i = 2 and have_cfg_i then
        have_ptr_i := false;
        base_i     := 0;
        -- report "Third data=" & integer'image(to_integer(unsigned(data_i)))
        --  severity note;
        if have_cfg_i and unsigned(data_i(4 downto 0)) = "00010" then
          -- report "Got address pointer " &
          --  integer'image(to_integer(unsigned(data_i(7 downto 5))))
          --  severity note;
          have_ptr_i := true;
          base_i     := to_integer(unsigned(data_i(7 downto 5)));
        end if;
        count_i      := count_i + 1;
      end if;

    end if;

  end process decode;

end behaviour;
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @brief Package of AD7417 model
--! @ingroup ad7417_lib
package ad7417_pack is
  component ad7417
    generic (
    VENDOR   :       std_logic_vector(3 downto 0) := "0101";
      ADD    :       std_logic_vector(2 downto 0) := "000";
      PERIOD :       time                         := 6.6 us;
      T      :       unsigned(15 downto 0)        := X"dead";
      ADC1   :       unsigned(15 downto 0)        := X"beaf";
      ADC2   :       unsigned(15 downto 0)        := X"1111";
      ADC3   :       unsigned(15 downto 0)        := X"0000";
      ADC4   :       unsigned(15 downto 0)        := X"aaaa");
    port (
      scl    : inout std_logic;
      sda    : inout std_logic);
  end component;
end ad7417_pack;
------------------------------------------------------------------------------
--
-- EOF
--
