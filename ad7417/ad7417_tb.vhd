------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ad7417_model;
use ad7417_model.ad7417_pack.all;

------------------------------------------------------------------------------
entity ad7417_tb is
end ad7417_tb;

------------------------------------------------------------------------------

architecture test of ad7417_tb is
  constant ADD    : std_logic_vector(2 downto 0) := "000";
  constant VENDOR : std_logic_vector(3 downto 0) := "0101";
  constant PERIOD : time                         := 25 ns;

  signal scl_i : std_logic;
  signal sda_i : std_logic;

  signal clk_i     : std_logic := '0';
  signal sel_i     : std_logic := '0';
  signal sel_scl_i : std_logic := '0';
  signal scl_in_i  : std_logic;
  signal sda_in_i  : std_logic;
  signal scl_out_i : std_logic := '1';
  signal sda_out_i : std_logic := '0';
  signal data_i    : std_logic_vector(7 downto 0);

  signal in_start_i : std_logic := '0';
  signal in_stop_i  : std_logic := '0';

  -- purpose: Send 1 byte of data
  procedure write_byte (
    constant data    : in  std_logic_vector(7 downto 0);  -- Data
    signal   clk     : in  std_logic;   -- Clock
    signal   sel     : out std_logic;   -- Select output
    signal   sel_scl : out std_logic;   -- Select clock
    signal   sda     : out std_logic;   -- Serial data
    signal   ack     : in  std_logic)   -- Acknowledge from slave
  is
    variable i       :     integer := 8;  -- counter
  begin  -- write_byte

    while i > 0 loop
      if clk /= '0' then
        wait until falling_edge(clk);
      end if;
      wait for PERIOD / 4;
      sda     <= data(i-1);
      i := i - 1;
      wait until rising_edge(clk);
      sel     <= '1';
      sel_scl <= '1';
    end loop;

    wait until falling_edge(clk);
    wait for PERIOD / 4;
    sel      <= '0';
    sel_scl  <= '0';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    assert ack = '0' report "No acknowledge from slave! " &
      std_logic'image(ack) severity warning;
    
  end write_byte;

  -- purpose: Send 1 byte of data
  procedure read_byte (
    signal   data : out std_logic_vector(7 downto 0);  -- Data
    signal   clk  : in  std_logic;      -- Clock
    signal   sel  : out std_logic;      -- Select output
    signal   sda  : in  std_logic;      -- Serial data
    signal   ack  : out std_logic;      -- Acknowledge to slave
    constant last : in  boolean)        -- Whether this is the last word
  is
    variable i    :     integer := 8;   -- counter
  begin  -- read_byte
    sel           <= '0';

    while i > 0 loop
      wait until rising_edge(clk);
      data(i - 1) <= sda;
      i := i - 1;
    end loop;

    wait until falling_edge(clk);
    wait for PERIOD / 4;
    sel   <= '1';
    if last then                        -- No acknowledge
      ack <= '1';
    else
      ack <= '0';
    end if;
    wait until rising_edge(clk);
    wait until falling_edge(clk);
    if last then
      sel <= '1';
    else
      sel <= '0';
    end if;
  end read_byte;

  -- purpose: Make a start condition
  procedure write_start (
    signal clk     : in  std_logic;     -- Clock
    signal sel     : out std_logic;     -- Select output
    signal sel_scl : out std_logic;     -- Select clock
    signal sda     : out std_logic;   -- Serial data
    signal here    : out std_logic) 
  is
  begin  -- write_start
    report "make start condition" severity note;
    here <= '1';
    wait until rising_edge(clk);
    sda     <= '1';
    sel     <= '1';
    wait for PERIOD / 4;
    sda     <= '0';
    sel_scl <= '1';
    -- wait until falling_edge(clk);
    here <= '0';
  end write_start;

  -- purpose: Make a stop condition
  procedure write_stop (
    signal clk     : in  std_logic;     -- Clock
    signal sel     : out std_logic;     -- Select
    signal sel_scl : out std_logic;     -- Select clock
    signal sda     : out std_logic;     -- Serial data
    signal here    : out std_logic)
  is
  begin  -- write_stop
    here <= '1';
    report "make stop condition" severity note;
    wait until rising_edge(clk);
    sel     <= '1';
    sel_scl <= '1';
    sda     <= '0';
    wait for PERIOD / 4;
    sda     <= '1';
    wait until rising_edge(clk);
    sel_scl <= '0';
    -- wait until falling_edge(clk);
    here <= '0';
  end write_stop;

  -- purpose: Send 1 byte of data to AD7417
  procedure write_1 (
    constant add      : in  std_logic_vector(2 downto 0);  -- Address
    constant data1    : in  std_logic_vector(7 downto 0);  -- Data
    signal   clk      : in  std_logic;                     -- Clock
    signal   sel      : out std_logic;                     -- Select
    signal   sel_scl  : out std_logic;                     -- Select clock
    signal   sda_out  : out std_logic;                     -- Data out
    signal   sda_in   : in  std_logic;                     -- Data in
    signal   in_start : out std_logic;
    signal   in_stop  : out std_logic)
  is
  begin  -- write_1
    sel <= '0';
    write_start(clk     => clk,
                sel     => sel,
                sda     => sda_out,
                sel_scl => sel_scl,
                here    => in_start);
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => VENDOR & ADD & '0');
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => data1);
    write_stop(clk      => clk,
               sel      => sel,
               sda      => sda_out,
               sel_scl  => sel_scl,
               here     => in_stop);
    sel <= '0';
  end write_1;

  -- purpose: Send 2 byte of data to AD7417
  procedure write_2 (
    constant add      : in  std_logic_vector(2 downto 0);  -- Address
    constant data1    : in  std_logic_vector(7 downto 0);  -- Data
    constant data2    : in  std_logic_vector(7 downto 0);  -- Data
    signal   clk      : in  std_logic;                     -- Clock
    signal   sel      : out std_logic;                     -- Select
    signal   sel_scl  : out std_logic;                     -- Select clock
    signal   sda_out  : out std_logic;                     -- Data out
    signal   sda_in   : in  std_logic;                     -- Data in
    signal   in_start : out std_logic;
    signal   in_stop  : out std_logic)
  is
  begin  -- write_2
    sel <= '0';
    write_start(clk     => clk,
                sel     => sel,
                sda     => sda_out,
                sel_scl => sel_scl,
                here    => in_start);
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => VENDOR & ADD & '0');
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => data1);
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => data2);
    write_stop(clk      => clk,
               sel      => sel,
               sda      => sda_out,
               sel_scl  => sel_scl,
               here     => in_stop);
    sel <= '0';
  end write_2;

  -- purpose: Send 3 byte of data to AD7417
  procedure write_3 (
    constant add      : in  std_logic_vector(2 downto 0);  -- Address
    constant data1    : in  std_logic_vector(7 downto 0);  -- Data
    constant data2    : in  std_logic_vector(7 downto 0);  -- Data
    constant data3    : in  std_logic_vector(7 downto 0);  -- Data
    signal   clk      : in  std_logic;                     -- Clock
    signal   sel      : out std_logic;                     -- Select
    signal   sel_scl  : out std_logic;                     -- Select clock
    signal   sda_out  : out std_logic;                     -- Data out
    signal   sda_in   : in  std_logic;                     -- Data in
    signal   in_start : out std_logic;
    signal   in_stop  : out std_logic)
  is
  begin  -- write_2
    sel <= '0';
    write_start(clk     => clk,
                sel     => sel,
                sda     => sda_out,
                sel_scl => sel_scl,
                here    => in_start);
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => VENDOR & ADD & '0');
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => data1);
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => data2);
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => data3);
    write_stop(clk      => clk,
               sel      => sel,
               sda      => sda_out,
               sel_scl  => sel_scl,
               here     => in_stop);
    sel <= '0';
  end write_3;

  -- purpose: Send 1 byte of data to AD7417
  procedure read_1 (
    constant add      : in  std_logic_vector(2 downto 0);  -- Address
    signal   data     : out std_logic_vector(7 downto 0);  -- Data
    signal   clk      : in  std_logic;                     -- Clock
    signal   sel      : out std_logic;                     -- Select
    signal   sel_scl  : out std_logic;                     -- Select clock
    signal   sda_out  : out std_logic;                     -- Data out
    signal   sda_in   : in  std_logic;                     -- Data in
    signal   in_start : out std_logic;
    signal   in_stop  : out std_logic)
  is
  begin  -- read_1
    sel     <= '0';
    sel_scl <= '1';
    write_start(clk     => clk,
                sel     => sel,
                sda     => sda_out,
                sel_scl => sel_scl,
                here    => in_start);
    write_byte(clk      => clk,
               sel      => sel,
               sel_scl  => sel_scl,
               sda      => sda_out,
               ack      => sda_in,
               data     => VENDOR & ADD & '1');
    read_byte(clk       => clk,
              sel       => sel,
              sda       => sda_in,
              ack       => sda_out,
              data      => data,
              last      => true);
    write_stop(clk      => clk,
               sel      => sel,
               sda      => sda_out,
               sel_scl  => sel_scl,
               here     => in_stop);
    report "End of addressed reading" severity note;
    sel_scl <= '0';
    sel     <= '0';
  end read_1;

begin  -- test
  -- purpose: Combinatorics
  combi : block
  begin  -- block combi
    sda_i    <= sda_out_i when sel_i = '1'     else 'H';
    sda_in_i <= sda_i     when sel_i = '0'     else 'Z';
    scl_i    <= clk_i     when sel_scl_i = '1' else 'H';
    scl_in_i <= scl_i     when sel_scl_i = '0' else 'Z';
  end block combi;

  DUT : ad7417
    generic map (
      ADD    => ADD,
      VENDOR => VENDOR,
      PERIOD => PERIOD)
    port map (
      scl    => scl_i,
      sda    => sda_i);

  -- purpose: provide stimuli
  stimuli : process
  begin  -- process stimuli
    wait for 100 ns;

    write_1(add      => ADD,
            data1    => "11111111",
            clk      => clk_i,
            sel      => sel_i,
            sel_scl  => sel_scl_i,
            sda_out  => sda_out_i,
            sda_in   => sda_in_i,
            in_start => in_start_i,
            in_stop  => in_stop_i);

    wait for 100 ns;
    write_2(add      => ADD,
            data1    => "10101010",
            data2    => "01010101",
            clk      => clk_i,
            sel      => sel_i,
            sel_scl  => sel_scl_i,
            sda_out  => sda_out_i,
            sda_in   => sda_in_i,
            in_start => in_start_i,
            in_stop  => in_stop_i);

    wait for 100 ns;
    read_1(add      => ADD,
           data     => data_i,
           clk      => clk_i,
           sel      => sel_i,
           sel_scl  => sel_scl_i,
           sda_out  => sda_out_i,
           sda_in   => sda_in_i,
           in_start => in_start_i,
           in_stop  => in_stop_i);

    wait;                               -- Forever
  end process stimuli;

  -- purpose: Make clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clock_gen : process
  begin  -- process clock_gen
    wait for PERIOD / 2;
    clk_i <= not clk_i;
  end process clock_gen;
end test;

------------------------------------------------------------------------------
